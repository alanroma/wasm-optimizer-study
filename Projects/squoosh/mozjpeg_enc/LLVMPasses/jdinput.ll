; ModuleID = 'jdinput.c'
source_filename = "jdinput.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_input_controller = type { %struct.jpeg_input_controller, i32 }

; Function Attrs: nounwind
define hidden void @jinit_input_controller(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %inputctl = alloca %struct.my_input_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_decompress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 0, i32 28)
  %6 = bitcast i8* %call to %struct.my_input_controller*
  store %struct.my_input_controller* %6, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %7 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %8 = bitcast %struct.my_input_controller* %7 to %struct.jpeg_input_controller*
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 81
  store %struct.jpeg_input_controller* %8, %struct.jpeg_input_controller** %inputctl1, align 4, !tbaa !14
  %10 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %10, i32 0, i32 0
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub, i32 0, i32 0
  store i32 (%struct.jpeg_decompress_struct*)* @consume_markers, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !15
  %11 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %11, i32 0, i32 0
  %reset_input_controller = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub2, i32 0, i32 1
  %reset_input_controller3 = bitcast {}** %reset_input_controller to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @reset_input_controller, void (%struct.jpeg_decompress_struct*)** %reset_input_controller3, align 4, !tbaa !18
  %12 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %12, i32 0, i32 0
  %start_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub4, i32 0, i32 2
  %start_input_pass5 = bitcast {}** %start_input_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_input_pass, void (%struct.jpeg_decompress_struct*)** %start_input_pass5, align 4, !tbaa !19
  %13 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub6 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %13, i32 0, i32 0
  %finish_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub6, i32 0, i32 3
  %finish_input_pass7 = bitcast {}** %finish_input_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @finish_input_pass, void (%struct.jpeg_decompress_struct*)** %finish_input_pass7, align 4, !tbaa !20
  %14 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub8 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %14, i32 0, i32 0
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub8, i32 0, i32 4
  store i32 0, i32* %has_multiple_scans, align 4, !tbaa !21
  %15 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub9 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %15, i32 0, i32 0
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub9, i32 0, i32 5
  store i32 0, i32* %eoi_reached, align 4, !tbaa !22
  %16 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %inheaders = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %16, i32 0, i32 1
  store i32 1, i32* %inheaders, align 4, !tbaa !23
  %17 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @consume_markers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %inputctl = alloca %struct.my_input_controller*, align 4
  %val = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 81
  %2 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_input_controller* %2 to %struct.my_input_controller*
  store %struct.my_input_controller* %3, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %4 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %5, i32 0, i32 0
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub, i32 0, i32 5
  %6 = load i32, i32* %eoi_reached, align 4, !tbaa !22
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 82
  %8 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %read_markers = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %8, i32 0, i32 1
  %9 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %read_markers, align 4, !tbaa !25
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %9(%struct.jpeg_decompress_struct* %10)
  store i32 %call, i32* %val, align 4, !tbaa !27
  %11 = load i32, i32* %val, align 4, !tbaa !27
  switch i32 %11, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb11
    i32 0, label %sw.epilog
  ]

sw.bb:                                            ; preds = %if.end
  %12 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %inheaders = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %12, i32 0, i32 1
  %13 = load i32, i32* %inheaders, align 4, !tbaa !23
  %tobool2 = icmp ne i32 %13, 0
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %sw.bb
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @initial_setup(%struct.jpeg_decompress_struct* %14)
  %15 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %inheaders4 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %15, i32 0, i32 1
  store i32 0, i32* %inheaders4, align 4, !tbaa !23
  br label %if.end10

if.else:                                          ; preds = %sw.bb
  %16 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %16, i32 0, i32 0
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub5, i32 0, i32 4
  %17 = load i32, i32* %has_multiple_scans, align 4, !tbaa !21
  %tobool6 = icmp ne i32 %17, 0
  br i1 %tobool6, label %if.end9, label %if.then7

if.then7:                                         ; preds = %if.else
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 35, i32* %msg_code, align 4, !tbaa !29
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !28
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 0
  %22 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !31
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24)
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.else
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_input_pass(%struct.jpeg_decompress_struct* %25)
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %if.then3
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.end
  %26 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %26, i32 0, i32 0
  %eoi_reached13 = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub12, i32 0, i32 5
  store i32 1, i32* %eoi_reached13, align 4, !tbaa !22
  %27 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %inheaders14 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %27, i32 0, i32 1
  %28 = load i32, i32* %inheaders14, align 4, !tbaa !23
  %tobool15 = icmp ne i32 %28, 0
  br i1 %tobool15, label %if.then16, label %if.else25

if.then16:                                        ; preds = %sw.bb11
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 82
  %30 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker17, align 8, !tbaa !24
  %saw_SOF = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %30, i32 0, i32 4
  %31 = load i32, i32* %saw_SOF, align 4, !tbaa !32
  %tobool18 = icmp ne i32 %31, 0
  br i1 %tobool18, label %if.then19, label %if.end24

if.then19:                                        ; preds = %if.then16
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !28
  %msg_code21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 5
  store i32 59, i32* %msg_code21, align 4, !tbaa !29
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err22, align 8, !tbaa !28
  %error_exit23 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 0
  %36 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit23, align 4, !tbaa !31
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %38 = bitcast %struct.jpeg_decompress_struct* %37 to %struct.jpeg_common_struct*
  call void %36(%struct.jpeg_common_struct* %38)
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %if.then16
  br label %if.end30

if.else25:                                        ; preds = %sw.bb11
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 37
  %40 = load i32, i32* %output_scan_number, align 8, !tbaa !33
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 35
  %42 = load i32, i32* %input_scan_number, align 8, !tbaa !34
  %cmp = icmp sgt i32 %40, %42
  br i1 %cmp, label %if.then26, label %if.end29

if.then26:                                        ; preds = %if.else25
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 35
  %44 = load i32, i32* %input_scan_number27, align 8, !tbaa !34
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 37
  store i32 %44, i32* %output_scan_number28, align 8, !tbaa !33
  br label %if.end29

if.end29:                                         ; preds = %if.then26, %if.else25
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.end24
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end, %if.end, %if.end30, %if.end10
  %46 = load i32, i32* %val, align 4, !tbaa !27
  store i32 %46, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %if.then
  %47 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

; Function Attrs: nounwind
define internal void @reset_input_controller(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %inputctl = alloca %struct.my_input_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 81
  %2 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_input_controller* %2 to %struct.my_input_controller*
  store %struct.my_input_controller* %3, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %4 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %4, i32 0, i32 0
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub, i32 0, i32 0
  store i32 (%struct.jpeg_decompress_struct*)* @consume_markers, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !15
  %5 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %5, i32 0, i32 0
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub2, i32 0, i32 4
  store i32 0, i32* %has_multiple_scans, align 4, !tbaa !21
  %6 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %6, i32 0, i32 0
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %pub3, i32 0, i32 5
  store i32 0, i32* %eoi_reached, align 4, !tbaa !22
  %7 = load %struct.my_input_controller*, %struct.my_input_controller** %inputctl, align 4, !tbaa !2
  %inheaders = getelementptr inbounds %struct.my_input_controller, %struct.my_input_controller* %7, i32 0, i32 1
  store i32 1, i32* %inheaders, align 4, !tbaa !23
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %reset_error_mgr = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 4
  %10 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %reset_error_mgr, align 4, !tbaa !35
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %12 = bitcast %struct.jpeg_decompress_struct* %11 to %struct.jpeg_common_struct*
  call void %10(%struct.jpeg_common_struct* %12)
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 82
  %14 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %reset_marker_reader = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %14, i32 0, i32 0
  %reset_marker_reader4 = bitcast {}** %reset_marker_reader to void (%struct.jpeg_decompress_struct*)**
  %15 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %reset_marker_reader4, align 4, !tbaa !36
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %15(%struct.jpeg_decompress_struct* %16)
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 39
  store [64 x i32]* null, [64 x i32]** %coef_bits, align 8, !tbaa !37
  %18 = bitcast %struct.my_input_controller** %inputctl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret void
}

; Function Attrs: nounwind
define internal void @start_input_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @per_scan_setup(%struct.jpeg_decompress_struct* %0)
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @latch_quant_tables(%struct.jpeg_decompress_struct* %1)
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 83
  %3 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy, align 4, !tbaa !38
  %start_pass = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %3, i32 0, i32 0
  %start_pass1 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  %4 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_pass1, align 4, !tbaa !39
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %4(%struct.jpeg_decompress_struct* %5)
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 79
  %7 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !41
  %start_input_pass = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %7, i32 0, i32 0
  %start_input_pass2 = bitcast {}** %start_input_pass to void (%struct.jpeg_decompress_struct*)**
  %8 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_input_pass2, align 4, !tbaa !42
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %8(%struct.jpeg_decompress_struct* %9)
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 79
  %11 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef3, align 4, !tbaa !41
  %consume_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %11, i32 0, i32 1
  %12 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_data, align 4, !tbaa !44
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 81
  %14 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !14
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %14, i32 0, i32 0
  store i32 (%struct.jpeg_decompress_struct*)* %12, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !45
  ret void
}

; Function Attrs: nounwind
define internal void @finish_input_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 81
  %1 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !14
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %1, i32 0, i32 0
  store i32 (%struct.jpeg_decompress_struct*)* @consume_markers, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !45
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @initial_setup(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 8
  %3 = load i32, i32* %image_height, align 8, !tbaa !46
  %cmp = icmp sgt i32 %3, 65500
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 7
  %5 = load i32, i32* %image_width, align 4, !tbaa !47
  %cmp1 = icmp sgt i32 %5, 65500
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 5
  store i32 41, i32* %msg_code, align 4, !tbaa !29
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !28
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 65500, i32* %arrayidx, align 4, !tbaa !48
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !28
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !31
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 43
  %16 = load i32, i32* %data_precision, align 4, !tbaa !49
  %cmp4 = icmp ne i32 %16, 8
  br i1 %cmp4, label %if.then5, label %if.end15

if.then5:                                         ; preds = %if.end
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !28
  %msg_code7 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 15, i32* %msg_code7, align 4, !tbaa !29
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 43
  %20 = load i32, i32* %data_precision8, align 4, !tbaa !49
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !28
  %msg_parm10 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 6
  %i11 = bitcast %union.anon* %msg_parm10 to [8 x i32]*
  %arrayidx12 = getelementptr inbounds [8 x i32], [8 x i32]* %i11, i32 0, i32 0
  store i32 %20, i32* %arrayidx12, align 4, !tbaa !48
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !28
  %error_exit14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit14, align 4, !tbaa !31
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_decompress_struct* %26 to %struct.jpeg_common_struct*
  call void %25(%struct.jpeg_common_struct* %27)
  br label %if.end15

if.end15:                                         ; preds = %if.then5, %if.end
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 9
  %29 = load i32, i32* %num_components, align 4, !tbaa !50
  %cmp16 = icmp sgt i32 %29, 10
  br i1 %cmp16, label %if.then17, label %if.end31

if.then17:                                        ; preds = %if.end15
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !28
  %msg_code19 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 5
  store i32 26, i32* %msg_code19, align 4, !tbaa !29
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 9
  %33 = load i32, i32* %num_components20, align 4, !tbaa !50
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err21, align 8, !tbaa !28
  %msg_parm22 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 6
  %i23 = bitcast %union.anon* %msg_parm22 to [8 x i32]*
  %arrayidx24 = getelementptr inbounds [8 x i32], [8 x i32]* %i23, i32 0, i32 0
  store i32 %33, i32* %arrayidx24, align 4, !tbaa !48
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !28
  %msg_parm26 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 6
  %i27 = bitcast %union.anon* %msg_parm26 to [8 x i32]*
  %arrayidx28 = getelementptr inbounds [8 x i32], [8 x i32]* %i27, i32 0, i32 1
  store i32 10, i32* %arrayidx28, align 4, !tbaa !48
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !28
  %error_exit30 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 0
  %40 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit30, align 4, !tbaa !31
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = bitcast %struct.jpeg_decompress_struct* %41 to %struct.jpeg_common_struct*
  call void %40(%struct.jpeg_common_struct* %42)
  br label %if.end31

if.end31:                                         ; preds = %if.then17, %if.end15
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 61
  store i32 1, i32* %max_h_samp_factor, align 4, !tbaa !51
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 62
  store i32 1, i32* %max_v_samp_factor, align 8, !tbaa !52
  store i32 0, i32* %ci, align 4, !tbaa !27
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 44
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !53
  store %struct.jpeg_component_info* %46, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end31
  %47 = load i32, i32* %ci, align 4, !tbaa !27
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 9
  %49 = load i32, i32* %num_components32, align 4, !tbaa !50
  %cmp33 = icmp slt i32 %47, %49
  br i1 %cmp33, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %50 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %50, i32 0, i32 2
  %51 = load i32, i32* %h_samp_factor, align 4, !tbaa !54
  %cmp34 = icmp sle i32 %51, 0
  br i1 %cmp34, label %if.then43, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %for.body
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %52, i32 0, i32 2
  %53 = load i32, i32* %h_samp_factor36, align 4, !tbaa !54
  %cmp37 = icmp sgt i32 %53, 4
  br i1 %cmp37, label %if.then43, label %lor.lhs.false38

lor.lhs.false38:                                  ; preds = %lor.lhs.false35
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 3
  %55 = load i32, i32* %v_samp_factor, align 4, !tbaa !56
  %cmp39 = icmp sle i32 %55, 0
  br i1 %cmp39, label %if.then43, label %lor.lhs.false40

lor.lhs.false40:                                  ; preds = %lor.lhs.false38
  %56 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor41 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %56, i32 0, i32 3
  %57 = load i32, i32* %v_samp_factor41, align 4, !tbaa !56
  %cmp42 = icmp sgt i32 %57, 4
  br i1 %cmp42, label %if.then43, label %if.end48

if.then43:                                        ; preds = %lor.lhs.false40, %lor.lhs.false38, %lor.lhs.false35, %for.body
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err44 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 0
  %59 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err44, align 8, !tbaa !28
  %msg_code45 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %59, i32 0, i32 5
  store i32 18, i32* %msg_code45, align 4, !tbaa !29
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 0
  %61 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !28
  %error_exit47 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %61, i32 0, i32 0
  %62 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit47, align 4, !tbaa !31
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %64 = bitcast %struct.jpeg_decompress_struct* %63 to %struct.jpeg_common_struct*
  call void %62(%struct.jpeg_common_struct* %64)
  br label %if.end48

if.end48:                                         ; preds = %if.then43, %lor.lhs.false40
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 61
  %66 = load i32, i32* %max_h_samp_factor49, align 4, !tbaa !51
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor50 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 0, i32 2
  %68 = load i32, i32* %h_samp_factor50, align 4, !tbaa !54
  %cmp51 = icmp sgt i32 %66, %68
  br i1 %cmp51, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end48
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 61
  %70 = load i32, i32* %max_h_samp_factor52, align 4, !tbaa !51
  br label %cond.end

cond.false:                                       ; preds = %if.end48
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 2
  %72 = load i32, i32* %h_samp_factor53, align 4, !tbaa !54
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %70, %cond.true ], [ %72, %cond.false ]
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 61
  store i32 %cond, i32* %max_h_samp_factor54, align 4, !tbaa !51
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 62
  %75 = load i32, i32* %max_v_samp_factor55, align 8, !tbaa !52
  %76 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor56 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %76, i32 0, i32 3
  %77 = load i32, i32* %v_samp_factor56, align 4, !tbaa !56
  %cmp57 = icmp sgt i32 %75, %77
  br i1 %cmp57, label %cond.true58, label %cond.false60

cond.true58:                                      ; preds = %cond.end
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor59 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 62
  %79 = load i32, i32* %max_v_samp_factor59, align 8, !tbaa !52
  br label %cond.end62

cond.false60:                                     ; preds = %cond.end
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor61 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 3
  %81 = load i32, i32* %v_samp_factor61, align 4, !tbaa !56
  br label %cond.end62

cond.end62:                                       ; preds = %cond.false60, %cond.true58
  %cond63 = phi i32 [ %79, %cond.true58 ], [ %81, %cond.false60 ]
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor64 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %82, i32 0, i32 62
  store i32 %cond63, i32* %max_v_samp_factor64, align 8, !tbaa !52
  br label %for.inc

for.inc:                                          ; preds = %cond.end62
  %83 = load i32, i32* %ci, align 4, !tbaa !27
  %inc = add nsw i32 %83, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !27
  %84 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %84, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 63
  store i32 8, i32* %min_DCT_scaled_size, align 4, !tbaa !57
  store i32 0, i32* %ci, align 4, !tbaa !27
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 44
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info65, align 8, !tbaa !53
  store %struct.jpeg_component_info* %87, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc94, %for.end
  %88 = load i32, i32* %ci, align 4, !tbaa !27
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 9
  %90 = load i32, i32* %num_components67, align 4, !tbaa !50
  %cmp68 = icmp slt i32 %88, %90
  br i1 %cmp68, label %for.body69, label %for.end97

for.body69:                                       ; preds = %for.cond66
  %91 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %91, i32 0, i32 9
  store i32 8, i32* %DCT_scaled_size, align 4, !tbaa !58
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width70 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %92, i32 0, i32 7
  %93 = load i32, i32* %image_width70, align 4, !tbaa !47
  %94 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor71 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %94, i32 0, i32 2
  %95 = load i32, i32* %h_samp_factor71, align 4, !tbaa !54
  %mul = mul nsw i32 %93, %95
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor72 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 61
  %97 = load i32, i32* %max_h_samp_factor72, align 4, !tbaa !51
  %mul73 = mul nsw i32 %97, 8
  %call = call i32 @jdiv_round_up(i32 %mul, i32 %mul73)
  %98 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %98, i32 0, i32 7
  store i32 %call, i32* %width_in_blocks, align 4, !tbaa !59
  %99 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height74 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %99, i32 0, i32 8
  %100 = load i32, i32* %image_height74, align 8, !tbaa !46
  %101 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor75 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %101, i32 0, i32 3
  %102 = load i32, i32* %v_samp_factor75, align 4, !tbaa !56
  %mul76 = mul nsw i32 %100, %102
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor77 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %103, i32 0, i32 62
  %104 = load i32, i32* %max_v_samp_factor77, align 8, !tbaa !52
  %mul78 = mul nsw i32 %104, 8
  %call79 = call i32 @jdiv_round_up(i32 %mul76, i32 %mul78)
  %105 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %105, i32 0, i32 8
  store i32 %call79, i32* %height_in_blocks, align 4, !tbaa !60
  %106 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %106, i32 0, i32 77
  %107 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !61
  %first_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %107, i32 0, i32 5
  %108 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx80 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col, i32 0, i32 %108
  store i32 0, i32* %arrayidx80, align 4, !tbaa !27
  %109 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks81 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %109, i32 0, i32 7
  %110 = load i32, i32* %width_in_blocks81, align 4, !tbaa !59
  %sub = sub i32 %110, 1
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master82 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %111, i32 0, i32 77
  %112 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master82, align 4, !tbaa !61
  %last_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %112, i32 0, i32 6
  %113 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx83 = getelementptr inbounds [10 x i32], [10 x i32]* %last_MCU_col, i32 0, i32 %113
  store i32 %sub, i32* %arrayidx83, align 4, !tbaa !27
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width84 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 7
  %115 = load i32, i32* %image_width84, align 4, !tbaa !47
  %116 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor85 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %116, i32 0, i32 2
  %117 = load i32, i32* %h_samp_factor85, align 4, !tbaa !54
  %mul86 = mul nsw i32 %115, %117
  %118 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %118, i32 0, i32 61
  %119 = load i32, i32* %max_h_samp_factor87, align 4, !tbaa !51
  %call88 = call i32 @jdiv_round_up(i32 %mul86, i32 %119)
  %120 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %120, i32 0, i32 10
  store i32 %call88, i32* %downsampled_width, align 4, !tbaa !62
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height89 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 8
  %122 = load i32, i32* %image_height89, align 8, !tbaa !46
  %123 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor90 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %123, i32 0, i32 3
  %124 = load i32, i32* %v_samp_factor90, align 4, !tbaa !56
  %mul91 = mul nsw i32 %122, %124
  %125 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor92 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %125, i32 0, i32 62
  %126 = load i32, i32* %max_v_samp_factor92, align 8, !tbaa !52
  %call93 = call i32 @jdiv_round_up(i32 %mul91, i32 %126)
  %127 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %127, i32 0, i32 11
  store i32 %call93, i32* %downsampled_height, align 4, !tbaa !63
  %128 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %128, i32 0, i32 12
  store i32 1, i32* %component_needed, align 4, !tbaa !64
  %129 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %129, i32 0, i32 19
  store %struct.JQUANT_TBL* null, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !65
  br label %for.inc94

for.inc94:                                        ; preds = %for.body69
  %130 = load i32, i32* %ci, align 4, !tbaa !27
  %inc95 = add nsw i32 %130, 1
  store i32 %inc95, i32* %ci, align 4, !tbaa !27
  %131 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr96 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %131, i32 1
  store %struct.jpeg_component_info* %incdec.ptr96, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond66

for.end97:                                        ; preds = %for.cond66
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height98 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %132, i32 0, i32 8
  %133 = load i32, i32* %image_height98, align 8, !tbaa !46
  %134 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor99 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %134, i32 0, i32 62
  %135 = load i32, i32* %max_v_samp_factor99, align 8, !tbaa !52
  %mul100 = mul nsw i32 %135, 8
  %call101 = call i32 @jdiv_round_up(i32 %133, i32 %mul100)
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %136, i32 0, i32 64
  store i32 %call101, i32* %total_iMCU_rows, align 8, !tbaa !66
  %137 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %137, i32 0, i32 66
  %138 = load i32, i32* %comps_in_scan, align 8, !tbaa !67
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components102 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 9
  %140 = load i32, i32* %num_components102, align 4, !tbaa !50
  %cmp103 = icmp slt i32 %138, %140
  br i1 %cmp103, label %if.then105, label %lor.lhs.false104

lor.lhs.false104:                                 ; preds = %for.end97
  %141 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %141, i32 0, i32 45
  %142 = load i32, i32* %progressive_mode, align 4, !tbaa !68
  %tobool = icmp ne i32 %142, 0
  br i1 %tobool, label %if.then105, label %if.else

if.then105:                                       ; preds = %lor.lhs.false104, %for.end97
  %143 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %143, i32 0, i32 81
  %144 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !14
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %144, i32 0, i32 4
  store i32 1, i32* %has_multiple_scans, align 4, !tbaa !69
  br label %if.end108

if.else:                                          ; preds = %lor.lhs.false104
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl106 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %145, i32 0, i32 81
  %146 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl106, align 4, !tbaa !14
  %has_multiple_scans107 = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %146, i32 0, i32 4
  store i32 0, i32* %has_multiple_scans107, align 4, !tbaa !69
  br label %if.end108

if.end108:                                        ; preds = %if.else, %if.then105
  %147 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %148 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  ret void
}

declare i32 @jdiv_round_up(i32, i32) #2

; Function Attrs: nounwind
define internal void @per_scan_setup(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci = alloca i32, align 4
  %mcublks = alloca i32, align 4
  %tmp = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %mcublks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 66
  %5 = load i32, i32* %comps_in_scan, align 8, !tbaa !67
  %cmp = icmp eq i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 67
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %7 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %7, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %8 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %8, i32 0, i32 7
  %9 = load i32, i32* %width_in_blocks, align 4, !tbaa !59
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 68
  store i32 %9, i32* %MCUs_per_row, align 4, !tbaa !70
  %11 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %11, i32 0, i32 8
  %12 = load i32, i32* %height_in_blocks, align 4, !tbaa !60
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_rows_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 69
  store i32 %12, i32* %MCU_rows_in_scan, align 8, !tbaa !71
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %14, i32 0, i32 13
  store i32 1, i32* %MCU_width, align 4, !tbaa !72
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %15, i32 0, i32 14
  store i32 1, i32* %MCU_height, align 4, !tbaa !73
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 15
  store i32 1, i32* %MCU_blocks, align 4, !tbaa !74
  %17 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %17, i32 0, i32 9
  %18 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !58
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %19, i32 0, i32 16
  store i32 %18, i32* %MCU_sample_width, align 4, !tbaa !75
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %20, i32 0, i32 17
  store i32 1, i32* %last_col_width, align 4, !tbaa !76
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks1 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 0, i32 8
  %22 = load i32, i32* %height_in_blocks1, align 4, !tbaa !60
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 3
  %24 = load i32, i32* %v_samp_factor, align 4, !tbaa !56
  %rem = urem i32 %22, %24
  store i32 %rem, i32* %tmp, align 4, !tbaa !27
  %25 = load i32, i32* %tmp, align 4, !tbaa !27
  %cmp2 = icmp eq i32 %25, 0
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor4 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 3
  %27 = load i32, i32* %v_samp_factor4, align 4, !tbaa !56
  store i32 %27, i32* %tmp, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %28 = load i32, i32* %tmp, align 4, !tbaa !27
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 0, i32 18
  store i32 %28, i32* %last_row_height, align 4, !tbaa !77
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 70
  store i32 1, i32* %blocks_in_MCU, align 4, !tbaa !78
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 71
  %arrayidx5 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 0
  store i32 0, i32* %arrayidx5, align 8, !tbaa !27
  br label %if.end70

if.else:                                          ; preds = %entry
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 66
  %33 = load i32, i32* %comps_in_scan6, align 8, !tbaa !67
  %cmp7 = icmp sle i32 %33, 0
  br i1 %cmp7, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 66
  %35 = load i32, i32* %comps_in_scan8, align 8, !tbaa !67
  %cmp9 = icmp sgt i32 %35, 4
  br i1 %cmp9, label %if.then10, label %if.end19

if.then10:                                        ; preds = %lor.lhs.false, %if.else
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 5
  store i32 26, i32* %msg_code, align 4, !tbaa !29
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 66
  %39 = load i32, i32* %comps_in_scan11, align 8, !tbaa !67
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !28
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx13 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %39, i32* %arrayidx13, align 4, !tbaa !48
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 0
  %43 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !28
  %msg_parm15 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %43, i32 0, i32 6
  %i16 = bitcast %union.anon* %msg_parm15 to [8 x i32]*
  %arrayidx17 = getelementptr inbounds [8 x i32], [8 x i32]* %i16, i32 0, i32 1
  store i32 4, i32* %arrayidx17, align 4, !tbaa !48
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 0
  %45 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !28
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %45, i32 0, i32 0
  %46 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !31
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %48 = bitcast %struct.jpeg_decompress_struct* %47 to %struct.jpeg_common_struct*
  call void %46(%struct.jpeg_common_struct* %48)
  br label %if.end19

if.end19:                                         ; preds = %if.then10, %lor.lhs.false
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 7
  %50 = load i32, i32* %image_width, align 4, !tbaa !47
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 61
  %52 = load i32, i32* %max_h_samp_factor, align 4, !tbaa !51
  %mul = mul nsw i32 %52, 8
  %call = call i32 @jdiv_round_up(i32 %50, i32 %mul)
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 68
  store i32 %call, i32* %MCUs_per_row20, align 4, !tbaa !70
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 8
  %55 = load i32, i32* %image_height, align 8, !tbaa !46
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 62
  %57 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !52
  %mul21 = mul nsw i32 %57, 8
  %call22 = call i32 @jdiv_round_up(i32 %55, i32 %mul21)
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_rows_in_scan23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 69
  store i32 %call22, i32* %MCU_rows_in_scan23, align 8, !tbaa !71
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU24 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 70
  store i32 0, i32* %blocks_in_MCU24, align 4, !tbaa !78
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end19
  %60 = load i32, i32* %ci, align 4, !tbaa !27
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 66
  %62 = load i32, i32* %comps_in_scan25, align 8, !tbaa !67
  %cmp26 = icmp slt i32 %60, %62
  br i1 %cmp26, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 67
  %64 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx28 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info27, i32 0, i32 %64
  %65 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx28, align 4, !tbaa !2
  store %struct.jpeg_component_info* %65, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 2
  %67 = load i32, i32* %h_samp_factor, align 4, !tbaa !54
  %68 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width29 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %68, i32 0, i32 13
  store i32 %67, i32* %MCU_width29, align 4, !tbaa !72
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor30 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 3
  %70 = load i32, i32* %v_samp_factor30, align 4, !tbaa !56
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height31 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 14
  store i32 %70, i32* %MCU_height31, align 4, !tbaa !73
  %72 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width32 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %72, i32 0, i32 13
  %73 = load i32, i32* %MCU_width32, align 4, !tbaa !72
  %74 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height33 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %74, i32 0, i32 14
  %75 = load i32, i32* %MCU_height33, align 4, !tbaa !73
  %mul34 = mul nsw i32 %73, %75
  %76 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks35 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %76, i32 0, i32 15
  store i32 %mul34, i32* %MCU_blocks35, align 4, !tbaa !74
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %77, i32 0, i32 13
  %78 = load i32, i32* %MCU_width36, align 4, !tbaa !72
  %79 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size37 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %79, i32 0, i32 9
  %80 = load i32, i32* %DCT_scaled_size37, align 4, !tbaa !58
  %mul38 = mul nsw i32 %78, %80
  %81 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %81, i32 0, i32 16
  store i32 %mul38, i32* %MCU_sample_width39, align 4, !tbaa !75
  %82 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %82, i32 0, i32 7
  %83 = load i32, i32* %width_in_blocks40, align 4, !tbaa !59
  %84 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width41 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %84, i32 0, i32 13
  %85 = load i32, i32* %MCU_width41, align 4, !tbaa !72
  %rem42 = urem i32 %83, %85
  store i32 %rem42, i32* %tmp, align 4, !tbaa !27
  %86 = load i32, i32* %tmp, align 4, !tbaa !27
  %cmp43 = icmp eq i32 %86, 0
  br i1 %cmp43, label %if.then44, label %if.end46

if.then44:                                        ; preds = %for.body
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width45 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %87, i32 0, i32 13
  %88 = load i32, i32* %MCU_width45, align 4, !tbaa !72
  store i32 %88, i32* %tmp, align 4, !tbaa !27
  br label %if.end46

if.end46:                                         ; preds = %if.then44, %for.body
  %89 = load i32, i32* %tmp, align 4, !tbaa !27
  %90 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width47 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %90, i32 0, i32 17
  store i32 %89, i32* %last_col_width47, align 4, !tbaa !76
  %91 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks48 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %91, i32 0, i32 8
  %92 = load i32, i32* %height_in_blocks48, align 4, !tbaa !60
  %93 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height49 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %93, i32 0, i32 14
  %94 = load i32, i32* %MCU_height49, align 4, !tbaa !73
  %rem50 = urem i32 %92, %94
  store i32 %rem50, i32* %tmp, align 4, !tbaa !27
  %95 = load i32, i32* %tmp, align 4, !tbaa !27
  %cmp51 = icmp eq i32 %95, 0
  br i1 %cmp51, label %if.then52, label %if.end54

if.then52:                                        ; preds = %if.end46
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 14
  %97 = load i32, i32* %MCU_height53, align 4, !tbaa !73
  store i32 %97, i32* %tmp, align 4, !tbaa !27
  br label %if.end54

if.end54:                                         ; preds = %if.then52, %if.end46
  %98 = load i32, i32* %tmp, align 4, !tbaa !27
  %99 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height55 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %99, i32 0, i32 18
  store i32 %98, i32* %last_row_height55, align 4, !tbaa !77
  %100 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks56 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %100, i32 0, i32 15
  %101 = load i32, i32* %MCU_blocks56, align 4, !tbaa !74
  store i32 %101, i32* %mcublks, align 4, !tbaa !27
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU57 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 70
  %103 = load i32, i32* %blocks_in_MCU57, align 4, !tbaa !78
  %104 = load i32, i32* %mcublks, align 4, !tbaa !27
  %add = add nsw i32 %103, %104
  %cmp58 = icmp sgt i32 %add, 10
  br i1 %cmp58, label %if.then59, label %if.end64

if.then59:                                        ; preds = %if.end54
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err60 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 0
  %106 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err60, align 8, !tbaa !28
  %msg_code61 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %106, i32 0, i32 5
  store i32 13, i32* %msg_code61, align 4, !tbaa !29
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err62 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %107, i32 0, i32 0
  %108 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err62, align 8, !tbaa !28
  %error_exit63 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %108, i32 0, i32 0
  %109 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit63, align 4, !tbaa !31
  %110 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %111 = bitcast %struct.jpeg_decompress_struct* %110 to %struct.jpeg_common_struct*
  call void %109(%struct.jpeg_common_struct* %111)
  br label %if.end64

if.end64:                                         ; preds = %if.then59, %if.end54
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end64
  %112 = load i32, i32* %mcublks, align 4, !tbaa !27
  %dec = add nsw i32 %112, -1
  store i32 %dec, i32* %mcublks, align 4, !tbaa !27
  %cmp65 = icmp sgt i32 %112, 0
  br i1 %cmp65, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %113 = load i32, i32* %ci, align 4, !tbaa !27
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership66 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 71
  %115 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %115, i32 0, i32 70
  %116 = load i32, i32* %blocks_in_MCU67, align 4, !tbaa !78
  %inc = add nsw i32 %116, 1
  store i32 %inc, i32* %blocks_in_MCU67, align 4, !tbaa !78
  %arrayidx68 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership66, i32 0, i32 %116
  store i32 %113, i32* %arrayidx68, align 4, !tbaa !27
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %117 = load i32, i32* %ci, align 4, !tbaa !27
  %inc69 = add nsw i32 %117, 1
  store i32 %inc69, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end70

if.end70:                                         ; preds = %for.end, %if.end
  %118 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast i32* %mcublks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  ret void
}

; Function Attrs: nounwind
define internal void @latch_quant_tables(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci = alloca i32, align 4
  %qtblno = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %qtbl = alloca %struct.JQUANT_TBL*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %qtblno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %ci, align 4, !tbaa !27
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 66
  %6 = load i32, i32* %comps_in_scan, align 8, !tbaa !67
  %cmp = icmp slt i32 %4, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 67
  %8 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %8
  %9 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %9, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %10 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %10, i32 0, i32 19
  %11 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !65
  %cmp1 = icmp ne %struct.JQUANT_TBL* %11, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 4
  %13 = load i32, i32* %quant_tbl_no, align 4, !tbaa !79
  store i32 %13, i32* %qtblno, align 4, !tbaa !27
  %14 = load i32, i32* %qtblno, align 4, !tbaa !27
  %cmp2 = icmp slt i32 %14, 0
  br i1 %cmp2, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %15 = load i32, i32* %qtblno, align 4, !tbaa !27
  %cmp3 = icmp sge i32 %15, 4
  br i1 %cmp3, label %if.then7, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 40
  %17 = load i32, i32* %qtblno, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %17
  %18 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx5, align 4, !tbaa !2
  %cmp6 = icmp eq %struct.JQUANT_TBL* %18, null
  br i1 %cmp6, label %if.then7, label %if.end11

if.then7:                                         ; preds = %lor.lhs.false4, %lor.lhs.false, %if.end
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 52, i32* %msg_code, align 4, !tbaa !29
  %21 = load i32, i32* %qtblno, align 4, !tbaa !27
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !28
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx9 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %21, i32* %arrayidx9, align 4, !tbaa !48
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 0
  %25 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !28
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %25, i32 0, i32 0
  %26 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !31
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = bitcast %struct.jpeg_decompress_struct* %27 to %struct.jpeg_common_struct*
  call void %26(%struct.jpeg_common_struct* %28)
  br label %if.end11

if.end11:                                         ; preds = %if.then7, %lor.lhs.false4
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 1
  %30 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %30, i32 0, i32 0
  %31 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = bitcast %struct.jpeg_decompress_struct* %32 to %struct.jpeg_common_struct*
  %call = call i8* %31(%struct.jpeg_common_struct* %33, i32 1, i32 132)
  %34 = bitcast i8* %call to %struct.JQUANT_TBL*
  store %struct.JQUANT_TBL* %34, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %35 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %36 = bitcast %struct.JQUANT_TBL* %35 to i8*
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 40
  %38 = load i32, i32* %qtblno, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs12, i32 0, i32 %38
  %39 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx13, align 4, !tbaa !2
  %40 = bitcast %struct.JQUANT_TBL* %39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %36, i8* align 1 %40, i32 132, i1 false)
  %41 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table14 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %42, i32 0, i32 19
  store %struct.JQUANT_TBL* %41, %struct.JQUANT_TBL** %quant_table14, align 4, !tbaa !65
  br label %for.inc

for.inc:                                          ; preds = %if.end11, %if.then
  %43 = load i32, i32* %ci, align 4, !tbaa !27
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %qtblno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 436}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !8, i64 24}
!17 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20}
!18 = !{!16, !3, i64 4}
!19 = !{!16, !3, i64 8}
!20 = !{!16, !3, i64 12}
!21 = !{!16, !8, i64 16}
!22 = !{!16, !8, i64 20}
!23 = !{!16, !8, i64 24}
!24 = !{!7, !3, i64 440}
!25 = !{!26, !3, i64 4}
!26 = !{!"jpeg_marker_reader", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24}
!27 = !{!8, !8, i64 0}
!28 = !{!7, !3, i64 0}
!29 = !{!30, !8, i64 20}
!30 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!31 = !{!30, !3, i64 0}
!32 = !{!26, !8, i64 16}
!33 = !{!7, !8, i64 152}
!34 = !{!7, !8, i64 144}
!35 = !{!30, !3, i64 16}
!36 = !{!26, !3, i64 0}
!37 = !{!7, !3, i64 160}
!38 = !{!7, !3, i64 444}
!39 = !{!40, !3, i64 0}
!40 = !{!"jpeg_entropy_decoder", !3, i64 0, !3, i64 4, !8, i64 8}
!41 = !{!7, !3, i64 428}
!42 = !{!43, !3, i64 0}
!43 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!44 = !{!43, !3, i64 4}
!45 = !{!17, !3, i64 0}
!46 = !{!7, !8, i64 32}
!47 = !{!7, !8, i64 28}
!48 = !{!4, !4, i64 0}
!49 = !{!7, !8, i64 212}
!50 = !{!7, !8, i64 36}
!51 = !{!7, !8, i64 308}
!52 = !{!7, !8, i64 312}
!53 = !{!7, !3, i64 216}
!54 = !{!55, !8, i64 8}
!55 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!56 = !{!55, !8, i64 12}
!57 = !{!7, !8, i64 316}
!58 = !{!55, !8, i64 36}
!59 = !{!55, !8, i64 28}
!60 = !{!55, !8, i64 32}
!61 = !{!7, !3, i64 420}
!62 = !{!55, !8, i64 40}
!63 = !{!55, !8, i64 44}
!64 = !{!55, !8, i64 48}
!65 = !{!55, !3, i64 76}
!66 = !{!7, !8, i64 320}
!67 = !{!7, !8, i64 328}
!68 = !{!7, !8, i64 220}
!69 = !{!17, !8, i64 16}
!70 = !{!7, !8, i64 348}
!71 = !{!7, !8, i64 352}
!72 = !{!55, !8, i64 52}
!73 = !{!55, !8, i64 56}
!74 = !{!55, !8, i64 60}
!75 = !{!55, !8, i64 64}
!76 = !{!55, !8, i64 68}
!77 = !{!55, !8, i64 72}
!78 = !{!7, !8, i64 356}
!79 = !{!55, !8, i64 16}
