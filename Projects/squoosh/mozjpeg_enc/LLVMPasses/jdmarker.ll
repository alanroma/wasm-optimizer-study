; ModuleID = 'jdmarker.c'
source_filename = "jdmarker.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, {}*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.my_marker_reader = type { %struct.jpeg_marker_reader, i32 (%struct.jpeg_decompress_struct*)*, [16 x i32 (%struct.jpeg_decompress_struct*)*], i32, [16 x i32], %struct.jpeg_marker_struct*, i32 }

@jpeg_natural_order = external constant [0 x i32], align 4

; Function Attrs: nounwind
define hidden i32 @jpeg_resync_to_restart(%struct.jpeg_decompress_struct* %cinfo, i32 %desired) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %desired.addr = alloca i32, align 4
  %marker = alloca i32, align 4
  %action = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %desired, i32* %desired.addr, align 4, !tbaa !6
  %0 = bitcast i32* %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 76
  %2 = load i32, i32* %unread_marker, align 8, !tbaa !8
  store i32 %2, i32* %marker, align 4, !tbaa !6
  %3 = bitcast i32* %action to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 1, i32* %action, align 4, !tbaa !6
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 121, i32* %msg_code, align 4, !tbaa !13
  %6 = load i32, i32* %marker, align 4, !tbaa !6
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %6, i32* %arrayidx, align 4, !tbaa !16
  %9 = load i32, i32* %desired.addr, align 4, !tbaa !6
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_parm3 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 6
  %i4 = bitcast %union.anon* %msg_parm3 to [8 x i32]*
  %arrayidx5 = getelementptr inbounds [8 x i32], [8 x i32]* %i4, i32 0, i32 1
  store i32 %9, i32* %arrayidx5, align 4, !tbaa !16
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 1
  %14 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_decompress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16, i32 -1)
  br label %for.cond

for.cond:                                         ; preds = %sw.epilog, %entry
  %17 = load i32, i32* %marker, align 4, !tbaa !6
  %cmp = icmp slt i32 %17, 192
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %for.cond
  store i32 2, i32* %action, align 4, !tbaa !6
  br label %if.end32

if.else:                                          ; preds = %for.cond
  %18 = load i32, i32* %marker, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %18, 208
  br i1 %cmp7, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %19 = load i32, i32* %marker, align 4, !tbaa !6
  %cmp8 = icmp sgt i32 %19, 215
  br i1 %cmp8, label %if.then9, label %if.else10

if.then9:                                         ; preds = %lor.lhs.false, %if.else
  store i32 3, i32* %action, align 4, !tbaa !6
  br label %if.end31

if.else10:                                        ; preds = %lor.lhs.false
  %20 = load i32, i32* %marker, align 4, !tbaa !6
  %21 = load i32, i32* %desired.addr, align 4, !tbaa !6
  %add = add nsw i32 %21, 1
  %and = and i32 %add, 7
  %add11 = add nsw i32 208, %and
  %cmp12 = icmp eq i32 %20, %add11
  br i1 %cmp12, label %if.then18, label %lor.lhs.false13

lor.lhs.false13:                                  ; preds = %if.else10
  %22 = load i32, i32* %marker, align 4, !tbaa !6
  %23 = load i32, i32* %desired.addr, align 4, !tbaa !6
  %add14 = add nsw i32 %23, 2
  %and15 = and i32 %add14, 7
  %add16 = add nsw i32 208, %and15
  %cmp17 = icmp eq i32 %22, %add16
  br i1 %cmp17, label %if.then18, label %if.else19

if.then18:                                        ; preds = %lor.lhs.false13, %if.else10
  store i32 3, i32* %action, align 4, !tbaa !6
  br label %if.end30

if.else19:                                        ; preds = %lor.lhs.false13
  %24 = load i32, i32* %marker, align 4, !tbaa !6
  %25 = load i32, i32* %desired.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %25, 1
  %and20 = and i32 %sub, 7
  %add21 = add nsw i32 208, %and20
  %cmp22 = icmp eq i32 %24, %add21
  br i1 %cmp22, label %if.then28, label %lor.lhs.false23

lor.lhs.false23:                                  ; preds = %if.else19
  %26 = load i32, i32* %marker, align 4, !tbaa !6
  %27 = load i32, i32* %desired.addr, align 4, !tbaa !6
  %sub24 = sub nsw i32 %27, 2
  %and25 = and i32 %sub24, 7
  %add26 = add nsw i32 208, %and25
  %cmp27 = icmp eq i32 %26, %add26
  br i1 %cmp27, label %if.then28, label %if.else29

if.then28:                                        ; preds = %lor.lhs.false23, %if.else19
  store i32 2, i32* %action, align 4, !tbaa !6
  br label %if.end

if.else29:                                        ; preds = %lor.lhs.false23
  store i32 1, i32* %action, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else29, %if.then28
  br label %if.end30

if.end30:                                         ; preds = %if.end, %if.then18
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then9
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err33, align 8, !tbaa !12
  %msg_code34 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 5
  store i32 97, i32* %msg_code34, align 4, !tbaa !13
  %30 = load i32, i32* %marker, align 4, !tbaa !6
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err35, align 8, !tbaa !12
  %msg_parm36 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 6
  %i37 = bitcast %union.anon* %msg_parm36 to [8 x i32]*
  %arrayidx38 = getelementptr inbounds [8 x i32], [8 x i32]* %i37, i32 0, i32 0
  store i32 %30, i32* %arrayidx38, align 4, !tbaa !16
  %33 = load i32, i32* %action, align 4, !tbaa !6
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err39, align 8, !tbaa !12
  %msg_parm40 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 6
  %i41 = bitcast %union.anon* %msg_parm40 to [8 x i32]*
  %arrayidx42 = getelementptr inbounds [8 x i32], [8 x i32]* %i41, i32 0, i32 1
  store i32 %33, i32* %arrayidx42, align 4, !tbaa !16
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 8, !tbaa !12
  %emit_message44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 1
  %38 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message44, align 4, !tbaa !17
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_decompress_struct* %39 to %struct.jpeg_common_struct*
  call void %38(%struct.jpeg_common_struct* %40, i32 4)
  %41 = load i32, i32* %action, align 4, !tbaa !6
  switch i32 %41, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb46
    i32 3, label %sw.bb50
  ]

sw.bb:                                            ; preds = %if.end32
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 76
  store i32 0, i32* %unread_marker45, align 8, !tbaa !8
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb46:                                          ; preds = %if.end32
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @next_marker(%struct.jpeg_decompress_struct* %43)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end48, label %if.then47

if.then47:                                        ; preds = %sw.bb46
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end48:                                         ; preds = %sw.bb46
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 76
  %45 = load i32, i32* %unread_marker49, align 8, !tbaa !8
  store i32 %45, i32* %marker, align 4, !tbaa !6
  br label %sw.epilog

sw.bb50:                                          ; preds = %if.end32
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %if.end32, %if.end48
  br label %for.cond

cleanup:                                          ; preds = %sw.bb50, %if.then47, %sw.bb
  %46 = bitcast i32* %action to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i32* %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @next_marker(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %c = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 6
  %3 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %3, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %4 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %5, i32 0, i32 0
  %6 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %6, i8** %next_input_byte, align 4, !tbaa !2
  %7 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 1
  %9 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %9, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %if.end53, %entry
  br label %do.body

do.body:                                          ; preds = %for.cond
  %10 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %10, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 3
  %12 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %12(%struct.jpeg_decompress_struct* %13)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 0
  %15 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %15, i8** %next_input_byte, align 4, !tbaa !2
  %16 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %16, i32 0, i32 1
  %17 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %17, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %18 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %18, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %19 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %19, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %20 = load i8, i8* %19, align 1, !tbaa !16
  %conv = zext i8 %20 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %if.end6
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %while.cond

while.cond:                                       ; preds = %do.end27, %do.end
  %21 = load i32, i32* %c, align 4, !tbaa !6
  %cmp7 = icmp ne i32 %21, 255
  br i1 %cmp7, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 82
  %23 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %discarded_bytes = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %23, i32 0, i32 6
  %24 = load i32, i32* %discarded_bytes, align 4, !tbaa !25
  %inc = add i32 %24, 1
  store i32 %inc, i32* %discarded_bytes, align 4, !tbaa !25
  %25 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %26 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte9 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %26, i32 0, i32 0
  store i8* %25, i8** %next_input_byte9, align 4, !tbaa !19
  %27 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 1
  store i32 %27, i32* %bytes_in_buffer10, align 4, !tbaa !21
  br label %do.body11

do.body11:                                        ; preds = %while.body
  %29 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp12 = icmp eq i32 %29, 0
  br i1 %cmp12, label %if.then14, label %if.end22

if.then14:                                        ; preds = %do.body11
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %30, i32 0, i32 3
  %31 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer15, align 4, !tbaa !23
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call16 = call i32 %31(%struct.jpeg_decompress_struct* %32)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %if.then14
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then14
  %33 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte20 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %33, i32 0, i32 0
  %34 = load i8*, i8** %next_input_byte20, align 4, !tbaa !19
  store i8* %34, i8** %next_input_byte, align 4, !tbaa !2
  %35 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer21 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %35, i32 0, i32 1
  %36 = load i32, i32* %bytes_in_buffer21, align 4, !tbaa !21
  store i32 %36, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end22

if.end22:                                         ; preds = %if.end19, %do.body11
  %37 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec23 = add i32 %37, -1
  store i32 %dec23, i32* %bytes_in_buffer, align 4, !tbaa !22
  %38 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %38, i32 1
  store i8* %incdec.ptr24, i8** %next_input_byte, align 4, !tbaa !2
  %39 = load i8, i8* %38, align 1, !tbaa !16
  %conv25 = zext i8 %39 to i32
  store i32 %conv25, i32* %c, align 4, !tbaa !6
  br label %do.cond26

do.cond26:                                        ; preds = %if.end22
  br label %do.end27

do.end27:                                         ; preds = %do.cond26
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %do.body28

do.body28:                                        ; preds = %do.cond46, %while.end
  br label %do.body29

do.body29:                                        ; preds = %do.body28
  %40 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp30 = icmp eq i32 %40, 0
  br i1 %cmp30, label %if.then32, label %if.end40

if.then32:                                        ; preds = %do.body29
  %41 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer33 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %41, i32 0, i32 3
  %42 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer33, align 4, !tbaa !23
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call34 = call i32 %42(%struct.jpeg_decompress_struct* %43)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.end37, label %if.then36

if.then36:                                        ; preds = %if.then32
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end37:                                         ; preds = %if.then32
  %44 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte38 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %44, i32 0, i32 0
  %45 = load i8*, i8** %next_input_byte38, align 4, !tbaa !19
  store i8* %45, i8** %next_input_byte, align 4, !tbaa !2
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer39 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 1
  %47 = load i32, i32* %bytes_in_buffer39, align 4, !tbaa !21
  store i32 %47, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end40

if.end40:                                         ; preds = %if.end37, %do.body29
  %48 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec41 = add i32 %48, -1
  store i32 %dec41, i32* %bytes_in_buffer, align 4, !tbaa !22
  %49 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr42 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr42, i8** %next_input_byte, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !16
  %conv43 = zext i8 %50 to i32
  store i32 %conv43, i32* %c, align 4, !tbaa !6
  br label %do.cond44

do.cond44:                                        ; preds = %if.end40
  br label %do.end45

do.end45:                                         ; preds = %do.cond44
  br label %do.cond46

do.cond46:                                        ; preds = %do.end45
  %51 = load i32, i32* %c, align 4, !tbaa !6
  %cmp47 = icmp eq i32 %51, 255
  br i1 %cmp47, label %do.body28, label %do.end49

do.end49:                                         ; preds = %do.cond46
  %52 = load i32, i32* %c, align 4, !tbaa !6
  %cmp50 = icmp ne i32 %52, 0
  br i1 %cmp50, label %if.then52, label %if.end53

if.then52:                                        ; preds = %do.end49
  br label %for.end

if.end53:                                         ; preds = %do.end49
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 82
  %54 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker54, align 8, !tbaa !24
  %discarded_bytes55 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %54, i32 0, i32 6
  %55 = load i32, i32* %discarded_bytes55, align 4, !tbaa !25
  %add = add i32 %55, 2
  store i32 %add, i32* %discarded_bytes55, align 4, !tbaa !25
  %56 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %57 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte56 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %57, i32 0, i32 0
  store i8* %56, i8** %next_input_byte56, align 4, !tbaa !19
  %58 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %59 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer57 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %59, i32 0, i32 1
  store i32 %58, i32* %bytes_in_buffer57, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %if.then52
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 82
  %61 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker58, align 8, !tbaa !24
  %discarded_bytes59 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %61, i32 0, i32 6
  %62 = load i32, i32* %discarded_bytes59, align 4, !tbaa !25
  %cmp60 = icmp ne i32 %62, 0
  br i1 %cmp60, label %if.then62, label %if.end73

if.then62:                                        ; preds = %for.end
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 5
  store i32 116, i32* %msg_code, align 4, !tbaa !13
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker63 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 82
  %66 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker63, align 8, !tbaa !24
  %discarded_bytes64 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %66, i32 0, i32 6
  %67 = load i32, i32* %discarded_bytes64, align 4, !tbaa !25
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 0
  %69 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err65, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %69, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %67, i32* %arrayidx, align 4, !tbaa !16
  %70 = load i32, i32* %c, align 4, !tbaa !6
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err66 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 0
  %72 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err66, align 8, !tbaa !12
  %msg_parm67 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %72, i32 0, i32 6
  %i68 = bitcast %union.anon* %msg_parm67 to [8 x i32]*
  %arrayidx69 = getelementptr inbounds [8 x i32], [8 x i32]* %i68, i32 0, i32 1
  store i32 %70, i32* %arrayidx69, align 4, !tbaa !16
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err70 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 0
  %74 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err70, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %74, i32 0, i32 1
  %75 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %76 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %77 = bitcast %struct.jpeg_decompress_struct* %76 to %struct.jpeg_common_struct*
  call void %75(%struct.jpeg_common_struct* %77, i32 -1)
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 82
  %79 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker71, align 8, !tbaa !24
  %discarded_bytes72 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %79, i32 0, i32 6
  store i32 0, i32* %discarded_bytes72, align 4, !tbaa !25
  br label %if.end73

if.end73:                                         ; preds = %if.then62, %for.end
  %80 = load i32, i32* %c, align 4, !tbaa !6
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 76
  store i32 %80, i32* %unread_marker, align 8, !tbaa !8
  %82 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %83 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte74 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %83, i32 0, i32 0
  store i8* %82, i8** %next_input_byte74, align 4, !tbaa !19
  %84 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %85 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer75 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %85, i32 0, i32 1
  store i32 %84, i32* %bytes_in_buffer75, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end73, %if.then36, %if.then18, %if.then3
  %86 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = load i32, i32* %retval, align 4
  ret i32 %90
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jinit_marker_reader(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %marker = alloca %struct.my_marker_reader*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 1
  %3 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !27
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %3, i32 0, i32 0
  %4 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !28
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  %call = call i8* %4(%struct.jpeg_common_struct* %6, i32 0, i32 172)
  %7 = bitcast i8* %call to %struct.my_marker_reader*
  store %struct.my_marker_reader* %7, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %8 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %9 = bitcast %struct.my_marker_reader* %8 to %struct.jpeg_marker_reader*
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 82
  store %struct.jpeg_marker_reader* %9, %struct.jpeg_marker_reader** %marker1, align 8, !tbaa !24
  %11 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %11, i32 0, i32 0
  %reset_marker_reader = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*)* @reset_marker_reader, void (%struct.jpeg_decompress_struct*)** %reset_marker_reader, align 4, !tbaa !30
  %12 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %12, i32 0, i32 0
  %read_markers = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub2, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* @read_markers, i32 (%struct.jpeg_decompress_struct*)** %read_markers, align 4, !tbaa !32
  %13 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %13, i32 0, i32 0
  %read_restart_marker = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub3, i32 0, i32 2
  store i32 (%struct.jpeg_decompress_struct*)* @read_restart_marker, i32 (%struct.jpeg_decompress_struct*)** %read_restart_marker, align 4, !tbaa !33
  %14 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %14, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* @skip_variable, i32 (%struct.jpeg_decompress_struct*)** %process_COM, align 4, !tbaa !34
  %15 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %15, i32 0, i32 3
  store i32 0, i32* %length_limit_COM, align 4, !tbaa !35
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %17, i32 0, i32 2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn, i32 0, i32 %18
  store i32 (%struct.jpeg_decompress_struct*)* @skip_variable, i32 (%struct.jpeg_decompress_struct*)** %arrayidx, align 4, !tbaa !2
  %19 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %19, i32 0, i32 4
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [16 x i32], [16 x i32]* %length_limit_APPn, i32 0, i32 %20
  store i32 0, i32* %arrayidx4, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_APPn5 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %22, i32 0, i32 2
  %arrayidx6 = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn5, i32 0, i32 0
  store i32 (%struct.jpeg_decompress_struct*)* @get_interesting_appn, i32 (%struct.jpeg_decompress_struct*)** %arrayidx6, align 4, !tbaa !2
  %23 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_APPn7 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %23, i32 0, i32 2
  %arrayidx8 = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn7, i32 0, i32 14
  store i32 (%struct.jpeg_decompress_struct*)* @get_interesting_appn, i32 (%struct.jpeg_decompress_struct*)** %arrayidx8, align 4, !tbaa !2
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @reset_marker_reader(%struct.jpeg_decompress_struct* %24)
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

; Function Attrs: nounwind
define internal void @reset_marker_reader(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %marker = alloca %struct.my_marker_reader*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 82
  %2 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker1, align 8, !tbaa !24
  %3 = bitcast %struct.jpeg_marker_reader* %2 to %struct.my_marker_reader*
  store %struct.my_marker_reader* %3, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 44
  store %struct.jpeg_component_info* null, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !36
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 35
  store i32 0, i32* %input_scan_number, align 8, !tbaa !37
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 76
  store i32 0, i32* %unread_marker, align 8, !tbaa !8
  %7 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %7, i32 0, i32 0
  %saw_SOI = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub, i32 0, i32 3
  store i32 0, i32* %saw_SOI, align 4, !tbaa !38
  %8 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %8, i32 0, i32 0
  %saw_SOF = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub2, i32 0, i32 4
  store i32 0, i32* %saw_SOF, align 4, !tbaa !39
  %9 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %9, i32 0, i32 0
  %discarded_bytes = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %pub3, i32 0, i32 6
  store i32 0, i32* %discarded_bytes, align 4, !tbaa !40
  %10 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %cur_marker = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %10, i32 0, i32 5
  store %struct.jpeg_marker_struct* null, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !41
  %11 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

; Function Attrs: nounwind
define internal i32 @read_markers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %sw.epilog, %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 76
  %1 = load i32, i32* %unread_marker, align 8, !tbaa !8
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %for.cond
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 82
  %3 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %saw_SOI = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %3, i32 0, i32 3
  %4 = load i32, i32* %saw_SOI, align 4, !tbaa !42
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.else, label %if.then1

if.then1:                                         ; preds = %if.then
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @first_marker(%struct.jpeg_decompress_struct* %5)
  %tobool2 = icmp ne i32 %call, 0
  br i1 %tobool2, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then1
  br label %if.end8

if.else:                                          ; preds = %if.then
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call4 = call i32 @next_marker(%struct.jpeg_decompress_struct* %6)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.else
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %if.end
  br label %if.end9

if.end9:                                          ; preds = %if.end8, %for.cond
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 76
  %8 = load i32, i32* %unread_marker10, align 8, !tbaa !8
  switch i32 %8, label %sw.default [
    i32 216, label %sw.bb
    i32 192, label %sw.bb15
    i32 193, label %sw.bb15
    i32 194, label %sw.bb20
    i32 201, label %sw.bb25
    i32 202, label %sw.bb30
    i32 195, label %sw.bb35
    i32 197, label %sw.bb35
    i32 198, label %sw.bb35
    i32 199, label %sw.bb35
    i32 200, label %sw.bb35
    i32 203, label %sw.bb35
    i32 205, label %sw.bb35
    i32 206, label %sw.bb35
    i32 207, label %sw.bb35
    i32 218, label %sw.bb39
    i32 217, label %sw.bb45
    i32 204, label %sw.bb50
    i32 196, label %sw.bb55
    i32 219, label %sw.bb60
    i32 221, label %sw.bb65
    i32 224, label %sw.bb70
    i32 225, label %sw.bb70
    i32 226, label %sw.bb70
    i32 227, label %sw.bb70
    i32 228, label %sw.bb70
    i32 229, label %sw.bb70
    i32 230, label %sw.bb70
    i32 231, label %sw.bb70
    i32 232, label %sw.bb70
    i32 233, label %sw.bb70
    i32 234, label %sw.bb70
    i32 235, label %sw.bb70
    i32 236, label %sw.bb70
    i32 237, label %sw.bb70
    i32 238, label %sw.bb70
    i32 239, label %sw.bb70
    i32 254, label %sw.bb78
    i32 208, label %sw.bb84
    i32 209, label %sw.bb84
    i32 210, label %sw.bb84
    i32 211, label %sw.bb84
    i32 212, label %sw.bb84
    i32 213, label %sw.bb84
    i32 214, label %sw.bb84
    i32 215, label %sw.bb84
    i32 1, label %sw.bb84
    i32 220, label %sw.bb94
  ]

sw.bb:                                            ; preds = %if.end9
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 @get_soi(%struct.jpeg_decompress_struct* %9)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %sw.bb
  store i32 0, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %sw.bb
  br label %sw.epilog

sw.bb15:                                          ; preds = %if.end9, %if.end9
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call16 = call i32 @get_sof(%struct.jpeg_decompress_struct* %10, i32 0, i32 0)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %sw.bb15
  store i32 0, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %sw.bb15
  br label %sw.epilog

sw.bb20:                                          ; preds = %if.end9
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call21 = call i32 @get_sof(%struct.jpeg_decompress_struct* %11, i32 1, i32 0)
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %sw.bb20
  store i32 0, i32* %retval, align 4
  br label %return

if.end24:                                         ; preds = %sw.bb20
  br label %sw.epilog

sw.bb25:                                          ; preds = %if.end9
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call26 = call i32 @get_sof(%struct.jpeg_decompress_struct* %12, i32 0, i32 1)
  %tobool27 = icmp ne i32 %call26, 0
  br i1 %tobool27, label %if.end29, label %if.then28

if.then28:                                        ; preds = %sw.bb25
  store i32 0, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %sw.bb25
  br label %sw.epilog

sw.bb30:                                          ; preds = %if.end9
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call31 = call i32 @get_sof(%struct.jpeg_decompress_struct* %13, i32 1, i32 1)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %sw.bb30
  store i32 0, i32* %retval, align 4
  br label %return

if.end34:                                         ; preds = %sw.bb30
  br label %sw.epilog

sw.bb35:                                          ; preds = %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 5
  store i32 60, i32* %msg_code, align 4, !tbaa !13
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker36 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 76
  %17 = load i32, i32* %unread_marker36, align 8, !tbaa !8
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err37, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %17, i32* %arrayidx, align 4, !tbaa !16
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err38, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 0
  %22 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24)
  br label %sw.epilog

sw.bb39:                                          ; preds = %if.end9
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call40 = call i32 @get_sos(%struct.jpeg_decompress_struct* %25)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %sw.bb39
  store i32 0, i32* %retval, align 4
  br label %return

if.end43:                                         ; preds = %sw.bb39
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker44 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 76
  store i32 0, i32* %unread_marker44, align 8, !tbaa !8
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb45:                                          ; preds = %if.end9
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !12
  %msg_code47 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 5
  store i32 85, i32* %msg_code47, align 4, !tbaa !13
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err48, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %30, i32 0, i32 1
  %31 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = bitcast %struct.jpeg_decompress_struct* %32 to %struct.jpeg_common_struct*
  call void %31(%struct.jpeg_common_struct* %33, i32 1)
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 76
  store i32 0, i32* %unread_marker49, align 8, !tbaa !8
  store i32 2, i32* %retval, align 4
  br label %return

sw.bb50:                                          ; preds = %if.end9
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call51 = call i32 @skip_variable(%struct.jpeg_decompress_struct* %35)
  %tobool52 = icmp ne i32 %call51, 0
  br i1 %tobool52, label %if.end54, label %if.then53

if.then53:                                        ; preds = %sw.bb50
  store i32 0, i32* %retval, align 4
  br label %return

if.end54:                                         ; preds = %sw.bb50
  br label %sw.epilog

sw.bb55:                                          ; preds = %if.end9
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call56 = call i32 @get_dht(%struct.jpeg_decompress_struct* %36)
  %tobool57 = icmp ne i32 %call56, 0
  br i1 %tobool57, label %if.end59, label %if.then58

if.then58:                                        ; preds = %sw.bb55
  store i32 0, i32* %retval, align 4
  br label %return

if.end59:                                         ; preds = %sw.bb55
  br label %sw.epilog

sw.bb60:                                          ; preds = %if.end9
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call61 = call i32 @get_dqt(%struct.jpeg_decompress_struct* %37)
  %tobool62 = icmp ne i32 %call61, 0
  br i1 %tobool62, label %if.end64, label %if.then63

if.then63:                                        ; preds = %sw.bb60
  store i32 0, i32* %retval, align 4
  br label %return

if.end64:                                         ; preds = %sw.bb60
  br label %sw.epilog

sw.bb65:                                          ; preds = %if.end9
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call66 = call i32 @get_dri(%struct.jpeg_decompress_struct* %38)
  %tobool67 = icmp ne i32 %call66, 0
  br i1 %tobool67, label %if.end69, label %if.then68

if.then68:                                        ; preds = %sw.bb65
  store i32 0, i32* %retval, align 4
  br label %return

if.end69:                                         ; preds = %sw.bb65
  br label %sw.epilog

sw.bb70:                                          ; preds = %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 82
  %40 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker71, align 8, !tbaa !24
  %41 = bitcast %struct.jpeg_marker_reader* %40 to %struct.my_marker_reader*
  %process_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %41, i32 0, i32 2
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker72 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 76
  %43 = load i32, i32* %unread_marker72, align 8, !tbaa !8
  %sub = sub nsw i32 %43, 224
  %arrayidx73 = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn, i32 0, i32 %sub
  %44 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %arrayidx73, align 4, !tbaa !2
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call74 = call i32 %44(%struct.jpeg_decompress_struct* %45)
  %tobool75 = icmp ne i32 %call74, 0
  br i1 %tobool75, label %if.end77, label %if.then76

if.then76:                                        ; preds = %sw.bb70
  store i32 0, i32* %retval, align 4
  br label %return

if.end77:                                         ; preds = %sw.bb70
  br label %sw.epilog

sw.bb78:                                          ; preds = %if.end9
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker79 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 82
  %47 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker79, align 8, !tbaa !24
  %48 = bitcast %struct.jpeg_marker_reader* %47 to %struct.my_marker_reader*
  %process_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %48, i32 0, i32 1
  %49 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %process_COM, align 4, !tbaa !34
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call80 = call i32 %49(%struct.jpeg_decompress_struct* %50)
  %tobool81 = icmp ne i32 %call80, 0
  br i1 %tobool81, label %if.end83, label %if.then82

if.then82:                                        ; preds = %sw.bb78
  store i32 0, i32* %retval, align 4
  br label %return

if.end83:                                         ; preds = %sw.bb78
  br label %sw.epilog

sw.bb84:                                          ; preds = %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9, %if.end9
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 0
  %52 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err85, align 8, !tbaa !12
  %msg_code86 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %52, i32 0, i32 5
  store i32 92, i32* %msg_code86, align 4, !tbaa !13
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 76
  %54 = load i32, i32* %unread_marker87, align 8, !tbaa !8
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err88 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err88, align 8, !tbaa !12
  %msg_parm89 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 6
  %i90 = bitcast %union.anon* %msg_parm89 to [8 x i32]*
  %arrayidx91 = getelementptr inbounds [8 x i32], [8 x i32]* %i90, i32 0, i32 0
  store i32 %54, i32* %arrayidx91, align 4, !tbaa !16
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err92 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 0
  %58 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err92, align 8, !tbaa !12
  %emit_message93 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %58, i32 0, i32 1
  %59 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message93, align 4, !tbaa !17
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %61 = bitcast %struct.jpeg_decompress_struct* %60 to %struct.jpeg_common_struct*
  call void %59(%struct.jpeg_common_struct* %61, i32 1)
  br label %sw.epilog

sw.bb94:                                          ; preds = %if.end9
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call95 = call i32 @skip_variable(%struct.jpeg_decompress_struct* %62)
  %tobool96 = icmp ne i32 %call95, 0
  br i1 %tobool96, label %if.end98, label %if.then97

if.then97:                                        ; preds = %sw.bb94
  store i32 0, i32* %retval, align 4
  br label %return

if.end98:                                         ; preds = %sw.bb94
  br label %sw.epilog

sw.default:                                       ; preds = %if.end9
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err99 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err99, align 8, !tbaa !12
  %msg_code100 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 5
  store i32 68, i32* %msg_code100, align 4, !tbaa !13
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 76
  %66 = load i32, i32* %unread_marker101, align 8, !tbaa !8
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err102 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 0
  %68 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err102, align 8, !tbaa !12
  %msg_parm103 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %68, i32 0, i32 6
  %i104 = bitcast %union.anon* %msg_parm103 to [8 x i32]*
  %arrayidx105 = getelementptr inbounds [8 x i32], [8 x i32]* %i104, i32 0, i32 0
  store i32 %66, i32* %arrayidx105, align 4, !tbaa !16
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err106 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err106, align 8, !tbaa !12
  %error_exit107 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 0
  %71 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit107, align 4, !tbaa !43
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %73 = bitcast %struct.jpeg_decompress_struct* %72 to %struct.jpeg_common_struct*
  call void %71(%struct.jpeg_common_struct* %73)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end98, %sw.bb84, %if.end83, %if.end77, %if.end69, %if.end64, %if.end59, %if.end54, %sw.bb35, %if.end34, %if.end29, %if.end24, %if.end19, %if.end14
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker108 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 76
  store i32 0, i32* %unread_marker108, align 8, !tbaa !8
  br label %for.cond

return:                                           ; preds = %if.then97, %if.then82, %if.then76, %if.then68, %if.then63, %if.then58, %if.then53, %sw.bb45, %if.end43, %if.then42, %if.then33, %if.then28, %if.then23, %if.then18, %if.then13, %if.then6, %if.then3
  %75 = load i32, i32* %retval, align 4
  ret i32 %75
}

; Function Attrs: nounwind
define internal i32 @read_restart_marker(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 76
  %1 = load i32, i32* %unread_marker, align 8, !tbaa !8
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end2

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @next_marker(%struct.jpeg_decompress_struct* %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then1

if.then1:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end2

if.end2:                                          ; preds = %if.end, %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 76
  %4 = load i32, i32* %unread_marker3, align 8, !tbaa !8
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 82
  %6 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %next_restart_num = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %6, i32 0, i32 5
  %7 = load i32, i32* %next_restart_num, align 4, !tbaa !44
  %add = add nsw i32 208, %7
  %cmp4 = icmp eq i32 %4, %add
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end2
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 5
  store i32 98, i32* %msg_code, align 4, !tbaa !13
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 82
  %11 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker6, align 8, !tbaa !24
  %next_restart_num7 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %11, i32 0, i32 5
  %12 = load i32, i32* %next_restart_num7, align 4, !tbaa !44
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %12, i32* %arrayidx, align 4, !tbaa !16
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 1
  %17 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_decompress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19, i32 3)
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 76
  store i32 0, i32* %unread_marker10, align 8, !tbaa !8
  br label %if.end18

if.else:                                          ; preds = %if.end2
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 6
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  %resync_to_restart = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 5
  %resync_to_restart11 = bitcast {}** %resync_to_restart to i32 (%struct.jpeg_decompress_struct*, i32)**
  %23 = load i32 (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)** %resync_to_restart11, align 4, !tbaa !45
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 82
  %26 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker12, align 8, !tbaa !24
  %next_restart_num13 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %26, i32 0, i32 5
  %27 = load i32, i32* %next_restart_num13, align 4, !tbaa !44
  %call14 = call i32 %23(%struct.jpeg_decompress_struct* %24, i32 %27)
  %tobool15 = icmp ne i32 %call14, 0
  br i1 %tobool15, label %if.end17, label %if.then16

if.then16:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.else
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.then5
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 82
  %29 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker19, align 8, !tbaa !24
  %next_restart_num20 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %29, i32 0, i32 5
  %30 = load i32, i32* %next_restart_num20, align 4, !tbaa !44
  %add21 = add nsw i32 %30, 1
  %and = and i32 %add21, 7
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 82
  %32 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker22, align 8, !tbaa !24
  %next_restart_num23 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %32, i32 0, i32 5
  store i32 %and, i32* %next_restart_num23, align 4, !tbaa !44
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end18, %if.then16, %if.then1
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: nounwind
define internal i32 @skip_variable(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 6
  %3 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %3, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %4 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %5, i32 0, i32 0
  %6 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %6, i8** %next_input_byte, align 4, !tbaa !2
  %7 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 1
  %9 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %9, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %10 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %10, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 3
  %12 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %12(%struct.jpeg_decompress_struct* %13)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 0
  %15 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %15, i8** %next_input_byte, align 4, !tbaa !2
  %16 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %16, i32 0, i32 1
  %17 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %17, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %18 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %18, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %19 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %19, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %20 = load i8, i8* %19, align 1, !tbaa !16
  %conv = zext i8 %20 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %21 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %21, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 3
  %23 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %23(%struct.jpeg_decompress_struct* %24)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %25 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %25, i32 0, i32 0
  %26 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %26, i8** %next_input_byte, align 4, !tbaa !2
  %27 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %27, i32 0, i32 1
  %28 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %28, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %29 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %29, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %30 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !16
  %conv20 = zext i8 %31 to i32
  %32 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %32, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  %33 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %33, 2
  store i32 %sub, i32* %length, align 4, !tbaa !22
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 5
  store i32 91, i32* %msg_code, align 4, !tbaa !13
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 76
  %37 = load i32, i32* %unread_marker, align 8, !tbaa !8
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err21, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %37, i32* %arrayidx, align 4, !tbaa !16
  %40 = load i32, i32* %length, align 4, !tbaa !22
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err22, align 8, !tbaa !12
  %msg_parm23 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 6
  %i24 = bitcast %union.anon* %msg_parm23 to [8 x i32]*
  %arrayidx25 = getelementptr inbounds [8 x i32], [8 x i32]* %i24, i32 0, i32 1
  store i32 %40, i32* %arrayidx25, align 4, !tbaa !16
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 0
  %44 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err26, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %44, i32 0, i32 1
  %45 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %47 = bitcast %struct.jpeg_decompress_struct* %46 to %struct.jpeg_common_struct*
  call void %45(%struct.jpeg_common_struct* %47, i32 1)
  %48 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %49 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte27 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %49, i32 0, i32 0
  store i8* %48, i8** %next_input_byte27, align 4, !tbaa !19
  %50 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %51 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer28 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %51, i32 0, i32 1
  store i32 %50, i32* %bytes_in_buffer28, align 4, !tbaa !21
  %52 = load i32, i32* %length, align 4, !tbaa !22
  %cmp29 = icmp sgt i32 %52, 0
  br i1 %cmp29, label %if.then31, label %if.end33

if.then31:                                        ; preds = %do.end
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 6
  %54 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src32, align 8, !tbaa !18
  %skip_input_data = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %54, i32 0, i32 4
  %55 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %skip_input_data, align 4, !tbaa !46
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = load i32, i32* %length, align 4, !tbaa !22
  call void %55(%struct.jpeg_decompress_struct* %56, i32 %57)
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %do.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end33, %if.then13, %if.then3
  %58 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = load i32, i32* %retval, align 4
  ret i32 %62
}

; Function Attrs: nounwind
define internal i32 @get_interesting_appn(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %b = alloca [14 x i8], align 1
  %i = alloca i32, align 4
  %numtoread = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast [14 x i8]* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 14, i8* %1) #4
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %numtoread to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 6
  %6 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %6, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %7 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 0
  %9 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %9, i8** %next_input_byte, align 4, !tbaa !2
  %10 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 1
  %12 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %12, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %13 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %13, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 3
  %15 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %15(%struct.jpeg_decompress_struct* %16)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %17 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %17, i32 0, i32 0
  %18 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %18, i8** %next_input_byte, align 4, !tbaa !2
  %19 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %19, i32 0, i32 1
  %20 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %20, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %21 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %21, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %22 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %22, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !16
  %conv = zext i8 %23 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %24 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %24, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %25 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %25, i32 0, i32 3
  %26 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %26(%struct.jpeg_decompress_struct* %27)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 0
  %29 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %29, i8** %next_input_byte, align 4, !tbaa !2
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %30, i32 0, i32 1
  %31 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %31, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %32 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %32, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %33 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %33, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %34 = load i8, i8* %33, align 1, !tbaa !16
  %conv20 = zext i8 %34 to i32
  %35 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %35, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  %36 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %36, 2
  store i32 %sub, i32* %length, align 4, !tbaa !22
  %37 = load i32, i32* %length, align 4, !tbaa !22
  %cmp21 = icmp sge i32 %37, 14
  br i1 %cmp21, label %if.then23, label %if.else

if.then23:                                        ; preds = %do.end
  store i32 14, i32* %numtoread, align 4, !tbaa !6
  br label %if.end29

if.else:                                          ; preds = %do.end
  %38 = load i32, i32* %length, align 4, !tbaa !22
  %cmp24 = icmp sgt i32 %38, 0
  br i1 %cmp24, label %if.then26, label %if.else27

if.then26:                                        ; preds = %if.else
  %39 = load i32, i32* %length, align 4, !tbaa !22
  store i32 %39, i32* %numtoread, align 4, !tbaa !6
  br label %if.end28

if.else27:                                        ; preds = %if.else
  store i32 0, i32* %numtoread, align 4, !tbaa !6
  br label %if.end28

if.end28:                                         ; preds = %if.else27, %if.then26
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then23
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end29
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %numtoread, align 4, !tbaa !6
  %cmp30 = icmp ult i32 %40, %41
  br i1 %cmp30, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body32

do.body32:                                        ; preds = %for.body
  %42 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp33 = icmp eq i32 %42, 0
  br i1 %cmp33, label %if.then35, label %if.end43

if.then35:                                        ; preds = %do.body32
  %43 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer36 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %43, i32 0, i32 3
  %44 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer36, align 4, !tbaa !23
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call37 = call i32 %44(%struct.jpeg_decompress_struct* %45)
  %tobool38 = icmp ne i32 %call37, 0
  br i1 %tobool38, label %if.end40, label %if.then39

if.then39:                                        ; preds = %if.then35
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %if.then35
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte41 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 0
  %47 = load i8*, i8** %next_input_byte41, align 4, !tbaa !19
  store i8* %47, i8** %next_input_byte, align 4, !tbaa !2
  %48 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer42 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %48, i32 0, i32 1
  %49 = load i32, i32* %bytes_in_buffer42, align 4, !tbaa !21
  store i32 %49, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end43

if.end43:                                         ; preds = %if.end40, %do.body32
  %50 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec44 = add i32 %50, -1
  store i32 %dec44, i32* %bytes_in_buffer, align 4, !tbaa !22
  %51 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr45, i8** %next_input_byte, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !16
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [14 x i8], [14 x i8]* %b, i32 0, i32 %53
  store i8 %52, i8* %arrayidx, align 1, !tbaa !16
  br label %do.cond46

do.cond46:                                        ; preds = %if.end43
  br label %do.end47

do.end47:                                         ; preds = %do.cond46
  br label %for.inc

for.inc:                                          ; preds = %do.end47
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %54, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %55 = load i32, i32* %numtoread, align 4, !tbaa !6
  %56 = load i32, i32* %length, align 4, !tbaa !22
  %sub48 = sub i32 %56, %55
  store i32 %sub48, i32* %length, align 4, !tbaa !22
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 76
  %58 = load i32, i32* %unread_marker, align 8, !tbaa !8
  switch i32 %58, label %sw.default [
    i32 224, label %sw.bb
    i32 238, label %sw.bb49
  ]

sw.bb:                                            ; preds = %for.end
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [14 x i8], [14 x i8]* %b, i32 0, i32 0
  %60 = load i32, i32* %numtoread, align 4, !tbaa !6
  %61 = load i32, i32* %length, align 4, !tbaa !22
  call void @examine_app0(%struct.jpeg_decompress_struct* %59, i8* %arraydecay, i32 %60, i32 %61)
  br label %sw.epilog

sw.bb49:                                          ; preds = %for.end
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arraydecay50 = getelementptr inbounds [14 x i8], [14 x i8]* %b, i32 0, i32 0
  %63 = load i32, i32* %numtoread, align 4, !tbaa !6
  %64 = load i32, i32* %length, align 4, !tbaa !22
  call void @examine_app14(%struct.jpeg_decompress_struct* %62, i8* %arraydecay50, i32 %63, i32 %64)
  br label %sw.epilog

sw.default:                                       ; preds = %for.end
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 0
  %66 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %66, i32 0, i32 5
  store i32 68, i32* %msg_code, align 4, !tbaa !13
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker51 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 76
  %68 = load i32, i32* %unread_marker51, align 8, !tbaa !8
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err52, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 6
  %i53 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx54 = getelementptr inbounds [8 x i32], [8 x i32]* %i53, i32 0, i32 0
  store i32 %68, i32* %arrayidx54, align 4, !tbaa !16
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 0
  %72 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err55, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %72, i32 0, i32 0
  %73 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %75 = bitcast %struct.jpeg_decompress_struct* %74 to %struct.jpeg_common_struct*
  call void %73(%struct.jpeg_common_struct* %75)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb49, %sw.bb
  %76 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %77 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte56 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %77, i32 0, i32 0
  store i8* %76, i8** %next_input_byte56, align 4, !tbaa !19
  %78 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %79 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer57 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %79, i32 0, i32 1
  store i32 %78, i32* %bytes_in_buffer57, align 4, !tbaa !21
  %80 = load i32, i32* %length, align 4, !tbaa !22
  %cmp58 = icmp sgt i32 %80, 0
  br i1 %cmp58, label %if.then60, label %if.end62

if.then60:                                        ; preds = %sw.epilog
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 6
  %82 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src61, align 8, !tbaa !18
  %skip_input_data = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %82, i32 0, i32 4
  %83 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %skip_input_data, align 4, !tbaa !46
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %85 = load i32, i32* %length, align 4, !tbaa !22
  call void %83(%struct.jpeg_decompress_struct* %84, i32 %85)
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %sw.epilog
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end62, %if.then39, %if.then13, %if.then3
  %86 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %numtoread to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast [14 x i8]* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 14, i8* %91) #4
  %92 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = load i32, i32* %retval, align 4
  ret i32 %93
}

; Function Attrs: nounwind
define hidden void @jpeg_save_markers(%struct.jpeg_decompress_struct* %cinfo, i32 %marker_code, i32 %length_limit) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %marker_code.addr = alloca i32, align 4
  %length_limit.addr = alloca i32, align 4
  %marker = alloca %struct.my_marker_reader*, align 4
  %maxlength = alloca i32, align 4
  %processor = alloca i32 (%struct.jpeg_decompress_struct*)*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %marker_code, i32* %marker_code.addr, align 4, !tbaa !6
  store i32 %length_limit, i32* %length_limit.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 82
  %2 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker1, align 8, !tbaa !24
  %3 = bitcast %struct.jpeg_marker_reader* %2 to %struct.my_marker_reader*
  store %struct.my_marker_reader* %3, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %4 = bitcast i32* %maxlength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32 (%struct.jpeg_decompress_struct*)** %processor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !27
  %max_alloc_chunk = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %7, i32 0, i32 12
  %8 = load i32, i32* %max_alloc_chunk, align 4, !tbaa !47
  %sub = sub i32 %8, 20
  store i32 %sub, i32* %maxlength, align 4, !tbaa !22
  %9 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %10 = load i32, i32* %maxlength, align 4, !tbaa !22
  %cmp = icmp sgt i32 %9, %10
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load i32, i32* %maxlength, align 4, !tbaa !22
  store i32 %11, i32* %length_limit.addr, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then2, label %if.else12

if.then2:                                         ; preds = %if.end
  store i32 (%struct.jpeg_decompress_struct*)* @save_marker, i32 (%struct.jpeg_decompress_struct*)** %processor, align 4, !tbaa !2
  %13 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp3 = icmp eq i32 %13, 224
  br i1 %cmp3, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then2
  %14 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %14, 14
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %land.lhs.true
  store i32 14, i32* %length_limit.addr, align 4, !tbaa !6
  br label %if.end11

if.else:                                          ; preds = %land.lhs.true, %if.then2
  %15 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %15, 238
  br i1 %cmp6, label %land.lhs.true7, label %if.end10

land.lhs.true7:                                   ; preds = %if.else
  %16 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %cmp8 = icmp ult i32 %16, 12
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %land.lhs.true7
  store i32 12, i32* %length_limit.addr, align 4, !tbaa !6
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %land.lhs.true7, %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.then5
  br label %if.end17

if.else12:                                        ; preds = %if.end
  store i32 (%struct.jpeg_decompress_struct*)* @skip_variable, i32 (%struct.jpeg_decompress_struct*)** %processor, align 4, !tbaa !2
  %17 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp13 = icmp eq i32 %17, 224
  br i1 %cmp13, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else12
  %18 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %18, 238
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %lor.lhs.false, %if.else12
  store i32 (%struct.jpeg_decompress_struct*)* @get_interesting_appn, i32 (%struct.jpeg_decompress_struct*)** %processor, align 4, !tbaa !2
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %lor.lhs.false
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end11
  %19 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp18 = icmp eq i32 %19, 254
  br i1 %cmp18, label %if.then19, label %if.else20

if.then19:                                        ; preds = %if.end17
  %20 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %processor, align 4, !tbaa !2
  %21 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %21, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* %20, i32 (%struct.jpeg_decompress_struct*)** %process_COM, align 4, !tbaa !34
  %22 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %23 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %23, i32 0, i32 3
  store i32 %22, i32* %length_limit_COM, align 4, !tbaa !35
  br label %if.end33

if.else20:                                        ; preds = %if.end17
  %24 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp21 = icmp sge i32 %24, 224
  br i1 %cmp21, label %land.lhs.true22, label %if.else28

land.lhs.true22:                                  ; preds = %if.else20
  %25 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp23 = icmp sle i32 %25, 239
  br i1 %cmp23, label %if.then24, label %if.else28

if.then24:                                        ; preds = %land.lhs.true22
  %26 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %processor, align 4, !tbaa !2
  %27 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %27, i32 0, i32 2
  %28 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %sub25 = sub nsw i32 %28, 224
  %arrayidx = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn, i32 0, i32 %sub25
  store i32 (%struct.jpeg_decompress_struct*)* %26, i32 (%struct.jpeg_decompress_struct*)** %arrayidx, align 4, !tbaa !2
  %29 = load i32, i32* %length_limit.addr, align 4, !tbaa !6
  %30 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %30, i32 0, i32 4
  %31 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %sub26 = sub nsw i32 %31, 224
  %arrayidx27 = getelementptr inbounds [16 x i32], [16 x i32]* %length_limit_APPn, i32 0, i32 %sub26
  store i32 %29, i32* %arrayidx27, align 4, !tbaa !6
  br label %if.end32

if.else28:                                        ; preds = %land.lhs.true22, %if.else20
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 5
  store i32 68, i32* %msg_code, align 4, !tbaa !13
  %34 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 0
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %36, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx30 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %34, i32* %arrayidx30, align 4, !tbaa !16
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  %38 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err31, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %38, i32 0, i32 0
  %39 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = bitcast %struct.jpeg_decompress_struct* %40 to %struct.jpeg_common_struct*
  call void %39(%struct.jpeg_common_struct* %41)
  br label %if.end32

if.end32:                                         ; preds = %if.else28, %if.then24
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then19
  %42 = bitcast i32 (%struct.jpeg_decompress_struct*)** %processor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i32* %maxlength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  ret void
}

; Function Attrs: nounwind
define internal i32 @save_marker(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %marker = alloca %struct.my_marker_reader*, align 4
  %cur_marker = alloca %struct.jpeg_marker_struct*, align 4
  %bytes_read = alloca i32, align 4
  %data_length = alloca i32, align 4
  %data = alloca i8*, align 4
  %length = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %limit = alloca i32, align 4
  %prev = alloca %struct.jpeg_marker_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 82
  %2 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker1, align 8, !tbaa !24
  %3 = bitcast %struct.jpeg_marker_reader* %2 to %struct.my_marker_reader*
  store %struct.my_marker_reader* %3, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %4 = bitcast %struct.jpeg_marker_struct** %cur_marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %cur_marker2 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %5, i32 0, i32 5
  %6 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker2, align 4, !tbaa !41
  store %struct.jpeg_marker_struct* %6, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %7 = bitcast i32* %bytes_read to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %data_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %length, align 4, !tbaa !22
  %11 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 6
  %13 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %13, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %14 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte3 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 0
  %16 = load i8*, i8** %next_input_byte3, align 4, !tbaa !19
  store i8* %16, i8** %next_input_byte, align 4, !tbaa !2
  %17 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %18, i32 0, i32 1
  %19 = load i32, i32* %bytes_in_buffer4, align 4, !tbaa !21
  store i32 %19, i32* %bytes_in_buffer, align 4, !tbaa !22
  %20 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %cmp = icmp eq %struct.jpeg_marker_struct* %20, null
  br i1 %cmp, label %if.then, label %if.else49

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %if.then
  %21 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp5 = icmp eq i32 %21, 0
  br i1 %cmp5, label %if.then6, label %if.end10

if.then6:                                         ; preds = %do.body
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 3
  %23 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %23(%struct.jpeg_decompress_struct* %24)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then7

if.then7:                                         ; preds = %if.then6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then6
  %25 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte8 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %25, i32 0, i32 0
  %26 = load i8*, i8** %next_input_byte8, align 4, !tbaa !19
  store i8* %26, i8** %next_input_byte, align 4, !tbaa !2
  %27 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer9 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %27, i32 0, i32 1
  %28 = load i32, i32* %bytes_in_buffer9, align 4, !tbaa !21
  store i32 %28, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end10

if.end10:                                         ; preds = %if.end, %do.body
  %29 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %29, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %30 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !16
  %conv = zext i8 %31 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %32 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp11 = icmp eq i32 %32, 0
  br i1 %cmp11, label %if.then13, label %if.end21

if.then13:                                        ; preds = %if.end10
  %33 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer14 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %33, i32 0, i32 3
  %34 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer14, align 4, !tbaa !23
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call15 = call i32 %34(%struct.jpeg_decompress_struct* %35)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.then13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %if.then13
  %36 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte19 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %36, i32 0, i32 0
  %37 = load i8*, i8** %next_input_byte19, align 4, !tbaa !19
  store i8* %37, i8** %next_input_byte, align 4, !tbaa !2
  %38 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer20 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %38, i32 0, i32 1
  %39 = load i32, i32* %bytes_in_buffer20, align 4, !tbaa !21
  store i32 %39, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end21

if.end21:                                         ; preds = %if.end18, %if.end10
  %40 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec22 = add i32 %40, -1
  store i32 %dec22, i32* %bytes_in_buffer, align 4, !tbaa !22
  %41 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr23 = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr23, i8** %next_input_byte, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !16
  %conv24 = zext i8 %42 to i32
  %43 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %43, %conv24
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end21
  br label %do.end

do.end:                                           ; preds = %do.cond
  %44 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %44, 2
  store i32 %sub, i32* %length, align 4, !tbaa !22
  %45 = load i32, i32* %length, align 4, !tbaa !22
  %cmp25 = icmp sge i32 %45, 0
  br i1 %cmp25, label %if.then27, label %if.else47

if.then27:                                        ; preds = %do.end
  %46 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #4
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 76
  %48 = load i32, i32* %unread_marker, align 8, !tbaa !8
  %cmp28 = icmp eq i32 %48, 254
  br i1 %cmp28, label %if.then30, label %if.else

if.then30:                                        ; preds = %if.then27
  %49 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %49, i32 0, i32 3
  %50 = load i32, i32* %length_limit_COM, align 4, !tbaa !35
  store i32 %50, i32* %limit, align 4, !tbaa !6
  br label %if.end33

if.else:                                          ; preds = %if.then27
  %51 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %length_limit_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %51, i32 0, i32 4
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 76
  %53 = load i32, i32* %unread_marker31, align 8, !tbaa !8
  %sub32 = sub nsw i32 %53, 224
  %arrayidx = getelementptr inbounds [16 x i32], [16 x i32]* %length_limit_APPn, i32 0, i32 %sub32
  %54 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %54, i32* %limit, align 4, !tbaa !6
  br label %if.end33

if.end33:                                         ; preds = %if.else, %if.then30
  %55 = load i32, i32* %length, align 4, !tbaa !22
  %56 = load i32, i32* %limit, align 4, !tbaa !6
  %cmp34 = icmp ult i32 %55, %56
  br i1 %cmp34, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.end33
  %57 = load i32, i32* %length, align 4, !tbaa !22
  store i32 %57, i32* %limit, align 4, !tbaa !6
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %if.end33
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 1
  %59 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !27
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %59, i32 0, i32 1
  %60 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !48
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %62 = bitcast %struct.jpeg_decompress_struct* %61 to %struct.jpeg_common_struct*
  %63 = load i32, i32* %limit, align 4, !tbaa !6
  %add38 = add i32 20, %63
  %call39 = call i8* %60(%struct.jpeg_common_struct* %62, i32 1, i32 %add38)
  %64 = bitcast i8* %call39 to %struct.jpeg_marker_struct*
  store %struct.jpeg_marker_struct* %64, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %65 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %65, i32 0, i32 0
  store %struct.jpeg_marker_struct* null, %struct.jpeg_marker_struct** %next, align 4, !tbaa !49
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 76
  %67 = load i32, i32* %unread_marker40, align 8, !tbaa !8
  %conv41 = trunc i32 %67 to i8
  %68 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %marker42 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %68, i32 0, i32 1
  store i8 %conv41, i8* %marker42, align 4, !tbaa !51
  %69 = load i32, i32* %length, align 4, !tbaa !22
  %70 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %original_length = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %70, i32 0, i32 2
  store i32 %69, i32* %original_length, align 4, !tbaa !52
  %71 = load i32, i32* %limit, align 4, !tbaa !6
  %72 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %data_length43 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %72, i32 0, i32 3
  store i32 %71, i32* %data_length43, align 4, !tbaa !53
  %73 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %73, i32 1
  %74 = bitcast %struct.jpeg_marker_struct* %add.ptr to i8*
  %75 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %data44 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %75, i32 0, i32 4
  store i8* %74, i8** %data44, align 4, !tbaa !54
  store i8* %74, i8** %data, align 4, !tbaa !2
  %76 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %77 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %cur_marker45 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %77, i32 0, i32 5
  store %struct.jpeg_marker_struct* %76, %struct.jpeg_marker_struct** %cur_marker45, align 4, !tbaa !41
  %78 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %bytes_read46 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %78, i32 0, i32 6
  store i32 0, i32* %bytes_read46, align 4, !tbaa !55
  store i32 0, i32* %bytes_read, align 4, !tbaa !6
  %79 = load i32, i32* %limit, align 4, !tbaa !6
  store i32 %79, i32* %data_length, align 4, !tbaa !6
  %80 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  br label %if.end48

if.else47:                                        ; preds = %do.end
  store i32 0, i32* %data_length, align 4, !tbaa !6
  store i32 0, i32* %bytes_read, align 4, !tbaa !6
  store i8* null, i8** %data, align 4, !tbaa !2
  br label %if.end48

if.end48:                                         ; preds = %if.else47, %if.end37
  br label %if.end54

if.else49:                                        ; preds = %entry
  %81 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %bytes_read50 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %81, i32 0, i32 6
  %82 = load i32, i32* %bytes_read50, align 4, !tbaa !55
  store i32 %82, i32* %bytes_read, align 4, !tbaa !6
  %83 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %data_length51 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %83, i32 0, i32 3
  %84 = load i32, i32* %data_length51, align 4, !tbaa !53
  store i32 %84, i32* %data_length, align 4, !tbaa !6
  %85 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %data52 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %85, i32 0, i32 4
  %86 = load i8*, i8** %data52, align 4, !tbaa !54
  %87 = load i32, i32* %bytes_read, align 4, !tbaa !6
  %add.ptr53 = getelementptr inbounds i8, i8* %86, i32 %87
  store i8* %add.ptr53, i8** %data, align 4, !tbaa !2
  br label %if.end54

if.end54:                                         ; preds = %if.else49, %if.end48
  br label %while.cond

while.cond:                                       ; preds = %while.end, %if.end54
  %88 = load i32, i32* %bytes_read, align 4, !tbaa !6
  %89 = load i32, i32* %data_length, align 4, !tbaa !6
  %cmp55 = icmp ult i32 %88, %89
  br i1 %cmp55, label %while.body, label %while.end80

while.body:                                       ; preds = %while.cond
  %90 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %91 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte57 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %91, i32 0, i32 0
  store i8* %90, i8** %next_input_byte57, align 4, !tbaa !19
  %92 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %93 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer58 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %93, i32 0, i32 1
  store i32 %92, i32* %bytes_in_buffer58, align 4, !tbaa !21
  %94 = load i32, i32* %bytes_read, align 4, !tbaa !6
  %95 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %bytes_read59 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %95, i32 0, i32 6
  store i32 %94, i32* %bytes_read59, align 4, !tbaa !55
  %96 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp60 = icmp eq i32 %96, 0
  br i1 %cmp60, label %if.then62, label %if.end70

if.then62:                                        ; preds = %while.body
  %97 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer63 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %97, i32 0, i32 3
  %98 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer63, align 4, !tbaa !23
  %99 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call64 = call i32 %98(%struct.jpeg_decompress_struct* %99)
  %tobool65 = icmp ne i32 %call64, 0
  br i1 %tobool65, label %if.end67, label %if.then66

if.then66:                                        ; preds = %if.then62
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end67:                                         ; preds = %if.then62
  %100 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte68 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %100, i32 0, i32 0
  %101 = load i8*, i8** %next_input_byte68, align 4, !tbaa !19
  store i8* %101, i8** %next_input_byte, align 4, !tbaa !2
  %102 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer69 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %102, i32 0, i32 1
  %103 = load i32, i32* %bytes_in_buffer69, align 4, !tbaa !21
  store i32 %103, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end70

if.end70:                                         ; preds = %if.end67, %while.body
  br label %while.cond71

while.cond71:                                     ; preds = %while.body76, %if.end70
  %104 = load i32, i32* %bytes_read, align 4, !tbaa !6
  %105 = load i32, i32* %data_length, align 4, !tbaa !6
  %cmp72 = icmp ult i32 %104, %105
  br i1 %cmp72, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond71
  %106 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp74 = icmp ugt i32 %106, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond71
  %107 = phi i1 [ false, %while.cond71 ], [ %cmp74, %land.rhs ]
  br i1 %107, label %while.body76, label %while.end

while.body76:                                     ; preds = %land.end
  %108 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr77 = getelementptr inbounds i8, i8* %108, i32 1
  store i8* %incdec.ptr77, i8** %next_input_byte, align 4, !tbaa !2
  %109 = load i8, i8* %108, align 1, !tbaa !16
  %110 = load i8*, i8** %data, align 4, !tbaa !2
  %incdec.ptr78 = getelementptr inbounds i8, i8* %110, i32 1
  store i8* %incdec.ptr78, i8** %data, align 4, !tbaa !2
  store i8 %109, i8* %110, align 1, !tbaa !16
  %111 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec79 = add i32 %111, -1
  store i32 %dec79, i32* %bytes_in_buffer, align 4, !tbaa !22
  %112 = load i32, i32* %bytes_read, align 4, !tbaa !6
  %inc = add i32 %112, 1
  store i32 %inc, i32* %bytes_read, align 4, !tbaa !6
  br label %while.cond71

while.end:                                        ; preds = %land.end
  br label %while.cond

while.end80:                                      ; preds = %while.cond
  %113 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %cmp81 = icmp ne %struct.jpeg_marker_struct* %113, null
  br i1 %cmp81, label %if.then83, label %if.end102

if.then83:                                        ; preds = %while.end80
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker_list = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 60
  %115 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %marker_list, align 8, !tbaa !56
  %cmp84 = icmp eq %struct.jpeg_marker_struct* %115, null
  br i1 %cmp84, label %if.then86, label %if.else88

if.then86:                                        ; preds = %if.then83
  %116 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %117 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker_list87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %117, i32 0, i32 60
  store %struct.jpeg_marker_struct* %116, %struct.jpeg_marker_struct** %marker_list87, align 8, !tbaa !56
  br label %if.end98

if.else88:                                        ; preds = %if.then83
  %118 = bitcast %struct.jpeg_marker_struct** %prev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #4
  %119 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker_list89 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %119, i32 0, i32 60
  %120 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %marker_list89, align 8, !tbaa !56
  store %struct.jpeg_marker_struct* %120, %struct.jpeg_marker_struct** %prev, align 4, !tbaa !2
  br label %while.cond90

while.cond90:                                     ; preds = %while.body94, %if.else88
  %121 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %prev, align 4, !tbaa !2
  %next91 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %121, i32 0, i32 0
  %122 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %next91, align 4, !tbaa !49
  %cmp92 = icmp ne %struct.jpeg_marker_struct* %122, null
  br i1 %cmp92, label %while.body94, label %while.end96

while.body94:                                     ; preds = %while.cond90
  %123 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %prev, align 4, !tbaa !2
  %next95 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %123, i32 0, i32 0
  %124 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %next95, align 4, !tbaa !49
  store %struct.jpeg_marker_struct* %124, %struct.jpeg_marker_struct** %prev, align 4, !tbaa !2
  br label %while.cond90

while.end96:                                      ; preds = %while.cond90
  %125 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %126 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %prev, align 4, !tbaa !2
  %next97 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %126, i32 0, i32 0
  store %struct.jpeg_marker_struct* %125, %struct.jpeg_marker_struct** %next97, align 4, !tbaa !49
  %127 = bitcast %struct.jpeg_marker_struct** %prev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #4
  br label %if.end98

if.end98:                                         ; preds = %while.end96, %if.then86
  %128 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %data99 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %128, i32 0, i32 4
  %129 = load i8*, i8** %data99, align 4, !tbaa !54
  store i8* %129, i8** %data, align 4, !tbaa !2
  %130 = load %struct.jpeg_marker_struct*, %struct.jpeg_marker_struct** %cur_marker, align 4, !tbaa !2
  %original_length100 = getelementptr inbounds %struct.jpeg_marker_struct, %struct.jpeg_marker_struct* %130, i32 0, i32 2
  %131 = load i32, i32* %original_length100, align 4, !tbaa !52
  %132 = load i32, i32* %data_length, align 4, !tbaa !6
  %sub101 = sub i32 %131, %132
  store i32 %sub101, i32* %length, align 4, !tbaa !22
  br label %if.end102

if.end102:                                        ; preds = %if.end98, %while.end80
  %133 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %cur_marker103 = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %133, i32 0, i32 5
  store %struct.jpeg_marker_struct* null, %struct.jpeg_marker_struct** %cur_marker103, align 4, !tbaa !41
  %134 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker104 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %134, i32 0, i32 76
  %135 = load i32, i32* %unread_marker104, align 8, !tbaa !8
  switch i32 %135, label %sw.default [
    i32 224, label %sw.bb
    i32 238, label %sw.bb105
  ]

sw.bb:                                            ; preds = %if.end102
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %137 = load i8*, i8** %data, align 4, !tbaa !2
  %138 = load i32, i32* %data_length, align 4, !tbaa !6
  %139 = load i32, i32* %length, align 4, !tbaa !22
  call void @examine_app0(%struct.jpeg_decompress_struct* %136, i8* %137, i32 %138, i32 %139)
  br label %sw.epilog

sw.bb105:                                         ; preds = %if.end102
  %140 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %141 = load i8*, i8** %data, align 4, !tbaa !2
  %142 = load i32, i32* %data_length, align 4, !tbaa !6
  %143 = load i32, i32* %length, align 4, !tbaa !22
  call void @examine_app14(%struct.jpeg_decompress_struct* %140, i8* %141, i32 %142, i32 %143)
  br label %sw.epilog

sw.default:                                       ; preds = %if.end102
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 0
  %145 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %145, i32 0, i32 5
  store i32 91, i32* %msg_code, align 4, !tbaa !13
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker106 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %146, i32 0, i32 76
  %147 = load i32, i32* %unread_marker106, align 8, !tbaa !8
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err107 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 0
  %149 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err107, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %149, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx108 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %147, i32* %arrayidx108, align 4, !tbaa !16
  %150 = load i32, i32* %data_length, align 4, !tbaa !6
  %151 = load i32, i32* %length, align 4, !tbaa !22
  %add109 = add i32 %150, %151
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err110 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 0
  %153 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err110, align 8, !tbaa !12
  %msg_parm111 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %153, i32 0, i32 6
  %i112 = bitcast %union.anon* %msg_parm111 to [8 x i32]*
  %arrayidx113 = getelementptr inbounds [8 x i32], [8 x i32]* %i112, i32 0, i32 1
  store i32 %add109, i32* %arrayidx113, align 4, !tbaa !16
  %154 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err114 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %154, i32 0, i32 0
  %155 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err114, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %155, i32 0, i32 1
  %156 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %157 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %158 = bitcast %struct.jpeg_decompress_struct* %157 to %struct.jpeg_common_struct*
  call void %156(%struct.jpeg_common_struct* %158, i32 1)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb105, %sw.bb
  %159 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %160 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte115 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %160, i32 0, i32 0
  store i8* %159, i8** %next_input_byte115, align 4, !tbaa !19
  %161 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %162 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer116 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %162, i32 0, i32 1
  store i32 %161, i32* %bytes_in_buffer116, align 4, !tbaa !21
  %163 = load i32, i32* %length, align 4, !tbaa !22
  %cmp117 = icmp sgt i32 %163, 0
  br i1 %cmp117, label %if.then119, label %if.end121

if.then119:                                       ; preds = %sw.epilog
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src120 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 6
  %165 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src120, align 8, !tbaa !18
  %skip_input_data = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %165, i32 0, i32 4
  %166 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %skip_input_data, align 4, !tbaa !46
  %167 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %168 = load i32, i32* %length, align 4, !tbaa !22
  call void %166(%struct.jpeg_decompress_struct* %167, i32 %168)
  br label %if.end121

if.end121:                                        ; preds = %if.then119, %sw.epilog
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end121, %if.then66, %if.then17, %if.then7
  %169 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  %172 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #4
  %173 = bitcast i8** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #4
  %174 = bitcast i32* %data_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #4
  %175 = bitcast i32* %bytes_read to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #4
  %176 = bitcast %struct.jpeg_marker_struct** %cur_marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #4
  %177 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #4
  %178 = load i32, i32* %retval, align 4
  ret i32 %178
}

; Function Attrs: nounwind
define hidden void @jpeg_set_marker_processor(%struct.jpeg_decompress_struct* %cinfo, i32 %marker_code, i32 (%struct.jpeg_decompress_struct*)* %routine) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %marker_code.addr = alloca i32, align 4
  %routine.addr = alloca i32 (%struct.jpeg_decompress_struct*)*, align 4
  %marker = alloca %struct.my_marker_reader*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %marker_code, i32* %marker_code.addr, align 4, !tbaa !6
  store i32 (%struct.jpeg_decompress_struct*)* %routine, i32 (%struct.jpeg_decompress_struct*)** %routine.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 82
  %2 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker1, align 8, !tbaa !24
  %3 = bitcast %struct.jpeg_marker_reader* %2 to %struct.my_marker_reader*
  store %struct.my_marker_reader* %3, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %4 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %4, 254
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %routine.addr, align 4, !tbaa !2
  %6 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_COM = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %6, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* %5, i32 (%struct.jpeg_decompress_struct*)** %process_COM, align 4, !tbaa !34
  br label %if.end9

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %7, 224
  br i1 %cmp2, label %land.lhs.true, label %if.else5

land.lhs.true:                                    ; preds = %if.else
  %8 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %cmp3 = icmp sle i32 %8, 239
  br i1 %cmp3, label %if.then4, label %if.else5

if.then4:                                         ; preds = %land.lhs.true
  %9 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %routine.addr, align 4, !tbaa !2
  %10 = load %struct.my_marker_reader*, %struct.my_marker_reader** %marker, align 4, !tbaa !2
  %process_APPn = getelementptr inbounds %struct.my_marker_reader, %struct.my_marker_reader* %10, i32 0, i32 2
  %11 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %11, 224
  %arrayidx = getelementptr inbounds [16 x i32 (%struct.jpeg_decompress_struct*)*], [16 x i32 (%struct.jpeg_decompress_struct*)*]* %process_APPn, i32 0, i32 %sub
  store i32 (%struct.jpeg_decompress_struct*)* %9, i32 (%struct.jpeg_decompress_struct*)** %arrayidx, align 4, !tbaa !2
  br label %if.end

if.else5:                                         ; preds = %land.lhs.true, %if.else
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 68, i32* %msg_code, align 4, !tbaa !13
  %14 = load i32, i32* %marker_code.addr, align 4, !tbaa !6
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx7 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %14, i32* %arrayidx7, align 4, !tbaa !16
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 0
  %19 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = bitcast %struct.jpeg_decompress_struct* %20 to %struct.jpeg_common_struct*
  call void %19(%struct.jpeg_common_struct* %21)
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then4
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %22 = bitcast %struct.my_marker_reader** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret void
}

; Function Attrs: nounwind
define internal i32 @first_marker(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %c = alloca i32, align 4
  %c2 = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 6
  %4 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %4, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %5 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %6, i32 0, i32 0
  %7 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %7, i8** %next_input_byte, align 4, !tbaa !2
  %8 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %9, i32 0, i32 1
  %10 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %10, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %11 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %11, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %12 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %12, i32 0, i32 3
  %13 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %13(%struct.jpeg_decompress_struct* %14)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 0
  %16 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %16, i8** %next_input_byte, align 4, !tbaa !2
  %17 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %17, i32 0, i32 1
  %18 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %18, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %19 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %19, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %20 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !16
  %conv = zext i8 %21 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %if.end6
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body7

do.body7:                                         ; preds = %do.end
  %22 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp8 = icmp eq i32 %22, 0
  br i1 %cmp8, label %if.then10, label %if.end18

if.then10:                                        ; preds = %do.body7
  %23 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer11 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %23, i32 0, i32 3
  %24 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer11, align 4, !tbaa !23
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call12 = call i32 %24(%struct.jpeg_decompress_struct* %25)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.then10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.then10
  %26 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %26, i32 0, i32 0
  %27 = load i8*, i8** %next_input_byte16, align 4, !tbaa !19
  store i8* %27, i8** %next_input_byte, align 4, !tbaa !2
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer17 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 1
  %29 = load i32, i32* %bytes_in_buffer17, align 4, !tbaa !21
  store i32 %29, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end18

if.end18:                                         ; preds = %if.end15, %do.body7
  %30 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec19 = add i32 %30, -1
  store i32 %dec19, i32* %bytes_in_buffer, align 4, !tbaa !22
  %31 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %31, i32 1
  store i8* %incdec.ptr20, i8** %next_input_byte, align 4, !tbaa !2
  %32 = load i8, i8* %31, align 1, !tbaa !16
  %conv21 = zext i8 %32 to i32
  store i32 %conv21, i32* %c2, align 4, !tbaa !6
  br label %do.cond22

do.cond22:                                        ; preds = %if.end18
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  %33 = load i32, i32* %c, align 4, !tbaa !6
  %cmp24 = icmp ne i32 %33, 255
  br i1 %cmp24, label %if.then28, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.end23
  %34 = load i32, i32* %c2, align 4, !tbaa !6
  %cmp26 = icmp ne i32 %34, 216
  br i1 %cmp26, label %if.then28, label %if.end35

if.then28:                                        ; preds = %lor.lhs.false, %do.end23
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 0
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %36, i32 0, i32 5
  store i32 53, i32* %msg_code, align 4, !tbaa !13
  %37 = load i32, i32* %c, align 4, !tbaa !6
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %37, i32* %arrayidx, align 4, !tbaa !16
  %40 = load i32, i32* %c2, align 4, !tbaa !6
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err30, align 8, !tbaa !12
  %msg_parm31 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 6
  %i32 = bitcast %union.anon* %msg_parm31 to [8 x i32]*
  %arrayidx33 = getelementptr inbounds [8 x i32], [8 x i32]* %i32, i32 0, i32 1
  store i32 %40, i32* %arrayidx33, align 4, !tbaa !16
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err34 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 0
  %44 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err34, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %44, i32 0, i32 0
  %45 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %47 = bitcast %struct.jpeg_decompress_struct* %46 to %struct.jpeg_common_struct*
  call void %45(%struct.jpeg_common_struct* %47)
  br label %if.end35

if.end35:                                         ; preds = %if.then28, %lor.lhs.false
  %48 = load i32, i32* %c2, align 4, !tbaa !6
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 76
  store i32 %48, i32* %unread_marker, align 8, !tbaa !8
  %50 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %51 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte36 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %51, i32 0, i32 0
  store i8* %50, i8** %next_input_byte36, align 4, !tbaa !19
  %52 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %53 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer37 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %53, i32 0, i32 1
  store i32 %52, i32* %bytes_in_buffer37, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end35, %if.then14, %if.then3
  %54 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = load i32, i32* %retval, align 4
  ret i32 %59
}

; Function Attrs: nounwind
define internal i32 @get_soi(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 0
  %2 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %2, i32 0, i32 5
  store i32 102, i32* %msg_code, align 4, !tbaa !13
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 1
  %5 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_decompress_struct* %6 to %struct.jpeg_common_struct*
  call void %5(%struct.jpeg_common_struct* %7, i32 1)
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 82
  %9 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %saw_SOI = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %9, i32 0, i32 3
  %10 = load i32, i32* %saw_SOI, align 4, !tbaa !42
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_code3 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 5
  store i32 61, i32* %msg_code3, align 4, !tbaa !13
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 0
  %15 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_decompress_struct* %16 to %struct.jpeg_common_struct*
  call void %15(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %18, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_dc_L = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 47
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* %arith_dc_L, i32 0, i32 %20
  store i8 0, i8* %arrayidx, align 1, !tbaa !16
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_dc_U = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 48
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [16 x i8], [16 x i8]* %arith_dc_U, i32 0, i32 %22
  store i8 1, i8* %arrayidx5, align 1, !tbaa !16
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_ac_K = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 49
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [16 x i8], [16 x i8]* %arith_ac_K, i32 0, i32 %24
  store i8 5, i8* %arrayidx6, align 1, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 50
  store i32 0, i32* %restart_interval, align 4, !tbaa !57
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 10
  store i32 0, i32* %jpeg_color_space, align 8, !tbaa !58
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 59
  store i32 0, i32* %CCIR601_sampling, align 4, !tbaa !59
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_JFIF_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 51
  store i32 0, i32* %saw_JFIF_marker, align 8, !tbaa !60
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 52
  store i8 1, i8* %JFIF_major_version, align 4, !tbaa !61
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 53
  store i8 1, i8* %JFIF_minor_version, align 1, !tbaa !62
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %density_unit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 54
  store i8 0, i8* %density_unit, align 2, !tbaa !63
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %X_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 55
  store i16 1, i16* %X_density, align 8, !tbaa !64
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Y_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 56
  store i16 1, i16* %Y_density, align 2, !tbaa !65
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_Adobe_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 57
  store i32 0, i32* %saw_Adobe_marker, align 4, !tbaa !66
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 58
  store i8 0, i8* %Adobe_transform, align 8, !tbaa !67
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 82
  %38 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker7, align 8, !tbaa !24
  %saw_SOI8 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %38, i32 0, i32 3
  store i32 1, i32* %saw_SOI8, align 4, !tbaa !42
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  ret i32 1
}

; Function Attrs: nounwind
define internal i32 @get_sof(%struct.jpeg_decompress_struct* %cinfo, i32 %is_prog, i32 %is_arith) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %is_prog.addr = alloca i32, align 4
  %is_arith.addr = alloca i32, align 4
  %length = alloca i32, align 4
  %c = alloca i32, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_mp = alloca i32*, align 4
  %_mp230 = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %is_prog, i32* %is_prog.addr, align 4, !tbaa !6
  store i32 %is_arith, i32* %is_arith.addr, align 4, !tbaa !6
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 6
  %6 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %6, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %7 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 0
  %9 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %9, i8** %next_input_byte, align 4, !tbaa !2
  %10 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 1
  %12 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %12, i32* %bytes_in_buffer, align 4, !tbaa !22
  %13 = load i32, i32* %is_prog.addr, align 4, !tbaa !6
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 45
  store i32 %13, i32* %progressive_mode, align 4, !tbaa !68
  %15 = load i32, i32* %is_arith.addr, align 4, !tbaa !6
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 46
  store i32 %15, i32* %arith_code, align 8, !tbaa !69
  br label %do.body

do.body:                                          ; preds = %entry
  %17 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %17, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %18 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %18, i32 0, i32 3
  %19 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %19(%struct.jpeg_decompress_struct* %20)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %21 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %21, i32 0, i32 0
  %22 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %22, i8** %next_input_byte, align 4, !tbaa !2
  %23 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %23, i32 0, i32 1
  %24 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %24, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %25 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %25, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %26 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %26, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %27 = load i8, i8* %26, align 1, !tbaa !16
  %conv = zext i8 %27 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %28 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %28, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %29 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %29, i32 0, i32 3
  %30 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %30(%struct.jpeg_decompress_struct* %31)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %32 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %32, i32 0, i32 0
  %33 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %33, i8** %next_input_byte, align 4, !tbaa !2
  %34 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %34, i32 0, i32 1
  %35 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %35, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %36 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %36, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %37 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %38 = load i8, i8* %37, align 1, !tbaa !16
  %conv20 = zext i8 %38 to i32
  %39 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %39, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body21

do.body21:                                        ; preds = %do.end
  %40 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp22 = icmp eq i32 %40, 0
  br i1 %cmp22, label %if.then24, label %if.end32

if.then24:                                        ; preds = %do.body21
  %41 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer25 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %41, i32 0, i32 3
  %42 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer25, align 4, !tbaa !23
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call26 = call i32 %42(%struct.jpeg_decompress_struct* %43)
  %tobool27 = icmp ne i32 %call26, 0
  br i1 %tobool27, label %if.end29, label %if.then28

if.then28:                                        ; preds = %if.then24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %if.then24
  %44 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte30 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %44, i32 0, i32 0
  %45 = load i8*, i8** %next_input_byte30, align 4, !tbaa !19
  store i8* %45, i8** %next_input_byte, align 4, !tbaa !2
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer31 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 1
  %47 = load i32, i32* %bytes_in_buffer31, align 4, !tbaa !21
  store i32 %47, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end32

if.end32:                                         ; preds = %if.end29, %do.body21
  %48 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec33 = add i32 %48, -1
  store i32 %dec33, i32* %bytes_in_buffer, align 4, !tbaa !22
  %49 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr34 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr34, i8** %next_input_byte, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !16
  %conv35 = zext i8 %50 to i32
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 43
  store i32 %conv35, i32* %data_precision, align 4, !tbaa !70
  br label %do.cond36

do.cond36:                                        ; preds = %if.end32
  br label %do.end37

do.end37:                                         ; preds = %do.cond36
  br label %do.body38

do.body38:                                        ; preds = %do.end37
  %52 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp39 = icmp eq i32 %52, 0
  br i1 %cmp39, label %if.then41, label %if.end49

if.then41:                                        ; preds = %do.body38
  %53 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer42 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %53, i32 0, i32 3
  %54 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer42, align 4, !tbaa !23
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call43 = call i32 %54(%struct.jpeg_decompress_struct* %55)
  %tobool44 = icmp ne i32 %call43, 0
  br i1 %tobool44, label %if.end46, label %if.then45

if.then45:                                        ; preds = %if.then41
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %if.then41
  %56 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte47 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %56, i32 0, i32 0
  %57 = load i8*, i8** %next_input_byte47, align 4, !tbaa !19
  store i8* %57, i8** %next_input_byte, align 4, !tbaa !2
  %58 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer48 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %58, i32 0, i32 1
  %59 = load i32, i32* %bytes_in_buffer48, align 4, !tbaa !21
  store i32 %59, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end49

if.end49:                                         ; preds = %if.end46, %do.body38
  %60 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec50 = add i32 %60, -1
  store i32 %dec50, i32* %bytes_in_buffer, align 4, !tbaa !22
  %61 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr51 = getelementptr inbounds i8, i8* %61, i32 1
  store i8* %incdec.ptr51, i8** %next_input_byte, align 4, !tbaa !2
  %62 = load i8, i8* %61, align 1, !tbaa !16
  %conv52 = zext i8 %62 to i32
  %shl53 = shl i32 %conv52, 8
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 8
  store i32 %shl53, i32* %image_height, align 8, !tbaa !71
  %64 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp54 = icmp eq i32 %64, 0
  br i1 %cmp54, label %if.then56, label %if.end64

if.then56:                                        ; preds = %if.end49
  %65 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer57 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %65, i32 0, i32 3
  %66 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer57, align 4, !tbaa !23
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call58 = call i32 %66(%struct.jpeg_decompress_struct* %67)
  %tobool59 = icmp ne i32 %call58, 0
  br i1 %tobool59, label %if.end61, label %if.then60

if.then60:                                        ; preds = %if.then56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end61:                                         ; preds = %if.then56
  %68 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte62 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %68, i32 0, i32 0
  %69 = load i8*, i8** %next_input_byte62, align 4, !tbaa !19
  store i8* %69, i8** %next_input_byte, align 4, !tbaa !2
  %70 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer63 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %70, i32 0, i32 1
  %71 = load i32, i32* %bytes_in_buffer63, align 4, !tbaa !21
  store i32 %71, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end64

if.end64:                                         ; preds = %if.end61, %if.end49
  %72 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec65 = add i32 %72, -1
  store i32 %dec65, i32* %bytes_in_buffer, align 4, !tbaa !22
  %73 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr66 = getelementptr inbounds i8, i8* %73, i32 1
  store i8* %incdec.ptr66, i8** %next_input_byte, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !16
  %conv67 = zext i8 %74 to i32
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 8
  %76 = load i32, i32* %image_height68, align 8, !tbaa !71
  %add69 = add i32 %76, %conv67
  store i32 %add69, i32* %image_height68, align 8, !tbaa !71
  br label %do.cond70

do.cond70:                                        ; preds = %if.end64
  br label %do.end71

do.end71:                                         ; preds = %do.cond70
  br label %do.body72

do.body72:                                        ; preds = %do.end71
  %77 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp73 = icmp eq i32 %77, 0
  br i1 %cmp73, label %if.then75, label %if.end83

if.then75:                                        ; preds = %do.body72
  %78 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer76 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %78, i32 0, i32 3
  %79 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer76, align 4, !tbaa !23
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call77 = call i32 %79(%struct.jpeg_decompress_struct* %80)
  %tobool78 = icmp ne i32 %call77, 0
  br i1 %tobool78, label %if.end80, label %if.then79

if.then79:                                        ; preds = %if.then75
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end80:                                         ; preds = %if.then75
  %81 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte81 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %81, i32 0, i32 0
  %82 = load i8*, i8** %next_input_byte81, align 4, !tbaa !19
  store i8* %82, i8** %next_input_byte, align 4, !tbaa !2
  %83 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer82 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %83, i32 0, i32 1
  %84 = load i32, i32* %bytes_in_buffer82, align 4, !tbaa !21
  store i32 %84, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end83

if.end83:                                         ; preds = %if.end80, %do.body72
  %85 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec84 = add i32 %85, -1
  store i32 %dec84, i32* %bytes_in_buffer, align 4, !tbaa !22
  %86 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr85 = getelementptr inbounds i8, i8* %86, i32 1
  store i8* %incdec.ptr85, i8** %next_input_byte, align 4, !tbaa !2
  %87 = load i8, i8* %86, align 1, !tbaa !16
  %conv86 = zext i8 %87 to i32
  %shl87 = shl i32 %conv86, 8
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 7
  store i32 %shl87, i32* %image_width, align 4, !tbaa !72
  %89 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp88 = icmp eq i32 %89, 0
  br i1 %cmp88, label %if.then90, label %if.end98

if.then90:                                        ; preds = %if.end83
  %90 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer91 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %90, i32 0, i32 3
  %91 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer91, align 4, !tbaa !23
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call92 = call i32 %91(%struct.jpeg_decompress_struct* %92)
  %tobool93 = icmp ne i32 %call92, 0
  br i1 %tobool93, label %if.end95, label %if.then94

if.then94:                                        ; preds = %if.then90
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end95:                                         ; preds = %if.then90
  %93 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte96 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %93, i32 0, i32 0
  %94 = load i8*, i8** %next_input_byte96, align 4, !tbaa !19
  store i8* %94, i8** %next_input_byte, align 4, !tbaa !2
  %95 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer97 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %95, i32 0, i32 1
  %96 = load i32, i32* %bytes_in_buffer97, align 4, !tbaa !21
  store i32 %96, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end98

if.end98:                                         ; preds = %if.end95, %if.end83
  %97 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec99 = add i32 %97, -1
  store i32 %dec99, i32* %bytes_in_buffer, align 4, !tbaa !22
  %98 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr100 = getelementptr inbounds i8, i8* %98, i32 1
  store i8* %incdec.ptr100, i8** %next_input_byte, align 4, !tbaa !2
  %99 = load i8, i8* %98, align 1, !tbaa !16
  %conv101 = zext i8 %99 to i32
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width102 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 7
  %101 = load i32, i32* %image_width102, align 4, !tbaa !72
  %add103 = add i32 %101, %conv101
  store i32 %add103, i32* %image_width102, align 4, !tbaa !72
  br label %do.cond104

do.cond104:                                       ; preds = %if.end98
  br label %do.end105

do.end105:                                        ; preds = %do.cond104
  br label %do.body106

do.body106:                                       ; preds = %do.end105
  %102 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp107 = icmp eq i32 %102, 0
  br i1 %cmp107, label %if.then109, label %if.end117

if.then109:                                       ; preds = %do.body106
  %103 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer110 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %103, i32 0, i32 3
  %104 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer110, align 4, !tbaa !23
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call111 = call i32 %104(%struct.jpeg_decompress_struct* %105)
  %tobool112 = icmp ne i32 %call111, 0
  br i1 %tobool112, label %if.end114, label %if.then113

if.then113:                                       ; preds = %if.then109
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end114:                                        ; preds = %if.then109
  %106 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte115 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %106, i32 0, i32 0
  %107 = load i8*, i8** %next_input_byte115, align 4, !tbaa !19
  store i8* %107, i8** %next_input_byte, align 4, !tbaa !2
  %108 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer116 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %108, i32 0, i32 1
  %109 = load i32, i32* %bytes_in_buffer116, align 4, !tbaa !21
  store i32 %109, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end117

if.end117:                                        ; preds = %if.end114, %do.body106
  %110 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec118 = add i32 %110, -1
  store i32 %dec118, i32* %bytes_in_buffer, align 4, !tbaa !22
  %111 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr119 = getelementptr inbounds i8, i8* %111, i32 1
  store i8* %incdec.ptr119, i8** %next_input_byte, align 4, !tbaa !2
  %112 = load i8, i8* %111, align 1, !tbaa !16
  %conv120 = zext i8 %112 to i32
  %113 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %113, i32 0, i32 9
  store i32 %conv120, i32* %num_components, align 4, !tbaa !73
  br label %do.cond121

do.cond121:                                       ; preds = %if.end117
  br label %do.end122

do.end122:                                        ; preds = %do.cond121
  %114 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %114, 8
  store i32 %sub, i32* %length, align 4, !tbaa !22
  br label %do.body123

do.body123:                                       ; preds = %do.end122
  %115 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #4
  %116 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %116, i32 0, i32 0
  %117 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %117, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %118 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %118, i32 0, i32 76
  %119 = load i32, i32* %unread_marker, align 8, !tbaa !8
  %120 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %120, i32 0
  store i32 %119, i32* %arrayidx, align 4, !tbaa !6
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width124 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 7
  %122 = load i32, i32* %image_width124, align 4, !tbaa !72
  %123 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %123, i32 1
  store i32 %122, i32* %arrayidx125, align 4, !tbaa !6
  %124 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height126 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %124, i32 0, i32 8
  %125 = load i32, i32* %image_height126, align 8, !tbaa !71
  %126 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %126, i32 2
  store i32 %125, i32* %arrayidx127, align 4, !tbaa !6
  %127 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components128 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %127, i32 0, i32 9
  %128 = load i32, i32* %num_components128, align 4, !tbaa !73
  %129 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %129, i32 3
  store i32 %128, i32* %arrayidx129, align 4, !tbaa !6
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err130 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 0
  %131 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err130, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %131, i32 0, i32 5
  store i32 100, i32* %msg_code, align 4, !tbaa !13
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err131 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %132, i32 0, i32 0
  %133 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err131, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %133, i32 0, i32 1
  %134 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %136 = bitcast %struct.jpeg_decompress_struct* %135 to %struct.jpeg_common_struct*
  call void %134(%struct.jpeg_common_struct* %136, i32 1)
  %137 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  br label %do.cond132

do.cond132:                                       ; preds = %do.body123
  br label %do.end133

do.end133:                                        ; preds = %do.cond132
  %138 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %138, i32 0, i32 82
  %139 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %saw_SOF = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %139, i32 0, i32 4
  %140 = load i32, i32* %saw_SOF, align 4, !tbaa !74
  %tobool134 = icmp ne i32 %140, 0
  br i1 %tobool134, label %if.then135, label %if.end139

if.then135:                                       ; preds = %do.end133
  %141 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err136 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %141, i32 0, i32 0
  %142 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err136, align 8, !tbaa !12
  %msg_code137 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %142, i32 0, i32 5
  store i32 58, i32* %msg_code137, align 4, !tbaa !13
  %143 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err138 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %143, i32 0, i32 0
  %144 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err138, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %144, i32 0, i32 0
  %145 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %147 = bitcast %struct.jpeg_decompress_struct* %146 to %struct.jpeg_common_struct*
  call void %145(%struct.jpeg_common_struct* %147)
  br label %if.end139

if.end139:                                        ; preds = %if.then135, %do.end133
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height140 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 8
  %149 = load i32, i32* %image_height140, align 8, !tbaa !71
  %cmp141 = icmp ule i32 %149, 0
  br i1 %cmp141, label %if.then150, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end139
  %150 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width143 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %150, i32 0, i32 7
  %151 = load i32, i32* %image_width143, align 4, !tbaa !72
  %cmp144 = icmp ule i32 %151, 0
  br i1 %cmp144, label %if.then150, label %lor.lhs.false146

lor.lhs.false146:                                 ; preds = %lor.lhs.false
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components147 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 9
  %153 = load i32, i32* %num_components147, align 4, !tbaa !73
  %cmp148 = icmp sle i32 %153, 0
  br i1 %cmp148, label %if.then150, label %if.end155

if.then150:                                       ; preds = %lor.lhs.false146, %lor.lhs.false, %if.end139
  %154 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err151 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %154, i32 0, i32 0
  %155 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err151, align 8, !tbaa !12
  %msg_code152 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %155, i32 0, i32 5
  store i32 32, i32* %msg_code152, align 4, !tbaa !13
  %156 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err153 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %156, i32 0, i32 0
  %157 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err153, align 8, !tbaa !12
  %error_exit154 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %157, i32 0, i32 0
  %158 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit154, align 4, !tbaa !43
  %159 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %160 = bitcast %struct.jpeg_decompress_struct* %159 to %struct.jpeg_common_struct*
  call void %158(%struct.jpeg_common_struct* %160)
  br label %if.end155

if.end155:                                        ; preds = %if.then150, %lor.lhs.false146
  %161 = load i32, i32* %length, align 4, !tbaa !22
  %162 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components156 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %162, i32 0, i32 9
  %163 = load i32, i32* %num_components156, align 4, !tbaa !73
  %mul = mul nsw i32 %163, 3
  %cmp157 = icmp ne i32 %161, %mul
  br i1 %cmp157, label %if.then159, label %if.end164

if.then159:                                       ; preds = %if.end155
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err160 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 0
  %165 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err160, align 8, !tbaa !12
  %msg_code161 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %165, i32 0, i32 5
  store i32 11, i32* %msg_code161, align 4, !tbaa !13
  %166 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err162 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %166, i32 0, i32 0
  %167 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err162, align 8, !tbaa !12
  %error_exit163 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %167, i32 0, i32 0
  %168 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit163, align 4, !tbaa !43
  %169 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %170 = bitcast %struct.jpeg_decompress_struct* %169 to %struct.jpeg_common_struct*
  call void %168(%struct.jpeg_common_struct* %170)
  br label %if.end164

if.end164:                                        ; preds = %if.then159, %if.end155
  %171 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %171, i32 0, i32 44
  %172 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !36
  %cmp165 = icmp eq %struct.jpeg_component_info* %172, null
  br i1 %cmp165, label %if.then167, label %if.end172

if.then167:                                       ; preds = %if.end164
  %173 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %173, i32 0, i32 1
  %174 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !27
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %174, i32 0, i32 0
  %175 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !28
  %176 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %177 = bitcast %struct.jpeg_decompress_struct* %176 to %struct.jpeg_common_struct*
  %178 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components168 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %178, i32 0, i32 9
  %179 = load i32, i32* %num_components168, align 4, !tbaa !73
  %mul169 = mul i32 %179, 84
  %call170 = call i8* %175(%struct.jpeg_common_struct* %177, i32 1, i32 %mul169)
  %180 = bitcast i8* %call170 to %struct.jpeg_component_info*
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info171 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 44
  store %struct.jpeg_component_info* %180, %struct.jpeg_component_info** %comp_info171, align 8, !tbaa !36
  br label %if.end172

if.end172:                                        ; preds = %if.then167, %if.end164
  store i32 0, i32* %ci, align 4, !tbaa !6
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info173 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 44
  %183 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info173, align 8, !tbaa !36
  store %struct.jpeg_component_info* %183, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end172
  %184 = load i32, i32* %ci, align 4, !tbaa !6
  %185 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components174 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %185, i32 0, i32 9
  %186 = load i32, i32* %num_components174, align 4, !tbaa !73
  %cmp175 = icmp slt i32 %184, %186
  br i1 %cmp175, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %187 = load i32, i32* %ci, align 4, !tbaa !6
  %188 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %188, i32 0, i32 1
  store i32 %187, i32* %component_index, align 4, !tbaa !75
  br label %do.body177

do.body177:                                       ; preds = %for.body
  %189 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp178 = icmp eq i32 %189, 0
  br i1 %cmp178, label %if.then180, label %if.end188

if.then180:                                       ; preds = %do.body177
  %190 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer181 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %190, i32 0, i32 3
  %191 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer181, align 4, !tbaa !23
  %192 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call182 = call i32 %191(%struct.jpeg_decompress_struct* %192)
  %tobool183 = icmp ne i32 %call182, 0
  br i1 %tobool183, label %if.end185, label %if.then184

if.then184:                                       ; preds = %if.then180
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end185:                                        ; preds = %if.then180
  %193 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte186 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %193, i32 0, i32 0
  %194 = load i8*, i8** %next_input_byte186, align 4, !tbaa !19
  store i8* %194, i8** %next_input_byte, align 4, !tbaa !2
  %195 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer187 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %195, i32 0, i32 1
  %196 = load i32, i32* %bytes_in_buffer187, align 4, !tbaa !21
  store i32 %196, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end188

if.end188:                                        ; preds = %if.end185, %do.body177
  %197 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec189 = add i32 %197, -1
  store i32 %dec189, i32* %bytes_in_buffer, align 4, !tbaa !22
  %198 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr190 = getelementptr inbounds i8, i8* %198, i32 1
  store i8* %incdec.ptr190, i8** %next_input_byte, align 4, !tbaa !2
  %199 = load i8, i8* %198, align 1, !tbaa !16
  %conv191 = zext i8 %199 to i32
  %200 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %200, i32 0, i32 0
  store i32 %conv191, i32* %component_id, align 4, !tbaa !77
  br label %do.cond192

do.cond192:                                       ; preds = %if.end188
  br label %do.end193

do.end193:                                        ; preds = %do.cond192
  br label %do.body194

do.body194:                                       ; preds = %do.end193
  %201 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp195 = icmp eq i32 %201, 0
  br i1 %cmp195, label %if.then197, label %if.end205

if.then197:                                       ; preds = %do.body194
  %202 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer198 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %202, i32 0, i32 3
  %203 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer198, align 4, !tbaa !23
  %204 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call199 = call i32 %203(%struct.jpeg_decompress_struct* %204)
  %tobool200 = icmp ne i32 %call199, 0
  br i1 %tobool200, label %if.end202, label %if.then201

if.then201:                                       ; preds = %if.then197
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end202:                                        ; preds = %if.then197
  %205 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte203 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %205, i32 0, i32 0
  %206 = load i8*, i8** %next_input_byte203, align 4, !tbaa !19
  store i8* %206, i8** %next_input_byte, align 4, !tbaa !2
  %207 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer204 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %207, i32 0, i32 1
  %208 = load i32, i32* %bytes_in_buffer204, align 4, !tbaa !21
  store i32 %208, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end205

if.end205:                                        ; preds = %if.end202, %do.body194
  %209 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec206 = add i32 %209, -1
  store i32 %dec206, i32* %bytes_in_buffer, align 4, !tbaa !22
  %210 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr207 = getelementptr inbounds i8, i8* %210, i32 1
  store i8* %incdec.ptr207, i8** %next_input_byte, align 4, !tbaa !2
  %211 = load i8, i8* %210, align 1, !tbaa !16
  %conv208 = zext i8 %211 to i32
  store i32 %conv208, i32* %c, align 4, !tbaa !6
  br label %do.cond209

do.cond209:                                       ; preds = %if.end205
  br label %do.end210

do.end210:                                        ; preds = %do.cond209
  %212 = load i32, i32* %c, align 4, !tbaa !6
  %shr = ashr i32 %212, 4
  %and = and i32 %shr, 15
  %213 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %213, i32 0, i32 2
  store i32 %and, i32* %h_samp_factor, align 4, !tbaa !78
  %214 = load i32, i32* %c, align 4, !tbaa !6
  %and211 = and i32 %214, 15
  %215 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %215, i32 0, i32 3
  store i32 %and211, i32* %v_samp_factor, align 4, !tbaa !79
  br label %do.body212

do.body212:                                       ; preds = %do.end210
  %216 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp213 = icmp eq i32 %216, 0
  br i1 %cmp213, label %if.then215, label %if.end223

if.then215:                                       ; preds = %do.body212
  %217 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer216 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %217, i32 0, i32 3
  %218 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer216, align 4, !tbaa !23
  %219 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call217 = call i32 %218(%struct.jpeg_decompress_struct* %219)
  %tobool218 = icmp ne i32 %call217, 0
  br i1 %tobool218, label %if.end220, label %if.then219

if.then219:                                       ; preds = %if.then215
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end220:                                        ; preds = %if.then215
  %220 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte221 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %220, i32 0, i32 0
  %221 = load i8*, i8** %next_input_byte221, align 4, !tbaa !19
  store i8* %221, i8** %next_input_byte, align 4, !tbaa !2
  %222 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer222 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %222, i32 0, i32 1
  %223 = load i32, i32* %bytes_in_buffer222, align 4, !tbaa !21
  store i32 %223, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end223

if.end223:                                        ; preds = %if.end220, %do.body212
  %224 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec224 = add i32 %224, -1
  store i32 %dec224, i32* %bytes_in_buffer, align 4, !tbaa !22
  %225 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr225 = getelementptr inbounds i8, i8* %225, i32 1
  store i8* %incdec.ptr225, i8** %next_input_byte, align 4, !tbaa !2
  %226 = load i8, i8* %225, align 1, !tbaa !16
  %conv226 = zext i8 %226 to i32
  %227 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %227, i32 0, i32 4
  store i32 %conv226, i32* %quant_tbl_no, align 4, !tbaa !80
  br label %do.cond227

do.cond227:                                       ; preds = %if.end223
  br label %do.end228

do.end228:                                        ; preds = %do.cond227
  br label %do.body229

do.body229:                                       ; preds = %do.end228
  %228 = bitcast i32** %_mp230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #4
  %229 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err231 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %229, i32 0, i32 0
  %230 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err231, align 8, !tbaa !12
  %msg_parm232 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %230, i32 0, i32 6
  %i233 = bitcast %union.anon* %msg_parm232 to [8 x i32]*
  %arraydecay234 = getelementptr inbounds [8 x i32], [8 x i32]* %i233, i32 0, i32 0
  store i32* %arraydecay234, i32** %_mp230, align 4, !tbaa !2
  %231 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id235 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %231, i32 0, i32 0
  %232 = load i32, i32* %component_id235, align 4, !tbaa !77
  %233 = load i32*, i32** %_mp230, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %233, i32 0
  store i32 %232, i32* %arrayidx236, align 4, !tbaa !6
  %234 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor237 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %234, i32 0, i32 2
  %235 = load i32, i32* %h_samp_factor237, align 4, !tbaa !78
  %236 = load i32*, i32** %_mp230, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds i32, i32* %236, i32 1
  store i32 %235, i32* %arrayidx238, align 4, !tbaa !6
  %237 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor239 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %237, i32 0, i32 3
  %238 = load i32, i32* %v_samp_factor239, align 4, !tbaa !79
  %239 = load i32*, i32** %_mp230, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i32, i32* %239, i32 2
  store i32 %238, i32* %arrayidx240, align 4, !tbaa !6
  %240 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no241 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %240, i32 0, i32 4
  %241 = load i32, i32* %quant_tbl_no241, align 4, !tbaa !80
  %242 = load i32*, i32** %_mp230, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds i32, i32* %242, i32 3
  store i32 %241, i32* %arrayidx242, align 4, !tbaa !6
  %243 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err243 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %243, i32 0, i32 0
  %244 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err243, align 8, !tbaa !12
  %msg_code244 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %244, i32 0, i32 5
  store i32 101, i32* %msg_code244, align 4, !tbaa !13
  %245 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err245 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %245, i32 0, i32 0
  %246 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err245, align 8, !tbaa !12
  %emit_message246 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %246, i32 0, i32 1
  %247 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message246, align 4, !tbaa !17
  %248 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %249 = bitcast %struct.jpeg_decompress_struct* %248 to %struct.jpeg_common_struct*
  call void %247(%struct.jpeg_common_struct* %249, i32 1)
  %250 = bitcast i32** %_mp230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #4
  br label %do.cond247

do.cond247:                                       ; preds = %do.body229
  br label %do.end248

do.end248:                                        ; preds = %do.cond247
  br label %for.inc

for.inc:                                          ; preds = %do.end248
  %251 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %251, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %252 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr249 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %252, i32 1
  store %struct.jpeg_component_info* %incdec.ptr249, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %253 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker250 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %253, i32 0, i32 82
  %254 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker250, align 8, !tbaa !24
  %saw_SOF251 = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %254, i32 0, i32 4
  store i32 1, i32* %saw_SOF251, align 4, !tbaa !74
  %255 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %256 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte252 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %256, i32 0, i32 0
  store i8* %255, i8** %next_input_byte252, align 4, !tbaa !19
  %257 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %258 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer253 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %258, i32 0, i32 1
  store i32 %257, i32* %bytes_in_buffer253, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then219, %if.then201, %if.then184, %if.then113, %if.then94, %if.then79, %if.then60, %if.then45, %if.then28, %if.then13, %if.then3
  %259 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #4
  %260 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #4
  %261 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #4
  %262 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #4
  %263 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  %264 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  %265 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #4
  %266 = load i32, i32* %retval, align 4
  ret i32 %266
}

; Function Attrs: nounwind
define internal i32 @get_sos(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %i = alloca i32, align 4
  %ci = alloca i32, align 4
  %n = alloca i32, align 4
  %c = alloca i32, align 4
  %cc = alloca i32, align 4
  %pi = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_mp = alloca i32*, align 4
  %_mp224 = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %pi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 6
  %10 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %10, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %11 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %12, i32 0, i32 0
  %13 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %13, i8** %next_input_byte, align 4, !tbaa !2
  %14 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 1
  %16 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %16, i32* %bytes_in_buffer, align 4, !tbaa !22
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 82
  %18 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !24
  %saw_SOF = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %18, i32 0, i32 4
  %19 = load i32, i32* %saw_SOF, align 4, !tbaa !74
  %tobool = icmp ne i32 %19, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 5
  store i32 62, i32* %msg_code, align 4, !tbaa !13
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 0
  %24 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %26 = bitcast %struct.jpeg_decompress_struct* %25 to %struct.jpeg_common_struct*
  call void %24(%struct.jpeg_common_struct* %26)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  br label %do.body

do.body:                                          ; preds = %if.end
  %27 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %27, 0
  br i1 %cmp, label %if.then4, label %if.end10

if.then4:                                         ; preds = %do.body
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 3
  %29 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %29(%struct.jpeg_decompress_struct* %30)
  %tobool5 = icmp ne i32 %call, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.then4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.then4
  %31 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte8 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %31, i32 0, i32 0
  %32 = load i8*, i8** %next_input_byte8, align 4, !tbaa !19
  store i8* %32, i8** %next_input_byte, align 4, !tbaa !2
  %33 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer9 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %33, i32 0, i32 1
  %34 = load i32, i32* %bytes_in_buffer9, align 4, !tbaa !21
  store i32 %34, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end10

if.end10:                                         ; preds = %if.end7, %do.body
  %35 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %35, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %36 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %37 = load i8, i8* %36, align 1, !tbaa !16
  %conv = zext i8 %37 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %38 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp11 = icmp eq i32 %38, 0
  br i1 %cmp11, label %if.then13, label %if.end21

if.then13:                                        ; preds = %if.end10
  %39 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer14 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %39, i32 0, i32 3
  %40 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer14, align 4, !tbaa !23
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call15 = call i32 %40(%struct.jpeg_decompress_struct* %41)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.then13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %if.then13
  %42 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte19 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %42, i32 0, i32 0
  %43 = load i8*, i8** %next_input_byte19, align 4, !tbaa !19
  store i8* %43, i8** %next_input_byte, align 4, !tbaa !2
  %44 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer20 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %44, i32 0, i32 1
  %45 = load i32, i32* %bytes_in_buffer20, align 4, !tbaa !21
  store i32 %45, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end21

if.end21:                                         ; preds = %if.end18, %if.end10
  %46 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec22 = add i32 %46, -1
  store i32 %dec22, i32* %bytes_in_buffer, align 4, !tbaa !22
  %47 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr23 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr23, i8** %next_input_byte, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !16
  %conv24 = zext i8 %48 to i32
  %49 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %49, %conv24
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end21
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body25

do.body25:                                        ; preds = %do.end
  %50 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp26 = icmp eq i32 %50, 0
  br i1 %cmp26, label %if.then28, label %if.end36

if.then28:                                        ; preds = %do.body25
  %51 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer29 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %51, i32 0, i32 3
  %52 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer29, align 4, !tbaa !23
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call30 = call i32 %52(%struct.jpeg_decompress_struct* %53)
  %tobool31 = icmp ne i32 %call30, 0
  br i1 %tobool31, label %if.end33, label %if.then32

if.then32:                                        ; preds = %if.then28
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end33:                                         ; preds = %if.then28
  %54 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte34 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %54, i32 0, i32 0
  %55 = load i8*, i8** %next_input_byte34, align 4, !tbaa !19
  store i8* %55, i8** %next_input_byte, align 4, !tbaa !2
  %56 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer35 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %56, i32 0, i32 1
  %57 = load i32, i32* %bytes_in_buffer35, align 4, !tbaa !21
  store i32 %57, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end36

if.end36:                                         ; preds = %if.end33, %do.body25
  %58 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec37 = add i32 %58, -1
  store i32 %dec37, i32* %bytes_in_buffer, align 4, !tbaa !22
  %59 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr38, i8** %next_input_byte, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !16
  %conv39 = zext i8 %60 to i32
  store i32 %conv39, i32* %n, align 4, !tbaa !6
  br label %do.cond40

do.cond40:                                        ; preds = %if.end36
  br label %do.end41

do.end41:                                         ; preds = %do.cond40
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !12
  %msg_code43 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 5
  store i32 103, i32* %msg_code43, align 4, !tbaa !13
  %63 = load i32, i32* %n, align 4, !tbaa !6
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err44 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 0
  %65 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err44, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %65, i32 0, i32 6
  %i45 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i45, i32 0, i32 0
  store i32 %63, i32* %arrayidx, align 4, !tbaa !16
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 0
  %67 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %67, i32 0, i32 1
  %68 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %70 = bitcast %struct.jpeg_decompress_struct* %69 to %struct.jpeg_common_struct*
  call void %68(%struct.jpeg_common_struct* %70, i32 1)
  %71 = load i32, i32* %length, align 4, !tbaa !22
  %72 = load i32, i32* %n, align 4, !tbaa !6
  %mul = mul nsw i32 %72, 2
  %add47 = add nsw i32 %mul, 6
  %cmp48 = icmp ne i32 %71, %add47
  br i1 %cmp48, label %if.then55, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.end41
  %73 = load i32, i32* %n, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %73, 1
  br i1 %cmp50, label %if.then55, label %lor.lhs.false52

lor.lhs.false52:                                  ; preds = %lor.lhs.false
  %74 = load i32, i32* %n, align 4, !tbaa !6
  %cmp53 = icmp sgt i32 %74, 4
  br i1 %cmp53, label %if.then55, label %if.end60

if.then55:                                        ; preds = %lor.lhs.false52, %lor.lhs.false, %do.end41
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 0
  %76 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err56, align 8, !tbaa !12
  %msg_code57 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %76, i32 0, i32 5
  store i32 11, i32* %msg_code57, align 4, !tbaa !13
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 0
  %78 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err58, align 8, !tbaa !12
  %error_exit59 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %78, i32 0, i32 0
  %79 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit59, align 4, !tbaa !43
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %81 = bitcast %struct.jpeg_decompress_struct* %80 to %struct.jpeg_common_struct*
  call void %79(%struct.jpeg_common_struct* %81)
  br label %if.end60

if.end60:                                         ; preds = %if.then55, %lor.lhs.false52
  %82 = load i32, i32* %n, align 4, !tbaa !6
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 66
  store i32 %82, i32* %comps_in_scan, align 8, !tbaa !81
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end60
  %84 = load i32, i32* %i, align 4, !tbaa !6
  %cmp61 = icmp slt i32 %84, 4
  br i1 %cmp61, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 67
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %86
  store %struct.jpeg_component_info* null, %struct.jpeg_component_info** %arrayidx63, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %87, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc166, %for.end
  %88 = load i32, i32* %i, align 4, !tbaa !6
  %89 = load i32, i32* %n, align 4, !tbaa !6
  %cmp65 = icmp slt i32 %88, %89
  br i1 %cmp65, label %for.body67, label %for.end168

for.body67:                                       ; preds = %for.cond64
  br label %do.body68

do.body68:                                        ; preds = %for.body67
  %90 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp69 = icmp eq i32 %90, 0
  br i1 %cmp69, label %if.then71, label %if.end79

if.then71:                                        ; preds = %do.body68
  %91 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer72 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %91, i32 0, i32 3
  %92 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer72, align 4, !tbaa !23
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call73 = call i32 %92(%struct.jpeg_decompress_struct* %93)
  %tobool74 = icmp ne i32 %call73, 0
  br i1 %tobool74, label %if.end76, label %if.then75

if.then75:                                        ; preds = %if.then71
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end76:                                         ; preds = %if.then71
  %94 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte77 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %94, i32 0, i32 0
  %95 = load i8*, i8** %next_input_byte77, align 4, !tbaa !19
  store i8* %95, i8** %next_input_byte, align 4, !tbaa !2
  %96 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer78 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %96, i32 0, i32 1
  %97 = load i32, i32* %bytes_in_buffer78, align 4, !tbaa !21
  store i32 %97, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end79

if.end79:                                         ; preds = %if.end76, %do.body68
  %98 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec80 = add i32 %98, -1
  store i32 %dec80, i32* %bytes_in_buffer, align 4, !tbaa !22
  %99 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr81 = getelementptr inbounds i8, i8* %99, i32 1
  store i8* %incdec.ptr81, i8** %next_input_byte, align 4, !tbaa !2
  %100 = load i8, i8* %99, align 1, !tbaa !16
  %conv82 = zext i8 %100 to i32
  store i32 %conv82, i32* %cc, align 4, !tbaa !6
  br label %do.cond83

do.cond83:                                        ; preds = %if.end79
  br label %do.end84

do.end84:                                         ; preds = %do.cond83
  br label %do.body85

do.body85:                                        ; preds = %do.end84
  %101 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp86 = icmp eq i32 %101, 0
  br i1 %cmp86, label %if.then88, label %if.end96

if.then88:                                        ; preds = %do.body85
  %102 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer89 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %102, i32 0, i32 3
  %103 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer89, align 4, !tbaa !23
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call90 = call i32 %103(%struct.jpeg_decompress_struct* %104)
  %tobool91 = icmp ne i32 %call90, 0
  br i1 %tobool91, label %if.end93, label %if.then92

if.then92:                                        ; preds = %if.then88
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end93:                                         ; preds = %if.then88
  %105 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte94 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %105, i32 0, i32 0
  %106 = load i8*, i8** %next_input_byte94, align 4, !tbaa !19
  store i8* %106, i8** %next_input_byte, align 4, !tbaa !2
  %107 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer95 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %107, i32 0, i32 1
  %108 = load i32, i32* %bytes_in_buffer95, align 4, !tbaa !21
  store i32 %108, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end96

if.end96:                                         ; preds = %if.end93, %do.body85
  %109 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec97 = add i32 %109, -1
  store i32 %dec97, i32* %bytes_in_buffer, align 4, !tbaa !22
  %110 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr98 = getelementptr inbounds i8, i8* %110, i32 1
  store i8* %incdec.ptr98, i8** %next_input_byte, align 4, !tbaa !2
  %111 = load i8, i8* %110, align 1, !tbaa !16
  %conv99 = zext i8 %111 to i32
  store i32 %conv99, i32* %c, align 4, !tbaa !6
  br label %do.cond100

do.cond100:                                       ; preds = %if.end96
  br label %do.end101

do.end101:                                        ; preds = %do.cond100
  store i32 0, i32* %ci, align 4, !tbaa !6
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 44
  %113 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !36
  store %struct.jpeg_component_info* %113, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc115, %do.end101
  %114 = load i32, i32* %ci, align 4, !tbaa !6
  %115 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %115, i32 0, i32 9
  %116 = load i32, i32* %num_components, align 4, !tbaa !73
  %cmp103 = icmp slt i32 %114, %116
  br i1 %cmp103, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond102
  %117 = load i32, i32* %ci, align 4, !tbaa !6
  %cmp105 = icmp slt i32 %117, 4
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond102
  %118 = phi i1 [ false, %for.cond102 ], [ %cmp105, %land.rhs ]
  br i1 %118, label %for.body107, label %for.end118

for.body107:                                      ; preds = %land.end
  %119 = load i32, i32* %cc, align 4, !tbaa !6
  %120 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %120, i32 0, i32 0
  %121 = load i32, i32* %component_id, align 4, !tbaa !77
  %cmp108 = icmp eq i32 %119, %121
  br i1 %cmp108, label %land.lhs.true, label %if.end114

land.lhs.true:                                    ; preds = %for.body107
  %122 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info110 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %122, i32 0, i32 67
  %123 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info110, i32 0, i32 %123
  %124 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx111, align 4, !tbaa !2
  %tobool112 = icmp ne %struct.jpeg_component_info* %124, null
  br i1 %tobool112, label %if.end114, label %if.then113

if.then113:                                       ; preds = %land.lhs.true
  br label %id_found

if.end114:                                        ; preds = %land.lhs.true, %for.body107
  br label %for.inc115

for.inc115:                                       ; preds = %if.end114
  %125 = load i32, i32* %ci, align 4, !tbaa !6
  %inc116 = add nsw i32 %125, 1
  store i32 %inc116, i32* %ci, align 4, !tbaa !6
  %126 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr117 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %126, i32 1
  store %struct.jpeg_component_info* %incdec.ptr117, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond102

for.end118:                                       ; preds = %land.end
  %127 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err119 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %127, i32 0, i32 0
  %128 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err119, align 8, !tbaa !12
  %msg_code120 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %128, i32 0, i32 5
  store i32 5, i32* %msg_code120, align 4, !tbaa !13
  %129 = load i32, i32* %cc, align 4, !tbaa !6
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err121 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 0
  %131 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err121, align 8, !tbaa !12
  %msg_parm122 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %131, i32 0, i32 6
  %i123 = bitcast %union.anon* %msg_parm122 to [8 x i32]*
  %arrayidx124 = getelementptr inbounds [8 x i32], [8 x i32]* %i123, i32 0, i32 0
  store i32 %129, i32* %arrayidx124, align 4, !tbaa !16
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err125 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %132, i32 0, i32 0
  %133 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err125, align 8, !tbaa !12
  %error_exit126 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %133, i32 0, i32 0
  %134 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit126, align 4, !tbaa !43
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %136 = bitcast %struct.jpeg_decompress_struct* %135 to %struct.jpeg_common_struct*
  call void %134(%struct.jpeg_common_struct* %136)
  br label %id_found

id_found:                                         ; preds = %for.end118, %if.then113
  %137 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %138 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info127 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %138, i32 0, i32 67
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx128 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info127, i32 0, i32 %139
  store %struct.jpeg_component_info* %137, %struct.jpeg_component_info** %arrayidx128, align 4, !tbaa !2
  %140 = load i32, i32* %c, align 4, !tbaa !6
  %shr = ashr i32 %140, 4
  %and = and i32 %shr, 15
  %141 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %141, i32 0, i32 5
  store i32 %and, i32* %dc_tbl_no, align 4, !tbaa !82
  %142 = load i32, i32* %c, align 4, !tbaa !6
  %and129 = and i32 %142, 15
  %143 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %143, i32 0, i32 6
  store i32 %and129, i32* %ac_tbl_no, align 4, !tbaa !83
  br label %do.body130

do.body130:                                       ; preds = %id_found
  %144 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #4
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err131 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %145, i32 0, i32 0
  %146 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err131, align 8, !tbaa !12
  %msg_parm132 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %146, i32 0, i32 6
  %i133 = bitcast %union.anon* %msg_parm132 to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i133, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %147 = load i32, i32* %cc, align 4, !tbaa !6
  %148 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds i32, i32* %148, i32 0
  store i32 %147, i32* %arrayidx134, align 4, !tbaa !6
  %149 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no135 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %149, i32 0, i32 5
  %150 = load i32, i32* %dc_tbl_no135, align 4, !tbaa !82
  %151 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %151, i32 1
  store i32 %150, i32* %arrayidx136, align 4, !tbaa !6
  %152 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no137 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %152, i32 0, i32 6
  %153 = load i32, i32* %ac_tbl_no137, align 4, !tbaa !83
  %154 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i32, i32* %154, i32 2
  store i32 %153, i32* %arrayidx138, align 4, !tbaa !6
  %155 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err139 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %155, i32 0, i32 0
  %156 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err139, align 8, !tbaa !12
  %msg_code140 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %156, i32 0, i32 5
  store i32 104, i32* %msg_code140, align 4, !tbaa !13
  %157 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err141 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %157, i32 0, i32 0
  %158 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err141, align 8, !tbaa !12
  %emit_message142 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %158, i32 0, i32 1
  %159 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message142, align 4, !tbaa !17
  %160 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %161 = bitcast %struct.jpeg_decompress_struct* %160 to %struct.jpeg_common_struct*
  call void %159(%struct.jpeg_common_struct* %161, i32 1)
  %162 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  br label %do.cond143

do.cond143:                                       ; preds = %do.body130
  br label %do.end144

do.end144:                                        ; preds = %do.cond143
  store i32 0, i32* %pi, align 4, !tbaa !6
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc163, %do.end144
  %163 = load i32, i32* %pi, align 4, !tbaa !6
  %164 = load i32, i32* %i, align 4, !tbaa !6
  %cmp146 = icmp slt i32 %163, %164
  br i1 %cmp146, label %for.body148, label %for.end165

for.body148:                                      ; preds = %for.cond145
  %165 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info149 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %165, i32 0, i32 67
  %166 = load i32, i32* %pi, align 4, !tbaa !6
  %arrayidx150 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info149, i32 0, i32 %166
  %167 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx150, align 4, !tbaa !2
  %168 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %cmp151 = icmp eq %struct.jpeg_component_info* %167, %168
  br i1 %cmp151, label %if.then153, label %if.end162

if.then153:                                       ; preds = %for.body148
  %169 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err154 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %169, i32 0, i32 0
  %170 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err154, align 8, !tbaa !12
  %msg_code155 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %170, i32 0, i32 5
  store i32 5, i32* %msg_code155, align 4, !tbaa !13
  %171 = load i32, i32* %cc, align 4, !tbaa !6
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err156 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %172, i32 0, i32 0
  %173 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err156, align 8, !tbaa !12
  %msg_parm157 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %173, i32 0, i32 6
  %i158 = bitcast %union.anon* %msg_parm157 to [8 x i32]*
  %arrayidx159 = getelementptr inbounds [8 x i32], [8 x i32]* %i158, i32 0, i32 0
  store i32 %171, i32* %arrayidx159, align 4, !tbaa !16
  %174 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err160 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %174, i32 0, i32 0
  %175 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err160, align 8, !tbaa !12
  %error_exit161 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %175, i32 0, i32 0
  %176 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit161, align 4, !tbaa !43
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %178 = bitcast %struct.jpeg_decompress_struct* %177 to %struct.jpeg_common_struct*
  call void %176(%struct.jpeg_common_struct* %178)
  br label %if.end162

if.end162:                                        ; preds = %if.then153, %for.body148
  br label %for.inc163

for.inc163:                                       ; preds = %if.end162
  %179 = load i32, i32* %pi, align 4, !tbaa !6
  %inc164 = add nsw i32 %179, 1
  store i32 %inc164, i32* %pi, align 4, !tbaa !6
  br label %for.cond145

for.end165:                                       ; preds = %for.cond145
  br label %for.inc166

for.inc166:                                       ; preds = %for.end165
  %180 = load i32, i32* %i, align 4, !tbaa !6
  %inc167 = add nsw i32 %180, 1
  store i32 %inc167, i32* %i, align 4, !tbaa !6
  br label %for.cond64

for.end168:                                       ; preds = %for.cond64
  br label %do.body169

do.body169:                                       ; preds = %for.end168
  %181 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp170 = icmp eq i32 %181, 0
  br i1 %cmp170, label %if.then172, label %if.end180

if.then172:                                       ; preds = %do.body169
  %182 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer173 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %182, i32 0, i32 3
  %183 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer173, align 4, !tbaa !23
  %184 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call174 = call i32 %183(%struct.jpeg_decompress_struct* %184)
  %tobool175 = icmp ne i32 %call174, 0
  br i1 %tobool175, label %if.end177, label %if.then176

if.then176:                                       ; preds = %if.then172
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end177:                                        ; preds = %if.then172
  %185 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte178 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %185, i32 0, i32 0
  %186 = load i8*, i8** %next_input_byte178, align 4, !tbaa !19
  store i8* %186, i8** %next_input_byte, align 4, !tbaa !2
  %187 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer179 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %187, i32 0, i32 1
  %188 = load i32, i32* %bytes_in_buffer179, align 4, !tbaa !21
  store i32 %188, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end180

if.end180:                                        ; preds = %if.end177, %do.body169
  %189 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec181 = add i32 %189, -1
  store i32 %dec181, i32* %bytes_in_buffer, align 4, !tbaa !22
  %190 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr182 = getelementptr inbounds i8, i8* %190, i32 1
  store i8* %incdec.ptr182, i8** %next_input_byte, align 4, !tbaa !2
  %191 = load i8, i8* %190, align 1, !tbaa !16
  %conv183 = zext i8 %191 to i32
  store i32 %conv183, i32* %c, align 4, !tbaa !6
  br label %do.cond184

do.cond184:                                       ; preds = %if.end180
  br label %do.end185

do.end185:                                        ; preds = %do.cond184
  %192 = load i32, i32* %c, align 4, !tbaa !6
  %193 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %193, i32 0, i32 72
  store i32 %192, i32* %Ss, align 8, !tbaa !84
  br label %do.body186

do.body186:                                       ; preds = %do.end185
  %194 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp187 = icmp eq i32 %194, 0
  br i1 %cmp187, label %if.then189, label %if.end197

if.then189:                                       ; preds = %do.body186
  %195 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer190 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %195, i32 0, i32 3
  %196 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer190, align 4, !tbaa !23
  %197 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call191 = call i32 %196(%struct.jpeg_decompress_struct* %197)
  %tobool192 = icmp ne i32 %call191, 0
  br i1 %tobool192, label %if.end194, label %if.then193

if.then193:                                       ; preds = %if.then189
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end194:                                        ; preds = %if.then189
  %198 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte195 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %198, i32 0, i32 0
  %199 = load i8*, i8** %next_input_byte195, align 4, !tbaa !19
  store i8* %199, i8** %next_input_byte, align 4, !tbaa !2
  %200 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer196 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %200, i32 0, i32 1
  %201 = load i32, i32* %bytes_in_buffer196, align 4, !tbaa !21
  store i32 %201, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end197

if.end197:                                        ; preds = %if.end194, %do.body186
  %202 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec198 = add i32 %202, -1
  store i32 %dec198, i32* %bytes_in_buffer, align 4, !tbaa !22
  %203 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr199 = getelementptr inbounds i8, i8* %203, i32 1
  store i8* %incdec.ptr199, i8** %next_input_byte, align 4, !tbaa !2
  %204 = load i8, i8* %203, align 1, !tbaa !16
  %conv200 = zext i8 %204 to i32
  store i32 %conv200, i32* %c, align 4, !tbaa !6
  br label %do.cond201

do.cond201:                                       ; preds = %if.end197
  br label %do.end202

do.end202:                                        ; preds = %do.cond201
  %205 = load i32, i32* %c, align 4, !tbaa !6
  %206 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %206, i32 0, i32 73
  store i32 %205, i32* %Se, align 4, !tbaa !85
  br label %do.body203

do.body203:                                       ; preds = %do.end202
  %207 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp204 = icmp eq i32 %207, 0
  br i1 %cmp204, label %if.then206, label %if.end214

if.then206:                                       ; preds = %do.body203
  %208 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer207 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %208, i32 0, i32 3
  %209 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer207, align 4, !tbaa !23
  %210 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call208 = call i32 %209(%struct.jpeg_decompress_struct* %210)
  %tobool209 = icmp ne i32 %call208, 0
  br i1 %tobool209, label %if.end211, label %if.then210

if.then210:                                       ; preds = %if.then206
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end211:                                        ; preds = %if.then206
  %211 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte212 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %211, i32 0, i32 0
  %212 = load i8*, i8** %next_input_byte212, align 4, !tbaa !19
  store i8* %212, i8** %next_input_byte, align 4, !tbaa !2
  %213 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer213 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %213, i32 0, i32 1
  %214 = load i32, i32* %bytes_in_buffer213, align 4, !tbaa !21
  store i32 %214, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end214

if.end214:                                        ; preds = %if.end211, %do.body203
  %215 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec215 = add i32 %215, -1
  store i32 %dec215, i32* %bytes_in_buffer, align 4, !tbaa !22
  %216 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr216 = getelementptr inbounds i8, i8* %216, i32 1
  store i8* %incdec.ptr216, i8** %next_input_byte, align 4, !tbaa !2
  %217 = load i8, i8* %216, align 1, !tbaa !16
  %conv217 = zext i8 %217 to i32
  store i32 %conv217, i32* %c, align 4, !tbaa !6
  br label %do.cond218

do.cond218:                                       ; preds = %if.end214
  br label %do.end219

do.end219:                                        ; preds = %do.cond218
  %218 = load i32, i32* %c, align 4, !tbaa !6
  %shr220 = ashr i32 %218, 4
  %and221 = and i32 %shr220, 15
  %219 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %219, i32 0, i32 74
  store i32 %and221, i32* %Ah, align 8, !tbaa !86
  %220 = load i32, i32* %c, align 4, !tbaa !6
  %and222 = and i32 %220, 15
  %221 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %221, i32 0, i32 75
  store i32 %and222, i32* %Al, align 4, !tbaa !87
  br label %do.body223

do.body223:                                       ; preds = %do.end219
  %222 = bitcast i32** %_mp224 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %222) #4
  %223 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err225 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %223, i32 0, i32 0
  %224 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err225, align 8, !tbaa !12
  %msg_parm226 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %224, i32 0, i32 6
  %i227 = bitcast %union.anon* %msg_parm226 to [8 x i32]*
  %arraydecay228 = getelementptr inbounds [8 x i32], [8 x i32]* %i227, i32 0, i32 0
  store i32* %arraydecay228, i32** %_mp224, align 4, !tbaa !2
  %225 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss229 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %225, i32 0, i32 72
  %226 = load i32, i32* %Ss229, align 8, !tbaa !84
  %227 = load i32*, i32** %_mp224, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds i32, i32* %227, i32 0
  store i32 %226, i32* %arrayidx230, align 4, !tbaa !6
  %228 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se231 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %228, i32 0, i32 73
  %229 = load i32, i32* %Se231, align 4, !tbaa !85
  %230 = load i32*, i32** %_mp224, align 4, !tbaa !2
  %arrayidx232 = getelementptr inbounds i32, i32* %230, i32 1
  store i32 %229, i32* %arrayidx232, align 4, !tbaa !6
  %231 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah233 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %231, i32 0, i32 74
  %232 = load i32, i32* %Ah233, align 8, !tbaa !86
  %233 = load i32*, i32** %_mp224, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i32, i32* %233, i32 2
  store i32 %232, i32* %arrayidx234, align 4, !tbaa !6
  %234 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al235 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %234, i32 0, i32 75
  %235 = load i32, i32* %Al235, align 4, !tbaa !87
  %236 = load i32*, i32** %_mp224, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %236, i32 3
  store i32 %235, i32* %arrayidx236, align 4, !tbaa !6
  %237 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err237 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %237, i32 0, i32 0
  %238 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err237, align 8, !tbaa !12
  %msg_code238 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %238, i32 0, i32 5
  store i32 105, i32* %msg_code238, align 4, !tbaa !13
  %239 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err239 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %239, i32 0, i32 0
  %240 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err239, align 8, !tbaa !12
  %emit_message240 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %240, i32 0, i32 1
  %241 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message240, align 4, !tbaa !17
  %242 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %243 = bitcast %struct.jpeg_decompress_struct* %242 to %struct.jpeg_common_struct*
  call void %241(%struct.jpeg_common_struct* %243, i32 1)
  %244 = bitcast i32** %_mp224 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #4
  br label %do.cond241

do.cond241:                                       ; preds = %do.body223
  br label %do.end242

do.end242:                                        ; preds = %do.cond241
  %245 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker243 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %245, i32 0, i32 82
  %246 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker243, align 8, !tbaa !24
  %next_restart_num = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %246, i32 0, i32 5
  store i32 0, i32* %next_restart_num, align 4, !tbaa !44
  %247 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %247, i32 0, i32 35
  %248 = load i32, i32* %input_scan_number, align 8, !tbaa !37
  %inc244 = add nsw i32 %248, 1
  store i32 %inc244, i32* %input_scan_number, align 8, !tbaa !37
  %249 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %250 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte245 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %250, i32 0, i32 0
  store i8* %249, i8** %next_input_byte245, align 4, !tbaa !19
  %251 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %252 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer246 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %252, i32 0, i32 1
  store i32 %251, i32* %bytes_in_buffer246, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end242, %if.then210, %if.then193, %if.then176, %if.then92, %if.then75, %if.then32, %if.then17, %if.then6
  %253 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #4
  %254 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #4
  %255 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #4
  %256 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #4
  %257 = bitcast i32* %pi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #4
  %258 = bitcast i32* %cc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #4
  %259 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #4
  %260 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #4
  %261 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #4
  %262 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #4
  %263 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  %264 = load i32, i32* %retval, align 4
  ret i32 %264
}

; Function Attrs: nounwind
define internal i32 @get_dht(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %bits = alloca [17 x i8], align 16
  %huffval = alloca [256 x i8], align 16
  %i = alloca i32, align 4
  %index = alloca i32, align 4
  %count = alloca i32, align 4
  %htblptr = alloca %struct.JHUFF_TBL**, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_mp = alloca i32*, align 4
  %_mp102 = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast [17 x i8]* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 17, i8* %1) #4
  %2 = bitcast [256 x i8]* %huffval to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %2) #4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast %struct.JHUFF_TBL*** %htblptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 6
  %9 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %9, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %10 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 0
  %12 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %12, i8** %next_input_byte, align 4, !tbaa !2
  %13 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 1
  %15 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %15, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %16 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %16, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %17 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %17, i32 0, i32 3
  %18 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %18(%struct.jpeg_decompress_struct* %19)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %20 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %20, i32 0, i32 0
  %21 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %21, i8** %next_input_byte, align 4, !tbaa !2
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 1
  %23 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %23, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %24 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %24, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %25 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %25, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !16
  %conv = zext i8 %26 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %27 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %27, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 3
  %29 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %29(%struct.jpeg_decompress_struct* %30)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %31 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %31, i32 0, i32 0
  %32 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %32, i8** %next_input_byte, align 4, !tbaa !2
  %33 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %33, i32 0, i32 1
  %34 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %34, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %35 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %35, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %36 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %37 = load i8, i8* %36, align 1, !tbaa !16
  %conv20 = zext i8 %37 to i32
  %38 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %38, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  %39 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %39, 2
  store i32 %sub, i32* %length, align 4, !tbaa !22
  br label %while.cond

while.cond:                                       ; preds = %if.end213, %do.end
  %40 = load i32, i32* %length, align 4, !tbaa !22
  %cmp21 = icmp sgt i32 %40, 16
  br i1 %cmp21, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  br label %do.body23

do.body23:                                        ; preds = %while.body
  %41 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp24 = icmp eq i32 %41, 0
  br i1 %cmp24, label %if.then26, label %if.end34

if.then26:                                        ; preds = %do.body23
  %42 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer27 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %42, i32 0, i32 3
  %43 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer27, align 4, !tbaa !23
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call28 = call i32 %43(%struct.jpeg_decompress_struct* %44)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %if.then26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %if.then26
  %45 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte32 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %45, i32 0, i32 0
  %46 = load i8*, i8** %next_input_byte32, align 4, !tbaa !19
  store i8* %46, i8** %next_input_byte, align 4, !tbaa !2
  %47 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer33 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %47, i32 0, i32 1
  %48 = load i32, i32* %bytes_in_buffer33, align 4, !tbaa !21
  store i32 %48, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end34

if.end34:                                         ; preds = %if.end31, %do.body23
  %49 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec35 = add i32 %49, -1
  store i32 %dec35, i32* %bytes_in_buffer, align 4, !tbaa !22
  %50 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %50, i32 1
  store i8* %incdec.ptr36, i8** %next_input_byte, align 4, !tbaa !2
  %51 = load i8, i8* %50, align 1, !tbaa !16
  %conv37 = zext i8 %51 to i32
  store i32 %conv37, i32* %index, align 4, !tbaa !6
  br label %do.cond38

do.cond38:                                        ; preds = %if.end34
  br label %do.end39

do.end39:                                         ; preds = %do.cond38
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 5
  store i32 80, i32* %msg_code, align 4, !tbaa !13
  %54 = load i32, i32* %index, align 4, !tbaa !6
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err40, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 6
  %i41 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i41, i32 0, i32 0
  store i32 %54, i32* %arrayidx, align 4, !tbaa !16
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 0
  %58 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %58, i32 0, i32 1
  %59 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %61 = bitcast %struct.jpeg_decompress_struct* %60 to %struct.jpeg_common_struct*
  call void %59(%struct.jpeg_common_struct* %61, i32 1)
  %arrayidx43 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 0
  store i8 0, i8* %arrayidx43, align 16, !tbaa !16
  store i32 0, i32* %count, align 4, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end39
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %cmp44 = icmp sle i32 %62, 16
  br i1 %cmp44, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body46

do.body46:                                        ; preds = %for.body
  %63 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp47 = icmp eq i32 %63, 0
  br i1 %cmp47, label %if.then49, label %if.end57

if.then49:                                        ; preds = %do.body46
  %64 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer50 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %64, i32 0, i32 3
  %65 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer50, align 4, !tbaa !23
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call51 = call i32 %65(%struct.jpeg_decompress_struct* %66)
  %tobool52 = icmp ne i32 %call51, 0
  br i1 %tobool52, label %if.end54, label %if.then53

if.then53:                                        ; preds = %if.then49
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end54:                                         ; preds = %if.then49
  %67 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte55 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %67, i32 0, i32 0
  %68 = load i8*, i8** %next_input_byte55, align 4, !tbaa !19
  store i8* %68, i8** %next_input_byte, align 4, !tbaa !2
  %69 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer56 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %69, i32 0, i32 1
  %70 = load i32, i32* %bytes_in_buffer56, align 4, !tbaa !21
  store i32 %70, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end57

if.end57:                                         ; preds = %if.end54, %do.body46
  %71 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec58 = add i32 %71, -1
  store i32 %dec58, i32* %bytes_in_buffer, align 4, !tbaa !22
  %72 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr59 = getelementptr inbounds i8, i8* %72, i32 1
  store i8* %incdec.ptr59, i8** %next_input_byte, align 4, !tbaa !2
  %73 = load i8, i8* %72, align 1, !tbaa !16
  %74 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 %74
  store i8 %73, i8* %arrayidx60, align 1, !tbaa !16
  br label %do.cond61

do.cond61:                                        ; preds = %if.end57
  br label %do.end62

do.end62:                                         ; preds = %do.cond61
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 %75
  %76 = load i8, i8* %arrayidx63, align 1, !tbaa !16
  %conv64 = zext i8 %76 to i32
  %77 = load i32, i32* %count, align 4, !tbaa !6
  %add65 = add nsw i32 %77, %conv64
  store i32 %add65, i32* %count, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %do.end62
  %78 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %78, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %79 = load i32, i32* %length, align 4, !tbaa !22
  %sub66 = sub nsw i32 %79, 17
  store i32 %sub66, i32* %length, align 4, !tbaa !22
  br label %do.body67

do.body67:                                        ; preds = %for.end
  %80 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 0
  %82 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err68, align 8, !tbaa !12
  %msg_parm69 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %82, i32 0, i32 6
  %i70 = bitcast %union.anon* %msg_parm69 to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i70, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 1
  %83 = load i8, i8* %arrayidx71, align 1, !tbaa !16
  %conv72 = zext i8 %83 to i32
  %84 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %84, i32 0
  store i32 %conv72, i32* %arrayidx73, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 2
  %85 = load i8, i8* %arrayidx74, align 2, !tbaa !16
  %conv75 = zext i8 %85 to i32
  %86 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %86, i32 1
  store i32 %conv75, i32* %arrayidx76, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 3
  %87 = load i8, i8* %arrayidx77, align 1, !tbaa !16
  %conv78 = zext i8 %87 to i32
  %88 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %88, i32 2
  store i32 %conv78, i32* %arrayidx79, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 4
  %89 = load i8, i8* %arrayidx80, align 4, !tbaa !16
  %conv81 = zext i8 %89 to i32
  %90 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %90, i32 3
  store i32 %conv81, i32* %arrayidx82, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 5
  %91 = load i8, i8* %arrayidx83, align 1, !tbaa !16
  %conv84 = zext i8 %91 to i32
  %92 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %92, i32 4
  store i32 %conv84, i32* %arrayidx85, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 6
  %93 = load i8, i8* %arrayidx86, align 2, !tbaa !16
  %conv87 = zext i8 %93 to i32
  %94 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %94, i32 5
  store i32 %conv87, i32* %arrayidx88, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 7
  %95 = load i8, i8* %arrayidx89, align 1, !tbaa !16
  %conv90 = zext i8 %95 to i32
  %96 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %96, i32 6
  store i32 %conv90, i32* %arrayidx91, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 8
  %97 = load i8, i8* %arrayidx92, align 8, !tbaa !16
  %conv93 = zext i8 %97 to i32
  %98 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i32, i32* %98, i32 7
  store i32 %conv93, i32* %arrayidx94, align 4, !tbaa !6
  %99 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err95 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %99, i32 0, i32 0
  %100 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err95, align 8, !tbaa !12
  %msg_code96 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %100, i32 0, i32 5
  store i32 86, i32* %msg_code96, align 4, !tbaa !13
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err97 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %101, i32 0, i32 0
  %102 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err97, align 8, !tbaa !12
  %emit_message98 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %102, i32 0, i32 1
  %103 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message98, align 4, !tbaa !17
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %105 = bitcast %struct.jpeg_decompress_struct* %104 to %struct.jpeg_common_struct*
  call void %103(%struct.jpeg_common_struct* %105, i32 2)
  %106 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  br label %do.cond99

do.cond99:                                        ; preds = %do.body67
  br label %do.end100

do.end100:                                        ; preds = %do.cond99
  br label %do.body101

do.body101:                                       ; preds = %do.end100
  %107 = bitcast i32** %_mp102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #4
  %108 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err103 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %108, i32 0, i32 0
  %109 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err103, align 8, !tbaa !12
  %msg_parm104 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %109, i32 0, i32 6
  %i105 = bitcast %union.anon* %msg_parm104 to [8 x i32]*
  %arraydecay106 = getelementptr inbounds [8 x i32], [8 x i32]* %i105, i32 0, i32 0
  store i32* %arraydecay106, i32** %_mp102, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 9
  %110 = load i8, i8* %arrayidx107, align 1, !tbaa !16
  %conv108 = zext i8 %110 to i32
  %111 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %111, i32 0
  store i32 %conv108, i32* %arrayidx109, align 4, !tbaa !6
  %arrayidx110 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 10
  %112 = load i8, i8* %arrayidx110, align 2, !tbaa !16
  %conv111 = zext i8 %112 to i32
  %113 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %113, i32 1
  store i32 %conv111, i32* %arrayidx112, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 11
  %114 = load i8, i8* %arrayidx113, align 1, !tbaa !16
  %conv114 = zext i8 %114 to i32
  %115 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %115, i32 2
  store i32 %conv114, i32* %arrayidx115, align 4, !tbaa !6
  %arrayidx116 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 12
  %116 = load i8, i8* %arrayidx116, align 4, !tbaa !16
  %conv117 = zext i8 %116 to i32
  %117 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %117, i32 3
  store i32 %conv117, i32* %arrayidx118, align 4, !tbaa !6
  %arrayidx119 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 13
  %118 = load i8, i8* %arrayidx119, align 1, !tbaa !16
  %conv120 = zext i8 %118 to i32
  %119 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %119, i32 4
  store i32 %conv120, i32* %arrayidx121, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 14
  %120 = load i8, i8* %arrayidx122, align 2, !tbaa !16
  %conv123 = zext i8 %120 to i32
  %121 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %121, i32 5
  store i32 %conv123, i32* %arrayidx124, align 4, !tbaa !6
  %arrayidx125 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 15
  %122 = load i8, i8* %arrayidx125, align 1, !tbaa !16
  %conv126 = zext i8 %122 to i32
  %123 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %123, i32 6
  store i32 %conv126, i32* %arrayidx127, align 4, !tbaa !6
  %arrayidx128 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 16
  %124 = load i8, i8* %arrayidx128, align 16, !tbaa !16
  %conv129 = zext i8 %124 to i32
  %125 = load i32*, i32** %_mp102, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %125, i32 7
  store i32 %conv129, i32* %arrayidx130, align 4, !tbaa !6
  %126 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err131 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %126, i32 0, i32 0
  %127 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err131, align 8, !tbaa !12
  %msg_code132 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %127, i32 0, i32 5
  store i32 86, i32* %msg_code132, align 4, !tbaa !13
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err133 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 0
  %129 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err133, align 8, !tbaa !12
  %emit_message134 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %129, i32 0, i32 1
  %130 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message134, align 4, !tbaa !17
  %131 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %132 = bitcast %struct.jpeg_decompress_struct* %131 to %struct.jpeg_common_struct*
  call void %130(%struct.jpeg_common_struct* %132, i32 2)
  %133 = bitcast i32** %_mp102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #4
  br label %do.cond135

do.cond135:                                       ; preds = %do.body101
  br label %do.end136

do.end136:                                        ; preds = %do.cond135
  %134 = load i32, i32* %count, align 4, !tbaa !6
  %cmp137 = icmp sgt i32 %134, 256
  br i1 %cmp137, label %if.then141, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.end136
  %135 = load i32, i32* %count, align 4, !tbaa !6
  %136 = load i32, i32* %length, align 4, !tbaa !22
  %cmp139 = icmp sgt i32 %135, %136
  br i1 %cmp139, label %if.then141, label %if.end145

if.then141:                                       ; preds = %lor.lhs.false, %do.end136
  %137 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err142 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %137, i32 0, i32 0
  %138 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err142, align 8, !tbaa !12
  %msg_code143 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %138, i32 0, i32 5
  store i32 8, i32* %msg_code143, align 4, !tbaa !13
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err144 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 0
  %140 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err144, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %140, i32 0, i32 0
  %141 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %142 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %143 = bitcast %struct.jpeg_decompress_struct* %142 to %struct.jpeg_common_struct*
  call void %141(%struct.jpeg_common_struct* %143)
  br label %if.end145

if.end145:                                        ; preds = %if.then141, %lor.lhs.false
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond146

for.cond146:                                      ; preds = %for.inc167, %if.end145
  %144 = load i32, i32* %i, align 4, !tbaa !6
  %145 = load i32, i32* %count, align 4, !tbaa !6
  %cmp147 = icmp slt i32 %144, %145
  br i1 %cmp147, label %for.body149, label %for.end169

for.body149:                                      ; preds = %for.cond146
  br label %do.body150

do.body150:                                       ; preds = %for.body149
  %146 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp151 = icmp eq i32 %146, 0
  br i1 %cmp151, label %if.then153, label %if.end161

if.then153:                                       ; preds = %do.body150
  %147 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer154 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %147, i32 0, i32 3
  %148 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer154, align 4, !tbaa !23
  %149 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call155 = call i32 %148(%struct.jpeg_decompress_struct* %149)
  %tobool156 = icmp ne i32 %call155, 0
  br i1 %tobool156, label %if.end158, label %if.then157

if.then157:                                       ; preds = %if.then153
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end158:                                        ; preds = %if.then153
  %150 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte159 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %150, i32 0, i32 0
  %151 = load i8*, i8** %next_input_byte159, align 4, !tbaa !19
  store i8* %151, i8** %next_input_byte, align 4, !tbaa !2
  %152 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer160 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %152, i32 0, i32 1
  %153 = load i32, i32* %bytes_in_buffer160, align 4, !tbaa !21
  store i32 %153, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end161

if.end161:                                        ; preds = %if.end158, %do.body150
  %154 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec162 = add i32 %154, -1
  store i32 %dec162, i32* %bytes_in_buffer, align 4, !tbaa !22
  %155 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr163 = getelementptr inbounds i8, i8* %155, i32 1
  store i8* %incdec.ptr163, i8** %next_input_byte, align 4, !tbaa !2
  %156 = load i8, i8* %155, align 1, !tbaa !16
  %157 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx164 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %157
  store i8 %156, i8* %arrayidx164, align 1, !tbaa !16
  br label %do.cond165

do.cond165:                                       ; preds = %if.end161
  br label %do.end166

do.end166:                                        ; preds = %do.cond165
  br label %for.inc167

for.inc167:                                       ; preds = %do.end166
  %158 = load i32, i32* %i, align 4, !tbaa !6
  %inc168 = add nsw i32 %158, 1
  store i32 %inc168, i32* %i, align 4, !tbaa !6
  br label %for.cond146

for.end169:                                       ; preds = %for.cond146
  %159 = load i32, i32* %count, align 4, !tbaa !6
  %arrayidx170 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %159
  %160 = load i32, i32* %count, align 4, !tbaa !6
  %sub171 = sub nsw i32 256, %160
  %mul = mul i32 %sub171, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx170, i8 0, i32 %mul, i1 false)
  %161 = load i32, i32* %count, align 4, !tbaa !6
  %162 = load i32, i32* %length, align 4, !tbaa !22
  %sub172 = sub nsw i32 %162, %161
  store i32 %sub172, i32* %length, align 4, !tbaa !22
  %163 = load i32, i32* %index, align 4, !tbaa !6
  %and = and i32 %163, 16
  %tobool173 = icmp ne i32 %and, 0
  br i1 %tobool173, label %if.then174, label %if.else

if.then174:                                       ; preds = %for.end169
  %164 = load i32, i32* %index, align 4, !tbaa !6
  %sub175 = sub nsw i32 %164, 16
  store i32 %sub175, i32* %index, align 4, !tbaa !6
  %165 = load i32, i32* %index, align 4, !tbaa !6
  %cmp176 = icmp slt i32 %165, 0
  br i1 %cmp176, label %if.then181, label %lor.lhs.false178

lor.lhs.false178:                                 ; preds = %if.then174
  %166 = load i32, i32* %index, align 4, !tbaa !6
  %cmp179 = icmp sge i32 %166, 4
  br i1 %cmp179, label %if.then181, label %if.end190

if.then181:                                       ; preds = %lor.lhs.false178, %if.then174
  %167 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err182 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %167, i32 0, i32 0
  %168 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err182, align 8, !tbaa !12
  %msg_code183 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %168, i32 0, i32 5
  store i32 30, i32* %msg_code183, align 4, !tbaa !13
  %169 = load i32, i32* %index, align 4, !tbaa !6
  %170 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err184 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %170, i32 0, i32 0
  %171 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err184, align 8, !tbaa !12
  %msg_parm185 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %171, i32 0, i32 6
  %i186 = bitcast %union.anon* %msg_parm185 to [8 x i32]*
  %arrayidx187 = getelementptr inbounds [8 x i32], [8 x i32]* %i186, i32 0, i32 0
  store i32 %169, i32* %arrayidx187, align 4, !tbaa !16
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err188 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %172, i32 0, i32 0
  %173 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err188, align 8, !tbaa !12
  %error_exit189 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %173, i32 0, i32 0
  %174 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit189, align 4, !tbaa !43
  %175 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %176 = bitcast %struct.jpeg_decompress_struct* %175 to %struct.jpeg_common_struct*
  call void %174(%struct.jpeg_common_struct* %176)
  br label %if.end190

if.end190:                                        ; preds = %if.then181, %lor.lhs.false178
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %177, i32 0, i32 42
  %178 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx191 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %178
  store %struct.JHUFF_TBL** %arrayidx191, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  br label %if.end208

if.else:                                          ; preds = %for.end169
  %179 = load i32, i32* %index, align 4, !tbaa !6
  %cmp192 = icmp slt i32 %179, 0
  br i1 %cmp192, label %if.then197, label %lor.lhs.false194

lor.lhs.false194:                                 ; preds = %if.else
  %180 = load i32, i32* %index, align 4, !tbaa !6
  %cmp195 = icmp sge i32 %180, 4
  br i1 %cmp195, label %if.then197, label %if.end206

if.then197:                                       ; preds = %lor.lhs.false194, %if.else
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err198 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 0
  %182 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err198, align 8, !tbaa !12
  %msg_code199 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %182, i32 0, i32 5
  store i32 30, i32* %msg_code199, align 4, !tbaa !13
  %183 = load i32, i32* %index, align 4, !tbaa !6
  %184 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err200 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %184, i32 0, i32 0
  %185 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err200, align 8, !tbaa !12
  %msg_parm201 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %185, i32 0, i32 6
  %i202 = bitcast %union.anon* %msg_parm201 to [8 x i32]*
  %arrayidx203 = getelementptr inbounds [8 x i32], [8 x i32]* %i202, i32 0, i32 0
  store i32 %183, i32* %arrayidx203, align 4, !tbaa !16
  %186 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err204 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %186, i32 0, i32 0
  %187 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err204, align 8, !tbaa !12
  %error_exit205 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %187, i32 0, i32 0
  %188 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit205, align 4, !tbaa !43
  %189 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %190 = bitcast %struct.jpeg_decompress_struct* %189 to %struct.jpeg_common_struct*
  call void %188(%struct.jpeg_common_struct* %190)
  br label %if.end206

if.end206:                                        ; preds = %if.then197, %lor.lhs.false194
  %191 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %191, i32 0, i32 41
  %192 = load i32, i32* %index, align 4, !tbaa !6
  %arrayidx207 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %192
  store %struct.JHUFF_TBL** %arrayidx207, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  br label %if.end208

if.end208:                                        ; preds = %if.end206, %if.end190
  %193 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  %194 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %193, align 4, !tbaa !2
  %cmp209 = icmp eq %struct.JHUFF_TBL* %194, null
  br i1 %cmp209, label %if.then211, label %if.end213

if.then211:                                       ; preds = %if.end208
  %195 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %196 = bitcast %struct.jpeg_decompress_struct* %195 to %struct.jpeg_common_struct*
  %call212 = call %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct* %196)
  %197 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %call212, %struct.JHUFF_TBL** %197, align 4, !tbaa !2
  br label %if.end213

if.end213:                                        ; preds = %if.then211, %if.end208
  %198 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  %199 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %198, align 4, !tbaa !2
  %bits214 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %199, i32 0, i32 0
  %arraydecay215 = getelementptr inbounds [17 x i8], [17 x i8]* %bits214, i32 0, i32 0
  %arraydecay216 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay215, i8* align 16 %arraydecay216, i32 17, i1 false)
  %200 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  %201 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %200, align 4, !tbaa !2
  %huffval217 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %201, i32 0, i32 1
  %arraydecay218 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval217, i32 0, i32 0
  %arraydecay219 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay218, i8* align 16 %arraydecay219, i32 256, i1 false)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %202 = load i32, i32* %length, align 4, !tbaa !22
  %cmp220 = icmp ne i32 %202, 0
  br i1 %cmp220, label %if.then222, label %if.end227

if.then222:                                       ; preds = %while.end
  %203 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err223 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %203, i32 0, i32 0
  %204 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err223, align 8, !tbaa !12
  %msg_code224 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %204, i32 0, i32 5
  store i32 11, i32* %msg_code224, align 4, !tbaa !13
  %205 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err225 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %205, i32 0, i32 0
  %206 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err225, align 8, !tbaa !12
  %error_exit226 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %206, i32 0, i32 0
  %207 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit226, align 4, !tbaa !43
  %208 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %209 = bitcast %struct.jpeg_decompress_struct* %208 to %struct.jpeg_common_struct*
  call void %207(%struct.jpeg_common_struct* %209)
  br label %if.end227

if.end227:                                        ; preds = %if.then222, %while.end
  %210 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %211 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte228 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %211, i32 0, i32 0
  store i8* %210, i8** %next_input_byte228, align 4, !tbaa !19
  %212 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %213 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer229 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %213, i32 0, i32 1
  store i32 %212, i32* %bytes_in_buffer229, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end227, %if.then157, %if.then53, %if.then30, %if.then13, %if.then3
  %214 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast %struct.JHUFF_TBL*** %htblptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  %221 = bitcast [256 x i8]* %huffval to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %221) #4
  %222 = bitcast [17 x i8]* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 17, i8* %222) #4
  %223 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %224 = load i32, i32* %retval, align 4
  ret i32 %224
}

; Function Attrs: nounwind
define internal i32 @get_dqt(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  %prec = alloca i32, align 4
  %tmp = alloca i32, align 4
  %quant_ptr = alloca %struct.JQUANT_TBL*, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %_mp = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast %struct.JQUANT_TBL** %quant_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 6
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %8, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %9 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %10, i32 0, i32 0
  %11 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %11, i8** %next_input_byte, align 4, !tbaa !2
  %12 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %13, i32 0, i32 1
  %14 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %14, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %15 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %15, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %16 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %16, i32 0, i32 3
  %17 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %17(%struct.jpeg_decompress_struct* %18)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %19 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %19, i32 0, i32 0
  %20 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %20, i8** %next_input_byte, align 4, !tbaa !2
  %21 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %21, i32 0, i32 1
  %22 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %22, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %23 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %23, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %24 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %25 = load i8, i8* %24, align 1, !tbaa !16
  %conv = zext i8 %25 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %26 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %26, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %27 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %27, i32 0, i32 3
  %28 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %28(%struct.jpeg_decompress_struct* %29)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %30, i32 0, i32 0
  %31 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %31, i8** %next_input_byte, align 4, !tbaa !2
  %32 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %32, i32 0, i32 1
  %33 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %33, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %34 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %34, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %35 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %35, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %36 = load i8, i8* %35, align 1, !tbaa !16
  %conv20 = zext i8 %36 to i32
  %37 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %37, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  %38 = load i32, i32* %length, align 4, !tbaa !22
  %sub = sub nsw i32 %38, 2
  store i32 %sub, i32* %length, align 4, !tbaa !22
  br label %while.cond

while.cond:                                       ; preds = %if.end191, %do.end
  %39 = load i32, i32* %length, align 4, !tbaa !22
  %cmp21 = icmp sgt i32 %39, 0
  br i1 %cmp21, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  br label %do.body23

do.body23:                                        ; preds = %while.body
  %40 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp24 = icmp eq i32 %40, 0
  br i1 %cmp24, label %if.then26, label %if.end34

if.then26:                                        ; preds = %do.body23
  %41 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer27 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %41, i32 0, i32 3
  %42 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer27, align 4, !tbaa !23
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call28 = call i32 %42(%struct.jpeg_decompress_struct* %43)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %if.then26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %if.then26
  %44 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte32 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %44, i32 0, i32 0
  %45 = load i8*, i8** %next_input_byte32, align 4, !tbaa !19
  store i8* %45, i8** %next_input_byte, align 4, !tbaa !2
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer33 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 1
  %47 = load i32, i32* %bytes_in_buffer33, align 4, !tbaa !21
  store i32 %47, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end34

if.end34:                                         ; preds = %if.end31, %do.body23
  %48 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec35 = add i32 %48, -1
  store i32 %dec35, i32* %bytes_in_buffer, align 4, !tbaa !22
  %49 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr36, i8** %next_input_byte, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !16
  %conv37 = zext i8 %50 to i32
  store i32 %conv37, i32* %n, align 4, !tbaa !6
  br label %do.cond38

do.cond38:                                        ; preds = %if.end34
  br label %do.end39

do.end39:                                         ; preds = %do.cond38
  %51 = load i32, i32* %n, align 4, !tbaa !6
  %shr = ashr i32 %51, 4
  store i32 %shr, i32* %prec, align 4, !tbaa !6
  %52 = load i32, i32* %n, align 4, !tbaa !6
  %and = and i32 %52, 15
  store i32 %and, i32* %n, align 4, !tbaa !6
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 0
  %54 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %54, i32 0, i32 5
  store i32 81, i32* %msg_code, align 4, !tbaa !13
  %55 = load i32, i32* %n, align 4, !tbaa !6
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 0
  %57 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err40, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %57, i32 0, i32 6
  %i41 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i41, i32 0, i32 0
  store i32 %55, i32* %arrayidx, align 4, !tbaa !16
  %58 = load i32, i32* %prec, align 4, !tbaa !6
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 0
  %60 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !12
  %msg_parm43 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %60, i32 0, i32 6
  %i44 = bitcast %union.anon* %msg_parm43 to [8 x i32]*
  %arrayidx45 = getelementptr inbounds [8 x i32], [8 x i32]* %i44, i32 0, i32 1
  store i32 %58, i32* %arrayidx45, align 4, !tbaa !16
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 1
  %63 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %65 = bitcast %struct.jpeg_decompress_struct* %64 to %struct.jpeg_common_struct*
  call void %63(%struct.jpeg_common_struct* %65, i32 1)
  %66 = load i32, i32* %n, align 4, !tbaa !6
  %cmp47 = icmp sge i32 %66, 4
  br i1 %cmp47, label %if.then49, label %if.end57

if.then49:                                        ; preds = %do.end39
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 0
  %68 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err50, align 8, !tbaa !12
  %msg_code51 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %68, i32 0, i32 5
  store i32 31, i32* %msg_code51, align 4, !tbaa !13
  %69 = load i32, i32* %n, align 4, !tbaa !6
  %70 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %70, i32 0, i32 0
  %71 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err52, align 8, !tbaa !12
  %msg_parm53 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %71, i32 0, i32 6
  %i54 = bitcast %union.anon* %msg_parm53 to [8 x i32]*
  %arrayidx55 = getelementptr inbounds [8 x i32], [8 x i32]* %i54, i32 0, i32 0
  store i32 %69, i32* %arrayidx55, align 4, !tbaa !16
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %72, i32 0, i32 0
  %73 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err56, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %73, i32 0, i32 0
  %74 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %76 = bitcast %struct.jpeg_decompress_struct* %75 to %struct.jpeg_common_struct*
  call void %74(%struct.jpeg_common_struct* %76)
  br label %if.end57

if.end57:                                         ; preds = %if.then49, %do.end39
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 40
  %78 = load i32, i32* %n, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %78
  %79 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx58, align 4, !tbaa !2
  %cmp59 = icmp eq %struct.JQUANT_TBL* %79, null
  br i1 %cmp59, label %if.then61, label %if.end65

if.then61:                                        ; preds = %if.end57
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %81 = bitcast %struct.jpeg_decompress_struct* %80 to %struct.jpeg_common_struct*
  %call62 = call %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct* %81)
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs63 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %82, i32 0, i32 40
  %83 = load i32, i32* %n, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs63, i32 0, i32 %83
  store %struct.JQUANT_TBL* %call62, %struct.JQUANT_TBL** %arrayidx64, align 4, !tbaa !2
  br label %if.end65

if.end65:                                         ; preds = %if.then61, %if.end57
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs66 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 40
  %85 = load i32, i32* %n, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs66, i32 0, i32 %85
  %86 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx67, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %86, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end65
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %cmp68 = icmp slt i32 %87, 64
  br i1 %cmp68, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %88 = load i32, i32* %prec, align 4, !tbaa !6
  %tobool70 = icmp ne i32 %88, 0
  br i1 %tobool70, label %if.then71, label %if.else

if.then71:                                        ; preds = %for.body
  br label %do.body72

do.body72:                                        ; preds = %if.then71
  %89 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp73 = icmp eq i32 %89, 0
  br i1 %cmp73, label %if.then75, label %if.end83

if.then75:                                        ; preds = %do.body72
  %90 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer76 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %90, i32 0, i32 3
  %91 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer76, align 4, !tbaa !23
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call77 = call i32 %91(%struct.jpeg_decompress_struct* %92)
  %tobool78 = icmp ne i32 %call77, 0
  br i1 %tobool78, label %if.end80, label %if.then79

if.then79:                                        ; preds = %if.then75
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end80:                                         ; preds = %if.then75
  %93 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte81 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %93, i32 0, i32 0
  %94 = load i8*, i8** %next_input_byte81, align 4, !tbaa !19
  store i8* %94, i8** %next_input_byte, align 4, !tbaa !2
  %95 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer82 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %95, i32 0, i32 1
  %96 = load i32, i32* %bytes_in_buffer82, align 4, !tbaa !21
  store i32 %96, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end83

if.end83:                                         ; preds = %if.end80, %do.body72
  %97 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec84 = add i32 %97, -1
  store i32 %dec84, i32* %bytes_in_buffer, align 4, !tbaa !22
  %98 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr85 = getelementptr inbounds i8, i8* %98, i32 1
  store i8* %incdec.ptr85, i8** %next_input_byte, align 4, !tbaa !2
  %99 = load i8, i8* %98, align 1, !tbaa !16
  %conv86 = zext i8 %99 to i32
  %shl87 = shl i32 %conv86, 8
  store i32 %shl87, i32* %tmp, align 4, !tbaa !6
  %100 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp88 = icmp eq i32 %100, 0
  br i1 %cmp88, label %if.then90, label %if.end98

if.then90:                                        ; preds = %if.end83
  %101 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer91 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %101, i32 0, i32 3
  %102 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer91, align 4, !tbaa !23
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call92 = call i32 %102(%struct.jpeg_decompress_struct* %103)
  %tobool93 = icmp ne i32 %call92, 0
  br i1 %tobool93, label %if.end95, label %if.then94

if.then94:                                        ; preds = %if.then90
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end95:                                         ; preds = %if.then90
  %104 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte96 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %104, i32 0, i32 0
  %105 = load i8*, i8** %next_input_byte96, align 4, !tbaa !19
  store i8* %105, i8** %next_input_byte, align 4, !tbaa !2
  %106 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer97 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %106, i32 0, i32 1
  %107 = load i32, i32* %bytes_in_buffer97, align 4, !tbaa !21
  store i32 %107, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end98

if.end98:                                         ; preds = %if.end95, %if.end83
  %108 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec99 = add i32 %108, -1
  store i32 %dec99, i32* %bytes_in_buffer, align 4, !tbaa !22
  %109 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr100 = getelementptr inbounds i8, i8* %109, i32 1
  store i8* %incdec.ptr100, i8** %next_input_byte, align 4, !tbaa !2
  %110 = load i8, i8* %109, align 1, !tbaa !16
  %conv101 = zext i8 %110 to i32
  %111 = load i32, i32* %tmp, align 4, !tbaa !6
  %add102 = add i32 %111, %conv101
  store i32 %add102, i32* %tmp, align 4, !tbaa !6
  br label %do.cond103

do.cond103:                                       ; preds = %if.end98
  br label %do.end104

do.end104:                                        ; preds = %do.cond103
  br label %if.end122

if.else:                                          ; preds = %for.body
  br label %do.body105

do.body105:                                       ; preds = %if.else
  %112 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp106 = icmp eq i32 %112, 0
  br i1 %cmp106, label %if.then108, label %if.end116

if.then108:                                       ; preds = %do.body105
  %113 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer109 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %113, i32 0, i32 3
  %114 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer109, align 4, !tbaa !23
  %115 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call110 = call i32 %114(%struct.jpeg_decompress_struct* %115)
  %tobool111 = icmp ne i32 %call110, 0
  br i1 %tobool111, label %if.end113, label %if.then112

if.then112:                                       ; preds = %if.then108
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end113:                                        ; preds = %if.then108
  %116 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte114 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %116, i32 0, i32 0
  %117 = load i8*, i8** %next_input_byte114, align 4, !tbaa !19
  store i8* %117, i8** %next_input_byte, align 4, !tbaa !2
  %118 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer115 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %118, i32 0, i32 1
  %119 = load i32, i32* %bytes_in_buffer115, align 4, !tbaa !21
  store i32 %119, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end116

if.end116:                                        ; preds = %if.end113, %do.body105
  %120 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec117 = add i32 %120, -1
  store i32 %dec117, i32* %bytes_in_buffer, align 4, !tbaa !22
  %121 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr118 = getelementptr inbounds i8, i8* %121, i32 1
  store i8* %incdec.ptr118, i8** %next_input_byte, align 4, !tbaa !2
  %122 = load i8, i8* %121, align 1, !tbaa !16
  %conv119 = zext i8 %122 to i32
  store i32 %conv119, i32* %tmp, align 4, !tbaa !6
  br label %do.cond120

do.cond120:                                       ; preds = %if.end116
  br label %do.end121

do.end121:                                        ; preds = %do.cond120
  br label %if.end122

if.end122:                                        ; preds = %do.end121, %do.end104
  %123 = load i32, i32* %tmp, align 4, !tbaa !6
  %conv123 = trunc i32 %123 to i16
  %124 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %124, i32 0, i32 0
  %125 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx124 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %125
  %126 = load i32, i32* %arrayidx124, align 4, !tbaa !6
  %arrayidx125 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %126
  store i16 %conv123, i16* %arrayidx125, align 2, !tbaa !88
  br label %for.inc

for.inc:                                          ; preds = %if.end122
  %127 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %127, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err126 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 0
  %129 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err126, align 8, !tbaa !12
  %trace_level = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %129, i32 0, i32 7
  %130 = load i32, i32* %trace_level, align 4, !tbaa !89
  %cmp127 = icmp sge i32 %130, 2
  br i1 %cmp127, label %if.then129, label %if.end186

if.then129:                                       ; preds = %for.end
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc183, %if.then129
  %131 = load i32, i32* %i, align 4, !tbaa !6
  %cmp131 = icmp slt i32 %131, 64
  br i1 %cmp131, label %for.body133, label %for.end185

for.body133:                                      ; preds = %for.cond130
  br label %do.body134

do.body134:                                       ; preds = %for.body133
  %132 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #4
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err135 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %133, i32 0, i32 0
  %134 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err135, align 8, !tbaa !12
  %msg_parm136 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %134, i32 0, i32 6
  %i137 = bitcast %union.anon* %msg_parm136 to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i137, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %135 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval138 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %135, i32 0, i32 0
  %136 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx139 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval138, i32 0, i32 %136
  %137 = load i16, i16* %arrayidx139, align 2, !tbaa !88
  %conv140 = zext i16 %137 to i32
  %138 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds i32, i32* %138, i32 0
  store i32 %conv140, i32* %arrayidx141, align 4, !tbaa !6
  %139 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval142 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %139, i32 0, i32 0
  %140 = load i32, i32* %i, align 4, !tbaa !6
  %add143 = add nsw i32 %140, 1
  %arrayidx144 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval142, i32 0, i32 %add143
  %141 = load i16, i16* %arrayidx144, align 2, !tbaa !88
  %conv145 = zext i16 %141 to i32
  %142 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %142, i32 1
  store i32 %conv145, i32* %arrayidx146, align 4, !tbaa !6
  %143 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval147 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %143, i32 0, i32 0
  %144 = load i32, i32* %i, align 4, !tbaa !6
  %add148 = add nsw i32 %144, 2
  %arrayidx149 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval147, i32 0, i32 %add148
  %145 = load i16, i16* %arrayidx149, align 2, !tbaa !88
  %conv150 = zext i16 %145 to i32
  %146 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %146, i32 2
  store i32 %conv150, i32* %arrayidx151, align 4, !tbaa !6
  %147 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval152 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %147, i32 0, i32 0
  %148 = load i32, i32* %i, align 4, !tbaa !6
  %add153 = add nsw i32 %148, 3
  %arrayidx154 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval152, i32 0, i32 %add153
  %149 = load i16, i16* %arrayidx154, align 2, !tbaa !88
  %conv155 = zext i16 %149 to i32
  %150 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %150, i32 3
  store i32 %conv155, i32* %arrayidx156, align 4, !tbaa !6
  %151 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval157 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %151, i32 0, i32 0
  %152 = load i32, i32* %i, align 4, !tbaa !6
  %add158 = add nsw i32 %152, 4
  %arrayidx159 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval157, i32 0, i32 %add158
  %153 = load i16, i16* %arrayidx159, align 2, !tbaa !88
  %conv160 = zext i16 %153 to i32
  %154 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %154, i32 4
  store i32 %conv160, i32* %arrayidx161, align 4, !tbaa !6
  %155 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval162 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %155, i32 0, i32 0
  %156 = load i32, i32* %i, align 4, !tbaa !6
  %add163 = add nsw i32 %156, 5
  %arrayidx164 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval162, i32 0, i32 %add163
  %157 = load i16, i16* %arrayidx164, align 2, !tbaa !88
  %conv165 = zext i16 %157 to i32
  %158 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %158, i32 5
  store i32 %conv165, i32* %arrayidx166, align 4, !tbaa !6
  %159 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval167 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %159, i32 0, i32 0
  %160 = load i32, i32* %i, align 4, !tbaa !6
  %add168 = add nsw i32 %160, 6
  %arrayidx169 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval167, i32 0, i32 %add168
  %161 = load i16, i16* %arrayidx169, align 2, !tbaa !88
  %conv170 = zext i16 %161 to i32
  %162 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %162, i32 6
  store i32 %conv170, i32* %arrayidx171, align 4, !tbaa !6
  %163 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_ptr, align 4, !tbaa !2
  %quantval172 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %163, i32 0, i32 0
  %164 = load i32, i32* %i, align 4, !tbaa !6
  %add173 = add nsw i32 %164, 7
  %arrayidx174 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval172, i32 0, i32 %add173
  %165 = load i16, i16* %arrayidx174, align 2, !tbaa !88
  %conv175 = zext i16 %165 to i32
  %166 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %166, i32 7
  store i32 %conv175, i32* %arrayidx176, align 4, !tbaa !6
  %167 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err177 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %167, i32 0, i32 0
  %168 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err177, align 8, !tbaa !12
  %msg_code178 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %168, i32 0, i32 5
  store i32 93, i32* %msg_code178, align 4, !tbaa !13
  %169 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err179 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %169, i32 0, i32 0
  %170 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err179, align 8, !tbaa !12
  %emit_message180 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %170, i32 0, i32 1
  %171 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message180, align 4, !tbaa !17
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %173 = bitcast %struct.jpeg_decompress_struct* %172 to %struct.jpeg_common_struct*
  call void %171(%struct.jpeg_common_struct* %173, i32 2)
  %174 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #4
  br label %do.cond181

do.cond181:                                       ; preds = %do.body134
  br label %do.end182

do.end182:                                        ; preds = %do.cond181
  br label %for.inc183

for.inc183:                                       ; preds = %do.end182
  %175 = load i32, i32* %i, align 4, !tbaa !6
  %add184 = add nsw i32 %175, 8
  store i32 %add184, i32* %i, align 4, !tbaa !6
  br label %for.cond130

for.end185:                                       ; preds = %for.cond130
  br label %if.end186

if.end186:                                        ; preds = %for.end185, %for.end
  %176 = load i32, i32* %length, align 4, !tbaa !22
  %sub187 = sub nsw i32 %176, 65
  store i32 %sub187, i32* %length, align 4, !tbaa !22
  %177 = load i32, i32* %prec, align 4, !tbaa !6
  %tobool188 = icmp ne i32 %177, 0
  br i1 %tobool188, label %if.then189, label %if.end191

if.then189:                                       ; preds = %if.end186
  %178 = load i32, i32* %length, align 4, !tbaa !22
  %sub190 = sub nsw i32 %178, 64
  store i32 %sub190, i32* %length, align 4, !tbaa !22
  br label %if.end191

if.end191:                                        ; preds = %if.then189, %if.end186
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %179 = load i32, i32* %length, align 4, !tbaa !22
  %cmp192 = icmp ne i32 %179, 0
  br i1 %cmp192, label %if.then194, label %if.end199

if.then194:                                       ; preds = %while.end
  %180 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err195 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %180, i32 0, i32 0
  %181 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err195, align 8, !tbaa !12
  %msg_code196 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %181, i32 0, i32 5
  store i32 11, i32* %msg_code196, align 4, !tbaa !13
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err197 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 0
  %183 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err197, align 8, !tbaa !12
  %error_exit198 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %183, i32 0, i32 0
  %184 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit198, align 4, !tbaa !43
  %185 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %186 = bitcast %struct.jpeg_decompress_struct* %185 to %struct.jpeg_common_struct*
  call void %184(%struct.jpeg_common_struct* %186)
  br label %if.end199

if.end199:                                        ; preds = %if.then194, %while.end
  %187 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %188 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte200 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %188, i32 0, i32 0
  store i8* %187, i8** %next_input_byte200, align 4, !tbaa !19
  %189 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %190 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer201 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %190, i32 0, i32 1
  store i32 %189, i32* %bytes_in_buffer201, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end199, %if.then112, %if.then94, %if.then79, %if.then30, %if.then13, %if.then3
  %191 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  %192 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #4
  %193 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %194 = bitcast %struct.JQUANT_TBL** %quant_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #4
  %195 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = load i32, i32* %retval, align 4
  ret i32 %200
}

; Function Attrs: nounwind
define internal i32 @get_dri(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %length = alloca i32, align 4
  %tmp = alloca i32, align 4
  %datasrc = alloca %struct.jpeg_source_mgr*, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 6
  %4 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !18
  store %struct.jpeg_source_mgr* %4, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %5 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %6, i32 0, i32 0
  %7 = load i8*, i8** %next_input_byte1, align 4, !tbaa !19
  store i8* %7, i8** %next_input_byte, align 4, !tbaa !2
  %8 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %9, i32 0, i32 1
  %10 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !21
  store i32 %10, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %do.body

do.body:                                          ; preds = %entry
  %11 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp = icmp eq i32 %11, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %do.body
  %12 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %12, i32 0, i32 3
  %13 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !23
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %13(%struct.jpeg_decompress_struct* %14)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 0
  %16 = load i8*, i8** %next_input_byte4, align 4, !tbaa !19
  store i8* %16, i8** %next_input_byte, align 4, !tbaa !2
  %17 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer5 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %17, i32 0, i32 1
  %18 = load i32, i32* %bytes_in_buffer5, align 4, !tbaa !21
  store i32 %18, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.end, %do.body
  %19 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %19, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %20 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !16
  %conv = zext i8 %21 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %length, align 4, !tbaa !22
  %22 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp7 = icmp eq i32 %22, 0
  br i1 %cmp7, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.end6
  %23 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer10 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %23, i32 0, i32 3
  %24 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer10, align 4, !tbaa !23
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call11 = call i32 %24(%struct.jpeg_decompress_struct* %25)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.then9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then9
  %26 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte15 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %26, i32 0, i32 0
  %27 = load i8*, i8** %next_input_byte15, align 4, !tbaa !19
  store i8* %27, i8** %next_input_byte, align 4, !tbaa !2
  %28 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer16 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %28, i32 0, i32 1
  %29 = load i32, i32* %bytes_in_buffer16, align 4, !tbaa !21
  store i32 %29, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end17

if.end17:                                         ; preds = %if.end14, %if.end6
  %30 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec18 = add i32 %30, -1
  store i32 %dec18, i32* %bytes_in_buffer, align 4, !tbaa !22
  %31 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %31, i32 1
  store i8* %incdec.ptr19, i8** %next_input_byte, align 4, !tbaa !2
  %32 = load i8, i8* %31, align 1, !tbaa !16
  %conv20 = zext i8 %32 to i32
  %33 = load i32, i32* %length, align 4, !tbaa !22
  %add = add nsw i32 %33, %conv20
  store i32 %add, i32* %length, align 4, !tbaa !22
  br label %do.cond

do.cond:                                          ; preds = %if.end17
  br label %do.end

do.end:                                           ; preds = %do.cond
  %34 = load i32, i32* %length, align 4, !tbaa !22
  %cmp21 = icmp ne i32 %34, 4
  br i1 %cmp21, label %if.then23, label %if.end25

if.then23:                                        ; preds = %do.end
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 0
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %36, i32 0, i32 5
  store i32 11, i32* %msg_code, align 4, !tbaa !13
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err24 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  %38 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err24, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %38, i32 0, i32 0
  %39 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !43
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = bitcast %struct.jpeg_decompress_struct* %40 to %struct.jpeg_common_struct*
  call void %39(%struct.jpeg_common_struct* %41)
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %do.end
  br label %do.body26

do.body26:                                        ; preds = %if.end25
  %42 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp27 = icmp eq i32 %42, 0
  br i1 %cmp27, label %if.then29, label %if.end37

if.then29:                                        ; preds = %do.body26
  %43 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer30 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %43, i32 0, i32 3
  %44 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer30, align 4, !tbaa !23
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call31 = call i32 %44(%struct.jpeg_decompress_struct* %45)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %if.then29
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end34:                                         ; preds = %if.then29
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte35 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 0
  %47 = load i8*, i8** %next_input_byte35, align 4, !tbaa !19
  store i8* %47, i8** %next_input_byte, align 4, !tbaa !2
  %48 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer36 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %48, i32 0, i32 1
  %49 = load i32, i32* %bytes_in_buffer36, align 4, !tbaa !21
  store i32 %49, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end37

if.end37:                                         ; preds = %if.end34, %do.body26
  %50 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec38 = add i32 %50, -1
  store i32 %dec38, i32* %bytes_in_buffer, align 4, !tbaa !22
  %51 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr39 = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr39, i8** %next_input_byte, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !16
  %conv40 = zext i8 %52 to i32
  %shl41 = shl i32 %conv40, 8
  store i32 %shl41, i32* %tmp, align 4, !tbaa !6
  %53 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp42 = icmp eq i32 %53, 0
  br i1 %cmp42, label %if.then44, label %if.end52

if.then44:                                        ; preds = %if.end37
  %54 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %fill_input_buffer45 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %54, i32 0, i32 3
  %55 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer45, align 4, !tbaa !23
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call46 = call i32 %55(%struct.jpeg_decompress_struct* %56)
  %tobool47 = icmp ne i32 %call46, 0
  br i1 %tobool47, label %if.end49, label %if.then48

if.then48:                                        ; preds = %if.then44
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end49:                                         ; preds = %if.then44
  %57 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte50 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %57, i32 0, i32 0
  %58 = load i8*, i8** %next_input_byte50, align 4, !tbaa !19
  store i8* %58, i8** %next_input_byte, align 4, !tbaa !2
  %59 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer51 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %59, i32 0, i32 1
  %60 = load i32, i32* %bytes_in_buffer51, align 4, !tbaa !21
  store i32 %60, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end52

if.end52:                                         ; preds = %if.end49, %if.end37
  %61 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec53 = add i32 %61, -1
  store i32 %dec53, i32* %bytes_in_buffer, align 4, !tbaa !22
  %62 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr54 = getelementptr inbounds i8, i8* %62, i32 1
  store i8* %incdec.ptr54, i8** %next_input_byte, align 4, !tbaa !2
  %63 = load i8, i8* %62, align 1, !tbaa !16
  %conv55 = zext i8 %63 to i32
  %64 = load i32, i32* %tmp, align 4, !tbaa !6
  %add56 = add i32 %64, %conv55
  store i32 %add56, i32* %tmp, align 4, !tbaa !6
  br label %do.cond57

do.cond57:                                        ; preds = %if.end52
  br label %do.end58

do.end58:                                         ; preds = %do.cond57
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err59 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 0
  %66 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err59, align 8, !tbaa !12
  %msg_code60 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %66, i32 0, i32 5
  store i32 82, i32* %msg_code60, align 4, !tbaa !13
  %67 = load i32, i32* %tmp, align 4, !tbaa !6
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 0
  %69 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err61, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %69, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %67, i32* %arrayidx, align 4, !tbaa !16
  %70 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err62 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %70, i32 0, i32 0
  %71 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err62, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %71, i32 0, i32 1
  %72 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %74 = bitcast %struct.jpeg_decompress_struct* %73 to %struct.jpeg_common_struct*
  call void %72(%struct.jpeg_common_struct* %74, i32 1)
  %75 = load i32, i32* %tmp, align 4, !tbaa !6
  %76 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %76, i32 0, i32 50
  store i32 %75, i32* %restart_interval, align 4, !tbaa !57
  %77 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %78 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %next_input_byte63 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %78, i32 0, i32 0
  store i8* %77, i8** %next_input_byte63, align 4, !tbaa !19
  %79 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %80 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %datasrc, align 4, !tbaa !2
  %bytes_in_buffer64 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %80, i32 0, i32 1
  store i32 %79, i32* %bytes_in_buffer64, align 4, !tbaa !21
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end58, %if.then48, %if.then33, %if.then13, %if.then3
  %81 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast %struct.jpeg_source_mgr** %datasrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = load i32, i32* %retval, align 4
  ret i32 %86
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define internal void @examine_app0(%struct.jpeg_decompress_struct* %cinfo, i8* %data, i32 %datalen, i32 %remaining) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %data.addr = alloca i8*, align 4
  %datalen.addr = alloca i32, align 4
  %remaining.addr = alloca i32, align 4
  %totallen = alloca i32, align 4
  %_mp = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %datalen, i32* %datalen.addr, align 4, !tbaa !6
  store i32 %remaining, i32* %remaining.addr, align 4, !tbaa !22
  %0 = bitcast i32* %totallen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %2 = load i32, i32* %remaining.addr, align 4, !tbaa !22
  %add = add nsw i32 %1, %2
  store i32 %add, i32* %totallen, align 4, !tbaa !22
  %3 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %cmp = icmp uge i32 %3, 14
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %4 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 0
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !16
  %conv = zext i8 %5 to i32
  %cmp1 = icmp eq i32 %conv, 74
  br i1 %cmp1, label %land.lhs.true3, label %if.else

land.lhs.true3:                                   ; preds = %land.lhs.true
  %6 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 1
  %7 = load i8, i8* %arrayidx4, align 1, !tbaa !16
  %conv5 = zext i8 %7 to i32
  %cmp6 = icmp eq i32 %conv5, 70
  br i1 %cmp6, label %land.lhs.true8, label %if.else

land.lhs.true8:                                   ; preds = %land.lhs.true3
  %8 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %8, i32 2
  %9 = load i8, i8* %arrayidx9, align 1, !tbaa !16
  %conv10 = zext i8 %9 to i32
  %cmp11 = icmp eq i32 %conv10, 73
  br i1 %cmp11, label %land.lhs.true13, label %if.else

land.lhs.true13:                                  ; preds = %land.lhs.true8
  %10 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %10, i32 3
  %11 = load i8, i8* %arrayidx14, align 1, !tbaa !16
  %conv15 = zext i8 %11 to i32
  %cmp16 = icmp eq i32 %conv15, 70
  br i1 %cmp16, label %land.lhs.true18, label %if.else

land.lhs.true18:                                  ; preds = %land.lhs.true13
  %12 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %12, i32 4
  %13 = load i8, i8* %arrayidx19, align 1, !tbaa !16
  %conv20 = zext i8 %13 to i32
  %cmp21 = icmp eq i32 %conv20, 0
  br i1 %cmp21, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true18
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_JFIF_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 51
  store i32 1, i32* %saw_JFIF_marker, align 8, !tbaa !60
  %15 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i8, i8* %15, i32 5
  %16 = load i8, i8* %arrayidx23, align 1, !tbaa !16
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 52
  store i8 %16, i8* %JFIF_major_version, align 4, !tbaa !61
  %18 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %18, i32 6
  %19 = load i8, i8* %arrayidx24, align 1, !tbaa !16
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 53
  store i8 %19, i8* %JFIF_minor_version, align 1, !tbaa !62
  %21 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %21, i32 7
  %22 = load i8, i8* %arrayidx25, align 1, !tbaa !16
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %density_unit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 54
  store i8 %22, i8* %density_unit, align 2, !tbaa !63
  %24 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %24, i32 8
  %25 = load i8, i8* %arrayidx26, align 1, !tbaa !16
  %conv27 = zext i8 %25 to i32
  %shl = shl i32 %conv27, 8
  %26 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i8, i8* %26, i32 9
  %27 = load i8, i8* %arrayidx28, align 1, !tbaa !16
  %conv29 = zext i8 %27 to i32
  %add30 = add nsw i32 %shl, %conv29
  %conv31 = trunc i32 %add30 to i16
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %X_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 55
  store i16 %conv31, i16* %X_density, align 8, !tbaa !64
  %29 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i8, i8* %29, i32 10
  %30 = load i8, i8* %arrayidx32, align 1, !tbaa !16
  %conv33 = zext i8 %30 to i32
  %shl34 = shl i32 %conv33, 8
  %31 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i8, i8* %31, i32 11
  %32 = load i8, i8* %arrayidx35, align 1, !tbaa !16
  %conv36 = zext i8 %32 to i32
  %add37 = add nsw i32 %shl34, %conv36
  %conv38 = trunc i32 %add37 to i16
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Y_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 56
  store i16 %conv38, i16* %Y_density, align 2, !tbaa !65
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 52
  %35 = load i8, i8* %JFIF_major_version39, align 4, !tbaa !61
  %conv40 = zext i8 %35 to i32
  %cmp41 = icmp ne i32 %conv40, 1
  br i1 %cmp41, label %if.then43, label %if.end

if.then43:                                        ; preds = %if.then
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 5
  store i32 119, i32* %msg_code, align 4, !tbaa !13
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version44 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 52
  %39 = load i8, i8* %JFIF_major_version44, align 4, !tbaa !61
  %conv45 = zext i8 %39 to i32
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx47 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %conv45, i32* %arrayidx47, align 4, !tbaa !16
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 53
  %43 = load i8, i8* %JFIF_minor_version48, align 1, !tbaa !62
  %conv49 = zext i8 %43 to i32
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 0
  %45 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err50, align 8, !tbaa !12
  %msg_parm51 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %45, i32 0, i32 6
  %i52 = bitcast %union.anon* %msg_parm51 to [8 x i32]*
  %arrayidx53 = getelementptr inbounds [8 x i32], [8 x i32]* %i52, i32 0, i32 1
  store i32 %conv49, i32* %arrayidx53, align 4, !tbaa !16
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err54, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 1
  %48 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %50 = bitcast %struct.jpeg_decompress_struct* %49 to %struct.jpeg_common_struct*
  call void %48(%struct.jpeg_common_struct* %50, i32 -1)
  br label %if.end

if.end:                                           ; preds = %if.then43, %if.then
  br label %do.body

do.body:                                          ; preds = %if.end
  %51 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #4
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err55, align 8, !tbaa !12
  %msg_parm56 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 6
  %i57 = bitcast %union.anon* %msg_parm56 to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i57, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 52
  %55 = load i8, i8* %JFIF_major_version58, align 4, !tbaa !61
  %conv59 = zext i8 %55 to i32
  %56 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %56, i32 0
  store i32 %conv59, i32* %arrayidx60, align 4, !tbaa !6
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 53
  %58 = load i8, i8* %JFIF_minor_version61, align 1, !tbaa !62
  %conv62 = zext i8 %58 to i32
  %59 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %59, i32 1
  store i32 %conv62, i32* %arrayidx63, align 4, !tbaa !6
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %X_density64 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 55
  %61 = load i16, i16* %X_density64, align 8, !tbaa !64
  %conv65 = zext i16 %61 to i32
  %62 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %62, i32 2
  store i32 %conv65, i32* %arrayidx66, align 4, !tbaa !6
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Y_density67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 56
  %64 = load i16, i16* %Y_density67, align 2, !tbaa !65
  %conv68 = zext i16 %64 to i32
  %65 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %65, i32 3
  store i32 %conv68, i32* %arrayidx69, align 4, !tbaa !6
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %density_unit70 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 54
  %67 = load i8, i8* %density_unit70, align 2, !tbaa !63
  %conv71 = zext i8 %67 to i32
  %68 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %68, i32 4
  store i32 %conv71, i32* %arrayidx72, align 4, !tbaa !6
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err73, align 8, !tbaa !12
  %msg_code74 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 5
  store i32 87, i32* %msg_code74, align 4, !tbaa !13
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err75 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 0
  %72 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err75, align 8, !tbaa !12
  %emit_message76 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %72, i32 0, i32 1
  %73 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message76, align 4, !tbaa !17
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %75 = bitcast %struct.jpeg_decompress_struct* %74 to %struct.jpeg_common_struct*
  call void %73(%struct.jpeg_common_struct* %75, i32 1)
  %76 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %do.cond
  %77 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i8, i8* %77, i32 12
  %78 = load i8, i8* %arrayidx77, align 1, !tbaa !16
  %conv78 = zext i8 %78 to i32
  %79 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %79, i32 13
  %80 = load i8, i8* %arrayidx79, align 1, !tbaa !16
  %conv80 = zext i8 %80 to i32
  %or = or i32 %conv78, %conv80
  %tobool = icmp ne i32 %or, 0
  br i1 %tobool, label %if.then81, label %if.end98

if.then81:                                        ; preds = %do.end
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err82 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 0
  %82 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err82, align 8, !tbaa !12
  %msg_code83 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %82, i32 0, i32 5
  store i32 90, i32* %msg_code83, align 4, !tbaa !13
  %83 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i8, i8* %83, i32 12
  %84 = load i8, i8* %arrayidx84, align 1, !tbaa !16
  %conv85 = zext i8 %84 to i32
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err86 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 0
  %86 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err86, align 8, !tbaa !12
  %msg_parm87 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %86, i32 0, i32 6
  %i88 = bitcast %union.anon* %msg_parm87 to [8 x i32]*
  %arrayidx89 = getelementptr inbounds [8 x i32], [8 x i32]* %i88, i32 0, i32 0
  store i32 %conv85, i32* %arrayidx89, align 4, !tbaa !16
  %87 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i8, i8* %87, i32 13
  %88 = load i8, i8* %arrayidx90, align 1, !tbaa !16
  %conv91 = zext i8 %88 to i32
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err92 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 0
  %90 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err92, align 8, !tbaa !12
  %msg_parm93 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %90, i32 0, i32 6
  %i94 = bitcast %union.anon* %msg_parm93 to [8 x i32]*
  %arrayidx95 = getelementptr inbounds [8 x i32], [8 x i32]* %i94, i32 0, i32 1
  store i32 %conv91, i32* %arrayidx95, align 4, !tbaa !16
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err96 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 0
  %92 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err96, align 8, !tbaa !12
  %emit_message97 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %92, i32 0, i32 1
  %93 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message97, align 4, !tbaa !17
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %95 = bitcast %struct.jpeg_decompress_struct* %94 to %struct.jpeg_common_struct*
  call void %93(%struct.jpeg_common_struct* %95, i32 1)
  br label %if.end98

if.end98:                                         ; preds = %if.then81, %do.end
  %96 = load i32, i32* %totallen, align 4, !tbaa !22
  %sub = sub nsw i32 %96, 14
  store i32 %sub, i32* %totallen, align 4, !tbaa !22
  %97 = load i32, i32* %totallen, align 4, !tbaa !22
  %98 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i8, i8* %98, i32 12
  %99 = load i8, i8* %arrayidx99, align 1, !tbaa !16
  %conv100 = zext i8 %99 to i32
  %100 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %100, i32 13
  %101 = load i8, i8* %arrayidx101, align 1, !tbaa !16
  %conv102 = zext i8 %101 to i32
  %mul = mul nsw i32 %conv100, %conv102
  %mul103 = mul nsw i32 %mul, 3
  %cmp104 = icmp ne i32 %97, %mul103
  br i1 %cmp104, label %if.then106, label %if.end115

if.then106:                                       ; preds = %if.end98
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err107 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 0
  %103 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err107, align 8, !tbaa !12
  %msg_code108 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %103, i32 0, i32 5
  store i32 88, i32* %msg_code108, align 4, !tbaa !13
  %104 = load i32, i32* %totallen, align 4, !tbaa !22
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err109 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 0
  %106 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err109, align 8, !tbaa !12
  %msg_parm110 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %106, i32 0, i32 6
  %i111 = bitcast %union.anon* %msg_parm110 to [8 x i32]*
  %arrayidx112 = getelementptr inbounds [8 x i32], [8 x i32]* %i111, i32 0, i32 0
  store i32 %104, i32* %arrayidx112, align 4, !tbaa !16
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err113 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %107, i32 0, i32 0
  %108 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err113, align 8, !tbaa !12
  %emit_message114 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %108, i32 0, i32 1
  %109 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message114, align 4, !tbaa !17
  %110 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %111 = bitcast %struct.jpeg_decompress_struct* %110 to %struct.jpeg_common_struct*
  call void %109(%struct.jpeg_common_struct* %111, i32 1)
  br label %if.end115

if.end115:                                        ; preds = %if.then106, %if.end98
  br label %if.end196

if.else:                                          ; preds = %land.lhs.true18, %land.lhs.true13, %land.lhs.true8, %land.lhs.true3, %land.lhs.true, %entry
  %112 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %cmp116 = icmp uge i32 %112, 6
  br i1 %cmp116, label %land.lhs.true118, label %if.else186

land.lhs.true118:                                 ; preds = %if.else
  %113 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i8, i8* %113, i32 0
  %114 = load i8, i8* %arrayidx119, align 1, !tbaa !16
  %conv120 = zext i8 %114 to i32
  %cmp121 = icmp eq i32 %conv120, 74
  br i1 %cmp121, label %land.lhs.true123, label %if.else186

land.lhs.true123:                                 ; preds = %land.lhs.true118
  %115 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i8, i8* %115, i32 1
  %116 = load i8, i8* %arrayidx124, align 1, !tbaa !16
  %conv125 = zext i8 %116 to i32
  %cmp126 = icmp eq i32 %conv125, 70
  br i1 %cmp126, label %land.lhs.true128, label %if.else186

land.lhs.true128:                                 ; preds = %land.lhs.true123
  %117 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i8, i8* %117, i32 2
  %118 = load i8, i8* %arrayidx129, align 1, !tbaa !16
  %conv130 = zext i8 %118 to i32
  %cmp131 = icmp eq i32 %conv130, 88
  br i1 %cmp131, label %land.lhs.true133, label %if.else186

land.lhs.true133:                                 ; preds = %land.lhs.true128
  %119 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds i8, i8* %119, i32 3
  %120 = load i8, i8* %arrayidx134, align 1, !tbaa !16
  %conv135 = zext i8 %120 to i32
  %cmp136 = icmp eq i32 %conv135, 88
  br i1 %cmp136, label %land.lhs.true138, label %if.else186

land.lhs.true138:                                 ; preds = %land.lhs.true133
  %121 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i8, i8* %121, i32 4
  %122 = load i8, i8* %arrayidx139, align 1, !tbaa !16
  %conv140 = zext i8 %122 to i32
  %cmp141 = icmp eq i32 %conv140, 0
  br i1 %cmp141, label %if.then143, label %if.else186

if.then143:                                       ; preds = %land.lhs.true138
  %123 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i8, i8* %123, i32 5
  %124 = load i8, i8* %arrayidx144, align 1, !tbaa !16
  %conv145 = zext i8 %124 to i32
  switch i32 %conv145, label %sw.default [
    i32 16, label %sw.bb
    i32 17, label %sw.bb154
    i32 19, label %sw.bb163
  ]

sw.bb:                                            ; preds = %if.then143
  %125 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err146 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %125, i32 0, i32 0
  %126 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err146, align 8, !tbaa !12
  %msg_code147 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %126, i32 0, i32 5
  store i32 108, i32* %msg_code147, align 4, !tbaa !13
  %127 = load i32, i32* %totallen, align 4, !tbaa !22
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err148 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 0
  %129 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err148, align 8, !tbaa !12
  %msg_parm149 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %129, i32 0, i32 6
  %i150 = bitcast %union.anon* %msg_parm149 to [8 x i32]*
  %arrayidx151 = getelementptr inbounds [8 x i32], [8 x i32]* %i150, i32 0, i32 0
  store i32 %127, i32* %arrayidx151, align 4, !tbaa !16
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err152 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 0
  %131 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err152, align 8, !tbaa !12
  %emit_message153 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %131, i32 0, i32 1
  %132 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message153, align 4, !tbaa !17
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %134 = bitcast %struct.jpeg_decompress_struct* %133 to %struct.jpeg_common_struct*
  call void %132(%struct.jpeg_common_struct* %134, i32 1)
  br label %sw.epilog

sw.bb154:                                         ; preds = %if.then143
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err155 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %135, i32 0, i32 0
  %136 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err155, align 8, !tbaa !12
  %msg_code156 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %136, i32 0, i32 5
  store i32 109, i32* %msg_code156, align 4, !tbaa !13
  %137 = load i32, i32* %totallen, align 4, !tbaa !22
  %138 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err157 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %138, i32 0, i32 0
  %139 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err157, align 8, !tbaa !12
  %msg_parm158 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %139, i32 0, i32 6
  %i159 = bitcast %union.anon* %msg_parm158 to [8 x i32]*
  %arrayidx160 = getelementptr inbounds [8 x i32], [8 x i32]* %i159, i32 0, i32 0
  store i32 %137, i32* %arrayidx160, align 4, !tbaa !16
  %140 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err161 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %140, i32 0, i32 0
  %141 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err161, align 8, !tbaa !12
  %emit_message162 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %141, i32 0, i32 1
  %142 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message162, align 4, !tbaa !17
  %143 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %144 = bitcast %struct.jpeg_decompress_struct* %143 to %struct.jpeg_common_struct*
  call void %142(%struct.jpeg_common_struct* %144, i32 1)
  br label %sw.epilog

sw.bb163:                                         ; preds = %if.then143
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err164 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %145, i32 0, i32 0
  %146 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err164, align 8, !tbaa !12
  %msg_code165 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %146, i32 0, i32 5
  store i32 110, i32* %msg_code165, align 4, !tbaa !13
  %147 = load i32, i32* %totallen, align 4, !tbaa !22
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err166 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 0
  %149 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err166, align 8, !tbaa !12
  %msg_parm167 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %149, i32 0, i32 6
  %i168 = bitcast %union.anon* %msg_parm167 to [8 x i32]*
  %arrayidx169 = getelementptr inbounds [8 x i32], [8 x i32]* %i168, i32 0, i32 0
  store i32 %147, i32* %arrayidx169, align 4, !tbaa !16
  %150 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err170 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %150, i32 0, i32 0
  %151 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err170, align 8, !tbaa !12
  %emit_message171 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %151, i32 0, i32 1
  %152 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message171, align 4, !tbaa !17
  %153 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %154 = bitcast %struct.jpeg_decompress_struct* %153 to %struct.jpeg_common_struct*
  call void %152(%struct.jpeg_common_struct* %154, i32 1)
  br label %sw.epilog

sw.default:                                       ; preds = %if.then143
  %155 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err172 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %155, i32 0, i32 0
  %156 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err172, align 8, !tbaa !12
  %msg_code173 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %156, i32 0, i32 5
  store i32 89, i32* %msg_code173, align 4, !tbaa !13
  %157 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i8, i8* %157, i32 5
  %158 = load i8, i8* %arrayidx174, align 1, !tbaa !16
  %conv175 = zext i8 %158 to i32
  %159 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err176 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %159, i32 0, i32 0
  %160 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err176, align 8, !tbaa !12
  %msg_parm177 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %160, i32 0, i32 6
  %i178 = bitcast %union.anon* %msg_parm177 to [8 x i32]*
  %arrayidx179 = getelementptr inbounds [8 x i32], [8 x i32]* %i178, i32 0, i32 0
  store i32 %conv175, i32* %arrayidx179, align 4, !tbaa !16
  %161 = load i32, i32* %totallen, align 4, !tbaa !22
  %162 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err180 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %162, i32 0, i32 0
  %163 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err180, align 8, !tbaa !12
  %msg_parm181 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %163, i32 0, i32 6
  %i182 = bitcast %union.anon* %msg_parm181 to [8 x i32]*
  %arrayidx183 = getelementptr inbounds [8 x i32], [8 x i32]* %i182, i32 0, i32 1
  store i32 %161, i32* %arrayidx183, align 4, !tbaa !16
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err184 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 0
  %165 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err184, align 8, !tbaa !12
  %emit_message185 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %165, i32 0, i32 1
  %166 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message185, align 4, !tbaa !17
  %167 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %168 = bitcast %struct.jpeg_decompress_struct* %167 to %struct.jpeg_common_struct*
  call void %166(%struct.jpeg_common_struct* %168, i32 1)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb163, %sw.bb154, %sw.bb
  br label %if.end195

if.else186:                                       ; preds = %land.lhs.true138, %land.lhs.true133, %land.lhs.true128, %land.lhs.true123, %land.lhs.true118, %if.else
  %169 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err187 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %169, i32 0, i32 0
  %170 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err187, align 8, !tbaa !12
  %msg_code188 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %170, i32 0, i32 5
  store i32 77, i32* %msg_code188, align 4, !tbaa !13
  %171 = load i32, i32* %totallen, align 4, !tbaa !22
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err189 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %172, i32 0, i32 0
  %173 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err189, align 8, !tbaa !12
  %msg_parm190 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %173, i32 0, i32 6
  %i191 = bitcast %union.anon* %msg_parm190 to [8 x i32]*
  %arrayidx192 = getelementptr inbounds [8 x i32], [8 x i32]* %i191, i32 0, i32 0
  store i32 %171, i32* %arrayidx192, align 4, !tbaa !16
  %174 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err193 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %174, i32 0, i32 0
  %175 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err193, align 8, !tbaa !12
  %emit_message194 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %175, i32 0, i32 1
  %176 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message194, align 4, !tbaa !17
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %178 = bitcast %struct.jpeg_decompress_struct* %177 to %struct.jpeg_common_struct*
  call void %176(%struct.jpeg_common_struct* %178, i32 1)
  br label %if.end195

if.end195:                                        ; preds = %if.else186, %sw.epilog
  br label %if.end196

if.end196:                                        ; preds = %if.end195, %if.end115
  %179 = bitcast i32* %totallen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #4
  ret void
}

; Function Attrs: nounwind
define internal void @examine_app14(%struct.jpeg_decompress_struct* %cinfo, i8* %data, i32 %datalen, i32 %remaining) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %data.addr = alloca i8*, align 4
  %datalen.addr = alloca i32, align 4
  %remaining.addr = alloca i32, align 4
  %version = alloca i32, align 4
  %flags0 = alloca i32, align 4
  %flags1 = alloca i32, align 4
  %transform = alloca i32, align 4
  %_mp = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %datalen, i32* %datalen.addr, align 4, !tbaa !6
  store i32 %remaining, i32* %remaining.addr, align 4, !tbaa !22
  %0 = bitcast i32* %version to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %flags0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %flags1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %transform to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %cmp = icmp uge i32 %4, 12
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %5 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 0
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !16
  %conv = zext i8 %6 to i32
  %cmp1 = icmp eq i32 %conv, 65
  br i1 %cmp1, label %land.lhs.true3, label %if.else

land.lhs.true3:                                   ; preds = %land.lhs.true
  %7 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %7, i32 1
  %8 = load i8, i8* %arrayidx4, align 1, !tbaa !16
  %conv5 = zext i8 %8 to i32
  %cmp6 = icmp eq i32 %conv5, 100
  br i1 %cmp6, label %land.lhs.true8, label %if.else

land.lhs.true8:                                   ; preds = %land.lhs.true3
  %9 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %9, i32 2
  %10 = load i8, i8* %arrayidx9, align 1, !tbaa !16
  %conv10 = zext i8 %10 to i32
  %cmp11 = icmp eq i32 %conv10, 111
  br i1 %cmp11, label %land.lhs.true13, label %if.else

land.lhs.true13:                                  ; preds = %land.lhs.true8
  %11 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx14, align 1, !tbaa !16
  %conv15 = zext i8 %12 to i32
  %cmp16 = icmp eq i32 %conv15, 98
  br i1 %cmp16, label %land.lhs.true18, label %if.else

land.lhs.true18:                                  ; preds = %land.lhs.true13
  %13 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %13, i32 4
  %14 = load i8, i8* %arrayidx19, align 1, !tbaa !16
  %conv20 = zext i8 %14 to i32
  %cmp21 = icmp eq i32 %conv20, 101
  br i1 %cmp21, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true18
  %15 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i8, i8* %15, i32 5
  %16 = load i8, i8* %arrayidx23, align 1, !tbaa !16
  %conv24 = zext i8 %16 to i32
  %shl = shl i32 %conv24, 8
  %17 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %17, i32 6
  %18 = load i8, i8* %arrayidx25, align 1, !tbaa !16
  %conv26 = zext i8 %18 to i32
  %add = add nsw i32 %shl, %conv26
  store i32 %add, i32* %version, align 4, !tbaa !6
  %19 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %19, i32 7
  %20 = load i8, i8* %arrayidx27, align 1, !tbaa !16
  %conv28 = zext i8 %20 to i32
  %shl29 = shl i32 %conv28, 8
  %21 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %21, i32 8
  %22 = load i8, i8* %arrayidx30, align 1, !tbaa !16
  %conv31 = zext i8 %22 to i32
  %add32 = add nsw i32 %shl29, %conv31
  store i32 %add32, i32* %flags0, align 4, !tbaa !6
  %23 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i8, i8* %23, i32 9
  %24 = load i8, i8* %arrayidx33, align 1, !tbaa !16
  %conv34 = zext i8 %24 to i32
  %shl35 = shl i32 %conv34, 8
  %25 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %25, i32 10
  %26 = load i8, i8* %arrayidx36, align 1, !tbaa !16
  %conv37 = zext i8 %26 to i32
  %add38 = add nsw i32 %shl35, %conv37
  store i32 %add38, i32* %flags1, align 4, !tbaa !6
  %27 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %27, i32 11
  %28 = load i8, i8* %arrayidx39, align 1, !tbaa !16
  %conv40 = zext i8 %28 to i32
  store i32 %conv40, i32* %transform, align 4, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %if.then
  %29 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %32 = load i32, i32* %version, align 4, !tbaa !6
  %33 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %33, i32 0
  store i32 %32, i32* %arrayidx41, align 4, !tbaa !6
  %34 = load i32, i32* %flags0, align 4, !tbaa !6
  %35 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %35, i32 1
  store i32 %34, i32* %arrayidx42, align 4, !tbaa !6
  %36 = load i32, i32* %flags1, align 4, !tbaa !6
  %37 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %37, i32 2
  store i32 %36, i32* %arrayidx43, align 4, !tbaa !6
  %38 = load i32, i32* %transform, align 4, !tbaa !6
  %39 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %39, i32 3
  store i32 %38, i32* %arrayidx44, align 4, !tbaa !6
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err45, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 5
  store i32 76, i32* %msg_code, align 4, !tbaa !13
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 0
  %43 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !12
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %43, i32 0, i32 1
  %44 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !17
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %46 = bitcast %struct.jpeg_decompress_struct* %45 to %struct.jpeg_common_struct*
  call void %44(%struct.jpeg_common_struct* %46, i32 1)
  %47 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %do.cond
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_Adobe_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 57
  store i32 1, i32* %saw_Adobe_marker, align 4, !tbaa !66
  %49 = load i32, i32* %transform, align 4, !tbaa !6
  %conv47 = trunc i32 %49 to i8
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 58
  store i8 %conv47, i8* %Adobe_transform, align 8, !tbaa !67
  br label %if.end

if.else:                                          ; preds = %land.lhs.true18, %land.lhs.true13, %land.lhs.true8, %land.lhs.true3, %land.lhs.true, %entry
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 0
  %52 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err48, align 8, !tbaa !12
  %msg_code49 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %52, i32 0, i32 5
  store i32 78, i32* %msg_code49, align 4, !tbaa !13
  %53 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %54 = load i32, i32* %remaining.addr, align 4, !tbaa !22
  %add50 = add i32 %53, %54
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err51 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err51, align 8, !tbaa !12
  %msg_parm52 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 6
  %i53 = bitcast %union.anon* %msg_parm52 to [8 x i32]*
  %arrayidx54 = getelementptr inbounds [8 x i32], [8 x i32]* %i53, i32 0, i32 0
  store i32 %add50, i32* %arrayidx54, align 4, !tbaa !16
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 0
  %58 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err55, align 8, !tbaa !12
  %emit_message56 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %58, i32 0, i32 1
  %59 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message56, align 4, !tbaa !17
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %61 = bitcast %struct.jpeg_decompress_struct* %60 to %struct.jpeg_common_struct*
  call void %59(%struct.jpeg_common_struct* %61, i32 1)
  br label %if.end

if.end:                                           ; preds = %if.else, %do.end
  %62 = bitcast i32* %transform to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %63 = bitcast i32* %flags1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  %64 = bitcast i32* %flags0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  %65 = bitcast i32* %version to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 416}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!9, !3, i64 0}
!13 = !{!14, !7, i64 20}
!14 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !15, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!15 = !{!"long", !4, i64 0}
!16 = !{!4, !4, i64 0}
!17 = !{!14, !3, i64 4}
!18 = !{!9, !3, i64 24}
!19 = !{!20, !3, i64 0}
!20 = !{!"jpeg_source_mgr", !3, i64 0, !15, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!21 = !{!20, !15, i64 4}
!22 = !{!15, !15, i64 0}
!23 = !{!20, !3, i64 12}
!24 = !{!9, !3, i64 440}
!25 = !{!26, !7, i64 24}
!26 = !{!"jpeg_marker_reader", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24}
!27 = !{!9, !3, i64 4}
!28 = !{!29, !3, i64 0}
!29 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !15, i64 44, !15, i64 48}
!30 = !{!31, !3, i64 0}
!31 = !{!"", !26, i64 0, !3, i64 28, !4, i64 32, !7, i64 96, !4, i64 100, !3, i64 164, !7, i64 168}
!32 = !{!31, !3, i64 4}
!33 = !{!31, !3, i64 8}
!34 = !{!31, !3, i64 28}
!35 = !{!31, !7, i64 96}
!36 = !{!9, !3, i64 216}
!37 = !{!9, !7, i64 144}
!38 = !{!31, !7, i64 12}
!39 = !{!31, !7, i64 16}
!40 = !{!31, !7, i64 24}
!41 = !{!31, !3, i64 164}
!42 = !{!26, !7, i64 12}
!43 = !{!14, !3, i64 0}
!44 = !{!26, !7, i64 20}
!45 = !{!20, !3, i64 20}
!46 = !{!20, !3, i64 16}
!47 = !{!29, !15, i64 48}
!48 = !{!29, !3, i64 4}
!49 = !{!50, !3, i64 0}
!50 = !{!"jpeg_marker_struct", !3, i64 0, !4, i64 4, !7, i64 8, !7, i64 12, !3, i64 16}
!51 = !{!50, !4, i64 4}
!52 = !{!50, !7, i64 8}
!53 = !{!50, !7, i64 12}
!54 = !{!50, !3, i64 16}
!55 = !{!31, !7, i64 168}
!56 = !{!9, !3, i64 304}
!57 = !{!9, !7, i64 276}
!58 = !{!9, !4, i64 40}
!59 = !{!9, !7, i64 300}
!60 = !{!9, !7, i64 280}
!61 = !{!9, !4, i64 284}
!62 = !{!9, !4, i64 285}
!63 = !{!9, !4, i64 286}
!64 = !{!9, !11, i64 288}
!65 = !{!9, !11, i64 290}
!66 = !{!9, !7, i64 292}
!67 = !{!9, !4, i64 296}
!68 = !{!9, !7, i64 220}
!69 = !{!9, !7, i64 224}
!70 = !{!9, !7, i64 212}
!71 = !{!9, !7, i64 32}
!72 = !{!9, !7, i64 28}
!73 = !{!9, !7, i64 36}
!74 = !{!26, !7, i64 16}
!75 = !{!76, !7, i64 4}
!76 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!77 = !{!76, !7, i64 0}
!78 = !{!76, !7, i64 8}
!79 = !{!76, !7, i64 12}
!80 = !{!76, !7, i64 16}
!81 = !{!9, !7, i64 328}
!82 = !{!76, !7, i64 20}
!83 = !{!76, !7, i64 24}
!84 = !{!9, !7, i64 400}
!85 = !{!9, !7, i64 404}
!86 = !{!9, !7, i64 408}
!87 = !{!9, !7, i64 412}
!88 = !{!11, !11, i64 0}
!89 = !{!14, !7, i64 104}
