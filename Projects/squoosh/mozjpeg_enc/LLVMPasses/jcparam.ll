; ModuleID = 'jcparam.c'
source_filename = "jcparam.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }

@std_luminance_quant_tbl = internal constant [9 x [64 x i32]] [[64 x i32] [i32 16, i32 11, i32 10, i32 16, i32 24, i32 40, i32 51, i32 61, i32 12, i32 12, i32 14, i32 19, i32 26, i32 58, i32 60, i32 55, i32 14, i32 13, i32 16, i32 24, i32 40, i32 57, i32 69, i32 56, i32 14, i32 17, i32 22, i32 29, i32 51, i32 87, i32 80, i32 62, i32 18, i32 22, i32 37, i32 56, i32 68, i32 109, i32 103, i32 77, i32 24, i32 35, i32 55, i32 64, i32 81, i32 104, i32 113, i32 92, i32 49, i32 64, i32 78, i32 87, i32 103, i32 121, i32 120, i32 101, i32 72, i32 92, i32 95, i32 98, i32 112, i32 100, i32 103, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16], [64 x i32] [i32 12, i32 17, i32 20, i32 21, i32 30, i32 34, i32 56, i32 63, i32 18, i32 20, i32 20, i32 26, i32 28, i32 51, i32 61, i32 55, i32 19, i32 20, i32 21, i32 26, i32 33, i32 58, i32 69, i32 55, i32 26, i32 26, i32 26, i32 30, i32 46, i32 87, i32 86, i32 66, i32 31, i32 33, i32 36, i32 40, i32 46, i32 96, i32 100, i32 73, i32 40, i32 35, i32 46, i32 62, i32 81, i32 100, i32 111, i32 91, i32 46, i32 66, i32 76, i32 86, i32 102, i32 121, i32 120, i32 101, i32 68, i32 90, i32 90, i32 96, i32 113, i32 102, i32 105, i32 103], [64 x i32] [i32 16, i32 16, i32 16, i32 18, i32 25, i32 37, i32 56, i32 85, i32 16, i32 17, i32 20, i32 27, i32 34, i32 40, i32 53, i32 75, i32 16, i32 20, i32 24, i32 31, i32 43, i32 62, i32 91, i32 135, i32 18, i32 27, i32 31, i32 40, i32 53, i32 74, i32 106, i32 156, i32 25, i32 34, i32 43, i32 53, i32 69, i32 94, i32 131, i32 189, i32 37, i32 40, i32 62, i32 74, i32 94, i32 124, i32 169, i32 238, i32 56, i32 53, i32 91, i32 106, i32 131, i32 169, i32 226, i32 311, i32 85, i32 75, i32 135, i32 156, i32 189, i32 238, i32 311, i32 418], [64 x i32] [i32 9, i32 10, i32 12, i32 14, i32 27, i32 32, i32 51, i32 62, i32 11, i32 12, i32 14, i32 19, i32 27, i32 44, i32 59, i32 73, i32 12, i32 14, i32 18, i32 25, i32 42, i32 59, i32 79, i32 78, i32 17, i32 18, i32 25, i32 42, i32 61, i32 92, i32 87, i32 92, i32 23, i32 28, i32 42, i32 75, i32 79, i32 112, i32 112, i32 99, i32 40, i32 42, i32 59, i32 84, i32 88, i32 124, i32 132, i32 111, i32 42, i32 64, i32 78, i32 95, i32 105, i32 126, i32 125, i32 99, i32 70, i32 75, i32 100, i32 102, i32 116, i32 100, i32 107, i32 98], [64 x i32] [i32 10, i32 12, i32 14, i32 19, i32 26, i32 38, i32 57, i32 86, i32 12, i32 18, i32 21, i32 28, i32 35, i32 41, i32 54, i32 76, i32 14, i32 21, i32 25, i32 32, i32 44, i32 63, i32 92, i32 136, i32 19, i32 28, i32 32, i32 41, i32 54, i32 75, i32 107, i32 157, i32 26, i32 35, i32 44, i32 54, i32 70, i32 95, i32 132, i32 190, i32 38, i32 41, i32 63, i32 75, i32 95, i32 125, i32 170, i32 239, i32 57, i32 54, i32 92, i32 107, i32 132, i32 170, i32 227, i32 312, i32 86, i32 76, i32 136, i32 157, i32 190, i32 239, i32 312, i32 419], [64 x i32] [i32 7, i32 8, i32 10, i32 14, i32 23, i32 44, i32 95, i32 241, i32 8, i32 8, i32 11, i32 15, i32 25, i32 47, i32 102, i32 255, i32 10, i32 11, i32 13, i32 19, i32 31, i32 58, i32 127, i32 255, i32 14, i32 15, i32 19, i32 27, i32 44, i32 83, i32 181, i32 255, i32 23, i32 25, i32 31, i32 44, i32 72, i32 136, i32 255, i32 255, i32 44, i32 47, i32 58, i32 83, i32 136, i32 255, i32 255, i32 255, i32 95, i32 102, i32 127, i32 181, i32 255, i32 255, i32 255, i32 255, i32 241, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255], [64 x i32] [i32 15, i32 11, i32 11, i32 12, i32 15, i32 19, i32 25, i32 32, i32 11, i32 13, i32 10, i32 10, i32 12, i32 15, i32 19, i32 24, i32 11, i32 10, i32 14, i32 14, i32 16, i32 18, i32 22, i32 27, i32 12, i32 10, i32 14, i32 18, i32 21, i32 24, i32 28, i32 33, i32 15, i32 12, i32 16, i32 21, i32 26, i32 31, i32 36, i32 42, i32 19, i32 15, i32 18, i32 24, i32 31, i32 38, i32 45, i32 53, i32 25, i32 19, i32 22, i32 28, i32 36, i32 45, i32 55, i32 65, i32 32, i32 24, i32 27, i32 33, i32 42, i32 53, i32 65, i32 77], [64 x i32] [i32 14, i32 10, i32 11, i32 14, i32 19, i32 25, i32 34, i32 45, i32 10, i32 11, i32 11, i32 12, i32 15, i32 20, i32 26, i32 33, i32 11, i32 11, i32 15, i32 18, i32 21, i32 25, i32 31, i32 38, i32 14, i32 12, i32 18, i32 24, i32 28, i32 33, i32 39, i32 47, i32 19, i32 15, i32 21, i32 28, i32 36, i32 43, i32 51, i32 59, i32 25, i32 20, i32 25, i32 33, i32 43, i32 54, i32 64, i32 74, i32 34, i32 26, i32 31, i32 39, i32 51, i32 64, i32 77, i32 91, i32 45, i32 33, i32 38, i32 47, i32 59, i32 74, i32 91, i32 108]], align 16
@std_chrominance_quant_tbl = internal constant [9 x [64 x i32]] [[64 x i32] [i32 17, i32 18, i32 24, i32 47, i32 99, i32 99, i32 99, i32 99, i32 18, i32 21, i32 26, i32 66, i32 99, i32 99, i32 99, i32 99, i32 24, i32 26, i32 56, i32 99, i32 99, i32 99, i32 99, i32 99, i32 47, i32 66, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16], [64 x i32] [i32 8, i32 12, i32 15, i32 15, i32 86, i32 96, i32 96, i32 98, i32 13, i32 13, i32 15, i32 26, i32 90, i32 96, i32 99, i32 98, i32 12, i32 15, i32 18, i32 96, i32 99, i32 99, i32 99, i32 99, i32 17, i32 16, i32 90, i32 96, i32 99, i32 99, i32 99, i32 99, i32 96, i32 96, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 18, i32 25, i32 37, i32 56, i32 85, i32 16, i32 17, i32 20, i32 27, i32 34, i32 40, i32 53, i32 75, i32 16, i32 20, i32 24, i32 31, i32 43, i32 62, i32 91, i32 135, i32 18, i32 27, i32 31, i32 40, i32 53, i32 74, i32 106, i32 156, i32 25, i32 34, i32 43, i32 53, i32 69, i32 94, i32 131, i32 189, i32 37, i32 40, i32 62, i32 74, i32 94, i32 124, i32 169, i32 238, i32 56, i32 53, i32 91, i32 106, i32 131, i32 169, i32 226, i32 311, i32 85, i32 75, i32 135, i32 156, i32 189, i32 238, i32 311, i32 418], [64 x i32] [i32 9, i32 10, i32 17, i32 19, i32 62, i32 89, i32 91, i32 97, i32 12, i32 13, i32 18, i32 29, i32 84, i32 91, i32 88, i32 98, i32 14, i32 19, i32 29, i32 93, i32 95, i32 95, i32 98, i32 97, i32 20, i32 26, i32 84, i32 88, i32 95, i32 95, i32 98, i32 94, i32 26, i32 86, i32 91, i32 93, i32 97, i32 99, i32 98, i32 99, i32 99, i32 100, i32 98, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 97, i32 97, i32 99, i32 99, i32 99, i32 99, i32 97, i32 99], [64 x i32] [i32 10, i32 12, i32 14, i32 19, i32 26, i32 38, i32 57, i32 86, i32 12, i32 18, i32 21, i32 28, i32 35, i32 41, i32 54, i32 76, i32 14, i32 21, i32 25, i32 32, i32 44, i32 63, i32 92, i32 136, i32 19, i32 28, i32 32, i32 41, i32 54, i32 75, i32 107, i32 157, i32 26, i32 35, i32 44, i32 54, i32 70, i32 95, i32 132, i32 190, i32 38, i32 41, i32 63, i32 75, i32 95, i32 125, i32 170, i32 239, i32 57, i32 54, i32 92, i32 107, i32 132, i32 170, i32 227, i32 312, i32 86, i32 76, i32 136, i32 157, i32 190, i32 239, i32 312, i32 419], [64 x i32] [i32 7, i32 8, i32 10, i32 14, i32 23, i32 44, i32 95, i32 241, i32 8, i32 8, i32 11, i32 15, i32 25, i32 47, i32 102, i32 255, i32 10, i32 11, i32 13, i32 19, i32 31, i32 58, i32 127, i32 255, i32 14, i32 15, i32 19, i32 27, i32 44, i32 83, i32 181, i32 255, i32 23, i32 25, i32 31, i32 44, i32 72, i32 136, i32 255, i32 255, i32 44, i32 47, i32 58, i32 83, i32 136, i32 255, i32 255, i32 255, i32 95, i32 102, i32 127, i32 181, i32 255, i32 255, i32 255, i32 255, i32 241, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255], [64 x i32] [i32 15, i32 11, i32 11, i32 12, i32 15, i32 19, i32 25, i32 32, i32 11, i32 13, i32 10, i32 10, i32 12, i32 15, i32 19, i32 24, i32 11, i32 10, i32 14, i32 14, i32 16, i32 18, i32 22, i32 27, i32 12, i32 10, i32 14, i32 18, i32 21, i32 24, i32 28, i32 33, i32 15, i32 12, i32 16, i32 21, i32 26, i32 31, i32 36, i32 42, i32 19, i32 15, i32 18, i32 24, i32 31, i32 38, i32 45, i32 53, i32 25, i32 19, i32 22, i32 28, i32 36, i32 45, i32 55, i32 65, i32 32, i32 24, i32 27, i32 33, i32 42, i32 53, i32 65, i32 77], [64 x i32] [i32 14, i32 10, i32 11, i32 14, i32 19, i32 25, i32 34, i32 45, i32 10, i32 11, i32 11, i32 12, i32 15, i32 20, i32 26, i32 33, i32 11, i32 11, i32 15, i32 18, i32 21, i32 25, i32 31, i32 38, i32 14, i32 12, i32 18, i32 24, i32 28, i32 33, i32 39, i32 47, i32 19, i32 15, i32 21, i32 28, i32 36, i32 43, i32 51, i32 59, i32 25, i32 20, i32 25, i32 33, i32 43, i32 54, i32 64, i32 74, i32 34, i32 26, i32 31, i32 39, i32 51, i32 64, i32 77, i32 91, i32 45, i32 33, i32 38, i32 47, i32 59, i32 74, i32 91, i32 108]], align 16
@std_huff_tables.bits_dc_luminance = internal constant [17 x i8] c"\00\00\01\05\01\01\01\01\01\01\00\00\00\00\00\00\00", align 16
@std_huff_tables.val_dc_luminance = internal constant [12 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B", align 1
@std_huff_tables.bits_dc_chrominance = internal constant [17 x i8] c"\00\00\03\01\01\01\01\01\01\01\01\01\00\00\00\00\00", align 16
@std_huff_tables.val_dc_chrominance = internal constant [12 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B", align 1
@std_huff_tables.bits_ac_luminance = internal constant [17 x i8] c"\00\00\02\01\03\03\02\04\03\05\05\04\04\00\00\01}", align 16
@std_huff_tables.val_ac_luminance = internal constant [162 x i8] c"\01\02\03\00\04\11\05\12!1A\06\13Qa\07\22q\142\81\91\A1\08#B\B1\C1\15R\D1\F0$3br\82\09\0A\16\17\18\19\1A%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\83\84\85\86\87\88\89\8A\92\93\94\95\96\97\98\99\9A\A2\A3\A4\A5\A6\A7\A8\A9\AA\B2\B3\B4\B5\B6\B7\B8\B9\BA\C2\C3\C4\C5\C6\C7\C8\C9\CA\D2\D3\D4\D5\D6\D7\D8\D9\DA\E1\E2\E3\E4\E5\E6\E7\E8\E9\EA\F1\F2\F3\F4\F5\F6\F7\F8\F9\FA", align 16
@std_huff_tables.bits_ac_chrominance = internal constant [17 x i8] c"\00\00\02\01\02\04\04\03\04\07\05\04\04\00\01\02w", align 16
@std_huff_tables.val_ac_chrominance = internal constant [162 x i8] c"\00\01\02\03\11\04\05!1\06\12AQ\07aq\13\222\81\08\14B\91\A1\B1\C1\09#3R\F0\15br\D1\0A\16$4\E1%\F1\17\18\19\1A&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\82\83\84\85\86\87\88\89\8A\92\93\94\95\96\97\98\99\9A\A2\A3\A4\A5\A6\A7\A8\A9\AA\B2\B3\B4\B5\B6\B7\B8\B9\BA\C2\C3\C4\C5\C6\C7\C8\C9\CA\D2\D3\D4\D5\D6\D7\D8\D9\DA\E2\E3\E4\E5\E6\E7\E8\E9\EA\F2\F3\F4\F5\F6\F7\F8\F9\FA", align 16
@__const.jpeg_search_progression.frequency_split = private unnamed_addr constant [5 x i32] [i32 2, i32 8, i32 5, i32 12, i32 18], align 16

; Function Attrs: nounwind
define hidden void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %cinfo, i32 %which_tbl, i32* %basic_table, i32 %scale_factor, i32 %force_baseline) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %which_tbl.addr = alloca i32, align 4
  %basic_table.addr = alloca i32*, align 4
  %scale_factor.addr = alloca i32, align 4
  %force_baseline.addr = alloca i32, align 4
  %qtblptr = alloca %struct.JQUANT_TBL**, align 4
  %i = alloca i32, align 4
  %temp = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %which_tbl, i32* %which_tbl.addr, align 4, !tbaa !6
  store i32* %basic_table, i32** %basic_table.addr, align 4, !tbaa !2
  store i32 %scale_factor, i32* %scale_factor.addr, align 4, !tbaa !6
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = bitcast %struct.JQUANT_TBL*** %qtblptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 5
  %4 = load i32, i32* %global_state, align 4, !tbaa !8
  %cmp = icmp ne i32 %4, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !13
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 5
  %8 = load i32, i32* %global_state1, align 4, !tbaa !8
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 6
  %i3 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i3, i32 0, i32 0
  store i32 %8, i32* %arrayidx, align 4, !tbaa !16
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_compress_struct* %14 to %struct.jpeg_common_struct*
  call void %13(%struct.jpeg_common_struct* %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load i32, i32* %which_tbl.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %16, 0
  br i1 %cmp5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %17 = load i32, i32* %which_tbl.addr, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %17, 4
  br i1 %cmp6, label %if.then7, label %if.end16

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !12
  %msg_code9 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 31, i32* %msg_code9, align 4, !tbaa !13
  %20 = load i32, i32* %which_tbl.addr, align 4, !tbaa !6
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !12
  %msg_parm11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 6
  %i12 = bitcast %union.anon* %msg_parm11 to [8 x i32]*
  %arrayidx13 = getelementptr inbounds [8 x i32], [8 x i32]* %i12, i32 0, i32 0
  store i32 %20, i32* %arrayidx13, align 4, !tbaa !16
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !12
  %error_exit15 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit15, align 4, !tbaa !17
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_compress_struct* %26 to %struct.jpeg_common_struct*
  call void %25(%struct.jpeg_common_struct* %27)
  br label %if.end16

if.end16:                                         ; preds = %if.then7, %lor.lhs.false
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 16
  %29 = load i32, i32* %which_tbl.addr, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %29
  store %struct.JQUANT_TBL** %arrayidx17, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %30 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %31 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %30, align 4, !tbaa !2
  %cmp18 = icmp eq %struct.JQUANT_TBL* %31, null
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = bitcast %struct.jpeg_compress_struct* %32 to %struct.jpeg_common_struct*
  %call = call %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct* %33)
  %34 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %call, %struct.JQUANT_TBL** %34, align 4, !tbaa !2
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.end16
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end20
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %35, 64
  br i1 %cmp21, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i32*, i32** %basic_table.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i32, i32* %36, i32 %37
  %38 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %39 = load i32, i32* %scale_factor.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %38, %39
  %add = add nsw i32 %mul, 50
  %div = sdiv i32 %add, 100
  store i32 %div, i32* %temp, align 4, !tbaa !18
  %40 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp23 = icmp sle i32 %40, 0
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %for.body
  store i32 1, i32* %temp, align 4, !tbaa !18
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %for.body
  %41 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp26 = icmp sgt i32 %41, 32767
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.end25
  store i32 32767, i32* %temp, align 4, !tbaa !18
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.end25
  %42 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %42, 0
  br i1 %tobool, label %land.lhs.true, label %if.end31

land.lhs.true:                                    ; preds = %if.end28
  %43 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp29 = icmp sgt i32 %43, 255
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true
  store i32 255, i32* %temp, align 4, !tbaa !18
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %land.lhs.true, %if.end28
  %44 = load i32, i32* %temp, align 4, !tbaa !18
  %conv = trunc i32 %44 to i16
  %45 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %46 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %45, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %46, i32 0, i32 0
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %47
  store i16 %conv, i16* %arrayidx32, align 2, !tbaa !19
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %49 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %50 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %49, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %50, i32 0, i32 1
  store i32 0, i32* %sent_table, align 4, !tbaa !20
  %51 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.JQUANT_TBL*** %qtblptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jpeg_set_linear_quality(%struct.jpeg_compress_struct* %cinfo, i32 %scale_factor, i32 %force_baseline) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %scale_factor.addr = alloca i32, align 4
  %force_baseline.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %scale_factor, i32* %scale_factor.addr, align 4, !tbaa !6
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !22
  %quant_tbl_master_idx = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %2, i32 0, i32 18
  %3 = load i32, i32* %quant_tbl_master_idx, align 8, !tbaa !23
  %arrayidx = getelementptr inbounds [9 x [64 x i32]], [9 x [64 x i32]]* @std_luminance_quant_tbl, i32 0, i32 %3
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx, i32 0, i32 0
  %4 = load i32, i32* %scale_factor.addr, align 4, !tbaa !6
  %5 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %0, i32 0, i32* %arraydecay, i32 %4, i32 %5)
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 54
  %8 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !22
  %quant_tbl_master_idx2 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %8, i32 0, i32 18
  %9 = load i32, i32* %quant_tbl_master_idx2, align 8, !tbaa !23
  %arrayidx3 = getelementptr inbounds [9 x [64 x i32]], [9 x [64 x i32]]* @std_chrominance_quant_tbl, i32 0, i32 %9
  %arraydecay4 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx3, i32 0, i32 0
  %10 = load i32, i32* %scale_factor.addr, align 4, !tbaa !6
  %11 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %6, i32 1, i32* %arraydecay4, i32 %10, i32 %11)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_quality_scaling(i32 %quality) #0 {
entry:
  %quality.addr = alloca i32, align 4
  store i32 %quality, i32* %quality.addr, align 4, !tbaa !6
  %0 = load i32, i32* %quality.addr, align 4, !tbaa !6
  %conv = sitofp i32 %0 to float
  %call = call float @jpeg_float_quality_scaling(float %conv)
  %conv1 = fptosi float %call to i32
  ret i32 %conv1
}

; Function Attrs: nounwind
define hidden float @jpeg_float_quality_scaling(float %quality) #0 {
entry:
  %quality.addr = alloca float, align 4
  store float %quality, float* %quality.addr, align 4, !tbaa !26
  %0 = load float, float* %quality.addr, align 4, !tbaa !26
  %cmp = fcmp ole float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %quality.addr, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %quality.addr, align 4, !tbaa !26
  %cmp1 = fcmp ogt float %1, 1.000000e+02
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+02, float* %quality.addr, align 4, !tbaa !26
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %quality.addr, align 4, !tbaa !26
  %cmp4 = fcmp olt float %2, 5.000000e+01
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end3
  %3 = load float, float* %quality.addr, align 4, !tbaa !26
  %div = fdiv float 5.000000e+03, %3
  store float %div, float* %quality.addr, align 4, !tbaa !26
  br label %if.end6

if.else:                                          ; preds = %if.end3
  %4 = load float, float* %quality.addr, align 4, !tbaa !26
  %mul = fmul float %4, 2.000000e+00
  %sub = fsub float 2.000000e+02, %mul
  store float %sub, float* %quality.addr, align 4, !tbaa !26
  br label %if.end6

if.end6:                                          ; preds = %if.else, %if.then5
  %5 = load float, float* %quality.addr, align 4, !tbaa !26
  ret float %5
}

; Function Attrs: nounwind
define hidden void @jpeg_set_quality(%struct.jpeg_compress_struct* %cinfo, i32 %quality, i32 %force_baseline) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %quality.addr = alloca i32, align 4
  %force_baseline.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %quality, i32* %quality.addr, align 4, !tbaa !6
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = load i32, i32* %quality.addr, align 4, !tbaa !6
  %call = call i32 @jpeg_quality_scaling(i32 %0)
  store i32 %call, i32* %quality.addr, align 4, !tbaa !6
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %2 = load i32, i32* %quality.addr, align 4, !tbaa !6
  %3 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_set_linear_quality(%struct.jpeg_compress_struct* %1, i32 %2, i32 %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_set_defaults(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !8
  %cmp = icmp ne i32 %2, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !13
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 5
  %6 = load i32, i32* %global_state1, align 4, !tbaa !8
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 6
  %i3 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i3, i32 0, i32 0
  store i32 %6, i32* %arrayidx, align 4, !tbaa !16
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 0
  %11 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = bitcast %struct.jpeg_compress_struct* %12 to %struct.jpeg_common_struct*
  call void %11(%struct.jpeg_common_struct* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 15
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  %cmp5 = icmp eq %struct.jpeg_component_info* %15, null
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 1
  %17 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !28
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %17, i32 0, i32 0
  %18 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !29
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_compress_struct* %19 to %struct.jpeg_common_struct*
  %call = call i8* %18(%struct.jpeg_common_struct* %20, i32 0, i32 840)
  %21 = bitcast i8* %call to %struct.jpeg_component_info*
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 15
  store %struct.jpeg_component_info* %21, %struct.jpeg_component_info** %comp_info7, align 4, !tbaa !27
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 12
  store i32 8, i32* %data_precision, align 8, !tbaa !31
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_quality(%struct.jpeg_compress_struct* %24, i32 75, i32 1)
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %26 = bitcast %struct.jpeg_compress_struct* %25 to %struct.jpeg_common_struct*
  call void @std_huff_tables(%struct.jpeg_common_struct* %26)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %27, 16
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_dc_L = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 19
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [16 x i8], [16 x i8]* %arith_dc_L, i32 0, i32 %29
  store i8 0, i8* %arrayidx10, align 1, !tbaa !16
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_dc_U = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 20
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [16 x i8], [16 x i8]* %arith_dc_U, i32 0, i32 %31
  store i8 1, i8* %arrayidx11, align 1, !tbaa !16
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_ac_K = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 21
  %33 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [16 x i8], [16 x i8]* %arith_ac_K, i32 0, i32 %33
  store i8 5, i8* %arrayidx12, align 1, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 23
  store %struct.jpeg_scan_info* null, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !32
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 22
  store i32 0, i32* %num_scans, align 8, !tbaa !33
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_in = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %37, i32 0, i32 24
  store i32 0, i32* %raw_data_in, align 8, !tbaa !34
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 25
  store i32 0, i32* %arith_code, align 4, !tbaa !35
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 54
  %40 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !22
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %40, i32 0, i32 16
  %41 = load i32, i32* %compress_profile, align 8, !tbaa !36
  %cmp13 = icmp eq i32 %41, 1560820397
  br i1 %cmp13, label %if.then14, label %if.else

if.then14:                                        ; preds = %for.end
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 26
  store i32 1, i32* %optimize_coding, align 8, !tbaa !37
  br label %if.end16

if.else:                                          ; preds = %for.end
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 26
  store i32 0, i32* %optimize_coding15, align 8, !tbaa !37
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then14
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 12
  %45 = load i32, i32* %data_precision17, align 8, !tbaa !31
  %cmp18 = icmp sgt i32 %45, 8
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end16
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 26
  store i32 1, i32* %optimize_coding20, align 8, !tbaa !37
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.end16
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 27
  store i32 0, i32* %CCIR601_sampling, align 4, !tbaa !38
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 54
  %49 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master22, align 4, !tbaa !22
  %compress_profile23 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %49, i32 0, i32 16
  %50 = load i32, i32* %compress_profile23, align 8, !tbaa !36
  %cmp24 = icmp eq i32 %50, 1560820397
  %conv = zext i1 %cmp24 to i32
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 54
  %52 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master25, align 4, !tbaa !22
  %overshoot_deringing = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %52, i32 0, i32 13
  store i32 %conv, i32* %overshoot_deringing, align 4, !tbaa !39
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 28
  store i32 0, i32* %smoothing_factor, align 8, !tbaa !40
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dct_method = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 29
  store i32 0, i32* %dct_method, align 4, !tbaa !41
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 30
  store i32 0, i32* %restart_interval, align 8, !tbaa !42
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_in_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 31
  store i32 0, i32* %restart_in_rows, align 4, !tbaa !43
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 33
  store i8 1, i8* %JFIF_major_version, align 4, !tbaa !44
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 34
  store i8 1, i8* %JFIF_minor_version, align 1, !tbaa !45
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %density_unit = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 35
  store i8 0, i8* %density_unit, align 2, !tbaa !46
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %X_density = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 36
  store i16 1, i16* %X_density, align 8, !tbaa !47
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Y_density = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 37
  store i16 1, i16* %Y_density, align 2, !tbaa !48
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_default_colorspace(%struct.jpeg_compress_struct* %62)
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master26 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 54
  %64 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master26, align 4, !tbaa !22
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %64, i32 0, i32 17
  store i32 1, i32* %dc_scan_opt_mode, align 4, !tbaa !49
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 54
  %66 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master27, align 4, !tbaa !22
  %compress_profile28 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %66, i32 0, i32 16
  %67 = load i32, i32* %compress_profile28, align 8, !tbaa !36
  %cmp29 = icmp eq i32 %67, 1560820397
  br i1 %cmp29, label %if.then31, label %if.else33

if.then31:                                        ; preds = %if.end21
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 54
  %69 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master32, align 4, !tbaa !22
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %69, i32 0, i32 5
  store i32 1, i32* %optimize_scans, align 4, !tbaa !50
  %70 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_simple_progression(%struct.jpeg_compress_struct* %70)
  br label %if.end36

if.else33:                                        ; preds = %if.end21
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master34 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 54
  %72 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master34, align 4, !tbaa !22
  %optimize_scans35 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %72, i32 0, i32 5
  store i32 0, i32* %optimize_scans35, align 4, !tbaa !50
  br label %if.end36

if.end36:                                         ; preds = %if.else33, %if.then31
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %73, i32 0, i32 54
  %74 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master37, align 4, !tbaa !22
  %compress_profile38 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %74, i32 0, i32 16
  %75 = load i32, i32* %compress_profile38, align 8, !tbaa !36
  %cmp39 = icmp eq i32 %75, 1560820397
  %conv40 = zext i1 %cmp39 to i32
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master41 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %76, i32 0, i32 54
  %77 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master41, align 4, !tbaa !22
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %77, i32 0, i32 6
  store i32 %conv40, i32* %trellis_quant, align 8, !tbaa !51
  %78 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master42 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %78, i32 0, i32 54
  %79 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master42, align 4, !tbaa !22
  %lambda_log_scale1 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %79, i32 0, i32 27
  store float 1.475000e+01, float* %lambda_log_scale1, align 4, !tbaa !52
  %80 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %80, i32 0, i32 54
  %81 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master43, align 4, !tbaa !22
  %lambda_log_scale2 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %81, i32 0, i32 28
  store float 1.650000e+01, float* %lambda_log_scale2, align 8, !tbaa !53
  %82 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %82, i32 0, i32 54
  %83 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master44, align 4, !tbaa !22
  %compress_profile45 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %83, i32 0, i32 16
  %84 = load i32, i32* %compress_profile45, align 8, !tbaa !36
  %cmp46 = icmp eq i32 %84, 1560820397
  %85 = zext i1 %cmp46 to i64
  %cond = select i1 %cmp46, i32 3, i32 0
  %86 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master48 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %86, i32 0, i32 54
  %87 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master48, align 4, !tbaa !22
  %quant_tbl_master_idx = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %87, i32 0, i32 18
  store i32 %cond, i32* %quant_tbl_master_idx, align 8, !tbaa !23
  %88 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master49 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %88, i32 0, i32 54
  %89 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master49, align 4, !tbaa !22
  %use_lambda_weight_tbl = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %89, i32 0, i32 9
  store i32 1, i32* %use_lambda_weight_tbl, align 4, !tbaa !54
  %90 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master50 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %90, i32 0, i32 54
  %91 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master50, align 4, !tbaa !22
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %91, i32 0, i32 10
  store i32 0, i32* %use_scans_in_trellis, align 8, !tbaa !55
  %92 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master51 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %92, i32 0, i32 54
  %93 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master51, align 4, !tbaa !22
  %trellis_freq_split = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %93, i32 0, i32 19
  store i32 8, i32* %trellis_freq_split, align 4, !tbaa !56
  %94 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master52 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %94, i32 0, i32 54
  %95 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master52, align 4, !tbaa !22
  %trellis_num_loops = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %95, i32 0, i32 20
  store i32 1, i32* %trellis_num_loops, align 8, !tbaa !57
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master53 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %96, i32 0, i32 54
  %97 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master53, align 4, !tbaa !22
  %trellis_q_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %97, i32 0, i32 12
  store i32 0, i32* %trellis_q_opt, align 8, !tbaa !58
  %98 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %98, i32 0, i32 54
  %99 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master54, align 4, !tbaa !22
  %trellis_quant_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %99, i32 0, i32 7
  store i32 1, i32* %trellis_quant_dc, align 4, !tbaa !59
  %100 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master55 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %100, i32 0, i32 54
  %101 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master55, align 4, !tbaa !22
  %trellis_delta_dc_weight = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %101, i32 0, i32 29
  store float 0.000000e+00, float* %trellis_delta_dc_weight, align 4, !tbaa !60
  %102 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  ret void
}

; Function Attrs: nounwind
define internal void @std_huff_tables(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %dc_huff_tbl_ptrs = alloca %struct.JHUFF_TBL**, align 4
  %ac_huff_tbl_ptrs = alloca %struct.JHUFF_TBL**, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %is_decompressor = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %2, i32 0, i32 4
  %3 = load i32, i32* %is_decompressor, align 4, !tbaa !61
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_common_struct* %4 to %struct.jpeg_decompress_struct*
  %dc_huff_tbl_ptrs1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 41
  %arraydecay = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs1, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_common_struct* %6 to %struct.jpeg_decompress_struct*
  %ac_huff_tbl_ptrs2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 42
  %arraydecay3 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs2, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay3, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %8 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_common_struct* %8 to %struct.jpeg_compress_struct*
  %dc_huff_tbl_ptrs4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 17
  %arraydecay5 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs4, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay5, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %10 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_common_struct* %10 to %struct.jpeg_compress_struct*
  %ac_huff_tbl_ptrs6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 18
  %arraydecay7 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs6, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay7, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %13, i32 0
  call void @add_huff_table(%struct.jpeg_common_struct* %12, %struct.JHUFF_TBL** %arrayidx, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_dc_luminance, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @std_huff_tables.val_dc_luminance, i32 0, i32 0))
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %15, i32 0
  call void @add_huff_table(%struct.jpeg_common_struct* %14, %struct.JHUFF_TBL** %arrayidx8, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_ac_luminance, i32 0, i32 0), i8* getelementptr inbounds ([162 x i8], [162 x i8]* @std_huff_tables.val_ac_luminance, i32 0, i32 0))
  %16 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %17, i32 1
  call void @add_huff_table(%struct.jpeg_common_struct* %16, %struct.JHUFF_TBL** %arrayidx9, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_dc_chrominance, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @std_huff_tables.val_dc_chrominance, i32 0, i32 0))
  %18 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %19, i32 1
  call void @add_huff_table(%struct.jpeg_common_struct* %18, %struct.JHUFF_TBL** %arrayidx10, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_ac_chrominance, i32 0, i32 0), i8* getelementptr inbounds ([162 x i8], [162 x i8]* @std_huff_tables.val_ac_chrominance, i32 0, i32 0))
  %20 = bitcast %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_default_colorspace(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 10
  %1 = load i32, i32* %in_color_space, align 8, !tbaa !63
  switch i32 %1, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 6, label %sw.bb1
    i32 7, label %sw.bb1
    i32 8, label %sw.bb1
    i32 9, label %sw.bb1
    i32 10, label %sw.bb1
    i32 11, label %sw.bb1
    i32 12, label %sw.bb1
    i32 13, label %sw.bb1
    i32 14, label %sw.bb1
    i32 15, label %sw.bb1
    i32 3, label %sw.bb2
    i32 4, label %sw.bb3
    i32 5, label %sw.bb4
    i32 0, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %2, i32 1)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %3, i32 3)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %4, i32 3)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %5, i32 4)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %6, i32 5)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %7, i32 0)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 5
  store i32 9, i32* %msg_code, align 4, !tbaa !13
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_compress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_simple_progression(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %ncomps = alloca i32, align 4
  %nscans = alloca i32, align 4
  %scanptr = alloca %struct.jpeg_scan_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 54
  %4 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !22
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %4, i32 0, i32 5
  %5 = load i32, i32* %optimize_scans, align 4, !tbaa !50
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.end2

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @jpeg_search_progression(%struct.jpeg_compress_struct* %6)
  %cmp = icmp eq i32 %call, 1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  br label %if.end2

if.end2:                                          ; preds = %if.end, %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 5
  %8 = load i32, i32* %global_state, align 4, !tbaa !8
  %cmp3 = icmp ne i32 %8, 100
  br i1 %cmp3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %if.end2
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !13
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 5
  %12 = load i32, i32* %global_state5, align 4, !tbaa !8
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %12, i32* %arrayidx, align 4, !tbaa !16
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 0
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_compress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  br label %if.end8

if.end8:                                          ; preds = %if.then4, %if.end2
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 13
  %21 = load i32, i32* %num_components, align 4, !tbaa !64
  store i32 %21, i32* %ncomps, align 4, !tbaa !6
  %22 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp9 = icmp eq i32 %22, 3
  br i1 %cmp9, label %land.lhs.true, label %if.else27

land.lhs.true:                                    ; preds = %if.end8
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 14
  %24 = load i32, i32* %jpeg_color_space, align 8, !tbaa !65
  %cmp10 = icmp eq i32 %24, 3
  br i1 %cmp10, label %if.then11, label %if.else27

if.then11:                                        ; preds = %land.lhs.true
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 54
  %26 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master12, align 4, !tbaa !22
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %26, i32 0, i32 16
  %27 = load i32, i32* %compress_profile, align 8, !tbaa !36
  %cmp13 = icmp eq i32 %27, 1560820397
  br i1 %cmp13, label %if.then14, label %if.else25

if.then14:                                        ; preds = %if.then11
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 54
  %29 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master15, align 4, !tbaa !22
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %29, i32 0, i32 17
  %30 = load i32, i32* %dc_scan_opt_mode, align 4, !tbaa !49
  %cmp16 = icmp eq i32 %30, 0
  br i1 %cmp16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.then14
  store i32 9, i32* %nscans, align 4, !tbaa !6
  br label %if.end24

if.else:                                          ; preds = %if.then14
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 54
  %32 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master18, align 4, !tbaa !22
  %dc_scan_opt_mode19 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %32, i32 0, i32 17
  %33 = load i32, i32* %dc_scan_opt_mode19, align 4, !tbaa !49
  %cmp20 = icmp eq i32 %33, 1
  br i1 %cmp20, label %if.then21, label %if.else22

if.then21:                                        ; preds = %if.else
  store i32 11, i32* %nscans, align 4, !tbaa !6
  br label %if.end23

if.else22:                                        ; preds = %if.else
  store i32 10, i32* %nscans, align 4, !tbaa !6
  br label %if.end23

if.end23:                                         ; preds = %if.else22, %if.then21
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then17
  br label %if.end26

if.else25:                                        ; preds = %if.then11
  store i32 10, i32* %nscans, align 4, !tbaa !6
  br label %if.end26

if.end26:                                         ; preds = %if.else25, %if.end24
  br label %if.end46

if.else27:                                        ; preds = %land.lhs.true, %if.end8
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master28 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 54
  %35 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master28, align 4, !tbaa !22
  %compress_profile29 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %35, i32 0, i32 16
  %36 = load i32, i32* %compress_profile29, align 8, !tbaa !36
  %cmp30 = icmp eq i32 %36, 1560820397
  br i1 %cmp30, label %if.then31, label %if.else37

if.then31:                                        ; preds = %if.else27
  %37 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp32 = icmp sgt i32 %37, 4
  br i1 %cmp32, label %if.then33, label %if.else34

if.then33:                                        ; preds = %if.then31
  %38 = load i32, i32* %ncomps, align 4, !tbaa !6
  %mul = mul nsw i32 5, %38
  store i32 %mul, i32* %nscans, align 4, !tbaa !6
  br label %if.end36

if.else34:                                        ; preds = %if.then31
  %39 = load i32, i32* %ncomps, align 4, !tbaa !6
  %mul35 = mul nsw i32 4, %39
  %add = add nsw i32 1, %mul35
  store i32 %add, i32* %nscans, align 4, !tbaa !6
  br label %if.end36

if.end36:                                         ; preds = %if.else34, %if.then33
  br label %if.end45

if.else37:                                        ; preds = %if.else27
  %40 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp38 = icmp sgt i32 %40, 4
  br i1 %cmp38, label %if.then39, label %if.else41

if.then39:                                        ; preds = %if.else37
  %41 = load i32, i32* %ncomps, align 4, !tbaa !6
  %mul40 = mul nsw i32 6, %41
  store i32 %mul40, i32* %nscans, align 4, !tbaa !6
  br label %if.end44

if.else41:                                        ; preds = %if.else37
  %42 = load i32, i32* %ncomps, align 4, !tbaa !6
  %mul42 = mul nsw i32 4, %42
  %add43 = add nsw i32 2, %mul42
  store i32 %add43, i32* %nscans, align 4, !tbaa !6
  br label %if.end44

if.end44:                                         ; preds = %if.else41, %if.then39
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end36
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end26
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 63
  %44 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %script_space, align 8, !tbaa !66
  %cmp47 = icmp eq %struct.jpeg_scan_info* %44, null
  br i1 %cmp47, label %if.then49, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end46
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 64
  %46 = load i32, i32* %script_space_size, align 4, !tbaa !67
  %47 = load i32, i32* %nscans, align 4, !tbaa !6
  %cmp48 = icmp slt i32 %46, %47
  br i1 %cmp48, label %if.then49, label %if.end56

if.then49:                                        ; preds = %lor.lhs.false, %if.end46
  %48 = load i32, i32* %nscans, align 4, !tbaa !6
  %cmp50 = icmp sgt i32 %48, 10
  br i1 %cmp50, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then49
  %49 = load i32, i32* %nscans, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then49
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %49, %cond.true ], [ 10, %cond.false ]
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size51 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 64
  store i32 %cond, i32* %script_space_size51, align 4, !tbaa !67
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 1
  %52 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !28
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %52, i32 0, i32 0
  %53 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !29
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %55 = bitcast %struct.jpeg_compress_struct* %54 to %struct.jpeg_common_struct*
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size52 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 64
  %57 = load i32, i32* %script_space_size52, align 4, !tbaa !67
  %mul53 = mul i32 %57, 36
  %call54 = call i8* %53(%struct.jpeg_common_struct* %55, i32 0, i32 %mul53)
  %58 = bitcast i8* %call54 to %struct.jpeg_scan_info*
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space55 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 63
  store %struct.jpeg_scan_info* %58, %struct.jpeg_scan_info** %script_space55, align 8, !tbaa !66
  br label %if.end56

if.end56:                                         ; preds = %cond.end, %lor.lhs.false
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 63
  %61 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %script_space57, align 8, !tbaa !66
  store %struct.jpeg_scan_info* %61, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %62 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 23
  store %struct.jpeg_scan_info* %62, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !32
  %64 = load i32, i32* %nscans, align 4, !tbaa !6
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 22
  store i32 %64, i32* %num_scans, align 8, !tbaa !33
  %66 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp58 = icmp eq i32 %66, 3
  br i1 %cmp58, label %land.lhs.true59, label %if.else105

land.lhs.true59:                                  ; preds = %if.end56
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 14
  %68 = load i32, i32* %jpeg_color_space60, align 8, !tbaa !65
  %cmp61 = icmp eq i32 %68, 3
  br i1 %cmp61, label %if.then62, label %if.else105

if.then62:                                        ; preds = %land.lhs.true59
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master63 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 54
  %70 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master63, align 4, !tbaa !22
  %compress_profile64 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %70, i32 0, i32 16
  %71 = load i32, i32* %compress_profile64, align 8, !tbaa !36
  %cmp65 = icmp eq i32 %71, 1560820397
  br i1 %cmp65, label %if.then66, label %if.else93

if.then66:                                        ; preds = %if.then62
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master67 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %72, i32 0, i32 54
  %73 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master67, align 4, !tbaa !22
  %dc_scan_opt_mode68 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %73, i32 0, i32 17
  %74 = load i32, i32* %dc_scan_opt_mode68, align 4, !tbaa !49
  %cmp69 = icmp eq i32 %74, 0
  br i1 %cmp69, label %if.then70, label %if.else72

if.then70:                                        ; preds = %if.then66
  %75 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %76 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call71 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %75, i32 %76, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call71, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end84

if.else72:                                        ; preds = %if.then66
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master73 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 54
  %78 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master73, align 4, !tbaa !22
  %dc_scan_opt_mode74 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %78, i32 0, i32 17
  %79 = load i32, i32* %dc_scan_opt_mode74, align 4, !tbaa !49
  %cmp75 = icmp eq i32 %79, 1
  br i1 %cmp75, label %if.then76, label %if.else80

if.then76:                                        ; preds = %if.else72
  %80 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call77 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %80, i32 0, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call77, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %81 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call78 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %81, i32 1, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call78, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %82 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call79 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %82, i32 2, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call79, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end83

if.else80:                                        ; preds = %if.else72
  %83 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call81 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %83, i32 1, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call81, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %84 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call82 = call %struct.jpeg_scan_info* @fill_a_scan_pair(%struct.jpeg_scan_info* %84, i32 1, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call82, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end83

if.end83:                                         ; preds = %if.else80, %if.then76
  br label %if.end84

if.end84:                                         ; preds = %if.end83, %if.then70
  %85 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call85 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %85, i32 0, i32 1, i32 8, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call85, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %86 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call86 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %86, i32 1, i32 1, i32 8, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call86, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %87 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call87 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %87, i32 2, i32 1, i32 8, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call87, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %88 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call88 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %88, i32 0, i32 9, i32 63, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call88, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %89 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call89 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %89, i32 0, i32 1, i32 63, i32 2, i32 1)
  store %struct.jpeg_scan_info* %call89, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %90 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call90 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %90, i32 0, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call90, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %91 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call91 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %91, i32 1, i32 9, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call91, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %92 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call92 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %92, i32 2, i32 9, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call92, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end104

if.else93:                                        ; preds = %if.then62
  %93 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %94 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call94 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %93, i32 %94, i32 0, i32 1)
  store %struct.jpeg_scan_info* %call94, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %95 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call95 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %95, i32 0, i32 1, i32 5, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call95, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %96 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call96 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %96, i32 2, i32 1, i32 63, i32 0, i32 1)
  store %struct.jpeg_scan_info* %call96, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %97 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call97 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %97, i32 1, i32 1, i32 63, i32 0, i32 1)
  store %struct.jpeg_scan_info* %call97, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %98 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call98 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %98, i32 0, i32 6, i32 63, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call98, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %99 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call99 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %99, i32 0, i32 1, i32 63, i32 2, i32 1)
  store %struct.jpeg_scan_info* %call99, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %100 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %101 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call100 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %100, i32 %101, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call100, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %102 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call101 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %102, i32 2, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call101, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %103 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call102 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %103, i32 1, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call102, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %104 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call103 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %104, i32 0, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call103, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end104

if.end104:                                        ; preds = %if.else93, %if.end84
  br label %if.end123

if.else105:                                       ; preds = %land.lhs.true59, %if.end56
  %105 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master106 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %105, i32 0, i32 54
  %106 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master106, align 4, !tbaa !22
  %compress_profile107 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %106, i32 0, i32 16
  %107 = load i32, i32* %compress_profile107, align 8, !tbaa !36
  %cmp108 = icmp eq i32 %107, 1560820397
  br i1 %cmp108, label %if.then109, label %if.else115

if.then109:                                       ; preds = %if.else105
  %108 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %109 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call110 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %108, i32 %109, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call110, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %110 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %111 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call111 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %110, i32 %111, i32 1, i32 8, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call111, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %112 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %113 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call112 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %112, i32 %113, i32 9, i32 63, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call112, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %114 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %115 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call113 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %114, i32 %115, i32 1, i32 63, i32 2, i32 1)
  store %struct.jpeg_scan_info* %call113, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %116 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %117 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call114 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %116, i32 %117, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call114, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end122

if.else115:                                       ; preds = %if.else105
  %118 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %119 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call116 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %118, i32 %119, i32 0, i32 1)
  store %struct.jpeg_scan_info* %call116, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %120 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %121 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call117 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %120, i32 %121, i32 1, i32 5, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call117, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %122 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %123 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call118 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %122, i32 %123, i32 6, i32 63, i32 0, i32 2)
  store %struct.jpeg_scan_info* %call118, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %124 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %125 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call119 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %124, i32 %125, i32 1, i32 63, i32 2, i32 1)
  store %struct.jpeg_scan_info* %call119, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %126 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %127 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call120 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %126, i32 %127, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call120, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %128 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %129 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call121 = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %128, i32 %129, i32 1, i32 63, i32 1, i32 0)
  store %struct.jpeg_scan_info* %call121, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end122

if.end122:                                        ; preds = %if.else115, %if.then109
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %if.end104
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end123, %if.then1
  %130 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %cinfo, i32 %colorspace) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %colorspace.addr = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %ci = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %colorspace, i32* %colorspace.addr, align 4, !tbaa !16
  %0 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state, align 4, !tbaa !8
  %cmp = icmp ne i32 %3, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !13
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state1, align 4, !tbaa !8
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !16
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_compress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i32, i32* %colorspace.addr, align 4, !tbaa !16
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 14
  store i32 %15, i32* %jpeg_color_space, align 8, !tbaa !65
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_JFIF_header = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 32
  store i32 0, i32* %write_JFIF_header, align 8, !tbaa !68
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_Adobe_marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 38
  store i32 0, i32* %write_Adobe_marker, align 4, !tbaa !69
  %19 = load i32, i32* %colorspace.addr, align 4, !tbaa !16
  switch i32 %19, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb6
    i32 3, label %sw.bb33
    i32 4, label %sw.bb60
    i32 5, label %sw.bb95
    i32 0, label %sw.bb130
  ]

sw.bb:                                            ; preds = %if.end
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_JFIF_header4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 32
  store i32 1, i32* %write_JFIF_header4, align 8, !tbaa !68
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 13
  store i32 1, i32* %num_components, align 4, !tbaa !64
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 15
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0
  store %struct.jpeg_component_info* %arrayidx5, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0, i32 0
  store i32 1, i32* %component_id, align 4, !tbaa !70
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 2
  store i32 1, i32* %h_samp_factor, align 4, !tbaa !72
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 3
  store i32 1, i32* %v_samp_factor, align 4, !tbaa !73
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no, align 4, !tbaa !74
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no, align 4, !tbaa !75
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no, align 4, !tbaa !76
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.end
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_Adobe_marker7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 38
  store i32 1, i32* %write_Adobe_marker7, align 4, !tbaa !69
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 13
  store i32 3, i32* %num_components8, align 4, !tbaa !64
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 15
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info9, align 4, !tbaa !27
  %arrayidx10 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0
  store %struct.jpeg_component_info* %arrayidx10, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %34 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id11 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %34, i32 0, i32 0
  store i32 82, i32* %component_id11, align 4, !tbaa !70
  %35 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor12 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %35, i32 0, i32 2
  store i32 1, i32* %h_samp_factor12, align 4, !tbaa !72
  %36 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor13 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %36, i32 0, i32 3
  store i32 1, i32* %v_samp_factor13, align 4, !tbaa !73
  %37 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no14 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %37, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no14, align 4, !tbaa !74
  %38 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no15 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %38, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no15, align 4, !tbaa !75
  %39 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no16 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %39, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no16, align 4, !tbaa !76
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %40, i32 0, i32 15
  %41 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info17, align 4, !tbaa !27
  %arrayidx18 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %41, i32 1
  store %struct.jpeg_component_info* %arrayidx18, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id19 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %42, i32 0, i32 0
  store i32 71, i32* %component_id19, align 4, !tbaa !70
  %43 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor20 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %43, i32 0, i32 2
  store i32 1, i32* %h_samp_factor20, align 4, !tbaa !72
  %44 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %44, i32 0, i32 3
  store i32 1, i32* %v_samp_factor21, align 4, !tbaa !73
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no22 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no22, align 4, !tbaa !74
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no23 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no23, align 4, !tbaa !75
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no24 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %47, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no24, align 4, !tbaa !76
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 15
  %49 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info25, align 4, !tbaa !27
  %arrayidx26 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %49, i32 2
  store %struct.jpeg_component_info* %arrayidx26, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %50 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id27 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %50, i32 0, i32 0
  store i32 66, i32* %component_id27, align 4, !tbaa !70
  %51 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor28 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %51, i32 0, i32 2
  store i32 1, i32* %h_samp_factor28, align 4, !tbaa !72
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor29 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %52, i32 0, i32 3
  store i32 1, i32* %v_samp_factor29, align 4, !tbaa !73
  %53 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no30 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %53, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no30, align 4, !tbaa !74
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no31 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no31, align 4, !tbaa !75
  %55 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no32 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %55, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no32, align 4, !tbaa !76
  br label %sw.epilog

sw.bb33:                                          ; preds = %if.end
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_JFIF_header34 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 32
  store i32 1, i32* %write_JFIF_header34, align 8, !tbaa !68
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 13
  store i32 3, i32* %num_components35, align 4, !tbaa !64
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info36 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 15
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info36, align 4, !tbaa !27
  %arrayidx37 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 0
  store %struct.jpeg_component_info* %arrayidx37, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %60 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id38 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %60, i32 0, i32 0
  store i32 1, i32* %component_id38, align 4, !tbaa !70
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %61, i32 0, i32 2
  store i32 2, i32* %h_samp_factor39, align 4, !tbaa !72
  %62 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %62, i32 0, i32 3
  store i32 2, i32* %v_samp_factor40, align 4, !tbaa !73
  %63 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no41 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %63, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no41, align 4, !tbaa !74
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no42 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no42, align 4, !tbaa !75
  %65 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no43 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %65, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no43, align 4, !tbaa !76
  %66 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %66, i32 0, i32 15
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info44, align 4, !tbaa !27
  %arrayidx45 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 1
  store %struct.jpeg_component_info* %arrayidx45, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %68 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id46 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %68, i32 0, i32 0
  store i32 2, i32* %component_id46, align 4, !tbaa !70
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor47 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 2
  store i32 1, i32* %h_samp_factor47, align 4, !tbaa !72
  %70 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor48 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %70, i32 0, i32 3
  store i32 1, i32* %v_samp_factor48, align 4, !tbaa !73
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no49 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 4
  store i32 1, i32* %quant_tbl_no49, align 4, !tbaa !74
  %72 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no50 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %72, i32 0, i32 5
  store i32 1, i32* %dc_tbl_no50, align 4, !tbaa !75
  %73 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no51 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %73, i32 0, i32 6
  store i32 1, i32* %ac_tbl_no51, align 4, !tbaa !76
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info52 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 15
  %75 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info52, align 4, !tbaa !27
  %arrayidx53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %75, i32 2
  store %struct.jpeg_component_info* %arrayidx53, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %76 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id54 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %76, i32 0, i32 0
  store i32 3, i32* %component_id54, align 4, !tbaa !70
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor55 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %77, i32 0, i32 2
  store i32 1, i32* %h_samp_factor55, align 4, !tbaa !72
  %78 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor56 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %78, i32 0, i32 3
  store i32 1, i32* %v_samp_factor56, align 4, !tbaa !73
  %79 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no57 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %79, i32 0, i32 4
  store i32 1, i32* %quant_tbl_no57, align 4, !tbaa !74
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no58 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 5
  store i32 1, i32* %dc_tbl_no58, align 4, !tbaa !75
  %81 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no59 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %81, i32 0, i32 6
  store i32 1, i32* %ac_tbl_no59, align 4, !tbaa !76
  br label %sw.epilog

sw.bb60:                                          ; preds = %if.end
  %82 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_Adobe_marker61 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %82, i32 0, i32 38
  store i32 1, i32* %write_Adobe_marker61, align 4, !tbaa !69
  %83 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components62 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %83, i32 0, i32 13
  store i32 4, i32* %num_components62, align 4, !tbaa !64
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info63 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %84, i32 0, i32 15
  %85 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info63, align 4, !tbaa !27
  %arrayidx64 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %85, i32 0
  store %struct.jpeg_component_info* %arrayidx64, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %86 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id65 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %86, i32 0, i32 0
  store i32 67, i32* %component_id65, align 4, !tbaa !70
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor66 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %87, i32 0, i32 2
  store i32 1, i32* %h_samp_factor66, align 4, !tbaa !72
  %88 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor67 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %88, i32 0, i32 3
  store i32 1, i32* %v_samp_factor67, align 4, !tbaa !73
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no68 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %89, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no68, align 4, !tbaa !74
  %90 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no69 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %90, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no69, align 4, !tbaa !75
  %91 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no70 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %91, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no70, align 4, !tbaa !76
  %92 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info71 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %92, i32 0, i32 15
  %93 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info71, align 4, !tbaa !27
  %arrayidx72 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %93, i32 1
  store %struct.jpeg_component_info* %arrayidx72, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %94 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id73 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %94, i32 0, i32 0
  store i32 77, i32* %component_id73, align 4, !tbaa !70
  %95 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor74 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %95, i32 0, i32 2
  store i32 1, i32* %h_samp_factor74, align 4, !tbaa !72
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor75 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 3
  store i32 1, i32* %v_samp_factor75, align 4, !tbaa !73
  %97 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no76 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %97, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no76, align 4, !tbaa !74
  %98 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no77 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %98, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no77, align 4, !tbaa !75
  %99 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no78 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %99, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no78, align 4, !tbaa !76
  %100 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info79 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %100, i32 0, i32 15
  %101 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info79, align 4, !tbaa !27
  %arrayidx80 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %101, i32 2
  store %struct.jpeg_component_info* %arrayidx80, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %102 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id81 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %102, i32 0, i32 0
  store i32 89, i32* %component_id81, align 4, !tbaa !70
  %103 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor82 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %103, i32 0, i32 2
  store i32 1, i32* %h_samp_factor82, align 4, !tbaa !72
  %104 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor83 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %104, i32 0, i32 3
  store i32 1, i32* %v_samp_factor83, align 4, !tbaa !73
  %105 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no84 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %105, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no84, align 4, !tbaa !74
  %106 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no85 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %106, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no85, align 4, !tbaa !75
  %107 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no86 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %107, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no86, align 4, !tbaa !76
  %108 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info87 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %108, i32 0, i32 15
  %109 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info87, align 4, !tbaa !27
  %arrayidx88 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %109, i32 3
  store %struct.jpeg_component_info* %arrayidx88, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %110 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id89 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %110, i32 0, i32 0
  store i32 75, i32* %component_id89, align 4, !tbaa !70
  %111 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor90 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %111, i32 0, i32 2
  store i32 1, i32* %h_samp_factor90, align 4, !tbaa !72
  %112 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor91 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %112, i32 0, i32 3
  store i32 1, i32* %v_samp_factor91, align 4, !tbaa !73
  %113 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no92 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %113, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no92, align 4, !tbaa !74
  %114 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no93 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %114, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no93, align 4, !tbaa !75
  %115 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no94 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %115, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no94, align 4, !tbaa !76
  br label %sw.epilog

sw.bb95:                                          ; preds = %if.end
  %116 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_Adobe_marker96 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %116, i32 0, i32 38
  store i32 1, i32* %write_Adobe_marker96, align 4, !tbaa !69
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components97 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 13
  store i32 4, i32* %num_components97, align 4, !tbaa !64
  %118 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info98 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %118, i32 0, i32 15
  %119 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info98, align 4, !tbaa !27
  %arrayidx99 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %119, i32 0
  store %struct.jpeg_component_info* %arrayidx99, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %120 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id100 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %120, i32 0, i32 0
  store i32 1, i32* %component_id100, align 4, !tbaa !70
  %121 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor101 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %121, i32 0, i32 2
  store i32 2, i32* %h_samp_factor101, align 4, !tbaa !72
  %122 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor102 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %122, i32 0, i32 3
  store i32 2, i32* %v_samp_factor102, align 4, !tbaa !73
  %123 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no103 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %123, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no103, align 4, !tbaa !74
  %124 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no104 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %124, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no104, align 4, !tbaa !75
  %125 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no105 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %125, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no105, align 4, !tbaa !76
  %126 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info106 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %126, i32 0, i32 15
  %127 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info106, align 4, !tbaa !27
  %arrayidx107 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %127, i32 1
  store %struct.jpeg_component_info* %arrayidx107, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %128 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id108 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %128, i32 0, i32 0
  store i32 2, i32* %component_id108, align 4, !tbaa !70
  %129 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor109 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %129, i32 0, i32 2
  store i32 1, i32* %h_samp_factor109, align 4, !tbaa !72
  %130 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor110 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %130, i32 0, i32 3
  store i32 1, i32* %v_samp_factor110, align 4, !tbaa !73
  %131 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no111 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %131, i32 0, i32 4
  store i32 1, i32* %quant_tbl_no111, align 4, !tbaa !74
  %132 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no112 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %132, i32 0, i32 5
  store i32 1, i32* %dc_tbl_no112, align 4, !tbaa !75
  %133 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no113 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %133, i32 0, i32 6
  store i32 1, i32* %ac_tbl_no113, align 4, !tbaa !76
  %134 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info114 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %134, i32 0, i32 15
  %135 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info114, align 4, !tbaa !27
  %arrayidx115 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %135, i32 2
  store %struct.jpeg_component_info* %arrayidx115, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %136 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id116 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %136, i32 0, i32 0
  store i32 3, i32* %component_id116, align 4, !tbaa !70
  %137 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor117 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %137, i32 0, i32 2
  store i32 1, i32* %h_samp_factor117, align 4, !tbaa !72
  %138 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor118 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %138, i32 0, i32 3
  store i32 1, i32* %v_samp_factor118, align 4, !tbaa !73
  %139 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no119 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %139, i32 0, i32 4
  store i32 1, i32* %quant_tbl_no119, align 4, !tbaa !74
  %140 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no120 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %140, i32 0, i32 5
  store i32 1, i32* %dc_tbl_no120, align 4, !tbaa !75
  %141 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no121 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %141, i32 0, i32 6
  store i32 1, i32* %ac_tbl_no121, align 4, !tbaa !76
  %142 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info122 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %142, i32 0, i32 15
  %143 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info122, align 4, !tbaa !27
  %arrayidx123 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %143, i32 3
  store %struct.jpeg_component_info* %arrayidx123, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %144 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id124 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %144, i32 0, i32 0
  store i32 4, i32* %component_id124, align 4, !tbaa !70
  %145 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor125 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %145, i32 0, i32 2
  store i32 2, i32* %h_samp_factor125, align 4, !tbaa !72
  %146 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor126 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %146, i32 0, i32 3
  store i32 2, i32* %v_samp_factor126, align 4, !tbaa !73
  %147 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no127 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %147, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no127, align 4, !tbaa !74
  %148 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no128 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %148, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no128, align 4, !tbaa !75
  %149 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no129 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %149, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no129, align 4, !tbaa !76
  br label %sw.epilog

sw.bb130:                                         ; preds = %if.end
  %150 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %150, i32 0, i32 9
  %151 = load i32, i32* %input_components, align 4, !tbaa !77
  %152 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components131 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %152, i32 0, i32 13
  store i32 %151, i32* %num_components131, align 4, !tbaa !64
  %153 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components132 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %153, i32 0, i32 13
  %154 = load i32, i32* %num_components132, align 4, !tbaa !64
  %cmp133 = icmp slt i32 %154, 1
  br i1 %cmp133, label %if.then136, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %sw.bb130
  %155 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components134 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %155, i32 0, i32 13
  %156 = load i32, i32* %num_components134, align 4, !tbaa !64
  %cmp135 = icmp sgt i32 %156, 10
  br i1 %cmp135, label %if.then136, label %if.end150

if.then136:                                       ; preds = %lor.lhs.false, %sw.bb130
  %157 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err137 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %157, i32 0, i32 0
  %158 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err137, align 8, !tbaa !12
  %msg_code138 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %158, i32 0, i32 5
  store i32 26, i32* %msg_code138, align 4, !tbaa !13
  %159 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components139 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %159, i32 0, i32 13
  %160 = load i32, i32* %num_components139, align 4, !tbaa !64
  %161 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err140 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %161, i32 0, i32 0
  %162 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err140, align 8, !tbaa !12
  %msg_parm141 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %162, i32 0, i32 6
  %i142 = bitcast %union.anon* %msg_parm141 to [8 x i32]*
  %arrayidx143 = getelementptr inbounds [8 x i32], [8 x i32]* %i142, i32 0, i32 0
  store i32 %160, i32* %arrayidx143, align 4, !tbaa !16
  %163 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err144 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %163, i32 0, i32 0
  %164 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err144, align 8, !tbaa !12
  %msg_parm145 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %164, i32 0, i32 6
  %i146 = bitcast %union.anon* %msg_parm145 to [8 x i32]*
  %arrayidx147 = getelementptr inbounds [8 x i32], [8 x i32]* %i146, i32 0, i32 1
  store i32 10, i32* %arrayidx147, align 4, !tbaa !16
  %165 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err148 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %165, i32 0, i32 0
  %166 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err148, align 8, !tbaa !12
  %error_exit149 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %166, i32 0, i32 0
  %167 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit149, align 4, !tbaa !17
  %168 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %169 = bitcast %struct.jpeg_compress_struct* %168 to %struct.jpeg_common_struct*
  call void %167(%struct.jpeg_common_struct* %169)
  br label %if.end150

if.end150:                                        ; preds = %if.then136, %lor.lhs.false
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end150
  %170 = load i32, i32* %ci, align 4, !tbaa !6
  %171 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components151 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %171, i32 0, i32 13
  %172 = load i32, i32* %num_components151, align 4, !tbaa !64
  %cmp152 = icmp slt i32 %170, %172
  br i1 %cmp152, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %173 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info153 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %173, i32 0, i32 15
  %174 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info153, align 4, !tbaa !27
  %175 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx154 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %174, i32 %175
  store %struct.jpeg_component_info* %arrayidx154, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %176 = load i32, i32* %ci, align 4, !tbaa !6
  %177 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id155 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %177, i32 0, i32 0
  store i32 %176, i32* %component_id155, align 4, !tbaa !70
  %178 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor156 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %178, i32 0, i32 2
  store i32 1, i32* %h_samp_factor156, align 4, !tbaa !72
  %179 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor157 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %179, i32 0, i32 3
  store i32 1, i32* %v_samp_factor157, align 4, !tbaa !73
  %180 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no158 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %180, i32 0, i32 4
  store i32 0, i32* %quant_tbl_no158, align 4, !tbaa !74
  %181 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no159 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %181, i32 0, i32 5
  store i32 0, i32* %dc_tbl_no159, align 4, !tbaa !75
  %182 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no160 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %182, i32 0, i32 6
  store i32 0, i32* %ac_tbl_no160, align 4, !tbaa !76
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %183 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %183, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  %184 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err161 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %184, i32 0, i32 0
  %185 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err161, align 8, !tbaa !12
  %msg_code162 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %185, i32 0, i32 5
  store i32 10, i32* %msg_code162, align 4, !tbaa !13
  %186 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err163 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %186, i32 0, i32 0
  %187 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err163, align 8, !tbaa !12
  %error_exit164 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %187, i32 0, i32 0
  %188 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit164, align 4, !tbaa !17
  %189 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %190 = bitcast %struct.jpeg_compress_struct* %189 to %struct.jpeg_common_struct*
  call void %188(%struct.jpeg_common_struct* %190)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %for.end, %sw.bb95, %sw.bb60, %sw.bb33, %sw.bb6, %sw.bb
  %191 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  %192 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #4
  ret void
}

; Function Attrs: nounwind
define internal i32 @jpeg_search_progression(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %ncomps = alloca i32, align 4
  %nscans = alloca i32, align 4
  %scanptr = alloca %struct.jpeg_scan_info*, align 4
  %Al = alloca i32, align 4
  %frequency_split = alloca [5 x i32], align 16
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 13
  %2 = load i32, i32* %num_components, align 4, !tbaa !64
  store i32 %2, i32* %ncomps, align 4, !tbaa !6
  %3 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast [5 x i32]* %frequency_split to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %6) #4
  %7 = bitcast [5 x i32]* %frequency_split to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %7, i8* align 16 bitcast ([5 x i32]* @__const.jpeg_search_progression.frequency_split to i8*), i32 20, i1 false)
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 5
  %10 = load i32, i32* %global_state, align 4, !tbaa !8
  %cmp = icmp ne i32 %10, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !12
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !13
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 5
  %14 = load i32, i32* %global_state1, align 4, !tbaa !8
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !12
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 6
  %i3 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i3, i32 0, i32 0
  store i32 %14, i32* %arrayidx, align 4, !tbaa !16
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !12
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 0
  %19 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = bitcast %struct.jpeg_compress_struct* %20 to %struct.jpeg_common_struct*
  call void %19(%struct.jpeg_common_struct* %21)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %22, 3
  br i1 %cmp5, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 14
  %24 = load i32, i32* %jpeg_color_space, align 8, !tbaa !65
  %cmp6 = icmp eq i32 %24, 3
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %land.lhs.true
  store i32 64, i32* %nscans, align 4, !tbaa !6
  br label %if.end12

if.else:                                          ; preds = %land.lhs.true, %if.end
  %25 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp8 = icmp eq i32 %25, 1
  br i1 %cmp8, label %if.then9, label %if.else10

if.then9:                                         ; preds = %if.else
  store i32 23, i32* %nscans, align 4, !tbaa !6
  br label %if.end11

if.else10:                                        ; preds = %if.else
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 54
  %27 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !22
  %num_scans_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %27, i32 0, i32 21
  store i32 0, i32* %num_scans_luma, align 4, !tbaa !78
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.then9
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then7
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 63
  %29 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %script_space, align 8, !tbaa !66
  %cmp13 = icmp eq %struct.jpeg_scan_info* %29, null
  br i1 %cmp13, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end12
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 64
  %31 = load i32, i32* %script_space_size, align 4, !tbaa !67
  %32 = load i32, i32* %nscans, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %31, %32
  br i1 %cmp14, label %if.then15, label %if.end20

if.then15:                                        ; preds = %lor.lhs.false, %if.end12
  %33 = load i32, i32* %nscans, align 4, !tbaa !6
  %cmp16 = icmp sgt i32 %33, 64
  br i1 %cmp16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then15
  %34 = load i32, i32* %nscans, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %34, %cond.true ], [ 64, %cond.false ]
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 64
  store i32 %cond, i32* %script_space_size17, align 4, !tbaa !67
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 1
  %37 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !28
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %37, i32 0, i32 0
  %38 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !29
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_compress_struct* %39 to %struct.jpeg_common_struct*
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space_size18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 64
  %42 = load i32, i32* %script_space_size18, align 4, !tbaa !67
  %mul = mul i32 %42, 36
  %call = call i8* %38(%struct.jpeg_common_struct* %40, i32 0, i32 %mul)
  %43 = bitcast i8* %call to %struct.jpeg_scan_info*
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 63
  store %struct.jpeg_scan_info* %43, %struct.jpeg_scan_info** %script_space19, align 8, !tbaa !66
  br label %if.end20

if.end20:                                         ; preds = %cond.end, %lor.lhs.false
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 63
  %46 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %script_space21, align 8, !tbaa !66
  store %struct.jpeg_scan_info* %46, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %47 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 23
  store %struct.jpeg_scan_info* %47, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !32
  %49 = load i32, i32* %nscans, align 4, !tbaa !6
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 22
  store i32 %49, i32* %num_scans, align 8, !tbaa !33
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 54
  %52 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master22, align 4, !tbaa !22
  %Al_max_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %52, i32 0, i32 25
  store i32 3, i32* %Al_max_luma, align 4, !tbaa !79
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 54
  %54 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master23, align 4, !tbaa !22
  %num_scans_luma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %54, i32 0, i32 22
  store i32 1, i32* %num_scans_luma_dc, align 8, !tbaa !80
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 54
  %56 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master24, align 4, !tbaa !22
  %num_frequency_splits = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %56, i32 0, i32 24
  store i32 5, i32* %num_frequency_splits, align 8, !tbaa !81
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 54
  %58 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master25, align 4, !tbaa !22
  %num_scans_luma_dc26 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %58, i32 0, i32 22
  %59 = load i32, i32* %num_scans_luma_dc26, align 8, !tbaa !80
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 54
  %61 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master27, align 4, !tbaa !22
  %Al_max_luma28 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %61, i32 0, i32 25
  %62 = load i32, i32* %Al_max_luma28, align 4, !tbaa !79
  %mul29 = mul nsw i32 3, %62
  %add = add nsw i32 %mul29, 2
  %add30 = add nsw i32 %59, %add
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master31 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 54
  %64 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master31, align 4, !tbaa !22
  %num_frequency_splits32 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %64, i32 0, i32 24
  %65 = load i32, i32* %num_frequency_splits32, align 8, !tbaa !81
  %mul33 = mul nsw i32 2, %65
  %add34 = add nsw i32 %mul33, 1
  %add35 = add nsw i32 %add30, %add34
  %66 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master36 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %66, i32 0, i32 54
  %67 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master36, align 4, !tbaa !22
  %num_scans_luma37 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %67, i32 0, i32 21
  store i32 %add35, i32* %num_scans_luma37, align 4, !tbaa !78
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 54
  %69 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master38, align 4, !tbaa !22
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %69, i32 0, i32 17
  %70 = load i32, i32* %dc_scan_opt_mode, align 4, !tbaa !49
  %cmp39 = icmp eq i32 %70, 0
  br i1 %cmp39, label %if.then40, label %if.else42

if.then40:                                        ; preds = %if.end20
  %71 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %72 = load i32, i32* %ncomps, align 4, !tbaa !6
  %call41 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %71, i32 %72, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call41, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end44

if.else42:                                        ; preds = %if.end20
  %73 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call43 = call %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %73, i32 1, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call43, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %if.end44

if.end44:                                         ; preds = %if.else42, %if.then40
  %74 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call45 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %74, i32 0, i32 1, i32 8, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call45, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %75 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call46 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %75, i32 0, i32 9, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call46, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  store i32 0, i32* %Al, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end44
  %76 = load i32, i32* %Al, align 4, !tbaa !6
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master47 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 54
  %78 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master47, align 4, !tbaa !22
  %Al_max_luma48 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %78, i32 0, i32 25
  %79 = load i32, i32* %Al_max_luma48, align 4, !tbaa !79
  %cmp49 = icmp slt i32 %76, %79
  br i1 %cmp49, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %80 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %81 = load i32, i32* %Al, align 4, !tbaa !6
  %add50 = add nsw i32 %81, 1
  %82 = load i32, i32* %Al, align 4, !tbaa !6
  %call51 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %80, i32 0, i32 1, i32 63, i32 %add50, i32 %82)
  store %struct.jpeg_scan_info* %call51, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %83 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %84 = load i32, i32* %Al, align 4, !tbaa !6
  %add52 = add nsw i32 %84, 1
  %call53 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %83, i32 0, i32 1, i32 8, i32 0, i32 %add52)
  store %struct.jpeg_scan_info* %call53, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %85 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %86 = load i32, i32* %Al, align 4, !tbaa !6
  %add54 = add nsw i32 %86, 1
  %call55 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %85, i32 0, i32 9, i32 63, i32 0, i32 %add54)
  store %struct.jpeg_scan_info* %call55, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %87 = load i32, i32* %Al, align 4, !tbaa !6
  %inc = add nsw i32 %87, 1
  store i32 %inc, i32* %Al, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %88 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call56 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %88, i32 0, i32 1, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call56, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc67, %for.end
  %89 = load i32, i32* %i, align 4, !tbaa !6
  %90 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master58 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %90, i32 0, i32 54
  %91 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master58, align 4, !tbaa !22
  %num_frequency_splits59 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %91, i32 0, i32 24
  %92 = load i32, i32* %num_frequency_splits59, align 8, !tbaa !81
  %cmp60 = icmp slt i32 %89, %92
  br i1 %cmp60, label %for.body61, label %for.end69

for.body61:                                       ; preds = %for.cond57
  %93 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %94 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %94
  %95 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %call63 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %93, i32 0, i32 1, i32 %95, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call63, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %96 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %97 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %add65 = add nsw i32 %98, 1
  %call66 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %96, i32 0, i32 %add65, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call66, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %for.inc67

for.inc67:                                        ; preds = %for.body61
  %99 = load i32, i32* %i, align 4, !tbaa !6
  %inc68 = add nsw i32 %99, 1
  store i32 %inc68, i32* %i, align 4, !tbaa !6
  br label %for.cond57

for.end69:                                        ; preds = %for.cond57
  %100 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp70 = icmp eq i32 %100, 1
  br i1 %cmp70, label %if.then71, label %if.else74

if.then71:                                        ; preds = %for.end69
  %101 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master72 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %101, i32 0, i32 54
  %102 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master72, align 4, !tbaa !22
  %Al_max_chroma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %102, i32 0, i32 26
  store i32 0, i32* %Al_max_chroma, align 8, !tbaa !82
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master73 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 54
  %104 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master73, align 4, !tbaa !22
  %num_scans_chroma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %104, i32 0, i32 23
  store i32 0, i32* %num_scans_chroma_dc, align 4, !tbaa !83
  br label %if.end126

if.else74:                                        ; preds = %for.end69
  %105 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master75 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %105, i32 0, i32 54
  %106 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master75, align 4, !tbaa !22
  %Al_max_chroma76 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %106, i32 0, i32 26
  store i32 2, i32* %Al_max_chroma76, align 8, !tbaa !82
  %107 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master77 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %107, i32 0, i32 54
  %108 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master77, align 4, !tbaa !22
  %num_scans_chroma_dc78 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %108, i32 0, i32 23
  store i32 3, i32* %num_scans_chroma_dc78, align 4, !tbaa !83
  %109 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call79 = call %struct.jpeg_scan_info* @fill_a_scan_pair(%struct.jpeg_scan_info* %109, i32 1, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call79, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %110 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call80 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %110, i32 1, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call80, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %111 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call81 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %111, i32 2, i32 0, i32 0, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call81, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %112 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call82 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %112, i32 1, i32 1, i32 8, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call82, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %113 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call83 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %113, i32 1, i32 9, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call83, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %114 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call84 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %114, i32 2, i32 1, i32 8, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call84, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %115 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call85 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %115, i32 2, i32 9, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call85, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  store i32 0, i32* %Al, align 4, !tbaa !6
  br label %for.cond86

for.cond86:                                       ; preds = %for.inc103, %if.else74
  %116 = load i32, i32* %Al, align 4, !tbaa !6
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master87 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 54
  %118 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master87, align 4, !tbaa !22
  %Al_max_chroma88 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %118, i32 0, i32 26
  %119 = load i32, i32* %Al_max_chroma88, align 8, !tbaa !82
  %cmp89 = icmp slt i32 %116, %119
  br i1 %cmp89, label %for.body90, label %for.end105

for.body90:                                       ; preds = %for.cond86
  %120 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %121 = load i32, i32* %Al, align 4, !tbaa !6
  %add91 = add nsw i32 %121, 1
  %122 = load i32, i32* %Al, align 4, !tbaa !6
  %call92 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %120, i32 1, i32 1, i32 63, i32 %add91, i32 %122)
  store %struct.jpeg_scan_info* %call92, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %123 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %124 = load i32, i32* %Al, align 4, !tbaa !6
  %add93 = add nsw i32 %124, 1
  %125 = load i32, i32* %Al, align 4, !tbaa !6
  %call94 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %123, i32 2, i32 1, i32 63, i32 %add93, i32 %125)
  store %struct.jpeg_scan_info* %call94, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %126 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %127 = load i32, i32* %Al, align 4, !tbaa !6
  %add95 = add nsw i32 %127, 1
  %call96 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %126, i32 1, i32 1, i32 8, i32 0, i32 %add95)
  store %struct.jpeg_scan_info* %call96, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %128 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %129 = load i32, i32* %Al, align 4, !tbaa !6
  %add97 = add nsw i32 %129, 1
  %call98 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %128, i32 1, i32 9, i32 63, i32 0, i32 %add97)
  store %struct.jpeg_scan_info* %call98, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %130 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %131 = load i32, i32* %Al, align 4, !tbaa !6
  %add99 = add nsw i32 %131, 1
  %call100 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %130, i32 2, i32 1, i32 8, i32 0, i32 %add99)
  store %struct.jpeg_scan_info* %call100, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %132 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %133 = load i32, i32* %Al, align 4, !tbaa !6
  %add101 = add nsw i32 %133, 1
  %call102 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %132, i32 2, i32 9, i32 63, i32 0, i32 %add101)
  store %struct.jpeg_scan_info* %call102, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %for.inc103

for.inc103:                                       ; preds = %for.body90
  %134 = load i32, i32* %Al, align 4, !tbaa !6
  %inc104 = add nsw i32 %134, 1
  store i32 %inc104, i32* %Al, align 4, !tbaa !6
  br label %for.cond86

for.end105:                                       ; preds = %for.cond86
  %135 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call106 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %135, i32 1, i32 1, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call106, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %136 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %call107 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %136, i32 2, i32 1, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call107, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc123, %for.end105
  %137 = load i32, i32* %i, align 4, !tbaa !6
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master109 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 54
  %139 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master109, align 4, !tbaa !22
  %num_frequency_splits110 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %139, i32 0, i32 24
  %140 = load i32, i32* %num_frequency_splits110, align 8, !tbaa !81
  %cmp111 = icmp slt i32 %137, %140
  br i1 %cmp111, label %for.body112, label %for.end125

for.body112:                                      ; preds = %for.cond108
  %141 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %142 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %142
  %143 = load i32, i32* %arrayidx113, align 4, !tbaa !6
  %call114 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %141, i32 1, i32 1, i32 %143, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call114, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %144 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %145 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx115 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %145
  %146 = load i32, i32* %arrayidx115, align 4, !tbaa !6
  %add116 = add nsw i32 %146, 1
  %call117 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %144, i32 1, i32 %add116, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call117, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %147 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %148 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx118 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %148
  %149 = load i32, i32* %arrayidx118, align 4, !tbaa !6
  %call119 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %147, i32 2, i32 1, i32 %149, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call119, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %150 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %151 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds [5 x i32], [5 x i32]* %frequency_split, i32 0, i32 %151
  %152 = load i32, i32* %arrayidx120, align 4, !tbaa !6
  %add121 = add nsw i32 %152, 1
  %call122 = call %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %150, i32 2, i32 %add121, i32 63, i32 0, i32 0)
  store %struct.jpeg_scan_info* %call122, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  br label %for.inc123

for.inc123:                                       ; preds = %for.body112
  %153 = load i32, i32* %i, align 4, !tbaa !6
  %inc124 = add nsw i32 %153, 1
  store i32 %inc124, i32* %i, align 4, !tbaa !6
  br label %for.cond108

for.end125:                                       ; preds = %for.cond108
  br label %if.end126

if.end126:                                        ; preds = %for.end125, %if.then71
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end126, %if.else10
  %154 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  %155 = bitcast [5 x i32]* %frequency_split to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %155) #4
  %156 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = load i32, i32* %retval, align 4
  ret i32 %160
}

; Function Attrs: nounwind
define internal %struct.jpeg_scan_info* @fill_dc_scans(%struct.jpeg_scan_info* %scanptr, i32 %ncomps, i32 %Ah, i32 %Al) #0 {
entry:
  %scanptr.addr = alloca %struct.jpeg_scan_info*, align 4
  %ncomps.addr = alloca i32, align 4
  %Ah.addr = alloca i32, align 4
  %Al.addr = alloca i32, align 4
  %ci = alloca i32, align 4
  store %struct.jpeg_scan_info* %scanptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  store i32 %ncomps, i32* %ncomps.addr, align 4, !tbaa !6
  store i32 %Ah, i32* %Ah.addr, align 4, !tbaa !6
  store i32 %Al, i32* %Al.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %ncomps.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %1, 4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %ncomps.addr, align 4, !tbaa !6
  %3 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %3, i32 0, i32 0
  store i32 %2, i32* %comps_in_scan, align 4, !tbaa !84
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %ci, align 4, !tbaa !6
  %5 = load i32, i32* %ncomps.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i32, i32* %ci, align 4, !tbaa !6
  %7 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %7, i32 0, i32 1
  %8 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 %8
  store i32 %6, i32* %arrayidx, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %10, i32 0, i32 3
  store i32 0, i32* %Se, align 4, !tbaa !86
  %11 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %11, i32 0, i32 2
  store i32 0, i32* %Ss, align 4, !tbaa !87
  %12 = load i32, i32* %Ah.addr, align 4, !tbaa !6
  %13 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ah2 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %13, i32 0, i32 4
  store i32 %12, i32* %Ah2, align 4, !tbaa !88
  %14 = load i32, i32* %Al.addr, align 4, !tbaa !6
  %15 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Al3 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %15, i32 0, i32 5
  store i32 %14, i32* %Al3, align 4, !tbaa !89
  %16 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %16, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %17 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %18 = load i32, i32* %ncomps.addr, align 4, !tbaa !6
  %19 = load i32, i32* %Ah.addr, align 4, !tbaa !6
  %20 = load i32, i32* %Al.addr, align 4, !tbaa !6
  %call = call %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %17, i32 %18, i32 0, i32 0, i32 %19, i32 %20)
  store %struct.jpeg_scan_info* %call, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  %21 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %22 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret %struct.jpeg_scan_info* %21
}

; Function Attrs: nounwind
define internal %struct.jpeg_scan_info* @fill_a_scan(%struct.jpeg_scan_info* %scanptr, i32 %ci, i32 %Ss, i32 %Se, i32 %Ah, i32 %Al) #0 {
entry:
  %scanptr.addr = alloca %struct.jpeg_scan_info*, align 4
  %ci.addr = alloca i32, align 4
  %Ss.addr = alloca i32, align 4
  %Se.addr = alloca i32, align 4
  %Ah.addr = alloca i32, align 4
  %Al.addr = alloca i32, align 4
  store %struct.jpeg_scan_info* %scanptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  store i32 %ci, i32* %ci.addr, align 4, !tbaa !6
  store i32 %Ss, i32* %Ss.addr, align 4, !tbaa !6
  store i32 %Se, i32* %Se.addr, align 4, !tbaa !6
  store i32 %Ah, i32* %Ah.addr, align 4, !tbaa !6
  store i32 %Al, i32* %Al.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %0, i32 0, i32 0
  store i32 1, i32* %comps_in_scan, align 4, !tbaa !84
  %1 = load i32, i32* %ci.addr, align 4, !tbaa !6
  %2 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 0
  store i32 %1, i32* %arrayidx, align 4, !tbaa !6
  %3 = load i32, i32* %Ss.addr, align 4, !tbaa !6
  %4 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ss1 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %4, i32 0, i32 2
  store i32 %3, i32* %Ss1, align 4, !tbaa !87
  %5 = load i32, i32* %Se.addr, align 4, !tbaa !6
  %6 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %6, i32 0, i32 3
  store i32 %5, i32* %Se2, align 4, !tbaa !86
  %7 = load i32, i32* %Ah.addr, align 4, !tbaa !6
  %8 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ah3 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %8, i32 0, i32 4
  store i32 %7, i32* %Ah3, align 4, !tbaa !88
  %9 = load i32, i32* %Al.addr, align 4, !tbaa !6
  %10 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Al4 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %10, i32 0, i32 5
  store i32 %9, i32* %Al4, align 4, !tbaa !89
  %11 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %11, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %12 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  ret %struct.jpeg_scan_info* %12
}

; Function Attrs: nounwind
define internal %struct.jpeg_scan_info* @fill_a_scan_pair(%struct.jpeg_scan_info* %scanptr, i32 %ci, i32 %Ss, i32 %Se, i32 %Ah, i32 %Al) #0 {
entry:
  %scanptr.addr = alloca %struct.jpeg_scan_info*, align 4
  %ci.addr = alloca i32, align 4
  %Ss.addr = alloca i32, align 4
  %Se.addr = alloca i32, align 4
  %Ah.addr = alloca i32, align 4
  %Al.addr = alloca i32, align 4
  store %struct.jpeg_scan_info* %scanptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  store i32 %ci, i32* %ci.addr, align 4, !tbaa !6
  store i32 %Ss, i32* %Ss.addr, align 4, !tbaa !6
  store i32 %Se, i32* %Se.addr, align 4, !tbaa !6
  store i32 %Ah, i32* %Ah.addr, align 4, !tbaa !6
  store i32 %Al, i32* %Al.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %0, i32 0, i32 0
  store i32 2, i32* %comps_in_scan, align 4, !tbaa !84
  %1 = load i32, i32* %ci.addr, align 4, !tbaa !6
  %2 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 0
  store i32 %1, i32* %arrayidx, align 4, !tbaa !6
  %3 = load i32, i32* %ci.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 1
  %4 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %component_index1 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %4, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index1, i32 0, i32 1
  store i32 %add, i32* %arrayidx2, align 4, !tbaa !6
  %5 = load i32, i32* %Ss.addr, align 4, !tbaa !6
  %6 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ss3 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %6, i32 0, i32 2
  store i32 %5, i32* %Ss3, align 4, !tbaa !87
  %7 = load i32, i32* %Se.addr, align 4, !tbaa !6
  %8 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Se4 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %8, i32 0, i32 3
  store i32 %7, i32* %Se4, align 4, !tbaa !86
  %9 = load i32, i32* %Ah.addr, align 4, !tbaa !6
  %10 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ah5 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %10, i32 0, i32 4
  store i32 %9, i32* %Ah5, align 4, !tbaa !88
  %11 = load i32, i32* %Al.addr, align 4, !tbaa !6
  %12 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Al6 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %12, i32 0, i32 5
  store i32 %11, i32* %Al6, align 4, !tbaa !89
  %13 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %13, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %14 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  ret %struct.jpeg_scan_info* %14
}

; Function Attrs: nounwind
define internal %struct.jpeg_scan_info* @fill_scans(%struct.jpeg_scan_info* %scanptr, i32 %ncomps, i32 %Ss, i32 %Se, i32 %Ah, i32 %Al) #0 {
entry:
  %scanptr.addr = alloca %struct.jpeg_scan_info*, align 4
  %ncomps.addr = alloca i32, align 4
  %Ss.addr = alloca i32, align 4
  %Se.addr = alloca i32, align 4
  %Ah.addr = alloca i32, align 4
  %Al.addr = alloca i32, align 4
  %ci = alloca i32, align 4
  store %struct.jpeg_scan_info* %scanptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  store i32 %ncomps, i32* %ncomps.addr, align 4, !tbaa !6
  store i32 %Ss, i32* %Ss.addr, align 4, !tbaa !6
  store i32 %Se, i32* %Se.addr, align 4, !tbaa !6
  store i32 %Ah, i32* %Ah.addr, align 4, !tbaa !6
  store i32 %Al, i32* %Al.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %ci, align 4, !tbaa !6
  %2 = load i32, i32* %ncomps.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %3, i32 0, i32 0
  store i32 1, i32* %comps_in_scan, align 4, !tbaa !84
  %4 = load i32, i32* %ci, align 4, !tbaa !6
  %5 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %5, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 0
  store i32 %4, i32* %arrayidx, align 4, !tbaa !6
  %6 = load i32, i32* %Ss.addr, align 4, !tbaa !6
  %7 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ss1 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %7, i32 0, i32 2
  store i32 %6, i32* %Ss1, align 4, !tbaa !87
  %8 = load i32, i32* %Se.addr, align 4, !tbaa !6
  %9 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %9, i32 0, i32 3
  store i32 %8, i32* %Se2, align 4, !tbaa !86
  %10 = load i32, i32* %Ah.addr, align 4, !tbaa !6
  %11 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Ah3 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %11, i32 0, i32 4
  store i32 %10, i32* %Ah3, align 4, !tbaa !88
  %12 = load i32, i32* %Al.addr, align 4, !tbaa !6
  %13 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %Al4 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %13, i32 0, i32 5
  store i32 %12, i32* %Al4, align 4, !tbaa !89
  %14 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %14, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr.addr, align 4, !tbaa !2
  %17 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret %struct.jpeg_scan_info* %16
}

; Function Attrs: nounwind
define internal void @add_huff_table(%struct.jpeg_common_struct* %cinfo, %struct.JHUFF_TBL** %htblptr, i8* %bits, i8* %val) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %htblptr.addr = alloca %struct.JHUFF_TBL**, align 4
  %bits.addr = alloca i8*, align 4
  %val.addr = alloca i8*, align 4
  %nsymbols = alloca i32, align 4
  %len = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.JHUFF_TBL** %htblptr, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  store i8* %bits, i8** %bits.addr, align 4, !tbaa !2
  store i8* %val, i8** %val.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nsymbols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %3 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %2, align 4, !tbaa !2
  %cmp = icmp eq %struct.JHUFF_TBL* %3, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct* %4)
  %5 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %call, %struct.JHUFF_TBL** %5, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %6 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %7 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %6, align 4, !tbaa !2
  %bits1 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %7, i32 0, i32 0
  %arraydecay = getelementptr inbounds [17 x i8], [17 x i8]* %bits1, i32 0, i32 0
  %8 = load i8*, i8** %bits.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay, i8* align 1 %8, i32 17, i1 false)
  store i32 0, i32* %nsymbols, align 4, !tbaa !6
  store i32 1, i32* %len, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %len, align 4, !tbaa !6
  %cmp2 = icmp sle i32 %9, 16
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8*, i8** %bits.addr, align 4, !tbaa !2
  %11 = load i32, i32* %len, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !16
  %conv = zext i8 %12 to i32
  %13 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %add = add nsw i32 %13, %conv
  store i32 %add, i32* %nsymbols, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %len, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %len, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %15, 1
  br i1 %cmp3, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %16 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %cmp5 = icmp sgt i32 %16, 256
  br i1 %cmp5, label %if.then7, label %if.end9

if.then7:                                         ; preds = %lor.lhs.false, %for.end
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !90
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 8, i32* %msg_code, align 4, !tbaa !13
  %19 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 4, !tbaa !90
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !17
  %22 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %21(%struct.jpeg_common_struct* %22)
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %lor.lhs.false
  %23 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %24 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %23, align 4, !tbaa !2
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %24, i32 0, i32 1
  %arraydecay10 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 0
  %25 = load i8*, i8** %val.addr, align 4, !tbaa !2
  %26 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %mul = mul i32 %26, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay10, i8* align 1 %25, i32 %mul, i1 false)
  %27 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %28 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %27, align 4, !tbaa !2
  %huffval11 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %28, i32 0, i32 1
  %29 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval11, i32 0, i32 %29
  %30 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %sub = sub nsw i32 256, %30
  %mul13 = mul i32 %sub, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx12, i8 0, i32 %mul13, i1 false)
  %31 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %32 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %31, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %32, i32 0, i32 2
  store i32 0, i32* %sent_table, align 4, !tbaa !91
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.else
  %33 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %nsymbols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 20}
!9 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !10, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !11, i64 216, !11, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!9, !3, i64 0}
!13 = !{!14, !7, i64 20}
!14 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !15, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!15 = !{!"long", !4, i64 0}
!16 = !{!4, !4, i64 0}
!17 = !{!14, !3, i64 0}
!18 = !{!15, !15, i64 0}
!19 = !{!11, !11, i64 0}
!20 = !{!21, !7, i64 128}
!21 = !{!"", !4, i64 0, !7, i64 128}
!22 = !{!9, !3, i64 332}
!23 = !{!24, !7, i64 4160}
!24 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !4, i64 56, !4, i64 2104, !7, i64 4152, !7, i64 4156, !7, i64 4160, !7, i64 4164, !7, i64 4168, !7, i64 4172, !7, i64 4176, !7, i64 4180, !7, i64 4184, !7, i64 4188, !7, i64 4192, !25, i64 4196, !25, i64 4200, !25, i64 4204}
!25 = !{!"float", !4, i64 0}
!26 = !{!25, !25, i64 0}
!27 = !{!9, !3, i64 68}
!28 = !{!9, !3, i64 4}
!29 = !{!30, !3, i64 0}
!30 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !15, i64 44, !15, i64 48}
!31 = !{!9, !7, i64 56}
!32 = !{!9, !3, i64 172}
!33 = !{!9, !7, i64 168}
!34 = !{!9, !7, i64 176}
!35 = !{!9, !7, i64 180}
!36 = !{!24, !7, i64 4152}
!37 = !{!9, !7, i64 184}
!38 = !{!9, !7, i64 188}
!39 = !{!24, !7, i64 52}
!40 = !{!9, !7, i64 192}
!41 = !{!9, !4, i64 196}
!42 = !{!9, !7, i64 200}
!43 = !{!9, !7, i64 204}
!44 = !{!9, !4, i64 212}
!45 = !{!9, !4, i64 213}
!46 = !{!9, !4, i64 214}
!47 = !{!9, !11, i64 216}
!48 = !{!9, !11, i64 218}
!49 = !{!24, !7, i64 4156}
!50 = !{!24, !7, i64 20}
!51 = !{!24, !7, i64 24}
!52 = !{!24, !25, i64 4196}
!53 = !{!24, !25, i64 4200}
!54 = !{!24, !7, i64 36}
!55 = !{!24, !7, i64 40}
!56 = !{!24, !7, i64 4164}
!57 = !{!24, !7, i64 4168}
!58 = !{!24, !7, i64 48}
!59 = !{!24, !7, i64 28}
!60 = !{!24, !25, i64 4204}
!61 = !{!62, !7, i64 16}
!62 = !{!"jpeg_common_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20}
!63 = !{!9, !4, i64 40}
!64 = !{!9, !7, i64 60}
!65 = !{!9, !4, i64 64}
!66 = !{!9, !3, i64 368}
!67 = !{!9, !7, i64 372}
!68 = !{!9, !7, i64 208}
!69 = !{!9, !7, i64 220}
!70 = !{!71, !7, i64 0}
!71 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!72 = !{!71, !7, i64 8}
!73 = !{!71, !7, i64 12}
!74 = !{!71, !7, i64 16}
!75 = !{!71, !7, i64 20}
!76 = !{!71, !7, i64 24}
!77 = !{!9, !7, i64 36}
!78 = !{!24, !7, i64 4172}
!79 = !{!24, !7, i64 4188}
!80 = !{!24, !7, i64 4176}
!81 = !{!24, !7, i64 4184}
!82 = !{!24, !7, i64 4192}
!83 = !{!24, !7, i64 4180}
!84 = !{!85, !7, i64 0}
!85 = !{!"", !7, i64 0, !4, i64 4, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32}
!86 = !{!85, !7, i64 24}
!87 = !{!85, !7, i64 20}
!88 = !{!85, !7, i64 28}
!89 = !{!85, !7, i64 32}
!90 = !{!62, !3, i64 0}
!91 = !{!92, !7, i64 276}
!92 = !{!"", !4, i64 0, !4, i64 17, !7, i64 276}
