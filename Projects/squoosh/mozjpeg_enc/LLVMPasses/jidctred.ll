; ModuleID = 'jidctred.c'
source_filename = "jidctred.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x {}*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }

; Function Attrs: nounwind
define hidden void @jpeg_idct_4x4(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [32 x i32], align 16
  %dcval = alloca i32, align 4
  %dcval131 = alloca i8, align 1
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 65
  %14 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %15 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast [32 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %16) #2
  %17 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %17, i16** %inptr, align 4, !tbaa !2
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 20
  %19 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %20 = bitcast i8* %19 to i32*
  store i32* %20, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [32 x i32], [32 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 8, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %21 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %21, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %22, 4
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %23 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 8
  %24 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %24 to i32
  %cmp2 = icmp eq i32 %conv, 0
  br i1 %cmp2, label %land.lhs.true, label %if.end36

land.lhs.true:                                    ; preds = %if.end
  %25 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %25, i32 16
  %26 = load i16, i16* %arrayidx4, align 2, !tbaa !14
  %conv5 = sext i16 %26 to i32
  %cmp6 = icmp eq i32 %conv5, 0
  br i1 %cmp6, label %land.lhs.true8, label %if.end36

land.lhs.true8:                                   ; preds = %land.lhs.true
  %27 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i16, i16* %27, i32 24
  %28 = load i16, i16* %arrayidx9, align 2, !tbaa !14
  %conv10 = sext i16 %28 to i32
  %cmp11 = icmp eq i32 %conv10, 0
  br i1 %cmp11, label %land.lhs.true13, label %if.end36

land.lhs.true13:                                  ; preds = %land.lhs.true8
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i16, i16* %29, i32 40
  %30 = load i16, i16* %arrayidx14, align 2, !tbaa !14
  %conv15 = sext i16 %30 to i32
  %cmp16 = icmp eq i32 %conv15, 0
  br i1 %cmp16, label %land.lhs.true18, label %if.end36

land.lhs.true18:                                  ; preds = %land.lhs.true13
  %31 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i16, i16* %31, i32 48
  %32 = load i16, i16* %arrayidx19, align 2, !tbaa !14
  %conv20 = sext i16 %32 to i32
  %cmp21 = icmp eq i32 %conv20, 0
  br i1 %cmp21, label %land.lhs.true23, label %if.end36

land.lhs.true23:                                  ; preds = %land.lhs.true18
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i16, i16* %33, i32 56
  %34 = load i16, i16* %arrayidx24, align 2, !tbaa !14
  %conv25 = sext i16 %34 to i32
  %cmp26 = icmp eq i32 %conv25, 0
  br i1 %cmp26, label %if.then28, label %if.end36

if.then28:                                        ; preds = %land.lhs.true23
  %35 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #2
  %36 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i16, i16* %36, i32 0
  %37 = load i16, i16* %arrayidx29, align 2, !tbaa !14
  %conv30 = sext i16 %37 to i32
  %38 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %38, i32 0
  %39 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  %mul = mul nsw i32 %conv30, %39
  %shl = shl i32 %mul, 2
  store i32 %shl, i32* %dcval, align 4, !tbaa !6
  %40 = load i32, i32* %dcval, align 4, !tbaa !6
  %41 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %41, i32 0
  store i32 %40, i32* %arrayidx32, align 4, !tbaa !6
  %42 = load i32, i32* %dcval, align 4, !tbaa !6
  %43 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %43, i32 8
  store i32 %42, i32* %arrayidx33, align 4, !tbaa !6
  %44 = load i32, i32* %dcval, align 4, !tbaa !6
  %45 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %45, i32 16
  store i32 %44, i32* %arrayidx34, align 4, !tbaa !6
  %46 = load i32, i32* %dcval, align 4, !tbaa !6
  %47 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %47, i32 24
  store i32 %46, i32* %arrayidx35, align 4, !tbaa !6
  %48 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #2
  br label %for.inc

if.end36:                                         ; preds = %land.lhs.true23, %land.lhs.true18, %land.lhs.true13, %land.lhs.true8, %land.lhs.true, %if.end
  %49 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i16, i16* %49, i32 0
  %50 = load i16, i16* %arrayidx37, align 2, !tbaa !14
  %conv38 = sext i16 %50 to i32
  %51 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %51, i32 0
  %52 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %mul40 = mul nsw i32 %conv38, %52
  store i32 %mul40, i32* %tmp0, align 4, !tbaa !15
  %53 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl41 = shl i32 %53, 14
  store i32 %shl41, i32* %tmp0, align 4, !tbaa !15
  %54 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i16, i16* %54, i32 16
  %55 = load i16, i16* %arrayidx42, align 2, !tbaa !14
  %conv43 = sext i16 %55 to i32
  %56 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %56, i32 16
  %57 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %mul45 = mul nsw i32 %conv43, %57
  store i32 %mul45, i32* %z2, align 4, !tbaa !15
  %58 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i16, i16* %58, i32 48
  %59 = load i16, i16* %arrayidx46, align 2, !tbaa !14
  %conv47 = sext i16 %59 to i32
  %60 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %60, i32 48
  %61 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %mul49 = mul nsw i32 %conv47, %61
  store i32 %mul49, i32* %z3, align 4, !tbaa !15
  %62 = load i32, i32* %z2, align 4, !tbaa !15
  %mul50 = mul nsw i32 %62, 15137
  %63 = load i32, i32* %z3, align 4, !tbaa !15
  %mul51 = mul nsw i32 %63, -6270
  %add = add nsw i32 %mul50, %mul51
  store i32 %add, i32* %tmp2, align 4, !tbaa !15
  %64 = load i32, i32* %tmp0, align 4, !tbaa !15
  %65 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add52 = add nsw i32 %64, %65
  store i32 %add52, i32* %tmp10, align 4, !tbaa !15
  %66 = load i32, i32* %tmp0, align 4, !tbaa !15
  %67 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub = sub nsw i32 %66, %67
  store i32 %sub, i32* %tmp12, align 4, !tbaa !15
  %68 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i16, i16* %68, i32 56
  %69 = load i16, i16* %arrayidx53, align 2, !tbaa !14
  %conv54 = sext i16 %69 to i32
  %70 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %70, i32 56
  %71 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %mul56 = mul nsw i32 %conv54, %71
  store i32 %mul56, i32* %z1, align 4, !tbaa !15
  %72 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i16, i16* %72, i32 40
  %73 = load i16, i16* %arrayidx57, align 2, !tbaa !14
  %conv58 = sext i16 %73 to i32
  %74 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %74, i32 40
  %75 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %mul60 = mul nsw i32 %conv58, %75
  store i32 %mul60, i32* %z2, align 4, !tbaa !15
  %76 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i16, i16* %76, i32 24
  %77 = load i16, i16* %arrayidx61, align 2, !tbaa !14
  %conv62 = sext i16 %77 to i32
  %78 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %78, i32 24
  %79 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %mul64 = mul nsw i32 %conv62, %79
  store i32 %mul64, i32* %z3, align 4, !tbaa !15
  %80 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i16, i16* %80, i32 8
  %81 = load i16, i16* %arrayidx65, align 2, !tbaa !14
  %conv66 = sext i16 %81 to i32
  %82 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %82, i32 8
  %83 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %mul68 = mul nsw i32 %conv66, %83
  store i32 %mul68, i32* %z4, align 4, !tbaa !15
  %84 = load i32, i32* %z1, align 4, !tbaa !15
  %mul69 = mul nsw i32 %84, -1730
  %85 = load i32, i32* %z2, align 4, !tbaa !15
  %mul70 = mul nsw i32 %85, 11893
  %add71 = add nsw i32 %mul69, %mul70
  %86 = load i32, i32* %z3, align 4, !tbaa !15
  %mul72 = mul nsw i32 %86, -17799
  %add73 = add nsw i32 %add71, %mul72
  %87 = load i32, i32* %z4, align 4, !tbaa !15
  %mul74 = mul nsw i32 %87, 8697
  %add75 = add nsw i32 %add73, %mul74
  store i32 %add75, i32* %tmp0, align 4, !tbaa !15
  %88 = load i32, i32* %z1, align 4, !tbaa !15
  %mul76 = mul nsw i32 %88, -4176
  %89 = load i32, i32* %z2, align 4, !tbaa !15
  %mul77 = mul nsw i32 %89, -4926
  %add78 = add nsw i32 %mul76, %mul77
  %90 = load i32, i32* %z3, align 4, !tbaa !15
  %mul79 = mul nsw i32 %90, 7373
  %add80 = add nsw i32 %add78, %mul79
  %91 = load i32, i32* %z4, align 4, !tbaa !15
  %mul81 = mul nsw i32 %91, 20995
  %add82 = add nsw i32 %add80, %mul81
  store i32 %add82, i32* %tmp2, align 4, !tbaa !15
  %92 = load i32, i32* %tmp10, align 4, !tbaa !15
  %93 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add83 = add nsw i32 %92, %93
  %add84 = add nsw i32 %add83, 2048
  %shr = ashr i32 %add84, 12
  %94 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %94, i32 0
  store i32 %shr, i32* %arrayidx85, align 4, !tbaa !6
  %95 = load i32, i32* %tmp10, align 4, !tbaa !15
  %96 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub86 = sub nsw i32 %95, %96
  %add87 = add nsw i32 %sub86, 2048
  %shr88 = ashr i32 %add87, 12
  %97 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %97, i32 24
  store i32 %shr88, i32* %arrayidx89, align 4, !tbaa !6
  %98 = load i32, i32* %tmp12, align 4, !tbaa !15
  %99 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add90 = add nsw i32 %98, %99
  %add91 = add nsw i32 %add90, 2048
  %shr92 = ashr i32 %add91, 12
  %100 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %100, i32 8
  store i32 %shr92, i32* %arrayidx93, align 4, !tbaa !6
  %101 = load i32, i32* %tmp12, align 4, !tbaa !15
  %102 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub94 = sub nsw i32 %101, %102
  %add95 = add nsw i32 %sub94, 2048
  %shr96 = ashr i32 %add95, 12
  %103 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %103, i32 16
  store i32 %shr96, i32* %arrayidx97, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %if.end36, %if.then28, %if.then
  %104 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %104, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %105 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr98 = getelementptr inbounds i32, i32* %105, i32 1
  store i32* %incdec.ptr98, i32** %quantptr, align 4, !tbaa !2
  %106 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr99 = getelementptr inbounds i32, i32* %106, i32 1
  store i32* %incdec.ptr99, i32** %wsptr, align 4, !tbaa !2
  %107 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %107, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay100 = getelementptr inbounds [32 x i32], [32 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay100, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc194, %for.end
  %108 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp102 = icmp slt i32 %108, 4
  br i1 %cmp102, label %for.body104, label %for.end195

for.body104:                                      ; preds = %for.cond101
  %109 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %110 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds i8*, i8** %109, i32 %110
  %111 = load i8*, i8** %arrayidx105, align 4, !tbaa !2
  %112 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr106 = getelementptr inbounds i8, i8* %111, i32 %112
  store i8* %add.ptr106, i8** %outptr, align 4, !tbaa !2
  %113 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds i32, i32* %113, i32 1
  %114 = load i32, i32* %arrayidx107, align 4, !tbaa !6
  %cmp108 = icmp eq i32 %114, 0
  br i1 %cmp108, label %land.lhs.true110, label %if.end141

land.lhs.true110:                                 ; preds = %for.body104
  %115 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %115, i32 2
  %116 = load i32, i32* %arrayidx111, align 4, !tbaa !6
  %cmp112 = icmp eq i32 %116, 0
  br i1 %cmp112, label %land.lhs.true114, label %if.end141

land.lhs.true114:                                 ; preds = %land.lhs.true110
  %117 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %117, i32 3
  %118 = load i32, i32* %arrayidx115, align 4, !tbaa !6
  %cmp116 = icmp eq i32 %118, 0
  br i1 %cmp116, label %land.lhs.true118, label %if.end141

land.lhs.true118:                                 ; preds = %land.lhs.true114
  %119 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i32, i32* %119, i32 5
  %120 = load i32, i32* %arrayidx119, align 4, !tbaa !6
  %cmp120 = icmp eq i32 %120, 0
  br i1 %cmp120, label %land.lhs.true122, label %if.end141

land.lhs.true122:                                 ; preds = %land.lhs.true118
  %121 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %121, i32 6
  %122 = load i32, i32* %arrayidx123, align 4, !tbaa !6
  %cmp124 = icmp eq i32 %122, 0
  br i1 %cmp124, label %land.lhs.true126, label %if.end141

land.lhs.true126:                                 ; preds = %land.lhs.true122
  %123 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %123, i32 7
  %124 = load i32, i32* %arrayidx127, align 4, !tbaa !6
  %cmp128 = icmp eq i32 %124, 0
  br i1 %cmp128, label %if.then130, label %if.end141

if.then130:                                       ; preds = %land.lhs.true126
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %dcval131) #2
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %126, i32 0
  %127 = load i32, i32* %arrayidx132, align 4, !tbaa !6
  %add133 = add nsw i32 %127, 16
  %shr134 = ashr i32 %add133, 5
  %and = and i32 %shr134, 1023
  %arrayidx135 = getelementptr inbounds i8, i8* %125, i32 %and
  %128 = load i8, i8* %arrayidx135, align 1, !tbaa !17
  store i8 %128, i8* %dcval131, align 1, !tbaa !17
  %129 = load i8, i8* %dcval131, align 1, !tbaa !17
  %130 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i8, i8* %130, i32 0
  store i8 %129, i8* %arrayidx136, align 1, !tbaa !17
  %131 = load i8, i8* %dcval131, align 1, !tbaa !17
  %132 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i8, i8* %132, i32 1
  store i8 %131, i8* %arrayidx137, align 1, !tbaa !17
  %133 = load i8, i8* %dcval131, align 1, !tbaa !17
  %134 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i8, i8* %134, i32 2
  store i8 %133, i8* %arrayidx138, align 1, !tbaa !17
  %135 = load i8, i8* %dcval131, align 1, !tbaa !17
  %136 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i8, i8* %136, i32 3
  store i8 %135, i8* %arrayidx139, align 1, !tbaa !17
  %137 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr140 = getelementptr inbounds i32, i32* %137, i32 8
  store i32* %add.ptr140, i32** %wsptr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %dcval131) #2
  br label %for.inc194

if.end141:                                        ; preds = %land.lhs.true126, %land.lhs.true122, %land.lhs.true118, %land.lhs.true114, %land.lhs.true110, %for.body104
  %138 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %138, i32 0
  %139 = load i32, i32* %arrayidx142, align 4, !tbaa !6
  %shl143 = shl i32 %139, 14
  store i32 %shl143, i32* %tmp0, align 4, !tbaa !15
  %140 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %140, i32 2
  %141 = load i32, i32* %arrayidx144, align 4, !tbaa !6
  %mul145 = mul nsw i32 %141, 15137
  %142 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %142, i32 6
  %143 = load i32, i32* %arrayidx146, align 4, !tbaa !6
  %mul147 = mul nsw i32 %143, -6270
  %add148 = add nsw i32 %mul145, %mul147
  store i32 %add148, i32* %tmp2, align 4, !tbaa !15
  %144 = load i32, i32* %tmp0, align 4, !tbaa !15
  %145 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add149 = add nsw i32 %144, %145
  store i32 %add149, i32* %tmp10, align 4, !tbaa !15
  %146 = load i32, i32* %tmp0, align 4, !tbaa !15
  %147 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub150 = sub nsw i32 %146, %147
  store i32 %sub150, i32* %tmp12, align 4, !tbaa !15
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %148, i32 7
  %149 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  store i32 %149, i32* %z1, align 4, !tbaa !15
  %150 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %150, i32 5
  %151 = load i32, i32* %arrayidx152, align 4, !tbaa !6
  store i32 %151, i32* %z2, align 4, !tbaa !15
  %152 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %152, i32 3
  %153 = load i32, i32* %arrayidx153, align 4, !tbaa !6
  store i32 %153, i32* %z3, align 4, !tbaa !15
  %154 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %154, i32 1
  %155 = load i32, i32* %arrayidx154, align 4, !tbaa !6
  store i32 %155, i32* %z4, align 4, !tbaa !15
  %156 = load i32, i32* %z1, align 4, !tbaa !15
  %mul155 = mul nsw i32 %156, -1730
  %157 = load i32, i32* %z2, align 4, !tbaa !15
  %mul156 = mul nsw i32 %157, 11893
  %add157 = add nsw i32 %mul155, %mul156
  %158 = load i32, i32* %z3, align 4, !tbaa !15
  %mul158 = mul nsw i32 %158, -17799
  %add159 = add nsw i32 %add157, %mul158
  %159 = load i32, i32* %z4, align 4, !tbaa !15
  %mul160 = mul nsw i32 %159, 8697
  %add161 = add nsw i32 %add159, %mul160
  store i32 %add161, i32* %tmp0, align 4, !tbaa !15
  %160 = load i32, i32* %z1, align 4, !tbaa !15
  %mul162 = mul nsw i32 %160, -4176
  %161 = load i32, i32* %z2, align 4, !tbaa !15
  %mul163 = mul nsw i32 %161, -4926
  %add164 = add nsw i32 %mul162, %mul163
  %162 = load i32, i32* %z3, align 4, !tbaa !15
  %mul165 = mul nsw i32 %162, 7373
  %add166 = add nsw i32 %add164, %mul165
  %163 = load i32, i32* %z4, align 4, !tbaa !15
  %mul167 = mul nsw i32 %163, 20995
  %add168 = add nsw i32 %add166, %mul167
  store i32 %add168, i32* %tmp2, align 4, !tbaa !15
  %164 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %165 = load i32, i32* %tmp10, align 4, !tbaa !15
  %166 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add169 = add nsw i32 %165, %166
  %add170 = add nsw i32 %add169, 262144
  %shr171 = ashr i32 %add170, 19
  %and172 = and i32 %shr171, 1023
  %arrayidx173 = getelementptr inbounds i8, i8* %164, i32 %and172
  %167 = load i8, i8* %arrayidx173, align 1, !tbaa !17
  %168 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i8, i8* %168, i32 0
  store i8 %167, i8* %arrayidx174, align 1, !tbaa !17
  %169 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %170 = load i32, i32* %tmp10, align 4, !tbaa !15
  %171 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub175 = sub nsw i32 %170, %171
  %add176 = add nsw i32 %sub175, 262144
  %shr177 = ashr i32 %add176, 19
  %and178 = and i32 %shr177, 1023
  %arrayidx179 = getelementptr inbounds i8, i8* %169, i32 %and178
  %172 = load i8, i8* %arrayidx179, align 1, !tbaa !17
  %173 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i8, i8* %173, i32 3
  store i8 %172, i8* %arrayidx180, align 1, !tbaa !17
  %174 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %175 = load i32, i32* %tmp12, align 4, !tbaa !15
  %176 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add181 = add nsw i32 %175, %176
  %add182 = add nsw i32 %add181, 262144
  %shr183 = ashr i32 %add182, 19
  %and184 = and i32 %shr183, 1023
  %arrayidx185 = getelementptr inbounds i8, i8* %174, i32 %and184
  %177 = load i8, i8* %arrayidx185, align 1, !tbaa !17
  %178 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i8, i8* %178, i32 1
  store i8 %177, i8* %arrayidx186, align 1, !tbaa !17
  %179 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %180 = load i32, i32* %tmp12, align 4, !tbaa !15
  %181 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub187 = sub nsw i32 %180, %181
  %add188 = add nsw i32 %sub187, 262144
  %shr189 = ashr i32 %add188, 19
  %and190 = and i32 %shr189, 1023
  %arrayidx191 = getelementptr inbounds i8, i8* %179, i32 %and190
  %182 = load i8, i8* %arrayidx191, align 1, !tbaa !17
  %183 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i8, i8* %183, i32 2
  store i8 %182, i8* %arrayidx192, align 1, !tbaa !17
  %184 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr193 = getelementptr inbounds i32, i32* %184, i32 8
  store i32* %add.ptr193, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc194

for.inc194:                                       ; preds = %if.end141, %if.then130
  %185 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %185, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  br label %for.cond101

for.end195:                                       ; preds = %for.cond101
  %186 = bitcast [32 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %186) #2
  %187 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #2
  %188 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #2
  %189 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #2
  %190 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #2
  %191 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #2
  %192 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #2
  %193 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #2
  %194 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #2
  %195 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #2
  %196 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #2
  %197 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #2
  %198 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #2
  %199 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #2
  %200 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jpeg_idct_2x2(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [16 x i32], align 16
  %dcval = alloca i32, align 4
  %dcval86 = alloca i8, align 1
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 65
  %9 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %10 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast [16 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %11) #2
  %12 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %12, i16** %inptr, align 4, !tbaa !2
  %13 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %13, i32 0, i32 20
  %14 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %15 = bitcast i8* %14 to i32*
  store i32* %15, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 8, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %16, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %17, 6
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %18 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %18, 4
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %19 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp4 = icmp eq i32 %19, 2
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false3, %lor.lhs.false, %for.body
  br label %for.inc

if.end:                                           ; preds = %lor.lhs.false3
  %20 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %20, i32 8
  %21 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %21 to i32
  %cmp5 = icmp eq i32 %conv, 0
  br i1 %cmp5, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.end
  %22 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i16, i16* %22, i32 24
  %23 = load i16, i16* %arrayidx7, align 2, !tbaa !14
  %conv8 = sext i16 %23 to i32
  %cmp9 = icmp eq i32 %conv8, 0
  br i1 %cmp9, label %land.lhs.true11, label %if.end27

land.lhs.true11:                                  ; preds = %land.lhs.true
  %24 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %24, i32 40
  %25 = load i16, i16* %arrayidx12, align 2, !tbaa !14
  %conv13 = sext i16 %25 to i32
  %cmp14 = icmp eq i32 %conv13, 0
  br i1 %cmp14, label %land.lhs.true16, label %if.end27

land.lhs.true16:                                  ; preds = %land.lhs.true11
  %26 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %26, i32 56
  %27 = load i16, i16* %arrayidx17, align 2, !tbaa !14
  %conv18 = sext i16 %27 to i32
  %cmp19 = icmp eq i32 %conv18, 0
  br i1 %cmp19, label %if.then21, label %if.end27

if.then21:                                        ; preds = %land.lhs.true16
  %28 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #2
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i16, i16* %29, i32 0
  %30 = load i16, i16* %arrayidx22, align 2, !tbaa !14
  %conv23 = sext i16 %30 to i32
  %31 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %31, i32 0
  %32 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %mul = mul nsw i32 %conv23, %32
  %shl = shl i32 %mul, 2
  store i32 %shl, i32* %dcval, align 4, !tbaa !6
  %33 = load i32, i32* %dcval, align 4, !tbaa !6
  %34 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %34, i32 0
  store i32 %33, i32* %arrayidx25, align 4, !tbaa !6
  %35 = load i32, i32* %dcval, align 4, !tbaa !6
  %36 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %36, i32 8
  store i32 %35, i32* %arrayidx26, align 4, !tbaa !6
  %37 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #2
  br label %for.inc

if.end27:                                         ; preds = %land.lhs.true16, %land.lhs.true11, %land.lhs.true, %if.end
  %38 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %38, i32 0
  %39 = load i16, i16* %arrayidx28, align 2, !tbaa !14
  %conv29 = sext i16 %39 to i32
  %40 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %40, i32 0
  %41 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %mul31 = mul nsw i32 %conv29, %41
  store i32 %mul31, i32* %z1, align 4, !tbaa !15
  %42 = load i32, i32* %z1, align 4, !tbaa !15
  %shl32 = shl i32 %42, 15
  store i32 %shl32, i32* %tmp10, align 4, !tbaa !15
  %43 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %43, i32 56
  %44 = load i16, i16* %arrayidx33, align 2, !tbaa !14
  %conv34 = sext i16 %44 to i32
  %45 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %45, i32 56
  %46 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %mul36 = mul nsw i32 %conv34, %46
  store i32 %mul36, i32* %z1, align 4, !tbaa !15
  %47 = load i32, i32* %z1, align 4, !tbaa !15
  %mul37 = mul nsw i32 %47, -5906
  store i32 %mul37, i32* %tmp0, align 4, !tbaa !15
  %48 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i16, i16* %48, i32 40
  %49 = load i16, i16* %arrayidx38, align 2, !tbaa !14
  %conv39 = sext i16 %49 to i32
  %50 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %50, i32 40
  %51 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %mul41 = mul nsw i32 %conv39, %51
  store i32 %mul41, i32* %z1, align 4, !tbaa !15
  %52 = load i32, i32* %z1, align 4, !tbaa !15
  %mul42 = mul nsw i32 %52, 6967
  %53 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add = add nsw i32 %53, %mul42
  store i32 %add, i32* %tmp0, align 4, !tbaa !15
  %54 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i16, i16* %54, i32 24
  %55 = load i16, i16* %arrayidx43, align 2, !tbaa !14
  %conv44 = sext i16 %55 to i32
  %56 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %56, i32 24
  %57 = load i32, i32* %arrayidx45, align 4, !tbaa !6
  %mul46 = mul nsw i32 %conv44, %57
  store i32 %mul46, i32* %z1, align 4, !tbaa !15
  %58 = load i32, i32* %z1, align 4, !tbaa !15
  %mul47 = mul nsw i32 %58, -10426
  %59 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add48 = add nsw i32 %59, %mul47
  store i32 %add48, i32* %tmp0, align 4, !tbaa !15
  %60 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i16, i16* %60, i32 8
  %61 = load i16, i16* %arrayidx49, align 2, !tbaa !14
  %conv50 = sext i16 %61 to i32
  %62 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %62, i32 8
  %63 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %mul52 = mul nsw i32 %conv50, %63
  store i32 %mul52, i32* %z1, align 4, !tbaa !15
  %64 = load i32, i32* %z1, align 4, !tbaa !15
  %mul53 = mul nsw i32 %64, 29692
  %65 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add54 = add nsw i32 %65, %mul53
  store i32 %add54, i32* %tmp0, align 4, !tbaa !15
  %66 = load i32, i32* %tmp10, align 4, !tbaa !15
  %67 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add55 = add nsw i32 %66, %67
  %add56 = add nsw i32 %add55, 4096
  %shr = ashr i32 %add56, 13
  %68 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %68, i32 0
  store i32 %shr, i32* %arrayidx57, align 4, !tbaa !6
  %69 = load i32, i32* %tmp10, align 4, !tbaa !15
  %70 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub = sub nsw i32 %69, %70
  %add58 = add nsw i32 %sub, 4096
  %shr59 = ashr i32 %add58, 13
  %71 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %71, i32 8
  store i32 %shr59, i32* %arrayidx60, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %if.end27, %if.then21, %if.then
  %72 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %72, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %73 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr61 = getelementptr inbounds i32, i32* %73, i32 1
  store i32* %incdec.ptr61, i32** %quantptr, align 4, !tbaa !2
  %74 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr62 = getelementptr inbounds i32, i32* %74, i32 1
  store i32* %incdec.ptr62, i32** %wsptr, align 4, !tbaa !2
  %75 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %75, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay63 = getelementptr inbounds [16 x i32], [16 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay63, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc121, %for.end
  %76 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp65 = icmp slt i32 %76, 2
  br i1 %cmp65, label %for.body67, label %for.end122

for.body67:                                       ; preds = %for.cond64
  %77 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %78 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx68 = getelementptr inbounds i8*, i8** %77, i32 %78
  %79 = load i8*, i8** %arrayidx68, align 4, !tbaa !2
  %80 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr69 = getelementptr inbounds i8, i8* %79, i32 %80
  store i8* %add.ptr69, i8** %outptr, align 4, !tbaa !2
  %81 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %81, i32 1
  %82 = load i32, i32* %arrayidx70, align 4, !tbaa !6
  %cmp71 = icmp eq i32 %82, 0
  br i1 %cmp71, label %land.lhs.true73, label %if.end94

land.lhs.true73:                                  ; preds = %for.body67
  %83 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %83, i32 3
  %84 = load i32, i32* %arrayidx74, align 4, !tbaa !6
  %cmp75 = icmp eq i32 %84, 0
  br i1 %cmp75, label %land.lhs.true77, label %if.end94

land.lhs.true77:                                  ; preds = %land.lhs.true73
  %85 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %85, i32 5
  %86 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %cmp79 = icmp eq i32 %86, 0
  br i1 %cmp79, label %land.lhs.true81, label %if.end94

land.lhs.true81:                                  ; preds = %land.lhs.true77
  %87 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %87, i32 7
  %88 = load i32, i32* %arrayidx82, align 4, !tbaa !6
  %cmp83 = icmp eq i32 %88, 0
  br i1 %cmp83, label %if.then85, label %if.end94

if.then85:                                        ; preds = %land.lhs.true81
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %dcval86) #2
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %90, i32 0
  %91 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %add88 = add nsw i32 %91, 16
  %shr89 = ashr i32 %add88, 5
  %and = and i32 %shr89, 1023
  %arrayidx90 = getelementptr inbounds i8, i8* %89, i32 %and
  %92 = load i8, i8* %arrayidx90, align 1, !tbaa !17
  store i8 %92, i8* %dcval86, align 1, !tbaa !17
  %93 = load i8, i8* %dcval86, align 1, !tbaa !17
  %94 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i8, i8* %94, i32 0
  store i8 %93, i8* %arrayidx91, align 1, !tbaa !17
  %95 = load i8, i8* %dcval86, align 1, !tbaa !17
  %96 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i8, i8* %96, i32 1
  store i8 %95, i8* %arrayidx92, align 1, !tbaa !17
  %97 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr93 = getelementptr inbounds i32, i32* %97, i32 8
  store i32* %add.ptr93, i32** %wsptr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %dcval86) #2
  br label %for.inc121

if.end94:                                         ; preds = %land.lhs.true81, %land.lhs.true77, %land.lhs.true73, %for.body67
  %98 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %98, i32 0
  %99 = load i32, i32* %arrayidx95, align 4, !tbaa !6
  %shl96 = shl i32 %99, 15
  store i32 %shl96, i32* %tmp10, align 4, !tbaa !15
  %100 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %100, i32 7
  %101 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  %mul98 = mul nsw i32 %101, -5906
  %102 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %102, i32 5
  %103 = load i32, i32* %arrayidx99, align 4, !tbaa !6
  %mul100 = mul nsw i32 %103, 6967
  %add101 = add nsw i32 %mul98, %mul100
  %104 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %104, i32 3
  %105 = load i32, i32* %arrayidx102, align 4, !tbaa !6
  %mul103 = mul nsw i32 %105, -10426
  %add104 = add nsw i32 %add101, %mul103
  %106 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %106, i32 1
  %107 = load i32, i32* %arrayidx105, align 4, !tbaa !6
  %mul106 = mul nsw i32 %107, 29692
  %add107 = add nsw i32 %add104, %mul106
  store i32 %add107, i32* %tmp0, align 4, !tbaa !15
  %108 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %109 = load i32, i32* %tmp10, align 4, !tbaa !15
  %110 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add108 = add nsw i32 %109, %110
  %add109 = add nsw i32 %add108, 524288
  %shr110 = ashr i32 %add109, 20
  %and111 = and i32 %shr110, 1023
  %arrayidx112 = getelementptr inbounds i8, i8* %108, i32 %and111
  %111 = load i8, i8* %arrayidx112, align 1, !tbaa !17
  %112 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i8, i8* %112, i32 0
  store i8 %111, i8* %arrayidx113, align 1, !tbaa !17
  %113 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %114 = load i32, i32* %tmp10, align 4, !tbaa !15
  %115 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub114 = sub nsw i32 %114, %115
  %add115 = add nsw i32 %sub114, 524288
  %shr116 = ashr i32 %add115, 20
  %and117 = and i32 %shr116, 1023
  %arrayidx118 = getelementptr inbounds i8, i8* %113, i32 %and117
  %116 = load i8, i8* %arrayidx118, align 1, !tbaa !17
  %117 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i8, i8* %117, i32 1
  store i8 %116, i8* %arrayidx119, align 1, !tbaa !17
  %118 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr120 = getelementptr inbounds i32, i32* %118, i32 8
  store i32* %add.ptr120, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc121

for.inc121:                                       ; preds = %if.end94, %if.then85
  %119 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %119, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  br label %for.cond64

for.end122:                                       ; preds = %for.cond64
  %120 = bitcast [16 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %120) #2
  %121 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #2
  %122 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #2
  %123 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #2
  %124 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #2
  %125 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #2
  %126 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #2
  %127 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #2
  %128 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #2
  %129 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_1x1(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %dcval = alloca i32, align 4
  %quantptr = alloca i32*, align 4
  %range_limit = alloca i8*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 65
  %4 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %5 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %5, i32 0, i32 20
  %6 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %7 = bitcast i8* %6 to i32*
  store i32* %7, i32** %quantptr, align 4, !tbaa !2
  %8 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 0
  %9 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %9 to i32
  %10 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %10, i32 0
  %11 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %11
  store i32 %mul, i32* %dcval, align 4, !tbaa !6
  %12 = load i32, i32* %dcval, align 4, !tbaa !6
  %add = add nsw i32 %12, 4
  %shr = ashr i32 %add, 3
  store i32 %shr, i32* %dcval, align 4, !tbaa !6
  %13 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %14 = load i32, i32* %dcval, align 4, !tbaa !6
  %and = and i32 %14, 1023
  %arrayidx2 = getelementptr inbounds i8, i8* %13, i32 %and
  %15 = load i8, i8* %arrayidx2, align 1, !tbaa !17
  %16 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 0
  %17 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  %18 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i8, i8* %17, i32 %18
  store i8 %15, i8* %arrayidx4, align 1, !tbaa !17
  %19 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 324}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 80}
!13 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!14 = !{!11, !11, i64 0}
!15 = !{!16, !16, i64 0}
!16 = !{!"long", !4, i64 0}
!17 = !{!4, !4, i64 0}
