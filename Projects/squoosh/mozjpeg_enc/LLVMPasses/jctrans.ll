; ModuleID = 'jctrans.c'
source_filename = "jctrans.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.my_coef_controller = type { %struct.jpeg_c_coef_controller, i32, i32, i32, i32, %struct.jvirt_barray_control**, [10 x [64 x i16]*] }

; Function Attrs: nounwind
define hidden void @jpeg_write_coefficients(%struct.jpeg_compress_struct* %cinfo, %struct.jvirt_barray_control** %coef_arrays) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %coef_arrays.addr = alloca %struct.jvirt_barray_control**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_barray_control** %coef_arrays, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 54
  %1 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !6
  %num_scans_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %1, i32 0, i32 21
  %2 = load i32, i32* %num_scans_luma, align 4, !tbaa !11
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 54
  %4 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !6
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %4, i32 0, i32 5
  store i32 0, i32* %optimize_scans, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 5
  %6 = load i32, i32* %global_state, align 4, !tbaa !15
  %cmp2 = icmp ne i32 %6, 100
  br i1 %cmp2, label %if.then3, label %if.end7

if.then3:                                         ; preds = %if.end
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !16
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !17
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 5
  %10 = load i32, i32* %global_state4, align 4, !tbaa !15
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !16
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %10, i32* %arrayidx, align 4, !tbaa !20
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !16
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 0
  %15 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !21
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_compress_struct* %16 to %struct.jpeg_common_struct*
  call void %15(%struct.jpeg_common_struct* %17)
  br label %if.end7

if.end7:                                          ; preds = %if.then3, %if.end
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_suppress_tables(%struct.jpeg_compress_struct* %18, i32 0)
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !16
  %reset_error_mgr = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 4
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %reset_error_mgr, align 4, !tbaa !22
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_compress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23)
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 6
  %25 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !23
  %init_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %25, i32 0, i32 2
  %26 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %init_destination, align 4, !tbaa !24
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %26(%struct.jpeg_compress_struct* %27)
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %29 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  call void @transencode_master_selection(%struct.jpeg_compress_struct* %28, %struct.jvirt_barray_control** %29)
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %next_scanline = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 39
  store i32 0, i32* %next_scanline, align 8, !tbaa !26
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 5
  store i32 103, i32* %global_state9, align 4, !tbaa !15
  ret void
}

declare void @jpeg_suppress_tables(%struct.jpeg_compress_struct*, i32) #1

; Function Attrs: nounwind
define internal void @transencode_master_selection(%struct.jpeg_compress_struct* %cinfo, %struct.jvirt_barray_control** %coef_arrays) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %coef_arrays.addr = alloca %struct.jvirt_barray_control**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_barray_control** %coef_arrays, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 9
  store i32 1, i32* %input_components, align 4, !tbaa !27
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_c_master_control(%struct.jpeg_compress_struct* %1, i32 1)
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 25
  %3 = load i32, i32* %arith_code, align 4, !tbaa !28
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !16
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 1, i32* %msg_code, align 4, !tbaa !17
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !16
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 0
  %8 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !21
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_compress_struct* %9 to %struct.jpeg_common_struct*
  call void %8(%struct.jpeg_common_struct* %10)
  br label %if.end5

if.else:                                          ; preds = %entry
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 40
  %12 = load i32, i32* %progressive_mode, align 4, !tbaa !29
  %tobool2 = icmp ne i32 %12, 0
  br i1 %tobool2, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_phuff_encoder(%struct.jpeg_compress_struct* %13)
  br label %if.end

if.else4:                                         ; preds = %if.else
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_huff_encoder(%struct.jpeg_compress_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  call void @transencode_coef_controller(%struct.jpeg_compress_struct* %15, %struct.jvirt_barray_control** %16)
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_marker_writer(%struct.jpeg_compress_struct* %17)
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 1
  %19 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !30
  %realize_virt_arrays = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %19, i32 0, i32 6
  %realize_virt_arrays6 = bitcast {}** %realize_virt_arrays to void (%struct.jpeg_common_struct*)**
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %realize_virt_arrays6, align 4, !tbaa !31
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_compress_struct* %21 to %struct.jpeg_common_struct*
  call void %20(%struct.jpeg_common_struct* %22)
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 58
  %24 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !33
  %write_file_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_file_header, align 4, !tbaa !34
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %25(%struct.jpeg_compress_struct* %26)
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_copy_critical_parameters(%struct.jpeg_decompress_struct* %srcinfo, %struct.jpeg_compress_struct* %dstinfo) #0 {
entry:
  %srcinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %dstinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %qtblptr = alloca %struct.JQUANT_TBL**, align 4
  %incomp = alloca %struct.jpeg_component_info*, align 4
  %outcomp = alloca %struct.jpeg_component_info*, align 4
  %c_quant = alloca %struct.JQUANT_TBL*, align 4
  %slot_quant = alloca %struct.JQUANT_TBL*, align 4
  %tblno = alloca i32, align 4
  %ci = alloca i32, align 4
  %coefi = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %srcinfo, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_compress_struct* %dstinfo, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.JQUANT_TBL*** %qtblptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.jpeg_component_info** %incomp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.jpeg_component_info** %outcomp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.JQUANT_TBL** %c_quant to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast %struct.JQUANT_TBL** %slot_quant to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 5
  %9 = load i32, i32* %global_state, align 4, !tbaa !15
  %cmp = icmp ne i32 %9, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !16
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !17
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 5
  %13 = load i32, i32* %global_state1, align 4, !tbaa !15
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !16
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %13, i32* %arrayidx, align 4, !tbaa !20
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !16
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 0
  %18 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !21
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_compress_struct* %19 to %struct.jpeg_common_struct*
  call void %18(%struct.jpeg_common_struct* %20)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 7
  %22 = load i32, i32* %image_width, align 4, !tbaa !36
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %image_width4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 7
  store i32 %22, i32* %image_width4, align 4, !tbaa !38
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 8
  %25 = load i32, i32* %image_height, align 8, !tbaa !39
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %image_height5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 8
  store i32 %25, i32* %image_height5, align 8, !tbaa !40
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 9
  %28 = load i32, i32* %num_components, align 4, !tbaa !41
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 9
  store i32 %28, i32* %input_components, align 4, !tbaa !27
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 10
  %31 = load i32, i32* %jpeg_color_space, align 8, !tbaa !42
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 10
  store i32 %31, i32* %in_color_space, align 8, !tbaa !43
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  call void @jpeg_set_defaults(%struct.jpeg_compress_struct* %33)
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 54
  %35 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !6
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %35, i32 0, i32 6
  store i32 0, i32* %trellis_quant, align 8, !tbaa !44
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %jpeg_color_space6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 10
  %38 = load i32, i32* %jpeg_color_space6, align 8, !tbaa !42
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %36, i32 %38)
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 43
  %40 = load i32, i32* %data_precision, align 4, !tbaa !45
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %data_precision7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 12
  store i32 %40, i32* %data_precision7, align 8, !tbaa !46
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 59
  %43 = load i32, i32* %CCIR601_sampling, align 4, !tbaa !47
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 27
  store i32 %43, i32* %CCIR601_sampling8, align 4, !tbaa !48
  store i32 0, i32* %tblno, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %45 = load i32, i32* %tblno, align 4, !tbaa !49
  %cmp9 = icmp slt i32 %45, 4
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 40
  %47 = load i32, i32* %tblno, align 4, !tbaa !49
  %arrayidx10 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %47
  %48 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx10, align 4, !tbaa !2
  %cmp11 = icmp ne %struct.JQUANT_TBL* %48, null
  br i1 %cmp11, label %if.then12, label %if.end22

if.then12:                                        ; preds = %for.body
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 16
  %50 = load i32, i32* %tblno, align 4, !tbaa !49
  %arrayidx14 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs13, i32 0, i32 %50
  store %struct.JQUANT_TBL** %arrayidx14, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %51 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %52 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %51, align 4, !tbaa !2
  %cmp15 = icmp eq %struct.JQUANT_TBL* %52, null
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.then12
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %54 = bitcast %struct.jpeg_compress_struct* %53 to %struct.jpeg_common_struct*
  %call = call %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct* %54)
  %55 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %call, %struct.JQUANT_TBL** %55, align 4, !tbaa !2
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.then12
  %56 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %57 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %56, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %57, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 0
  %58 = bitcast i16* %arraydecay to i8*
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 40
  %60 = load i32, i32* %tblno, align 4, !tbaa !49
  %arrayidx19 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs18, i32 0, i32 %60
  %61 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx19, align 4, !tbaa !2
  %quantval20 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %61, i32 0, i32 0
  %arraydecay21 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval20, i32 0, i32 0
  %62 = bitcast i16* %arraydecay21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %62, i32 128, i1 false)
  %63 = load %struct.JQUANT_TBL**, %struct.JQUANT_TBL*** %qtblptr, align 4, !tbaa !2
  %64 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %63, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %64, i32 0, i32 1
  store i32 0, i32* %sent_table, align 4, !tbaa !50
  br label %if.end22

if.end22:                                         ; preds = %if.end17, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %65 = load i32, i32* %tblno, align 4, !tbaa !49
  %inc = add nsw i32 %65, 1
  store i32 %inc, i32* %tblno, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %num_components23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 9
  %67 = load i32, i32* %num_components23, align 4, !tbaa !41
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %num_components24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 13
  store i32 %67, i32* %num_components24, align 4, !tbaa !52
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %num_components25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 13
  %70 = load i32, i32* %num_components25, align 4, !tbaa !52
  %cmp26 = icmp slt i32 %70, 1
  br i1 %cmp26, label %if.then29, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %num_components27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 13
  %72 = load i32, i32* %num_components27, align 4, !tbaa !52
  %cmp28 = icmp sgt i32 %72, 10
  br i1 %cmp28, label %if.then29, label %if.end43

if.then29:                                        ; preds = %lor.lhs.false, %for.end
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %73, i32 0, i32 0
  %74 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err30, align 8, !tbaa !16
  %msg_code31 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %74, i32 0, i32 5
  store i32 26, i32* %msg_code31, align 4, !tbaa !17
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %num_components32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 13
  %76 = load i32, i32* %num_components32, align 4, !tbaa !52
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 0
  %78 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err33, align 8, !tbaa !16
  %msg_parm34 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %78, i32 0, i32 6
  %i35 = bitcast %union.anon* %msg_parm34 to [8 x i32]*
  %arrayidx36 = getelementptr inbounds [8 x i32], [8 x i32]* %i35, i32 0, i32 0
  store i32 %76, i32* %arrayidx36, align 4, !tbaa !20
  %79 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %79, i32 0, i32 0
  %80 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err37, align 8, !tbaa !16
  %msg_parm38 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %80, i32 0, i32 6
  %i39 = bitcast %union.anon* %msg_parm38 to [8 x i32]*
  %arrayidx40 = getelementptr inbounds [8 x i32], [8 x i32]* %i39, i32 0, i32 1
  store i32 10, i32* %arrayidx40, align 4, !tbaa !20
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 0
  %82 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 8, !tbaa !16
  %error_exit42 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %82, i32 0, i32 0
  %83 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit42, align 4, !tbaa !21
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %85 = bitcast %struct.jpeg_compress_struct* %84 to %struct.jpeg_common_struct*
  call void %83(%struct.jpeg_common_struct* %85)
  br label %if.end43

if.end43:                                         ; preds = %if.then29, %lor.lhs.false
  store i32 0, i32* %ci, align 4, !tbaa !49
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 44
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !53
  store %struct.jpeg_component_info* %87, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %88 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %comp_info44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %88, i32 0, i32 15
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info44, align 4, !tbaa !54
  store %struct.jpeg_component_info* %89, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc99, %if.end43
  %90 = load i32, i32* %ci, align 4, !tbaa !49
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %num_components46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 13
  %92 = load i32, i32* %num_components46, align 4, !tbaa !52
  %cmp47 = icmp slt i32 %90, %92
  br i1 %cmp47, label %for.body48, label %for.end102

for.body48:                                       ; preds = %for.cond45
  %93 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %93, i32 0, i32 0
  %94 = load i32, i32* %component_id, align 4, !tbaa !55
  %95 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %component_id49 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %95, i32 0, i32 0
  store i32 %94, i32* %component_id49, align 4, !tbaa !55
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 2
  %97 = load i32, i32* %h_samp_factor, align 4, !tbaa !57
  %98 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %h_samp_factor50 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %98, i32 0, i32 2
  store i32 %97, i32* %h_samp_factor50, align 4, !tbaa !57
  %99 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %99, i32 0, i32 3
  %100 = load i32, i32* %v_samp_factor, align 4, !tbaa !58
  %101 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %v_samp_factor51 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %101, i32 0, i32 3
  store i32 %100, i32* %v_samp_factor51, align 4, !tbaa !58
  %102 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %102, i32 0, i32 4
  %103 = load i32, i32* %quant_tbl_no, align 4, !tbaa !59
  %104 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %quant_tbl_no52 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %104, i32 0, i32 4
  store i32 %103, i32* %quant_tbl_no52, align 4, !tbaa !59
  %105 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %quant_tbl_no53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %105, i32 0, i32 4
  %106 = load i32, i32* %quant_tbl_no53, align 4, !tbaa !59
  store i32 %106, i32* %tblno, align 4, !tbaa !49
  %107 = load i32, i32* %tblno, align 4, !tbaa !49
  %cmp54 = icmp slt i32 %107, 0
  br i1 %cmp54, label %if.then61, label %lor.lhs.false55

lor.lhs.false55:                                  ; preds = %for.body48
  %108 = load i32, i32* %tblno, align 4, !tbaa !49
  %cmp56 = icmp sge i32 %108, 4
  br i1 %cmp56, label %if.then61, label %lor.lhs.false57

lor.lhs.false57:                                  ; preds = %lor.lhs.false55
  %109 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %109, i32 0, i32 40
  %110 = load i32, i32* %tblno, align 4, !tbaa !49
  %arrayidx59 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs58, i32 0, i32 %110
  %111 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx59, align 4, !tbaa !2
  %cmp60 = icmp eq %struct.JQUANT_TBL* %111, null
  br i1 %cmp60, label %if.then61, label %if.end70

if.then61:                                        ; preds = %lor.lhs.false57, %lor.lhs.false55, %for.body48
  %112 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err62 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %112, i32 0, i32 0
  %113 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err62, align 8, !tbaa !16
  %msg_code63 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %113, i32 0, i32 5
  store i32 52, i32* %msg_code63, align 4, !tbaa !17
  %114 = load i32, i32* %tblno, align 4, !tbaa !49
  %115 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err64 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %115, i32 0, i32 0
  %116 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err64, align 8, !tbaa !16
  %msg_parm65 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %116, i32 0, i32 6
  %i66 = bitcast %union.anon* %msg_parm65 to [8 x i32]*
  %arrayidx67 = getelementptr inbounds [8 x i32], [8 x i32]* %i66, i32 0, i32 0
  store i32 %114, i32* %arrayidx67, align 4, !tbaa !20
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err68 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 0
  %118 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err68, align 8, !tbaa !16
  %error_exit69 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %118, i32 0, i32 0
  %119 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit69, align 4, !tbaa !21
  %120 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %121 = bitcast %struct.jpeg_compress_struct* %120 to %struct.jpeg_common_struct*
  call void %119(%struct.jpeg_common_struct* %121)
  br label %if.end70

if.end70:                                         ; preds = %if.then61, %lor.lhs.false57
  %122 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %122, i32 0, i32 40
  %123 = load i32, i32* %tblno, align 4, !tbaa !49
  %arrayidx72 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs71, i32 0, i32 %123
  %124 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx72, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %124, %struct.JQUANT_TBL** %slot_quant, align 4, !tbaa !2
  %125 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %125, i32 0, i32 19
  %126 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !60
  store %struct.JQUANT_TBL* %126, %struct.JQUANT_TBL** %c_quant, align 4, !tbaa !2
  %127 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %c_quant, align 4, !tbaa !2
  %cmp73 = icmp ne %struct.JQUANT_TBL* %127, null
  br i1 %cmp73, label %if.then74, label %if.end98

if.then74:                                        ; preds = %if.end70
  store i32 0, i32* %coefi, align 4, !tbaa !49
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc95, %if.then74
  %128 = load i32, i32* %coefi, align 4, !tbaa !49
  %cmp76 = icmp slt i32 %128, 64
  br i1 %cmp76, label %for.body77, label %for.end97

for.body77:                                       ; preds = %for.cond75
  %129 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %c_quant, align 4, !tbaa !2
  %quantval78 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %129, i32 0, i32 0
  %130 = load i32, i32* %coefi, align 4, !tbaa !49
  %arrayidx79 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval78, i32 0, i32 %130
  %131 = load i16, i16* %arrayidx79, align 2, !tbaa !61
  %conv = zext i16 %131 to i32
  %132 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %slot_quant, align 4, !tbaa !2
  %quantval80 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %132, i32 0, i32 0
  %133 = load i32, i32* %coefi, align 4, !tbaa !49
  %arrayidx81 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval80, i32 0, i32 %133
  %134 = load i16, i16* %arrayidx81, align 2, !tbaa !61
  %conv82 = zext i16 %134 to i32
  %cmp83 = icmp ne i32 %conv, %conv82
  br i1 %cmp83, label %if.then85, label %if.end94

if.then85:                                        ; preds = %for.body77
  %135 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err86 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %135, i32 0, i32 0
  %136 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err86, align 8, !tbaa !16
  %msg_code87 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %136, i32 0, i32 5
  store i32 44, i32* %msg_code87, align 4, !tbaa !17
  %137 = load i32, i32* %tblno, align 4, !tbaa !49
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err88 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 0
  %139 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err88, align 8, !tbaa !16
  %msg_parm89 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %139, i32 0, i32 6
  %i90 = bitcast %union.anon* %msg_parm89 to [8 x i32]*
  %arrayidx91 = getelementptr inbounds [8 x i32], [8 x i32]* %i90, i32 0, i32 0
  store i32 %137, i32* %arrayidx91, align 4, !tbaa !20
  %140 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %err92 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %140, i32 0, i32 0
  %141 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err92, align 8, !tbaa !16
  %error_exit93 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %141, i32 0, i32 0
  %142 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit93, align 4, !tbaa !21
  %143 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %144 = bitcast %struct.jpeg_compress_struct* %143 to %struct.jpeg_common_struct*
  call void %142(%struct.jpeg_common_struct* %144)
  br label %if.end94

if.end94:                                         ; preds = %if.then85, %for.body77
  br label %for.inc95

for.inc95:                                        ; preds = %if.end94
  %145 = load i32, i32* %coefi, align 4, !tbaa !49
  %inc96 = add nsw i32 %145, 1
  store i32 %inc96, i32* %coefi, align 4, !tbaa !49
  br label %for.cond75

for.end97:                                        ; preds = %for.cond75
  br label %if.end98

if.end98:                                         ; preds = %for.end97, %if.end70
  br label %for.inc99

for.inc99:                                        ; preds = %if.end98
  %146 = load i32, i32* %ci, align 4, !tbaa !49
  %inc100 = add nsw i32 %146, 1
  store i32 %inc100, i32* %ci, align 4, !tbaa !49
  %147 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %147, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %incomp, align 4, !tbaa !2
  %148 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  %incdec.ptr101 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %148, i32 1
  store %struct.jpeg_component_info* %incdec.ptr101, %struct.jpeg_component_info** %outcomp, align 4, !tbaa !2
  br label %for.cond45

for.end102:                                       ; preds = %for.cond45
  %149 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %saw_JFIF_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %149, i32 0, i32 51
  %150 = load i32, i32* %saw_JFIF_marker, align 8, !tbaa !62
  %tobool = icmp ne i32 %150, 0
  br i1 %tobool, label %if.then103, label %if.end115

if.then103:                                       ; preds = %for.end102
  %151 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %JFIF_major_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %151, i32 0, i32 52
  %152 = load i8, i8* %JFIF_major_version, align 4, !tbaa !63
  %conv104 = zext i8 %152 to i32
  %cmp105 = icmp eq i32 %conv104, 1
  br i1 %cmp105, label %if.then107, label %if.end111

if.then107:                                       ; preds = %if.then103
  %153 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %JFIF_major_version108 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %153, i32 0, i32 52
  %154 = load i8, i8* %JFIF_major_version108, align 4, !tbaa !63
  %155 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %JFIF_major_version109 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %155, i32 0, i32 33
  store i8 %154, i8* %JFIF_major_version109, align 4, !tbaa !64
  %156 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %156, i32 0, i32 53
  %157 = load i8, i8* %JFIF_minor_version, align 1, !tbaa !65
  %158 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version110 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %158, i32 0, i32 34
  store i8 %157, i8* %JFIF_minor_version110, align 1, !tbaa !66
  br label %if.end111

if.end111:                                        ; preds = %if.then107, %if.then103
  %159 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %density_unit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %159, i32 0, i32 54
  %160 = load i8, i8* %density_unit, align 2, !tbaa !67
  %161 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %density_unit112 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %161, i32 0, i32 35
  store i8 %160, i8* %density_unit112, align 2, !tbaa !68
  %162 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %X_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %162, i32 0, i32 55
  %163 = load i16, i16* %X_density, align 8, !tbaa !69
  %164 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %X_density113 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %164, i32 0, i32 36
  store i16 %163, i16* %X_density113, align 8, !tbaa !70
  %165 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %srcinfo.addr, align 4, !tbaa !2
  %Y_density = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %165, i32 0, i32 56
  %166 = load i16, i16* %Y_density, align 2, !tbaa !71
  %167 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %dstinfo.addr, align 4, !tbaa !2
  %Y_density114 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %167, i32 0, i32 37
  store i16 %166, i16* %Y_density114, align 2, !tbaa !72
  br label %if.end115

if.end115:                                        ; preds = %if.end111, %for.end102
  %168 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  %170 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #3
  %171 = bitcast %struct.JQUANT_TBL** %slot_quant to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast %struct.JQUANT_TBL** %c_quant to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  %173 = bitcast %struct.jpeg_component_info** %outcomp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #3
  %174 = bitcast %struct.jpeg_component_info** %incomp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #3
  %175 = bitcast %struct.JQUANT_TBL*** %qtblptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare void @jpeg_set_defaults(%struct.jpeg_compress_struct*) #1

declare void @jpeg_set_colorspace(%struct.jpeg_compress_struct*, i32) #1

declare %struct.JQUANT_TBL* @jpeg_alloc_quant_table(%struct.jpeg_common_struct*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

declare void @jinit_c_master_control(%struct.jpeg_compress_struct*, i32) #1

declare void @jinit_phuff_encoder(%struct.jpeg_compress_struct*) #1

declare void @jinit_huff_encoder(%struct.jpeg_compress_struct*) #1

; Function Attrs: nounwind
define internal void @transencode_coef_controller(%struct.jpeg_compress_struct* %cinfo, %struct.jvirt_barray_control** %coef_arrays) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %coef_arrays.addr = alloca %struct.jvirt_barray_control**, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %buffer = alloca [64 x i16]*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_barray_control** %coef_arrays, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !30
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 0
  %5 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !73
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_compress_struct* %6 to %struct.jpeg_common_struct*
  %call = call i8* %5(%struct.jpeg_common_struct* %7, i32 1, i32 68)
  %8 = bitcast i8* %call to %struct.my_coef_controller*
  store %struct.my_coef_controller* %8, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %9 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %10 = bitcast %struct.my_coef_controller* %9 to %struct.jpeg_c_coef_controller*
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 57
  store %struct.jpeg_c_coef_controller* %10, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !74
  %12 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %12, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub, i32 0, i32 0
  store void (%struct.jpeg_compress_struct*, i32)* @start_pass_coef, void (%struct.jpeg_compress_struct*, i32)** %start_pass, align 4, !tbaa !75
  %13 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %13, i32 0, i32 0
  %compress_data = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub2, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, i8***)* @compress_output, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data, align 4, !tbaa !78
  %14 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %coef_arrays.addr, align 4, !tbaa !2
  %15 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %15, i32 0, i32 5
  store %struct.jvirt_barray_control** %14, %struct.jvirt_barray_control*** %whole_image, align 4, !tbaa !79
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 1
  %17 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem3, align 4, !tbaa !30
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %17, i32 0, i32 1
  %18 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !80
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_compress_struct* %19 to %struct.jpeg_common_struct*
  %call4 = call i8* %18(%struct.jpeg_common_struct* %20, i32 1, i32 1280)
  %21 = bitcast i8* %call4 to [64 x i16]*
  store [64 x i16]* %21, [64 x i16]** %buffer, align 4, !tbaa !2
  %22 = load [64 x i16]*, [64 x i16]** %buffer, align 4, !tbaa !2
  %23 = bitcast [64 x i16]* %22 to i8*
  call void @jzero_far(i8* %23, i32 1280)
  store i32 0, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %24 = load i32, i32* %i, align 4, !tbaa !49
  %cmp = icmp slt i32 %24, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load [64 x i16]*, [64 x i16]** %buffer, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !49
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %25, i32 %26
  %27 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %dummy_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %27, i32 0, i32 6
  %28 = load i32, i32* %i, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %dummy_buffer, i32 0, i32 %28
  store [64 x i16]* %add.ptr, [64 x i16]** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !49
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  ret void
}

declare void @jinit_marker_writer(%struct.jpeg_compress_struct*) #1

; Function Attrs: nounwind
define internal void @start_pass_coef(%struct.jpeg_compress_struct* %cinfo, i32 %pass_mode) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %pass_mode.addr = alloca i32, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pass_mode, i32* %pass_mode.addr, align 4, !tbaa !20
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !74
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load i32, i32* %pass_mode.addr, align 4, !tbaa !20
  %cmp = icmp ne i32 %4, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !16
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !17
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !16
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 0
  %9 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !21
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_compress_struct* %10 to %struct.jpeg_common_struct*
  call void %9(%struct.jpeg_common_struct* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %12, i32 0, i32 1
  store i32 0, i32* %iMCU_row_num, align 4, !tbaa !81
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_compress_struct* %13)
  %14 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @compress_output(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %MCU_col_num = alloca i32, align 4
  %last_MCU_col = alloca i32, align 4
  %last_iMCU_row = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %xindex = alloca i32, align 4
  %yindex = alloca i32, align 4
  %yoffset = alloca i32, align 4
  %blockcnt = alloca i32, align 4
  %start_col = alloca i32, align 4
  %buffer = alloca [4 x [64 x i16]**], align 16
  %MCU_buffer = alloca [10 x [64 x i16]*], align 16
  %buffer_ptr = alloca [64 x i16]*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !74
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 46
  %7 = load i32, i32* %MCUs_per_row, align 8, !tbaa !82
  %sub = sub i32 %7, 1
  store i32 %sub, i32* %last_MCU_col, align 4, !tbaa !49
  %8 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 43
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !83
  %sub2 = sub i32 %10, 1
  store i32 %sub2, i32* %last_iMCU_row, align 4, !tbaa !49
  %11 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %blockcnt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #3
  %19 = bitcast [10 x [64 x i16]*]* %MCU_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %19) #3
  %20 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  store i32 0, i32* %ci, align 4, !tbaa !49
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %22 = load i32, i32* %ci, align 4, !tbaa !49
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 44
  %24 = load i32, i32* %comps_in_scan, align 4, !tbaa !84
  %cmp = icmp slt i32 %22, %24
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 45
  %26 = load i32, i32* %ci, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %26
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %27, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 1
  %29 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !30
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %29, i32 0, i32 8
  %30 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !85
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = bitcast %struct.jpeg_compress_struct* %31 to %struct.jpeg_common_struct*
  %33 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %33, i32 0, i32 5
  %34 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %whole_image, align 4, !tbaa !79
  %35 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %35, i32 0, i32 1
  %36 = load i32, i32* %component_index, align 4, !tbaa !86
  %arrayidx3 = getelementptr inbounds %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %34, i32 %36
  %37 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx3, align 4, !tbaa !2
  %38 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %38, i32 0, i32 1
  %39 = load i32, i32* %iMCU_row_num, align 4, !tbaa !81
  %40 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %40, i32 0, i32 3
  %41 = load i32, i32* %v_samp_factor, align 4, !tbaa !58
  %mul = mul i32 %39, %41
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor4 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %42, i32 0, i32 3
  %43 = load i32, i32* %v_samp_factor4, align 4, !tbaa !58
  %call = call [64 x i16]** %30(%struct.jpeg_common_struct* %32, %struct.jvirt_barray_control* %37, i32 %mul, i32 %43, i32 0)
  %44 = load i32, i32* %ci, align 4, !tbaa !49
  %arrayidx5 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %44
  store [64 x i16]** %call, [64 x i16]*** %arrayidx5, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %45 = load i32, i32* %ci, align 4, !tbaa !49
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !49
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %46 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %46, i32 0, i32 3
  %47 = load i32, i32* %MCU_vert_offset, align 4, !tbaa !87
  store i32 %47, i32* %yoffset, align 4, !tbaa !49
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc71, %for.end
  %48 = load i32, i32* %yoffset, align 4, !tbaa !49
  %49 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %49, i32 0, i32 4
  %50 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !88
  %cmp7 = icmp slt i32 %48, %50
  br i1 %cmp7, label %for.body8, label %for.end73

for.body8:                                        ; preds = %for.cond6
  %51 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %51, i32 0, i32 2
  %52 = load i32, i32* %mcu_ctr, align 4, !tbaa !89
  store i32 %52, i32* %MCU_col_num, align 4, !tbaa !49
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc67, %for.body8
  %53 = load i32, i32* %MCU_col_num, align 4, !tbaa !49
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 46
  %55 = load i32, i32* %MCUs_per_row10, align 8, !tbaa !82
  %cmp11 = icmp ult i32 %53, %55
  br i1 %cmp11, label %for.body12, label %for.end69

for.body12:                                       ; preds = %for.cond9
  store i32 0, i32* %blkn, align 4, !tbaa !49
  store i32 0, i32* %ci, align 4, !tbaa !49
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc59, %for.body12
  %56 = load i32, i32* %ci, align 4, !tbaa !49
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 44
  %58 = load i32, i32* %comps_in_scan14, align 4, !tbaa !84
  %cmp15 = icmp slt i32 %56, %58
  br i1 %cmp15, label %for.body16, label %for.end61

for.body16:                                       ; preds = %for.cond13
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 45
  %60 = load i32, i32* %ci, align 4, !tbaa !49
  %arrayidx18 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info17, i32 0, i32 %60
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx18, align 4, !tbaa !2
  store %struct.jpeg_component_info* %61, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %62 = load i32, i32* %MCU_col_num, align 4, !tbaa !49
  %63 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %63, i32 0, i32 13
  %64 = load i32, i32* %MCU_width, align 4, !tbaa !90
  %mul19 = mul i32 %62, %64
  store i32 %mul19, i32* %start_col, align 4, !tbaa !49
  %65 = load i32, i32* %MCU_col_num, align 4, !tbaa !49
  %66 = load i32, i32* %last_MCU_col, align 4, !tbaa !49
  %cmp20 = icmp ult i32 %65, %66
  br i1 %cmp20, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body16
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 0, i32 13
  %68 = load i32, i32* %MCU_width21, align 4, !tbaa !90
  br label %cond.end

cond.false:                                       ; preds = %for.body16
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 17
  %70 = load i32, i32* %last_col_width, align 4, !tbaa !91
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %68, %cond.true ], [ %70, %cond.false ]
  store i32 %cond, i32* %blockcnt, align 4, !tbaa !49
  store i32 0, i32* %yindex, align 4, !tbaa !49
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc56, %cond.end
  %71 = load i32, i32* %yindex, align 4, !tbaa !49
  %72 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %72, i32 0, i32 14
  %73 = load i32, i32* %MCU_height, align 4, !tbaa !92
  %cmp23 = icmp slt i32 %71, %73
  br i1 %cmp23, label %for.body24, label %for.end58

for.body24:                                       ; preds = %for.cond22
  %74 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num25 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %74, i32 0, i32 1
  %75 = load i32, i32* %iMCU_row_num25, align 4, !tbaa !81
  %76 = load i32, i32* %last_iMCU_row, align 4, !tbaa !49
  %cmp26 = icmp ult i32 %75, %76
  br i1 %cmp26, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body24
  %77 = load i32, i32* %yindex, align 4, !tbaa !49
  %78 = load i32, i32* %yoffset, align 4, !tbaa !49
  %add = add nsw i32 %77, %78
  %79 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %79, i32 0, i32 18
  %80 = load i32, i32* %last_row_height, align 4, !tbaa !93
  %cmp27 = icmp slt i32 %add, %80
  br i1 %cmp27, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %for.body24
  %81 = load i32, i32* %ci, align 4, !tbaa !49
  %arrayidx28 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %81
  %82 = load [64 x i16]**, [64 x i16]*** %arrayidx28, align 4, !tbaa !2
  %83 = load i32, i32* %yindex, align 4, !tbaa !49
  %84 = load i32, i32* %yoffset, align 4, !tbaa !49
  %add29 = add nsw i32 %83, %84
  %arrayidx30 = getelementptr inbounds [64 x i16]*, [64 x i16]** %82, i32 %add29
  %85 = load [64 x i16]*, [64 x i16]** %arrayidx30, align 4, !tbaa !2
  %86 = load i32, i32* %start_col, align 4, !tbaa !49
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %85, i32 %86
  store [64 x i16]* %add.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store i32 0, i32* %xindex, align 4, !tbaa !49
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc36, %if.then
  %87 = load i32, i32* %xindex, align 4, !tbaa !49
  %88 = load i32, i32* %blockcnt, align 4, !tbaa !49
  %cmp32 = icmp slt i32 %87, %88
  br i1 %cmp32, label %for.body33, label %for.end38

for.body33:                                       ; preds = %for.cond31
  %89 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %89, i32 1
  store [64 x i16]* %incdec.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %90 = load i32, i32* %blkn, align 4, !tbaa !49
  %inc34 = add nsw i32 %90, 1
  store i32 %inc34, i32* %blkn, align 4, !tbaa !49
  %arrayidx35 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %90
  store [64 x i16]* %89, [64 x i16]** %arrayidx35, align 4, !tbaa !2
  br label %for.inc36

for.inc36:                                        ; preds = %for.body33
  %91 = load i32, i32* %xindex, align 4, !tbaa !49
  %inc37 = add nsw i32 %91, 1
  store i32 %inc37, i32* %xindex, align 4, !tbaa !49
  br label %for.cond31

for.end38:                                        ; preds = %for.cond31
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  store i32 0, i32* %xindex, align 4, !tbaa !49
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end38
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc53, %if.end
  %92 = load i32, i32* %xindex, align 4, !tbaa !49
  %93 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %93, i32 0, i32 13
  %94 = load i32, i32* %MCU_width40, align 4, !tbaa !90
  %cmp41 = icmp slt i32 %92, %94
  br i1 %cmp41, label %for.body42, label %for.end55

for.body42:                                       ; preds = %for.cond39
  %95 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %dummy_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %95, i32 0, i32 6
  %96 = load i32, i32* %blkn, align 4, !tbaa !49
  %arrayidx43 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %dummy_buffer, i32 0, i32 %96
  %97 = load [64 x i16]*, [64 x i16]** %arrayidx43, align 4, !tbaa !2
  %98 = load i32, i32* %blkn, align 4, !tbaa !49
  %arrayidx44 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %98
  store [64 x i16]* %97, [64 x i16]** %arrayidx44, align 4, !tbaa !2
  %99 = load i32, i32* %blkn, align 4, !tbaa !49
  %sub45 = sub nsw i32 %99, 1
  %arrayidx46 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %sub45
  %100 = load [64 x i16]*, [64 x i16]** %arrayidx46, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds [64 x i16], [64 x i16]* %100, i32 0
  %arrayidx48 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx47, i32 0, i32 0
  %101 = load i16, i16* %arrayidx48, align 2, !tbaa !61
  %102 = load i32, i32* %blkn, align 4, !tbaa !49
  %arrayidx49 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %102
  %103 = load [64 x i16]*, [64 x i16]** %arrayidx49, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds [64 x i16], [64 x i16]* %103, i32 0
  %arrayidx51 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx50, i32 0, i32 0
  store i16 %101, i16* %arrayidx51, align 2, !tbaa !61
  %104 = load i32, i32* %blkn, align 4, !tbaa !49
  %inc52 = add nsw i32 %104, 1
  store i32 %inc52, i32* %blkn, align 4, !tbaa !49
  br label %for.inc53

for.inc53:                                        ; preds = %for.body42
  %105 = load i32, i32* %xindex, align 4, !tbaa !49
  %inc54 = add nsw i32 %105, 1
  store i32 %inc54, i32* %xindex, align 4, !tbaa !49
  br label %for.cond39

for.end55:                                        ; preds = %for.cond39
  br label %for.inc56

for.inc56:                                        ; preds = %for.end55
  %106 = load i32, i32* %yindex, align 4, !tbaa !49
  %inc57 = add nsw i32 %106, 1
  store i32 %inc57, i32* %yindex, align 4, !tbaa !49
  br label %for.cond22

for.end58:                                        ; preds = %for.cond22
  br label %for.inc59

for.inc59:                                        ; preds = %for.end58
  %107 = load i32, i32* %ci, align 4, !tbaa !49
  %inc60 = add nsw i32 %107, 1
  store i32 %inc60, i32* %ci, align 4, !tbaa !49
  br label %for.cond13

for.end61:                                        ; preds = %for.cond13
  %108 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %108, i32 0, i32 62
  %109 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy, align 4, !tbaa !94
  %encode_mcu = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %109, i32 0, i32 1
  %110 = load i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu, align 4, !tbaa !95
  %111 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 0
  %call62 = call i32 %110(%struct.jpeg_compress_struct* %111, [64 x i16]** %arraydecay)
  %tobool = icmp ne i32 %call62, 0
  br i1 %tobool, label %if.end66, label %if.then63

if.then63:                                        ; preds = %for.end61
  %112 = load i32, i32* %yoffset, align 4, !tbaa !49
  %113 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset64 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %113, i32 0, i32 3
  store i32 %112, i32* %MCU_vert_offset64, align 4, !tbaa !87
  %114 = load i32, i32* %MCU_col_num, align 4, !tbaa !49
  %115 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr65 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %115, i32 0, i32 2
  store i32 %114, i32* %mcu_ctr65, align 4, !tbaa !89
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end66:                                         ; preds = %for.end61
  br label %for.inc67

for.inc67:                                        ; preds = %if.end66
  %116 = load i32, i32* %MCU_col_num, align 4, !tbaa !49
  %inc68 = add i32 %116, 1
  store i32 %inc68, i32* %MCU_col_num, align 4, !tbaa !49
  br label %for.cond9

for.end69:                                        ; preds = %for.cond9
  %117 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr70 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %117, i32 0, i32 2
  store i32 0, i32* %mcu_ctr70, align 4, !tbaa !89
  br label %for.inc71

for.inc71:                                        ; preds = %for.end69
  %118 = load i32, i32* %yoffset, align 4, !tbaa !49
  %inc72 = add nsw i32 %118, 1
  store i32 %inc72, i32* %yoffset, align 4, !tbaa !49
  br label %for.cond6

for.end73:                                        ; preds = %for.cond6
  %119 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num74 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %119, i32 0, i32 1
  %120 = load i32, i32* %iMCU_row_num74, align 4, !tbaa !81
  %inc75 = add i32 %120, 1
  store i32 %inc75, i32* %iMCU_row_num74, align 4, !tbaa !81
  %121 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_compress_struct* %121)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end73, %if.then63
  %122 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast [10 x [64 x i16]*]* %MCU_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %124) #3
  %125 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %125) #3
  %126 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %blockcnt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %129 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #3
  %130 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #3
  %131 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #3
  %132 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #3
  %133 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #3
  %134 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #3
  %137 = load i32, i32* %retval, align 4
  ret i32 %137
}

declare void @jzero_far(i8*, i32) #1

; Function Attrs: nounwind
define internal void @start_iMCU_row(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !74
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 44
  %5 = load i32, i32* %comps_in_scan, align 4, !tbaa !84
  %cmp = icmp sgt i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %6, i32 0, i32 4
  store i32 1, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !88
  br label %if.end9

if.else:                                          ; preds = %entry
  %7 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %7, i32 0, i32 1
  %8 = load i32, i32* %iMCU_row_num, align 4, !tbaa !81
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 43
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !83
  %sub = sub i32 %10, 1
  %cmp2 = icmp ult i32 %8, %sub
  br i1 %cmp2, label %if.then3, label %if.else5

if.then3:                                         ; preds = %if.else
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 45
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 8, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 3
  %13 = load i32, i32* %v_samp_factor, align 4, !tbaa !58
  %14 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %14, i32 0, i32 4
  store i32 %13, i32* %MCU_rows_per_iMCU_row4, align 4, !tbaa !88
  br label %if.end

if.else5:                                         ; preds = %if.else
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 45
  %arrayidx7 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info6, i32 0, i32 0
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx7, align 8, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 18
  %17 = load i32, i32* %last_row_height, align 4, !tbaa !93
  %18 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %18, i32 0, i32 4
  store i32 %17, i32* %MCU_rows_per_iMCU_row8, align 4, !tbaa !88
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %19 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %19, i32 0, i32 2
  store i32 0, i32* %mcu_ctr, align 4, !tbaa !89
  %20 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %20, i32 0, i32 3
  store i32 0, i32* %MCU_vert_offset, align 4, !tbaa !87
  %21 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 332}
!7 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !9, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !10, i64 216, !10, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !8, i64 4172}
!12 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !4, i64 56, !4, i64 2104, !8, i64 4152, !8, i64 4156, !8, i64 4160, !8, i64 4164, !8, i64 4168, !8, i64 4172, !8, i64 4176, !8, i64 4180, !8, i64 4184, !8, i64 4188, !8, i64 4192, !13, i64 4196, !13, i64 4200, !13, i64 4204}
!13 = !{!"float", !4, i64 0}
!14 = !{!12, !8, i64 20}
!15 = !{!7, !8, i64 20}
!16 = !{!7, !3, i64 0}
!17 = !{!18, !8, i64 20}
!18 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !19, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!19 = !{!"long", !4, i64 0}
!20 = !{!4, !4, i64 0}
!21 = !{!18, !3, i64 0}
!22 = !{!18, !3, i64 16}
!23 = !{!7, !3, i64 24}
!24 = !{!25, !3, i64 8}
!25 = !{!"jpeg_destination_mgr", !3, i64 0, !19, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!26 = !{!7, !8, i64 224}
!27 = !{!7, !8, i64 36}
!28 = !{!7, !8, i64 180}
!29 = !{!7, !8, i64 228}
!30 = !{!7, !3, i64 4}
!31 = !{!32, !3, i64 24}
!32 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !19, i64 44, !19, i64 48}
!33 = !{!7, !3, i64 348}
!34 = !{!35, !3, i64 0}
!35 = !{!"jpeg_marker_writer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!36 = !{!37, !8, i64 28}
!37 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!38 = !{!7, !8, i64 28}
!39 = !{!37, !8, i64 32}
!40 = !{!7, !8, i64 32}
!41 = !{!37, !8, i64 36}
!42 = !{!37, !4, i64 40}
!43 = !{!7, !4, i64 40}
!44 = !{!12, !8, i64 24}
!45 = !{!37, !8, i64 212}
!46 = !{!7, !8, i64 56}
!47 = !{!37, !8, i64 300}
!48 = !{!7, !8, i64 188}
!49 = !{!8, !8, i64 0}
!50 = !{!51, !8, i64 128}
!51 = !{!"", !4, i64 0, !8, i64 128}
!52 = !{!7, !8, i64 60}
!53 = !{!37, !3, i64 216}
!54 = !{!7, !3, i64 68}
!55 = !{!56, !8, i64 0}
!56 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!57 = !{!56, !8, i64 8}
!58 = !{!56, !8, i64 12}
!59 = !{!56, !8, i64 16}
!60 = !{!56, !3, i64 76}
!61 = !{!10, !10, i64 0}
!62 = !{!37, !8, i64 280}
!63 = !{!37, !4, i64 284}
!64 = !{!7, !4, i64 212}
!65 = !{!37, !4, i64 285}
!66 = !{!7, !4, i64 213}
!67 = !{!37, !4, i64 286}
!68 = !{!7, !4, i64 214}
!69 = !{!37, !10, i64 288}
!70 = !{!7, !10, i64 216}
!71 = !{!37, !10, i64 290}
!72 = !{!7, !10, i64 218}
!73 = !{!32, !3, i64 0}
!74 = !{!7, !3, i64 344}
!75 = !{!76, !3, i64 0}
!76 = !{!"", !77, i64 0, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !4, i64 28}
!77 = !{!"jpeg_c_coef_controller", !3, i64 0, !3, i64 4}
!78 = !{!76, !3, i64 4}
!79 = !{!76, !3, i64 24}
!80 = !{!32, !3, i64 4}
!81 = !{!76, !8, i64 8}
!82 = !{!7, !8, i64 264}
!83 = !{!7, !8, i64 240}
!84 = !{!7, !8, i64 244}
!85 = !{!32, !3, i64 32}
!86 = !{!56, !8, i64 4}
!87 = !{!76, !8, i64 16}
!88 = !{!76, !8, i64 20}
!89 = !{!76, !8, i64 12}
!90 = !{!56, !8, i64 52}
!91 = !{!56, !8, i64 68}
!92 = !{!56, !8, i64 56}
!93 = !{!56, !8, i64 72}
!94 = !{!7, !3, i64 364}
!95 = !{!96, !3, i64 4}
!96 = !{!"jpeg_entropy_encoder", !3, i64 0, !3, i64 4, !3, i64 8}
