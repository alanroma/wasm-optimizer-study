; ModuleID = 'jutils.c'
source_filename = "jutils.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@jpeg_natural_order = hidden constant [80 x i32] [i32 0, i32 1, i32 8, i32 16, i32 9, i32 2, i32 3, i32 10, i32 17, i32 24, i32 32, i32 25, i32 18, i32 11, i32 4, i32 5, i32 12, i32 19, i32 26, i32 33, i32 40, i32 48, i32 41, i32 34, i32 27, i32 20, i32 13, i32 6, i32 7, i32 14, i32 21, i32 28, i32 35, i32 42, i32 49, i32 56, i32 57, i32 50, i32 43, i32 36, i32 29, i32 22, i32 15, i32 23, i32 30, i32 37, i32 44, i32 51, i32 58, i32 59, i32 52, i32 45, i32 38, i32 31, i32 39, i32 46, i32 53, i32 60, i32 61, i32 54, i32 47, i32 55, i32 62, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63, i32 63], align 16

; Function Attrs: nounwind
define hidden i32 @jdiv_round_up(i32 %a, i32 %b) #0 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4, !tbaa !2
  store i32 %b, i32* %b.addr, align 4, !tbaa !2
  %0 = load i32, i32* %a.addr, align 4, !tbaa !2
  %1 = load i32, i32* %b.addr, align 4, !tbaa !2
  %add = add nsw i32 %0, %1
  %sub = sub nsw i32 %add, 1
  %2 = load i32, i32* %b.addr, align 4, !tbaa !2
  %div = sdiv i32 %sub, %2
  ret i32 %div
}

; Function Attrs: nounwind
define hidden i32 @jround_up(i32 %a, i32 %b) #0 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4, !tbaa !2
  store i32 %b, i32* %b.addr, align 4, !tbaa !2
  %0 = load i32, i32* %b.addr, align 4, !tbaa !2
  %sub = sub nsw i32 %0, 1
  %1 = load i32, i32* %a.addr, align 4, !tbaa !2
  %add = add nsw i32 %1, %sub
  store i32 %add, i32* %a.addr, align 4, !tbaa !2
  %2 = load i32, i32* %a.addr, align 4, !tbaa !2
  %3 = load i32, i32* %a.addr, align 4, !tbaa !2
  %4 = load i32, i32* %b.addr, align 4, !tbaa !2
  %rem = srem i32 %3, %4
  %sub1 = sub nsw i32 %2, %rem
  ret i32 %sub1
}

; Function Attrs: nounwind
define hidden void @jcopy_sample_rows(i8** %input_array, i32 %source_row, i8** %output_array, i32 %dest_row, i32 %num_rows, i32 %num_cols) #0 {
entry:
  %input_array.addr = alloca i8**, align 4
  %source_row.addr = alloca i32, align 4
  %output_array.addr = alloca i8**, align 4
  %dest_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %num_cols.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %count = alloca i32, align 4
  %row = alloca i32, align 4
  store i8** %input_array, i8*** %input_array.addr, align 4, !tbaa !6
  store i32 %source_row, i32* %source_row.addr, align 4, !tbaa !8
  store i8** %output_array, i8*** %output_array.addr, align 4, !tbaa !6
  store i32 %dest_row, i32* %dest_row.addr, align 4, !tbaa !8
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !8
  store i32 %num_cols, i32* %num_cols.addr, align 4, !tbaa !8
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i32, i32* %num_cols.addr, align 4, !tbaa !8
  %mul = mul i32 %3, 1
  store i32 %mul, i32* %count, align 4, !tbaa !2
  %4 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %source_row.addr, align 4, !tbaa !8
  %6 = load i8**, i8*** %input_array.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8*, i8** %6, i32 %5
  store i8** %add.ptr, i8*** %input_array.addr, align 4, !tbaa !6
  %7 = load i32, i32* %dest_row.addr, align 4, !tbaa !8
  %8 = load i8**, i8*** %output_array.addr, align 4, !tbaa !6
  %add.ptr1 = getelementptr inbounds i8*, i8** %8, i32 %7
  store i8** %add.ptr1, i8*** %output_array.addr, align 4, !tbaa !6
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !8
  store i32 %9, i32* %row, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %row, align 4, !tbaa !8
  %cmp = icmp sgt i32 %10, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i8**, i8*** %input_array.addr, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %input_array.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %11, align 4, !tbaa !6
  store i8* %12, i8** %inptr, align 4, !tbaa !6
  %13 = load i8**, i8*** %output_array.addr, align 4, !tbaa !6
  %incdec.ptr2 = getelementptr inbounds i8*, i8** %13, i32 1
  store i8** %incdec.ptr2, i8*** %output_array.addr, align 4, !tbaa !6
  %14 = load i8*, i8** %13, align 4, !tbaa !6
  store i8* %14, i8** %outptr, align 4, !tbaa !6
  %15 = load i8*, i8** %outptr, align 4, !tbaa !6
  %16 = load i8*, i8** %inptr, align 4, !tbaa !6
  %17 = load i32, i32* %count, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %15, i8* align 1 %16, i32 %17, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %row, align 4, !tbaa !8
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %row, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jcopy_block_row([64 x i16]* %input_row, [64 x i16]* %output_row, i32 %num_blocks) #0 {
entry:
  %input_row.addr = alloca [64 x i16]*, align 4
  %output_row.addr = alloca [64 x i16]*, align 4
  %num_blocks.addr = alloca i32, align 4
  store [64 x i16]* %input_row, [64 x i16]** %input_row.addr, align 4, !tbaa !6
  store [64 x i16]* %output_row, [64 x i16]** %output_row.addr, align 4, !tbaa !6
  store i32 %num_blocks, i32* %num_blocks.addr, align 4, !tbaa !8
  %0 = load [64 x i16]*, [64 x i16]** %output_row.addr, align 4, !tbaa !6
  %1 = bitcast [64 x i16]* %0 to i8*
  %2 = load [64 x i16]*, [64 x i16]** %input_row.addr, align 4, !tbaa !6
  %3 = bitcast [64 x i16]* %2 to i8*
  %4 = load i32, i32* %num_blocks.addr, align 4, !tbaa !8
  %mul = mul i32 %4, 128
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 1 %3, i32 %mul, i1 false)
  ret void
}

; Function Attrs: nounwind
define hidden void @jzero_far(i8* %target, i32 %bytestozero) #0 {
entry:
  %target.addr = alloca i8*, align 4
  %bytestozero.addr = alloca i32, align 4
  store i8* %target, i8** %target.addr, align 4, !tbaa !6
  store i32 %bytestozero, i32* %bytestozero.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %target.addr, align 4, !tbaa !6
  %1 = load i32, i32* %bytestozero.addr, align 4, !tbaa !2
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 %1, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"long", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
