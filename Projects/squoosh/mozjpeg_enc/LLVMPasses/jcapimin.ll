; ModuleID = 'jcapimin.c'
source_filename = "jcapimin.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @jpeg_CreateCompress(%struct.jpeg_compress_struct* %cinfo, i32 %version, i32 %structsize) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %version.addr = alloca i32, align 4
  %structsize.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %err23 = alloca %struct.jpeg_error_mgr*, align 4
  %client_data = alloca i8*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %version, i32* %version.addr, align 4, !tbaa !6
  store i32 %structsize, i32* %structsize.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 1
  store %struct.jpeg_memory_mgr* null, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !10
  %2 = load i32, i32* %version.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %2, 62
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 12, i32* %msg_code, align 4, !tbaa !15
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 6
  %i2 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i2, i32 0, i32 0
  store i32 62, i32* %arrayidx, align 4, !tbaa !17
  %7 = load i32, i32* %version.addr, align 4, !tbaa !6
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !14
  %msg_parm4 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i5 = bitcast %union.anon* %msg_parm4 to [8 x i32]*
  %arrayidx6 = getelementptr inbounds [8 x i32], [8 x i32]* %i5, i32 0, i32 1
  store i32 %7, i32* %arrayidx6, align 4, !tbaa !17
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_compress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i32, i32* %structsize.addr, align 4, !tbaa !8
  %cmp8 = icmp ne i32 %15, 376
  br i1 %cmp8, label %if.then9, label %if.end22

if.then9:                                         ; preds = %if.end
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !14
  %msg_code11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 21, i32* %msg_code11, align 4, !tbaa !15
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !14
  %msg_parm13 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 6
  %i14 = bitcast %union.anon* %msg_parm13 to [8 x i32]*
  %arrayidx15 = getelementptr inbounds [8 x i32], [8 x i32]* %i14, i32 0, i32 0
  store i32 376, i32* %arrayidx15, align 4, !tbaa !17
  %20 = load i32, i32* %structsize.addr, align 4, !tbaa !8
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err16, align 8, !tbaa !14
  %msg_parm17 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 6
  %i18 = bitcast %union.anon* %msg_parm17 to [8 x i32]*
  %arrayidx19 = getelementptr inbounds [8 x i32], [8 x i32]* %i18, i32 0, i32 1
  store i32 %20, i32* %arrayidx19, align 4, !tbaa !17
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !14
  %error_exit21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit21, align 4, !tbaa !18
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_compress_struct* %26 to %struct.jpeg_common_struct*
  call void %25(%struct.jpeg_common_struct* %27)
  br label %if.end22

if.end22:                                         ; preds = %if.then9, %if.end
  %28 = bitcast %struct.jpeg_error_mgr** %err23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err24, align 8, !tbaa !14
  store %struct.jpeg_error_mgr* %30, %struct.jpeg_error_mgr** %err23, align 4, !tbaa !2
  %31 = bitcast i8** %client_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %client_data25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 3
  %33 = load i8*, i8** %client_data25, align 4, !tbaa !19
  store i8* %33, i8** %client_data, align 4, !tbaa !2
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %35 = bitcast %struct.jpeg_compress_struct* %34 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %35, i8 0, i32 376, i1 false)
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err23, align 4, !tbaa !2
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err26 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %37, i32 0, i32 0
  store %struct.jpeg_error_mgr* %36, %struct.jpeg_error_mgr** %err26, align 8, !tbaa !14
  %38 = load i8*, i8** %client_data, align 4, !tbaa !2
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %client_data27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 3
  store i8* %38, i8** %client_data27, align 4, !tbaa !19
  %40 = bitcast i8** %client_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast %struct.jpeg_error_mgr** %err23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %is_decompressor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 4
  store i32 0, i32* %is_decompressor, align 8, !tbaa !20
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %44 = bitcast %struct.jpeg_compress_struct* %43 to %struct.jpeg_common_struct*
  call void @jinit_memory_mgr(%struct.jpeg_common_struct* %44)
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 2
  store %struct.jpeg_progress_mgr* null, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !21
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 6
  store %struct.jpeg_destination_mgr* null, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !22
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 15
  store %struct.jpeg_component_info* null, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !23
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end22
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %48, 4
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 16
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %50
  store %struct.JQUANT_TBL* null, %struct.JQUANT_TBL** %arrayidx29, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc35, %for.end
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %cmp31 = icmp slt i32 %52, 4
  br i1 %cmp31, label %for.body32, label %for.end37

for.body32:                                       ; preds = %for.cond30
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 17
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %54
  store %struct.JHUFF_TBL* null, %struct.JHUFF_TBL** %arrayidx33, align 4, !tbaa !2
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 18
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %56
  store %struct.JHUFF_TBL* null, %struct.JHUFF_TBL** %arrayidx34, align 4, !tbaa !2
  br label %for.inc35

for.inc35:                                        ; preds = %for.body32
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %inc36 = add nsw i32 %57, 1
  store i32 %inc36, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.end37:                                        ; preds = %for.cond30
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %script_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 63
  store %struct.jpeg_scan_info* null, %struct.jpeg_scan_info** %script_space, align 8, !tbaa !24
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_gamma = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 11
  store double 1.000000e+00, double* %input_gamma, align 8, !tbaa !25
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 5
  store i32 100, i32* %global_state, align 4, !tbaa !26
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 1
  %62 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem38, align 4, !tbaa !10
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %62, i32 0, i32 0
  %63 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !27
  %64 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %65 = bitcast %struct.jpeg_compress_struct* %64 to %struct.jpeg_common_struct*
  %call = call i8* %63(%struct.jpeg_common_struct* %65, i32 0, i32 5032)
  %66 = bitcast i8* %call to %struct.jpeg_comp_master*
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 54
  store %struct.jpeg_comp_master* %66, %struct.jpeg_comp_master** %master, align 4, !tbaa !29
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master39 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 54
  %69 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master39, align 4, !tbaa !29
  %70 = bitcast %struct.jpeg_comp_master* %69 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %70, i8 0, i32 5032, i1 false)
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master40 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 54
  %72 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master40, align 4, !tbaa !29
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %72, i32 0, i32 16
  store i32 1560820397, i32* %compress_profile, align 8, !tbaa !30
  %73 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jinit_memory_mgr(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define hidden void @jpeg_destroy_compress(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = bitcast %struct.jpeg_compress_struct* %0 to %struct.jpeg_common_struct*
  call void @jpeg_destroy(%struct.jpeg_common_struct* %1)
  ret void
}

declare void @jpeg_destroy(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define hidden void @jpeg_abort_compress(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = bitcast %struct.jpeg_compress_struct* %0 to %struct.jpeg_common_struct*
  call void @jpeg_abort(%struct.jpeg_common_struct* %1)
  ret void
}

declare void @jpeg_abort(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define hidden void @jpeg_suppress_tables(%struct.jpeg_compress_struct* %cinfo, i32 %suppress) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %suppress.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %qtbl = alloca %struct.JQUANT_TBL*, align 4
  %htbl = alloca %struct.JHUFF_TBL*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %suppress, i32* %suppress.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 16
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %5
  %6 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %6, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %cmp1 = icmp ne %struct.JQUANT_TBL* %6, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %7 = load i32, i32* %suppress.addr, align 4, !tbaa !6
  %8 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %8, i32 0, i32 1
  store i32 %7, i32* %sent_table, align 4, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc15, %for.end
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %10, 4
  br i1 %cmp3, label %for.body4, label %for.end17

for.body4:                                        ; preds = %for.cond2
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 17
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %12
  %13 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx5, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %13, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %cmp6 = icmp ne %struct.JHUFF_TBL* %13, null
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %for.body4
  %14 = load i32, i32* %suppress.addr, align 4, !tbaa !6
  %15 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %sent_table8 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %15, i32 0, i32 2
  store i32 %14, i32* %sent_table8, align 4, !tbaa !35
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %for.body4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 18
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %17
  %18 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx10, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %18, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %cmp11 = icmp ne %struct.JHUFF_TBL* %18, null
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end9
  %19 = load i32, i32* %suppress.addr, align 4, !tbaa !6
  %20 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %sent_table13 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %20, i32 0, i32 2
  store i32 %19, i32* %sent_table13, align 4, !tbaa !35
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end9
  br label %for.inc15

for.inc15:                                        ; preds = %if.end14
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %21, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.end17:                                        ; preds = %for.cond2
  %22 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  %23 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_finish_compress(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %iMCU_row = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !26
  %cmp = icmp eq i32 %2, 101
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 5
  %4 = load i32, i32* %global_state1, align 4, !tbaa !26
  %cmp2 = icmp eq i32 %4, 102
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %next_scanline = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 39
  %6 = load i32, i32* %next_scanline, align 8, !tbaa !37
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 8
  %8 = load i32, i32* %image_height, align 8, !tbaa !38
  %cmp3 = icmp ult i32 %6, %8
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 67, i32* %msg_code, align 4, !tbaa !15
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_compress_struct* %14 to %struct.jpeg_common_struct*
  call void %13(%struct.jpeg_common_struct* %15)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 54
  %17 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !29
  %finish_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %17, i32 0, i32 2
  %18 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %finish_pass, align 8, !tbaa !39
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %18(%struct.jpeg_compress_struct* %19)
  br label %if.end16

if.else:                                          ; preds = %lor.lhs.false
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 5
  %21 = load i32, i32* %global_state6, align 4, !tbaa !26
  %cmp7 = icmp ne i32 %21, 103
  br i1 %cmp7, label %if.then8, label %if.end15

if.then8:                                         ; preds = %if.else
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !14
  %msg_code10 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 5
  store i32 20, i32* %msg_code10, align 4, !tbaa !15
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 5
  %25 = load i32, i32* %global_state11, align 4, !tbaa !26
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 0
  %27 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %27, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %25, i32* %arrayidx, align 4, !tbaa !17
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !14
  %error_exit14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 0
  %30 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit14, align 4, !tbaa !18
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = bitcast %struct.jpeg_compress_struct* %31 to %struct.jpeg_common_struct*
  call void %30(%struct.jpeg_common_struct* %32)
  br label %if.end15

if.end15:                                         ; preds = %if.then8, %if.else
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.end
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.end16
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %33, i32 0, i32 54
  %34 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master17, align 4, !tbaa !29
  %is_last_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %34, i32 0, i32 4
  %35 = load i32, i32* %is_last_pass, align 8, !tbaa !40
  %tobool = icmp ne i32 %35, 0
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 54
  %37 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master18, align 4, !tbaa !29
  %prepare_for_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %37, i32 0, i32 0
  %38 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %prepare_for_pass, align 8, !tbaa !41
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %38(%struct.jpeg_compress_struct* %39)
  store i32 0, i32* %iMCU_row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %40 = load i32, i32* %iMCU_row, align 4, !tbaa !6
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 43
  %42 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !42
  %cmp19 = icmp ult i32 %40, %42
  br i1 %cmp19, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 2
  %44 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !21
  %cmp20 = icmp ne %struct.jpeg_progress_mgr* %44, null
  br i1 %cmp20, label %if.then21, label %if.end27

if.then21:                                        ; preds = %for.body
  %45 = load i32, i32* %iMCU_row, align 4, !tbaa !6
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 2
  %47 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress22, align 8, !tbaa !21
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %47, i32 0, i32 1
  store i32 %45, i32* %pass_counter, align 4, !tbaa !43
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 43
  %49 = load i32, i32* %total_iMCU_rows23, align 8, !tbaa !42
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 2
  %51 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress24, align 8, !tbaa !21
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %51, i32 0, i32 2
  store i32 %49, i32* %pass_limit, align 4, !tbaa !45
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 2
  %53 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress25, align 8, !tbaa !21
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %53, i32 0, i32 0
  %progress_monitor26 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %54 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor26, align 4, !tbaa !46
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %56 = bitcast %struct.jpeg_compress_struct* %55 to %struct.jpeg_common_struct*
  call void %54(%struct.jpeg_common_struct* %56)
  br label %if.end27

if.end27:                                         ; preds = %if.then21, %for.body
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 57
  %58 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef, align 8, !tbaa !47
  %compress_data = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %58, i32 0, i32 1
  %59 = load i32 (%struct.jpeg_compress_struct*, i8***)*, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data, align 4, !tbaa !48
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %59(%struct.jpeg_compress_struct* %60, i8*** null)
  %tobool28 = icmp ne i32 %call, 0
  br i1 %tobool28, label %if.end34, label %if.then29

if.then29:                                        ; preds = %if.end27
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err30, align 8, !tbaa !14
  %msg_code31 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 5
  store i32 24, i32* %msg_code31, align 4, !tbaa !15
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err32, align 8, !tbaa !14
  %error_exit33 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 0
  %65 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit33, align 4, !tbaa !18
  %66 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %67 = bitcast %struct.jpeg_compress_struct* %66 to %struct.jpeg_common_struct*
  call void %65(%struct.jpeg_common_struct* %67)
  br label %if.end34

if.end34:                                         ; preds = %if.then29, %if.end27
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %68 = load i32, i32* %iMCU_row, align 4, !tbaa !6
  %inc = add i32 %68, 1
  store i32 %inc, i32* %iMCU_row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 54
  %70 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master35, align 4, !tbaa !29
  %finish_pass36 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %70, i32 0, i32 2
  %71 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %finish_pass36, align 8, !tbaa !39
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %71(%struct.jpeg_compress_struct* %72)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %73, i32 0, i32 58
  %74 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !50
  %write_file_trailer = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %74, i32 0, i32 3
  %75 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_file_trailer, align 4, !tbaa !51
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %75(%struct.jpeg_compress_struct* %76)
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 6
  %78 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !22
  %term_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %78, i32 0, i32 4
  %79 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %term_destination, align 4, !tbaa !53
  %80 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %79(%struct.jpeg_compress_struct* %80)
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %82 = bitcast %struct.jpeg_compress_struct* %81 to %struct.jpeg_common_struct*
  call void @jpeg_abort(%struct.jpeg_common_struct* %82)
  %83 = bitcast i32* %iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_write_marker(%struct.jpeg_compress_struct* %cinfo, i32 %marker, i8* %dataptr, i32 %datalen) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker.addr = alloca i32, align 4
  %dataptr.addr = alloca i8*, align 4
  %datalen.addr = alloca i32, align 4
  %write_marker_byte = alloca void (%struct.jpeg_compress_struct*, i32)*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %marker, i32* %marker.addr, align 4, !tbaa !6
  store i8* %dataptr, i8** %dataptr.addr, align 4, !tbaa !2
  store i32 %datalen, i32* %datalen.addr, align 4, !tbaa !6
  %0 = bitcast void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %next_scanline = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 39
  %2 = load i32, i32* %next_scanline, align 8, !tbaa !37
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 5
  %4 = load i32, i32* %global_state, align 4, !tbaa !26
  %cmp1 = icmp ne i32 %4, 101
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %lor.lhs.false
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 5
  %6 = load i32, i32* %global_state2, align 4, !tbaa !26
  %cmp3 = icmp ne i32 %6, 102
  br i1 %cmp3, label %land.lhs.true4, label %if.end

land.lhs.true4:                                   ; preds = %land.lhs.true
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 5
  %8 = load i32, i32* %global_state5, align 4, !tbaa !26
  %cmp6 = icmp ne i32 %8, 103
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true4, %entry
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 5
  %12 = load i32, i32* %global_state7, align 4, !tbaa !26
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %12, i32* %arrayidx, align 4, !tbaa !17
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 0
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_compress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true4, %land.lhs.true, %lor.lhs.false
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 58
  %21 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker10, align 4, !tbaa !50
  %write_marker_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %21, i32 0, i32 5
  %22 = load void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32, i32)** %write_marker_header, align 4, !tbaa !55
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = load i32, i32* %marker.addr, align 4, !tbaa !6
  %25 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  call void %22(%struct.jpeg_compress_struct* %23, i32 %24, i32 %25)
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 58
  %27 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker11, align 4, !tbaa !50
  %write_marker_byte12 = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %27, i32 0, i32 6
  %28 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte12, align 4, !tbaa !56
  store void (%struct.jpeg_compress_struct*, i32)* %28, void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %29 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  %dec = add i32 %29, -1
  store i32 %dec, i32* %datalen.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %29, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %30 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte, align 4, !tbaa !2
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = load i8*, i8** %dataptr.addr, align 4, !tbaa !2
  %33 = load i8, i8* %32, align 1, !tbaa !17
  %conv = zext i8 %33 to i32
  call void %30(%struct.jpeg_compress_struct* %31, i32 %conv)
  %34 = load i8*, i8** %dataptr.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr, i8** %dataptr.addr, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %35 = bitcast void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_write_m_header(%struct.jpeg_compress_struct* %cinfo, i32 %marker, i32 %datalen) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker.addr = alloca i32, align 4
  %datalen.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %marker, i32* %marker.addr, align 4, !tbaa !6
  store i32 %datalen, i32* %datalen.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %next_scanline = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 39
  %1 = load i32, i32* %next_scanline, align 8, !tbaa !37
  %cmp = icmp ne i32 %1, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state, align 4, !tbaa !26
  %cmp1 = icmp ne i32 %3, 101
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %lor.lhs.false
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 5
  %5 = load i32, i32* %global_state2, align 4, !tbaa !26
  %cmp3 = icmp ne i32 %5, 102
  br i1 %cmp3, label %land.lhs.true4, label %if.end

land.lhs.true4:                                   ; preds = %land.lhs.true
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state5, align 4, !tbaa !26
  %cmp6 = icmp ne i32 %7, 103
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true4, %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 5
  %11 = load i32, i32* %global_state7, align 4, !tbaa !26
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %11, i32* %arrayidx, align 4, !tbaa !17
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = bitcast %struct.jpeg_compress_struct* %17 to %struct.jpeg_common_struct*
  call void %16(%struct.jpeg_common_struct* %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true4, %land.lhs.true, %lor.lhs.false
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 58
  %20 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker10, align 4, !tbaa !50
  %write_marker_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %20, i32 0, i32 5
  %21 = load void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32, i32)** %write_marker_header, align 4, !tbaa !55
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i32, i32* %marker.addr, align 4, !tbaa !6
  %24 = load i32, i32* %datalen.addr, align 4, !tbaa !6
  call void %21(%struct.jpeg_compress_struct* %22, i32 %23, i32 %24)
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_write_m_byte(%struct.jpeg_compress_struct* %cinfo, i32 %val) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %val.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 58
  %1 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !50
  %write_marker_byte = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %1, i32 0, i32 6
  %2 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte, align 4, !tbaa !56
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %4 = load i32, i32* %val.addr, align 4, !tbaa !6
  call void %2(%struct.jpeg_compress_struct* %3, i32 %4)
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_write_tables(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !26
  %cmp = icmp ne i32 %1, 100
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 0
  %3 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %3, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 5
  %5 = load i32, i32* %global_state1, align 4, !tbaa !26
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %5, i32* %arrayidx, align 4, !tbaa !17
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 0
  %10 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %12 = bitcast %struct.jpeg_compress_struct* %11 to %struct.jpeg_common_struct*
  call void %10(%struct.jpeg_common_struct* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !14
  %reset_error_mgr = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 4
  %15 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %reset_error_mgr, align 4, !tbaa !57
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_compress_struct* %16 to %struct.jpeg_common_struct*
  call void %15(%struct.jpeg_common_struct* %17)
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 6
  %19 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !22
  %init_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %19, i32 0, i32 2
  %20 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %init_destination, align 4, !tbaa !58
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %20(%struct.jpeg_compress_struct* %21)
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_marker_writer(%struct.jpeg_compress_struct* %22)
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 58
  %24 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !50
  %write_tables_only = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %24, i32 0, i32 4
  %25 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_tables_only, align 4, !tbaa !59
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %25(%struct.jpeg_compress_struct* %26)
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 6
  %28 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest5, align 8, !tbaa !22
  %term_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %28, i32 0, i32 4
  %29 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %term_destination, align 4, !tbaa !53
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %29(%struct.jpeg_compress_struct* %30)
  ret void
}

declare void @jinit_marker_writer(%struct.jpeg_compress_struct*) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!11, !3, i64 4}
!11 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !12, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !13, i64 216, !13, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!12 = !{!"double", !4, i64 0}
!13 = !{!"short", !4, i64 0}
!14 = !{!11, !3, i64 0}
!15 = !{!16, !7, i64 20}
!16 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !9, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!17 = !{!4, !4, i64 0}
!18 = !{!16, !3, i64 0}
!19 = !{!11, !3, i64 12}
!20 = !{!11, !7, i64 16}
!21 = !{!11, !3, i64 8}
!22 = !{!11, !3, i64 24}
!23 = !{!11, !3, i64 68}
!24 = !{!11, !3, i64 368}
!25 = !{!11, !12, i64 48}
!26 = !{!11, !7, i64 20}
!27 = !{!28, !3, i64 0}
!28 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !9, i64 44, !9, i64 48}
!29 = !{!11, !3, i64 332}
!30 = !{!31, !7, i64 4152}
!31 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !4, i64 56, !4, i64 2104, !7, i64 4152, !7, i64 4156, !7, i64 4160, !7, i64 4164, !7, i64 4168, !7, i64 4172, !7, i64 4176, !7, i64 4180, !7, i64 4184, !7, i64 4188, !7, i64 4192, !32, i64 4196, !32, i64 4200, !32, i64 4204}
!32 = !{!"float", !4, i64 0}
!33 = !{!34, !7, i64 128}
!34 = !{!"", !4, i64 0, !7, i64 128}
!35 = !{!36, !7, i64 276}
!36 = !{!"", !4, i64 0, !4, i64 17, !7, i64 276}
!37 = !{!11, !7, i64 224}
!38 = !{!11, !7, i64 32}
!39 = !{!31, !3, i64 8}
!40 = !{!31, !7, i64 16}
!41 = !{!31, !3, i64 0}
!42 = !{!11, !7, i64 240}
!43 = !{!44, !9, i64 4}
!44 = !{!"jpeg_progress_mgr", !3, i64 0, !9, i64 4, !9, i64 8, !7, i64 12, !7, i64 16}
!45 = !{!44, !9, i64 8}
!46 = !{!44, !3, i64 0}
!47 = !{!11, !3, i64 344}
!48 = !{!49, !3, i64 4}
!49 = !{!"jpeg_c_coef_controller", !3, i64 0, !3, i64 4}
!50 = !{!11, !3, i64 348}
!51 = !{!52, !3, i64 12}
!52 = !{!"jpeg_marker_writer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!53 = !{!54, !3, i64 16}
!54 = !{!"jpeg_destination_mgr", !3, i64 0, !9, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!55 = !{!52, !3, i64 20}
!56 = !{!52, !3, i64 24}
!57 = !{!16, !3, i64 16}
!58 = !{!54, !3, i64 8}
!59 = !{!52, !3, i64 16}
