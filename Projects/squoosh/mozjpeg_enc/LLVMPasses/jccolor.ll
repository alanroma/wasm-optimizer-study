; ModuleID = 'jccolor.c'
source_filename = "jccolor.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_compress_struct*)*, {}* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { {}*, {}*, {}*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { {}*, {}*, {}*, {}*, {}*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { {}*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { {}*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { {}*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, {}* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_color_converter = type { %struct.jpeg_color_converter, i32* }

@rgb_pixelsize = internal constant [17 x i32] [i32 -1, i32 -1, i32 3, i32 -1, i32 -1, i32 -1, i32 3, i32 4, i32 3, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 -1], align 16
@rgb_red = internal constant [17 x i32] [i32 -1, i32 -1, i32 0, i32 -1, i32 -1, i32 -1, i32 0, i32 0, i32 2, i32 2, i32 3, i32 1, i32 0, i32 2, i32 3, i32 1, i32 -1], align 16
@rgb_green = internal constant [17 x i32] [i32 -1, i32 -1, i32 1, i32 -1, i32 -1, i32 -1, i32 1, i32 1, i32 1, i32 1, i32 2, i32 2, i32 1, i32 1, i32 2, i32 2, i32 -1], align 16
@rgb_blue = internal constant [17 x i32] [i32 -1, i32 -1, i32 2, i32 -1, i32 -1, i32 -1, i32 2, i32 2, i32 0, i32 0, i32 1, i32 3, i32 2, i32 0, i32 1, i32 3, i32 -1], align 16

; Function Attrs: nounwind
define hidden void @jinit_color_converter(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_compress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 12)
  %6 = bitcast i8* %call to %struct.my_color_converter*
  store %struct.my_color_converter* %6, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %7 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %8 = bitcast %struct.my_color_converter* %7 to %struct.jpeg_color_converter*
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 59
  store %struct.jpeg_color_converter* %8, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %10 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %10, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @null_method, void (%struct.jpeg_compress_struct*)** %start_pass2, align 4, !tbaa !15
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 10
  %12 = load i32, i32* %in_color_space, align 8, !tbaa !18
  switch i32 %12, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb4
    i32 6, label %sw.bb4
    i32 7, label %sw.bb4
    i32 8, label %sw.bb4
    i32 9, label %sw.bb4
    i32 10, label %sw.bb4
    i32 11, label %sw.bb4
    i32 12, label %sw.bb4
    i32 13, label %sw.bb4
    i32 14, label %sw.bb4
    i32 15, label %sw.bb4
    i32 3, label %sw.bb14
    i32 4, label %sw.bb23
    i32 5, label %sw.bb23
  ]

sw.bb:                                            ; preds = %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 9
  %14 = load i32, i32* %input_components, align 4, !tbaa !19
  %cmp = icmp ne i32 %14, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !20
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 5
  store i32 9, i32* %msg_code, align 4, !tbaa !21
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !20
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 0
  %19 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !23
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = bitcast %struct.jpeg_compress_struct* %20 to %struct.jpeg_common_struct*
  call void %19(%struct.jpeg_common_struct* %21)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 9
  %23 = load i32, i32* %input_components5, align 4, !tbaa !19
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 10
  %25 = load i32, i32* %in_color_space6, align 8, !tbaa !18
  %arrayidx = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %25
  %26 = load i32, i32* %arrayidx, align 4, !tbaa !24
  %cmp7 = icmp ne i32 %23, %26
  br i1 %cmp7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %sw.bb4
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !20
  %msg_code10 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 5
  store i32 9, i32* %msg_code10, align 4, !tbaa !21
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !20
  %error_exit12 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %30, i32 0, i32 0
  %31 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit12, align 4, !tbaa !23
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = bitcast %struct.jpeg_compress_struct* %32 to %struct.jpeg_common_struct*
  call void %31(%struct.jpeg_common_struct* %33)
  br label %if.end13

if.end13:                                         ; preds = %if.then8, %sw.bb4
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 9
  %35 = load i32, i32* %input_components15, align 4, !tbaa !19
  %cmp16 = icmp ne i32 %35, 3
  br i1 %cmp16, label %if.then17, label %if.end22

if.then17:                                        ; preds = %sw.bb14
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !20
  %msg_code19 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 5
  store i32 9, i32* %msg_code19, align 4, !tbaa !21
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !20
  %error_exit21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 0
  %40 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit21, align 4, !tbaa !23
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = bitcast %struct.jpeg_compress_struct* %41 to %struct.jpeg_common_struct*
  call void %40(%struct.jpeg_common_struct* %42)
  br label %if.end22

if.end22:                                         ; preds = %if.then17, %sw.bb14
  br label %sw.epilog

sw.bb23:                                          ; preds = %entry, %entry
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 9
  %44 = load i32, i32* %input_components24, align 4, !tbaa !19
  %cmp25 = icmp ne i32 %44, 4
  br i1 %cmp25, label %if.then26, label %if.end31

if.then26:                                        ; preds = %sw.bb23
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 0
  %46 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err27, align 8, !tbaa !20
  %msg_code28 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %46, i32 0, i32 5
  store i32 9, i32* %msg_code28, align 4, !tbaa !21
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 0
  %48 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !20
  %error_exit30 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %48, i32 0, i32 0
  %49 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit30, align 4, !tbaa !23
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %51 = bitcast %struct.jpeg_compress_struct* %50 to %struct.jpeg_common_struct*
  call void %49(%struct.jpeg_common_struct* %51)
  br label %if.end31

if.end31:                                         ; preds = %if.then26, %sw.bb23
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 9
  %53 = load i32, i32* %input_components32, align 4, !tbaa !19
  %cmp33 = icmp slt i32 %53, 1
  br i1 %cmp33, label %if.then34, label %if.end39

if.then34:                                        ; preds = %sw.default
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 0
  %55 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err35, align 8, !tbaa !20
  %msg_code36 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %55, i32 0, i32 5
  store i32 9, i32* %msg_code36, align 4, !tbaa !21
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 0
  %57 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err37, align 8, !tbaa !20
  %error_exit38 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %57, i32 0, i32 0
  %58 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit38, align 4, !tbaa !23
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %60 = bitcast %struct.jpeg_compress_struct* %59 to %struct.jpeg_common_struct*
  call void %58(%struct.jpeg_common_struct* %60)
  br label %if.end39

if.end39:                                         ; preds = %if.then34, %sw.default
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end39, %if.end31, %if.end22, %if.end13, %if.end
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 14
  %62 = load i32, i32* %jpeg_color_space, align 8, !tbaa !25
  switch i32 %62, label %sw.default295 [
    i32 1, label %sw.bb40
    i32 2, label %sw.bb109
    i32 3, label %sw.bb178
    i32 4, label %sw.bb245
    i32 5, label %sw.bb265
  ]

sw.bb40:                                          ; preds = %sw.epilog
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 13
  %64 = load i32, i32* %num_components, align 4, !tbaa !26
  %cmp41 = icmp ne i32 %64, 1
  br i1 %cmp41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %sw.bb40
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 0
  %66 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 8, !tbaa !20
  %msg_code44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %66, i32 0, i32 5
  store i32 10, i32* %msg_code44, align 4, !tbaa !21
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err45 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 0
  %68 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err45, align 8, !tbaa !20
  %error_exit46 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %68, i32 0, i32 0
  %69 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit46, align 4, !tbaa !23
  %70 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %71 = bitcast %struct.jpeg_compress_struct* %70 to %struct.jpeg_common_struct*
  call void %69(%struct.jpeg_common_struct* %71)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %sw.bb40
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space48 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %72, i32 0, i32 10
  %73 = load i32, i32* %in_color_space48, align 8, !tbaa !18
  %cmp49 = icmp eq i32 %73, 1
  br i1 %cmp49, label %if.then50, label %if.else

if.then50:                                        ; preds = %if.end47
  %74 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub51 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %74, i32 0, i32 0
  %color_convert = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub51, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @grayscale_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert, align 4, !tbaa !27
  br label %if.end108

if.else:                                          ; preds = %if.end47
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space52 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 10
  %76 = load i32, i32* %in_color_space52, align 8, !tbaa !18
  %cmp53 = icmp eq i32 %76, 2
  br i1 %cmp53, label %if.then83, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 10
  %78 = load i32, i32* %in_color_space54, align 8, !tbaa !18
  %cmp55 = icmp eq i32 %78, 6
  br i1 %cmp55, label %if.then83, label %lor.lhs.false56

lor.lhs.false56:                                  ; preds = %lor.lhs.false
  %79 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %79, i32 0, i32 10
  %80 = load i32, i32* %in_color_space57, align 8, !tbaa !18
  %cmp58 = icmp eq i32 %80, 7
  br i1 %cmp58, label %if.then83, label %lor.lhs.false59

lor.lhs.false59:                                  ; preds = %lor.lhs.false56
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 10
  %82 = load i32, i32* %in_color_space60, align 8, !tbaa !18
  %cmp61 = icmp eq i32 %82, 8
  br i1 %cmp61, label %if.then83, label %lor.lhs.false62

lor.lhs.false62:                                  ; preds = %lor.lhs.false59
  %83 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space63 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %83, i32 0, i32 10
  %84 = load i32, i32* %in_color_space63, align 8, !tbaa !18
  %cmp64 = icmp eq i32 %84, 9
  br i1 %cmp64, label %if.then83, label %lor.lhs.false65

lor.lhs.false65:                                  ; preds = %lor.lhs.false62
  %85 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space66 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %85, i32 0, i32 10
  %86 = load i32, i32* %in_color_space66, align 8, !tbaa !18
  %cmp67 = icmp eq i32 %86, 10
  br i1 %cmp67, label %if.then83, label %lor.lhs.false68

lor.lhs.false68:                                  ; preds = %lor.lhs.false65
  %87 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space69 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %87, i32 0, i32 10
  %88 = load i32, i32* %in_color_space69, align 8, !tbaa !18
  %cmp70 = icmp eq i32 %88, 11
  br i1 %cmp70, label %if.then83, label %lor.lhs.false71

lor.lhs.false71:                                  ; preds = %lor.lhs.false68
  %89 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space72 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %89, i32 0, i32 10
  %90 = load i32, i32* %in_color_space72, align 8, !tbaa !18
  %cmp73 = icmp eq i32 %90, 12
  br i1 %cmp73, label %if.then83, label %lor.lhs.false74

lor.lhs.false74:                                  ; preds = %lor.lhs.false71
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space75 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 10
  %92 = load i32, i32* %in_color_space75, align 8, !tbaa !18
  %cmp76 = icmp eq i32 %92, 13
  br i1 %cmp76, label %if.then83, label %lor.lhs.false77

lor.lhs.false77:                                  ; preds = %lor.lhs.false74
  %93 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space78 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %93, i32 0, i32 10
  %94 = load i32, i32* %in_color_space78, align 8, !tbaa !18
  %cmp79 = icmp eq i32 %94, 14
  br i1 %cmp79, label %if.then83, label %lor.lhs.false80

lor.lhs.false80:                                  ; preds = %lor.lhs.false77
  %95 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space81 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %95, i32 0, i32 10
  %96 = load i32, i32* %in_color_space81, align 8, !tbaa !18
  %cmp82 = icmp eq i32 %96, 15
  br i1 %cmp82, label %if.then83, label %if.else95

if.then83:                                        ; preds = %lor.lhs.false80, %lor.lhs.false77, %lor.lhs.false74, %lor.lhs.false71, %lor.lhs.false68, %lor.lhs.false65, %lor.lhs.false62, %lor.lhs.false59, %lor.lhs.false56, %lor.lhs.false, %if.else
  %call84 = call i32 @jsimd_can_rgb_gray()
  %tobool = icmp ne i32 %call84, 0
  br i1 %tobool, label %if.then85, label %if.else88

if.then85:                                        ; preds = %if.then83
  %97 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub86 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %97, i32 0, i32 0
  %color_convert87 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub86, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @jsimd_rgb_gray_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert87, align 4, !tbaa !27
  br label %if.end94

if.else88:                                        ; preds = %if.then83
  %98 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub89 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %98, i32 0, i32 0
  %start_pass90 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub89, i32 0, i32 0
  %start_pass91 = bitcast {}** %start_pass90 to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @rgb_ycc_start, void (%struct.jpeg_compress_struct*)** %start_pass91, align 4, !tbaa !15
  %99 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub92 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %99, i32 0, i32 0
  %color_convert93 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub92, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @rgb_gray_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert93, align 4, !tbaa !27
  br label %if.end94

if.end94:                                         ; preds = %if.else88, %if.then85
  br label %if.end107

if.else95:                                        ; preds = %lor.lhs.false80
  %100 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space96 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %100, i32 0, i32 10
  %101 = load i32, i32* %in_color_space96, align 8, !tbaa !18
  %cmp97 = icmp eq i32 %101, 3
  br i1 %cmp97, label %if.then98, label %if.else101

if.then98:                                        ; preds = %if.else95
  %102 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub99 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %102, i32 0, i32 0
  %color_convert100 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub99, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @grayscale_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert100, align 4, !tbaa !27
  br label %if.end106

if.else101:                                       ; preds = %if.else95
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err102 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 0
  %104 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err102, align 8, !tbaa !20
  %msg_code103 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %104, i32 0, i32 5
  store i32 27, i32* %msg_code103, align 4, !tbaa !21
  %105 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err104 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %105, i32 0, i32 0
  %106 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err104, align 8, !tbaa !20
  %error_exit105 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %106, i32 0, i32 0
  %107 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit105, align 4, !tbaa !23
  %108 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %109 = bitcast %struct.jpeg_compress_struct* %108 to %struct.jpeg_common_struct*
  call void %107(%struct.jpeg_common_struct* %109)
  br label %if.end106

if.end106:                                        ; preds = %if.else101, %if.then98
  br label %if.end107

if.end107:                                        ; preds = %if.end106, %if.end94
  br label %if.end108

if.end108:                                        ; preds = %if.end107, %if.then50
  br label %sw.epilog311

sw.bb109:                                         ; preds = %sw.epilog
  %110 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components110 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %110, i32 0, i32 13
  %111 = load i32, i32* %num_components110, align 4, !tbaa !26
  %cmp111 = icmp ne i32 %111, 3
  br i1 %cmp111, label %if.then112, label %if.end117

if.then112:                                       ; preds = %sw.bb109
  %112 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err113 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %112, i32 0, i32 0
  %113 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err113, align 8, !tbaa !20
  %msg_code114 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %113, i32 0, i32 5
  store i32 10, i32* %msg_code114, align 4, !tbaa !21
  %114 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err115 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %114, i32 0, i32 0
  %115 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err115, align 8, !tbaa !20
  %error_exit116 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %115, i32 0, i32 0
  %116 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit116, align 4, !tbaa !23
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %118 = bitcast %struct.jpeg_compress_struct* %117 to %struct.jpeg_common_struct*
  call void %116(%struct.jpeg_common_struct* %118)
  br label %if.end117

if.end117:                                        ; preds = %if.then112, %sw.bb109
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space118 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 10
  %120 = load i32, i32* %in_color_space118, align 8, !tbaa !18
  %arrayidx119 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %120
  %121 = load i32, i32* %arrayidx119, align 4, !tbaa !24
  %cmp120 = icmp eq i32 %121, 0
  br i1 %cmp120, label %land.lhs.true, label %if.else135

land.lhs.true:                                    ; preds = %if.end117
  %122 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space121 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %122, i32 0, i32 10
  %123 = load i32, i32* %in_color_space121, align 8, !tbaa !18
  %arrayidx122 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %123
  %124 = load i32, i32* %arrayidx122, align 4, !tbaa !24
  %cmp123 = icmp eq i32 %124, 1
  br i1 %cmp123, label %land.lhs.true124, label %if.else135

land.lhs.true124:                                 ; preds = %land.lhs.true
  %125 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space125 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %125, i32 0, i32 10
  %126 = load i32, i32* %in_color_space125, align 8, !tbaa !18
  %arrayidx126 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %126
  %127 = load i32, i32* %arrayidx126, align 4, !tbaa !24
  %cmp127 = icmp eq i32 %127, 2
  br i1 %cmp127, label %land.lhs.true128, label %if.else135

land.lhs.true128:                                 ; preds = %land.lhs.true124
  %128 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space129 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %128, i32 0, i32 10
  %129 = load i32, i32* %in_color_space129, align 8, !tbaa !18
  %arrayidx130 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %129
  %130 = load i32, i32* %arrayidx130, align 4, !tbaa !24
  %cmp131 = icmp eq i32 %130, 3
  br i1 %cmp131, label %if.then132, label %if.else135

if.then132:                                       ; preds = %land.lhs.true128
  %131 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub133 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %131, i32 0, i32 0
  %color_convert134 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub133, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @null_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert134, align 4, !tbaa !27
  br label %if.end177

if.else135:                                       ; preds = %land.lhs.true128, %land.lhs.true124, %land.lhs.true, %if.end117
  %132 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space136 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %132, i32 0, i32 10
  %133 = load i32, i32* %in_color_space136, align 8, !tbaa !18
  %cmp137 = icmp eq i32 %133, 2
  br i1 %cmp137, label %if.then168, label %lor.lhs.false138

lor.lhs.false138:                                 ; preds = %if.else135
  %134 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space139 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %134, i32 0, i32 10
  %135 = load i32, i32* %in_color_space139, align 8, !tbaa !18
  %cmp140 = icmp eq i32 %135, 6
  br i1 %cmp140, label %if.then168, label %lor.lhs.false141

lor.lhs.false141:                                 ; preds = %lor.lhs.false138
  %136 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space142 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %136, i32 0, i32 10
  %137 = load i32, i32* %in_color_space142, align 8, !tbaa !18
  %cmp143 = icmp eq i32 %137, 7
  br i1 %cmp143, label %if.then168, label %lor.lhs.false144

lor.lhs.false144:                                 ; preds = %lor.lhs.false141
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space145 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 10
  %139 = load i32, i32* %in_color_space145, align 8, !tbaa !18
  %cmp146 = icmp eq i32 %139, 8
  br i1 %cmp146, label %if.then168, label %lor.lhs.false147

lor.lhs.false147:                                 ; preds = %lor.lhs.false144
  %140 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space148 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %140, i32 0, i32 10
  %141 = load i32, i32* %in_color_space148, align 8, !tbaa !18
  %cmp149 = icmp eq i32 %141, 9
  br i1 %cmp149, label %if.then168, label %lor.lhs.false150

lor.lhs.false150:                                 ; preds = %lor.lhs.false147
  %142 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space151 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %142, i32 0, i32 10
  %143 = load i32, i32* %in_color_space151, align 8, !tbaa !18
  %cmp152 = icmp eq i32 %143, 10
  br i1 %cmp152, label %if.then168, label %lor.lhs.false153

lor.lhs.false153:                                 ; preds = %lor.lhs.false150
  %144 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space154 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %144, i32 0, i32 10
  %145 = load i32, i32* %in_color_space154, align 8, !tbaa !18
  %cmp155 = icmp eq i32 %145, 11
  br i1 %cmp155, label %if.then168, label %lor.lhs.false156

lor.lhs.false156:                                 ; preds = %lor.lhs.false153
  %146 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space157 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %146, i32 0, i32 10
  %147 = load i32, i32* %in_color_space157, align 8, !tbaa !18
  %cmp158 = icmp eq i32 %147, 12
  br i1 %cmp158, label %if.then168, label %lor.lhs.false159

lor.lhs.false159:                                 ; preds = %lor.lhs.false156
  %148 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space160 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %148, i32 0, i32 10
  %149 = load i32, i32* %in_color_space160, align 8, !tbaa !18
  %cmp161 = icmp eq i32 %149, 13
  br i1 %cmp161, label %if.then168, label %lor.lhs.false162

lor.lhs.false162:                                 ; preds = %lor.lhs.false159
  %150 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space163 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %150, i32 0, i32 10
  %151 = load i32, i32* %in_color_space163, align 8, !tbaa !18
  %cmp164 = icmp eq i32 %151, 14
  br i1 %cmp164, label %if.then168, label %lor.lhs.false165

lor.lhs.false165:                                 ; preds = %lor.lhs.false162
  %152 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space166 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %152, i32 0, i32 10
  %153 = load i32, i32* %in_color_space166, align 8, !tbaa !18
  %cmp167 = icmp eq i32 %153, 15
  br i1 %cmp167, label %if.then168, label %if.else171

if.then168:                                       ; preds = %lor.lhs.false165, %lor.lhs.false162, %lor.lhs.false159, %lor.lhs.false156, %lor.lhs.false153, %lor.lhs.false150, %lor.lhs.false147, %lor.lhs.false144, %lor.lhs.false141, %lor.lhs.false138, %if.else135
  %154 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub169 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %154, i32 0, i32 0
  %color_convert170 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub169, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @rgb_rgb_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert170, align 4, !tbaa !27
  br label %if.end176

if.else171:                                       ; preds = %lor.lhs.false165
  %155 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err172 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %155, i32 0, i32 0
  %156 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err172, align 8, !tbaa !20
  %msg_code173 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %156, i32 0, i32 5
  store i32 27, i32* %msg_code173, align 4, !tbaa !21
  %157 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err174 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %157, i32 0, i32 0
  %158 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err174, align 8, !tbaa !20
  %error_exit175 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %158, i32 0, i32 0
  %159 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit175, align 4, !tbaa !23
  %160 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %161 = bitcast %struct.jpeg_compress_struct* %160 to %struct.jpeg_common_struct*
  call void %159(%struct.jpeg_common_struct* %161)
  br label %if.end176

if.end176:                                        ; preds = %if.else171, %if.then168
  br label %if.end177

if.end177:                                        ; preds = %if.end176, %if.then132
  br label %sw.epilog311

sw.bb178:                                         ; preds = %sw.epilog
  %162 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components179 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %162, i32 0, i32 13
  %163 = load i32, i32* %num_components179, align 4, !tbaa !26
  %cmp180 = icmp ne i32 %163, 3
  br i1 %cmp180, label %if.then181, label %if.end186

if.then181:                                       ; preds = %sw.bb178
  %164 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err182 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %164, i32 0, i32 0
  %165 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err182, align 8, !tbaa !20
  %msg_code183 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %165, i32 0, i32 5
  store i32 10, i32* %msg_code183, align 4, !tbaa !21
  %166 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err184 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %166, i32 0, i32 0
  %167 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err184, align 8, !tbaa !20
  %error_exit185 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %167, i32 0, i32 0
  %168 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit185, align 4, !tbaa !23
  %169 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %170 = bitcast %struct.jpeg_compress_struct* %169 to %struct.jpeg_common_struct*
  call void %168(%struct.jpeg_common_struct* %170)
  br label %if.end186

if.end186:                                        ; preds = %if.then181, %sw.bb178
  %171 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space187 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %171, i32 0, i32 10
  %172 = load i32, i32* %in_color_space187, align 8, !tbaa !18
  %cmp188 = icmp eq i32 %172, 2
  br i1 %cmp188, label %if.then219, label %lor.lhs.false189

lor.lhs.false189:                                 ; preds = %if.end186
  %173 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space190 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %173, i32 0, i32 10
  %174 = load i32, i32* %in_color_space190, align 8, !tbaa !18
  %cmp191 = icmp eq i32 %174, 6
  br i1 %cmp191, label %if.then219, label %lor.lhs.false192

lor.lhs.false192:                                 ; preds = %lor.lhs.false189
  %175 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space193 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %175, i32 0, i32 10
  %176 = load i32, i32* %in_color_space193, align 8, !tbaa !18
  %cmp194 = icmp eq i32 %176, 7
  br i1 %cmp194, label %if.then219, label %lor.lhs.false195

lor.lhs.false195:                                 ; preds = %lor.lhs.false192
  %177 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space196 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %177, i32 0, i32 10
  %178 = load i32, i32* %in_color_space196, align 8, !tbaa !18
  %cmp197 = icmp eq i32 %178, 8
  br i1 %cmp197, label %if.then219, label %lor.lhs.false198

lor.lhs.false198:                                 ; preds = %lor.lhs.false195
  %179 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space199 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %179, i32 0, i32 10
  %180 = load i32, i32* %in_color_space199, align 8, !tbaa !18
  %cmp200 = icmp eq i32 %180, 9
  br i1 %cmp200, label %if.then219, label %lor.lhs.false201

lor.lhs.false201:                                 ; preds = %lor.lhs.false198
  %181 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space202 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %181, i32 0, i32 10
  %182 = load i32, i32* %in_color_space202, align 8, !tbaa !18
  %cmp203 = icmp eq i32 %182, 10
  br i1 %cmp203, label %if.then219, label %lor.lhs.false204

lor.lhs.false204:                                 ; preds = %lor.lhs.false201
  %183 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space205 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %183, i32 0, i32 10
  %184 = load i32, i32* %in_color_space205, align 8, !tbaa !18
  %cmp206 = icmp eq i32 %184, 11
  br i1 %cmp206, label %if.then219, label %lor.lhs.false207

lor.lhs.false207:                                 ; preds = %lor.lhs.false204
  %185 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space208 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %185, i32 0, i32 10
  %186 = load i32, i32* %in_color_space208, align 8, !tbaa !18
  %cmp209 = icmp eq i32 %186, 12
  br i1 %cmp209, label %if.then219, label %lor.lhs.false210

lor.lhs.false210:                                 ; preds = %lor.lhs.false207
  %187 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space211 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %187, i32 0, i32 10
  %188 = load i32, i32* %in_color_space211, align 8, !tbaa !18
  %cmp212 = icmp eq i32 %188, 13
  br i1 %cmp212, label %if.then219, label %lor.lhs.false213

lor.lhs.false213:                                 ; preds = %lor.lhs.false210
  %189 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space214 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %189, i32 0, i32 10
  %190 = load i32, i32* %in_color_space214, align 8, !tbaa !18
  %cmp215 = icmp eq i32 %190, 14
  br i1 %cmp215, label %if.then219, label %lor.lhs.false216

lor.lhs.false216:                                 ; preds = %lor.lhs.false213
  %191 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space217 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %191, i32 0, i32 10
  %192 = load i32, i32* %in_color_space217, align 8, !tbaa !18
  %cmp218 = icmp eq i32 %192, 15
  br i1 %cmp218, label %if.then219, label %if.else232

if.then219:                                       ; preds = %lor.lhs.false216, %lor.lhs.false213, %lor.lhs.false210, %lor.lhs.false207, %lor.lhs.false204, %lor.lhs.false201, %lor.lhs.false198, %lor.lhs.false195, %lor.lhs.false192, %lor.lhs.false189, %if.end186
  %call220 = call i32 @jsimd_can_rgb_ycc()
  %tobool221 = icmp ne i32 %call220, 0
  br i1 %tobool221, label %if.then222, label %if.else225

if.then222:                                       ; preds = %if.then219
  %193 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub223 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %193, i32 0, i32 0
  %color_convert224 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub223, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @jsimd_rgb_ycc_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert224, align 4, !tbaa !27
  br label %if.end231

if.else225:                                       ; preds = %if.then219
  %194 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub226 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %194, i32 0, i32 0
  %start_pass227 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub226, i32 0, i32 0
  %start_pass228 = bitcast {}** %start_pass227 to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @rgb_ycc_start, void (%struct.jpeg_compress_struct*)** %start_pass228, align 4, !tbaa !15
  %195 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub229 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %195, i32 0, i32 0
  %color_convert230 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub229, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @rgb_ycc_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert230, align 4, !tbaa !27
  br label %if.end231

if.end231:                                        ; preds = %if.else225, %if.then222
  br label %if.end244

if.else232:                                       ; preds = %lor.lhs.false216
  %196 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space233 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %196, i32 0, i32 10
  %197 = load i32, i32* %in_color_space233, align 8, !tbaa !18
  %cmp234 = icmp eq i32 %197, 3
  br i1 %cmp234, label %if.then235, label %if.else238

if.then235:                                       ; preds = %if.else232
  %198 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub236 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %198, i32 0, i32 0
  %color_convert237 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub236, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @null_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert237, align 4, !tbaa !27
  br label %if.end243

if.else238:                                       ; preds = %if.else232
  %199 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err239 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %199, i32 0, i32 0
  %200 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err239, align 8, !tbaa !20
  %msg_code240 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %200, i32 0, i32 5
  store i32 27, i32* %msg_code240, align 4, !tbaa !21
  %201 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err241 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %201, i32 0, i32 0
  %202 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err241, align 8, !tbaa !20
  %error_exit242 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %202, i32 0, i32 0
  %203 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit242, align 4, !tbaa !23
  %204 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %205 = bitcast %struct.jpeg_compress_struct* %204 to %struct.jpeg_common_struct*
  call void %203(%struct.jpeg_common_struct* %205)
  br label %if.end243

if.end243:                                        ; preds = %if.else238, %if.then235
  br label %if.end244

if.end244:                                        ; preds = %if.end243, %if.end231
  br label %sw.epilog311

sw.bb245:                                         ; preds = %sw.epilog
  %206 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components246 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %206, i32 0, i32 13
  %207 = load i32, i32* %num_components246, align 4, !tbaa !26
  %cmp247 = icmp ne i32 %207, 4
  br i1 %cmp247, label %if.then248, label %if.end253

if.then248:                                       ; preds = %sw.bb245
  %208 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err249 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %208, i32 0, i32 0
  %209 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err249, align 8, !tbaa !20
  %msg_code250 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %209, i32 0, i32 5
  store i32 10, i32* %msg_code250, align 4, !tbaa !21
  %210 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err251 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %210, i32 0, i32 0
  %211 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err251, align 8, !tbaa !20
  %error_exit252 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %211, i32 0, i32 0
  %212 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit252, align 4, !tbaa !23
  %213 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %214 = bitcast %struct.jpeg_compress_struct* %213 to %struct.jpeg_common_struct*
  call void %212(%struct.jpeg_common_struct* %214)
  br label %if.end253

if.end253:                                        ; preds = %if.then248, %sw.bb245
  %215 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space254 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %215, i32 0, i32 10
  %216 = load i32, i32* %in_color_space254, align 8, !tbaa !18
  %cmp255 = icmp eq i32 %216, 4
  br i1 %cmp255, label %if.then256, label %if.else259

if.then256:                                       ; preds = %if.end253
  %217 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub257 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %217, i32 0, i32 0
  %color_convert258 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub257, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @null_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert258, align 4, !tbaa !27
  br label %if.end264

if.else259:                                       ; preds = %if.end253
  %218 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err260 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %218, i32 0, i32 0
  %219 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err260, align 8, !tbaa !20
  %msg_code261 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %219, i32 0, i32 5
  store i32 27, i32* %msg_code261, align 4, !tbaa !21
  %220 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err262 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %220, i32 0, i32 0
  %221 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err262, align 8, !tbaa !20
  %error_exit263 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %221, i32 0, i32 0
  %222 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit263, align 4, !tbaa !23
  %223 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %224 = bitcast %struct.jpeg_compress_struct* %223 to %struct.jpeg_common_struct*
  call void %222(%struct.jpeg_common_struct* %224)
  br label %if.end264

if.end264:                                        ; preds = %if.else259, %if.then256
  br label %sw.epilog311

sw.bb265:                                         ; preds = %sw.epilog
  %225 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components266 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %225, i32 0, i32 13
  %226 = load i32, i32* %num_components266, align 4, !tbaa !26
  %cmp267 = icmp ne i32 %226, 4
  br i1 %cmp267, label %if.then268, label %if.end273

if.then268:                                       ; preds = %sw.bb265
  %227 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err269 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %227, i32 0, i32 0
  %228 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err269, align 8, !tbaa !20
  %msg_code270 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %228, i32 0, i32 5
  store i32 10, i32* %msg_code270, align 4, !tbaa !21
  %229 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err271 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %229, i32 0, i32 0
  %230 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err271, align 8, !tbaa !20
  %error_exit272 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %230, i32 0, i32 0
  %231 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit272, align 4, !tbaa !23
  %232 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %233 = bitcast %struct.jpeg_compress_struct* %232 to %struct.jpeg_common_struct*
  call void %231(%struct.jpeg_common_struct* %233)
  br label %if.end273

if.end273:                                        ; preds = %if.then268, %sw.bb265
  %234 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space274 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %234, i32 0, i32 10
  %235 = load i32, i32* %in_color_space274, align 8, !tbaa !18
  %cmp275 = icmp eq i32 %235, 4
  br i1 %cmp275, label %if.then276, label %if.else282

if.then276:                                       ; preds = %if.end273
  %236 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub277 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %236, i32 0, i32 0
  %start_pass278 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub277, i32 0, i32 0
  %start_pass279 = bitcast {}** %start_pass278 to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @rgb_ycc_start, void (%struct.jpeg_compress_struct*)** %start_pass279, align 4, !tbaa !15
  %237 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub280 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %237, i32 0, i32 0
  %color_convert281 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub280, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @cmyk_ycck_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert281, align 4, !tbaa !27
  br label %if.end294

if.else282:                                       ; preds = %if.end273
  %238 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space283 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %238, i32 0, i32 10
  %239 = load i32, i32* %in_color_space283, align 8, !tbaa !18
  %cmp284 = icmp eq i32 %239, 5
  br i1 %cmp284, label %if.then285, label %if.else288

if.then285:                                       ; preds = %if.else282
  %240 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub286 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %240, i32 0, i32 0
  %color_convert287 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub286, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @null_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert287, align 4, !tbaa !27
  br label %if.end293

if.else288:                                       ; preds = %if.else282
  %241 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err289 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %241, i32 0, i32 0
  %242 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err289, align 8, !tbaa !20
  %msg_code290 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %242, i32 0, i32 5
  store i32 27, i32* %msg_code290, align 4, !tbaa !21
  %243 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err291 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %243, i32 0, i32 0
  %244 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err291, align 8, !tbaa !20
  %error_exit292 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %244, i32 0, i32 0
  %245 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit292, align 4, !tbaa !23
  %246 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %247 = bitcast %struct.jpeg_compress_struct* %246 to %struct.jpeg_common_struct*
  call void %245(%struct.jpeg_common_struct* %247)
  br label %if.end293

if.end293:                                        ; preds = %if.else288, %if.then285
  br label %if.end294

if.end294:                                        ; preds = %if.end293, %if.then276
  br label %sw.epilog311

sw.default295:                                    ; preds = %sw.epilog
  %248 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space296 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %248, i32 0, i32 14
  %249 = load i32, i32* %jpeg_color_space296, align 8, !tbaa !25
  %250 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space297 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %250, i32 0, i32 10
  %251 = load i32, i32* %in_color_space297, align 8, !tbaa !18
  %cmp298 = icmp ne i32 %249, %251
  br i1 %cmp298, label %if.then303, label %lor.lhs.false299

lor.lhs.false299:                                 ; preds = %sw.default295
  %252 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components300 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %252, i32 0, i32 13
  %253 = load i32, i32* %num_components300, align 4, !tbaa !26
  %254 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components301 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %254, i32 0, i32 9
  %255 = load i32, i32* %input_components301, align 4, !tbaa !19
  %cmp302 = icmp ne i32 %253, %255
  br i1 %cmp302, label %if.then303, label %if.end308

if.then303:                                       ; preds = %lor.lhs.false299, %sw.default295
  %256 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err304 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %256, i32 0, i32 0
  %257 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err304, align 8, !tbaa !20
  %msg_code305 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %257, i32 0, i32 5
  store i32 27, i32* %msg_code305, align 4, !tbaa !21
  %258 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err306 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %258, i32 0, i32 0
  %259 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err306, align 8, !tbaa !20
  %error_exit307 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %259, i32 0, i32 0
  %260 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit307, align 4, !tbaa !23
  %261 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %262 = bitcast %struct.jpeg_compress_struct* %261 to %struct.jpeg_common_struct*
  call void %260(%struct.jpeg_common_struct* %262)
  br label %if.end308

if.end308:                                        ; preds = %if.then303, %lor.lhs.false299
  %263 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %pub309 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %263, i32 0, i32 0
  %color_convert310 = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %pub309, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* @null_convert, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert310, align 4, !tbaa !27
  br label %sw.epilog311

sw.epilog311:                                     ; preds = %if.end308, %if.end294, %if.end264, %if.end244, %if.end177, %if.end108
  %264 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @null_method(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @grayscale_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %instride = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 7
  %5 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %5, i32* %num_cols, align 4, !tbaa !24
  %6 = bitcast i32* %instride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 9
  %8 = load i32, i32* %input_components, align 4, !tbaa !19
  store i32 %8, i32* %instride, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %9, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %10, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %10, align 4, !tbaa !2
  store i8* %11, i8** %inptr, align 4, !tbaa !2
  %12 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %12, i32 0
  %13 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %14 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %13, i32 %14
  %15 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %15, i8** %outptr, align 4, !tbaa !2
  %16 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %16, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %17 = load i32, i32* %col, align 4, !tbaa !24
  %18 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp2 = icmp ult i32 %17, %18
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %19, i32 0
  %20 = load i8, i8* %arrayidx3, align 1, !tbaa !29
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %22 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8, i8* %21, i32 %22
  store i8 %20, i8* %arrayidx4, align 1, !tbaa !29
  %23 = load i32, i32* %instride, align 4, !tbaa !24
  %24 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %24, i32 %23
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %col, align 4, !tbaa !24
  %inc5 = add i32 %25, 1
  store i32 %inc5, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %26 = bitcast i32* %instride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %28 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %30 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  ret void
}

declare i32 @jsimd_can_rgb_gray() #2

declare void @jsimd_rgb_gray_convert(%struct.jpeg_compress_struct*, i8**, i8***, i32, i32) #2

; Function Attrs: nounwind
define internal void @rgb_ycc_start(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %rgb_ycc_tab = alloca i32*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32** %rgb_ycc_tab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %7, i32 0, i32 0
  %8 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_compress_struct* %9 to %struct.jpeg_common_struct*
  %call = call i8* %8(%struct.jpeg_common_struct* %10, i32 1, i32 8192)
  %11 = bitcast i8* %call to i32*
  store i32* %11, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %12 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab2 = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %12, i32 0, i32 1
  store i32* %11, i32** %rgb_ycc_tab2, align 4, !tbaa !30
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !31
  %cmp = icmp sle i32 %13, 255
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %i, align 4, !tbaa !31
  %mul = mul nsw i32 19595, %14
  %15 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !31
  %add = add nsw i32 %16, 0
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 %add
  store i32 %mul, i32* %arrayidx, align 4, !tbaa !31
  %17 = load i32, i32* %i, align 4, !tbaa !31
  %mul3 = mul nsw i32 38470, %17
  %18 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !31
  %add4 = add nsw i32 %19, 256
  %arrayidx5 = getelementptr inbounds i32, i32* %18, i32 %add4
  store i32 %mul3, i32* %arrayidx5, align 4, !tbaa !31
  %20 = load i32, i32* %i, align 4, !tbaa !31
  %mul6 = mul nsw i32 7471, %20
  %add7 = add nsw i32 %mul6, 32768
  %21 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !31
  %add8 = add nsw i32 %22, 512
  %arrayidx9 = getelementptr inbounds i32, i32* %21, i32 %add8
  store i32 %add7, i32* %arrayidx9, align 4, !tbaa !31
  %23 = load i32, i32* %i, align 4, !tbaa !31
  %mul10 = mul nsw i32 -11059, %23
  %24 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !31
  %add11 = add nsw i32 %25, 768
  %arrayidx12 = getelementptr inbounds i32, i32* %24, i32 %add11
  store i32 %mul10, i32* %arrayidx12, align 4, !tbaa !31
  %26 = load i32, i32* %i, align 4, !tbaa !31
  %mul13 = mul nsw i32 -21709, %26
  %27 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %28 = load i32, i32* %i, align 4, !tbaa !31
  %add14 = add nsw i32 %28, 1024
  %arrayidx15 = getelementptr inbounds i32, i32* %27, i32 %add14
  store i32 %mul13, i32* %arrayidx15, align 4, !tbaa !31
  %29 = load i32, i32* %i, align 4, !tbaa !31
  %mul16 = mul nsw i32 32768, %29
  %add17 = add nsw i32 %mul16, 8388608
  %add18 = add nsw i32 %add17, 32768
  %sub = sub nsw i32 %add18, 1
  %30 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !31
  %add19 = add nsw i32 %31, 1280
  %arrayidx20 = getelementptr inbounds i32, i32* %30, i32 %add19
  store i32 %sub, i32* %arrayidx20, align 4, !tbaa !31
  %32 = load i32, i32* %i, align 4, !tbaa !31
  %mul21 = mul nsw i32 -27439, %32
  %33 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !31
  %add22 = add nsw i32 %34, 1536
  %arrayidx23 = getelementptr inbounds i32, i32* %33, i32 %add22
  store i32 %mul21, i32* %arrayidx23, align 4, !tbaa !31
  %35 = load i32, i32* %i, align 4, !tbaa !31
  %mul24 = mul nsw i32 -5329, %35
  %36 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !31
  %add25 = add nsw i32 %37, 1792
  %arrayidx26 = getelementptr inbounds i32, i32* %36, i32 %add25
  store i32 %mul24, i32* %arrayidx26, align 4, !tbaa !31
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %38 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add nsw i32 %38, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i32** %rgb_ycc_tab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_gray_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 10
  %1 = load i32, i32* %in_color_space, align 8, !tbaa !18
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %4 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %5 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgb_gray_convert_internal(%struct.jpeg_compress_struct* %2, i8** %3, i8*** %4, i32 %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %9 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %10 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgbx_gray_convert_internal(%struct.jpeg_compress_struct* %7, i8** %8, i8*** %9, i32 %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %14 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %15 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgr_gray_convert_internal(%struct.jpeg_compress_struct* %12, i8** %13, i8*** %14, i32 %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %20 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgrx_gray_convert_internal(%struct.jpeg_compress_struct* %17, i8** %18, i8*** %19, i32 %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %24 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %25 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxbgr_gray_convert_internal(%struct.jpeg_compress_struct* %22, i8** %23, i8*** %24, i32 %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxrgb_gray_convert_internal(%struct.jpeg_compress_struct* %27, i8** %28, i8*** %29, i32 %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %34 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %35 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @rgb_gray_convert_internal(%struct.jpeg_compress_struct* %32, i8** %33, i8*** %34, i32 %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @null_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %outptr3 = alloca i8*, align 4
  %col = alloca i32, align 4
  %ci = alloca i32, align 4
  %nc = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i8** %outptr3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 13
  %10 = load i32, i32* %num_components, align 4, !tbaa !26
  store i32 %10, i32* %nc, align 4, !tbaa !24
  %11 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 7
  %13 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %13, i32* %num_cols, align 4, !tbaa !24
  %14 = load i32, i32* %nc, align 4, !tbaa !24
  %cmp = icmp eq i32 %14, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.then
  %15 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp1 = icmp sge i32 %dec, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %16 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %16, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %17 = load i8*, i8** %16, align 4, !tbaa !2
  store i8* %17, i8** %inptr, align 4, !tbaa !2
  %18 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %18, i32 0
  %19 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %20 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %19, i32 %20
  %21 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %21, i8** %outptr0, align 4, !tbaa !2
  %22 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %22, i32 1
  %23 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %24 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %23, i32 %24
  %25 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %25, i8** %outptr1, align 4, !tbaa !2
  %26 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %26, i32 2
  %27 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %28 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %27, i32 %28
  %29 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %29, i8** %outptr2, align 4, !tbaa !2
  %30 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %30, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %31 = load i32, i32* %col, align 4, !tbaa !24
  %32 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %31, %32
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %33, i32 1
  store i8* %incdec.ptr8, i8** %inptr, align 4, !tbaa !2
  %34 = load i8, i8* %33, align 1, !tbaa !29
  %35 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %36 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %35, i32 %36
  store i8 %34, i8* %arrayidx9, align 1, !tbaa !29
  %37 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr10, i8** %inptr, align 4, !tbaa !2
  %38 = load i8, i8* %37, align 1, !tbaa !29
  %39 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %40 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx11 = getelementptr inbounds i8, i8* %39, i32 %40
  store i8 %38, i8* %arrayidx11, align 1, !tbaa !29
  %41 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr12, i8** %inptr, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !29
  %43 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %43, i32 %44
  store i8 %42, i8* %arrayidx13, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %45 = load i32, i32* %col, align 4, !tbaa !24
  %inc14 = add i32 %45, 1
  store i32 %inc14, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end70

if.else:                                          ; preds = %entry
  %46 = load i32, i32* %nc, align 4, !tbaa !24
  %cmp15 = icmp eq i32 %46, 4
  br i1 %cmp15, label %if.then16, label %if.else46

if.then16:                                        ; preds = %if.else
  br label %while.cond17

while.cond17:                                     ; preds = %for.end44, %if.then16
  %47 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec18 = add nsw i32 %47, -1
  store i32 %dec18, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp19 = icmp sge i32 %dec18, 0
  br i1 %cmp19, label %while.body20, label %while.end45

while.body20:                                     ; preds = %while.cond17
  %48 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i8*, i8** %48, i32 1
  store i8** %incdec.ptr21, i8*** %input_buf.addr, align 4, !tbaa !2
  %49 = load i8*, i8** %48, align 4, !tbaa !2
  store i8* %49, i8** %inptr, align 4, !tbaa !2
  %50 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8**, i8*** %50, i32 0
  %51 = load i8**, i8*** %arrayidx22, align 4, !tbaa !2
  %52 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx23 = getelementptr inbounds i8*, i8** %51, i32 %52
  %53 = load i8*, i8** %arrayidx23, align 4, !tbaa !2
  store i8* %53, i8** %outptr0, align 4, !tbaa !2
  %54 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8**, i8*** %54, i32 1
  %55 = load i8**, i8*** %arrayidx24, align 4, !tbaa !2
  %56 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx25 = getelementptr inbounds i8*, i8** %55, i32 %56
  %57 = load i8*, i8** %arrayidx25, align 4, !tbaa !2
  store i8* %57, i8** %outptr1, align 4, !tbaa !2
  %58 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8**, i8*** %58, i32 2
  %59 = load i8**, i8*** %arrayidx26, align 4, !tbaa !2
  %60 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx27 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load i8*, i8** %arrayidx27, align 4, !tbaa !2
  store i8* %61, i8** %outptr2, align 4, !tbaa !2
  %62 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i8**, i8*** %62, i32 3
  %63 = load i8**, i8*** %arrayidx28, align 4, !tbaa !2
  %64 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx29 = getelementptr inbounds i8*, i8** %63, i32 %64
  %65 = load i8*, i8** %arrayidx29, align 4, !tbaa !2
  store i8* %65, i8** %outptr3, align 4, !tbaa !2
  %66 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc30 = add i32 %66, 1
  store i32 %inc30, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc42, %while.body20
  %67 = load i32, i32* %col, align 4, !tbaa !24
  %68 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp32 = icmp ult i32 %67, %68
  br i1 %cmp32, label %for.body33, label %for.end44

for.body33:                                       ; preds = %for.cond31
  %69 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr34 = getelementptr inbounds i8, i8* %69, i32 1
  store i8* %incdec.ptr34, i8** %inptr, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !29
  %71 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %72 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx35 = getelementptr inbounds i8, i8* %71, i32 %72
  store i8 %70, i8* %arrayidx35, align 1, !tbaa !29
  %73 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %73, i32 1
  store i8* %incdec.ptr36, i8** %inptr, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !29
  %75 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx37 = getelementptr inbounds i8, i8* %75, i32 %76
  store i8 %74, i8* %arrayidx37, align 1, !tbaa !29
  %77 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %77, i32 1
  store i8* %incdec.ptr38, i8** %inptr, align 4, !tbaa !2
  %78 = load i8, i8* %77, align 1, !tbaa !29
  %79 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %80 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx39 = getelementptr inbounds i8, i8* %79, i32 %80
  store i8 %78, i8* %arrayidx39, align 1, !tbaa !29
  %81 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr40 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr40, i8** %inptr, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !29
  %83 = load i8*, i8** %outptr3, align 4, !tbaa !2
  %84 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx41 = getelementptr inbounds i8, i8* %83, i32 %84
  store i8 %82, i8* %arrayidx41, align 1, !tbaa !29
  br label %for.inc42

for.inc42:                                        ; preds = %for.body33
  %85 = load i32, i32* %col, align 4, !tbaa !24
  %inc43 = add i32 %85, 1
  store i32 %inc43, i32* %col, align 4, !tbaa !24
  br label %for.cond31

for.end44:                                        ; preds = %for.cond31
  br label %while.cond17

while.end45:                                      ; preds = %while.cond17
  br label %if.end

if.else46:                                        ; preds = %if.else
  br label %while.cond47

while.cond47:                                     ; preds = %for.end66, %if.else46
  %86 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec48 = add nsw i32 %86, -1
  store i32 %dec48, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp49 = icmp sge i32 %dec48, 0
  br i1 %cmp49, label %while.body50, label %while.end69

while.body50:                                     ; preds = %while.cond47
  store i32 0, i32* %ci, align 4, !tbaa !24
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc64, %while.body50
  %87 = load i32, i32* %ci, align 4, !tbaa !24
  %88 = load i32, i32* %nc, align 4, !tbaa !24
  %cmp52 = icmp slt i32 %87, %88
  br i1 %cmp52, label %for.body53, label %for.end66

for.body53:                                       ; preds = %for.cond51
  %89 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %90 = load i8*, i8** %89, align 4, !tbaa !2
  store i8* %90, i8** %inptr, align 4, !tbaa !2
  %91 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %92 = load i32, i32* %ci, align 4, !tbaa !24
  %arrayidx54 = getelementptr inbounds i8**, i8*** %91, i32 %92
  %93 = load i8**, i8*** %arrayidx54, align 4, !tbaa !2
  %94 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx55 = getelementptr inbounds i8*, i8** %93, i32 %94
  %95 = load i8*, i8** %arrayidx55, align 4, !tbaa !2
  store i8* %95, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc61, %for.body53
  %96 = load i32, i32* %col, align 4, !tbaa !24
  %97 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp57 = icmp ult i32 %96, %97
  br i1 %cmp57, label %for.body58, label %for.end63

for.body58:                                       ; preds = %for.cond56
  %98 = load i8*, i8** %inptr, align 4, !tbaa !2
  %99 = load i32, i32* %ci, align 4, !tbaa !24
  %arrayidx59 = getelementptr inbounds i8, i8* %98, i32 %99
  %100 = load i8, i8* %arrayidx59, align 1, !tbaa !29
  %101 = load i8*, i8** %outptr, align 4, !tbaa !2
  %102 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx60 = getelementptr inbounds i8, i8* %101, i32 %102
  store i8 %100, i8* %arrayidx60, align 1, !tbaa !29
  %103 = load i32, i32* %nc, align 4, !tbaa !24
  %104 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %104, i32 %103
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc61

for.inc61:                                        ; preds = %for.body58
  %105 = load i32, i32* %col, align 4, !tbaa !24
  %inc62 = add i32 %105, 1
  store i32 %inc62, i32* %col, align 4, !tbaa !24
  br label %for.cond56

for.end63:                                        ; preds = %for.cond56
  br label %for.inc64

for.inc64:                                        ; preds = %for.end63
  %106 = load i32, i32* %ci, align 4, !tbaa !24
  %inc65 = add nsw i32 %106, 1
  store i32 %inc65, i32* %ci, align 4, !tbaa !24
  br label %for.cond51

for.end66:                                        ; preds = %for.cond51
  %107 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr67 = getelementptr inbounds i8*, i8** %107, i32 1
  store i8** %incdec.ptr67, i8*** %input_buf.addr, align 4, !tbaa !2
  %108 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc68 = add i32 %108, 1
  store i32 %inc68, i32* %output_row.addr, align 4, !tbaa !24
  br label %while.cond47

while.end69:                                      ; preds = %while.cond47
  br label %if.end

if.end:                                           ; preds = %while.end69, %while.end45
  br label %if.end70

if.end70:                                         ; preds = %if.end, %while.end
  %109 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i8** %outptr3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %114 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  %115 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  %116 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #4
  %117 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  %118 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #4
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_rgb_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 10
  %1 = load i32, i32* %in_color_space, align 8, !tbaa !18
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %4 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %5 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgb_rgb_convert_internal(%struct.jpeg_compress_struct* %2, i8** %3, i8*** %4, i32 %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %9 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %10 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgbx_rgb_convert_internal(%struct.jpeg_compress_struct* %7, i8** %8, i8*** %9, i32 %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %14 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %15 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgr_rgb_convert_internal(%struct.jpeg_compress_struct* %12, i8** %13, i8*** %14, i32 %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %20 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgrx_rgb_convert_internal(%struct.jpeg_compress_struct* %17, i8** %18, i8*** %19, i32 %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %24 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %25 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxbgr_rgb_convert_internal(%struct.jpeg_compress_struct* %22, i8** %23, i8*** %24, i32 %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxrgb_rgb_convert_internal(%struct.jpeg_compress_struct* %27, i8** %28, i8*** %29, i32 %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %34 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %35 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @rgb_rgb_convert_internal(%struct.jpeg_compress_struct* %32, i8** %33, i8*** %34, i32 %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

declare i32 @jsimd_can_rgb_ycc() #2

declare void @jsimd_rgb_ycc_convert(%struct.jpeg_compress_struct*, i8**, i8***, i32, i32) #2

; Function Attrs: nounwind
define internal void @rgb_ycc_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 10
  %1 = load i32, i32* %in_color_space, align 8, !tbaa !18
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %4 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %5 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgb_ycc_convert_internal(%struct.jpeg_compress_struct* %2, i8** %3, i8*** %4, i32 %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %9 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %10 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extrgbx_ycc_convert_internal(%struct.jpeg_compress_struct* %7, i8** %8, i8*** %9, i32 %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %14 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %15 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgr_ycc_convert_internal(%struct.jpeg_compress_struct* %12, i8** %13, i8*** %14, i32 %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %20 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extbgrx_ycc_convert_internal(%struct.jpeg_compress_struct* %17, i8** %18, i8*** %19, i32 %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %24 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %25 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxbgr_ycc_convert_internal(%struct.jpeg_compress_struct* %22, i8** %23, i8*** %24, i32 %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @extxrgb_ycc_convert_internal(%struct.jpeg_compress_struct* %27, i8** %28, i8*** %29, i32 %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %34 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %35 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  call void @rgb_ycc_convert_internal(%struct.jpeg_compress_struct* %32, i8** %33, i8*** %34, i32 %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @cmyk_ycck_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %outptr3 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %outptr3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 7
  %18 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %18, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %19 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %19, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %20, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %21 = load i8*, i8** %20, align 4, !tbaa !2
  store i8* %21, i8** %inptr, align 4, !tbaa !2
  %22 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %22, i32 0
  %23 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %24 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %23, i32 %24
  %25 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %25, i8** %outptr0, align 4, !tbaa !2
  %26 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %26, i32 1
  %27 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %28 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %27, i32 %28
  %29 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %29, i8** %outptr1, align 4, !tbaa !2
  %30 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %30, i32 2
  %31 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %32 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %33, i8** %outptr2, align 4, !tbaa !2
  %34 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8**, i8*** %34, i32 3
  %35 = load i8**, i8*** %arrayidx7, align 4, !tbaa !2
  %36 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx8 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %37, i8** %outptr3, align 4, !tbaa !2
  %38 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %38, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %40 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp9 = icmp ult i32 %39, %40
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %41 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %41, i32 0
  %42 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv = zext i8 %42 to i32
  %sub = sub nsw i32 255, %conv
  store i32 %sub, i32* %r, align 4, !tbaa !24
  %43 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %43, i32 1
  %44 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %44 to i32
  %sub13 = sub nsw i32 255, %conv12
  store i32 %sub13, i32* %g, align 4, !tbaa !24
  %45 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %45, i32 2
  %46 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %46 to i32
  %sub16 = sub nsw i32 255, %conv15
  store i32 %sub16, i32* %b, align 4, !tbaa !24
  %47 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i8, i8* %47, i32 3
  %48 = load i8, i8* %arrayidx17, align 1, !tbaa !29
  %49 = load i8*, i8** %outptr3, align 4, !tbaa !2
  %50 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx18 = getelementptr inbounds i8, i8* %49, i32 %50
  store i8 %48, i8* %arrayidx18, align 1, !tbaa !29
  %51 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %51, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %52 = load i32*, i32** %ctab, align 4, !tbaa !2
  %53 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %53, 0
  %arrayidx19 = getelementptr inbounds i32, i32* %52, i32 %add
  %54 = load i32, i32* %arrayidx19, align 4, !tbaa !31
  %55 = load i32*, i32** %ctab, align 4, !tbaa !2
  %56 = load i32, i32* %g, align 4, !tbaa !24
  %add20 = add nsw i32 %56, 256
  %arrayidx21 = getelementptr inbounds i32, i32* %55, i32 %add20
  %57 = load i32, i32* %arrayidx21, align 4, !tbaa !31
  %add22 = add nsw i32 %54, %57
  %58 = load i32*, i32** %ctab, align 4, !tbaa !2
  %59 = load i32, i32* %b, align 4, !tbaa !24
  %add23 = add nsw i32 %59, 512
  %arrayidx24 = getelementptr inbounds i32, i32* %58, i32 %add23
  %60 = load i32, i32* %arrayidx24, align 4, !tbaa !31
  %add25 = add nsw i32 %add22, %60
  %shr = ashr i32 %add25, 16
  %conv26 = trunc i32 %shr to i8
  %61 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %62 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx27 = getelementptr inbounds i8, i8* %61, i32 %62
  store i8 %conv26, i8* %arrayidx27, align 1, !tbaa !29
  %63 = load i32*, i32** %ctab, align 4, !tbaa !2
  %64 = load i32, i32* %r, align 4, !tbaa !24
  %add28 = add nsw i32 %64, 768
  %arrayidx29 = getelementptr inbounds i32, i32* %63, i32 %add28
  %65 = load i32, i32* %arrayidx29, align 4, !tbaa !31
  %66 = load i32*, i32** %ctab, align 4, !tbaa !2
  %67 = load i32, i32* %g, align 4, !tbaa !24
  %add30 = add nsw i32 %67, 1024
  %arrayidx31 = getelementptr inbounds i32, i32* %66, i32 %add30
  %68 = load i32, i32* %arrayidx31, align 4, !tbaa !31
  %add32 = add nsw i32 %65, %68
  %69 = load i32*, i32** %ctab, align 4, !tbaa !2
  %70 = load i32, i32* %b, align 4, !tbaa !24
  %add33 = add nsw i32 %70, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %69, i32 %add33
  %71 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %add35 = add nsw i32 %add32, %71
  %shr36 = ashr i32 %add35, 16
  %conv37 = trunc i32 %shr36 to i8
  %72 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %73 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx38 = getelementptr inbounds i8, i8* %72, i32 %73
  store i8 %conv37, i8* %arrayidx38, align 1, !tbaa !29
  %74 = load i32*, i32** %ctab, align 4, !tbaa !2
  %75 = load i32, i32* %r, align 4, !tbaa !24
  %add39 = add nsw i32 %75, 1280
  %arrayidx40 = getelementptr inbounds i32, i32* %74, i32 %add39
  %76 = load i32, i32* %arrayidx40, align 4, !tbaa !31
  %77 = load i32*, i32** %ctab, align 4, !tbaa !2
  %78 = load i32, i32* %g, align 4, !tbaa !24
  %add41 = add nsw i32 %78, 1536
  %arrayidx42 = getelementptr inbounds i32, i32* %77, i32 %add41
  %79 = load i32, i32* %arrayidx42, align 4, !tbaa !31
  %add43 = add nsw i32 %76, %79
  %80 = load i32*, i32** %ctab, align 4, !tbaa !2
  %81 = load i32, i32* %b, align 4, !tbaa !24
  %add44 = add nsw i32 %81, 1792
  %arrayidx45 = getelementptr inbounds i32, i32* %80, i32 %add44
  %82 = load i32, i32* %arrayidx45, align 4, !tbaa !31
  %add46 = add nsw i32 %add43, %82
  %shr47 = ashr i32 %add46, 16
  %conv48 = trunc i32 %shr47 to i8
  %83 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %84 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx49 = getelementptr inbounds i8, i8* %83, i32 %84
  store i8 %conv48, i8* %arrayidx49, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %85 = load i32, i32* %col, align 4, !tbaa !24
  %inc50 = add i32 %85, 1
  store i32 %inc50, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %86 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %outptr3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: alwaysinline nounwind
define internal void @extrgb_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 1
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 2
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgbx_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 1
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 2
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgr_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 2
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 1
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 0
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgrx_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 2
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 1
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 0
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxbgr_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 3
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 2
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxrgb_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 1
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 2
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 3
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_gray_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 7
  %15 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %15, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %16, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %17, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %17, align 4, !tbaa !2
  store i8* %18, i8** %inptr, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %outptr, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp3 = icmp ult i32 %24, %25
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx4, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 1
  %29 = load i8, i8* %arrayidx5, align 1, !tbaa !29
  %conv6 = zext i8 %29 to i32
  store i32 %conv6, i32* %g, align 4, !tbaa !24
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %30, i32 2
  %31 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv8 = zext i8 %31 to i32
  store i32 %conv8, i32* %b, align 4, !tbaa !24
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %33 = load i32*, i32** %ctab, align 4, !tbaa !2
  %34 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %34, 0
  %arrayidx9 = getelementptr inbounds i32, i32* %33, i32 %add
  %35 = load i32, i32* %arrayidx9, align 4, !tbaa !31
  %36 = load i32*, i32** %ctab, align 4, !tbaa !2
  %37 = load i32, i32* %g, align 4, !tbaa !24
  %add10 = add nsw i32 %37, 256
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 %add10
  %38 = load i32, i32* %arrayidx11, align 4, !tbaa !31
  %add12 = add nsw i32 %35, %38
  %39 = load i32*, i32** %ctab, align 4, !tbaa !2
  %40 = load i32, i32* %b, align 4, !tbaa !24
  %add13 = add nsw i32 %40, 512
  %arrayidx14 = getelementptr inbounds i32, i32* %39, i32 %add13
  %41 = load i32, i32* %arrayidx14, align 4, !tbaa !31
  %add15 = add nsw i32 %add12, %41
  %shr = ashr i32 %add15, 16
  %conv16 = trunc i32 %shr to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %44, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %48 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgb_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 2
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgbx_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 2
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgr_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 2
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 0
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgrx_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 2
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 0
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxbgr_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 3
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 2
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 1
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxrgb_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 1
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 2
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 3
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %7, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %9, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %9, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %11, i32 0
  %12 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %13 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx1 = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %14, i8** %outptr0, align 4, !tbaa !2
  %15 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %15, i32 1
  %16 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %18, i8** %outptr1, align 4, !tbaa !2
  %19 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %19, i32 2
  %20 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %21 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx5 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %22, i8** %outptr2, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %23, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !24
  %25 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 0
  %27 = load i8, i8* %arrayidx7, align 1, !tbaa !29
  %conv = zext i8 %27 to i32
  %conv8 = trunc i32 %conv to i8
  %28 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %29 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx9 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !29
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 1
  %31 = load i8, i8* %arrayidx10, align 1, !tbaa !29
  %conv11 = zext i8 %31 to i32
  %conv12 = trunc i32 %conv11 to i8
  %32 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %33 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx13 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %conv12, i8* %arrayidx13, align 1, !tbaa !29
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 2
  %35 = load i8, i8* %arrayidx14, align 1, !tbaa !29
  %conv15 = zext i8 %35 to i32
  %conv16 = trunc i32 %conv15 to i8
  %36 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx17 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !29
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !24
  %inc18 = add i32 %39, 1
  store i32 %inc18, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgb_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 0
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 1
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 2
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgbx_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 0
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 1
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 2
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgr_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 2
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 1
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 0
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgrx_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 2
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 1
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 0
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxbgr_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 3
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 2
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 1
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxrgb_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 1
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 2
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 3
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 4
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_ycc_convert_internal(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_converter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %inptr = alloca i8*, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %outptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !24
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !24
  %0 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 59
  %2 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_converter* %2 to %struct.my_color_converter*
  store %struct.my_color_converter* %3, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_converter*, %struct.my_color_converter** %cconvert, align 4, !tbaa !2
  %rgb_ycc_tab = getelementptr inbounds %struct.my_color_converter, %struct.my_color_converter* %8, i32 0, i32 1
  %9 = load i32*, i32** %rgb_ycc_tab, align 4, !tbaa !30
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 7
  %17 = load i32, i32* %image_width, align 4, !tbaa !28
  store i32 %17, i32* %num_cols, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !24
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !24
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %19, i32 1
  store i8** %incdec.ptr, i8*** %input_buf.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %19, align 4, !tbaa !2
  store i8* %20, i8** %inptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %21, i32 0
  %22 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %23 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %outptr0, align 4, !tbaa !2
  %25 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %25, i32 1
  %26 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %27 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx4 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %28, i8** %outptr1, align 4, !tbaa !2
  %29 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 2
  %30 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %31 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %arrayidx6 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %32, i8** %outptr2, align 4, !tbaa !2
  %33 = load i32, i32* %output_row.addr, align 4, !tbaa !24
  %inc = add i32 %33, 1
  store i32 %inc, i32* %output_row.addr, align 4, !tbaa !24
  store i32 0, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !24
  %35 = load i32, i32* %num_cols, align 4, !tbaa !24
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 0
  %37 = load i8, i8* %arrayidx8, align 1, !tbaa !29
  %conv = zext i8 %37 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !24
  %38 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %38, i32 1
  %39 = load i8, i8* %arrayidx9, align 1, !tbaa !29
  %conv10 = zext i8 %39 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !24
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8, i8* %40, i32 2
  %41 = load i8, i8* %arrayidx11, align 1, !tbaa !29
  %conv12 = zext i8 %41 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !24
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 3
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %43 = load i32*, i32** %ctab, align 4, !tbaa !2
  %44 = load i32, i32* %r, align 4, !tbaa !24
  %add = add nsw i32 %44, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 %add
  %45 = load i32, i32* %arrayidx13, align 4, !tbaa !31
  %46 = load i32*, i32** %ctab, align 4, !tbaa !2
  %47 = load i32, i32* %g, align 4, !tbaa !24
  %add14 = add nsw i32 %47, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %46, i32 %add14
  %48 = load i32, i32* %arrayidx15, align 4, !tbaa !31
  %add16 = add nsw i32 %45, %48
  %49 = load i32*, i32** %ctab, align 4, !tbaa !2
  %50 = load i32, i32* %b, align 4, !tbaa !24
  %add17 = add nsw i32 %50, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %49, i32 %add17
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !31
  %add19 = add nsw i32 %add16, %51
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %52 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx21 = getelementptr inbounds i8, i8* %52, i32 %53
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !29
  %54 = load i32*, i32** %ctab, align 4, !tbaa !2
  %55 = load i32, i32* %r, align 4, !tbaa !24
  %add22 = add nsw i32 %55, 768
  %arrayidx23 = getelementptr inbounds i32, i32* %54, i32 %add22
  %56 = load i32, i32* %arrayidx23, align 4, !tbaa !31
  %57 = load i32*, i32** %ctab, align 4, !tbaa !2
  %58 = load i32, i32* %g, align 4, !tbaa !24
  %add24 = add nsw i32 %58, 1024
  %arrayidx25 = getelementptr inbounds i32, i32* %57, i32 %add24
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !31
  %add26 = add nsw i32 %56, %59
  %60 = load i32*, i32** %ctab, align 4, !tbaa !2
  %61 = load i32, i32* %b, align 4, !tbaa !24
  %add27 = add nsw i32 %61, 1280
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %add27
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !31
  %add29 = add nsw i32 %add26, %62
  %shr30 = ashr i32 %add29, 16
  %conv31 = trunc i32 %shr30 to i8
  %63 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %64 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx32 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %conv31, i8* %arrayidx32, align 1, !tbaa !29
  %65 = load i32*, i32** %ctab, align 4, !tbaa !2
  %66 = load i32, i32* %r, align 4, !tbaa !24
  %add33 = add nsw i32 %66, 1280
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 %add33
  %67 = load i32, i32* %arrayidx34, align 4, !tbaa !31
  %68 = load i32*, i32** %ctab, align 4, !tbaa !2
  %69 = load i32, i32* %g, align 4, !tbaa !24
  %add35 = add nsw i32 %69, 1536
  %arrayidx36 = getelementptr inbounds i32, i32* %68, i32 %add35
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !31
  %add37 = add nsw i32 %67, %70
  %71 = load i32*, i32** %ctab, align 4, !tbaa !2
  %72 = load i32, i32* %b, align 4, !tbaa !24
  %add38 = add nsw i32 %72, 1792
  %arrayidx39 = getelementptr inbounds i32, i32* %71, i32 %add38
  %73 = load i32, i32* %arrayidx39, align 4, !tbaa !31
  %add40 = add nsw i32 %add37, %73
  %shr41 = ashr i32 %add40, 16
  %conv42 = trunc i32 %shr41 to i8
  %74 = load i8*, i8** %outptr2, align 4, !tbaa !2
  %75 = load i32, i32* %col, align 4, !tbaa !24
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 %75
  store i8 %conv42, i8* %arrayidx43, align 1, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %col, align 4, !tbaa !24
  %inc44 = add i32 %76, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %78 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast i8** %outptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast %struct.my_color_converter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !9, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !10, i64 216, !10, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 352}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !3, i64 8}
!17 = !{!"jpeg_color_converter", !3, i64 0, !3, i64 4}
!18 = !{!7, !4, i64 40}
!19 = !{!7, !8, i64 36}
!20 = !{!7, !3, i64 0}
!21 = !{!22, !8, i64 20}
!22 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!23 = !{!22, !3, i64 0}
!24 = !{!8, !8, i64 0}
!25 = !{!7, !4, i64 64}
!26 = !{!7, !8, i64 60}
!27 = !{!16, !3, i64 4}
!28 = !{!7, !8, i64 28}
!29 = !{!4, !4, i64 0}
!30 = !{!16, !3, i64 8}
!31 = !{!13, !13, i64 0}
