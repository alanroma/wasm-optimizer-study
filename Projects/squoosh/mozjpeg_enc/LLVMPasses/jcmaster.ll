; ModuleID = 'jcmaster.c'
source_filename = "jcmaster.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, {}* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { {}*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_comp_master = type { %struct.jpeg_comp_master, i32, i32, i32, i32, i32, [64 x i8*], [64 x i32], [64 x i32], i32, i32, i32, i32, i32, i32, %struct.jpeg_destination_mgr*, i8* }

@.str = private unnamed_addr constant [39 x i8] c"mozjpeg version 3.3.1 (build 20201023)\00", align 1
@stderr = external constant %struct._IO_FILE*, align 4
@.str.1 = private unnamed_addr constant [6 x i8] c"SCAN \00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c"%s%d\00", align 1
@.str.3 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.4 = private unnamed_addr constant [2 x i8] c",\00", align 1
@.str.5 = private unnamed_addr constant [8 x i8] c": %d %d\00", align 1
@.str.6 = private unnamed_addr constant [7 x i8] c" %d %d\00", align 1
@.str.7 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1

; Function Attrs: nounwind
define hidden void @jinit_c_master_control(%struct.jpeg_compress_struct* %cinfo, i32 %transcode_only) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %transcode_only.addr = alloca i32, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %transcode_only, i32* %transcode_only.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %3 = bitcast %struct.jpeg_comp_master* %2 to %struct.my_comp_master*
  store %struct.my_comp_master* %3, %struct.my_comp_master** %master, align 4, !tbaa !2
  %4 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %4, i32 0, i32 0
  %prepare_for_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub, i32 0, i32 0
  store void (%struct.jpeg_compress_struct*)* @prepare_for_pass, void (%struct.jpeg_compress_struct*)** %prepare_for_pass, align 8, !tbaa !12
  %5 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %5, i32 0, i32 0
  %pass_startup = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub2, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*)* @pass_startup, void (%struct.jpeg_compress_struct*)** %pass_startup, align 4, !tbaa !17
  %6 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %6, i32 0, i32 0
  %finish_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub3, i32 0, i32 2
  store void (%struct.jpeg_compress_struct*)* @finish_pass_master, void (%struct.jpeg_compress_struct*)** %finish_pass, align 8, !tbaa !18
  %7 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %7, i32 0, i32 0
  %is_last_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub4, i32 0, i32 4
  store i32 0, i32* %is_last_pass, align 8, !tbaa !19
  %8 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %8, i32 0, i32 0
  %call_pass_startup = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub5, i32 0, i32 3
  store i32 0, i32* %call_pass_startup, align 4, !tbaa !20
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = load i32, i32* %transcode_only.addr, align 4, !tbaa !6
  call void @initial_setup(%struct.jpeg_compress_struct* %9, i32 %10)
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 23
  %12 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !21
  %cmp = icmp ne %struct.jpeg_scan_info* %12, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @validate_script(%struct.jpeg_compress_struct* %13)
  br label %if.end

if.else:                                          ; preds = %entry
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 40
  store i32 0, i32* %progressive_mode, align 4, !tbaa !22
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 22
  store i32 1, i32* %num_scans, align 8, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 40
  %17 = load i32, i32* %progressive_mode6, align 4, !tbaa !22
  %tobool = icmp ne i32 %17, 0
  br i1 %tobool, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %if.end
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 25
  %19 = load i32, i32* %arith_code, align 4, !tbaa !24
  %tobool7 = icmp ne i32 %19, 0
  br i1 %tobool7, label %if.end9, label %if.then8

if.then8:                                         ; preds = %land.lhs.true
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 26
  store i32 1, i32* %optimize_coding, align 8, !tbaa !25
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %land.lhs.true, %if.end
  %21 = load i32, i32* %transcode_only.addr, align 4, !tbaa !6
  %tobool10 = icmp ne i32 %21, 0
  br i1 %tobool10, label %if.then11, label %if.else18

if.then11:                                        ; preds = %if.end9
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 26
  %23 = load i32, i32* %optimize_coding12, align 8, !tbaa !25
  %tobool13 = icmp ne i32 %23, 0
  br i1 %tobool13, label %if.then14, label %if.else15

if.then14:                                        ; preds = %if.then11
  %24 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %24, i32 0, i32 1
  store i32 1, i32* %pass_type, align 8, !tbaa !26
  br label %if.end17

if.else15:                                        ; preds = %if.then11
  %25 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type16 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %25, i32 0, i32 1
  store i32 2, i32* %pass_type16, align 8, !tbaa !26
  br label %if.end17

if.end17:                                         ; preds = %if.else15, %if.then14
  br label %if.end20

if.else18:                                        ; preds = %if.end9
  %26 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type19 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %26, i32 0, i32 1
  store i32 0, i32* %pass_type19, align 8, !tbaa !26
  br label %if.end20

if.end20:                                         ; preds = %if.else18, %if.end17
  %27 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %27, i32 0, i32 4
  store i32 0, i32* %scan_number, align 4, !tbaa !27
  %28 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %28, i32 0, i32 2
  store i32 0, i32* %pass_number, align 4, !tbaa !28
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 26
  %30 = load i32, i32* %optimize_coding21, align 8, !tbaa !25
  %tobool22 = icmp ne i32 %30, 0
  br i1 %tobool22, label %if.then23, label %if.else25

if.then23:                                        ; preds = %if.end20
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 22
  %32 = load i32, i32* %num_scans24, align 8, !tbaa !23
  %mul = mul nsw i32 %32, 2
  %33 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %33, i32 0, i32 3
  store i32 %mul, i32* %total_passes, align 8, !tbaa !29
  br label %if.end28

if.else25:                                        ; preds = %if.end20
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans26 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 22
  %35 = load i32, i32* %num_scans26, align 8, !tbaa !23
  %36 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes27 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %36, i32 0, i32 3
  store i32 %35, i32* %total_passes27, align 8, !tbaa !29
  br label %if.end28

if.end28:                                         ; preds = %if.else25, %if.then23
  %37 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %jpeg_version = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %37, i32 0, i32 16
  store i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str, i32 0, i32 0), i8** %jpeg_version, align 8, !tbaa !30
  %38 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %38, i32 0, i32 5
  store i32 0, i32* %pass_number_scan_opt_base, align 8, !tbaa !31
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master29 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 54
  %40 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master29, align 4, !tbaa !8
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %40, i32 0, i32 6
  %41 = load i32, i32* %trellis_quant, align 8, !tbaa !32
  %tobool30 = icmp ne i32 %41, 0
  br i1 %tobool30, label %if.then31, label %if.end56

if.then31:                                        ; preds = %if.end28
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 26
  %43 = load i32, i32* %optimize_coding32, align 8, !tbaa !25
  %tobool33 = icmp ne i32 %43, 0
  br i1 %tobool33, label %if.then34, label %if.else41

if.then34:                                        ; preds = %if.then31
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 54
  %45 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master35, align 4, !tbaa !8
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %45, i32 0, i32 10
  %46 = load i32, i32* %use_scans_in_trellis, align 8, !tbaa !33
  %tobool36 = icmp ne i32 %46, 0
  %47 = zext i1 %tobool36 to i64
  %cond = select i1 %tobool36, i32 4, i32 2
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 13
  %49 = load i32, i32* %num_components, align 4, !tbaa !34
  %mul37 = mul nsw i32 %cond, %49
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 54
  %51 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master38, align 4, !tbaa !8
  %trellis_num_loops = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %51, i32 0, i32 20
  %52 = load i32, i32* %trellis_num_loops, align 8, !tbaa !35
  %mul39 = mul nsw i32 %mul37, %52
  %53 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base40 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %53, i32 0, i32 5
  store i32 %mul39, i32* %pass_number_scan_opt_base40, align 8, !tbaa !31
  br label %if.end52

if.else41:                                        ; preds = %if.then31
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master42 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 54
  %55 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master42, align 4, !tbaa !8
  %use_scans_in_trellis43 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %55, i32 0, i32 10
  %56 = load i32, i32* %use_scans_in_trellis43, align 8, !tbaa !33
  %tobool44 = icmp ne i32 %56, 0
  %57 = zext i1 %tobool44 to i64
  %cond45 = select i1 %tobool44, i32 2, i32 1
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 13
  %59 = load i32, i32* %num_components46, align 4, !tbaa !34
  %mul47 = mul nsw i32 %cond45, %59
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master48 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 54
  %61 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master48, align 4, !tbaa !8
  %trellis_num_loops49 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %61, i32 0, i32 20
  %62 = load i32, i32* %trellis_num_loops49, align 8, !tbaa !35
  %mul50 = mul nsw i32 %mul47, %62
  %add = add nsw i32 %mul50, 1
  %63 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base51 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %63, i32 0, i32 5
  store i32 %add, i32* %pass_number_scan_opt_base51, align 8, !tbaa !31
  br label %if.end52

if.end52:                                         ; preds = %if.else41, %if.then34
  %64 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base53 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %64, i32 0, i32 5
  %65 = load i32, i32* %pass_number_scan_opt_base53, align 8, !tbaa !31
  %66 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes54 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %66, i32 0, i32 3
  %67 = load i32, i32* %total_passes54, align 8, !tbaa !29
  %add55 = add nsw i32 %67, %65
  store i32 %add55, i32* %total_passes54, align 8, !tbaa !29
  br label %if.end56

if.end56:                                         ; preds = %if.end52, %if.end28
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 54
  %69 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master57, align 4, !tbaa !8
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %69, i32 0, i32 5
  %70 = load i32, i32* %optimize_scans, align 4, !tbaa !36
  %tobool58 = icmp ne i32 %70, 0
  br i1 %tobool58, label %if.then59, label %if.end62

if.then59:                                        ; preds = %if.end56
  %71 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %72, i32 0, i32 13
  store i32 0, i32* %best_Al_chroma, align 4, !tbaa !37
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then59
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 22
  %75 = load i32, i32* %num_scans60, align 8, !tbaa !23
  %cmp61 = icmp slt i32 %73, %75
  br i1 %cmp61, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %76 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_buffer = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %76, i32 0, i32 6
  %77 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x i8*], [64 x i8*]* %scan_buffer, i32 0, i32 %77
  store i8* null, i8** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %78 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %78, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %79 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  br label %if.end62

if.end62:                                         ; preds = %for.end, %if.end56
  %80 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @prepare_for_pass(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %3 = bitcast %struct.jpeg_comp_master* %2 to %struct.my_comp_master*
  store %struct.my_comp_master* %3, %struct.my_comp_master** %master, align 4, !tbaa !2
  %4 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %4, i32 0, i32 2
  %5 = load i32, i32* %pass_number, align 4, !tbaa !28
  %6 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %6, i32 0, i32 5
  %7 = load i32, i32* %pass_number_scan_opt_base, align 8, !tbaa !31
  %cmp = icmp slt i32 %5, %7
  %conv = zext i1 %cmp to i32
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 54
  %9 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !8
  %trellis_passes = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %9, i32 0, i32 11
  store i32 %conv, i32* %trellis_passes, align 4, !tbaa !38
  %10 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %10, i32 0, i32 1
  %11 = load i32, i32* %pass_type, align 8, !tbaa !26
  switch i32 %11, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb26
    i32 2, label %sw.bb46
    i32 3, label %sw.bb75
  ]

sw.bb:                                            ; preds = %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @select_scan_parameters(%struct.jpeg_compress_struct* %12)
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @per_scan_setup(%struct.jpeg_compress_struct* %13)
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_in = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 24
  %15 = load i32, i32* %raw_data_in, align 8, !tbaa !39
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %sw.bb
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 59
  %17 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert, align 8, !tbaa !40
  %start_pass = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %17, i32 0, i32 0
  %18 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %start_pass, align 4, !tbaa !41
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %18(%struct.jpeg_compress_struct* %19)
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 60
  %21 = load %struct.jpeg_downsampler*, %struct.jpeg_downsampler** %downsample, align 4, !tbaa !43
  %start_pass3 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %21, i32 0, i32 0
  %22 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %start_pass3, align 4, !tbaa !44
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %22(%struct.jpeg_compress_struct* %23)
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 56
  %25 = load %struct.jpeg_c_prep_controller*, %struct.jpeg_c_prep_controller** %prep, align 4, !tbaa !46
  %start_pass4 = getelementptr inbounds %struct.jpeg_c_prep_controller, %struct.jpeg_c_prep_controller* %25, i32 0, i32 0
  %26 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass4, align 4, !tbaa !47
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %26(%struct.jpeg_compress_struct* %27, i32 0)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %fdct = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 61
  %29 = load %struct.jpeg_forward_dct*, %struct.jpeg_forward_dct** %fdct, align 8, !tbaa !49
  %start_pass5 = getelementptr inbounds %struct.jpeg_forward_dct, %struct.jpeg_forward_dct* %29, i32 0, i32 0
  %30 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %start_pass5, align 4, !tbaa !50
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %30(%struct.jpeg_compress_struct* %31)
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 62
  %33 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy, align 4, !tbaa !52
  %start_pass6 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %33, i32 0, i32 0
  %start_pass7 = bitcast {}** %start_pass6 to void (%struct.jpeg_compress_struct*, i32)**
  %34 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass7, align 4, !tbaa !53
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 26
  %37 = load i32, i32* %optimize_coding, align 8, !tbaa !25
  %tobool8 = icmp ne i32 %37, 0
  br i1 %tobool8, label %land.rhs, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 54
  %39 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master9, align 4, !tbaa !8
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %39, i32 0, i32 6
  %40 = load i32, i32* %trellis_quant, align 8, !tbaa !32
  %tobool10 = icmp ne i32 %40, 0
  br i1 %tobool10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.lhs.false, %if.end
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 25
  %42 = load i32, i32* %arith_code, align 4, !tbaa !24
  %tobool11 = icmp ne i32 %42, 0
  %lnot = xor i1 %tobool11, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.lhs.false
  %43 = phi i1 [ false, %lor.lhs.false ], [ %lnot, %land.rhs ]
  %land.ext = zext i1 %43 to i32
  call void %34(%struct.jpeg_compress_struct* %35, i32 %land.ext)
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 57
  %45 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef, align 8, !tbaa !55
  %start_pass12 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %45, i32 0, i32 0
  %46 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass12, align 4, !tbaa !56
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %48 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %48, i32 0, i32 3
  %49 = load i32, i32* %total_passes, align 8, !tbaa !29
  %cmp13 = icmp sgt i32 %49, 1
  %50 = zext i1 %cmp13 to i64
  %cond = select i1 %cmp13, i32 3, i32 0
  call void %46(%struct.jpeg_compress_struct* %47, i32 %cond)
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 55
  %52 = load %struct.jpeg_c_main_controller*, %struct.jpeg_c_main_controller** %main, align 8, !tbaa !58
  %start_pass15 = getelementptr inbounds %struct.jpeg_c_main_controller, %struct.jpeg_c_main_controller* %52, i32 0, i32 0
  %53 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass15, align 4, !tbaa !59
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %53(%struct.jpeg_compress_struct* %54, i32 0)
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 26
  %56 = load i32, i32* %optimize_coding16, align 8, !tbaa !25
  %tobool17 = icmp ne i32 %56, 0
  br i1 %tobool17, label %if.then22, label %lor.lhs.false18

lor.lhs.false18:                                  ; preds = %land.end
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 54
  %58 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master19, align 4, !tbaa !8
  %trellis_quant20 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %58, i32 0, i32 6
  %59 = load i32, i32* %trellis_quant20, align 8, !tbaa !32
  %tobool21 = icmp ne i32 %59, 0
  br i1 %tobool21, label %if.then22, label %if.else

if.then22:                                        ; preds = %lor.lhs.false18, %land.end
  %60 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %60, i32 0, i32 0
  %call_pass_startup = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub, i32 0, i32 3
  store i32 0, i32* %call_pass_startup, align 4, !tbaa !20
  br label %if.end25

if.else:                                          ; preds = %lor.lhs.false18
  %61 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub23 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %61, i32 0, i32 0
  %call_pass_startup24 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub23, i32 0, i32 3
  store i32 1, i32* %call_pass_startup24, align 4, !tbaa !20
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then22
  br label %sw.epilog

sw.bb26:                                          ; preds = %entry
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @select_scan_parameters(%struct.jpeg_compress_struct* %62)
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @per_scan_setup(%struct.jpeg_compress_struct* %63)
  %64 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %64, i32 0, i32 50
  %65 = load i32, i32* %Ss, align 4, !tbaa !61
  %cmp27 = icmp ne i32 %65, 0
  br i1 %cmp27, label %if.then35, label %lor.lhs.false29

lor.lhs.false29:                                  ; preds = %sw.bb26
  %66 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %66, i32 0, i32 52
  %67 = load i32, i32* %Ah, align 4, !tbaa !62
  %cmp30 = icmp eq i32 %67, 0
  br i1 %cmp30, label %if.then35, label %lor.lhs.false32

lor.lhs.false32:                                  ; preds = %lor.lhs.false29
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 25
  %69 = load i32, i32* %arith_code33, align 4, !tbaa !24
  %tobool34 = icmp ne i32 %69, 0
  br i1 %tobool34, label %if.then35, label %if.end43

if.then35:                                        ; preds = %lor.lhs.false32, %lor.lhs.false29, %sw.bb26
  %70 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy36 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %70, i32 0, i32 62
  %71 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy36, align 4, !tbaa !52
  %start_pass37 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %71, i32 0, i32 0
  %start_pass38 = bitcast {}** %start_pass37 to void (%struct.jpeg_compress_struct*, i32)**
  %72 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass38, align 4, !tbaa !53
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %72(%struct.jpeg_compress_struct* %73, i32 1)
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef39 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 57
  %75 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef39, align 8, !tbaa !55
  %start_pass40 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %75, i32 0, i32 0
  %76 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass40, align 4, !tbaa !56
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %76(%struct.jpeg_compress_struct* %77, i32 2)
  %78 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub41 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %78, i32 0, i32 0
  %call_pass_startup42 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub41, i32 0, i32 3
  store i32 0, i32* %call_pass_startup42, align 4, !tbaa !20
  br label %sw.epilog

if.end43:                                         ; preds = %lor.lhs.false32
  %79 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type44 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %79, i32 0, i32 1
  store i32 2, i32* %pass_type44, align 8, !tbaa !26
  %80 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number45 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %80, i32 0, i32 2
  %81 = load i32, i32* %pass_number45, align 4, !tbaa !28
  %inc = add nsw i32 %81, 1
  store i32 %inc, i32* %pass_number45, align 4, !tbaa !28
  br label %sw.bb46

sw.bb46:                                          ; preds = %entry, %if.end43
  %82 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding47 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %82, i32 0, i32 26
  %83 = load i32, i32* %optimize_coding47, align 8, !tbaa !25
  %tobool48 = icmp ne i32 %83, 0
  br i1 %tobool48, label %if.end50, label %if.then49

if.then49:                                        ; preds = %sw.bb46
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @select_scan_parameters(%struct.jpeg_compress_struct* %84)
  %85 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @per_scan_setup(%struct.jpeg_compress_struct* %85)
  br label %if.end50

if.end50:                                         ; preds = %if.then49, %sw.bb46
  %86 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master51 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %86, i32 0, i32 54
  %87 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master51, align 4, !tbaa !8
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %87, i32 0, i32 5
  %88 = load i32, i32* %optimize_scans, align 4, !tbaa !36
  %tobool52 = icmp ne i32 %88, 0
  br i1 %tobool52, label %if.then53, label %if.end61

if.then53:                                        ; preds = %if.end50
  %89 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %89, i32 0, i32 6
  %90 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !63
  %91 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %saved_dest = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %91, i32 0, i32 15
  store %struct.jpeg_destination_mgr* %90, %struct.jpeg_destination_mgr** %saved_dest, align 4, !tbaa !64
  %92 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %92, i32 0, i32 6
  store %struct.jpeg_destination_mgr* null, %struct.jpeg_destination_mgr** %dest54, align 8, !tbaa !63
  %93 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %93, i32 0, i32 7
  %94 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %94, i32 0, i32 4
  %95 = load i32, i32* %scan_number, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size, i32 0, i32 %95
  store i32 0, i32* %arrayidx, align 4, !tbaa !65
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %97 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_buffer = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %97, i32 0, i32 6
  %98 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number55 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %98, i32 0, i32 4
  %99 = load i32, i32* %scan_number55, align 4, !tbaa !27
  %arrayidx56 = getelementptr inbounds [64 x i8*], [64 x i8*]* %scan_buffer, i32 0, i32 %99
  %100 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size57 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %100, i32 0, i32 7
  %101 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number58 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %101, i32 0, i32 4
  %102 = load i32, i32* %scan_number58, align 4, !tbaa !27
  %arrayidx59 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size57, i32 0, i32 %102
  call void @jpeg_mem_dest_internal(%struct.jpeg_compress_struct* %96, i8** %arrayidx56, i32* %arrayidx59, i32 1)
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 6
  %104 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest60, align 8, !tbaa !63
  %init_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %104, i32 0, i32 2
  %105 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %init_destination, align 4, !tbaa !66
  %106 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %105(%struct.jpeg_compress_struct* %106)
  br label %if.end61

if.end61:                                         ; preds = %if.then53, %if.end50
  %107 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy62 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %107, i32 0, i32 62
  %108 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy62, align 4, !tbaa !52
  %start_pass63 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %108, i32 0, i32 0
  %start_pass64 = bitcast {}** %start_pass63 to void (%struct.jpeg_compress_struct*, i32)**
  %109 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass64, align 4, !tbaa !53
  %110 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %109(%struct.jpeg_compress_struct* %110, i32 0)
  %111 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef65 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %111, i32 0, i32 57
  %112 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef65, align 8, !tbaa !55
  %start_pass66 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %112, i32 0, i32 0
  %113 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass66, align 4, !tbaa !56
  %114 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %113(%struct.jpeg_compress_struct* %114, i32 2)
  %115 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number67 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %115, i32 0, i32 4
  %116 = load i32, i32* %scan_number67, align 4, !tbaa !27
  %cmp68 = icmp eq i32 %116, 0
  br i1 %cmp68, label %if.then70, label %if.end71

if.then70:                                        ; preds = %if.end61
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 58
  %118 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !68
  %write_frame_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %118, i32 0, i32 1
  %119 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_frame_header, align 4, !tbaa !69
  %120 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %119(%struct.jpeg_compress_struct* %120)
  br label %if.end71

if.end71:                                         ; preds = %if.then70, %if.end61
  %121 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker72 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %121, i32 0, i32 58
  %122 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker72, align 4, !tbaa !68
  %write_scan_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %122, i32 0, i32 2
  %123 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_scan_header, align 4, !tbaa !71
  %124 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %123(%struct.jpeg_compress_struct* %124)
  %125 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub73 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %125, i32 0, i32 0
  %call_pass_startup74 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub73, i32 0, i32 3
  store i32 0, i32* %call_pass_startup74, align 4, !tbaa !20
  br label %sw.epilog

sw.bb75:                                          ; preds = %entry
  %126 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number76 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %126, i32 0, i32 2
  %127 = load i32, i32* %pass_number76, align 4, !tbaa !28
  %128 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %128, i32 0, i32 13
  %129 = load i32, i32* %num_components, align 4, !tbaa !34
  %130 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master77 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %130, i32 0, i32 54
  %131 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master77, align 4, !tbaa !8
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %131, i32 0, i32 10
  %132 = load i32, i32* %use_scans_in_trellis, align 8, !tbaa !33
  %tobool78 = icmp ne i32 %132, 0
  %133 = zext i1 %tobool78 to i64
  %cond79 = select i1 %tobool78, i32 4, i32 2
  %mul = mul nsw i32 %129, %cond79
  %rem = srem i32 %127, %mul
  %cmp80 = icmp eq i32 %rem, 1
  br i1 %cmp80, label %land.lhs.true, label %if.end101

land.lhs.true:                                    ; preds = %sw.bb75
  %134 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master82 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %134, i32 0, i32 54
  %135 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master82, align 4, !tbaa !8
  %trellis_q_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %135, i32 0, i32 12
  %136 = load i32, i32* %trellis_q_opt, align 8, !tbaa !72
  %tobool83 = icmp ne i32 %136, 0
  br i1 %tobool83, label %if.then84, label %if.end101

if.then84:                                        ; preds = %land.lhs.true
  %137 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #3
  %138 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc98, %if.then84
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %cmp85 = icmp slt i32 %139, 4
  br i1 %cmp85, label %for.body, label %for.end100

for.body:                                         ; preds = %for.cond
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond87

for.cond87:                                       ; preds = %for.inc, %for.body
  %140 = load i32, i32* %j, align 4, !tbaa !6
  %cmp88 = icmp slt i32 %140, 64
  br i1 %cmp88, label %for.body90, label %for.end

for.body90:                                       ; preds = %for.cond87
  %141 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master91 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %141, i32 0, i32 54
  %142 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master91, align 4, !tbaa !8
  %norm_src = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %142, i32 0, i32 14
  %143 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_src, i32 0, i32 %143
  %144 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx93 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx92, i32 0, i32 %144
  store double 0.000000e+00, double* %arrayidx93, align 8, !tbaa !73
  %145 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master94 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %145, i32 0, i32 54
  %146 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master94, align 4, !tbaa !8
  %norm_coef = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %146, i32 0, i32 15
  %147 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_coef, i32 0, i32 %147
  %148 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx96 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx95, i32 0, i32 %148
  store double 0.000000e+00, double* %arrayidx96, align 8, !tbaa !73
  br label %for.inc

for.inc:                                          ; preds = %for.body90
  %149 = load i32, i32* %j, align 4, !tbaa !6
  %inc97 = add nsw i32 %149, 1
  store i32 %inc97, i32* %j, align 4, !tbaa !6
  br label %for.cond87

for.end:                                          ; preds = %for.cond87
  br label %for.inc98

for.inc98:                                        ; preds = %for.end
  %150 = load i32, i32* %i, align 4, !tbaa !6
  %inc99 = add nsw i32 %150, 1
  store i32 %inc99, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end100:                                       ; preds = %for.cond
  %151 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  %152 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  br label %if.end101

if.end101:                                        ; preds = %for.end100, %land.lhs.true, %sw.bb75
  %153 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy102 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %153, i32 0, i32 62
  %154 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy102, align 4, !tbaa !52
  %start_pass103 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %154, i32 0, i32 0
  %start_pass104 = bitcast {}** %start_pass103 to void (%struct.jpeg_compress_struct*, i32)**
  %155 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass104, align 4, !tbaa !53
  %156 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %157 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code105 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %157, i32 0, i32 25
  %158 = load i32, i32* %arith_code105, align 4, !tbaa !24
  %tobool106 = icmp ne i32 %158, 0
  %lnot107 = xor i1 %tobool106, true
  %lnot.ext = zext i1 %lnot107 to i32
  call void %155(%struct.jpeg_compress_struct* %156, i32 %lnot.ext)
  %159 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef108 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %159, i32 0, i32 57
  %160 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef108, align 8, !tbaa !55
  %start_pass109 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %160, i32 0, i32 0
  %161 = load void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i32)** %start_pass109, align 4, !tbaa !56
  %162 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %161(%struct.jpeg_compress_struct* %162, i32 4)
  %163 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub110 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %163, i32 0, i32 0
  %call_pass_startup111 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub110, i32 0, i32 3
  store i32 0, i32* %call_pass_startup111, align 4, !tbaa !20
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %164 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %164, i32 0, i32 0
  %165 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %165, i32 0, i32 5
  store i32 48, i32* %msg_code, align 4, !tbaa !75
  %166 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err112 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %166, i32 0, i32 0
  %167 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err112, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %167, i32 0, i32 0
  %168 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %169 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %170 = bitcast %struct.jpeg_compress_struct* %169 to %struct.jpeg_common_struct*
  call void %168(%struct.jpeg_common_struct* %170)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end101, %if.end71, %if.then35, %if.end25
  %171 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number113 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %171, i32 0, i32 2
  %172 = load i32, i32* %pass_number113, align 4, !tbaa !28
  %173 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes114 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %173, i32 0, i32 3
  %174 = load i32, i32* %total_passes114, align 8, !tbaa !29
  %sub = sub nsw i32 %174, 1
  %cmp115 = icmp eq i32 %172, %sub
  %conv116 = zext i1 %cmp115 to i32
  %175 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub117 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %175, i32 0, i32 0
  %is_last_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub117, i32 0, i32 4
  store i32 %conv116, i32* %is_last_pass, align 8, !tbaa !19
  %176 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %176, i32 0, i32 2
  %177 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !78
  %cmp118 = icmp ne %struct.jpeg_progress_mgr* %177, null
  br i1 %cmp118, label %if.then120, label %if.end126

if.then120:                                       ; preds = %sw.epilog
  %178 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number121 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %178, i32 0, i32 2
  %179 = load i32, i32* %pass_number121, align 4, !tbaa !28
  %180 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress122 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %180, i32 0, i32 2
  %181 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress122, align 8, !tbaa !78
  %completed_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %181, i32 0, i32 3
  store i32 %179, i32* %completed_passes, align 4, !tbaa !79
  %182 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes123 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %182, i32 0, i32 3
  %183 = load i32, i32* %total_passes123, align 8, !tbaa !29
  %184 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress124 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %184, i32 0, i32 2
  %185 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress124, align 8, !tbaa !78
  %total_passes125 = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %185, i32 0, i32 4
  store i32 %183, i32* %total_passes125, align 4, !tbaa !81
  br label %if.end126

if.end126:                                        ; preds = %if.then120, %sw.epilog
  %186 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  ret void
}

; Function Attrs: nounwind
define internal void @pass_startup(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %0, i32 0, i32 54
  %1 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !8
  %call_pass_startup = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %1, i32 0, i32 3
  store i32 0, i32* %call_pass_startup, align 4, !tbaa !82
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 58
  %3 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker, align 4, !tbaa !68
  %write_frame_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %3, i32 0, i32 1
  %4 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_frame_header, align 4, !tbaa !69
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %4(%struct.jpeg_compress_struct* %5)
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 58
  %7 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker1, align 4, !tbaa !68
  %write_scan_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %7, i32 0, i32 2
  %8 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %write_scan_header, align 4, !tbaa !71
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %8(%struct.jpeg_compress_struct* %9)
  ret void
}

; Function Attrs: nounwind
define internal void @finish_pass_master(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %q = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %3 = bitcast %struct.jpeg_comp_master* %2 to %struct.my_comp_master*
  store %struct.my_comp_master* %3, %struct.my_comp_master** %master, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 62
  %5 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy, align 4, !tbaa !52
  %finish_pass = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %5, i32 0, i32 2
  %6 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %finish_pass, align 4, !tbaa !83
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %6(%struct.jpeg_compress_struct* %7)
  %8 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %8, i32 0, i32 1
  %9 = load i32, i32* %pass_type, align 8, !tbaa !26
  switch i32 %9, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb8
    i32 2, label %sw.bb10
    i32 3, label %sw.bb24
  ]

sw.bb:                                            ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 54
  %11 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !8
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %11, i32 0, i32 6
  %12 = load i32, i32* %trellis_quant, align 8, !tbaa !32
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %13 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type3 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %13, i32 0, i32 1
  store i32 3, i32* %pass_type3, align 8, !tbaa !26
  br label %if.end7

if.else:                                          ; preds = %sw.bb
  %14 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type4 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %14, i32 0, i32 1
  store i32 2, i32* %pass_type4, align 8, !tbaa !26
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 26
  %16 = load i32, i32* %optimize_coding, align 8, !tbaa !25
  %tobool5 = icmp ne i32 %16, 0
  br i1 %tobool5, label %if.end, label %if.then6

if.then6:                                         ; preds = %if.else
  %17 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %17, i32 0, i32 4
  %18 = load i32, i32* %scan_number, align 4, !tbaa !27
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %scan_number, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  br label %sw.epilog

sw.bb8:                                           ; preds = %entry
  %19 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %19, i32 0, i32 2
  %20 = load i32, i32* %pass_number, align 4, !tbaa !28
  %21 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %21, i32 0, i32 5
  %22 = load i32, i32* %pass_number_scan_opt_base, align 8, !tbaa !31
  %sub = sub nsw i32 %22, 1
  %cmp = icmp slt i32 %20, %sub
  %23 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 3, i32 2
  %24 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type9 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %24, i32 0, i32 1
  store i32 %cond, i32* %pass_type9, align 8, !tbaa !26
  br label %sw.epilog

sw.bb10:                                          ; preds = %entry
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 26
  %26 = load i32, i32* %optimize_coding11, align 8, !tbaa !25
  %tobool12 = icmp ne i32 %26, 0
  br i1 %tobool12, label %if.then13, label %if.end15

if.then13:                                        ; preds = %sw.bb10
  %27 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type14 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %27, i32 0, i32 1
  store i32 1, i32* %pass_type14, align 8, !tbaa !26
  br label %if.end15

if.end15:                                         ; preds = %if.then13, %sw.bb10
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 54
  %29 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master16, align 4, !tbaa !8
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %29, i32 0, i32 5
  %30 = load i32, i32* %optimize_scans, align 4, !tbaa !36
  %tobool17 = icmp ne i32 %30, 0
  br i1 %tobool17, label %if.then18, label %if.end21

if.then18:                                        ; preds = %if.end15
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 6
  %32 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !63
  %term_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %32, i32 0, i32 4
  %33 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %term_destination, align 4, !tbaa !84
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %33(%struct.jpeg_compress_struct* %34)
  %35 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %saved_dest = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %35, i32 0, i32 15
  %36 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %saved_dest, align 4, !tbaa !64
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %37, i32 0, i32 6
  store %struct.jpeg_destination_mgr* %36, %struct.jpeg_destination_mgr** %dest19, align 8, !tbaa !63
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %39 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number20 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %39, i32 0, i32 4
  %40 = load i32, i32* %scan_number20, align 4, !tbaa !27
  %add = add nsw i32 %40, 1
  call void @select_scans(%struct.jpeg_compress_struct* %38, i32 %add)
  br label %if.end21

if.end21:                                         ; preds = %if.then18, %if.end15
  %41 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number22 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %41, i32 0, i32 4
  %42 = load i32, i32* %scan_number22, align 4, !tbaa !27
  %inc23 = add nsw i32 %42, 1
  store i32 %inc23, i32* %scan_number22, align 4, !tbaa !27
  br label %sw.epilog

sw.bb24:                                          ; preds = %entry
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 26
  %44 = load i32, i32* %optimize_coding25, align 8, !tbaa !25
  %tobool26 = icmp ne i32 %44, 0
  br i1 %tobool26, label %if.then27, label %if.else29

if.then27:                                        ; preds = %sw.bb24
  %45 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type28 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %45, i32 0, i32 1
  store i32 1, i32* %pass_type28, align 8, !tbaa !26
  br label %if.end36

if.else29:                                        ; preds = %sw.bb24
  %46 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number30 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %46, i32 0, i32 2
  %47 = load i32, i32* %pass_number30, align 4, !tbaa !28
  %48 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base31 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %48, i32 0, i32 5
  %49 = load i32, i32* %pass_number_scan_opt_base31, align 8, !tbaa !31
  %sub32 = sub nsw i32 %49, 1
  %cmp33 = icmp slt i32 %47, %sub32
  %50 = zext i1 %cmp33 to i64
  %cond34 = select i1 %cmp33, i32 3, i32 2
  %51 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_type35 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %51, i32 0, i32 1
  store i32 %cond34, i32* %pass_type35, align 8, !tbaa !26
  br label %if.end36

if.end36:                                         ; preds = %if.else29, %if.then27
  %52 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number37 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %52, i32 0, i32 2
  %53 = load i32, i32* %pass_number37, align 4, !tbaa !28
  %add38 = add nsw i32 %53, 1
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 13
  %55 = load i32, i32* %num_components, align 4, !tbaa !34
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master39 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 54
  %57 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master39, align 4, !tbaa !8
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %57, i32 0, i32 10
  %58 = load i32, i32* %use_scans_in_trellis, align 8, !tbaa !33
  %tobool40 = icmp ne i32 %58, 0
  %59 = zext i1 %tobool40 to i64
  %cond41 = select i1 %tobool40, i32 4, i32 2
  %mul = mul nsw i32 %55, %cond41
  %rem = srem i32 %add38, %mul
  %cmp42 = icmp eq i32 %rem, 0
  br i1 %cmp42, label %land.lhs.true, label %if.end78

land.lhs.true:                                    ; preds = %if.end36
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 54
  %61 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master43, align 4, !tbaa !8
  %trellis_q_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %61, i32 0, i32 12
  %62 = load i32, i32* %trellis_q_opt, align 8, !tbaa !72
  %tobool44 = icmp ne i32 %62, 0
  br i1 %tobool44, label %if.then45, label %if.end78

if.then45:                                        ; preds = %land.lhs.true
  %63 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #3
  %64 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %if.then45
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %cmp46 = icmp slt i32 %65, 4
  br i1 %cmp46, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc, %for.body
  %66 = load i32, i32* %j, align 4, !tbaa !6
  %cmp48 = icmp slt i32 %66, 64
  br i1 %cmp48, label %for.body49, label %for.end

for.body49:                                       ; preds = %for.cond47
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master50 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 54
  %68 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master50, align 4, !tbaa !8
  %norm_coef = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %68, i32 0, i32 15
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_coef, i32 0, i32 %69
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx, i32 0, i32 %70
  %71 = load double, double* %arrayidx51, align 8, !tbaa !73
  %cmp52 = fcmp une double %71, 0.000000e+00
  br i1 %cmp52, label %if.then53, label %if.end73

if.then53:                                        ; preds = %for.body49
  %72 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #3
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %73, i32 0, i32 54
  %74 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master54, align 4, !tbaa !8
  %norm_src = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %74, i32 0, i32 14
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_src, i32 0, i32 %75
  %76 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx55, i32 0, i32 %76
  %77 = load double, double* %arrayidx56, align 8, !tbaa !73
  %78 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %78, i32 0, i32 54
  %79 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master57, align 4, !tbaa !8
  %norm_coef58 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %79, i32 0, i32 15
  %80 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_coef58, i32 0, i32 %80
  %81 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx59, i32 0, i32 %81
  %82 = load double, double* %arrayidx60, align 8, !tbaa !73
  %div = fdiv double %77, %82
  %add61 = fadd double %div, 5.000000e-01
  %conv = fptosi double %add61 to i32
  store i32 %conv, i32* %q, align 4, !tbaa !6
  %83 = load i32, i32* %q, align 4, !tbaa !6
  %cmp62 = icmp sgt i32 %83, 254
  br i1 %cmp62, label %if.then64, label %if.end65

if.then64:                                        ; preds = %if.then53
  store i32 254, i32* %q, align 4, !tbaa !6
  br label %if.end65

if.end65:                                         ; preds = %if.then64, %if.then53
  %84 = load i32, i32* %q, align 4, !tbaa !6
  %cmp66 = icmp slt i32 %84, 1
  br i1 %cmp66, label %if.then68, label %if.end69

if.then68:                                        ; preds = %if.end65
  store i32 1, i32* %q, align 4, !tbaa !6
  br label %if.end69

if.end69:                                         ; preds = %if.then68, %if.end65
  %85 = load i32, i32* %q, align 4, !tbaa !6
  %conv70 = trunc i32 %85 to i16
  %86 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %86, i32 0, i32 16
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %87
  %88 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx71, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %88, i32 0, i32 0
  %89 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %89
  store i16 %conv70, i16* %arrayidx72, align 2, !tbaa !85
  %90 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  br label %if.end73

if.end73:                                         ; preds = %if.end69, %for.body49
  br label %for.inc

for.inc:                                          ; preds = %if.end73
  %91 = load i32, i32* %j, align 4, !tbaa !6
  %inc74 = add nsw i32 %91, 1
  store i32 %inc74, i32* %j, align 4, !tbaa !6
  br label %for.cond47

for.end:                                          ; preds = %for.cond47
  br label %for.inc75

for.inc75:                                        ; preds = %for.end
  %92 = load i32, i32* %i, align 4, !tbaa !6
  %inc76 = add nsw i32 %92, 1
  store i32 %inc76, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  %93 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  br label %if.end78

if.end78:                                         ; preds = %for.end77, %land.lhs.true, %if.end36
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %if.end78, %if.end21, %sw.bb8, %if.end7
  %95 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number79 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %95, i32 0, i32 2
  %96 = load i32, i32* %pass_number79, align 4, !tbaa !28
  %inc80 = add nsw i32 %96, 1
  store i32 %inc80, i32* %pass_number79, align 4, !tbaa !28
  %97 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  ret void
}

; Function Attrs: nounwind
define internal void @initial_setup(%struct.jpeg_compress_struct* %cinfo, i32 %transcode_only) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %transcode_only.addr = alloca i32, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %samplesperrow = alloca i32, align 4
  %jd_samplesperrow = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %transcode_only, i32* %transcode_only.addr, align 4, !tbaa !6
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %samplesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %jd_samplesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 8
  %5 = load i32, i32* %image_height, align 8, !tbaa !86
  %cmp = icmp ule i32 %5, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !87
  %cmp1 = icmp ule i32 %7, 0
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 13
  %9 = load i32, i32* %num_components, align 4, !tbaa !34
  %cmp3 = icmp sle i32 %9, 0
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 9
  %11 = load i32, i32* %input_components, align 4, !tbaa !88
  %cmp5 = icmp sle i32 %11, 0
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 32, i32* %msg_code, align 4, !tbaa !75
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = bitcast %struct.jpeg_compress_struct* %17 to %struct.jpeg_common_struct*
  call void %16(%struct.jpeg_common_struct* %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false4
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 8
  %20 = load i32, i32* %image_height7, align 8, !tbaa !86
  %cmp8 = icmp sgt i32 %20, 65500
  br i1 %cmp8, label %if.then12, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %if.end
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 7
  %22 = load i32, i32* %image_width10, align 4, !tbaa !87
  %cmp11 = icmp sgt i32 %22, 65500
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %lor.lhs.false9, %if.end
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !74
  %msg_code14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 5
  store i32 41, i32* %msg_code14, align 4, !tbaa !75
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !74
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 65500, i32* %arrayidx, align 4, !tbaa !89
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err16, align 8, !tbaa !74
  %error_exit17 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit17, align 4, !tbaa !77
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_compress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %if.end18

if.end18:                                         ; preds = %if.then12, %lor.lhs.false9
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 7
  %33 = load i32, i32* %image_width19, align 4, !tbaa !87
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_components20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 9
  %35 = load i32, i32* %input_components20, align 4, !tbaa !88
  %mul = mul nsw i32 %33, %35
  store i32 %mul, i32* %samplesperrow, align 4, !tbaa !65
  %36 = load i32, i32* %samplesperrow, align 4, !tbaa !65
  store i32 %36, i32* %jd_samplesperrow, align 4, !tbaa !6
  %37 = load i32, i32* %jd_samplesperrow, align 4, !tbaa !6
  %38 = load i32, i32* %samplesperrow, align 4, !tbaa !65
  %cmp21 = icmp ne i32 %37, %38
  br i1 %cmp21, label %if.then22, label %if.end27

if.then22:                                        ; preds = %if.end18
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 0
  %40 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err23, align 8, !tbaa !74
  %msg_code24 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %40, i32 0, i32 5
  store i32 70, i32* %msg_code24, align 4, !tbaa !75
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !74
  %error_exit26 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 0
  %43 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit26, align 4, !tbaa !77
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %45 = bitcast %struct.jpeg_compress_struct* %44 to %struct.jpeg_common_struct*
  call void %43(%struct.jpeg_common_struct* %45)
  br label %if.end27

if.end27:                                         ; preds = %if.then22, %if.end18
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 12
  %47 = load i32, i32* %data_precision, align 8, !tbaa !90
  %cmp28 = icmp ne i32 %47, 8
  br i1 %cmp28, label %if.then29, label %if.end39

if.then29:                                        ; preds = %if.end27
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 0
  %49 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err30, align 8, !tbaa !74
  %msg_code31 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %49, i32 0, i32 5
  store i32 15, i32* %msg_code31, align 4, !tbaa !75
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 12
  %51 = load i32, i32* %data_precision32, align 8, !tbaa !90
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err33, align 8, !tbaa !74
  %msg_parm34 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 6
  %i35 = bitcast %union.anon* %msg_parm34 to [8 x i32]*
  %arrayidx36 = getelementptr inbounds [8 x i32], [8 x i32]* %i35, i32 0, i32 0
  store i32 %51, i32* %arrayidx36, align 4, !tbaa !89
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 0
  %55 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err37, align 8, !tbaa !74
  %error_exit38 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %55, i32 0, i32 0
  %56 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit38, align 4, !tbaa !77
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %58 = bitcast %struct.jpeg_compress_struct* %57 to %struct.jpeg_common_struct*
  call void %56(%struct.jpeg_common_struct* %58)
  br label %if.end39

if.end39:                                         ; preds = %if.then29, %if.end27
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components40 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 13
  %60 = load i32, i32* %num_components40, align 4, !tbaa !34
  %cmp41 = icmp sgt i32 %60, 10
  br i1 %cmp41, label %if.then42, label %if.end56

if.then42:                                        ; preds = %if.end39
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 8, !tbaa !74
  %msg_code44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 5
  store i32 26, i32* %msg_code44, align 4, !tbaa !75
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components45 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 13
  %64 = load i32, i32* %num_components45, align 4, !tbaa !34
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 0
  %66 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !74
  %msg_parm47 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %66, i32 0, i32 6
  %i48 = bitcast %union.anon* %msg_parm47 to [8 x i32]*
  %arrayidx49 = getelementptr inbounds [8 x i32], [8 x i32]* %i48, i32 0, i32 0
  store i32 %64, i32* %arrayidx49, align 4, !tbaa !89
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err50 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 0
  %68 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err50, align 8, !tbaa !74
  %msg_parm51 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %68, i32 0, i32 6
  %i52 = bitcast %union.anon* %msg_parm51 to [8 x i32]*
  %arrayidx53 = getelementptr inbounds [8 x i32], [8 x i32]* %i52, i32 0, i32 1
  store i32 10, i32* %arrayidx53, align 4, !tbaa !89
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err54, align 8, !tbaa !74
  %error_exit55 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 0
  %71 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit55, align 4, !tbaa !77
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %73 = bitcast %struct.jpeg_compress_struct* %72 to %struct.jpeg_common_struct*
  call void %71(%struct.jpeg_common_struct* %73)
  br label %if.end56

if.end56:                                         ; preds = %if.then42, %if.end39
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 41
  store i32 1, i32* %max_h_samp_factor, align 8, !tbaa !91
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 42
  store i32 1, i32* %max_v_samp_factor, align 4, !tbaa !92
  store i32 0, i32* %ci, align 4, !tbaa !6
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %76, i32 0, i32 15
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !93
  store %struct.jpeg_component_info* %77, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end56
  %78 = load i32, i32* %ci, align 4, !tbaa !6
  %79 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %79, i32 0, i32 13
  %80 = load i32, i32* %num_components57, align 4, !tbaa !34
  %cmp58 = icmp slt i32 %78, %80
  br i1 %cmp58, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %81 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %81, i32 0, i32 2
  %82 = load i32, i32* %h_samp_factor, align 4, !tbaa !94
  %cmp59 = icmp sle i32 %82, 0
  br i1 %cmp59, label %if.then68, label %lor.lhs.false60

lor.lhs.false60:                                  ; preds = %for.body
  %83 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor61 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %83, i32 0, i32 2
  %84 = load i32, i32* %h_samp_factor61, align 4, !tbaa !94
  %cmp62 = icmp sgt i32 %84, 4
  br i1 %cmp62, label %if.then68, label %lor.lhs.false63

lor.lhs.false63:                                  ; preds = %lor.lhs.false60
  %85 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %85, i32 0, i32 3
  %86 = load i32, i32* %v_samp_factor, align 4, !tbaa !96
  %cmp64 = icmp sle i32 %86, 0
  br i1 %cmp64, label %if.then68, label %lor.lhs.false65

lor.lhs.false65:                                  ; preds = %lor.lhs.false63
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor66 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %87, i32 0, i32 3
  %88 = load i32, i32* %v_samp_factor66, align 4, !tbaa !96
  %cmp67 = icmp sgt i32 %88, 4
  br i1 %cmp67, label %if.then68, label %if.end73

if.then68:                                        ; preds = %lor.lhs.false65, %lor.lhs.false63, %lor.lhs.false60, %for.body
  %89 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err69 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %89, i32 0, i32 0
  %90 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err69, align 8, !tbaa !74
  %msg_code70 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %90, i32 0, i32 5
  store i32 18, i32* %msg_code70, align 4, !tbaa !75
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err71 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 0
  %92 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err71, align 8, !tbaa !74
  %error_exit72 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %92, i32 0, i32 0
  %93 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit72, align 4, !tbaa !77
  %94 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %95 = bitcast %struct.jpeg_compress_struct* %94 to %struct.jpeg_common_struct*
  call void %93(%struct.jpeg_common_struct* %95)
  br label %if.end73

if.end73:                                         ; preds = %if.then68, %lor.lhs.false65
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor74 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %96, i32 0, i32 41
  %97 = load i32, i32* %max_h_samp_factor74, align 8, !tbaa !91
  %98 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor75 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %98, i32 0, i32 2
  %99 = load i32, i32* %h_samp_factor75, align 4, !tbaa !94
  %cmp76 = icmp sgt i32 %97, %99
  br i1 %cmp76, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end73
  %100 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor77 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %100, i32 0, i32 41
  %101 = load i32, i32* %max_h_samp_factor77, align 8, !tbaa !91
  br label %cond.end

cond.false:                                       ; preds = %if.end73
  %102 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor78 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %102, i32 0, i32 2
  %103 = load i32, i32* %h_samp_factor78, align 4, !tbaa !94
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %101, %cond.true ], [ %103, %cond.false ]
  %104 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor79 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %104, i32 0, i32 41
  store i32 %cond, i32* %max_h_samp_factor79, align 8, !tbaa !91
  %105 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor80 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %105, i32 0, i32 42
  %106 = load i32, i32* %max_v_samp_factor80, align 4, !tbaa !92
  %107 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor81 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %107, i32 0, i32 3
  %108 = load i32, i32* %v_samp_factor81, align 4, !tbaa !96
  %cmp82 = icmp sgt i32 %106, %108
  br i1 %cmp82, label %cond.true83, label %cond.false85

cond.true83:                                      ; preds = %cond.end
  %109 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor84 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %109, i32 0, i32 42
  %110 = load i32, i32* %max_v_samp_factor84, align 4, !tbaa !92
  br label %cond.end87

cond.false85:                                     ; preds = %cond.end
  %111 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor86 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %111, i32 0, i32 3
  %112 = load i32, i32* %v_samp_factor86, align 4, !tbaa !96
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false85, %cond.true83
  %cond88 = phi i32 [ %110, %cond.true83 ], [ %112, %cond.false85 ]
  %113 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor89 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %113, i32 0, i32 42
  store i32 %cond88, i32* %max_v_samp_factor89, align 4, !tbaa !92
  br label %for.inc

for.inc:                                          ; preds = %cond.end87
  %114 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %114, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %115 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %115, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %ci, align 4, !tbaa !6
  %116 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info90 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %116, i32 0, i32 15
  %117 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info90, align 4, !tbaa !93
  store %struct.jpeg_component_info* %117, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc116, %for.end
  %118 = load i32, i32* %ci, align 4, !tbaa !6
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components92 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 13
  %120 = load i32, i32* %num_components92, align 4, !tbaa !34
  %cmp93 = icmp slt i32 %118, %120
  br i1 %cmp93, label %for.body94, label %for.end119

for.body94:                                       ; preds = %for.cond91
  %121 = load i32, i32* %ci, align 4, !tbaa !6
  %122 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %122, i32 0, i32 1
  store i32 %121, i32* %component_index, align 4, !tbaa !97
  %123 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %123, i32 0, i32 9
  store i32 8, i32* %DCT_scaled_size, align 4, !tbaa !98
  %124 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width95 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %124, i32 0, i32 7
  %125 = load i32, i32* %image_width95, align 4, !tbaa !87
  %126 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor96 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %126, i32 0, i32 2
  %127 = load i32, i32* %h_samp_factor96, align 4, !tbaa !94
  %mul97 = mul nsw i32 %125, %127
  %128 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor98 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %128, i32 0, i32 41
  %129 = load i32, i32* %max_h_samp_factor98, align 8, !tbaa !91
  %mul99 = mul nsw i32 %129, 8
  %call = call i32 @jdiv_round_up(i32 %mul97, i32 %mul99)
  %130 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %130, i32 0, i32 7
  store i32 %call, i32* %width_in_blocks, align 4, !tbaa !99
  %131 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height100 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %131, i32 0, i32 8
  %132 = load i32, i32* %image_height100, align 8, !tbaa !86
  %133 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor101 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %133, i32 0, i32 3
  %134 = load i32, i32* %v_samp_factor101, align 4, !tbaa !96
  %mul102 = mul nsw i32 %132, %134
  %135 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor103 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %135, i32 0, i32 42
  %136 = load i32, i32* %max_v_samp_factor103, align 4, !tbaa !92
  %mul104 = mul nsw i32 %136, 8
  %call105 = call i32 @jdiv_round_up(i32 %mul102, i32 %mul104)
  %137 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %137, i32 0, i32 8
  store i32 %call105, i32* %height_in_blocks, align 4, !tbaa !100
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width106 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 7
  %139 = load i32, i32* %image_width106, align 4, !tbaa !87
  %140 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor107 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %140, i32 0, i32 2
  %141 = load i32, i32* %h_samp_factor107, align 4, !tbaa !94
  %mul108 = mul nsw i32 %139, %141
  %142 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor109 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %142, i32 0, i32 41
  %143 = load i32, i32* %max_h_samp_factor109, align 8, !tbaa !91
  %call110 = call i32 @jdiv_round_up(i32 %mul108, i32 %143)
  %144 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %144, i32 0, i32 10
  store i32 %call110, i32* %downsampled_width, align 4, !tbaa !101
  %145 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height111 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %145, i32 0, i32 8
  %146 = load i32, i32* %image_height111, align 8, !tbaa !86
  %147 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor112 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %147, i32 0, i32 3
  %148 = load i32, i32* %v_samp_factor112, align 4, !tbaa !96
  %mul113 = mul nsw i32 %146, %148
  %149 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor114 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %149, i32 0, i32 42
  %150 = load i32, i32* %max_v_samp_factor114, align 4, !tbaa !92
  %call115 = call i32 @jdiv_round_up(i32 %mul113, i32 %150)
  %151 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %151, i32 0, i32 11
  store i32 %call115, i32* %downsampled_height, align 4, !tbaa !102
  %152 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %152, i32 0, i32 12
  store i32 1, i32* %component_needed, align 4, !tbaa !103
  br label %for.inc116

for.inc116:                                       ; preds = %for.body94
  %153 = load i32, i32* %ci, align 4, !tbaa !6
  %inc117 = add nsw i32 %153, 1
  store i32 %inc117, i32* %ci, align 4, !tbaa !6
  %154 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr118 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %154, i32 1
  store %struct.jpeg_component_info* %incdec.ptr118, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond91

for.end119:                                       ; preds = %for.cond91
  %155 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height120 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %155, i32 0, i32 8
  %156 = load i32, i32* %image_height120, align 8, !tbaa !86
  %157 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor121 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %157, i32 0, i32 42
  %158 = load i32, i32* %max_v_samp_factor121, align 4, !tbaa !92
  %mul122 = mul nsw i32 %158, 8
  %call123 = call i32 @jdiv_round_up(i32 %156, i32 %mul122)
  %159 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %159, i32 0, i32 43
  store i32 %call123, i32* %total_iMCU_rows, align 8, !tbaa !104
  %160 = bitcast i32* %jd_samplesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast i32* %samplesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  ret void
}

; Function Attrs: nounwind
define internal void @validate_script(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %scanptr = alloca %struct.jpeg_scan_info*, align 4
  %scanno = alloca i32, align 4
  %ncomps = alloca i32, align 4
  %ci = alloca i32, align 4
  %coefi = alloca i32, align 4
  %thisi = alloca i32, align 4
  %Ss = alloca i32, align 4
  %Se = alloca i32, align 4
  %Ah = alloca i32, align 4
  %Al = alloca i32, align 4
  %component_sent = alloca [10 x i32], align 16
  %last_bitpos_ptr = alloca i32*, align 4
  %last_bitpos = alloca [10 x [64 x i32]], align 16
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %scanno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %thisi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %Ss to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %Ah to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast [10 x i32]* %component_sent to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %10) #3
  %11 = bitcast i32** %last_bitpos_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast [10 x [64 x i32]]* %last_bitpos to i8*
  call void @llvm.lifetime.start.p0i8(i64 2560, i8* %12) #3
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 54
  %14 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !8
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %14, i32 0, i32 5
  %15 = load i32, i32* %optimize_scans, align 4, !tbaa !36
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 40
  store i32 1, i32* %progressive_mode, align 4, !tbaa !22
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 22
  %18 = load i32, i32* %num_scans, align 8, !tbaa !23
  %cmp = icmp sle i32 %18, 0
  br i1 %cmp, label %if.then1, label %if.end4

if.then1:                                         ; preds = %if.end
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 19, i32* %msg_code, align 4, !tbaa !75
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !74
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4, !tbaa !89
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_compress_struct* %26 to %struct.jpeg_common_struct*
  call void %25(%struct.jpeg_common_struct* %27)
  br label %if.end4

if.end4:                                          ; preds = %if.then1, %if.end
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 23
  %29 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !21
  store %struct.jpeg_scan_info* %29, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %30 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ss5 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %30, i32 0, i32 2
  %31 = load i32, i32* %Ss5, align 4, !tbaa !105
  %cmp6 = icmp ne i32 %31, 0
  br i1 %cmp6, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end4
  %32 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Se7 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %32, i32 0, i32 3
  %33 = load i32, i32* %Se7, align 4, !tbaa !107
  %cmp8 = icmp ne i32 %33, 63
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %lor.lhs.false, %if.end4
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 40
  store i32 1, i32* %progressive_mode10, align 4, !tbaa !22
  %arrayidx11 = getelementptr inbounds [10 x [64 x i32]], [10 x [64 x i32]]* %last_bitpos, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx11, i32 0, i32 0
  store i32* %arrayidx12, i32** %last_bitpos_ptr, align 4, !tbaa !2
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %if.then9
  %35 = load i32, i32* %ci, align 4, !tbaa !6
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 13
  %37 = load i32, i32* %num_components, align 4, !tbaa !34
  %cmp13 = icmp slt i32 %35, %37
  br i1 %cmp13, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %coefi, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body
  %38 = load i32, i32* %coefi, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %38, 64
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %39 = load i32*, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %39, i32 1
  store i32* %incdec.ptr, i32** %last_bitpos_ptr, align 4, !tbaa !2
  store i32 -1, i32* %39, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body16
  %40 = load i32, i32* %coefi, align 4, !tbaa !6
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %coefi, align 4, !tbaa !6
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %41 = load i32, i32* %ci, align 4, !tbaa !6
  %inc18 = add nsw i32 %41, 1
  store i32 %inc18, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  br label %if.end29

if.else:                                          ; preds = %lor.lhs.false
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 40
  store i32 0, i32* %progressive_mode20, align 4, !tbaa !22
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc26, %if.else
  %43 = load i32, i32* %ci, align 4, !tbaa !6
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 13
  %45 = load i32, i32* %num_components22, align 4, !tbaa !34
  %cmp23 = icmp slt i32 %43, %45
  br i1 %cmp23, label %for.body24, label %for.end28

for.body24:                                       ; preds = %for.cond21
  %46 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds [10 x i32], [10 x i32]* %component_sent, i32 0, i32 %46
  store i32 0, i32* %arrayidx25, align 4, !tbaa !6
  br label %for.inc26

for.inc26:                                        ; preds = %for.body24
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %inc27 = add nsw i32 %47, 1
  store i32 %inc27, i32* %ci, align 4, !tbaa !6
  br label %for.cond21

for.end28:                                        ; preds = %for.cond21
  br label %if.end29

if.end29:                                         ; preds = %for.end28, %for.end19
  store i32 1, i32* %scanno, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc246, %if.end29
  %48 = load i32, i32* %scanno, align 4, !tbaa !6
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans31 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 22
  %50 = load i32, i32* %num_scans31, align 8, !tbaa !23
  %cmp32 = icmp sle i32 %48, %50
  br i1 %cmp32, label %for.body33, label %for.end249

for.body33:                                       ; preds = %for.cond30
  %51 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %51, i32 0, i32 0
  %52 = load i32, i32* %comps_in_scan, align 4, !tbaa !108
  store i32 %52, i32* %ncomps, align 4, !tbaa !6
  %53 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp34 = icmp sle i32 %53, 0
  br i1 %cmp34, label %if.then37, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %for.body33
  %54 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp36 = icmp sgt i32 %54, 4
  br i1 %cmp36, label %if.then37, label %if.end50

if.then37:                                        ; preds = %lor.lhs.false35, %for.body33
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err38, align 8, !tbaa !74
  %msg_code39 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 5
  store i32 26, i32* %msg_code39, align 4, !tbaa !75
  %57 = load i32, i32* %ncomps, align 4, !tbaa !6
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err40 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 0
  %59 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err40, align 8, !tbaa !74
  %msg_parm41 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %59, i32 0, i32 6
  %i42 = bitcast %union.anon* %msg_parm41 to [8 x i32]*
  %arrayidx43 = getelementptr inbounds [8 x i32], [8 x i32]* %i42, i32 0, i32 0
  store i32 %57, i32* %arrayidx43, align 4, !tbaa !89
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 0
  %61 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err44, align 8, !tbaa !74
  %msg_parm45 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %61, i32 0, i32 6
  %i46 = bitcast %union.anon* %msg_parm45 to [8 x i32]*
  %arrayidx47 = getelementptr inbounds [8 x i32], [8 x i32]* %i46, i32 0, i32 1
  store i32 4, i32* %arrayidx47, align 4, !tbaa !89
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err48 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %62, i32 0, i32 0
  %63 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err48, align 8, !tbaa !74
  %error_exit49 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %63, i32 0, i32 0
  %64 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit49, align 4, !tbaa !77
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %66 = bitcast %struct.jpeg_compress_struct* %65 to %struct.jpeg_common_struct*
  call void %64(%struct.jpeg_common_struct* %66)
  br label %if.end50

if.end50:                                         ; preds = %if.then37, %lor.lhs.false35
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc83, %if.end50
  %67 = load i32, i32* %ci, align 4, !tbaa !6
  %68 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp52 = icmp slt i32 %67, %68
  br i1 %cmp52, label %for.body53, label %for.end85

for.body53:                                       ; preds = %for.cond51
  %69 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %69, i32 0, i32 1
  %70 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx54, align 4, !tbaa !6
  store i32 %71, i32* %thisi, align 4, !tbaa !6
  %72 = load i32, i32* %thisi, align 4, !tbaa !6
  %cmp55 = icmp slt i32 %72, 0
  br i1 %cmp55, label %if.then59, label %lor.lhs.false56

lor.lhs.false56:                                  ; preds = %for.body53
  %73 = load i32, i32* %thisi, align 4, !tbaa !6
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 13
  %75 = load i32, i32* %num_components57, align 4, !tbaa !34
  %cmp58 = icmp sge i32 %73, %75
  br i1 %cmp58, label %if.then59, label %if.end68

if.then59:                                        ; preds = %lor.lhs.false56, %for.body53
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %76, i32 0, i32 0
  %77 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err60, align 8, !tbaa !74
  %msg_code61 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %77, i32 0, i32 5
  store i32 19, i32* %msg_code61, align 4, !tbaa !75
  %78 = load i32, i32* %scanno, align 4, !tbaa !6
  %79 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err62 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %79, i32 0, i32 0
  %80 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err62, align 8, !tbaa !74
  %msg_parm63 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %80, i32 0, i32 6
  %i64 = bitcast %union.anon* %msg_parm63 to [8 x i32]*
  %arrayidx65 = getelementptr inbounds [8 x i32], [8 x i32]* %i64, i32 0, i32 0
  store i32 %78, i32* %arrayidx65, align 4, !tbaa !89
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err66 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 0
  %82 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err66, align 8, !tbaa !74
  %error_exit67 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %82, i32 0, i32 0
  %83 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit67, align 4, !tbaa !77
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %85 = bitcast %struct.jpeg_compress_struct* %84 to %struct.jpeg_common_struct*
  call void %83(%struct.jpeg_common_struct* %85)
  br label %if.end68

if.end68:                                         ; preds = %if.then59, %lor.lhs.false56
  %86 = load i32, i32* %ci, align 4, !tbaa !6
  %cmp69 = icmp sgt i32 %86, 0
  br i1 %cmp69, label %land.lhs.true, label %if.end82

land.lhs.true:                                    ; preds = %if.end68
  %87 = load i32, i32* %thisi, align 4, !tbaa !6
  %88 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index70 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %88, i32 0, i32 1
  %89 = load i32, i32* %ci, align 4, !tbaa !6
  %sub = sub nsw i32 %89, 1
  %arrayidx71 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index70, i32 0, i32 %sub
  %90 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %cmp72 = icmp sle i32 %87, %90
  br i1 %cmp72, label %if.then73, label %if.end82

if.then73:                                        ; preds = %land.lhs.true
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err74 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 0
  %92 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err74, align 8, !tbaa !74
  %msg_code75 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %92, i32 0, i32 5
  store i32 19, i32* %msg_code75, align 4, !tbaa !75
  %93 = load i32, i32* %scanno, align 4, !tbaa !6
  %94 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err76 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %94, i32 0, i32 0
  %95 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err76, align 8, !tbaa !74
  %msg_parm77 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %95, i32 0, i32 6
  %i78 = bitcast %union.anon* %msg_parm77 to [8 x i32]*
  %arrayidx79 = getelementptr inbounds [8 x i32], [8 x i32]* %i78, i32 0, i32 0
  store i32 %93, i32* %arrayidx79, align 4, !tbaa !89
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err80 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %96, i32 0, i32 0
  %97 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err80, align 8, !tbaa !74
  %error_exit81 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %97, i32 0, i32 0
  %98 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit81, align 4, !tbaa !77
  %99 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %100 = bitcast %struct.jpeg_compress_struct* %99 to %struct.jpeg_common_struct*
  call void %98(%struct.jpeg_common_struct* %100)
  br label %if.end82

if.end82:                                         ; preds = %if.then73, %land.lhs.true, %if.end68
  br label %for.inc83

for.inc83:                                        ; preds = %if.end82
  %101 = load i32, i32* %ci, align 4, !tbaa !6
  %inc84 = add nsw i32 %101, 1
  store i32 %inc84, i32* %ci, align 4, !tbaa !6
  br label %for.cond51

for.end85:                                        ; preds = %for.cond51
  %102 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ss86 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %102, i32 0, i32 2
  %103 = load i32, i32* %Ss86, align 4, !tbaa !105
  store i32 %103, i32* %Ss, align 4, !tbaa !6
  %104 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Se87 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %104, i32 0, i32 3
  %105 = load i32, i32* %Se87, align 4, !tbaa !107
  store i32 %105, i32* %Se, align 4, !tbaa !6
  %106 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ah88 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %106, i32 0, i32 4
  %107 = load i32, i32* %Ah88, align 4, !tbaa !109
  store i32 %107, i32* %Ah, align 4, !tbaa !6
  %108 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Al89 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %108, i32 0, i32 5
  %109 = load i32, i32* %Al89, align 4, !tbaa !110
  store i32 %109, i32* %Al, align 4, !tbaa !6
  %110 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode90 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %110, i32 0, i32 40
  %111 = load i32, i32* %progressive_mode90, align 4, !tbaa !22
  %tobool91 = icmp ne i32 %111, 0
  br i1 %tobool91, label %if.then92, label %if.else206

if.then92:                                        ; preds = %for.end85
  %112 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp93 = icmp slt i32 %112, 0
  br i1 %cmp93, label %if.then108, label %lor.lhs.false94

lor.lhs.false94:                                  ; preds = %if.then92
  %113 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp95 = icmp sge i32 %113, 64
  br i1 %cmp95, label %if.then108, label %lor.lhs.false96

lor.lhs.false96:                                  ; preds = %lor.lhs.false94
  %114 = load i32, i32* %Se, align 4, !tbaa !6
  %115 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp97 = icmp slt i32 %114, %115
  br i1 %cmp97, label %if.then108, label %lor.lhs.false98

lor.lhs.false98:                                  ; preds = %lor.lhs.false96
  %116 = load i32, i32* %Se, align 4, !tbaa !6
  %cmp99 = icmp sge i32 %116, 64
  br i1 %cmp99, label %if.then108, label %lor.lhs.false100

lor.lhs.false100:                                 ; preds = %lor.lhs.false98
  %117 = load i32, i32* %Ah, align 4, !tbaa !6
  %cmp101 = icmp slt i32 %117, 0
  br i1 %cmp101, label %if.then108, label %lor.lhs.false102

lor.lhs.false102:                                 ; preds = %lor.lhs.false100
  %118 = load i32, i32* %Ah, align 4, !tbaa !6
  %cmp103 = icmp sgt i32 %118, 10
  br i1 %cmp103, label %if.then108, label %lor.lhs.false104

lor.lhs.false104:                                 ; preds = %lor.lhs.false102
  %119 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp105 = icmp slt i32 %119, 0
  br i1 %cmp105, label %if.then108, label %lor.lhs.false106

lor.lhs.false106:                                 ; preds = %lor.lhs.false104
  %120 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp107 = icmp sgt i32 %120, 10
  br i1 %cmp107, label %if.then108, label %if.end117

if.then108:                                       ; preds = %lor.lhs.false106, %lor.lhs.false104, %lor.lhs.false102, %lor.lhs.false100, %lor.lhs.false98, %lor.lhs.false96, %lor.lhs.false94, %if.then92
  %121 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err109 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %121, i32 0, i32 0
  %122 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err109, align 8, !tbaa !74
  %msg_code110 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %122, i32 0, i32 5
  store i32 17, i32* %msg_code110, align 4, !tbaa !75
  %123 = load i32, i32* %scanno, align 4, !tbaa !6
  %124 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err111 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %124, i32 0, i32 0
  %125 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err111, align 8, !tbaa !74
  %msg_parm112 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %125, i32 0, i32 6
  %i113 = bitcast %union.anon* %msg_parm112 to [8 x i32]*
  %arrayidx114 = getelementptr inbounds [8 x i32], [8 x i32]* %i113, i32 0, i32 0
  store i32 %123, i32* %arrayidx114, align 4, !tbaa !89
  %126 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err115 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %126, i32 0, i32 0
  %127 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err115, align 8, !tbaa !74
  %error_exit116 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %127, i32 0, i32 0
  %128 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit116, align 4, !tbaa !77
  %129 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %130 = bitcast %struct.jpeg_compress_struct* %129 to %struct.jpeg_common_struct*
  call void %128(%struct.jpeg_common_struct* %130)
  br label %if.end117

if.end117:                                        ; preds = %if.then108, %lor.lhs.false106
  %131 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp118 = icmp eq i32 %131, 0
  br i1 %cmp118, label %if.then119, label %if.else131

if.then119:                                       ; preds = %if.end117
  %132 = load i32, i32* %Se, align 4, !tbaa !6
  %cmp120 = icmp ne i32 %132, 0
  br i1 %cmp120, label %if.then121, label %if.end130

if.then121:                                       ; preds = %if.then119
  %133 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err122 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %133, i32 0, i32 0
  %134 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err122, align 8, !tbaa !74
  %msg_code123 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %134, i32 0, i32 5
  store i32 17, i32* %msg_code123, align 4, !tbaa !75
  %135 = load i32, i32* %scanno, align 4, !tbaa !6
  %136 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err124 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %136, i32 0, i32 0
  %137 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err124, align 8, !tbaa !74
  %msg_parm125 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %137, i32 0, i32 6
  %i126 = bitcast %union.anon* %msg_parm125 to [8 x i32]*
  %arrayidx127 = getelementptr inbounds [8 x i32], [8 x i32]* %i126, i32 0, i32 0
  store i32 %135, i32* %arrayidx127, align 4, !tbaa !89
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err128 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 0
  %139 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err128, align 8, !tbaa !74
  %error_exit129 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %139, i32 0, i32 0
  %140 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit129, align 4, !tbaa !77
  %141 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %142 = bitcast %struct.jpeg_compress_struct* %141 to %struct.jpeg_common_struct*
  call void %140(%struct.jpeg_common_struct* %142)
  br label %if.end130

if.end130:                                        ; preds = %if.then121, %if.then119
  br label %if.end143

if.else131:                                       ; preds = %if.end117
  %143 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp132 = icmp ne i32 %143, 1
  br i1 %cmp132, label %if.then133, label %if.end142

if.then133:                                       ; preds = %if.else131
  %144 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err134 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %144, i32 0, i32 0
  %145 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err134, align 8, !tbaa !74
  %msg_code135 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %145, i32 0, i32 5
  store i32 17, i32* %msg_code135, align 4, !tbaa !75
  %146 = load i32, i32* %scanno, align 4, !tbaa !6
  %147 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err136 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %147, i32 0, i32 0
  %148 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err136, align 8, !tbaa !74
  %msg_parm137 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %148, i32 0, i32 6
  %i138 = bitcast %union.anon* %msg_parm137 to [8 x i32]*
  %arrayidx139 = getelementptr inbounds [8 x i32], [8 x i32]* %i138, i32 0, i32 0
  store i32 %146, i32* %arrayidx139, align 4, !tbaa !89
  %149 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err140 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %149, i32 0, i32 0
  %150 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err140, align 8, !tbaa !74
  %error_exit141 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %150, i32 0, i32 0
  %151 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit141, align 4, !tbaa !77
  %152 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %153 = bitcast %struct.jpeg_compress_struct* %152 to %struct.jpeg_common_struct*
  call void %151(%struct.jpeg_common_struct* %153)
  br label %if.end142

if.end142:                                        ; preds = %if.then133, %if.else131
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %if.end130
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond144

for.cond144:                                      ; preds = %for.inc203, %if.end143
  %154 = load i32, i32* %ci, align 4, !tbaa !6
  %155 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp145 = icmp slt i32 %154, %155
  br i1 %cmp145, label %for.body146, label %for.end205

for.body146:                                      ; preds = %for.cond144
  %156 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index147 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %156, i32 0, i32 1
  %157 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index147, i32 0, i32 %157
  %158 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  %arrayidx149 = getelementptr inbounds [10 x [64 x i32]], [10 x [64 x i32]]* %last_bitpos, i32 0, i32 %158
  %arrayidx150 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx149, i32 0, i32 0
  store i32* %arrayidx150, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %159 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp151 = icmp ne i32 %159, 0
  br i1 %cmp151, label %land.lhs.true152, label %if.end164

land.lhs.true152:                                 ; preds = %for.body146
  %160 = load i32*, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %160, i32 0
  %161 = load i32, i32* %arrayidx153, align 4, !tbaa !6
  %cmp154 = icmp slt i32 %161, 0
  br i1 %cmp154, label %if.then155, label %if.end164

if.then155:                                       ; preds = %land.lhs.true152
  %162 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err156 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %162, i32 0, i32 0
  %163 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err156, align 8, !tbaa !74
  %msg_code157 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %163, i32 0, i32 5
  store i32 17, i32* %msg_code157, align 4, !tbaa !75
  %164 = load i32, i32* %scanno, align 4, !tbaa !6
  %165 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err158 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %165, i32 0, i32 0
  %166 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err158, align 8, !tbaa !74
  %msg_parm159 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %166, i32 0, i32 6
  %i160 = bitcast %union.anon* %msg_parm159 to [8 x i32]*
  %arrayidx161 = getelementptr inbounds [8 x i32], [8 x i32]* %i160, i32 0, i32 0
  store i32 %164, i32* %arrayidx161, align 4, !tbaa !89
  %167 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err162 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %167, i32 0, i32 0
  %168 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err162, align 8, !tbaa !74
  %error_exit163 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %168, i32 0, i32 0
  %169 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit163, align 4, !tbaa !77
  %170 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %171 = bitcast %struct.jpeg_compress_struct* %170 to %struct.jpeg_common_struct*
  call void %169(%struct.jpeg_common_struct* %171)
  br label %if.end164

if.end164:                                        ; preds = %if.then155, %land.lhs.true152, %for.body146
  %172 = load i32, i32* %Ss, align 4, !tbaa !6
  store i32 %172, i32* %coefi, align 4, !tbaa !6
  br label %for.cond165

for.cond165:                                      ; preds = %for.inc200, %if.end164
  %173 = load i32, i32* %coefi, align 4, !tbaa !6
  %174 = load i32, i32* %Se, align 4, !tbaa !6
  %cmp166 = icmp sle i32 %173, %174
  br i1 %cmp166, label %for.body167, label %for.end202

for.body167:                                      ; preds = %for.cond165
  %175 = load i32*, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %176 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx168 = getelementptr inbounds i32, i32* %175, i32 %176
  %177 = load i32, i32* %arrayidx168, align 4, !tbaa !6
  %cmp169 = icmp slt i32 %177, 0
  br i1 %cmp169, label %if.then170, label %if.else182

if.then170:                                       ; preds = %for.body167
  %178 = load i32, i32* %Ah, align 4, !tbaa !6
  %cmp171 = icmp ne i32 %178, 0
  br i1 %cmp171, label %if.then172, label %if.end181

if.then172:                                       ; preds = %if.then170
  %179 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err173 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %179, i32 0, i32 0
  %180 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err173, align 8, !tbaa !74
  %msg_code174 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %180, i32 0, i32 5
  store i32 17, i32* %msg_code174, align 4, !tbaa !75
  %181 = load i32, i32* %scanno, align 4, !tbaa !6
  %182 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err175 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %182, i32 0, i32 0
  %183 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err175, align 8, !tbaa !74
  %msg_parm176 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %183, i32 0, i32 6
  %i177 = bitcast %union.anon* %msg_parm176 to [8 x i32]*
  %arrayidx178 = getelementptr inbounds [8 x i32], [8 x i32]* %i177, i32 0, i32 0
  store i32 %181, i32* %arrayidx178, align 4, !tbaa !89
  %184 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err179 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %184, i32 0, i32 0
  %185 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err179, align 8, !tbaa !74
  %error_exit180 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %185, i32 0, i32 0
  %186 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit180, align 4, !tbaa !77
  %187 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %188 = bitcast %struct.jpeg_compress_struct* %187 to %struct.jpeg_common_struct*
  call void %186(%struct.jpeg_common_struct* %188)
  br label %if.end181

if.end181:                                        ; preds = %if.then172, %if.then170
  br label %if.end198

if.else182:                                       ; preds = %for.body167
  %189 = load i32, i32* %Ah, align 4, !tbaa !6
  %190 = load i32*, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %191 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx183 = getelementptr inbounds i32, i32* %190, i32 %191
  %192 = load i32, i32* %arrayidx183, align 4, !tbaa !6
  %cmp184 = icmp ne i32 %189, %192
  br i1 %cmp184, label %if.then188, label %lor.lhs.false185

lor.lhs.false185:                                 ; preds = %if.else182
  %193 = load i32, i32* %Al, align 4, !tbaa !6
  %194 = load i32, i32* %Ah, align 4, !tbaa !6
  %sub186 = sub nsw i32 %194, 1
  %cmp187 = icmp ne i32 %193, %sub186
  br i1 %cmp187, label %if.then188, label %if.end197

if.then188:                                       ; preds = %lor.lhs.false185, %if.else182
  %195 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err189 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %195, i32 0, i32 0
  %196 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err189, align 8, !tbaa !74
  %msg_code190 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %196, i32 0, i32 5
  store i32 17, i32* %msg_code190, align 4, !tbaa !75
  %197 = load i32, i32* %scanno, align 4, !tbaa !6
  %198 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err191 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %198, i32 0, i32 0
  %199 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err191, align 8, !tbaa !74
  %msg_parm192 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %199, i32 0, i32 6
  %i193 = bitcast %union.anon* %msg_parm192 to [8 x i32]*
  %arrayidx194 = getelementptr inbounds [8 x i32], [8 x i32]* %i193, i32 0, i32 0
  store i32 %197, i32* %arrayidx194, align 4, !tbaa !89
  %200 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err195 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %200, i32 0, i32 0
  %201 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err195, align 8, !tbaa !74
  %error_exit196 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %201, i32 0, i32 0
  %202 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit196, align 4, !tbaa !77
  %203 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %204 = bitcast %struct.jpeg_compress_struct* %203 to %struct.jpeg_common_struct*
  call void %202(%struct.jpeg_common_struct* %204)
  br label %if.end197

if.end197:                                        ; preds = %if.then188, %lor.lhs.false185
  br label %if.end198

if.end198:                                        ; preds = %if.end197, %if.end181
  %205 = load i32, i32* %Al, align 4, !tbaa !6
  %206 = load i32*, i32** %last_bitpos_ptr, align 4, !tbaa !2
  %207 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx199 = getelementptr inbounds i32, i32* %206, i32 %207
  store i32 %205, i32* %arrayidx199, align 4, !tbaa !6
  br label %for.inc200

for.inc200:                                       ; preds = %if.end198
  %208 = load i32, i32* %coefi, align 4, !tbaa !6
  %inc201 = add nsw i32 %208, 1
  store i32 %inc201, i32* %coefi, align 4, !tbaa !6
  br label %for.cond165

for.end202:                                       ; preds = %for.cond165
  br label %for.inc203

for.inc203:                                       ; preds = %for.end202
  %209 = load i32, i32* %ci, align 4, !tbaa !6
  %inc204 = add nsw i32 %209, 1
  store i32 %inc204, i32* %ci, align 4, !tbaa !6
  br label %for.cond144

for.end205:                                       ; preds = %for.cond144
  br label %if.end245

if.else206:                                       ; preds = %for.end85
  %210 = load i32, i32* %Ss, align 4, !tbaa !6
  %cmp207 = icmp ne i32 %210, 0
  br i1 %cmp207, label %if.then214, label %lor.lhs.false208

lor.lhs.false208:                                 ; preds = %if.else206
  %211 = load i32, i32* %Se, align 4, !tbaa !6
  %cmp209 = icmp ne i32 %211, 63
  br i1 %cmp209, label %if.then214, label %lor.lhs.false210

lor.lhs.false210:                                 ; preds = %lor.lhs.false208
  %212 = load i32, i32* %Ah, align 4, !tbaa !6
  %cmp211 = icmp ne i32 %212, 0
  br i1 %cmp211, label %if.then214, label %lor.lhs.false212

lor.lhs.false212:                                 ; preds = %lor.lhs.false210
  %213 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp213 = icmp ne i32 %213, 0
  br i1 %cmp213, label %if.then214, label %if.end223

if.then214:                                       ; preds = %lor.lhs.false212, %lor.lhs.false210, %lor.lhs.false208, %if.else206
  %214 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err215 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %214, i32 0, i32 0
  %215 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err215, align 8, !tbaa !74
  %msg_code216 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %215, i32 0, i32 5
  store i32 17, i32* %msg_code216, align 4, !tbaa !75
  %216 = load i32, i32* %scanno, align 4, !tbaa !6
  %217 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err217 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %217, i32 0, i32 0
  %218 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err217, align 8, !tbaa !74
  %msg_parm218 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %218, i32 0, i32 6
  %i219 = bitcast %union.anon* %msg_parm218 to [8 x i32]*
  %arrayidx220 = getelementptr inbounds [8 x i32], [8 x i32]* %i219, i32 0, i32 0
  store i32 %216, i32* %arrayidx220, align 4, !tbaa !89
  %219 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err221 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %219, i32 0, i32 0
  %220 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err221, align 8, !tbaa !74
  %error_exit222 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %220, i32 0, i32 0
  %221 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit222, align 4, !tbaa !77
  %222 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %223 = bitcast %struct.jpeg_compress_struct* %222 to %struct.jpeg_common_struct*
  call void %221(%struct.jpeg_common_struct* %223)
  br label %if.end223

if.end223:                                        ; preds = %if.then214, %lor.lhs.false212
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond224

for.cond224:                                      ; preds = %for.inc242, %if.end223
  %224 = load i32, i32* %ci, align 4, !tbaa !6
  %225 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp225 = icmp slt i32 %224, %225
  br i1 %cmp225, label %for.body226, label %for.end244

for.body226:                                      ; preds = %for.cond224
  %226 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index227 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %226, i32 0, i32 1
  %227 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx228 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index227, i32 0, i32 %227
  %228 = load i32, i32* %arrayidx228, align 4, !tbaa !6
  store i32 %228, i32* %thisi, align 4, !tbaa !6
  %229 = load i32, i32* %thisi, align 4, !tbaa !6
  %arrayidx229 = getelementptr inbounds [10 x i32], [10 x i32]* %component_sent, i32 0, i32 %229
  %230 = load i32, i32* %arrayidx229, align 4, !tbaa !6
  %tobool230 = icmp ne i32 %230, 0
  br i1 %tobool230, label %if.then231, label %if.end240

if.then231:                                       ; preds = %for.body226
  %231 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err232 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %231, i32 0, i32 0
  %232 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err232, align 8, !tbaa !74
  %msg_code233 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %232, i32 0, i32 5
  store i32 19, i32* %msg_code233, align 4, !tbaa !75
  %233 = load i32, i32* %scanno, align 4, !tbaa !6
  %234 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err234 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %234, i32 0, i32 0
  %235 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err234, align 8, !tbaa !74
  %msg_parm235 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %235, i32 0, i32 6
  %i236 = bitcast %union.anon* %msg_parm235 to [8 x i32]*
  %arrayidx237 = getelementptr inbounds [8 x i32], [8 x i32]* %i236, i32 0, i32 0
  store i32 %233, i32* %arrayidx237, align 4, !tbaa !89
  %236 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err238 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %236, i32 0, i32 0
  %237 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err238, align 8, !tbaa !74
  %error_exit239 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %237, i32 0, i32 0
  %238 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit239, align 4, !tbaa !77
  %239 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %240 = bitcast %struct.jpeg_compress_struct* %239 to %struct.jpeg_common_struct*
  call void %238(%struct.jpeg_common_struct* %240)
  br label %if.end240

if.end240:                                        ; preds = %if.then231, %for.body226
  %241 = load i32, i32* %thisi, align 4, !tbaa !6
  %arrayidx241 = getelementptr inbounds [10 x i32], [10 x i32]* %component_sent, i32 0, i32 %241
  store i32 1, i32* %arrayidx241, align 4, !tbaa !6
  br label %for.inc242

for.inc242:                                       ; preds = %if.end240
  %242 = load i32, i32* %ci, align 4, !tbaa !6
  %inc243 = add nsw i32 %242, 1
  store i32 %inc243, i32* %ci, align 4, !tbaa !6
  br label %for.cond224

for.end244:                                       ; preds = %for.cond224
  br label %if.end245

if.end245:                                        ; preds = %for.end244, %for.end205
  br label %for.inc246

for.inc246:                                       ; preds = %if.end245
  %243 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %incdec.ptr247 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %243, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr247, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %244 = load i32, i32* %scanno, align 4, !tbaa !6
  %inc248 = add nsw i32 %244, 1
  store i32 %inc248, i32* %scanno, align 4, !tbaa !6
  br label %for.cond30

for.end249:                                       ; preds = %for.cond30
  %245 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode250 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %245, i32 0, i32 40
  %246 = load i32, i32* %progressive_mode250, align 4, !tbaa !22
  %tobool251 = icmp ne i32 %246, 0
  br i1 %tobool251, label %if.then252, label %if.else269

if.then252:                                       ; preds = %for.end249
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond253

for.cond253:                                      ; preds = %for.inc266, %if.then252
  %247 = load i32, i32* %ci, align 4, !tbaa !6
  %248 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components254 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %248, i32 0, i32 13
  %249 = load i32, i32* %num_components254, align 4, !tbaa !34
  %cmp255 = icmp slt i32 %247, %249
  br i1 %cmp255, label %for.body256, label %for.end268

for.body256:                                      ; preds = %for.cond253
  %250 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx257 = getelementptr inbounds [10 x [64 x i32]], [10 x [64 x i32]]* %last_bitpos, i32 0, i32 %250
  %arrayidx258 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx257, i32 0, i32 0
  %251 = load i32, i32* %arrayidx258, align 16, !tbaa !6
  %cmp259 = icmp slt i32 %251, 0
  br i1 %cmp259, label %if.then260, label %if.end265

if.then260:                                       ; preds = %for.body256
  %252 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err261 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %252, i32 0, i32 0
  %253 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err261, align 8, !tbaa !74
  %msg_code262 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %253, i32 0, i32 5
  store i32 45, i32* %msg_code262, align 4, !tbaa !75
  %254 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err263 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %254, i32 0, i32 0
  %255 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err263, align 8, !tbaa !74
  %error_exit264 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %255, i32 0, i32 0
  %256 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit264, align 4, !tbaa !77
  %257 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %258 = bitcast %struct.jpeg_compress_struct* %257 to %struct.jpeg_common_struct*
  call void %256(%struct.jpeg_common_struct* %258)
  br label %if.end265

if.end265:                                        ; preds = %if.then260, %for.body256
  br label %for.inc266

for.inc266:                                       ; preds = %if.end265
  %259 = load i32, i32* %ci, align 4, !tbaa !6
  %inc267 = add nsw i32 %259, 1
  store i32 %inc267, i32* %ci, align 4, !tbaa !6
  br label %for.cond253

for.end268:                                       ; preds = %for.cond253
  br label %if.end285

if.else269:                                       ; preds = %for.end249
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond270

for.cond270:                                      ; preds = %for.inc282, %if.else269
  %260 = load i32, i32* %ci, align 4, !tbaa !6
  %261 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components271 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %261, i32 0, i32 13
  %262 = load i32, i32* %num_components271, align 4, !tbaa !34
  %cmp272 = icmp slt i32 %260, %262
  br i1 %cmp272, label %for.body273, label %for.end284

for.body273:                                      ; preds = %for.cond270
  %263 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx274 = getelementptr inbounds [10 x i32], [10 x i32]* %component_sent, i32 0, i32 %263
  %264 = load i32, i32* %arrayidx274, align 4, !tbaa !6
  %tobool275 = icmp ne i32 %264, 0
  br i1 %tobool275, label %if.end281, label %if.then276

if.then276:                                       ; preds = %for.body273
  %265 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err277 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %265, i32 0, i32 0
  %266 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err277, align 8, !tbaa !74
  %msg_code278 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %266, i32 0, i32 5
  store i32 45, i32* %msg_code278, align 4, !tbaa !75
  %267 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err279 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %267, i32 0, i32 0
  %268 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err279, align 8, !tbaa !74
  %error_exit280 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %268, i32 0, i32 0
  %269 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit280, align 4, !tbaa !77
  %270 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %271 = bitcast %struct.jpeg_compress_struct* %270 to %struct.jpeg_common_struct*
  call void %269(%struct.jpeg_common_struct* %271)
  br label %if.end281

if.end281:                                        ; preds = %if.then276, %for.body273
  br label %for.inc282

for.inc282:                                       ; preds = %if.end281
  %272 = load i32, i32* %ci, align 4, !tbaa !6
  %inc283 = add nsw i32 %272, 1
  store i32 %inc283, i32* %ci, align 4, !tbaa !6
  br label %for.cond270

for.end284:                                       ; preds = %for.cond270
  br label %if.end285

if.end285:                                        ; preds = %for.end284, %for.end268
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end285, %if.then
  %273 = bitcast [10 x [64 x i32]]* %last_bitpos to i8*
  call void @llvm.lifetime.end.p0i8(i64 2560, i8* %273) #3
  %274 = bitcast i32** %last_bitpos_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #3
  %275 = bitcast [10 x i32]* %component_sent to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %275) #3
  %276 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #3
  %277 = bitcast i32* %Ah to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #3
  %278 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #3
  %279 = bitcast i32* %Ss to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #3
  %280 = bitcast i32* %thisi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #3
  %281 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #3
  %282 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #3
  %283 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #3
  %284 = bitcast i32* %scanno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #3
  %285 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @select_scan_parameters(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %ci = alloca i32, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %scanptr = alloca %struct.jpeg_scan_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 54
  %3 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %4 = bitcast %struct.jpeg_comp_master* %3 to %struct.my_comp_master*
  store %struct.my_comp_master* %4, %struct.my_comp_master** %master, align 4, !tbaa !2
  %5 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %5, i32 0, i32 2
  %6 = load i32, i32* %pass_number, align 4, !tbaa !28
  %7 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %7, i32 0, i32 5
  %8 = load i32, i32* %pass_number_scan_opt_base, align 8, !tbaa !31
  %cmp = icmp slt i32 %6, %8
  br i1 %cmp, label %if.then, label %if.else30

if.then:                                          ; preds = %entry
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 44
  store i32 1, i32* %comps_in_scan, align 4, !tbaa !111
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 54
  %11 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !8
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %11, i32 0, i32 10
  %12 = load i32, i32* %use_scans_in_trellis, align 8, !tbaa !33
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 15
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !93
  %15 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number4 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %15, i32 0, i32 2
  %16 = load i32, i32* %pass_number4, align 4, !tbaa !28
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 54
  %18 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master5, align 4, !tbaa !8
  %trellis_num_loops = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %18, i32 0, i32 20
  %19 = load i32, i32* %trellis_num_loops, align 8, !tbaa !35
  %mul = mul nsw i32 4, %19
  %div = sdiv i32 %16, %mul
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %14, i32 %div
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 45
  %arrayidx6 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  store %struct.jpeg_component_info* %arrayidx, %struct.jpeg_component_info** %arrayidx6, align 8, !tbaa !2
  %21 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number7 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %21, i32 0, i32 2
  %22 = load i32, i32* %pass_number7, align 4, !tbaa !28
  %rem = srem i32 %22, 4
  %cmp8 = icmp slt i32 %rem, 2
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then3
  br label %cond.end

cond.false:                                       ; preds = %if.then3
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 54
  %24 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master9, align 4, !tbaa !8
  %trellis_freq_split = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %24, i32 0, i32 19
  %25 = load i32, i32* %trellis_freq_split, align 4, !tbaa !112
  %add = add nsw i32 %25, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %add, %cond.false ]
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 50
  store i32 %cond, i32* %Ss, align 4, !tbaa !61
  %27 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number10 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %27, i32 0, i32 2
  %28 = load i32, i32* %pass_number10, align 4, !tbaa !28
  %rem11 = srem i32 %28, 4
  %cmp12 = icmp slt i32 %rem11, 2
  br i1 %cmp12, label %cond.true13, label %cond.false16

cond.true13:                                      ; preds = %cond.end
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 54
  %30 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master14, align 4, !tbaa !8
  %trellis_freq_split15 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %30, i32 0, i32 19
  %31 = load i32, i32* %trellis_freq_split15, align 4, !tbaa !112
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true13
  %cond18 = phi i32 [ %31, %cond.true13 ], [ 63, %cond.false16 ]
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 51
  store i32 %cond18, i32* %Se, align 8, !tbaa !113
  br label %if.end

if.else:                                          ; preds = %if.then
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %33, i32 0, i32 15
  %34 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info19, align 4, !tbaa !93
  %35 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number20 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %35, i32 0, i32 2
  %36 = load i32, i32* %pass_number20, align 4, !tbaa !28
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %37, i32 0, i32 54
  %38 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master21, align 4, !tbaa !8
  %trellis_num_loops22 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %38, i32 0, i32 20
  %39 = load i32, i32* %trellis_num_loops22, align 8, !tbaa !35
  %mul23 = mul nsw i32 2, %39
  %div24 = sdiv i32 %36, %mul23
  %arrayidx25 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %34, i32 %div24
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info26 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %40, i32 0, i32 45
  %arrayidx27 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info26, i32 0, i32 0
  store %struct.jpeg_component_info* %arrayidx25, %struct.jpeg_component_info** %arrayidx27, align 8, !tbaa !2
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss28 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 50
  store i32 1, i32* %Ss28, align 4, !tbaa !61
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se29 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 51
  store i32 63, i32* %Se29, align 8, !tbaa !113
  br label %if.end

if.end:                                           ; preds = %if.else, %cond.end17
  br label %if.end115

if.else30:                                        ; preds = %entry
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 23
  %44 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !21
  %cmp31 = icmp ne %struct.jpeg_scan_info* %44, null
  br i1 %cmp31, label %if.then32, label %if.else85

if.then32:                                        ; preds = %if.else30
  %45 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 23
  %47 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info33, align 4, !tbaa !21
  %48 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %48, i32 0, i32 4
  %49 = load i32, i32* %scan_number, align 4, !tbaa !27
  %add.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %47, i32 %49
  store %struct.jpeg_scan_info* %add.ptr, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %50 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %comps_in_scan34 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %50, i32 0, i32 0
  %51 = load i32, i32* %comps_in_scan34, align 4, !tbaa !108
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 44
  store i32 %51, i32* %comps_in_scan35, align 4, !tbaa !111
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then32
  %53 = load i32, i32* %ci, align 4, !tbaa !6
  %54 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %comps_in_scan36 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %54, i32 0, i32 0
  %55 = load i32, i32* %comps_in_scan36, align 4, !tbaa !108
  %cmp37 = icmp slt i32 %53, %55
  br i1 %cmp37, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 15
  %57 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info38, align 4, !tbaa !93
  %58 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %58, i32 0, i32 1
  %59 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %57, i32 %60
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info41 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 45
  %62 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info41, i32 0, i32 %62
  store %struct.jpeg_component_info* %arrayidx40, %struct.jpeg_component_info** %arrayidx42, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %63 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %64 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ss43 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %64, i32 0, i32 2
  %65 = load i32, i32* %Ss43, align 4, !tbaa !105
  %66 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %66, i32 0, i32 50
  store i32 %65, i32* %Ss44, align 4, !tbaa !61
  %67 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Se45 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %67, i32 0, i32 3
  %68 = load i32, i32* %Se45, align 4, !tbaa !107
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 51
  store i32 %68, i32* %Se46, align 8, !tbaa !113
  %70 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %70, i32 0, i32 4
  %71 = load i32, i32* %Ah, align 4, !tbaa !109
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah47 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %72, i32 0, i32 52
  store i32 %71, i32* %Ah47, align 4, !tbaa !62
  %73 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %73, i32 0, i32 5
  %74 = load i32, i32* %Al, align 4, !tbaa !110
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al48 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 53
  store i32 %74, i32* %Al48, align 8, !tbaa !114
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master49 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %76, i32 0, i32 54
  %77 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master49, align 4, !tbaa !8
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %77, i32 0, i32 5
  %78 = load i32, i32* %optimize_scans, align 4, !tbaa !36
  %tobool50 = icmp ne i32 %78, 0
  br i1 %tobool50, label %if.then51, label %if.end81

if.then51:                                        ; preds = %for.end
  %79 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number52 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %79, i32 0, i32 4
  %80 = load i32, i32* %scan_number52, align 4, !tbaa !27
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master53 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 54
  %82 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master53, align 4, !tbaa !8
  %num_scans_luma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %82, i32 0, i32 22
  %83 = load i32, i32* %num_scans_luma_dc, align 8, !tbaa !115
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master54 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %84, i32 0, i32 54
  %85 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master54, align 4, !tbaa !8
  %Al_max_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %85, i32 0, i32 25
  %86 = load i32, i32* %Al_max_luma, align 4, !tbaa !116
  %mul55 = mul nsw i32 3, %86
  %add56 = add nsw i32 %83, %mul55
  %add57 = add nsw i32 %add56, 2
  %cmp58 = icmp sge i32 %80, %add57
  br i1 %cmp58, label %land.lhs.true, label %if.end64

land.lhs.true:                                    ; preds = %if.then51
  %87 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number59 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %87, i32 0, i32 4
  %88 = load i32, i32* %scan_number59, align 4, !tbaa !27
  %89 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master60 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %89, i32 0, i32 54
  %90 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master60, align 4, !tbaa !8
  %num_scans_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %90, i32 0, i32 21
  %91 = load i32, i32* %num_scans_luma, align 4, !tbaa !117
  %cmp61 = icmp slt i32 %88, %91
  br i1 %cmp61, label %if.then62, label %if.end64

if.then62:                                        ; preds = %land.lhs.true
  %92 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_luma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %92, i32 0, i32 12
  %93 = load i32, i32* %best_Al_luma, align 8, !tbaa !118
  %94 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al63 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %94, i32 0, i32 53
  store i32 %93, i32* %Al63, align 8, !tbaa !114
  br label %if.end64

if.end64:                                         ; preds = %if.then62, %land.lhs.true, %if.then51
  %95 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number65 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %95, i32 0, i32 4
  %96 = load i32, i32* %scan_number65, align 4, !tbaa !27
  %97 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master66 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %97, i32 0, i32 54
  %98 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master66, align 4, !tbaa !8
  %num_scans_luma67 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %98, i32 0, i32 21
  %99 = load i32, i32* %num_scans_luma67, align 4, !tbaa !117
  %100 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master68 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %100, i32 0, i32 54
  %101 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master68, align 4, !tbaa !8
  %num_scans_chroma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %101, i32 0, i32 23
  %102 = load i32, i32* %num_scans_chroma_dc, align 4, !tbaa !119
  %add69 = add nsw i32 %99, %102
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master70 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 54
  %104 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master70, align 4, !tbaa !8
  %Al_max_chroma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %104, i32 0, i32 26
  %105 = load i32, i32* %Al_max_chroma, align 8, !tbaa !120
  %mul71 = mul nsw i32 6, %105
  %add72 = add nsw i32 %mul71, 4
  %add73 = add nsw i32 %add69, %add72
  %cmp74 = icmp sge i32 %96, %add73
  br i1 %cmp74, label %land.lhs.true75, label %if.end80

land.lhs.true75:                                  ; preds = %if.end64
  %106 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number76 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %106, i32 0, i32 4
  %107 = load i32, i32* %scan_number76, align 4, !tbaa !27
  %108 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %108, i32 0, i32 22
  %109 = load i32, i32* %num_scans, align 8, !tbaa !23
  %cmp77 = icmp slt i32 %107, %109
  br i1 %cmp77, label %if.then78, label %if.end80

if.then78:                                        ; preds = %land.lhs.true75
  %110 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %110, i32 0, i32 13
  %111 = load i32, i32* %best_Al_chroma, align 4, !tbaa !37
  %112 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al79 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %112, i32 0, i32 53
  store i32 %111, i32* %Al79, align 8, !tbaa !114
  br label %if.end80

if.end80:                                         ; preds = %if.then78, %land.lhs.true75, %if.end64
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %for.end
  %113 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al82 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %113, i32 0, i32 53
  %114 = load i32, i32* %Al82, align 8, !tbaa !114
  %115 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %actual_Al = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %115, i32 0, i32 8
  %116 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number83 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %116, i32 0, i32 4
  %117 = load i32, i32* %scan_number83, align 4, !tbaa !27
  %arrayidx84 = getelementptr inbounds [64 x i32], [64 x i32]* %actual_Al, i32 0, i32 %117
  store i32 %114, i32* %arrayidx84, align 4, !tbaa !6
  %118 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  br label %if.end114

if.else85:                                        ; preds = %if.else30
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 13
  %120 = load i32, i32* %num_components, align 4, !tbaa !34
  %cmp86 = icmp sgt i32 %120, 4
  br i1 %cmp86, label %if.then87, label %if.end96

if.then87:                                        ; preds = %if.else85
  %121 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %121, i32 0, i32 0
  %122 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %122, i32 0, i32 5
  store i32 26, i32* %msg_code, align 4, !tbaa !75
  %123 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components88 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %123, i32 0, i32 13
  %124 = load i32, i32* %num_components88, align 4, !tbaa !34
  %125 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err89 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %125, i32 0, i32 0
  %126 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err89, align 8, !tbaa !74
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %126, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx90 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %124, i32* %arrayidx90, align 4, !tbaa !89
  %127 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err91 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %127, i32 0, i32 0
  %128 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err91, align 8, !tbaa !74
  %msg_parm92 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %128, i32 0, i32 6
  %i93 = bitcast %union.anon* %msg_parm92 to [8 x i32]*
  %arrayidx94 = getelementptr inbounds [8 x i32], [8 x i32]* %i93, i32 0, i32 1
  store i32 4, i32* %arrayidx94, align 4, !tbaa !89
  %129 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err95 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %129, i32 0, i32 0
  %130 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err95, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %130, i32 0, i32 0
  %131 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %132 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %133 = bitcast %struct.jpeg_compress_struct* %132 to %struct.jpeg_common_struct*
  call void %131(%struct.jpeg_common_struct* %133)
  br label %if.end96

if.end96:                                         ; preds = %if.then87, %if.else85
  %134 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components97 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %134, i32 0, i32 13
  %135 = load i32, i32* %num_components97, align 4, !tbaa !34
  %136 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan98 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %136, i32 0, i32 44
  store i32 %135, i32* %comps_in_scan98, align 4, !tbaa !111
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc107, %if.end96
  %137 = load i32, i32* %ci, align 4, !tbaa !6
  %138 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components100 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %138, i32 0, i32 13
  %139 = load i32, i32* %num_components100, align 4, !tbaa !34
  %cmp101 = icmp slt i32 %137, %139
  br i1 %cmp101, label %for.body102, label %for.end109

for.body102:                                      ; preds = %for.cond99
  %140 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info103 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %140, i32 0, i32 15
  %141 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info103, align 4, !tbaa !93
  %142 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx104 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %141, i32 %142
  %143 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info105 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %143, i32 0, i32 45
  %144 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info105, i32 0, i32 %144
  store %struct.jpeg_component_info* %arrayidx104, %struct.jpeg_component_info** %arrayidx106, align 4, !tbaa !2
  br label %for.inc107

for.inc107:                                       ; preds = %for.body102
  %145 = load i32, i32* %ci, align 4, !tbaa !6
  %inc108 = add nsw i32 %145, 1
  store i32 %inc108, i32* %ci, align 4, !tbaa !6
  br label %for.cond99

for.end109:                                       ; preds = %for.cond99
  %146 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss110 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %146, i32 0, i32 50
  store i32 0, i32* %Ss110, align 4, !tbaa !61
  %147 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se111 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %147, i32 0, i32 51
  store i32 63, i32* %Se111, align 8, !tbaa !113
  %148 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah112 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %148, i32 0, i32 52
  store i32 0, i32* %Ah112, align 4, !tbaa !62
  %149 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al113 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %149, i32 0, i32 53
  store i32 0, i32* %Al113, align 8, !tbaa !114
  br label %if.end114

if.end114:                                        ; preds = %for.end109, %if.end81
  br label %if.end115

if.end115:                                        ; preds = %if.end114, %if.end
  %150 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  %151 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  ret void
}

; Function Attrs: nounwind
define internal void @per_scan_setup(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %ci = alloca i32, align 4
  %mcublks = alloca i32, align 4
  %tmp = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %nominal = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %mcublks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 44
  %5 = load i32, i32* %comps_in_scan, align 4, !tbaa !111
  %cmp = icmp eq i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 45
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %7 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 8, !tbaa !2
  store %struct.jpeg_component_info* %7, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %8 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %8, i32 0, i32 7
  %9 = load i32, i32* %width_in_blocks, align 4, !tbaa !99
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 46
  store i32 %9, i32* %MCUs_per_row, align 8, !tbaa !121
  %11 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %11, i32 0, i32 8
  %12 = load i32, i32* %height_in_blocks, align 4, !tbaa !100
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_rows_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 47
  store i32 %12, i32* %MCU_rows_in_scan, align 4, !tbaa !122
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %14, i32 0, i32 13
  store i32 1, i32* %MCU_width, align 4, !tbaa !123
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %15, i32 0, i32 14
  store i32 1, i32* %MCU_height, align 4, !tbaa !124
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 15
  store i32 1, i32* %MCU_blocks, align 4, !tbaa !125
  %17 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %17, i32 0, i32 16
  store i32 8, i32* %MCU_sample_width, align 4, !tbaa !126
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 17
  store i32 1, i32* %last_col_width, align 4, !tbaa !127
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks1 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %19, i32 0, i32 8
  %20 = load i32, i32* %height_in_blocks1, align 4, !tbaa !100
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 0, i32 3
  %22 = load i32, i32* %v_samp_factor, align 4, !tbaa !96
  %rem = urem i32 %20, %22
  store i32 %rem, i32* %tmp, align 4, !tbaa !6
  %23 = load i32, i32* %tmp, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %23, 0
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor4 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0, i32 3
  %25 = load i32, i32* %v_samp_factor4, align 4, !tbaa !96
  store i32 %25, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %26 = load i32, i32* %tmp, align 4, !tbaa !6
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 18
  store i32 %26, i32* %last_row_height, align 4, !tbaa !128
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 48
  store i32 1, i32* %blocks_in_MCU, align 8, !tbaa !129
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 49
  %arrayidx5 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 0
  store i32 0, i32* %arrayidx5, align 4, !tbaa !6
  br label %if.end69

if.else:                                          ; preds = %entry
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 44
  %31 = load i32, i32* %comps_in_scan6, align 4, !tbaa !111
  %cmp7 = icmp sle i32 %31, 0
  br i1 %cmp7, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 44
  %33 = load i32, i32* %comps_in_scan8, align 4, !tbaa !111
  %cmp9 = icmp sgt i32 %33, 4
  br i1 %cmp9, label %if.then10, label %if.end19

if.then10:                                        ; preds = %lor.lhs.false, %if.else
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 5
  store i32 26, i32* %msg_code, align 4, !tbaa !75
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 44
  %37 = load i32, i32* %comps_in_scan11, align 4, !tbaa !111
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !74
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx13 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %37, i32* %arrayidx13, align 4, !tbaa !89
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !74
  %msg_parm15 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 6
  %i16 = bitcast %union.anon* %msg_parm15 to [8 x i32]*
  %arrayidx17 = getelementptr inbounds [8 x i32], [8 x i32]* %i16, i32 0, i32 1
  store i32 4, i32* %arrayidx17, align 4, !tbaa !89
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 0
  %43 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %43, i32 0, i32 0
  %44 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %46 = bitcast %struct.jpeg_compress_struct* %45 to %struct.jpeg_common_struct*
  call void %44(%struct.jpeg_common_struct* %46)
  br label %if.end19

if.end19:                                         ; preds = %if.then10, %lor.lhs.false
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 7
  %48 = load i32, i32* %image_width, align 4, !tbaa !87
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 41
  %50 = load i32, i32* %max_h_samp_factor, align 8, !tbaa !91
  %mul = mul nsw i32 %50, 8
  %call = call i32 @jdiv_round_up(i32 %48, i32 %mul)
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 46
  store i32 %call, i32* %MCUs_per_row20, align 8, !tbaa !121
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 8
  %53 = load i32, i32* %image_height, align 8, !tbaa !86
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %54, i32 0, i32 42
  %55 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !92
  %mul21 = mul nsw i32 %55, 8
  %call22 = call i32 @jdiv_round_up(i32 %53, i32 %mul21)
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_rows_in_scan23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 47
  store i32 %call22, i32* %MCU_rows_in_scan23, align 4, !tbaa !122
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 48
  store i32 0, i32* %blocks_in_MCU24, align 8, !tbaa !129
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end19
  %58 = load i32, i32* %ci, align 4, !tbaa !6
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 44
  %60 = load i32, i32* %comps_in_scan25, align 4, !tbaa !111
  %cmp26 = icmp slt i32 %58, %60
  br i1 %cmp26, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 45
  %62 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info27, i32 0, i32 %62
  %63 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx28, align 4, !tbaa !2
  store %struct.jpeg_component_info* %63, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 0, i32 2
  %65 = load i32, i32* %h_samp_factor, align 4, !tbaa !94
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width29 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 13
  store i32 %65, i32* %MCU_width29, align 4, !tbaa !123
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor30 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 0, i32 3
  %68 = load i32, i32* %v_samp_factor30, align 4, !tbaa !96
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height31 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 14
  store i32 %68, i32* %MCU_height31, align 4, !tbaa !124
  %70 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width32 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %70, i32 0, i32 13
  %71 = load i32, i32* %MCU_width32, align 4, !tbaa !123
  %72 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height33 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %72, i32 0, i32 14
  %73 = load i32, i32* %MCU_height33, align 4, !tbaa !124
  %mul34 = mul nsw i32 %71, %73
  %74 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks35 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %74, i32 0, i32 15
  store i32 %mul34, i32* %MCU_blocks35, align 4, !tbaa !125
  %75 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %75, i32 0, i32 13
  %76 = load i32, i32* %MCU_width36, align 4, !tbaa !123
  %mul37 = mul nsw i32 %76, 8
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width38 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %77, i32 0, i32 16
  store i32 %mul37, i32* %MCU_sample_width38, align 4, !tbaa !126
  %78 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %78, i32 0, i32 7
  %79 = load i32, i32* %width_in_blocks39, align 4, !tbaa !99
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 13
  %81 = load i32, i32* %MCU_width40, align 4, !tbaa !123
  %rem41 = urem i32 %79, %81
  store i32 %rem41, i32* %tmp, align 4, !tbaa !6
  %82 = load i32, i32* %tmp, align 4, !tbaa !6
  %cmp42 = icmp eq i32 %82, 0
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %for.body
  %83 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width44 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %83, i32 0, i32 13
  %84 = load i32, i32* %MCU_width44, align 4, !tbaa !123
  store i32 %84, i32* %tmp, align 4, !tbaa !6
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %for.body
  %85 = load i32, i32* %tmp, align 4, !tbaa !6
  %86 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width46 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %86, i32 0, i32 17
  store i32 %85, i32* %last_col_width46, align 4, !tbaa !127
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks47 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %87, i32 0, i32 8
  %88 = load i32, i32* %height_in_blocks47, align 4, !tbaa !100
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height48 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %89, i32 0, i32 14
  %90 = load i32, i32* %MCU_height48, align 4, !tbaa !124
  %rem49 = urem i32 %88, %90
  store i32 %rem49, i32* %tmp, align 4, !tbaa !6
  %91 = load i32, i32* %tmp, align 4, !tbaa !6
  %cmp50 = icmp eq i32 %91, 0
  br i1 %cmp50, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.end45
  %92 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height52 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %92, i32 0, i32 14
  %93 = load i32, i32* %MCU_height52, align 4, !tbaa !124
  store i32 %93, i32* %tmp, align 4, !tbaa !6
  br label %if.end53

if.end53:                                         ; preds = %if.then51, %if.end45
  %94 = load i32, i32* %tmp, align 4, !tbaa !6
  %95 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height54 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %95, i32 0, i32 18
  store i32 %94, i32* %last_row_height54, align 4, !tbaa !128
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks55 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 15
  %97 = load i32, i32* %MCU_blocks55, align 4, !tbaa !125
  store i32 %97, i32* %mcublks, align 4, !tbaa !6
  %98 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU56 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %98, i32 0, i32 48
  %99 = load i32, i32* %blocks_in_MCU56, align 8, !tbaa !129
  %100 = load i32, i32* %mcublks, align 4, !tbaa !6
  %add = add nsw i32 %99, %100
  %cmp57 = icmp sgt i32 %add, 10
  br i1 %cmp57, label %if.then58, label %if.end63

if.then58:                                        ; preds = %if.end53
  %101 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err59 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %101, i32 0, i32 0
  %102 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err59, align 8, !tbaa !74
  %msg_code60 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %102, i32 0, i32 5
  store i32 13, i32* %msg_code60, align 4, !tbaa !75
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err61 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 0
  %104 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err61, align 8, !tbaa !74
  %error_exit62 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %104, i32 0, i32 0
  %105 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit62, align 4, !tbaa !77
  %106 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %107 = bitcast %struct.jpeg_compress_struct* %106 to %struct.jpeg_common_struct*
  call void %105(%struct.jpeg_common_struct* %107)
  br label %if.end63

if.end63:                                         ; preds = %if.then58, %if.end53
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end63
  %108 = load i32, i32* %mcublks, align 4, !tbaa !6
  %dec = add nsw i32 %108, -1
  store i32 %dec, i32* %mcublks, align 4, !tbaa !6
  %cmp64 = icmp sgt i32 %108, 0
  br i1 %cmp64, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %109 = load i32, i32* %ci, align 4, !tbaa !6
  %110 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership65 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %110, i32 0, i32 49
  %111 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU66 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %111, i32 0, i32 48
  %112 = load i32, i32* %blocks_in_MCU66, align 8, !tbaa !129
  %inc = add nsw i32 %112, 1
  store i32 %inc, i32* %blocks_in_MCU66, align 8, !tbaa !129
  %arrayidx67 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership65, i32 0, i32 %112
  store i32 %109, i32* %arrayidx67, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %113 = load i32, i32* %ci, align 4, !tbaa !6
  %inc68 = add nsw i32 %113, 1
  store i32 %inc68, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end69

if.end69:                                         ; preds = %for.end, %if.end
  %114 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_in_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %114, i32 0, i32 31
  %115 = load i32, i32* %restart_in_rows, align 4, !tbaa !130
  %cmp70 = icmp sgt i32 %115, 0
  br i1 %cmp70, label %if.then71, label %if.end76

if.then71:                                        ; preds = %if.end69
  %116 = bitcast i32* %nominal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_in_rows72 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 31
  %118 = load i32, i32* %restart_in_rows72, align 4, !tbaa !130
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row73 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 46
  %120 = load i32, i32* %MCUs_per_row73, align 8, !tbaa !121
  %mul74 = mul nsw i32 %118, %120
  store i32 %mul74, i32* %nominal, align 4, !tbaa !65
  %121 = load i32, i32* %nominal, align 4, !tbaa !65
  %cmp75 = icmp slt i32 %121, 65535
  br i1 %cmp75, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then71
  %122 = load i32, i32* %nominal, align 4, !tbaa !65
  br label %cond.end

cond.false:                                       ; preds = %if.then71
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %122, %cond.true ], [ 65535, %cond.false ]
  %123 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %123, i32 0, i32 30
  store i32 %cond, i32* %restart_interval, align 8, !tbaa !131
  %124 = bitcast i32* %nominal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  br label %if.end76

if.end76:                                         ; preds = %cond.end, %if.end69
  %125 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %mcublks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  ret void
}

declare void @jpeg_mem_dest_internal(%struct.jpeg_compress_struct*, i8**, i32*, i32) #2

declare i32 @jdiv_round_up(i32, i32) #2

; Function Attrs: nounwind
define internal void @select_scans(%struct.jpeg_compress_struct* %cinfo, i32 %next_scan_number) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %next_scan_number.addr = alloca i32, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %base_scan_idx = alloca i32, align 4
  %luma_freq_split_scan_start = alloca i32, align 4
  %chroma_freq_split_scan_start = alloca i32, align 4
  %passes_per_scan = alloca i32, align 4
  %Al = alloca i32, align 4
  %i = alloca i32, align 4
  %cost = alloca i32, align 4
  %idx = alloca i32, align 4
  %cost59 = alloca i32, align 4
  %Al155 = alloca i32, align 4
  %i158 = alloca i32, align 4
  %cost159 = alloca i32, align 4
  %idx242 = alloca i32, align 4
  %cost245 = alloca i32, align 4
  %i322 = alloca i32, align 4
  %Al323 = alloca i32, align 4
  %min_Al = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %next_scan_number, i32* %next_scan_number.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %3 = bitcast %struct.jpeg_comp_master* %2 to %struct.my_comp_master*
  store %struct.my_comp_master* %3, %struct.my_comp_master** %master, align 4, !tbaa !2
  %4 = bitcast i32* %base_scan_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store i32 0, i32* %base_scan_idx, align 4, !tbaa !6
  %5 = bitcast i32* %luma_freq_split_scan_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 54
  %7 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !8
  %num_scans_luma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %7, i32 0, i32 22
  %8 = load i32, i32* %num_scans_luma_dc, align 8, !tbaa !115
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 54
  %10 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master3, align 4, !tbaa !8
  %Al_max_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %10, i32 0, i32 25
  %11 = load i32, i32* %Al_max_luma, align 4, !tbaa !116
  %mul = mul nsw i32 3, %11
  %add = add nsw i32 %8, %mul
  %add4 = add nsw i32 %add, 2
  store i32 %add4, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %12 = bitcast i32* %chroma_freq_split_scan_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 54
  %14 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master5, align 4, !tbaa !8
  %num_scans_luma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %14, i32 0, i32 21
  %15 = load i32, i32* %num_scans_luma, align 4, !tbaa !117
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 54
  %17 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master6, align 4, !tbaa !8
  %num_scans_chroma_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %17, i32 0, i32 23
  %18 = load i32, i32* %num_scans_chroma_dc, align 4, !tbaa !119
  %add7 = add nsw i32 %15, %18
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 54
  %20 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master8, align 4, !tbaa !8
  %Al_max_chroma = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %20, i32 0, i32 26
  %21 = load i32, i32* %Al_max_chroma, align 8, !tbaa !120
  %mul9 = mul nsw i32 6, %21
  %add10 = add nsw i32 %mul9, 4
  %add11 = add nsw i32 %add7, %add10
  store i32 %add11, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %22 = bitcast i32* %passes_per_scan to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %optimize_coding = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 26
  %24 = load i32, i32* %optimize_coding, align 8, !tbaa !25
  %tobool = icmp ne i32 %24, 0
  %25 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 2, i32 1
  store i32 %cond, i32* %passes_per_scan, align 4, !tbaa !6
  %26 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %26, 1
  br i1 %cmp, label %land.lhs.true, label %if.else39

land.lhs.true:                                    ; preds = %entry
  %27 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %28 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %cmp12 = icmp sle i32 %27, %28
  br i1 %cmp12, label %if.then, label %if.else39

if.then:                                          ; preds = %land.lhs.true
  %29 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %29, 1
  %rem = srem i32 %sub, 3
  %cmp13 = icmp eq i32 %rem, 2
  br i1 %cmp13, label %if.then14, label %if.end38

if.then14:                                        ; preds = %if.then
  %30 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub15 = sub nsw i32 %31, 1
  %div = sdiv i32 %sub15, 3
  store i32 %div, i32* %Al, align 4, !tbaa !6
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i32* %cost to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  store i32 0, i32* %cost, align 4, !tbaa !65
  %34 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %34, i32 0, i32 7
  %35 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub16 = sub nsw i32 %35, 2
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size, i32 0, i32 %sub16
  %36 = load i32, i32* %arrayidx, align 4, !tbaa !65
  %37 = load i32, i32* %cost, align 4, !tbaa !65
  %add17 = add i32 %37, %36
  store i32 %add17, i32* %cost, align 4, !tbaa !65
  %38 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size18 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %38, i32 0, i32 7
  %39 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub19 = sub nsw i32 %39, 1
  %arrayidx20 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size18, i32 0, i32 %sub19
  %40 = load i32, i32* %arrayidx20, align 4, !tbaa !65
  %41 = load i32, i32* %cost, align 4, !tbaa !65
  %add21 = add i32 %41, %40
  store i32 %add21, i32* %cost, align 4, !tbaa !65
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then14
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %43 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %42, %43
  br i1 %cmp22, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %44 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size23 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %44, i32 0, i32 7
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %mul24 = mul nsw i32 3, %45
  %add25 = add nsw i32 3, %mul24
  %arrayidx26 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size23, i32 0, i32 %add25
  %46 = load i32, i32* %arrayidx26, align 4, !tbaa !65
  %47 = load i32, i32* %cost, align 4, !tbaa !65
  %add27 = add i32 %47, %46
  store i32 %add27, i32* %cost, align 4, !tbaa !65
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %49 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp28 = icmp eq i32 %49, 0
  br i1 %cmp28, label %if.then30, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %50 = load i32, i32* %cost, align 4, !tbaa !65
  %51 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %51, i32 0, i32 9
  %52 = load i32, i32* %best_cost, align 4, !tbaa !132
  %cmp29 = icmp ult i32 %50, %52
  br i1 %cmp29, label %if.then30, label %if.else

if.then30:                                        ; preds = %lor.lhs.false, %for.end
  %53 = load i32, i32* %cost, align 4, !tbaa !65
  %54 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost31 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %54, i32 0, i32 9
  store i32 %53, i32* %best_cost31, align 4, !tbaa !132
  %55 = load i32, i32* %Al, align 4, !tbaa !6
  %56 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_luma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %56, i32 0, i32 12
  store i32 %55, i32* %best_Al_luma, align 8, !tbaa !118
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %57 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %sub32 = sub nsw i32 %57, 1
  %58 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %58, i32 0, i32 4
  store i32 %sub32, i32* %scan_number, align 4, !tbaa !27
  %59 = load i32, i32* %passes_per_scan, align 4, !tbaa !6
  %60 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number33 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %60, i32 0, i32 4
  %61 = load i32, i32* %scan_number33, align 4, !tbaa !27
  %add34 = add nsw i32 %61, 1
  %mul35 = mul nsw i32 %59, %add34
  %sub36 = sub nsw i32 %mul35, 1
  %62 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %62, i32 0, i32 5
  %63 = load i32, i32* %pass_number_scan_opt_base, align 8, !tbaa !31
  %add37 = add nsw i32 %sub36, %63
  %64 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %64, i32 0, i32 2
  store i32 %add37, i32* %pass_number, align 4, !tbaa !28
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then30
  %65 = bitcast i32* %cost to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  %66 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  %67 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  br label %if.end38

if.end38:                                         ; preds = %if.end, %if.then
  br label %if.end315

if.else39:                                        ; preds = %land.lhs.true, %entry
  %68 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %69 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %cmp40 = icmp sgt i32 %68, %69
  br i1 %cmp40, label %land.lhs.true41, label %if.else106

land.lhs.true41:                                  ; preds = %if.else39
  %70 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master42 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 54
  %72 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master42, align 4, !tbaa !8
  %num_scans_luma43 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %72, i32 0, i32 21
  %73 = load i32, i32* %num_scans_luma43, align 4, !tbaa !117
  %cmp44 = icmp sle i32 %70, %73
  br i1 %cmp44, label %if.then45, label %if.else106

if.then45:                                        ; preds = %land.lhs.true41
  %74 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %75 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %add46 = add nsw i32 %75, 1
  %cmp47 = icmp eq i32 %74, %add46
  br i1 %cmp47, label %if.then48, label %if.else53

if.then48:                                        ; preds = %if.then45
  %76 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %76, i32 0, i32 10
  store i32 0, i32* %best_freq_split_idx_luma, align 8, !tbaa !133
  %77 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size49 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %77, i32 0, i32 7
  %78 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub50 = sub nsw i32 %78, 1
  %arrayidx51 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size49, i32 0, i32 %sub50
  %79 = load i32, i32* %arrayidx51, align 4, !tbaa !65
  %80 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost52 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %80, i32 0, i32 9
  store i32 %79, i32* %best_cost52, align 4, !tbaa !132
  br label %if.end105

if.else53:                                        ; preds = %if.then45
  %81 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %82 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %sub54 = sub nsw i32 %81, %82
  %rem55 = srem i32 %sub54, 2
  %cmp56 = icmp eq i32 %rem55, 1
  br i1 %cmp56, label %if.then57, label %if.end104

if.then57:                                        ; preds = %if.else53
  %83 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #3
  %84 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %85 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %sub58 = sub nsw i32 %84, %85
  %shr = ashr i32 %sub58, 1
  store i32 %shr, i32* %idx, align 4, !tbaa !6
  %86 = bitcast i32* %cost59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #3
  store i32 0, i32* %cost59, align 4, !tbaa !65
  %87 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size60 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %87, i32 0, i32 7
  %88 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub61 = sub nsw i32 %88, 2
  %arrayidx62 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size60, i32 0, i32 %sub61
  %89 = load i32, i32* %arrayidx62, align 4, !tbaa !65
  %90 = load i32, i32* %cost59, align 4, !tbaa !65
  %add63 = add i32 %90, %89
  store i32 %add63, i32* %cost59, align 4, !tbaa !65
  %91 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size64 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %91, i32 0, i32 7
  %92 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub65 = sub nsw i32 %92, 1
  %arrayidx66 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size64, i32 0, i32 %sub65
  %93 = load i32, i32* %arrayidx66, align 4, !tbaa !65
  %94 = load i32, i32* %cost59, align 4, !tbaa !65
  %add67 = add i32 %94, %93
  store i32 %add67, i32* %cost59, align 4, !tbaa !65
  %95 = load i32, i32* %cost59, align 4, !tbaa !65
  %96 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost68 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %96, i32 0, i32 9
  %97 = load i32, i32* %best_cost68, align 4, !tbaa !132
  %cmp69 = icmp ult i32 %95, %97
  br i1 %cmp69, label %if.then70, label %if.end73

if.then70:                                        ; preds = %if.then57
  %98 = load i32, i32* %cost59, align 4, !tbaa !65
  %99 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost71 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %99, i32 0, i32 9
  store i32 %98, i32* %best_cost71, align 4, !tbaa !132
  %100 = load i32, i32* %idx, align 4, !tbaa !6
  %101 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma72 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %101, i32 0, i32 10
  store i32 %100, i32* %best_freq_split_idx_luma72, align 8, !tbaa !133
  br label %if.end73

if.end73:                                         ; preds = %if.then70, %if.then57
  %102 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp74 = icmp eq i32 %102, 2
  br i1 %cmp74, label %land.lhs.true75, label %lor.lhs.false78

land.lhs.true75:                                  ; preds = %if.end73
  %103 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma76 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %103, i32 0, i32 10
  %104 = load i32, i32* %best_freq_split_idx_luma76, align 8, !tbaa !133
  %cmp77 = icmp eq i32 %104, 0
  br i1 %cmp77, label %if.then88, label %lor.lhs.false78

lor.lhs.false78:                                  ; preds = %land.lhs.true75, %if.end73
  %105 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp79 = icmp eq i32 %105, 3
  br i1 %cmp79, label %land.lhs.true80, label %lor.lhs.false83

land.lhs.true80:                                  ; preds = %lor.lhs.false78
  %106 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma81 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %106, i32 0, i32 10
  %107 = load i32, i32* %best_freq_split_idx_luma81, align 8, !tbaa !133
  %cmp82 = icmp ne i32 %107, 2
  br i1 %cmp82, label %if.then88, label %lor.lhs.false83

lor.lhs.false83:                                  ; preds = %land.lhs.true80, %lor.lhs.false78
  %108 = load i32, i32* %idx, align 4, !tbaa !6
  %cmp84 = icmp eq i32 %108, 4
  br i1 %cmp84, label %land.lhs.true85, label %if.end103

land.lhs.true85:                                  ; preds = %lor.lhs.false83
  %109 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma86 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %109, i32 0, i32 10
  %110 = load i32, i32* %best_freq_split_idx_luma86, align 8, !tbaa !133
  %cmp87 = icmp ne i32 %110, 4
  br i1 %cmp87, label %if.then88, label %if.end103

if.then88:                                        ; preds = %land.lhs.true85, %land.lhs.true80, %land.lhs.true75
  %111 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master89 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %111, i32 0, i32 54
  %112 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master89, align 4, !tbaa !8
  %num_scans_luma90 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %112, i32 0, i32 21
  %113 = load i32, i32* %num_scans_luma90, align 4, !tbaa !117
  %sub91 = sub nsw i32 %113, 1
  %114 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number92 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %114, i32 0, i32 4
  store i32 %sub91, i32* %scan_number92, align 4, !tbaa !27
  %115 = load i32, i32* %passes_per_scan, align 4, !tbaa !6
  %116 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number93 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %116, i32 0, i32 4
  %117 = load i32, i32* %scan_number93, align 4, !tbaa !27
  %add94 = add nsw i32 %117, 1
  %mul95 = mul nsw i32 %115, %add94
  %sub96 = sub nsw i32 %mul95, 1
  %118 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base97 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %118, i32 0, i32 5
  %119 = load i32, i32* %pass_number_scan_opt_base97, align 8, !tbaa !31
  %add98 = add nsw i32 %sub96, %119
  %120 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number99 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %120, i32 0, i32 2
  store i32 %add98, i32* %pass_number99, align 4, !tbaa !28
  %121 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number100 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %121, i32 0, i32 2
  %122 = load i32, i32* %pass_number100, align 4, !tbaa !28
  %123 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %123, i32 0, i32 3
  %124 = load i32, i32* %total_passes, align 8, !tbaa !29
  %sub101 = sub nsw i32 %124, 1
  %cmp102 = icmp eq i32 %122, %sub101
  %conv = zext i1 %cmp102 to i32
  %125 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %125, i32 0, i32 0
  %is_last_pass = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub, i32 0, i32 4
  store i32 %conv, i32* %is_last_pass, align 8, !tbaa !19
  br label %if.end103

if.end103:                                        ; preds = %if.then88, %land.lhs.true85, %lor.lhs.false83
  %126 = bitcast i32* %cost59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  br label %if.end104

if.end104:                                        ; preds = %if.end103, %if.else53
  br label %if.end105

if.end105:                                        ; preds = %if.end104, %if.then48
  br label %if.end314

if.else106:                                       ; preds = %land.lhs.true41, %if.else39
  %128 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %128, i32 0, i32 22
  %129 = load i32, i32* %num_scans, align 8, !tbaa !23
  %130 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master107 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %130, i32 0, i32 54
  %131 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master107, align 4, !tbaa !8
  %num_scans_luma108 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %131, i32 0, i32 21
  %132 = load i32, i32* %num_scans_luma108, align 4, !tbaa !117
  %cmp109 = icmp sgt i32 %129, %132
  br i1 %cmp109, label %if.then111, label %if.end313

if.then111:                                       ; preds = %if.else106
  %133 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %134 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master112 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %134, i32 0, i32 54
  %135 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master112, align 4, !tbaa !8
  %num_scans_luma113 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %135, i32 0, i32 21
  %136 = load i32, i32* %num_scans_luma113, align 4, !tbaa !117
  %137 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master114 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %137, i32 0, i32 54
  %138 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master114, align 4, !tbaa !8
  %num_scans_chroma_dc115 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %138, i32 0, i32 23
  %139 = load i32, i32* %num_scans_chroma_dc115, align 4, !tbaa !119
  %add116 = add nsw i32 %136, %139
  %cmp117 = icmp eq i32 %133, %add116
  br i1 %cmp117, label %if.then119, label %if.else133

if.then119:                                       ; preds = %if.then111
  %140 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master120 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %140, i32 0, i32 54
  %141 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master120, align 4, !tbaa !8
  %num_scans_luma121 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %141, i32 0, i32 21
  %142 = load i32, i32* %num_scans_luma121, align 4, !tbaa !117
  store i32 %142, i32* %base_scan_idx, align 4, !tbaa !6
  %143 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size122 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %143, i32 0, i32 7
  %144 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %arrayidx123 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size122, i32 0, i32 %144
  %145 = load i32, i32* %arrayidx123, align 4, !tbaa !65
  %146 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size124 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %146, i32 0, i32 7
  %147 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add125 = add nsw i32 %147, 1
  %arrayidx126 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size124, i32 0, i32 %add125
  %148 = load i32, i32* %arrayidx126, align 4, !tbaa !65
  %149 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size127 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %149, i32 0, i32 7
  %150 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add128 = add nsw i32 %150, 2
  %arrayidx129 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size127, i32 0, i32 %add128
  %151 = load i32, i32* %arrayidx129, align 4, !tbaa !65
  %add130 = add i32 %148, %151
  %cmp131 = icmp ule i32 %145, %add130
  %conv132 = zext i1 %cmp131 to i32
  %152 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %interleave_chroma_dc = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %152, i32 0, i32 14
  store i32 %conv132, i32* %interleave_chroma_dc, align 8, !tbaa !134
  br label %if.end312

if.else133:                                       ; preds = %if.then111
  %153 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %154 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master134 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %154, i32 0, i32 54
  %155 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master134, align 4, !tbaa !8
  %num_scans_luma135 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %155, i32 0, i32 21
  %156 = load i32, i32* %num_scans_luma135, align 4, !tbaa !117
  %157 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master136 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %157, i32 0, i32 54
  %158 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master136, align 4, !tbaa !8
  %num_scans_chroma_dc137 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %158, i32 0, i32 23
  %159 = load i32, i32* %num_scans_chroma_dc137, align 4, !tbaa !119
  %add138 = add nsw i32 %156, %159
  %cmp139 = icmp sgt i32 %153, %add138
  br i1 %cmp139, label %land.lhs.true141, label %if.else215

land.lhs.true141:                                 ; preds = %if.else133
  %160 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %161 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %cmp142 = icmp sle i32 %160, %161
  br i1 %cmp142, label %if.then144, label %if.else215

if.then144:                                       ; preds = %land.lhs.true141
  %162 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master145 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %162, i32 0, i32 54
  %163 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master145, align 4, !tbaa !8
  %num_scans_luma146 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %163, i32 0, i32 21
  %164 = load i32, i32* %num_scans_luma146, align 4, !tbaa !117
  %165 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master147 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %165, i32 0, i32 54
  %166 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master147, align 4, !tbaa !8
  %num_scans_chroma_dc148 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %166, i32 0, i32 23
  %167 = load i32, i32* %num_scans_chroma_dc148, align 4, !tbaa !119
  %add149 = add nsw i32 %164, %167
  store i32 %add149, i32* %base_scan_idx, align 4, !tbaa !6
  %168 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %169 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %sub150 = sub nsw i32 %168, %169
  %rem151 = srem i32 %sub150, 6
  %cmp152 = icmp eq i32 %rem151, 4
  br i1 %cmp152, label %if.then154, label %if.end214

if.then154:                                       ; preds = %if.then144
  %170 = bitcast i32* %Al155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #3
  %171 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %172 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %sub156 = sub nsw i32 %171, %172
  %div157 = sdiv i32 %sub156, 6
  store i32 %div157, i32* %Al155, align 4, !tbaa !6
  %173 = bitcast i32* %i158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #3
  %174 = bitcast i32* %cost159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %174) #3
  store i32 0, i32* %cost159, align 4, !tbaa !65
  %175 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size160 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %175, i32 0, i32 7
  %176 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub161 = sub nsw i32 %176, 4
  %arrayidx162 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size160, i32 0, i32 %sub161
  %177 = load i32, i32* %arrayidx162, align 4, !tbaa !65
  %178 = load i32, i32* %cost159, align 4, !tbaa !65
  %add163 = add i32 %178, %177
  store i32 %add163, i32* %cost159, align 4, !tbaa !65
  %179 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size164 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %179, i32 0, i32 7
  %180 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub165 = sub nsw i32 %180, 3
  %arrayidx166 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size164, i32 0, i32 %sub165
  %181 = load i32, i32* %arrayidx166, align 4, !tbaa !65
  %182 = load i32, i32* %cost159, align 4, !tbaa !65
  %add167 = add i32 %182, %181
  store i32 %add167, i32* %cost159, align 4, !tbaa !65
  %183 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size168 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %183, i32 0, i32 7
  %184 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub169 = sub nsw i32 %184, 2
  %arrayidx170 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size168, i32 0, i32 %sub169
  %185 = load i32, i32* %arrayidx170, align 4, !tbaa !65
  %186 = load i32, i32* %cost159, align 4, !tbaa !65
  %add171 = add i32 %186, %185
  store i32 %add171, i32* %cost159, align 4, !tbaa !65
  %187 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size172 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %187, i32 0, i32 7
  %188 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub173 = sub nsw i32 %188, 1
  %arrayidx174 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size172, i32 0, i32 %sub173
  %189 = load i32, i32* %arrayidx174, align 4, !tbaa !65
  %190 = load i32, i32* %cost159, align 4, !tbaa !65
  %add175 = add i32 %190, %189
  store i32 %add175, i32* %cost159, align 4, !tbaa !65
  store i32 0, i32* %i158, align 4, !tbaa !6
  br label %for.cond176

for.cond176:                                      ; preds = %for.inc192, %if.then154
  %191 = load i32, i32* %i158, align 4, !tbaa !6
  %192 = load i32, i32* %Al155, align 4, !tbaa !6
  %cmp177 = icmp slt i32 %191, %192
  br i1 %cmp177, label %for.body179, label %for.end194

for.body179:                                      ; preds = %for.cond176
  %193 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size180 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %193, i32 0, i32 7
  %194 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add181 = add nsw i32 %194, 4
  %195 = load i32, i32* %i158, align 4, !tbaa !6
  %mul182 = mul nsw i32 6, %195
  %add183 = add nsw i32 %add181, %mul182
  %arrayidx184 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size180, i32 0, i32 %add183
  %196 = load i32, i32* %arrayidx184, align 4, !tbaa !65
  %197 = load i32, i32* %cost159, align 4, !tbaa !65
  %add185 = add i32 %197, %196
  store i32 %add185, i32* %cost159, align 4, !tbaa !65
  %198 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size186 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %198, i32 0, i32 7
  %199 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add187 = add nsw i32 %199, 5
  %200 = load i32, i32* %i158, align 4, !tbaa !6
  %mul188 = mul nsw i32 6, %200
  %add189 = add nsw i32 %add187, %mul188
  %arrayidx190 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size186, i32 0, i32 %add189
  %201 = load i32, i32* %arrayidx190, align 4, !tbaa !65
  %202 = load i32, i32* %cost159, align 4, !tbaa !65
  %add191 = add i32 %202, %201
  store i32 %add191, i32* %cost159, align 4, !tbaa !65
  br label %for.inc192

for.inc192:                                       ; preds = %for.body179
  %203 = load i32, i32* %i158, align 4, !tbaa !6
  %inc193 = add nsw i32 %203, 1
  store i32 %inc193, i32* %i158, align 4, !tbaa !6
  br label %for.cond176

for.end194:                                       ; preds = %for.cond176
  %204 = load i32, i32* %Al155, align 4, !tbaa !6
  %cmp195 = icmp eq i32 %204, 0
  br i1 %cmp195, label %if.then201, label %lor.lhs.false197

lor.lhs.false197:                                 ; preds = %for.end194
  %205 = load i32, i32* %cost159, align 4, !tbaa !65
  %206 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost198 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %206, i32 0, i32 9
  %207 = load i32, i32* %best_cost198, align 4, !tbaa !132
  %cmp199 = icmp ult i32 %205, %207
  br i1 %cmp199, label %if.then201, label %if.else203

if.then201:                                       ; preds = %lor.lhs.false197, %for.end194
  %208 = load i32, i32* %cost159, align 4, !tbaa !65
  %209 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost202 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %209, i32 0, i32 9
  store i32 %208, i32* %best_cost202, align 4, !tbaa !132
  %210 = load i32, i32* %Al155, align 4, !tbaa !6
  %211 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %211, i32 0, i32 13
  store i32 %210, i32* %best_Al_chroma, align 4, !tbaa !37
  br label %if.end213

if.else203:                                       ; preds = %lor.lhs.false197
  %212 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %sub204 = sub nsw i32 %212, 1
  %213 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number205 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %213, i32 0, i32 4
  store i32 %sub204, i32* %scan_number205, align 4, !tbaa !27
  %214 = load i32, i32* %passes_per_scan, align 4, !tbaa !6
  %215 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number206 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %215, i32 0, i32 4
  %216 = load i32, i32* %scan_number206, align 4, !tbaa !27
  %add207 = add nsw i32 %216, 1
  %mul208 = mul nsw i32 %214, %add207
  %sub209 = sub nsw i32 %mul208, 1
  %217 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base210 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %217, i32 0, i32 5
  %218 = load i32, i32* %pass_number_scan_opt_base210, align 8, !tbaa !31
  %add211 = add nsw i32 %sub209, %218
  %219 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number212 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %219, i32 0, i32 2
  store i32 %add211, i32* %pass_number212, align 4, !tbaa !28
  br label %if.end213

if.end213:                                        ; preds = %if.else203, %if.then201
  %220 = bitcast i32* %cost159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #3
  %221 = bitcast i32* %i158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #3
  %222 = bitcast i32* %Al155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #3
  br label %if.end214

if.end214:                                        ; preds = %if.end213, %if.then144
  br label %if.end311

if.else215:                                       ; preds = %land.lhs.true141, %if.else133
  %223 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %224 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %cmp216 = icmp sgt i32 %223, %224
  br i1 %cmp216, label %land.lhs.true218, label %if.end310

land.lhs.true218:                                 ; preds = %if.else215
  %225 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %226 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans219 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %226, i32 0, i32 22
  %227 = load i32, i32* %num_scans219, align 8, !tbaa !23
  %cmp220 = icmp sle i32 %225, %227
  br i1 %cmp220, label %if.then222, label %if.end310

if.then222:                                       ; preds = %land.lhs.true218
  %228 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %229 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %add223 = add nsw i32 %229, 2
  %cmp224 = icmp eq i32 %228, %add223
  br i1 %cmp224, label %if.then226, label %if.else236

if.then226:                                       ; preds = %if.then222
  %230 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %230, i32 0, i32 11
  store i32 0, i32* %best_freq_split_idx_chroma, align 4, !tbaa !135
  %231 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size227 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %231, i32 0, i32 7
  %232 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub228 = sub nsw i32 %232, 2
  %arrayidx229 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size227, i32 0, i32 %sub228
  %233 = load i32, i32* %arrayidx229, align 4, !tbaa !65
  %234 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost230 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %234, i32 0, i32 9
  store i32 %233, i32* %best_cost230, align 4, !tbaa !132
  %235 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size231 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %235, i32 0, i32 7
  %236 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub232 = sub nsw i32 %236, 1
  %arrayidx233 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size231, i32 0, i32 %sub232
  %237 = load i32, i32* %arrayidx233, align 4, !tbaa !65
  %238 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost234 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %238, i32 0, i32 9
  %239 = load i32, i32* %best_cost234, align 4, !tbaa !132
  %add235 = add i32 %239, %237
  store i32 %add235, i32* %best_cost234, align 4, !tbaa !132
  br label %if.end309

if.else236:                                       ; preds = %if.then222
  %240 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %241 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %sub237 = sub nsw i32 %240, %241
  %rem238 = srem i32 %sub237, 4
  %cmp239 = icmp eq i32 %rem238, 2
  br i1 %cmp239, label %if.then241, label %if.end308

if.then241:                                       ; preds = %if.else236
  %242 = bitcast i32* %idx242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %242) #3
  %243 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %244 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %sub243 = sub nsw i32 %243, %244
  %shr244 = ashr i32 %sub243, 2
  store i32 %shr244, i32* %idx242, align 4, !tbaa !6
  %245 = bitcast i32* %cost245 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %245) #3
  store i32 0, i32* %cost245, align 4, !tbaa !65
  %246 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size246 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %246, i32 0, i32 7
  %247 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub247 = sub nsw i32 %247, 4
  %arrayidx248 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size246, i32 0, i32 %sub247
  %248 = load i32, i32* %arrayidx248, align 4, !tbaa !65
  %249 = load i32, i32* %cost245, align 4, !tbaa !65
  %add249 = add i32 %249, %248
  store i32 %add249, i32* %cost245, align 4, !tbaa !65
  %250 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size250 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %250, i32 0, i32 7
  %251 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub251 = sub nsw i32 %251, 3
  %arrayidx252 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size250, i32 0, i32 %sub251
  %252 = load i32, i32* %arrayidx252, align 4, !tbaa !65
  %253 = load i32, i32* %cost245, align 4, !tbaa !65
  %add253 = add i32 %253, %252
  store i32 %add253, i32* %cost245, align 4, !tbaa !65
  %254 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size254 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %254, i32 0, i32 7
  %255 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub255 = sub nsw i32 %255, 2
  %arrayidx256 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size254, i32 0, i32 %sub255
  %256 = load i32, i32* %arrayidx256, align 4, !tbaa !65
  %257 = load i32, i32* %cost245, align 4, !tbaa !65
  %add257 = add i32 %257, %256
  store i32 %add257, i32* %cost245, align 4, !tbaa !65
  %258 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size258 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %258, i32 0, i32 7
  %259 = load i32, i32* %next_scan_number.addr, align 4, !tbaa !6
  %sub259 = sub nsw i32 %259, 1
  %arrayidx260 = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size258, i32 0, i32 %sub259
  %260 = load i32, i32* %arrayidx260, align 4, !tbaa !65
  %261 = load i32, i32* %cost245, align 4, !tbaa !65
  %add261 = add i32 %261, %260
  store i32 %add261, i32* %cost245, align 4, !tbaa !65
  %262 = load i32, i32* %cost245, align 4, !tbaa !65
  %263 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost262 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %263, i32 0, i32 9
  %264 = load i32, i32* %best_cost262, align 4, !tbaa !132
  %cmp263 = icmp ult i32 %262, %264
  br i1 %cmp263, label %if.then265, label %if.end268

if.then265:                                       ; preds = %if.then241
  %265 = load i32, i32* %cost245, align 4, !tbaa !65
  %266 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_cost266 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %266, i32 0, i32 9
  store i32 %265, i32* %best_cost266, align 4, !tbaa !132
  %267 = load i32, i32* %idx242, align 4, !tbaa !6
  %268 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma267 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %268, i32 0, i32 11
  store i32 %267, i32* %best_freq_split_idx_chroma267, align 4, !tbaa !135
  br label %if.end268

if.end268:                                        ; preds = %if.then265, %if.then241
  %269 = load i32, i32* %idx242, align 4, !tbaa !6
  %cmp269 = icmp eq i32 %269, 2
  br i1 %cmp269, label %land.lhs.true271, label %lor.lhs.false275

land.lhs.true271:                                 ; preds = %if.end268
  %270 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma272 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %270, i32 0, i32 11
  %271 = load i32, i32* %best_freq_split_idx_chroma272, align 4, !tbaa !135
  %cmp273 = icmp eq i32 %271, 0
  br i1 %cmp273, label %if.then289, label %lor.lhs.false275

lor.lhs.false275:                                 ; preds = %land.lhs.true271, %if.end268
  %272 = load i32, i32* %idx242, align 4, !tbaa !6
  %cmp276 = icmp eq i32 %272, 3
  br i1 %cmp276, label %land.lhs.true278, label %lor.lhs.false282

land.lhs.true278:                                 ; preds = %lor.lhs.false275
  %273 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma279 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %273, i32 0, i32 11
  %274 = load i32, i32* %best_freq_split_idx_chroma279, align 4, !tbaa !135
  %cmp280 = icmp ne i32 %274, 2
  br i1 %cmp280, label %if.then289, label %lor.lhs.false282

lor.lhs.false282:                                 ; preds = %land.lhs.true278, %lor.lhs.false275
  %275 = load i32, i32* %idx242, align 4, !tbaa !6
  %cmp283 = icmp eq i32 %275, 4
  br i1 %cmp283, label %land.lhs.true285, label %if.end307

land.lhs.true285:                                 ; preds = %lor.lhs.false282
  %276 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma286 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %276, i32 0, i32 11
  %277 = load i32, i32* %best_freq_split_idx_chroma286, align 4, !tbaa !135
  %cmp287 = icmp ne i32 %277, 4
  br i1 %cmp287, label %if.then289, label %if.end307

if.then289:                                       ; preds = %land.lhs.true285, %land.lhs.true278, %land.lhs.true271
  %278 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans290 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %278, i32 0, i32 22
  %279 = load i32, i32* %num_scans290, align 8, !tbaa !23
  %sub291 = sub nsw i32 %279, 1
  %280 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number292 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %280, i32 0, i32 4
  store i32 %sub291, i32* %scan_number292, align 4, !tbaa !27
  %281 = load i32, i32* %passes_per_scan, align 4, !tbaa !6
  %282 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number293 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %282, i32 0, i32 4
  %283 = load i32, i32* %scan_number293, align 4, !tbaa !27
  %add294 = add nsw i32 %283, 1
  %mul295 = mul nsw i32 %281, %add294
  %sub296 = sub nsw i32 %mul295, 1
  %284 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number_scan_opt_base297 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %284, i32 0, i32 5
  %285 = load i32, i32* %pass_number_scan_opt_base297, align 8, !tbaa !31
  %add298 = add nsw i32 %sub296, %285
  %286 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number299 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %286, i32 0, i32 2
  store i32 %add298, i32* %pass_number299, align 4, !tbaa !28
  %287 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pass_number300 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %287, i32 0, i32 2
  %288 = load i32, i32* %pass_number300, align 4, !tbaa !28
  %289 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %total_passes301 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %289, i32 0, i32 3
  %290 = load i32, i32* %total_passes301, align 8, !tbaa !29
  %sub302 = sub nsw i32 %290, 1
  %cmp303 = icmp eq i32 %288, %sub302
  %conv304 = zext i1 %cmp303 to i32
  %291 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %pub305 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %291, i32 0, i32 0
  %is_last_pass306 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %pub305, i32 0, i32 4
  store i32 %conv304, i32* %is_last_pass306, align 8, !tbaa !19
  br label %if.end307

if.end307:                                        ; preds = %if.then289, %land.lhs.true285, %lor.lhs.false282
  %292 = bitcast i32* %cost245 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #3
  %293 = bitcast i32* %idx242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #3
  br label %if.end308

if.end308:                                        ; preds = %if.end307, %if.else236
  br label %if.end309

if.end309:                                        ; preds = %if.end308, %if.then226
  br label %if.end310

if.end310:                                        ; preds = %if.end309, %land.lhs.true218, %if.else215
  br label %if.end311

if.end311:                                        ; preds = %if.end310, %if.end214
  br label %if.end312

if.end312:                                        ; preds = %if.end311, %if.then119
  br label %if.end313

if.end313:                                        ; preds = %if.end312, %if.else106
  br label %if.end314

if.end314:                                        ; preds = %if.end313, %if.end105
  br label %if.end315

if.end315:                                        ; preds = %if.end314, %if.end38
  %294 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_number316 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %294, i32 0, i32 4
  %295 = load i32, i32* %scan_number316, align 4, !tbaa !27
  %296 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans317 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %296, i32 0, i32 22
  %297 = load i32, i32* %num_scans317, align 8, !tbaa !23
  %sub318 = sub nsw i32 %297, 1
  %cmp319 = icmp eq i32 %295, %sub318
  br i1 %cmp319, label %if.then321, label %if.end473

if.then321:                                       ; preds = %if.end315
  %298 = bitcast i32* %i322 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %298) #3
  %299 = bitcast i32* %Al323 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %299) #3
  %300 = bitcast i32* %min_Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %300) #3
  %301 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_luma324 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %301, i32 0, i32 12
  %302 = load i32, i32* %best_Al_luma324, align 8, !tbaa !118
  %303 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma325 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %303, i32 0, i32 13
  %304 = load i32, i32* %best_Al_chroma325, align 4, !tbaa !37
  %cmp326 = icmp slt i32 %302, %304
  br i1 %cmp326, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then321
  %305 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_luma328 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %305, i32 0, i32 12
  %306 = load i32, i32* %best_Al_luma328, align 8, !tbaa !118
  br label %cond.end

cond.false:                                       ; preds = %if.then321
  %307 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma329 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %307, i32 0, i32 13
  %308 = load i32, i32* %best_Al_chroma329, align 4, !tbaa !37
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond330 = phi i32 [ %306, %cond.true ], [ %308, %cond.false ]
  store i32 %cond330, i32* %min_Al, align 4, !tbaa !6
  %309 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @copy_buffer(%struct.jpeg_compress_struct* %309, i32 0)
  %310 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans331 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %310, i32 0, i32 22
  %311 = load i32, i32* %num_scans331, align 8, !tbaa !23
  %312 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master332 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %312, i32 0, i32 54
  %313 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master332, align 4, !tbaa !8
  %num_scans_luma333 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %313, i32 0, i32 21
  %314 = load i32, i32* %num_scans_luma333, align 4, !tbaa !117
  %cmp334 = icmp sgt i32 %311, %314
  br i1 %cmp334, label %land.lhs.true336, label %if.end355

land.lhs.true336:                                 ; preds = %cond.end
  %315 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master337 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %315, i32 0, i32 54
  %316 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master337, align 4, !tbaa !8
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %316, i32 0, i32 17
  %317 = load i32, i32* %dc_scan_opt_mode, align 4, !tbaa !136
  %cmp338 = icmp ne i32 %317, 0
  br i1 %cmp338, label %if.then340, label %if.end355

if.then340:                                       ; preds = %land.lhs.true336
  %318 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master341 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %318, i32 0, i32 54
  %319 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master341, align 4, !tbaa !8
  %num_scans_luma342 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %319, i32 0, i32 21
  %320 = load i32, i32* %num_scans_luma342, align 4, !tbaa !117
  store i32 %320, i32* %base_scan_idx, align 4, !tbaa !6
  %321 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %interleave_chroma_dc343 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %321, i32 0, i32 14
  %322 = load i32, i32* %interleave_chroma_dc343, align 8, !tbaa !134
  %tobool344 = icmp ne i32 %322, 0
  br i1 %tobool344, label %land.lhs.true345, label %if.else351

land.lhs.true345:                                 ; preds = %if.then340
  %323 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master346 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %323, i32 0, i32 54
  %324 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master346, align 4, !tbaa !8
  %dc_scan_opt_mode347 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %324, i32 0, i32 17
  %325 = load i32, i32* %dc_scan_opt_mode347, align 4, !tbaa !136
  %cmp348 = icmp ne i32 %325, 1
  br i1 %cmp348, label %if.then350, label %if.else351

if.then350:                                       ; preds = %land.lhs.true345
  %326 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %327 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  call void @copy_buffer(%struct.jpeg_compress_struct* %326, i32 %327)
  br label %if.end354

if.else351:                                       ; preds = %land.lhs.true345, %if.then340
  %328 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %329 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add352 = add nsw i32 %329, 1
  call void @copy_buffer(%struct.jpeg_compress_struct* %328, i32 %add352)
  %330 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %331 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %add353 = add nsw i32 %331, 2
  call void @copy_buffer(%struct.jpeg_compress_struct* %330, i32 %add353)
  br label %if.end354

if.end354:                                        ; preds = %if.else351, %if.then350
  br label %if.end355

if.end355:                                        ; preds = %if.end354, %land.lhs.true336, %cond.end
  %332 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma356 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %332, i32 0, i32 10
  %333 = load i32, i32* %best_freq_split_idx_luma356, align 8, !tbaa !133
  %cmp357 = icmp eq i32 %333, 0
  br i1 %cmp357, label %if.then359, label %if.else360

if.then359:                                       ; preds = %if.end355
  %334 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %335 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  call void @copy_buffer(%struct.jpeg_compress_struct* %334, i32 %335)
  br label %if.end371

if.else360:                                       ; preds = %if.end355
  %336 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %337 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %338 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma361 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %338, i32 0, i32 10
  %339 = load i32, i32* %best_freq_split_idx_luma361, align 8, !tbaa !133
  %sub362 = sub nsw i32 %339, 1
  %mul363 = mul nsw i32 2, %sub362
  %add364 = add nsw i32 %337, %mul363
  %add365 = add nsw i32 %add364, 1
  call void @copy_buffer(%struct.jpeg_compress_struct* %336, i32 %add365)
  %340 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %341 = load i32, i32* %luma_freq_split_scan_start, align 4, !tbaa !6
  %342 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_luma366 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %342, i32 0, i32 10
  %343 = load i32, i32* %best_freq_split_idx_luma366, align 8, !tbaa !133
  %sub367 = sub nsw i32 %343, 1
  %mul368 = mul nsw i32 2, %sub367
  %add369 = add nsw i32 %341, %mul368
  %add370 = add nsw i32 %add369, 2
  call void @copy_buffer(%struct.jpeg_compress_struct* %340, i32 %add370)
  br label %if.end371

if.end371:                                        ; preds = %if.else360, %if.then359
  %344 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_luma372 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %344, i32 0, i32 12
  %345 = load i32, i32* %best_Al_luma372, align 8, !tbaa !118
  %sub373 = sub nsw i32 %345, 1
  store i32 %sub373, i32* %Al323, align 4, !tbaa !6
  br label %for.cond374

for.cond374:                                      ; preds = %for.inc380, %if.end371
  %346 = load i32, i32* %Al323, align 4, !tbaa !6
  %347 = load i32, i32* %min_Al, align 4, !tbaa !6
  %cmp375 = icmp sge i32 %346, %347
  br i1 %cmp375, label %for.body377, label %for.end381

for.body377:                                      ; preds = %for.cond374
  %348 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %349 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul378 = mul nsw i32 3, %349
  %add379 = add nsw i32 3, %mul378
  call void @copy_buffer(%struct.jpeg_compress_struct* %348, i32 %add379)
  br label %for.inc380

for.inc380:                                       ; preds = %for.body377
  %350 = load i32, i32* %Al323, align 4, !tbaa !6
  %dec = add nsw i32 %350, -1
  store i32 %dec, i32* %Al323, align 4, !tbaa !6
  br label %for.cond374

for.end381:                                       ; preds = %for.cond374
  %351 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans382 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %351, i32 0, i32 22
  %352 = load i32, i32* %num_scans382, align 8, !tbaa !23
  %353 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master383 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %353, i32 0, i32 54
  %354 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master383, align 4, !tbaa !8
  %num_scans_luma384 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %354, i32 0, i32 21
  %355 = load i32, i32* %num_scans_luma384, align 4, !tbaa !117
  %cmp385 = icmp sgt i32 %352, %355
  br i1 %cmp385, label %if.then387, label %if.end435

if.then387:                                       ; preds = %for.end381
  %356 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma388 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %356, i32 0, i32 11
  %357 = load i32, i32* %best_freq_split_idx_chroma388, align 4, !tbaa !135
  %cmp389 = icmp eq i32 %357, 0
  br i1 %cmp389, label %if.then391, label %if.else393

if.then391:                                       ; preds = %if.then387
  %358 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %359 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  call void @copy_buffer(%struct.jpeg_compress_struct* %358, i32 %359)
  %360 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %361 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %add392 = add nsw i32 %361, 1
  call void @copy_buffer(%struct.jpeg_compress_struct* %360, i32 %add392)
  br label %if.end414

if.else393:                                       ; preds = %if.then387
  %362 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %363 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %364 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma394 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %364, i32 0, i32 11
  %365 = load i32, i32* %best_freq_split_idx_chroma394, align 4, !tbaa !135
  %sub395 = sub nsw i32 %365, 1
  %mul396 = mul nsw i32 4, %sub395
  %add397 = add nsw i32 %363, %mul396
  %add398 = add nsw i32 %add397, 2
  call void @copy_buffer(%struct.jpeg_compress_struct* %362, i32 %add398)
  %366 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %367 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %368 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma399 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %368, i32 0, i32 11
  %369 = load i32, i32* %best_freq_split_idx_chroma399, align 4, !tbaa !135
  %sub400 = sub nsw i32 %369, 1
  %mul401 = mul nsw i32 4, %sub400
  %add402 = add nsw i32 %367, %mul401
  %add403 = add nsw i32 %add402, 3
  call void @copy_buffer(%struct.jpeg_compress_struct* %366, i32 %add403)
  %370 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %371 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %372 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma404 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %372, i32 0, i32 11
  %373 = load i32, i32* %best_freq_split_idx_chroma404, align 4, !tbaa !135
  %sub405 = sub nsw i32 %373, 1
  %mul406 = mul nsw i32 4, %sub405
  %add407 = add nsw i32 %371, %mul406
  %add408 = add nsw i32 %add407, 4
  call void @copy_buffer(%struct.jpeg_compress_struct* %370, i32 %add408)
  %374 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %375 = load i32, i32* %chroma_freq_split_scan_start, align 4, !tbaa !6
  %376 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_freq_split_idx_chroma409 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %376, i32 0, i32 11
  %377 = load i32, i32* %best_freq_split_idx_chroma409, align 4, !tbaa !135
  %sub410 = sub nsw i32 %377, 1
  %mul411 = mul nsw i32 4, %sub410
  %add412 = add nsw i32 %375, %mul411
  %add413 = add nsw i32 %add412, 5
  call void @copy_buffer(%struct.jpeg_compress_struct* %374, i32 %add413)
  br label %if.end414

if.end414:                                        ; preds = %if.else393, %if.then391
  %378 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master415 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %378, i32 0, i32 54
  %379 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master415, align 4, !tbaa !8
  %num_scans_luma416 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %379, i32 0, i32 21
  %380 = load i32, i32* %num_scans_luma416, align 4, !tbaa !117
  %381 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master417 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %381, i32 0, i32 54
  %382 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master417, align 4, !tbaa !8
  %num_scans_chroma_dc418 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %382, i32 0, i32 23
  %383 = load i32, i32* %num_scans_chroma_dc418, align 4, !tbaa !119
  %add419 = add nsw i32 %380, %383
  store i32 %add419, i32* %base_scan_idx, align 4, !tbaa !6
  %384 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %best_Al_chroma420 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %384, i32 0, i32 13
  %385 = load i32, i32* %best_Al_chroma420, align 4, !tbaa !37
  %sub421 = sub nsw i32 %385, 1
  store i32 %sub421, i32* %Al323, align 4, !tbaa !6
  br label %for.cond422

for.cond422:                                      ; preds = %for.inc432, %if.end414
  %386 = load i32, i32* %Al323, align 4, !tbaa !6
  %387 = load i32, i32* %min_Al, align 4, !tbaa !6
  %cmp423 = icmp sge i32 %386, %387
  br i1 %cmp423, label %for.body425, label %for.end434

for.body425:                                      ; preds = %for.cond422
  %388 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %389 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %390 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul426 = mul nsw i32 6, %390
  %add427 = add nsw i32 %389, %mul426
  %add428 = add nsw i32 %add427, 4
  call void @copy_buffer(%struct.jpeg_compress_struct* %388, i32 %add428)
  %391 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %392 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %393 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul429 = mul nsw i32 6, %393
  %add430 = add nsw i32 %392, %mul429
  %add431 = add nsw i32 %add430, 5
  call void @copy_buffer(%struct.jpeg_compress_struct* %391, i32 %add431)
  br label %for.inc432

for.inc432:                                       ; preds = %for.body425
  %394 = load i32, i32* %Al323, align 4, !tbaa !6
  %dec433 = add nsw i32 %394, -1
  store i32 %dec433, i32* %Al323, align 4, !tbaa !6
  br label %for.cond422

for.end434:                                       ; preds = %for.cond422
  br label %if.end435

if.end435:                                        ; preds = %for.end434, %for.end381
  %395 = load i32, i32* %min_Al, align 4, !tbaa !6
  %sub436 = sub nsw i32 %395, 1
  store i32 %sub436, i32* %Al323, align 4, !tbaa !6
  br label %for.cond437

for.cond437:                                      ; preds = %for.inc456, %if.end435
  %396 = load i32, i32* %Al323, align 4, !tbaa !6
  %cmp438 = icmp sge i32 %396, 0
  br i1 %cmp438, label %for.body440, label %for.end458

for.body440:                                      ; preds = %for.cond437
  %397 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %398 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul441 = mul nsw i32 3, %398
  %add442 = add nsw i32 3, %mul441
  call void @copy_buffer(%struct.jpeg_compress_struct* %397, i32 %add442)
  %399 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans443 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %399, i32 0, i32 22
  %400 = load i32, i32* %num_scans443, align 8, !tbaa !23
  %401 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master444 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %401, i32 0, i32 54
  %402 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master444, align 4, !tbaa !8
  %num_scans_luma445 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %402, i32 0, i32 21
  %403 = load i32, i32* %num_scans_luma445, align 4, !tbaa !117
  %cmp446 = icmp sgt i32 %400, %403
  br i1 %cmp446, label %if.then448, label %if.end455

if.then448:                                       ; preds = %for.body440
  %404 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %405 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %406 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul449 = mul nsw i32 6, %406
  %add450 = add nsw i32 %405, %mul449
  %add451 = add nsw i32 %add450, 4
  call void @copy_buffer(%struct.jpeg_compress_struct* %404, i32 %add451)
  %407 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %408 = load i32, i32* %base_scan_idx, align 4, !tbaa !6
  %409 = load i32, i32* %Al323, align 4, !tbaa !6
  %mul452 = mul nsw i32 6, %409
  %add453 = add nsw i32 %408, %mul452
  %add454 = add nsw i32 %add453, 5
  call void @copy_buffer(%struct.jpeg_compress_struct* %407, i32 %add454)
  br label %if.end455

if.end455:                                        ; preds = %if.then448, %for.body440
  br label %for.inc456

for.inc456:                                       ; preds = %if.end455
  %410 = load i32, i32* %Al323, align 4, !tbaa !6
  %dec457 = add nsw i32 %410, -1
  store i32 %dec457, i32* %Al323, align 4, !tbaa !6
  br label %for.cond437

for.end458:                                       ; preds = %for.cond437
  store i32 0, i32* %i322, align 4, !tbaa !6
  br label %for.cond459

for.cond459:                                      ; preds = %for.inc470, %for.end458
  %411 = load i32, i32* %i322, align 4, !tbaa !6
  %412 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans460 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %412, i32 0, i32 22
  %413 = load i32, i32* %num_scans460, align 8, !tbaa !23
  %cmp461 = icmp slt i32 %411, %413
  br i1 %cmp461, label %for.body463, label %for.end472

for.body463:                                      ; preds = %for.cond459
  %414 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_buffer = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %414, i32 0, i32 6
  %415 = load i32, i32* %i322, align 4, !tbaa !6
  %arrayidx464 = getelementptr inbounds [64 x i8*], [64 x i8*]* %scan_buffer, i32 0, i32 %415
  %416 = load i8*, i8** %arrayidx464, align 4, !tbaa !2
  %tobool465 = icmp ne i8* %416, null
  br i1 %tobool465, label %if.then466, label %if.end469

if.then466:                                       ; preds = %for.body463
  %417 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_buffer467 = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %417, i32 0, i32 6
  %418 = load i32, i32* %i322, align 4, !tbaa !6
  %arrayidx468 = getelementptr inbounds [64 x i8*], [64 x i8*]* %scan_buffer467, i32 0, i32 %418
  %419 = load i8*, i8** %arrayidx468, align 4, !tbaa !2
  call void @free(i8* %419)
  br label %if.end469

if.end469:                                        ; preds = %if.then466, %for.body463
  br label %for.inc470

for.inc470:                                       ; preds = %if.end469
  %420 = load i32, i32* %i322, align 4, !tbaa !6
  %inc471 = add nsw i32 %420, 1
  store i32 %inc471, i32* %i322, align 4, !tbaa !6
  br label %for.cond459

for.end472:                                       ; preds = %for.cond459
  %421 = bitcast i32* %min_Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #3
  %422 = bitcast i32* %Al323 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %422) #3
  %423 = bitcast i32* %i322 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #3
  br label %if.end473

if.end473:                                        ; preds = %for.end472, %if.end315
  %424 = bitcast i32* %passes_per_scan to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #3
  %425 = bitcast i32* %chroma_freq_split_scan_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #3
  %426 = bitcast i32* %luma_freq_split_scan_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #3
  %427 = bitcast i32* %base_scan_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #3
  %428 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #3
  ret void
}

; Function Attrs: nounwind
define internal void @copy_buffer(%struct.jpeg_compress_struct* %cinfo, i32 %scan_idx) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %scan_idx.addr = alloca i32, align 4
  %master = alloca %struct.my_comp_master*, align 4
  %size = alloca i32, align 4
  %src = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %scan_idx, i32* %scan_idx.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master1, align 4, !tbaa !8
  %3 = bitcast %struct.jpeg_comp_master* %2 to %struct.my_comp_master*
  store %struct.my_comp_master* %3, %struct.my_comp_master** %master, align 4, !tbaa !2
  %4 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_size = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %5, i32 0, i32 7
  %6 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %scan_size, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !65
  store i32 %7, i32* %size, align 4, !tbaa !65
  %8 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %scan_buffer = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %9, i32 0, i32 6
  %10 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [64 x i8*], [64 x i8*]* %scan_buffer, i32 0, i32 %10
  %11 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %11, i8** %src, align 4, !tbaa !2
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !74
  %trace_level = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 7
  %15 = load i32, i32* %trace_level, align 4, !tbaa !137
  %cmp = icmp sgt i32 %15, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0))
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 23
  %19 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !21
  %20 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %19, i32 %20
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %arrayidx3, i32 0, i32 0
  %21 = load i32, i32* %comps_in_scan, align 4, !tbaa !108
  %cmp4 = icmp slt i32 %17, %21
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %cmp5 = icmp eq i32 %23, 0
  %24 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0)
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 23
  %26 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info6, align 4, !tbaa !21
  %27 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %26, i32 %27
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %arrayidx7, i32 0, i32 1
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0), i8* %cond, i32 %29)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 23
  %33 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info10, align 4, !tbaa !21
  %34 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %33, i32 %34
  %Ss = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %arrayidx11, i32 0, i32 2
  %35 = load i32, i32* %Ss, align 4, !tbaa !105
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 23
  %37 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info12, align 4, !tbaa !21
  %38 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %37, i32 %38
  %Se = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %arrayidx13, i32 0, i32 3
  %39 = load i32, i32* %Se, align 4, !tbaa !107
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i32 0, i32 0), i32 %35, i32 %39)
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 23
  %42 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scan_info15, align 4, !tbaa !21
  %43 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %42, i32 %43
  %Ah = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %arrayidx16, i32 0, i32 4
  %44 = load i32, i32* %Ah, align 4, !tbaa !109
  %45 = load %struct.my_comp_master*, %struct.my_comp_master** %master, align 4, !tbaa !2
  %actual_Al = getelementptr inbounds %struct.my_comp_master, %struct.my_comp_master* %45, i32 0, i32 8
  %46 = load i32, i32* %scan_idx.addr, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [64 x i32], [64 x i32]* %actual_Al, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), i32 %44, i32 %47)
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.7, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  br label %while.cond

while.cond:                                       ; preds = %if.end40, %if.end
  %49 = load i32, i32* %size, align 4, !tbaa !65
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 6
  %51 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !63
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %51, i32 0, i32 1
  %52 = load i32, i32* %free_in_buffer, align 4, !tbaa !138
  %cmp20 = icmp uge i32 %49, %52
  br i1 %cmp20, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 6
  %54 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest21, align 8, !tbaa !63
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %54, i32 0, i32 0
  %55 = load i8*, i8** %next_output_byte, align 4, !tbaa !139
  %56 = load i8*, i8** %src, align 4, !tbaa !2
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 6
  %58 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest22, align 8, !tbaa !63
  %free_in_buffer23 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %58, i32 0, i32 1
  %59 = load i32, i32* %free_in_buffer23, align 4, !tbaa !138
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %55, i8* align 1 %56, i32 %59, i1 false)
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %60, i32 0, i32 6
  %61 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest24, align 8, !tbaa !63
  %free_in_buffer25 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %61, i32 0, i32 1
  %62 = load i32, i32* %free_in_buffer25, align 4, !tbaa !138
  %63 = load i8*, i8** %src, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %63, i32 %62
  store i8* %add.ptr, i8** %src, align 4, !tbaa !2
  %64 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest26 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %64, i32 0, i32 6
  %65 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest26, align 8, !tbaa !63
  %free_in_buffer27 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %65, i32 0, i32 1
  %66 = load i32, i32* %free_in_buffer27, align 4, !tbaa !138
  %67 = load i32, i32* %size, align 4, !tbaa !65
  %sub = sub i32 %67, %66
  store i32 %sub, i32* %size, align 4, !tbaa !65
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest28 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %68, i32 0, i32 6
  %69 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest28, align 8, !tbaa !63
  %free_in_buffer29 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %69, i32 0, i32 1
  %70 = load i32, i32* %free_in_buffer29, align 4, !tbaa !138
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 6
  %72 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest30, align 8, !tbaa !63
  %next_output_byte31 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %72, i32 0, i32 0
  %73 = load i8*, i8** %next_output_byte31, align 4, !tbaa !139
  %add.ptr32 = getelementptr inbounds i8, i8* %73, i32 %70
  store i8* %add.ptr32, i8** %next_output_byte31, align 4, !tbaa !139
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 6
  %75 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest33, align 8, !tbaa !63
  %free_in_buffer34 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %75, i32 0, i32 1
  store i32 0, i32* %free_in_buffer34, align 4, !tbaa !138
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %76, i32 0, i32 6
  %77 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest35, align 8, !tbaa !63
  %empty_output_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %77, i32 0, i32 3
  %78 = load i32 (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)** %empty_output_buffer, align 4, !tbaa !140
  %79 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call36 = call i32 %78(%struct.jpeg_compress_struct* %79)
  %tobool = icmp ne i32 %call36, 0
  br i1 %tobool, label %if.end40, label %if.then37

if.then37:                                        ; preds = %while.body
  %80 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %80, i32 0, i32 0
  %81 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err38, align 8, !tbaa !74
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %81, i32 0, i32 5
  store i32 127, i32* %msg_code, align 4, !tbaa !75
  %82 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err39 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %82, i32 0, i32 0
  %83 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err39, align 8, !tbaa !74
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %83, i32 0, i32 0
  %84 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !77
  %85 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %86 = bitcast %struct.jpeg_compress_struct* %85 to %struct.jpeg_common_struct*
  call void %84(%struct.jpeg_common_struct* %86)
  br label %if.end40

if.end40:                                         ; preds = %if.then37, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %87 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest41 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %87, i32 0, i32 6
  %88 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest41, align 8, !tbaa !63
  %next_output_byte42 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %88, i32 0, i32 0
  %89 = load i8*, i8** %next_output_byte42, align 4, !tbaa !139
  %90 = load i8*, i8** %src, align 4, !tbaa !2
  %91 = load i32, i32* %size, align 4, !tbaa !65
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %89, i8* align 1 %90, i32 %91, i1 false)
  %92 = load i32, i32* %size, align 4, !tbaa !65
  %93 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %93, i32 0, i32 6
  %94 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest43, align 8, !tbaa !63
  %next_output_byte44 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %94, i32 0, i32 0
  %95 = load i8*, i8** %next_output_byte44, align 4, !tbaa !139
  %add.ptr45 = getelementptr inbounds i8, i8* %95, i32 %92
  store i8* %add.ptr45, i8** %next_output_byte44, align 4, !tbaa !139
  %96 = load i32, i32* %size, align 4, !tbaa !65
  %97 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %97, i32 0, i32 6
  %98 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest46, align 8, !tbaa !63
  %free_in_buffer47 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %98, i32 0, i32 1
  %99 = load i32, i32* %free_in_buffer47, align 4, !tbaa !138
  %sub48 = sub i32 %99, %96
  store i32 %sub48, i32* %free_in_buffer47, align 4, !tbaa !138
  %100 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #3
  %101 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  %102 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #3
  %103 = bitcast %struct.my_comp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #3
  ret void
}

declare void @free(i8*) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 332}
!9 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !10, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !11, i64 216, !11, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"", !14, i64 0, !4, i64 4208, !7, i64 4212, !7, i64 4216, !7, i64 4220, !7, i64 4224, !4, i64 4228, !4, i64 4484, !4, i64 4740, !16, i64 4996, !7, i64 5000, !7, i64 5004, !7, i64 5008, !7, i64 5012, !7, i64 5016, !3, i64 5020, !3, i64 5024}
!14 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !4, i64 56, !4, i64 2104, !7, i64 4152, !7, i64 4156, !7, i64 4160, !7, i64 4164, !7, i64 4168, !7, i64 4172, !7, i64 4176, !7, i64 4180, !7, i64 4184, !7, i64 4188, !7, i64 4192, !15, i64 4196, !15, i64 4200, !15, i64 4204}
!15 = !{!"float", !4, i64 0}
!16 = !{!"long", !4, i64 0}
!17 = !{!13, !3, i64 4}
!18 = !{!13, !3, i64 8}
!19 = !{!13, !7, i64 16}
!20 = !{!13, !7, i64 12}
!21 = !{!9, !3, i64 172}
!22 = !{!9, !7, i64 228}
!23 = !{!9, !7, i64 168}
!24 = !{!9, !7, i64 180}
!25 = !{!9, !7, i64 184}
!26 = !{!13, !4, i64 4208}
!27 = !{!13, !7, i64 4220}
!28 = !{!13, !7, i64 4212}
!29 = !{!13, !7, i64 4216}
!30 = !{!13, !3, i64 5024}
!31 = !{!13, !7, i64 4224}
!32 = !{!14, !7, i64 24}
!33 = !{!14, !7, i64 40}
!34 = !{!9, !7, i64 60}
!35 = !{!14, !7, i64 4168}
!36 = !{!14, !7, i64 20}
!37 = !{!13, !7, i64 5012}
!38 = !{!14, !7, i64 44}
!39 = !{!9, !7, i64 176}
!40 = !{!9, !3, i64 352}
!41 = !{!42, !3, i64 0}
!42 = !{!"jpeg_color_converter", !3, i64 0, !3, i64 4}
!43 = !{!9, !3, i64 356}
!44 = !{!45, !3, i64 0}
!45 = !{!"jpeg_downsampler", !3, i64 0, !3, i64 4, !7, i64 8}
!46 = !{!9, !3, i64 340}
!47 = !{!48, !3, i64 0}
!48 = !{!"jpeg_c_prep_controller", !3, i64 0, !3, i64 4}
!49 = !{!9, !3, i64 360}
!50 = !{!51, !3, i64 0}
!51 = !{!"jpeg_forward_dct", !3, i64 0, !3, i64 4}
!52 = !{!9, !3, i64 364}
!53 = !{!54, !3, i64 0}
!54 = !{!"jpeg_entropy_encoder", !3, i64 0, !3, i64 4, !3, i64 8}
!55 = !{!9, !3, i64 344}
!56 = !{!57, !3, i64 0}
!57 = !{!"jpeg_c_coef_controller", !3, i64 0, !3, i64 4}
!58 = !{!9, !3, i64 336}
!59 = !{!60, !3, i64 0}
!60 = !{!"jpeg_c_main_controller", !3, i64 0, !3, i64 4}
!61 = !{!9, !7, i64 316}
!62 = !{!9, !7, i64 324}
!63 = !{!9, !3, i64 24}
!64 = !{!13, !3, i64 5020}
!65 = !{!16, !16, i64 0}
!66 = !{!67, !3, i64 8}
!67 = !{!"jpeg_destination_mgr", !3, i64 0, !16, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!68 = !{!9, !3, i64 348}
!69 = !{!70, !3, i64 4}
!70 = !{!"jpeg_marker_writer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!71 = !{!70, !3, i64 8}
!72 = !{!14, !7, i64 48}
!73 = !{!10, !10, i64 0}
!74 = !{!9, !3, i64 0}
!75 = !{!76, !7, i64 20}
!76 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !16, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!77 = !{!76, !3, i64 0}
!78 = !{!9, !3, i64 8}
!79 = !{!80, !7, i64 12}
!80 = !{!"jpeg_progress_mgr", !3, i64 0, !16, i64 4, !16, i64 8, !7, i64 12, !7, i64 16}
!81 = !{!80, !7, i64 16}
!82 = !{!14, !7, i64 12}
!83 = !{!54, !3, i64 8}
!84 = !{!67, !3, i64 16}
!85 = !{!11, !11, i64 0}
!86 = !{!9, !7, i64 32}
!87 = !{!9, !7, i64 28}
!88 = !{!9, !7, i64 36}
!89 = !{!4, !4, i64 0}
!90 = !{!9, !7, i64 56}
!91 = !{!9, !7, i64 232}
!92 = !{!9, !7, i64 236}
!93 = !{!9, !3, i64 68}
!94 = !{!95, !7, i64 8}
!95 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!96 = !{!95, !7, i64 12}
!97 = !{!95, !7, i64 4}
!98 = !{!95, !7, i64 36}
!99 = !{!95, !7, i64 28}
!100 = !{!95, !7, i64 32}
!101 = !{!95, !7, i64 40}
!102 = !{!95, !7, i64 44}
!103 = !{!95, !7, i64 48}
!104 = !{!9, !7, i64 240}
!105 = !{!106, !7, i64 20}
!106 = !{!"", !7, i64 0, !4, i64 4, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32}
!107 = !{!106, !7, i64 24}
!108 = !{!106, !7, i64 0}
!109 = !{!106, !7, i64 28}
!110 = !{!106, !7, i64 32}
!111 = !{!9, !7, i64 244}
!112 = !{!14, !7, i64 4164}
!113 = !{!9, !7, i64 320}
!114 = !{!9, !7, i64 328}
!115 = !{!14, !7, i64 4176}
!116 = !{!14, !7, i64 4188}
!117 = !{!14, !7, i64 4172}
!118 = !{!13, !7, i64 5008}
!119 = !{!14, !7, i64 4180}
!120 = !{!14, !7, i64 4192}
!121 = !{!9, !7, i64 264}
!122 = !{!9, !7, i64 268}
!123 = !{!95, !7, i64 52}
!124 = !{!95, !7, i64 56}
!125 = !{!95, !7, i64 60}
!126 = !{!95, !7, i64 64}
!127 = !{!95, !7, i64 68}
!128 = !{!95, !7, i64 72}
!129 = !{!9, !7, i64 272}
!130 = !{!9, !7, i64 204}
!131 = !{!9, !7, i64 200}
!132 = !{!13, !16, i64 4996}
!133 = !{!13, !7, i64 5000}
!134 = !{!13, !7, i64 5016}
!135 = !{!13, !7, i64 5004}
!136 = !{!14, !7, i64 4156}
!137 = !{!76, !7, i64 104}
!138 = !{!67, !16, i64 4}
!139 = !{!67, !3, i64 0}
!140 = !{!67, !3, i64 12}
