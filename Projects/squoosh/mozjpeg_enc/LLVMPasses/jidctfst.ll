; ModuleID = 'jidctfst.c'
source_filename = "jidctfst.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x {}*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }

; Function Attrs: nounwind
define hidden void @jpeg_idct_ifast(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp4 = alloca i32, align 4
  %tmp5 = alloca i32, align 4
  %tmp6 = alloca i32, align 4
  %tmp7 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %z5 = alloca i32, align 4
  %z10 = alloca i32, align 4
  %z11 = alloca i32, align 4
  %z12 = alloca i32, align 4
  %z13 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [64 x i32], align 16
  %dcval = alloca i32, align 4
  %dcval159 = alloca i8, align 1
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 65
  %23 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %23, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %24 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  %25 = bitcast [64 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %25) #2
  %26 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %26, i16** %inptr, align 4, !tbaa !2
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 20
  %28 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %29 = bitcast i8* %28 to i32*
  store i32* %29, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 8, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %30 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %30, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %31, i32 8
  %32 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %32 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %33, i32 16
  %34 = load i16, i16* %arrayidx3, align 2, !tbaa !14
  %conv4 = sext i16 %34 to i32
  %cmp5 = icmp eq i32 %conv4, 0
  br i1 %cmp5, label %land.lhs.true7, label %if.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %35 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %35, i32 24
  %36 = load i16, i16* %arrayidx8, align 2, !tbaa !14
  %conv9 = sext i16 %36 to i32
  %cmp10 = icmp eq i32 %conv9, 0
  br i1 %cmp10, label %land.lhs.true12, label %if.end

land.lhs.true12:                                  ; preds = %land.lhs.true7
  %37 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %37, i32 32
  %38 = load i16, i16* %arrayidx13, align 2, !tbaa !14
  %conv14 = sext i16 %38 to i32
  %cmp15 = icmp eq i32 %conv14, 0
  br i1 %cmp15, label %land.lhs.true17, label %if.end

land.lhs.true17:                                  ; preds = %land.lhs.true12
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %39, i32 40
  %40 = load i16, i16* %arrayidx18, align 2, !tbaa !14
  %conv19 = sext i16 %40 to i32
  %cmp20 = icmp eq i32 %conv19, 0
  br i1 %cmp20, label %land.lhs.true22, label %if.end

land.lhs.true22:                                  ; preds = %land.lhs.true17
  %41 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i16, i16* %41, i32 48
  %42 = load i16, i16* %arrayidx23, align 2, !tbaa !14
  %conv24 = sext i16 %42 to i32
  %cmp25 = icmp eq i32 %conv24, 0
  br i1 %cmp25, label %land.lhs.true27, label %if.end

land.lhs.true27:                                  ; preds = %land.lhs.true22
  %43 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %43, i32 56
  %44 = load i16, i16* %arrayidx28, align 2, !tbaa !14
  %conv29 = sext i16 %44 to i32
  %cmp30 = icmp eq i32 %conv29, 0
  br i1 %cmp30, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true27
  %45 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #2
  %46 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i16, i16* %46, i32 0
  %47 = load i16, i16* %arrayidx32, align 2, !tbaa !14
  %conv33 = sext i16 %47 to i32
  %48 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %48, i32 0
  %49 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %mul = mul nsw i32 %conv33, %49
  store i32 %mul, i32* %dcval, align 4, !tbaa !6
  %50 = load i32, i32* %dcval, align 4, !tbaa !6
  %51 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %51, i32 0
  store i32 %50, i32* %arrayidx35, align 4, !tbaa !6
  %52 = load i32, i32* %dcval, align 4, !tbaa !6
  %53 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %53, i32 8
  store i32 %52, i32* %arrayidx36, align 4, !tbaa !6
  %54 = load i32, i32* %dcval, align 4, !tbaa !6
  %55 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %55, i32 16
  store i32 %54, i32* %arrayidx37, align 4, !tbaa !6
  %56 = load i32, i32* %dcval, align 4, !tbaa !6
  %57 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %57, i32 24
  store i32 %56, i32* %arrayidx38, align 4, !tbaa !6
  %58 = load i32, i32* %dcval, align 4, !tbaa !6
  %59 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %59, i32 32
  store i32 %58, i32* %arrayidx39, align 4, !tbaa !6
  %60 = load i32, i32* %dcval, align 4, !tbaa !6
  %61 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %61, i32 40
  store i32 %60, i32* %arrayidx40, align 4, !tbaa !6
  %62 = load i32, i32* %dcval, align 4, !tbaa !6
  %63 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %63, i32 48
  store i32 %62, i32* %arrayidx41, align 4, !tbaa !6
  %64 = load i32, i32* %dcval, align 4, !tbaa !6
  %65 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %65, i32 56
  store i32 %64, i32* %arrayidx42, align 4, !tbaa !6
  %66 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %66, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %67 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr43 = getelementptr inbounds i32, i32* %67, i32 1
  store i32* %incdec.ptr43, i32** %quantptr, align 4, !tbaa !2
  %68 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr44 = getelementptr inbounds i32, i32* %68, i32 1
  store i32* %incdec.ptr44, i32** %wsptr, align 4, !tbaa !2
  %69 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #2
  br label %for.inc

if.end:                                           ; preds = %land.lhs.true27, %land.lhs.true22, %land.lhs.true17, %land.lhs.true12, %land.lhs.true7, %land.lhs.true, %for.body
  %70 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i16, i16* %70, i32 0
  %71 = load i16, i16* %arrayidx45, align 2, !tbaa !14
  %conv46 = sext i16 %71 to i32
  %72 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %72, i32 0
  %73 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %conv46, %73
  store i32 %mul48, i32* %tmp0, align 4, !tbaa !6
  %74 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i16, i16* %74, i32 16
  %75 = load i16, i16* %arrayidx49, align 2, !tbaa !14
  %conv50 = sext i16 %75 to i32
  %76 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %76, i32 16
  %77 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %mul52 = mul nsw i32 %conv50, %77
  store i32 %mul52, i32* %tmp1, align 4, !tbaa !6
  %78 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i16, i16* %78, i32 32
  %79 = load i16, i16* %arrayidx53, align 2, !tbaa !14
  %conv54 = sext i16 %79 to i32
  %80 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %80, i32 32
  %81 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %mul56 = mul nsw i32 %conv54, %81
  store i32 %mul56, i32* %tmp2, align 4, !tbaa !6
  %82 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i16, i16* %82, i32 48
  %83 = load i16, i16* %arrayidx57, align 2, !tbaa !14
  %conv58 = sext i16 %83 to i32
  %84 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %84, i32 48
  %85 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %mul60 = mul nsw i32 %conv58, %85
  store i32 %mul60, i32* %tmp3, align 4, !tbaa !6
  %86 = load i32, i32* %tmp0, align 4, !tbaa !6
  %87 = load i32, i32* %tmp2, align 4, !tbaa !6
  %add = add nsw i32 %86, %87
  store i32 %add, i32* %tmp10, align 4, !tbaa !6
  %88 = load i32, i32* %tmp0, align 4, !tbaa !6
  %89 = load i32, i32* %tmp2, align 4, !tbaa !6
  %sub = sub nsw i32 %88, %89
  store i32 %sub, i32* %tmp11, align 4, !tbaa !6
  %90 = load i32, i32* %tmp1, align 4, !tbaa !6
  %91 = load i32, i32* %tmp3, align 4, !tbaa !6
  %add61 = add nsw i32 %90, %91
  store i32 %add61, i32* %tmp13, align 4, !tbaa !6
  %92 = load i32, i32* %tmp1, align 4, !tbaa !6
  %93 = load i32, i32* %tmp3, align 4, !tbaa !6
  %sub62 = sub nsw i32 %92, %93
  %mul63 = mul nsw i32 %sub62, 362
  %shr = ashr i32 %mul63, 8
  %94 = load i32, i32* %tmp13, align 4, !tbaa !6
  %sub64 = sub nsw i32 %shr, %94
  store i32 %sub64, i32* %tmp12, align 4, !tbaa !6
  %95 = load i32, i32* %tmp10, align 4, !tbaa !6
  %96 = load i32, i32* %tmp13, align 4, !tbaa !6
  %add65 = add nsw i32 %95, %96
  store i32 %add65, i32* %tmp0, align 4, !tbaa !6
  %97 = load i32, i32* %tmp10, align 4, !tbaa !6
  %98 = load i32, i32* %tmp13, align 4, !tbaa !6
  %sub66 = sub nsw i32 %97, %98
  store i32 %sub66, i32* %tmp3, align 4, !tbaa !6
  %99 = load i32, i32* %tmp11, align 4, !tbaa !6
  %100 = load i32, i32* %tmp12, align 4, !tbaa !6
  %add67 = add nsw i32 %99, %100
  store i32 %add67, i32* %tmp1, align 4, !tbaa !6
  %101 = load i32, i32* %tmp11, align 4, !tbaa !6
  %102 = load i32, i32* %tmp12, align 4, !tbaa !6
  %sub68 = sub nsw i32 %101, %102
  store i32 %sub68, i32* %tmp2, align 4, !tbaa !6
  %103 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i16, i16* %103, i32 8
  %104 = load i16, i16* %arrayidx69, align 2, !tbaa !14
  %conv70 = sext i16 %104 to i32
  %105 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %105, i32 8
  %106 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %mul72 = mul nsw i32 %conv70, %106
  store i32 %mul72, i32* %tmp4, align 4, !tbaa !6
  %107 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i16, i16* %107, i32 24
  %108 = load i16, i16* %arrayidx73, align 2, !tbaa !14
  %conv74 = sext i16 %108 to i32
  %109 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %109, i32 24
  %110 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %mul76 = mul nsw i32 %conv74, %110
  store i32 %mul76, i32* %tmp5, align 4, !tbaa !6
  %111 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i16, i16* %111, i32 40
  %112 = load i16, i16* %arrayidx77, align 2, !tbaa !14
  %conv78 = sext i16 %112 to i32
  %113 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %113, i32 40
  %114 = load i32, i32* %arrayidx79, align 4, !tbaa !6
  %mul80 = mul nsw i32 %conv78, %114
  store i32 %mul80, i32* %tmp6, align 4, !tbaa !6
  %115 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i16, i16* %115, i32 56
  %116 = load i16, i16* %arrayidx81, align 2, !tbaa !14
  %conv82 = sext i16 %116 to i32
  %117 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %117, i32 56
  %118 = load i32, i32* %arrayidx83, align 4, !tbaa !6
  %mul84 = mul nsw i32 %conv82, %118
  store i32 %mul84, i32* %tmp7, align 4, !tbaa !6
  %119 = load i32, i32* %tmp6, align 4, !tbaa !6
  %120 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add85 = add nsw i32 %119, %120
  store i32 %add85, i32* %z13, align 4, !tbaa !6
  %121 = load i32, i32* %tmp6, align 4, !tbaa !6
  %122 = load i32, i32* %tmp5, align 4, !tbaa !6
  %sub86 = sub nsw i32 %121, %122
  store i32 %sub86, i32* %z10, align 4, !tbaa !6
  %123 = load i32, i32* %tmp4, align 4, !tbaa !6
  %124 = load i32, i32* %tmp7, align 4, !tbaa !6
  %add87 = add nsw i32 %123, %124
  store i32 %add87, i32* %z11, align 4, !tbaa !6
  %125 = load i32, i32* %tmp4, align 4, !tbaa !6
  %126 = load i32, i32* %tmp7, align 4, !tbaa !6
  %sub88 = sub nsw i32 %125, %126
  store i32 %sub88, i32* %z12, align 4, !tbaa !6
  %127 = load i32, i32* %z11, align 4, !tbaa !6
  %128 = load i32, i32* %z13, align 4, !tbaa !6
  %add89 = add nsw i32 %127, %128
  store i32 %add89, i32* %tmp7, align 4, !tbaa !6
  %129 = load i32, i32* %z11, align 4, !tbaa !6
  %130 = load i32, i32* %z13, align 4, !tbaa !6
  %sub90 = sub nsw i32 %129, %130
  %mul91 = mul nsw i32 %sub90, 362
  %shr92 = ashr i32 %mul91, 8
  store i32 %shr92, i32* %tmp11, align 4, !tbaa !6
  %131 = load i32, i32* %z10, align 4, !tbaa !6
  %132 = load i32, i32* %z12, align 4, !tbaa !6
  %add93 = add nsw i32 %131, %132
  %mul94 = mul nsw i32 %add93, 473
  %shr95 = ashr i32 %mul94, 8
  store i32 %shr95, i32* %z5, align 4, !tbaa !6
  %133 = load i32, i32* %z12, align 4, !tbaa !6
  %mul96 = mul nsw i32 %133, 277
  %shr97 = ashr i32 %mul96, 8
  %134 = load i32, i32* %z5, align 4, !tbaa !6
  %sub98 = sub nsw i32 %shr97, %134
  store i32 %sub98, i32* %tmp10, align 4, !tbaa !6
  %135 = load i32, i32* %z10, align 4, !tbaa !6
  %mul99 = mul nsw i32 %135, -669
  %shr100 = ashr i32 %mul99, 8
  %136 = load i32, i32* %z5, align 4, !tbaa !6
  %add101 = add nsw i32 %shr100, %136
  store i32 %add101, i32* %tmp12, align 4, !tbaa !6
  %137 = load i32, i32* %tmp12, align 4, !tbaa !6
  %138 = load i32, i32* %tmp7, align 4, !tbaa !6
  %sub102 = sub nsw i32 %137, %138
  store i32 %sub102, i32* %tmp6, align 4, !tbaa !6
  %139 = load i32, i32* %tmp11, align 4, !tbaa !6
  %140 = load i32, i32* %tmp6, align 4, !tbaa !6
  %sub103 = sub nsw i32 %139, %140
  store i32 %sub103, i32* %tmp5, align 4, !tbaa !6
  %141 = load i32, i32* %tmp10, align 4, !tbaa !6
  %142 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add104 = add nsw i32 %141, %142
  store i32 %add104, i32* %tmp4, align 4, !tbaa !6
  %143 = load i32, i32* %tmp0, align 4, !tbaa !6
  %144 = load i32, i32* %tmp7, align 4, !tbaa !6
  %add105 = add nsw i32 %143, %144
  %145 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %145, i32 0
  store i32 %add105, i32* %arrayidx106, align 4, !tbaa !6
  %146 = load i32, i32* %tmp0, align 4, !tbaa !6
  %147 = load i32, i32* %tmp7, align 4, !tbaa !6
  %sub107 = sub nsw i32 %146, %147
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %148, i32 56
  store i32 %sub107, i32* %arrayidx108, align 4, !tbaa !6
  %149 = load i32, i32* %tmp1, align 4, !tbaa !6
  %150 = load i32, i32* %tmp6, align 4, !tbaa !6
  %add109 = add nsw i32 %149, %150
  %151 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %151, i32 8
  store i32 %add109, i32* %arrayidx110, align 4, !tbaa !6
  %152 = load i32, i32* %tmp1, align 4, !tbaa !6
  %153 = load i32, i32* %tmp6, align 4, !tbaa !6
  %sub111 = sub nsw i32 %152, %153
  %154 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %154, i32 48
  store i32 %sub111, i32* %arrayidx112, align 4, !tbaa !6
  %155 = load i32, i32* %tmp2, align 4, !tbaa !6
  %156 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add113 = add nsw i32 %155, %156
  %157 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds i32, i32* %157, i32 16
  store i32 %add113, i32* %arrayidx114, align 4, !tbaa !6
  %158 = load i32, i32* %tmp2, align 4, !tbaa !6
  %159 = load i32, i32* %tmp5, align 4, !tbaa !6
  %sub115 = sub nsw i32 %158, %159
  %160 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %160, i32 40
  store i32 %sub115, i32* %arrayidx116, align 4, !tbaa !6
  %161 = load i32, i32* %tmp3, align 4, !tbaa !6
  %162 = load i32, i32* %tmp4, align 4, !tbaa !6
  %add117 = add nsw i32 %161, %162
  %163 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %163, i32 32
  store i32 %add117, i32* %arrayidx118, align 4, !tbaa !6
  %164 = load i32, i32* %tmp3, align 4, !tbaa !6
  %165 = load i32, i32* %tmp4, align 4, !tbaa !6
  %sub119 = sub nsw i32 %164, %165
  %166 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %166, i32 24
  store i32 %sub119, i32* %arrayidx120, align 4, !tbaa !6
  %167 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr121 = getelementptr inbounds i16, i16* %167, i32 1
  store i16* %incdec.ptr121, i16** %inptr, align 4, !tbaa !2
  %168 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr122 = getelementptr inbounds i32, i32* %168, i32 1
  store i32* %incdec.ptr122, i32** %quantptr, align 4, !tbaa !2
  %169 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr123 = getelementptr inbounds i32, i32* %169, i32 1
  store i32* %incdec.ptr123, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %170 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %170, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay124 = getelementptr inbounds [64 x i32], [64 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay124, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc261, %for.end
  %171 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp126 = icmp slt i32 %171, 8
  br i1 %cmp126, label %for.body128, label %for.end262

for.body128:                                      ; preds = %for.cond125
  %172 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %173 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx129 = getelementptr inbounds i8*, i8** %172, i32 %173
  %174 = load i8*, i8** %arrayidx129, align 4, !tbaa !2
  %175 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr130 = getelementptr inbounds i8, i8* %174, i32 %175
  store i8* %add.ptr130, i8** %outptr, align 4, !tbaa !2
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %176, i32 1
  %177 = load i32, i32* %arrayidx131, align 4, !tbaa !6
  %cmp132 = icmp eq i32 %177, 0
  br i1 %cmp132, label %land.lhs.true134, label %if.end172

land.lhs.true134:                                 ; preds = %for.body128
  %178 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %178, i32 2
  %179 = load i32, i32* %arrayidx135, align 4, !tbaa !6
  %cmp136 = icmp eq i32 %179, 0
  br i1 %cmp136, label %land.lhs.true138, label %if.end172

land.lhs.true138:                                 ; preds = %land.lhs.true134
  %180 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %180, i32 3
  %181 = load i32, i32* %arrayidx139, align 4, !tbaa !6
  %cmp140 = icmp eq i32 %181, 0
  br i1 %cmp140, label %land.lhs.true142, label %if.end172

land.lhs.true142:                                 ; preds = %land.lhs.true138
  %182 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %182, i32 4
  %183 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  %cmp144 = icmp eq i32 %183, 0
  br i1 %cmp144, label %land.lhs.true146, label %if.end172

land.lhs.true146:                                 ; preds = %land.lhs.true142
  %184 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %184, i32 5
  %185 = load i32, i32* %arrayidx147, align 4, !tbaa !6
  %cmp148 = icmp eq i32 %185, 0
  br i1 %cmp148, label %land.lhs.true150, label %if.end172

land.lhs.true150:                                 ; preds = %land.lhs.true146
  %186 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %186, i32 6
  %187 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  %cmp152 = icmp eq i32 %187, 0
  br i1 %cmp152, label %land.lhs.true154, label %if.end172

land.lhs.true154:                                 ; preds = %land.lhs.true150
  %188 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %188, i32 7
  %189 = load i32, i32* %arrayidx155, align 4, !tbaa !6
  %cmp156 = icmp eq i32 %189, 0
  br i1 %cmp156, label %if.then158, label %if.end172

if.then158:                                       ; preds = %land.lhs.true154
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %dcval159) #2
  %190 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %191 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i32, i32* %191, i32 0
  %192 = load i32, i32* %arrayidx160, align 4, !tbaa !6
  %shr161 = ashr i32 %192, 5
  %and = and i32 %shr161, 1023
  %arrayidx162 = getelementptr inbounds i8, i8* %190, i32 %and
  %193 = load i8, i8* %arrayidx162, align 1, !tbaa !15
  store i8 %193, i8* %dcval159, align 1, !tbaa !15
  %194 = load i8, i8* %dcval159, align 1, !tbaa !15
  %195 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i8, i8* %195, i32 0
  store i8 %194, i8* %arrayidx163, align 1, !tbaa !15
  %196 = load i8, i8* %dcval159, align 1, !tbaa !15
  %197 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i8, i8* %197, i32 1
  store i8 %196, i8* %arrayidx164, align 1, !tbaa !15
  %198 = load i8, i8* %dcval159, align 1, !tbaa !15
  %199 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i8, i8* %199, i32 2
  store i8 %198, i8* %arrayidx165, align 1, !tbaa !15
  %200 = load i8, i8* %dcval159, align 1, !tbaa !15
  %201 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i8, i8* %201, i32 3
  store i8 %200, i8* %arrayidx166, align 1, !tbaa !15
  %202 = load i8, i8* %dcval159, align 1, !tbaa !15
  %203 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i8, i8* %203, i32 4
  store i8 %202, i8* %arrayidx167, align 1, !tbaa !15
  %204 = load i8, i8* %dcval159, align 1, !tbaa !15
  %205 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i8, i8* %205, i32 5
  store i8 %204, i8* %arrayidx168, align 1, !tbaa !15
  %206 = load i8, i8* %dcval159, align 1, !tbaa !15
  %207 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i8, i8* %207, i32 6
  store i8 %206, i8* %arrayidx169, align 1, !tbaa !15
  %208 = load i8, i8* %dcval159, align 1, !tbaa !15
  %209 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i8, i8* %209, i32 7
  store i8 %208, i8* %arrayidx170, align 1, !tbaa !15
  %210 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr171 = getelementptr inbounds i32, i32* %210, i32 8
  store i32* %add.ptr171, i32** %wsptr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %dcval159) #2
  br label %for.inc261

if.end172:                                        ; preds = %land.lhs.true154, %land.lhs.true150, %land.lhs.true146, %land.lhs.true142, %land.lhs.true138, %land.lhs.true134, %for.body128
  %211 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i32, i32* %211, i32 0
  %212 = load i32, i32* %arrayidx173, align 4, !tbaa !6
  %213 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %213, i32 4
  %214 = load i32, i32* %arrayidx174, align 4, !tbaa !6
  %add175 = add nsw i32 %212, %214
  store i32 %add175, i32* %tmp10, align 4, !tbaa !6
  %215 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %215, i32 0
  %216 = load i32, i32* %arrayidx176, align 4, !tbaa !6
  %217 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %217, i32 4
  %218 = load i32, i32* %arrayidx177, align 4, !tbaa !6
  %sub178 = sub nsw i32 %216, %218
  store i32 %sub178, i32* %tmp11, align 4, !tbaa !6
  %219 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %219, i32 2
  %220 = load i32, i32* %arrayidx179, align 4, !tbaa !6
  %221 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i32, i32* %221, i32 6
  %222 = load i32, i32* %arrayidx180, align 4, !tbaa !6
  %add181 = add nsw i32 %220, %222
  store i32 %add181, i32* %tmp13, align 4, !tbaa !6
  %223 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i32, i32* %223, i32 2
  %224 = load i32, i32* %arrayidx182, align 4, !tbaa !6
  %225 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %225, i32 6
  %226 = load i32, i32* %arrayidx183, align 4, !tbaa !6
  %sub184 = sub nsw i32 %224, %226
  %mul185 = mul nsw i32 %sub184, 362
  %shr186 = ashr i32 %mul185, 8
  %227 = load i32, i32* %tmp13, align 4, !tbaa !6
  %sub187 = sub nsw i32 %shr186, %227
  store i32 %sub187, i32* %tmp12, align 4, !tbaa !6
  %228 = load i32, i32* %tmp10, align 4, !tbaa !6
  %229 = load i32, i32* %tmp13, align 4, !tbaa !6
  %add188 = add nsw i32 %228, %229
  store i32 %add188, i32* %tmp0, align 4, !tbaa !6
  %230 = load i32, i32* %tmp10, align 4, !tbaa !6
  %231 = load i32, i32* %tmp13, align 4, !tbaa !6
  %sub189 = sub nsw i32 %230, %231
  store i32 %sub189, i32* %tmp3, align 4, !tbaa !6
  %232 = load i32, i32* %tmp11, align 4, !tbaa !6
  %233 = load i32, i32* %tmp12, align 4, !tbaa !6
  %add190 = add nsw i32 %232, %233
  store i32 %add190, i32* %tmp1, align 4, !tbaa !6
  %234 = load i32, i32* %tmp11, align 4, !tbaa !6
  %235 = load i32, i32* %tmp12, align 4, !tbaa !6
  %sub191 = sub nsw i32 %234, %235
  store i32 %sub191, i32* %tmp2, align 4, !tbaa !6
  %236 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %236, i32 5
  %237 = load i32, i32* %arrayidx192, align 4, !tbaa !6
  %238 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i32, i32* %238, i32 3
  %239 = load i32, i32* %arrayidx193, align 4, !tbaa !6
  %add194 = add nsw i32 %237, %239
  store i32 %add194, i32* %z13, align 4, !tbaa !6
  %240 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i32, i32* %240, i32 5
  %241 = load i32, i32* %arrayidx195, align 4, !tbaa !6
  %242 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %242, i32 3
  %243 = load i32, i32* %arrayidx196, align 4, !tbaa !6
  %sub197 = sub nsw i32 %241, %243
  store i32 %sub197, i32* %z10, align 4, !tbaa !6
  %244 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %244, i32 1
  %245 = load i32, i32* %arrayidx198, align 4, !tbaa !6
  %246 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %246, i32 7
  %247 = load i32, i32* %arrayidx199, align 4, !tbaa !6
  %add200 = add nsw i32 %245, %247
  store i32 %add200, i32* %z11, align 4, !tbaa !6
  %248 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i32, i32* %248, i32 1
  %249 = load i32, i32* %arrayidx201, align 4, !tbaa !6
  %250 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds i32, i32* %250, i32 7
  %251 = load i32, i32* %arrayidx202, align 4, !tbaa !6
  %sub203 = sub nsw i32 %249, %251
  store i32 %sub203, i32* %z12, align 4, !tbaa !6
  %252 = load i32, i32* %z11, align 4, !tbaa !6
  %253 = load i32, i32* %z13, align 4, !tbaa !6
  %add204 = add nsw i32 %252, %253
  store i32 %add204, i32* %tmp7, align 4, !tbaa !6
  %254 = load i32, i32* %z11, align 4, !tbaa !6
  %255 = load i32, i32* %z13, align 4, !tbaa !6
  %sub205 = sub nsw i32 %254, %255
  %mul206 = mul nsw i32 %sub205, 362
  %shr207 = ashr i32 %mul206, 8
  store i32 %shr207, i32* %tmp11, align 4, !tbaa !6
  %256 = load i32, i32* %z10, align 4, !tbaa !6
  %257 = load i32, i32* %z12, align 4, !tbaa !6
  %add208 = add nsw i32 %256, %257
  %mul209 = mul nsw i32 %add208, 473
  %shr210 = ashr i32 %mul209, 8
  store i32 %shr210, i32* %z5, align 4, !tbaa !6
  %258 = load i32, i32* %z12, align 4, !tbaa !6
  %mul211 = mul nsw i32 %258, 277
  %shr212 = ashr i32 %mul211, 8
  %259 = load i32, i32* %z5, align 4, !tbaa !6
  %sub213 = sub nsw i32 %shr212, %259
  store i32 %sub213, i32* %tmp10, align 4, !tbaa !6
  %260 = load i32, i32* %z10, align 4, !tbaa !6
  %mul214 = mul nsw i32 %260, -669
  %shr215 = ashr i32 %mul214, 8
  %261 = load i32, i32* %z5, align 4, !tbaa !6
  %add216 = add nsw i32 %shr215, %261
  store i32 %add216, i32* %tmp12, align 4, !tbaa !6
  %262 = load i32, i32* %tmp12, align 4, !tbaa !6
  %263 = load i32, i32* %tmp7, align 4, !tbaa !6
  %sub217 = sub nsw i32 %262, %263
  store i32 %sub217, i32* %tmp6, align 4, !tbaa !6
  %264 = load i32, i32* %tmp11, align 4, !tbaa !6
  %265 = load i32, i32* %tmp6, align 4, !tbaa !6
  %sub218 = sub nsw i32 %264, %265
  store i32 %sub218, i32* %tmp5, align 4, !tbaa !6
  %266 = load i32, i32* %tmp10, align 4, !tbaa !6
  %267 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add219 = add nsw i32 %266, %267
  store i32 %add219, i32* %tmp4, align 4, !tbaa !6
  %268 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %269 = load i32, i32* %tmp0, align 4, !tbaa !6
  %270 = load i32, i32* %tmp7, align 4, !tbaa !6
  %add220 = add nsw i32 %269, %270
  %shr221 = ashr i32 %add220, 5
  %and222 = and i32 %shr221, 1023
  %arrayidx223 = getelementptr inbounds i8, i8* %268, i32 %and222
  %271 = load i8, i8* %arrayidx223, align 1, !tbaa !15
  %272 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i8, i8* %272, i32 0
  store i8 %271, i8* %arrayidx224, align 1, !tbaa !15
  %273 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %274 = load i32, i32* %tmp0, align 4, !tbaa !6
  %275 = load i32, i32* %tmp7, align 4, !tbaa !6
  %sub225 = sub nsw i32 %274, %275
  %shr226 = ashr i32 %sub225, 5
  %and227 = and i32 %shr226, 1023
  %arrayidx228 = getelementptr inbounds i8, i8* %273, i32 %and227
  %276 = load i8, i8* %arrayidx228, align 1, !tbaa !15
  %277 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i8, i8* %277, i32 7
  store i8 %276, i8* %arrayidx229, align 1, !tbaa !15
  %278 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %279 = load i32, i32* %tmp1, align 4, !tbaa !6
  %280 = load i32, i32* %tmp6, align 4, !tbaa !6
  %add230 = add nsw i32 %279, %280
  %shr231 = ashr i32 %add230, 5
  %and232 = and i32 %shr231, 1023
  %arrayidx233 = getelementptr inbounds i8, i8* %278, i32 %and232
  %281 = load i8, i8* %arrayidx233, align 1, !tbaa !15
  %282 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i8, i8* %282, i32 1
  store i8 %281, i8* %arrayidx234, align 1, !tbaa !15
  %283 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %284 = load i32, i32* %tmp1, align 4, !tbaa !6
  %285 = load i32, i32* %tmp6, align 4, !tbaa !6
  %sub235 = sub nsw i32 %284, %285
  %shr236 = ashr i32 %sub235, 5
  %and237 = and i32 %shr236, 1023
  %arrayidx238 = getelementptr inbounds i8, i8* %283, i32 %and237
  %286 = load i8, i8* %arrayidx238, align 1, !tbaa !15
  %287 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i8, i8* %287, i32 6
  store i8 %286, i8* %arrayidx239, align 1, !tbaa !15
  %288 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %289 = load i32, i32* %tmp2, align 4, !tbaa !6
  %290 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add240 = add nsw i32 %289, %290
  %shr241 = ashr i32 %add240, 5
  %and242 = and i32 %shr241, 1023
  %arrayidx243 = getelementptr inbounds i8, i8* %288, i32 %and242
  %291 = load i8, i8* %arrayidx243, align 1, !tbaa !15
  %292 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i8, i8* %292, i32 2
  store i8 %291, i8* %arrayidx244, align 1, !tbaa !15
  %293 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %294 = load i32, i32* %tmp2, align 4, !tbaa !6
  %295 = load i32, i32* %tmp5, align 4, !tbaa !6
  %sub245 = sub nsw i32 %294, %295
  %shr246 = ashr i32 %sub245, 5
  %and247 = and i32 %shr246, 1023
  %arrayidx248 = getelementptr inbounds i8, i8* %293, i32 %and247
  %296 = load i8, i8* %arrayidx248, align 1, !tbaa !15
  %297 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i8, i8* %297, i32 5
  store i8 %296, i8* %arrayidx249, align 1, !tbaa !15
  %298 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %299 = load i32, i32* %tmp3, align 4, !tbaa !6
  %300 = load i32, i32* %tmp4, align 4, !tbaa !6
  %add250 = add nsw i32 %299, %300
  %shr251 = ashr i32 %add250, 5
  %and252 = and i32 %shr251, 1023
  %arrayidx253 = getelementptr inbounds i8, i8* %298, i32 %and252
  %301 = load i8, i8* %arrayidx253, align 1, !tbaa !15
  %302 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i8, i8* %302, i32 4
  store i8 %301, i8* %arrayidx254, align 1, !tbaa !15
  %303 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %304 = load i32, i32* %tmp3, align 4, !tbaa !6
  %305 = load i32, i32* %tmp4, align 4, !tbaa !6
  %sub255 = sub nsw i32 %304, %305
  %shr256 = ashr i32 %sub255, 5
  %and257 = and i32 %shr256, 1023
  %arrayidx258 = getelementptr inbounds i8, i8* %303, i32 %and257
  %306 = load i8, i8* %arrayidx258, align 1, !tbaa !15
  %307 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i8, i8* %307, i32 3
  store i8 %306, i8* %arrayidx259, align 1, !tbaa !15
  %308 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr260 = getelementptr inbounds i32, i32* %308, i32 8
  store i32* %add.ptr260, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc261

for.inc261:                                       ; preds = %if.end172, %if.then158
  %309 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %309, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  br label %for.cond125

for.end262:                                       ; preds = %for.cond125
  %310 = bitcast [64 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %310) #2
  %311 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #2
  %312 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #2
  %313 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #2
  %314 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #2
  %315 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #2
  %316 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #2
  %317 = bitcast i32* %z13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #2
  %318 = bitcast i32* %z12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #2
  %319 = bitcast i32* %z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #2
  %320 = bitcast i32* %z10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #2
  %321 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #2
  %322 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #2
  %323 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #2
  %324 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #2
  %325 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #2
  %326 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #2
  %327 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #2
  %328 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %328) #2
  %329 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %329) #2
  %330 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #2
  %331 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #2
  %332 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #2
  %333 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 324}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 80}
!13 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!14 = !{!11, !11, i64 0}
!15 = !{!4, !4, i64 0}
