; ModuleID = 'jmemmgr.c'
source_filename = "jmemmgr.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%struct.jpeg_error_mgr = type { {}*, void (%struct.jpeg_common_struct*, i32)*, {}*, void (%struct.jpeg_common_struct*, i8*)*, {}*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type { i8**, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.jvirt_sarray_control*, %struct.backing_store_struct }
%struct.backing_store_struct = type { void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)*, %struct._IO_FILE*, [64 x i8] }
%struct._IO_FILE = type opaque
%struct.jvirt_barray_control = type { [64 x i16]**, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.jvirt_barray_control*, %struct.backing_store_struct }
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.my_memory_mgr = type { %struct.jpeg_memory_mgr, [2 x %struct.small_pool_struct*], [2 x %struct.large_pool_struct*], %struct.jvirt_sarray_control*, %struct.jvirt_barray_control*, i32, i32 }
%struct.small_pool_struct = type { %struct.small_pool_struct*, i32, i32 }
%struct.large_pool_struct = type { %struct.large_pool_struct*, i32, i32 }

@.str = private unnamed_addr constant [8 x i8] c"JPEGMEM\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%ld%c\00", align 1
@first_pool_slop = internal constant [2 x i32] [i32 1600, i32 16000], align 4
@extra_pool_slop = internal constant [2 x i32] [i32 0, i32 5000], align 4

; Function Attrs: nounwind
define hidden void @jinit_memory_mgr(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %max_to_use = alloca i32, align 4
  %pool = alloca i32, align 4
  %test_mac = alloca i32, align 4
  %memenv = alloca i8*, align 4
  %ch = alloca i8, align 1
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %max_to_use to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %test_mac to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %4, i32 0, i32 1
  store %struct.jpeg_memory_mgr* null, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  store i32 1000000000, i32* %test_mac, align 4, !tbaa !9
  %5 = load i32, i32* %test_mac, align 4, !tbaa !9
  %cmp = icmp ne i32 %5, 1000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 5
  store i32 3, i32* %msg_code, align 4, !tbaa !12
  %8 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 0
  %error_exit3 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %10 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit3, align 4, !tbaa !14
  %11 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %10(%struct.jpeg_common_struct* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @jpeg_mem_init(%struct.jpeg_common_struct* %12)
  store i32 %call, i32* %max_to_use, align 4, !tbaa !9
  %13 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %call4 = call i8* @jpeg_get_small(%struct.jpeg_common_struct* %13, i32 84)
  %14 = bitcast i8* %call4 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %14, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %15 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %cmp5 = icmp eq %struct.my_memory_mgr* %15, null
  br i1 %cmp5, label %if.then6, label %if.end13

if.then6:                                         ; preds = %if.end
  %16 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_mem_term(%struct.jpeg_common_struct* %16)
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 4, !tbaa !11
  %msg_code8 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 54, i32* %msg_code8, align 4, !tbaa !12
  %19 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4, !tbaa !15
  %21 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 4, !tbaa !11
  %error_exit11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %error_exit12 = bitcast {}** %error_exit11 to void (%struct.jpeg_common_struct*)**
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit12, align 4, !tbaa !14
  %24 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %23(%struct.jpeg_common_struct* %24)
  br label %if.end13

if.end13:                                         ; preds = %if.then6, %if.end
  %25 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %25, i32 0, i32 0
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub, i32 0, i32 0
  store i8* (%struct.jpeg_common_struct*, i32, i32)* @alloc_small, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !16
  %26 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub14 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %26, i32 0, i32 0
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub14, i32 0, i32 1
  store i8* (%struct.jpeg_common_struct*, i32, i32)* @alloc_large, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !19
  %27 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub15 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %27, i32 0, i32 0
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub15, i32 0, i32 2
  store i8** (%struct.jpeg_common_struct*, i32, i32, i32)* @alloc_sarray, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !20
  %28 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub16 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %28, i32 0, i32 0
  %alloc_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub16, i32 0, i32 3
  store [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)* @alloc_barray, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_barray, align 4, !tbaa !21
  %29 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub17 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %29, i32 0, i32 0
  %request_virt_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub17, i32 0, i32 4
  store %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)* @request_virt_sarray, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)** %request_virt_sarray, align 4, !tbaa !22
  %30 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub18 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %30, i32 0, i32 0
  %request_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub18, i32 0, i32 5
  store %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)* @request_virt_barray, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)** %request_virt_barray, align 4, !tbaa !23
  %31 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub19 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %31, i32 0, i32 0
  %realize_virt_arrays = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub19, i32 0, i32 6
  %realize_virt_arrays20 = bitcast {}** %realize_virt_arrays to void (%struct.jpeg_common_struct*)**
  store void (%struct.jpeg_common_struct*)* @realize_virt_arrays, void (%struct.jpeg_common_struct*)** %realize_virt_arrays20, align 4, !tbaa !24
  %32 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub21 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %32, i32 0, i32 0
  %access_virt_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub21, i32 0, i32 7
  store i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)* @access_virt_sarray, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)** %access_virt_sarray, align 4, !tbaa !25
  %33 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub22 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %33, i32 0, i32 0
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub22, i32 0, i32 8
  store [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)* @access_virt_barray, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !26
  %34 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub23 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %34, i32 0, i32 0
  %free_pool = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub23, i32 0, i32 9
  store void (%struct.jpeg_common_struct*, i32)* @free_pool, void (%struct.jpeg_common_struct*, i32)** %free_pool, align 4, !tbaa !27
  %35 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub24 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %35, i32 0, i32 0
  %self_destruct = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub24, i32 0, i32 10
  %self_destruct25 = bitcast {}** %self_destruct to void (%struct.jpeg_common_struct*)**
  store void (%struct.jpeg_common_struct*)* @self_destruct, void (%struct.jpeg_common_struct*)** %self_destruct25, align 4, !tbaa !28
  %36 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub26 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %36, i32 0, i32 0
  %max_alloc_chunk = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub26, i32 0, i32 12
  store i32 1000000000, i32* %max_alloc_chunk, align 4, !tbaa !29
  %37 = load i32, i32* %max_to_use, align 4, !tbaa !9
  %38 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub27 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %38, i32 0, i32 0
  %max_memory_to_use = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub27, i32 0, i32 11
  store i32 %37, i32* %max_memory_to_use, align 4, !tbaa !30
  store i32 1, i32* %pool, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end13
  %39 = load i32, i32* %pool, align 4, !tbaa !31
  %cmp28 = icmp sge i32 %39, 0
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %40 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %small_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %40, i32 0, i32 1
  %41 = load i32, i32* %pool, align 4, !tbaa !31
  %arrayidx29 = getelementptr inbounds [2 x %struct.small_pool_struct*], [2 x %struct.small_pool_struct*]* %small_list, i32 0, i32 %41
  store %struct.small_pool_struct* null, %struct.small_pool_struct** %arrayidx29, align 4, !tbaa !2
  %42 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %large_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %42, i32 0, i32 2
  %43 = load i32, i32* %pool, align 4, !tbaa !31
  %arrayidx30 = getelementptr inbounds [2 x %struct.large_pool_struct*], [2 x %struct.large_pool_struct*]* %large_list, i32 0, i32 %43
  store %struct.large_pool_struct* null, %struct.large_pool_struct** %arrayidx30, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %pool, align 4, !tbaa !31
  %dec = add nsw i32 %44, -1
  store i32 %dec, i32* %pool, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %45 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %45, i32 0, i32 3
  store %struct.jvirt_sarray_control* null, %struct.jvirt_sarray_control** %virt_sarray_list, align 4, !tbaa !32
  %46 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %46, i32 0, i32 4
  store %struct.jvirt_barray_control* null, %struct.jvirt_barray_control** %virt_barray_list, align 4, !tbaa !33
  %47 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %47, i32 0, i32 5
  store i32 84, i32* %total_space_allocated, align 4, !tbaa !34
  %48 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub31 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %48, i32 0, i32 0
  %49 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem32 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %49, i32 0, i32 1
  store %struct.jpeg_memory_mgr* %pub31, %struct.jpeg_memory_mgr** %mem32, align 4, !tbaa !6
  %50 = bitcast i8** %memenv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #3
  %call33 = call i8* @getenv(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0))
  store i8* %call33, i8** %memenv, align 4, !tbaa !2
  %cmp34 = icmp ne i8* %call33, null
  br i1 %cmp34, label %if.then35, label %if.end50

if.then35:                                        ; preds = %for.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ch) #3
  store i8 120, i8* %ch, align 1, !tbaa !15
  %51 = load i8*, i8** %memenv, align 4, !tbaa !2
  %call36 = call i32 (i8*, i8*, ...) @sscanf(i8* %51, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %max_to_use, i8* %ch)
  %cmp37 = icmp sgt i32 %call36, 0
  br i1 %cmp37, label %if.then38, label %if.end49

if.then38:                                        ; preds = %if.then35
  %52 = load i8, i8* %ch, align 1, !tbaa !15
  %conv = sext i8 %52 to i32
  %cmp39 = icmp eq i32 %conv, 109
  br i1 %cmp39, label %if.then44, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then38
  %53 = load i8, i8* %ch, align 1, !tbaa !15
  %conv41 = sext i8 %53 to i32
  %cmp42 = icmp eq i32 %conv41, 77
  br i1 %cmp42, label %if.then44, label %if.end45

if.then44:                                        ; preds = %lor.lhs.false, %if.then38
  %54 = load i32, i32* %max_to_use, align 4, !tbaa !9
  %mul = mul nsw i32 %54, 1000
  store i32 %mul, i32* %max_to_use, align 4, !tbaa !9
  br label %if.end45

if.end45:                                         ; preds = %if.then44, %lor.lhs.false
  %55 = load i32, i32* %max_to_use, align 4, !tbaa !9
  %mul46 = mul nsw i32 %55, 1000
  %56 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %pub47 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %56, i32 0, i32 0
  %max_memory_to_use48 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %pub47, i32 0, i32 11
  store i32 %mul46, i32* %max_memory_to_use48, align 4, !tbaa !30
  br label %if.end49

if.end49:                                         ; preds = %if.end45, %if.then35
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ch) #3
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %for.end
  %57 = bitcast i8** %memenv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %test_mac to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i32* %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast i32* %max_to_use to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  %61 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @jpeg_mem_init(%struct.jpeg_common_struct*) #2

declare i8* @jpeg_get_small(%struct.jpeg_common_struct*, i32) #2

declare void @jpeg_mem_term(%struct.jpeg_common_struct*) #2

; Function Attrs: nounwind
define internal i8* @alloc_small(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %sizeofobject.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %hdr_ptr = alloca %struct.small_pool_struct*, align 4
  %prev_hdr_ptr = alloca %struct.small_pool_struct*, align 4
  %data_ptr = alloca i8*, align 4
  %min_request = alloca i32, align 4
  %slop = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !9
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.small_pool_struct** %hdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.small_pool_struct** %prev_hdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %data_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %min_request to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %slop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %cmp = icmp ugt i32 %9, 1000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %10, i32 7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %call = call i32 @round_up_pow2(i32 %11, i32 8)
  store i32 %call, i32* %sizeofobject.addr, align 4, !tbaa !9
  %12 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %add = add i32 12, %12
  %add2 = add i32 %add, 8
  %sub = sub i32 %add2, 1
  %cmp3 = icmp ugt i32 %sub, 1000000000
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %13 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %13, i32 1)
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %14 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp6 = icmp slt i32 %14, 0
  br i1 %cmp6, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end5
  %15 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp7 = icmp sge i32 %15, 2
  br i1 %cmp7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %lor.lhs.false, %if.end5
  %16 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 14, i32* %msg_code, align 4, !tbaa !12
  %18 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %19 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %18, i32* %arrayidx, align 4, !tbaa !15
  %21 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %error_exit11 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !14
  %24 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %23(%struct.jpeg_common_struct* %24)
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %lor.lhs.false
  store %struct.small_pool_struct* null, %struct.small_pool_struct** %prev_hdr_ptr, align 4, !tbaa !2
  %25 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %small_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %25, i32 0, i32 1
  %26 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx13 = getelementptr inbounds [2 x %struct.small_pool_struct*], [2 x %struct.small_pool_struct*]* %small_list, i32 0, i32 %26
  %27 = load %struct.small_pool_struct*, %struct.small_pool_struct** %arrayidx13, align 4, !tbaa !2
  store %struct.small_pool_struct* %27, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end17, %if.end12
  %28 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %cmp14 = icmp ne %struct.small_pool_struct* %28, null
  br i1 %cmp14, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %29 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_left = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %29, i32 0, i32 2
  %30 = load i32, i32* %bytes_left, align 4, !tbaa !35
  %31 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %cmp15 = icmp uge i32 %30, %31
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %while.body
  br label %while.end

if.end17:                                         ; preds = %while.body
  %32 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  store %struct.small_pool_struct* %32, %struct.small_pool_struct** %prev_hdr_ptr, align 4, !tbaa !2
  %33 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %33, i32 0, i32 0
  %34 = load %struct.small_pool_struct*, %struct.small_pool_struct** %next, align 4, !tbaa !37
  store %struct.small_pool_struct* %34, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %if.then16, %while.cond
  %35 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %cmp18 = icmp eq %struct.small_pool_struct* %35, null
  br i1 %cmp18, label %if.then19, label %if.end53

if.then19:                                        ; preds = %while.end
  %36 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %add20 = add i32 12, %36
  %add21 = add i32 %add20, 8
  %sub22 = sub i32 %add21, 1
  store i32 %sub22, i32* %min_request, align 4, !tbaa !9
  %37 = load %struct.small_pool_struct*, %struct.small_pool_struct** %prev_hdr_ptr, align 4, !tbaa !2
  %cmp23 = icmp eq %struct.small_pool_struct* %37, null
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then19
  %38 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx25 = getelementptr inbounds [2 x i32], [2 x i32]* @first_pool_slop, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx25, align 4, !tbaa !9
  store i32 %39, i32* %slop, align 4, !tbaa !9
  br label %if.end27

if.else:                                          ; preds = %if.then19
  %40 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx26 = getelementptr inbounds [2 x i32], [2 x i32]* @extra_pool_slop, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx26, align 4, !tbaa !9
  store i32 %41, i32* %slop, align 4, !tbaa !9
  br label %if.end27

if.end27:                                         ; preds = %if.else, %if.then24
  %42 = load i32, i32* %slop, align 4, !tbaa !9
  %43 = load i32, i32* %min_request, align 4, !tbaa !9
  %sub28 = sub i32 1000000000, %43
  %cmp29 = icmp ugt i32 %42, %sub28
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %if.end27
  %44 = load i32, i32* %min_request, align 4, !tbaa !9
  %sub31 = sub i32 1000000000, %44
  store i32 %sub31, i32* %slop, align 4, !tbaa !9
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %if.end27
  br label %for.cond

for.cond:                                         ; preds = %if.end40, %if.end32
  %45 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %46 = load i32, i32* %min_request, align 4, !tbaa !9
  %47 = load i32, i32* %slop, align 4, !tbaa !9
  %add33 = add i32 %46, %47
  %call34 = call i8* @jpeg_get_small(%struct.jpeg_common_struct* %45, i32 %add33)
  %48 = bitcast i8* %call34 to %struct.small_pool_struct*
  store %struct.small_pool_struct* %48, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %49 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %cmp35 = icmp ne %struct.small_pool_struct* %49, null
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %for.cond
  br label %for.end

if.end37:                                         ; preds = %for.cond
  %50 = load i32, i32* %slop, align 4, !tbaa !9
  %div = udiv i32 %50, 2
  store i32 %div, i32* %slop, align 4, !tbaa !9
  %51 = load i32, i32* %slop, align 4, !tbaa !9
  %cmp38 = icmp ult i32 %51, 50
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.end37
  %52 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %52, i32 2)
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %if.end37
  br label %for.cond

for.end:                                          ; preds = %if.then36
  %53 = load i32, i32* %min_request, align 4, !tbaa !9
  %54 = load i32, i32* %slop, align 4, !tbaa !9
  %add41 = add i32 %53, %54
  %55 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %55, i32 0, i32 5
  %56 = load i32, i32* %total_space_allocated, align 4, !tbaa !34
  %add42 = add i32 %56, %add41
  store i32 %add42, i32* %total_space_allocated, align 4, !tbaa !34
  %57 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %next43 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %57, i32 0, i32 0
  store %struct.small_pool_struct* null, %struct.small_pool_struct** %next43, align 4, !tbaa !37
  %58 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_used = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %58, i32 0, i32 1
  store i32 0, i32* %bytes_used, align 4, !tbaa !38
  %59 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %60 = load i32, i32* %slop, align 4, !tbaa !9
  %add44 = add i32 %59, %60
  %61 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_left45 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %61, i32 0, i32 2
  store i32 %add44, i32* %bytes_left45, align 4, !tbaa !35
  %62 = load %struct.small_pool_struct*, %struct.small_pool_struct** %prev_hdr_ptr, align 4, !tbaa !2
  %cmp46 = icmp eq %struct.small_pool_struct* %62, null
  br i1 %cmp46, label %if.then47, label %if.else50

if.then47:                                        ; preds = %for.end
  %63 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %64 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %small_list48 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %64, i32 0, i32 1
  %65 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx49 = getelementptr inbounds [2 x %struct.small_pool_struct*], [2 x %struct.small_pool_struct*]* %small_list48, i32 0, i32 %65
  store %struct.small_pool_struct* %63, %struct.small_pool_struct** %arrayidx49, align 4, !tbaa !2
  br label %if.end52

if.else50:                                        ; preds = %for.end
  %66 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %67 = load %struct.small_pool_struct*, %struct.small_pool_struct** %prev_hdr_ptr, align 4, !tbaa !2
  %next51 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %67, i32 0, i32 0
  store %struct.small_pool_struct* %66, %struct.small_pool_struct** %next51, align 4, !tbaa !37
  br label %if.end52

if.end52:                                         ; preds = %if.else50, %if.then47
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %while.end
  %68 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %69 = bitcast %struct.small_pool_struct* %68 to i8*
  store i8* %69, i8** %data_ptr, align 4, !tbaa !2
  %70 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %70, i32 12
  store i8* %add.ptr, i8** %data_ptr, align 4, !tbaa !2
  %71 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %72 = ptrtoint i8* %71 to i32
  %rem = urem i32 %72, 8
  %tobool = icmp ne i32 %rem, 0
  br i1 %tobool, label %if.then54, label %if.end58

if.then54:                                        ; preds = %if.end53
  %73 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %74 = ptrtoint i8* %73 to i32
  %rem55 = urem i32 %74, 8
  %sub56 = sub i32 8, %rem55
  %75 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %75, i32 %sub56
  store i8* %add.ptr57, i8** %data_ptr, align 4, !tbaa !2
  br label %if.end58

if.end58:                                         ; preds = %if.then54, %if.end53
  %76 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_used59 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %76, i32 0, i32 1
  %77 = load i32, i32* %bytes_used59, align 4, !tbaa !38
  %78 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %add.ptr60 = getelementptr inbounds i8, i8* %78, i32 %77
  store i8* %add.ptr60, i8** %data_ptr, align 4, !tbaa !2
  %79 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %80 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_used61 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %80, i32 0, i32 1
  %81 = load i32, i32* %bytes_used61, align 4, !tbaa !38
  %add62 = add i32 %81, %79
  store i32 %add62, i32* %bytes_used61, align 4, !tbaa !38
  %82 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %83 = load %struct.small_pool_struct*, %struct.small_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_left63 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %83, i32 0, i32 2
  %84 = load i32, i32* %bytes_left63, align 4, !tbaa !35
  %sub64 = sub i32 %84, %82
  store i32 %sub64, i32* %bytes_left63, align 4, !tbaa !35
  %85 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %86 = bitcast i32* %slop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  %87 = bitcast i32* %min_request to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #3
  %88 = bitcast i8** %data_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #3
  %89 = bitcast %struct.small_pool_struct** %prev_hdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  %90 = bitcast %struct.small_pool_struct** %hdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  %91 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #3
  ret i8* %85
}

; Function Attrs: nounwind
define internal i8* @alloc_large(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %sizeofobject.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %hdr_ptr = alloca %struct.large_pool_struct*, align 4
  %data_ptr = alloca i8*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !9
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.large_pool_struct** %hdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %data_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %cmp = icmp ugt i32 %6, 1000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %7, i32 8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %call = call i32 @round_up_pow2(i32 %8, i32 8)
  store i32 %call, i32* %sizeofobject.addr, align 4, !tbaa !9
  %9 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %add = add i32 12, %9
  %add2 = add i32 %add, 8
  %sub = sub i32 %add2, 1
  %cmp3 = icmp ugt i32 %sub, 1000000000
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %10 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %10, i32 3)
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %11 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp6 = icmp slt i32 %11, 0
  br i1 %cmp6, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end5
  %12 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp7 = icmp sge i32 %12, 2
  br i1 %cmp7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %lor.lhs.false, %if.end5
  %13 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 5
  store i32 14, i32* %msg_code, align 4, !tbaa !12
  %15 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %16 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %15, i32* %arrayidx, align 4, !tbaa !15
  %18 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 0
  %error_exit11 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !14
  %21 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %20(%struct.jpeg_common_struct* %21)
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %lor.lhs.false
  %22 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %add13 = add i32 %23, 12
  %add14 = add i32 %add13, 8
  %sub15 = sub i32 %add14, 1
  %call16 = call i8* @jpeg_get_large(%struct.jpeg_common_struct* %22, i32 %sub15)
  %24 = bitcast i8* %call16 to %struct.large_pool_struct*
  store %struct.large_pool_struct* %24, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %25 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %cmp17 = icmp eq %struct.large_pool_struct* %25, null
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end12
  %26 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %26, i32 4)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end12
  %27 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %add20 = add i32 %27, 12
  %add21 = add i32 %add20, 8
  %sub22 = sub i32 %add21, 1
  %28 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %28, i32 0, i32 5
  %29 = load i32, i32* %total_space_allocated, align 4, !tbaa !34
  %add23 = add i32 %29, %sub22
  store i32 %add23, i32* %total_space_allocated, align 4, !tbaa !34
  %30 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %large_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %30, i32 0, i32 2
  %31 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx24 = getelementptr inbounds [2 x %struct.large_pool_struct*], [2 x %struct.large_pool_struct*]* %large_list, i32 0, i32 %31
  %32 = load %struct.large_pool_struct*, %struct.large_pool_struct** %arrayidx24, align 4, !tbaa !2
  %33 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %33, i32 0, i32 0
  store %struct.large_pool_struct* %32, %struct.large_pool_struct** %next, align 4, !tbaa !39
  %34 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !9
  %35 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_used = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %35, i32 0, i32 1
  store i32 %34, i32* %bytes_used, align 4, !tbaa !41
  %36 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %bytes_left = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %36, i32 0, i32 2
  store i32 0, i32* %bytes_left, align 4, !tbaa !42
  %37 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %38 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %large_list25 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %38, i32 0, i32 2
  %39 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx26 = getelementptr inbounds [2 x %struct.large_pool_struct*], [2 x %struct.large_pool_struct*]* %large_list25, i32 0, i32 %39
  store %struct.large_pool_struct* %37, %struct.large_pool_struct** %arrayidx26, align 4, !tbaa !2
  %40 = load %struct.large_pool_struct*, %struct.large_pool_struct** %hdr_ptr, align 4, !tbaa !2
  %41 = bitcast %struct.large_pool_struct* %40 to i8*
  store i8* %41, i8** %data_ptr, align 4, !tbaa !2
  %42 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 12
  store i8* %add.ptr, i8** %data_ptr, align 4, !tbaa !2
  %43 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %44 = ptrtoint i8* %43 to i32
  %rem = urem i32 %44, 8
  %tobool = icmp ne i32 %rem, 0
  br i1 %tobool, label %if.then27, label %if.end31

if.then27:                                        ; preds = %if.end19
  %45 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %46 = ptrtoint i8* %45 to i32
  %rem28 = urem i32 %46, 8
  %sub29 = sub i32 8, %rem28
  %47 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %add.ptr30 = getelementptr inbounds i8, i8* %47, i32 %sub29
  store i8* %add.ptr30, i8** %data_ptr, align 4, !tbaa !2
  br label %if.end31

if.end31:                                         ; preds = %if.then27, %if.end19
  %48 = load i8*, i8** %data_ptr, align 4, !tbaa !2
  %49 = bitcast i8** %data_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast %struct.large_pool_struct** %hdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret i8* %48
}

; Function Attrs: nounwind
define internal i8** @alloc_sarray(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %samplesperrow, i32 %numrows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %samplesperrow.addr = alloca i32, align 4
  %numrows.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %result = alloca i8**, align 4
  %workspace = alloca i8*, align 4
  %rowsperchunk = alloca i32, align 4
  %currow = alloca i32, align 4
  %i = alloca i32, align 4
  %ltemp = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %samplesperrow, i32* %samplesperrow.addr, align 4, !tbaa !31
  store i32 %numrows, i32* %numrows.addr, align 4, !tbaa !31
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast i8*** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %rowsperchunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %currow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %cmp = icmp ugt i32 %10, 1000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %11, i32 9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %call = call i32 @round_up_pow2(i32 %12, i32 16)
  store i32 %call, i32* %samplesperrow.addr, align 4, !tbaa !31
  %13 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %mul = mul i32 %13, 1
  %div = udiv i32 999999988, %mul
  store i32 %div, i32* %ltemp, align 4, !tbaa !9
  %14 = load i32, i32* %ltemp, align 4, !tbaa !9
  %cmp2 = icmp sle i32 %14, 0
  br i1 %cmp2, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.end
  %15 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 5
  store i32 70, i32* %msg_code, align 4, !tbaa !12
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 0
  %error_exit5 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %19 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit5, align 4, !tbaa !14
  %20 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %19(%struct.jpeg_common_struct* %20)
  br label %if.end6

if.end6:                                          ; preds = %if.then3, %if.end
  %21 = load i32, i32* %ltemp, align 4, !tbaa !9
  %22 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %cmp7 = icmp slt i32 %21, %22
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end6
  %23 = load i32, i32* %ltemp, align 4, !tbaa !9
  store i32 %23, i32* %rowsperchunk, align 4, !tbaa !31
  br label %if.end9

if.else:                                          ; preds = %if.end6
  %24 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  store i32 %24, i32* %rowsperchunk, align 4, !tbaa !31
  br label %if.end9

if.end9:                                          ; preds = %if.else, %if.then8
  %25 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %26 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %last_rowsperchunk = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %26, i32 0, i32 6
  store i32 %25, i32* %last_rowsperchunk, align 4, !tbaa !43
  %27 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %29 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %mul10 = mul i32 %29, 4
  %call11 = call i8* @alloc_small(%struct.jpeg_common_struct* %27, i32 %28, i32 %mul10)
  %30 = bitcast i8* %call11 to i8**
  store i8** %30, i8*** %result, align 4, !tbaa !2
  store i32 0, i32* %currow, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.end9
  %31 = load i32, i32* %currow, align 4, !tbaa !31
  %32 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %cmp12 = icmp ult i32 %31, %32
  br i1 %cmp12, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %34 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %35 = load i32, i32* %currow, align 4, !tbaa !31
  %sub = sub i32 %34, %35
  %cmp13 = icmp ult i32 %33, %sub
  br i1 %cmp13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %36 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %37 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %38 = load i32, i32* %currow, align 4, !tbaa !31
  %sub14 = sub i32 %37, %38
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %36, %cond.true ], [ %sub14, %cond.false ]
  store i32 %cond, i32* %rowsperchunk, align 4, !tbaa !31
  %39 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %41 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %42 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %mul15 = mul i32 %41, %42
  %mul16 = mul i32 %mul15, 1
  %call17 = call i8* @alloc_large(%struct.jpeg_common_struct* %39, i32 %40, i32 %mul16)
  store i8* %call17, i8** %workspace, align 4, !tbaa !2
  %43 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  store i32 %43, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %44 = load i32, i32* %i, align 4, !tbaa !31
  %cmp18 = icmp ugt i32 %44, 0
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %45 = load i8*, i8** %workspace, align 4, !tbaa !2
  %46 = load i8**, i8*** %result, align 4, !tbaa !2
  %47 = load i32, i32* %currow, align 4, !tbaa !31
  %inc = add i32 %47, 1
  store i32 %inc, i32* %currow, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds i8*, i8** %46, i32 %47
  store i8* %45, i8** %arrayidx, align 4, !tbaa !2
  %48 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %49 = load i8*, i8** %workspace, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %49, i32 %48
  store i8* %add.ptr, i8** %workspace, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %i, align 4, !tbaa !31
  %dec = add i32 %50, -1
  store i32 %dec, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %51 = load i8**, i8*** %result, align 4, !tbaa !2
  %52 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %currow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %rowsperchunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i8** %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i8*** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  ret i8** %51
}

; Function Attrs: nounwind
define internal [64 x i16]** @alloc_barray(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %blocksperrow, i32 %numrows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %blocksperrow.addr = alloca i32, align 4
  %numrows.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %result = alloca [64 x i16]**, align 4
  %workspace = alloca [64 x i16]*, align 4
  %rowsperchunk = alloca i32, align 4
  %currow = alloca i32, align 4
  %i = alloca i32, align 4
  %ltemp = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %blocksperrow, i32* %blocksperrow.addr, align 4, !tbaa !31
  store i32 %numrows, i32* %numrows.addr, align 4, !tbaa !31
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast [64 x i16]*** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast [64 x i16]** %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %rowsperchunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %currow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load i32, i32* %blocksperrow.addr, align 4, !tbaa !31
  %mul = mul i32 %10, 128
  %div = udiv i32 999999988, %mul
  store i32 %div, i32* %ltemp, align 4, !tbaa !9
  %11 = load i32, i32* %ltemp, align 4, !tbaa !9
  %cmp = icmp sle i32 %11, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 70, i32* %msg_code, align 4, !tbaa !12
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %error_exit3 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit3, align 4, !tbaa !14
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %16(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = load i32, i32* %ltemp, align 4, !tbaa !9
  %19 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %20 = load i32, i32* %ltemp, align 4, !tbaa !9
  store i32 %20, i32* %rowsperchunk, align 4, !tbaa !31
  br label %if.end6

if.else:                                          ; preds = %if.end
  %21 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  store i32 %21, i32* %rowsperchunk, align 4, !tbaa !31
  br label %if.end6

if.end6:                                          ; preds = %if.else, %if.then5
  %22 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %23 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %last_rowsperchunk = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %23, i32 0, i32 6
  store i32 %22, i32* %last_rowsperchunk, align 4, !tbaa !43
  %24 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %26 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %mul7 = mul i32 %26, 4
  %call = call i8* @alloc_small(%struct.jpeg_common_struct* %24, i32 %25, i32 %mul7)
  %27 = bitcast i8* %call to [64 x i16]**
  store [64 x i16]** %27, [64 x i16]*** %result, align 4, !tbaa !2
  store i32 0, i32* %currow, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.end6
  %28 = load i32, i32* %currow, align 4, !tbaa !31
  %29 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %cmp8 = icmp ult i32 %28, %29
  br i1 %cmp8, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %30 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %31 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %32 = load i32, i32* %currow, align 4, !tbaa !31
  %sub = sub i32 %31, %32
  %cmp9 = icmp ult i32 %30, %sub
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %33 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %34 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %35 = load i32, i32* %currow, align 4, !tbaa !31
  %sub10 = sub i32 %34, %35
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %33, %cond.true ], [ %sub10, %cond.false ]
  store i32 %cond, i32* %rowsperchunk, align 4, !tbaa !31
  %36 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %38 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  %39 = load i32, i32* %blocksperrow.addr, align 4, !tbaa !31
  %mul11 = mul i32 %38, %39
  %mul12 = mul i32 %mul11, 128
  %call13 = call i8* @alloc_large(%struct.jpeg_common_struct* %36, i32 %37, i32 %mul12)
  %40 = bitcast i8* %call13 to [64 x i16]*
  store [64 x i16]* %40, [64 x i16]** %workspace, align 4, !tbaa !2
  %41 = load i32, i32* %rowsperchunk, align 4, !tbaa !31
  store i32 %41, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %42 = load i32, i32* %i, align 4, !tbaa !31
  %cmp14 = icmp ugt i32 %42, 0
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %43 = load [64 x i16]*, [64 x i16]** %workspace, align 4, !tbaa !2
  %44 = load [64 x i16]**, [64 x i16]*** %result, align 4, !tbaa !2
  %45 = load i32, i32* %currow, align 4, !tbaa !31
  %inc = add i32 %45, 1
  store i32 %inc, i32* %currow, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %44, i32 %45
  store [64 x i16]* %43, [64 x i16]** %arrayidx, align 4, !tbaa !2
  %46 = load i32, i32* %blocksperrow.addr, align 4, !tbaa !31
  %47 = load [64 x i16]*, [64 x i16]** %workspace, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %47, i32 %46
  store [64 x i16]* %add.ptr, [64 x i16]** %workspace, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %i, align 4, !tbaa !31
  %dec = add i32 %48, -1
  store i32 %dec, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %49 = load [64 x i16]**, [64 x i16]*** %result, align 4, !tbaa !2
  %50 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %currow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %rowsperchunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast [64 x i16]** %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast [64 x i16]*** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  ret [64 x i16]** %49
}

; Function Attrs: nounwind
define internal %struct.jvirt_sarray_control* @request_virt_sarray(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %pre_zero, i32 %samplesperrow, i32 %numrows, i32 %maxaccess) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %pre_zero.addr = alloca i32, align 4
  %samplesperrow.addr = alloca i32, align 4
  %numrows.addr = alloca i32, align 4
  %maxaccess.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %result = alloca %struct.jvirt_sarray_control*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %pre_zero, i32* %pre_zero.addr, align 4, !tbaa !31
  store i32 %samplesperrow, i32* %samplesperrow.addr, align 4, !tbaa !31
  store i32 %numrows, i32* %numrows.addr, align 4, !tbaa !31
  store i32 %maxaccess, i32* %maxaccess.addr, align 4, !tbaa !31
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.jvirt_sarray_control** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp = icmp ne i32 %5, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 5
  store i32 14, i32* %msg_code, align 4, !tbaa !12
  %8 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %9 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %8, i32* %arrayidx, align 4, !tbaa !15
  %11 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %error_exit4 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit4, align 4, !tbaa !14
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %13(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %call = call i8* @alloc_small(%struct.jpeg_common_struct* %15, i32 %16, i32 128)
  %17 = bitcast i8* %call to %struct.jvirt_sarray_control*
  store %struct.jvirt_sarray_control* %17, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %18 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %18, i32 0, i32 0
  store i8** null, i8*** %mem_buffer, align 4, !tbaa !44
  %19 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %20 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %20, i32 0, i32 1
  store i32 %19, i32* %rows_in_array, align 4, !tbaa !47
  %21 = load i32, i32* %samplesperrow.addr, align 4, !tbaa !31
  %22 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %samplesperrow5 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %22, i32 0, i32 2
  store i32 %21, i32* %samplesperrow5, align 4, !tbaa !48
  %23 = load i32, i32* %maxaccess.addr, align 4, !tbaa !31
  %24 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %maxaccess6 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %24, i32 0, i32 3
  store i32 %23, i32* %maxaccess6, align 4, !tbaa !49
  %25 = load i32, i32* %pre_zero.addr, align 4, !tbaa !31
  %26 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %pre_zero7 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %26, i32 0, i32 8
  store i32 %25, i32* %pre_zero7, align 4, !tbaa !50
  %27 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %27, i32 0, i32 10
  store i32 0, i32* %b_s_open, align 4, !tbaa !51
  %28 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %28, i32 0, i32 3
  %29 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %virt_sarray_list, align 4, !tbaa !32
  %30 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %30, i32 0, i32 11
  store %struct.jvirt_sarray_control* %29, %struct.jvirt_sarray_control** %next, align 4, !tbaa !52
  %31 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %32 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list8 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %32, i32 0, i32 3
  store %struct.jvirt_sarray_control* %31, %struct.jvirt_sarray_control** %virt_sarray_list8, align 4, !tbaa !32
  %33 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %result, align 4, !tbaa !2
  %34 = bitcast %struct.jvirt_sarray_control** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  ret %struct.jvirt_sarray_control* %33
}

; Function Attrs: nounwind
define internal %struct.jvirt_barray_control* @request_virt_barray(%struct.jpeg_common_struct* %cinfo, i32 %pool_id, i32 %pre_zero, i32 %blocksperrow, i32 %numrows, i32 %maxaccess) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %pre_zero.addr = alloca i32, align 4
  %blocksperrow.addr = alloca i32, align 4
  %numrows.addr = alloca i32, align 4
  %maxaccess.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %result = alloca %struct.jvirt_barray_control*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  store i32 %pre_zero, i32* %pre_zero.addr, align 4, !tbaa !31
  store i32 %blocksperrow, i32* %blocksperrow.addr, align 4, !tbaa !31
  store i32 %numrows, i32* %numrows.addr, align 4, !tbaa !31
  store i32 %maxaccess, i32* %maxaccess.addr, align 4, !tbaa !31
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.jvirt_barray_control** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp = icmp ne i32 %5, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 5
  store i32 14, i32* %msg_code, align 4, !tbaa !12
  %8 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %9 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %8, i32* %arrayidx, align 4, !tbaa !15
  %11 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %error_exit4 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit4, align 4, !tbaa !14
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %13(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %call = call i8* @alloc_small(%struct.jpeg_common_struct* %15, i32 %16, i32 128)
  %17 = bitcast i8* %call to %struct.jvirt_barray_control*
  store %struct.jvirt_barray_control* %17, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %18 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %18, i32 0, i32 0
  store [64 x i16]** null, [64 x i16]*** %mem_buffer, align 4, !tbaa !53
  %19 = load i32, i32* %numrows.addr, align 4, !tbaa !31
  %20 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %20, i32 0, i32 1
  store i32 %19, i32* %rows_in_array, align 4, !tbaa !55
  %21 = load i32, i32* %blocksperrow.addr, align 4, !tbaa !31
  %22 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %blocksperrow5 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %22, i32 0, i32 2
  store i32 %21, i32* %blocksperrow5, align 4, !tbaa !56
  %23 = load i32, i32* %maxaccess.addr, align 4, !tbaa !31
  %24 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %maxaccess6 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %24, i32 0, i32 3
  store i32 %23, i32* %maxaccess6, align 4, !tbaa !57
  %25 = load i32, i32* %pre_zero.addr, align 4, !tbaa !31
  %26 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %pre_zero7 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %26, i32 0, i32 8
  store i32 %25, i32* %pre_zero7, align 4, !tbaa !58
  %27 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %27, i32 0, i32 10
  store i32 0, i32* %b_s_open, align 4, !tbaa !59
  %28 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %28, i32 0, i32 4
  %29 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %virt_barray_list, align 4, !tbaa !33
  %30 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %30, i32 0, i32 11
  store %struct.jvirt_barray_control* %29, %struct.jvirt_barray_control** %next, align 4, !tbaa !60
  %31 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %32 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list8 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %32, i32 0, i32 4
  store %struct.jvirt_barray_control* %31, %struct.jvirt_barray_control** %virt_barray_list8, align 4, !tbaa !33
  %33 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %result, align 4, !tbaa !2
  %34 = bitcast %struct.jvirt_barray_control** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  ret %struct.jvirt_barray_control* %33
}

; Function Attrs: nounwind
define internal void @realize_virt_arrays(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %space_per_minheight = alloca i32, align 4
  %maximum_space = alloca i32, align 4
  %avail_mem = alloca i32, align 4
  %minheights = alloca i32, align 4
  %max_minheights = alloca i32, align 4
  %sptr = alloca %struct.jvirt_sarray_control*, align 4
  %bptr = alloca %struct.jvirt_barray_control*, align 4
  %new_space = alloca i32, align 4
  %new_space17 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast i32* %space_per_minheight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %maximum_space to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %avail_mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %minheights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %max_minheights to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast %struct.jvirt_sarray_control** %sptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast %struct.jvirt_barray_control** %bptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 0, i32* %space_per_minheight, align 4, !tbaa !9
  store i32 0, i32* %maximum_space, align 4, !tbaa !9
  %11 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %11, i32 0, i32 3
  %12 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %virt_sarray_list, align 4, !tbaa !32
  store %struct.jvirt_sarray_control* %12, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %cmp = icmp ne %struct.jvirt_sarray_control* %13, null
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %14, i32 0, i32 0
  %15 = load i8**, i8*** %mem_buffer, align 4, !tbaa !44
  %cmp2 = icmp eq i8** %15, null
  br i1 %cmp2, label %if.then, label %if.end10

if.then:                                          ; preds = %for.body
  %16 = bitcast i32* %new_space to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %17, i32 0, i32 1
  %18 = load i32, i32* %rows_in_array, align 4, !tbaa !47
  %19 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %samplesperrow = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %19, i32 0, i32 2
  %20 = load i32, i32* %samplesperrow, align 4, !tbaa !48
  %mul = mul nsw i32 %18, %20
  %mul3 = mul i32 %mul, 1
  store i32 %mul3, i32* %new_space, align 4, !tbaa !9
  %21 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %maxaccess = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %21, i32 0, i32 3
  %22 = load i32, i32* %maxaccess, align 4, !tbaa !49
  %23 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %samplesperrow4 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %23, i32 0, i32 2
  %24 = load i32, i32* %samplesperrow4, align 4, !tbaa !48
  %mul5 = mul nsw i32 %22, %24
  %mul6 = mul i32 %mul5, 1
  %25 = load i32, i32* %space_per_minheight, align 4, !tbaa !9
  %add = add i32 %25, %mul6
  store i32 %add, i32* %space_per_minheight, align 4, !tbaa !9
  %26 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %sub = sub i32 -1, %26
  %27 = load i32, i32* %new_space, align 4, !tbaa !9
  %cmp7 = icmp ult i32 %sub, %27
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  %28 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %28, i32 10)
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  %29 = load i32, i32* %new_space, align 4, !tbaa !9
  %30 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %add9 = add i32 %30, %29
  store i32 %add9, i32* %maximum_space, align 4, !tbaa !9
  %31 = bitcast i32* %new_space to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  br label %if.end10

if.end10:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %32 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %32, i32 0, i32 11
  %33 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %next, align 4, !tbaa !52
  store %struct.jvirt_sarray_control* %33, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %34, i32 0, i32 4
  %35 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %virt_barray_list, align 4, !tbaa !33
  store %struct.jvirt_barray_control* %35, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc32, %for.end
  %36 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %cmp12 = icmp ne %struct.jvirt_barray_control* %36, null
  br i1 %cmp12, label %for.body13, label %for.end34

for.body13:                                       ; preds = %for.cond11
  %37 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %mem_buffer14 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %37, i32 0, i32 0
  %38 = load [64 x i16]**, [64 x i16]*** %mem_buffer14, align 4, !tbaa !53
  %cmp15 = icmp eq [64 x i16]** %38, null
  br i1 %cmp15, label %if.then16, label %if.end31

if.then16:                                        ; preds = %for.body13
  %39 = bitcast i32* %new_space17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #3
  %40 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_array18 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %40, i32 0, i32 1
  %41 = load i32, i32* %rows_in_array18, align 4, !tbaa !55
  %42 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %blocksperrow = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %42, i32 0, i32 2
  %43 = load i32, i32* %blocksperrow, align 4, !tbaa !56
  %mul19 = mul nsw i32 %41, %43
  %mul20 = mul i32 %mul19, 128
  store i32 %mul20, i32* %new_space17, align 4, !tbaa !9
  %44 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %maxaccess21 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %44, i32 0, i32 3
  %45 = load i32, i32* %maxaccess21, align 4, !tbaa !57
  %46 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %blocksperrow22 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %46, i32 0, i32 2
  %47 = load i32, i32* %blocksperrow22, align 4, !tbaa !56
  %mul23 = mul nsw i32 %45, %47
  %mul24 = mul i32 %mul23, 128
  %48 = load i32, i32* %space_per_minheight, align 4, !tbaa !9
  %add25 = add i32 %48, %mul24
  store i32 %add25, i32* %space_per_minheight, align 4, !tbaa !9
  %49 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %sub26 = sub i32 -1, %49
  %50 = load i32, i32* %new_space17, align 4, !tbaa !9
  %cmp27 = icmp ult i32 %sub26, %50
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then16
  %51 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @out_of_memory(%struct.jpeg_common_struct* %51, i32 11)
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.then16
  %52 = load i32, i32* %new_space17, align 4, !tbaa !9
  %53 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %add30 = add i32 %53, %52
  store i32 %add30, i32* %maximum_space, align 4, !tbaa !9
  %54 = bitcast i32* %new_space17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  br label %if.end31

if.end31:                                         ; preds = %if.end29, %for.body13
  br label %for.inc32

for.inc32:                                        ; preds = %if.end31
  %55 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %next33 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %55, i32 0, i32 11
  %56 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %next33, align 4, !tbaa !60
  store %struct.jvirt_barray_control* %56, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond11

for.end34:                                        ; preds = %for.cond11
  %57 = load i32, i32* %space_per_minheight, align 4, !tbaa !9
  %cmp35 = icmp ule i32 %57, 0
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %for.end34
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end37:                                         ; preds = %for.end34
  %58 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %59 = load i32, i32* %space_per_minheight, align 4, !tbaa !9
  %60 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %61 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %61, i32 0, i32 5
  %62 = load i32, i32* %total_space_allocated, align 4, !tbaa !34
  %call = call i32 @jpeg_mem_available(%struct.jpeg_common_struct* %58, i32 %59, i32 %60, i32 %62)
  store i32 %call, i32* %avail_mem, align 4, !tbaa !9
  %63 = load i32, i32* %avail_mem, align 4, !tbaa !9
  %64 = load i32, i32* %maximum_space, align 4, !tbaa !9
  %cmp38 = icmp uge i32 %63, %64
  br i1 %cmp38, label %if.then39, label %if.else

if.then39:                                        ; preds = %if.end37
  store i32 1000000000, i32* %max_minheights, align 4, !tbaa !9
  br label %if.end43

if.else:                                          ; preds = %if.end37
  %65 = load i32, i32* %avail_mem, align 4, !tbaa !9
  %66 = load i32, i32* %space_per_minheight, align 4, !tbaa !9
  %div = udiv i32 %65, %66
  store i32 %div, i32* %max_minheights, align 4, !tbaa !9
  %67 = load i32, i32* %max_minheights, align 4, !tbaa !9
  %cmp40 = icmp ule i32 %67, 0
  br i1 %cmp40, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.else
  store i32 1, i32* %max_minheights, align 4, !tbaa !9
  br label %if.end42

if.end42:                                         ; preds = %if.then41, %if.else
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.then39
  %68 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list44 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %68, i32 0, i32 3
  %69 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %virt_sarray_list44, align 4, !tbaa !32
  store %struct.jvirt_sarray_control* %69, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc73, %if.end43
  %70 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %cmp46 = icmp ne %struct.jvirt_sarray_control* %70, null
  br i1 %cmp46, label %for.body47, label %for.end75

for.body47:                                       ; preds = %for.cond45
  %71 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %mem_buffer48 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %71, i32 0, i32 0
  %72 = load i8**, i8*** %mem_buffer48, align 4, !tbaa !44
  %cmp49 = icmp eq i8** %72, null
  br i1 %cmp49, label %if.then50, label %if.end72

if.then50:                                        ; preds = %for.body47
  %73 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_array51 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %73, i32 0, i32 1
  %74 = load i32, i32* %rows_in_array51, align 4, !tbaa !47
  %sub52 = sub nsw i32 %74, 1
  %75 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %maxaccess53 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %75, i32 0, i32 3
  %76 = load i32, i32* %maxaccess53, align 4, !tbaa !49
  %div54 = udiv i32 %sub52, %76
  %add55 = add i32 %div54, 1
  store i32 %add55, i32* %minheights, align 4, !tbaa !9
  %77 = load i32, i32* %minheights, align 4, !tbaa !9
  %78 = load i32, i32* %max_minheights, align 4, !tbaa !9
  %cmp56 = icmp ule i32 %77, %78
  br i1 %cmp56, label %if.then57, label %if.else59

if.then57:                                        ; preds = %if.then50
  %79 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_array58 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %79, i32 0, i32 1
  %80 = load i32, i32* %rows_in_array58, align 4, !tbaa !47
  %81 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_mem = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %81, i32 0, i32 4
  store i32 %80, i32* %rows_in_mem, align 4, !tbaa !61
  br label %if.end67

if.else59:                                        ; preds = %if.then50
  %82 = load i32, i32* %max_minheights, align 4, !tbaa !9
  %83 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %maxaccess60 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %83, i32 0, i32 3
  %84 = load i32, i32* %maxaccess60, align 4, !tbaa !49
  %mul61 = mul i32 %82, %84
  %85 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_mem62 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %85, i32 0, i32 4
  store i32 %mul61, i32* %rows_in_mem62, align 4, !tbaa !61
  %86 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %87 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_info = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %87, i32 0, i32 12
  %88 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_array63 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %88, i32 0, i32 1
  %89 = load i32, i32* %rows_in_array63, align 4, !tbaa !47
  %90 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %samplesperrow64 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %90, i32 0, i32 2
  %91 = load i32, i32* %samplesperrow64, align 4, !tbaa !48
  %mul65 = mul nsw i32 %89, %91
  %mul66 = mul nsw i32 %mul65, 1
  call void @jpeg_open_backing_store(%struct.jpeg_common_struct* %86, %struct.backing_store_struct* %b_s_info, i32 %mul66)
  %92 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %92, i32 0, i32 10
  store i32 1, i32* %b_s_open, align 4, !tbaa !51
  br label %if.end67

if.end67:                                         ; preds = %if.else59, %if.then57
  %93 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %94 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %samplesperrow68 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %94, i32 0, i32 2
  %95 = load i32, i32* %samplesperrow68, align 4, !tbaa !48
  %96 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rows_in_mem69 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %96, i32 0, i32 4
  %97 = load i32, i32* %rows_in_mem69, align 4, !tbaa !61
  %call70 = call i8** @alloc_sarray(%struct.jpeg_common_struct* %93, i32 1, i32 %95, i32 %97)
  %98 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %mem_buffer71 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %98, i32 0, i32 0
  store i8** %call70, i8*** %mem_buffer71, align 4, !tbaa !44
  %99 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %last_rowsperchunk = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %99, i32 0, i32 6
  %100 = load i32, i32* %last_rowsperchunk, align 4, !tbaa !43
  %101 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %rowsperchunk = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %101, i32 0, i32 5
  store i32 %100, i32* %rowsperchunk, align 4, !tbaa !62
  %102 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %cur_start_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %102, i32 0, i32 6
  store i32 0, i32* %cur_start_row, align 4, !tbaa !63
  %103 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %first_undef_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %103, i32 0, i32 7
  store i32 0, i32* %first_undef_row, align 4, !tbaa !64
  %104 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %dirty = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %104, i32 0, i32 9
  store i32 0, i32* %dirty, align 4, !tbaa !65
  br label %if.end72

if.end72:                                         ; preds = %if.end67, %for.body47
  br label %for.inc73

for.inc73:                                        ; preds = %if.end72
  %105 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %next74 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %105, i32 0, i32 11
  %106 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %next74, align 4, !tbaa !52
  store %struct.jvirt_sarray_control* %106, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond45

for.end75:                                        ; preds = %for.cond45
  %107 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list76 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %107, i32 0, i32 4
  %108 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %virt_barray_list76, align 4, !tbaa !33
  store %struct.jvirt_barray_control* %108, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc113, %for.end75
  %109 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %cmp78 = icmp ne %struct.jvirt_barray_control* %109, null
  br i1 %cmp78, label %for.body79, label %for.end115

for.body79:                                       ; preds = %for.cond77
  %110 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %mem_buffer80 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %110, i32 0, i32 0
  %111 = load [64 x i16]**, [64 x i16]*** %mem_buffer80, align 4, !tbaa !53
  %cmp81 = icmp eq [64 x i16]** %111, null
  br i1 %cmp81, label %if.then82, label %if.end112

if.then82:                                        ; preds = %for.body79
  %112 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_array83 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %112, i32 0, i32 1
  %113 = load i32, i32* %rows_in_array83, align 4, !tbaa !55
  %sub84 = sub nsw i32 %113, 1
  %114 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %maxaccess85 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %114, i32 0, i32 3
  %115 = load i32, i32* %maxaccess85, align 4, !tbaa !57
  %div86 = udiv i32 %sub84, %115
  %add87 = add i32 %div86, 1
  store i32 %add87, i32* %minheights, align 4, !tbaa !9
  %116 = load i32, i32* %minheights, align 4, !tbaa !9
  %117 = load i32, i32* %max_minheights, align 4, !tbaa !9
  %cmp88 = icmp ule i32 %116, %117
  br i1 %cmp88, label %if.then89, label %if.else92

if.then89:                                        ; preds = %if.then82
  %118 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_array90 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %118, i32 0, i32 1
  %119 = load i32, i32* %rows_in_array90, align 4, !tbaa !55
  %120 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_mem91 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %120, i32 0, i32 4
  store i32 %119, i32* %rows_in_mem91, align 4, !tbaa !66
  br label %if.end102

if.else92:                                        ; preds = %if.then82
  %121 = load i32, i32* %max_minheights, align 4, !tbaa !9
  %122 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %maxaccess93 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %122, i32 0, i32 3
  %123 = load i32, i32* %maxaccess93, align 4, !tbaa !57
  %mul94 = mul i32 %121, %123
  %124 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_mem95 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %124, i32 0, i32 4
  store i32 %mul94, i32* %rows_in_mem95, align 4, !tbaa !66
  %125 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %126 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_info96 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %126, i32 0, i32 12
  %127 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_array97 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %127, i32 0, i32 1
  %128 = load i32, i32* %rows_in_array97, align 4, !tbaa !55
  %129 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %blocksperrow98 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %129, i32 0, i32 2
  %130 = load i32, i32* %blocksperrow98, align 4, !tbaa !56
  %mul99 = mul nsw i32 %128, %130
  %mul100 = mul nsw i32 %mul99, 128
  call void @jpeg_open_backing_store(%struct.jpeg_common_struct* %125, %struct.backing_store_struct* %b_s_info96, i32 %mul100)
  %131 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_open101 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %131, i32 0, i32 10
  store i32 1, i32* %b_s_open101, align 4, !tbaa !59
  br label %if.end102

if.end102:                                        ; preds = %if.else92, %if.then89
  %132 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %133 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %blocksperrow103 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %133, i32 0, i32 2
  %134 = load i32, i32* %blocksperrow103, align 4, !tbaa !56
  %135 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rows_in_mem104 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %135, i32 0, i32 4
  %136 = load i32, i32* %rows_in_mem104, align 4, !tbaa !66
  %call105 = call [64 x i16]** @alloc_barray(%struct.jpeg_common_struct* %132, i32 1, i32 %134, i32 %136)
  %137 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %mem_buffer106 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %137, i32 0, i32 0
  store [64 x i16]** %call105, [64 x i16]*** %mem_buffer106, align 4, !tbaa !53
  %138 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %last_rowsperchunk107 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %138, i32 0, i32 6
  %139 = load i32, i32* %last_rowsperchunk107, align 4, !tbaa !43
  %140 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %rowsperchunk108 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %140, i32 0, i32 5
  store i32 %139, i32* %rowsperchunk108, align 4, !tbaa !67
  %141 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %cur_start_row109 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %141, i32 0, i32 6
  store i32 0, i32* %cur_start_row109, align 4, !tbaa !68
  %142 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %first_undef_row110 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %142, i32 0, i32 7
  store i32 0, i32* %first_undef_row110, align 4, !tbaa !69
  %143 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %dirty111 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %143, i32 0, i32 9
  store i32 0, i32* %dirty111, align 4, !tbaa !70
  br label %if.end112

if.end112:                                        ; preds = %if.end102, %for.body79
  br label %for.inc113

for.inc113:                                       ; preds = %if.end112
  %144 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %next114 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %144, i32 0, i32 11
  %145 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %next114, align 4, !tbaa !60
  store %struct.jvirt_barray_control* %145, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond77

for.end115:                                       ; preds = %for.cond77
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end115, %if.then36
  %146 = bitcast %struct.jvirt_barray_control** %bptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  %147 = bitcast %struct.jvirt_sarray_control** %sptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %148 = bitcast i32* %max_minheights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  %149 = bitcast i32* %minheights to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast i32* %avail_mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  %151 = bitcast i32* %maximum_space to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  %152 = bitcast i32* %space_per_minheight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  %153 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i8** @access_virt_sarray(%struct.jpeg_common_struct* %cinfo, %struct.jvirt_sarray_control* %ptr, i32 %start_row, i32 %num_rows, i32 %writable) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %ptr.addr = alloca %struct.jvirt_sarray_control*, align 4
  %start_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %writable.addr = alloca i32, align 4
  %end_row = alloca i32, align 4
  %undef_row = alloca i32, align 4
  %ltemp = alloca i32, align 4
  %bytesperrow = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_sarray_control* %ptr, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  store i32 %start_row, i32* %start_row.addr, align 4, !tbaa !31
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !31
  store i32 %writable, i32* %writable.addr, align 4, !tbaa !31
  %0 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %2 = load i32, i32* %num_rows.addr, align 4, !tbaa !31
  %add = add i32 %1, %2
  store i32 %add, i32* %end_row, align 4, !tbaa !31
  %3 = bitcast i32* %undef_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %end_row, align 4, !tbaa !31
  %5 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %5, i32 0, i32 1
  %6 = load i32, i32* %rows_in_array, align 4, !tbaa !47
  %cmp = icmp ugt i32 %4, %6
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %7 = load i32, i32* %num_rows.addr, align 4, !tbaa !31
  %8 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %maxaccess = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %8, i32 0, i32 3
  %9 = load i32, i32* %maxaccess, align 4, !tbaa !49
  %cmp1 = icmp ugt i32 %7, %9
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %10 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %10, i32 0, i32 0
  %11 = load i8**, i8*** %mem_buffer, align 4, !tbaa !44
  %cmp3 = icmp eq i8** %11, null
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 22, i32* %msg_code, align 4, !tbaa !12
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %error_exit5 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit5, align 4, !tbaa !14
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %16(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false2
  %18 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %19 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %19, i32 0, i32 6
  %20 = load i32, i32* %cur_start_row, align 4, !tbaa !63
  %cmp6 = icmp ult i32 %18, %20
  br i1 %cmp6, label %if.then11, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end
  %21 = load i32, i32* %end_row, align 4, !tbaa !31
  %22 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row8 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %22, i32 0, i32 6
  %23 = load i32, i32* %cur_start_row8, align 4, !tbaa !63
  %24 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %24, i32 0, i32 4
  %25 = load i32, i32* %rows_in_mem, align 4, !tbaa !61
  %add9 = add i32 %23, %25
  %cmp10 = icmp ugt i32 %21, %add9
  br i1 %cmp10, label %if.then11, label %if.end33

if.then11:                                        ; preds = %lor.lhs.false7, %if.end
  %26 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %26, i32 0, i32 10
  %27 = load i32, i32* %b_s_open, align 4, !tbaa !51
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.end18, label %if.then12

if.then12:                                        ; preds = %if.then11
  %28 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 4, !tbaa !11
  %msg_code14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 5
  store i32 69, i32* %msg_code14, align 4, !tbaa !12
  %30 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 4, !tbaa !11
  %error_exit16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 0
  %error_exit17 = bitcast {}** %error_exit16 to void (%struct.jpeg_common_struct*)**
  %32 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit17, align 4, !tbaa !14
  %33 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %32(%struct.jpeg_common_struct* %33)
  br label %if.end18

if.end18:                                         ; preds = %if.then12, %if.then11
  %34 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %dirty = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %34, i32 0, i32 9
  %35 = load i32, i32* %dirty, align 4, !tbaa !65
  %tobool19 = icmp ne i32 %35, 0
  br i1 %tobool19, label %if.then20, label %if.end22

if.then20:                                        ; preds = %if.end18
  %36 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  call void @do_sarray_io(%struct.jpeg_common_struct* %36, %struct.jvirt_sarray_control* %37, i32 1)
  %38 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %dirty21 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %38, i32 0, i32 9
  store i32 0, i32* %dirty21, align 4, !tbaa !65
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end18
  %39 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %40 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row23 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %40, i32 0, i32 6
  %41 = load i32, i32* %cur_start_row23, align 4, !tbaa !63
  %cmp24 = icmp ugt i32 %39, %41
  br i1 %cmp24, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end22
  %42 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %43 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row26 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %43, i32 0, i32 6
  store i32 %42, i32* %cur_start_row26, align 4, !tbaa !63
  br label %if.end32

if.else:                                          ; preds = %if.end22
  %44 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load i32, i32* %end_row, align 4, !tbaa !31
  %46 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem27 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %46, i32 0, i32 4
  %47 = load i32, i32* %rows_in_mem27, align 4, !tbaa !61
  %sub = sub nsw i32 %45, %47
  store i32 %sub, i32* %ltemp, align 4, !tbaa !9
  %48 = load i32, i32* %ltemp, align 4, !tbaa !9
  %cmp28 = icmp slt i32 %48, 0
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.else
  store i32 0, i32* %ltemp, align 4, !tbaa !9
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.else
  %49 = load i32, i32* %ltemp, align 4, !tbaa !9
  %50 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row31 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %50, i32 0, i32 6
  store i32 %49, i32* %cur_start_row31, align 4, !tbaa !63
  %51 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  br label %if.end32

if.end32:                                         ; preds = %if.end30, %if.then25
  %52 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %53 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  call void @do_sarray_io(%struct.jpeg_common_struct* %52, %struct.jvirt_sarray_control* %53, i32 0)
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %lor.lhs.false7
  %54 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %54, i32 0, i32 7
  %55 = load i32, i32* %first_undef_row, align 4, !tbaa !64
  %56 = load i32, i32* %end_row, align 4, !tbaa !31
  %cmp34 = icmp ult i32 %55, %56
  br i1 %cmp34, label %if.then35, label %if.end72

if.then35:                                        ; preds = %if.end33
  %57 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row36 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %57, i32 0, i32 7
  %58 = load i32, i32* %first_undef_row36, align 4, !tbaa !64
  %59 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %cmp37 = icmp ult i32 %58, %59
  br i1 %cmp37, label %if.then38, label %if.else47

if.then38:                                        ; preds = %if.then35
  %60 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool39 = icmp ne i32 %60, 0
  br i1 %tobool39, label %if.then40, label %if.end46

if.then40:                                        ; preds = %if.then38
  %61 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 4, !tbaa !11
  %msg_code42 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 5
  store i32 22, i32* %msg_code42, align 4, !tbaa !12
  %63 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 4, !tbaa !11
  %error_exit44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 0
  %error_exit45 = bitcast {}** %error_exit44 to void (%struct.jpeg_common_struct*)**
  %65 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit45, align 4, !tbaa !14
  %66 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %65(%struct.jpeg_common_struct* %66)
  br label %if.end46

if.end46:                                         ; preds = %if.then40, %if.then38
  %67 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  store i32 %67, i32* %undef_row, align 4, !tbaa !31
  br label %if.end49

if.else47:                                        ; preds = %if.then35
  %68 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row48 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %68, i32 0, i32 7
  %69 = load i32, i32* %first_undef_row48, align 4, !tbaa !64
  store i32 %69, i32* %undef_row, align 4, !tbaa !31
  br label %if.end49

if.end49:                                         ; preds = %if.else47, %if.end46
  %70 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool50 = icmp ne i32 %70, 0
  br i1 %tobool50, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.end49
  %71 = load i32, i32* %end_row, align 4, !tbaa !31
  %72 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row52 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %72, i32 0, i32 7
  store i32 %71, i32* %first_undef_row52, align 4, !tbaa !64
  br label %if.end53

if.end53:                                         ; preds = %if.then51, %if.end49
  %73 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %pre_zero = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %73, i32 0, i32 8
  %74 = load i32, i32* %pre_zero, align 4, !tbaa !50
  %tobool54 = icmp ne i32 %74, 0
  br i1 %tobool54, label %if.then55, label %if.else62

if.then55:                                        ; preds = %if.end53
  %75 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  %76 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %samplesperrow = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %76, i32 0, i32 2
  %77 = load i32, i32* %samplesperrow, align 4, !tbaa !48
  %mul = mul i32 %77, 1
  store i32 %mul, i32* %bytesperrow, align 4, !tbaa !9
  %78 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row56 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %78, i32 0, i32 6
  %79 = load i32, i32* %cur_start_row56, align 4, !tbaa !63
  %80 = load i32, i32* %undef_row, align 4, !tbaa !31
  %sub57 = sub i32 %80, %79
  store i32 %sub57, i32* %undef_row, align 4, !tbaa !31
  %81 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row58 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %81, i32 0, i32 6
  %82 = load i32, i32* %cur_start_row58, align 4, !tbaa !63
  %83 = load i32, i32* %end_row, align 4, !tbaa !31
  %sub59 = sub i32 %83, %82
  store i32 %sub59, i32* %end_row, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then55
  %84 = load i32, i32* %undef_row, align 4, !tbaa !31
  %85 = load i32, i32* %end_row, align 4, !tbaa !31
  %cmp60 = icmp ult i32 %84, %85
  br i1 %cmp60, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %86 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer61 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %86, i32 0, i32 0
  %87 = load i8**, i8*** %mem_buffer61, align 4, !tbaa !44
  %88 = load i32, i32* %undef_row, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds i8*, i8** %87, i32 %88
  %89 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %90 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  call void @jzero_far(i8* %89, i32 %90)
  %91 = load i32, i32* %undef_row, align 4, !tbaa !31
  %inc = add i32 %91, 1
  store i32 %inc, i32* %undef_row, align 4, !tbaa !31
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %92 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  br label %if.end71

if.else62:                                        ; preds = %if.end53
  %93 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool63 = icmp ne i32 %93, 0
  br i1 %tobool63, label %if.end70, label %if.then64

if.then64:                                        ; preds = %if.else62
  %94 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err65 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %94, i32 0, i32 0
  %95 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err65, align 4, !tbaa !11
  %msg_code66 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %95, i32 0, i32 5
  store i32 22, i32* %msg_code66, align 4, !tbaa !12
  %96 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err67 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %96, i32 0, i32 0
  %97 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err67, align 4, !tbaa !11
  %error_exit68 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %97, i32 0, i32 0
  %error_exit69 = bitcast {}** %error_exit68 to void (%struct.jpeg_common_struct*)**
  %98 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit69, align 4, !tbaa !14
  %99 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %98(%struct.jpeg_common_struct* %99)
  br label %if.end70

if.end70:                                         ; preds = %if.then64, %if.else62
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %while.end
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %if.end33
  %100 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool73 = icmp ne i32 %100, 0
  br i1 %tobool73, label %if.then74, label %if.end76

if.then74:                                        ; preds = %if.end72
  %101 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %dirty75 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %101, i32 0, i32 9
  store i32 1, i32* %dirty75, align 4, !tbaa !65
  br label %if.end76

if.end76:                                         ; preds = %if.then74, %if.end72
  %102 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer77 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %102, i32 0, i32 0
  %103 = load i8**, i8*** %mem_buffer77, align 4, !tbaa !44
  %104 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %105 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row78 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %105, i32 0, i32 6
  %106 = load i32, i32* %cur_start_row78, align 4, !tbaa !63
  %sub79 = sub i32 %104, %106
  %add.ptr = getelementptr inbounds i8*, i8** %103, i32 %sub79
  %107 = bitcast i32* %undef_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  ret i8** %add.ptr
}

; Function Attrs: nounwind
define internal [64 x i16]** @access_virt_barray(%struct.jpeg_common_struct* %cinfo, %struct.jvirt_barray_control* %ptr, i32 %start_row, i32 %num_rows, i32 %writable) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %ptr.addr = alloca %struct.jvirt_barray_control*, align 4
  %start_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  %writable.addr = alloca i32, align 4
  %end_row = alloca i32, align 4
  %undef_row = alloca i32, align 4
  %ltemp = alloca i32, align 4
  %bytesperrow = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_barray_control* %ptr, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  store i32 %start_row, i32* %start_row.addr, align 4, !tbaa !31
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !31
  store i32 %writable, i32* %writable.addr, align 4, !tbaa !31
  %0 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %2 = load i32, i32* %num_rows.addr, align 4, !tbaa !31
  %add = add i32 %1, %2
  store i32 %add, i32* %end_row, align 4, !tbaa !31
  %3 = bitcast i32* %undef_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %end_row, align 4, !tbaa !31
  %5 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %5, i32 0, i32 1
  %6 = load i32, i32* %rows_in_array, align 4, !tbaa !55
  %cmp = icmp ugt i32 %4, %6
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %7 = load i32, i32* %num_rows.addr, align 4, !tbaa !31
  %8 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %maxaccess = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %8, i32 0, i32 3
  %9 = load i32, i32* %maxaccess, align 4, !tbaa !57
  %cmp1 = icmp ugt i32 %7, %9
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %10 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %10, i32 0, i32 0
  %11 = load [64 x i16]**, [64 x i16]*** %mem_buffer, align 4, !tbaa !53
  %cmp3 = icmp eq [64 x i16]** %11, null
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 22, i32* %msg_code, align 4, !tbaa !12
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %error_exit5 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit5, align 4, !tbaa !14
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %16(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false2
  %18 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %19 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %19, i32 0, i32 6
  %20 = load i32, i32* %cur_start_row, align 4, !tbaa !68
  %cmp6 = icmp ult i32 %18, %20
  br i1 %cmp6, label %if.then11, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end
  %21 = load i32, i32* %end_row, align 4, !tbaa !31
  %22 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row8 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %22, i32 0, i32 6
  %23 = load i32, i32* %cur_start_row8, align 4, !tbaa !68
  %24 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %24, i32 0, i32 4
  %25 = load i32, i32* %rows_in_mem, align 4, !tbaa !66
  %add9 = add i32 %23, %25
  %cmp10 = icmp ugt i32 %21, %add9
  br i1 %cmp10, label %if.then11, label %if.end33

if.then11:                                        ; preds = %lor.lhs.false7, %if.end
  %26 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %26, i32 0, i32 10
  %27 = load i32, i32* %b_s_open, align 4, !tbaa !59
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.end18, label %if.then12

if.then12:                                        ; preds = %if.then11
  %28 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 4, !tbaa !11
  %msg_code14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 5
  store i32 69, i32* %msg_code14, align 4, !tbaa !12
  %30 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 4, !tbaa !11
  %error_exit16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 0
  %error_exit17 = bitcast {}** %error_exit16 to void (%struct.jpeg_common_struct*)**
  %32 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit17, align 4, !tbaa !14
  %33 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %32(%struct.jpeg_common_struct* %33)
  br label %if.end18

if.end18:                                         ; preds = %if.then12, %if.then11
  %34 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %dirty = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %34, i32 0, i32 9
  %35 = load i32, i32* %dirty, align 4, !tbaa !70
  %tobool19 = icmp ne i32 %35, 0
  br i1 %tobool19, label %if.then20, label %if.end22

if.then20:                                        ; preds = %if.end18
  %36 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  call void @do_barray_io(%struct.jpeg_common_struct* %36, %struct.jvirt_barray_control* %37, i32 1)
  %38 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %dirty21 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %38, i32 0, i32 9
  store i32 0, i32* %dirty21, align 4, !tbaa !70
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end18
  %39 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %40 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row23 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %40, i32 0, i32 6
  %41 = load i32, i32* %cur_start_row23, align 4, !tbaa !68
  %cmp24 = icmp ugt i32 %39, %41
  br i1 %cmp24, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end22
  %42 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %43 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row26 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %43, i32 0, i32 6
  store i32 %42, i32* %cur_start_row26, align 4, !tbaa !68
  br label %if.end32

if.else:                                          ; preds = %if.end22
  %44 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load i32, i32* %end_row, align 4, !tbaa !31
  %46 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem27 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %46, i32 0, i32 4
  %47 = load i32, i32* %rows_in_mem27, align 4, !tbaa !66
  %sub = sub nsw i32 %45, %47
  store i32 %sub, i32* %ltemp, align 4, !tbaa !9
  %48 = load i32, i32* %ltemp, align 4, !tbaa !9
  %cmp28 = icmp slt i32 %48, 0
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.else
  store i32 0, i32* %ltemp, align 4, !tbaa !9
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.else
  %49 = load i32, i32* %ltemp, align 4, !tbaa !9
  %50 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row31 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %50, i32 0, i32 6
  store i32 %49, i32* %cur_start_row31, align 4, !tbaa !68
  %51 = bitcast i32* %ltemp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  br label %if.end32

if.end32:                                         ; preds = %if.end30, %if.then25
  %52 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %53 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  call void @do_barray_io(%struct.jpeg_common_struct* %52, %struct.jvirt_barray_control* %53, i32 0)
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %lor.lhs.false7
  %54 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %54, i32 0, i32 7
  %55 = load i32, i32* %first_undef_row, align 4, !tbaa !69
  %56 = load i32, i32* %end_row, align 4, !tbaa !31
  %cmp34 = icmp ult i32 %55, %56
  br i1 %cmp34, label %if.then35, label %if.end72

if.then35:                                        ; preds = %if.end33
  %57 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row36 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %57, i32 0, i32 7
  %58 = load i32, i32* %first_undef_row36, align 4, !tbaa !69
  %59 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %cmp37 = icmp ult i32 %58, %59
  br i1 %cmp37, label %if.then38, label %if.else47

if.then38:                                        ; preds = %if.then35
  %60 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool39 = icmp ne i32 %60, 0
  br i1 %tobool39, label %if.then40, label %if.end46

if.then40:                                        ; preds = %if.then38
  %61 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 4, !tbaa !11
  %msg_code42 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 5
  store i32 22, i32* %msg_code42, align 4, !tbaa !12
  %63 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 4, !tbaa !11
  %error_exit44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 0
  %error_exit45 = bitcast {}** %error_exit44 to void (%struct.jpeg_common_struct*)**
  %65 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit45, align 4, !tbaa !14
  %66 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %65(%struct.jpeg_common_struct* %66)
  br label %if.end46

if.end46:                                         ; preds = %if.then40, %if.then38
  %67 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  store i32 %67, i32* %undef_row, align 4, !tbaa !31
  br label %if.end49

if.else47:                                        ; preds = %if.then35
  %68 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row48 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %68, i32 0, i32 7
  %69 = load i32, i32* %first_undef_row48, align 4, !tbaa !69
  store i32 %69, i32* %undef_row, align 4, !tbaa !31
  br label %if.end49

if.end49:                                         ; preds = %if.else47, %if.end46
  %70 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool50 = icmp ne i32 %70, 0
  br i1 %tobool50, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.end49
  %71 = load i32, i32* %end_row, align 4, !tbaa !31
  %72 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row52 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %72, i32 0, i32 7
  store i32 %71, i32* %first_undef_row52, align 4, !tbaa !69
  br label %if.end53

if.end53:                                         ; preds = %if.then51, %if.end49
  %73 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %pre_zero = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %73, i32 0, i32 8
  %74 = load i32, i32* %pre_zero, align 4, !tbaa !58
  %tobool54 = icmp ne i32 %74, 0
  br i1 %tobool54, label %if.then55, label %if.else62

if.then55:                                        ; preds = %if.end53
  %75 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  %76 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %blocksperrow = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %76, i32 0, i32 2
  %77 = load i32, i32* %blocksperrow, align 4, !tbaa !56
  %mul = mul i32 %77, 128
  store i32 %mul, i32* %bytesperrow, align 4, !tbaa !9
  %78 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row56 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %78, i32 0, i32 6
  %79 = load i32, i32* %cur_start_row56, align 4, !tbaa !68
  %80 = load i32, i32* %undef_row, align 4, !tbaa !31
  %sub57 = sub i32 %80, %79
  store i32 %sub57, i32* %undef_row, align 4, !tbaa !31
  %81 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row58 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %81, i32 0, i32 6
  %82 = load i32, i32* %cur_start_row58, align 4, !tbaa !68
  %83 = load i32, i32* %end_row, align 4, !tbaa !31
  %sub59 = sub i32 %83, %82
  store i32 %sub59, i32* %end_row, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then55
  %84 = load i32, i32* %undef_row, align 4, !tbaa !31
  %85 = load i32, i32* %end_row, align 4, !tbaa !31
  %cmp60 = icmp ult i32 %84, %85
  br i1 %cmp60, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %86 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer61 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %86, i32 0, i32 0
  %87 = load [64 x i16]**, [64 x i16]*** %mem_buffer61, align 4, !tbaa !53
  %88 = load i32, i32* %undef_row, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %87, i32 %88
  %89 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  %90 = bitcast [64 x i16]* %89 to i8*
  %91 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  call void @jzero_far(i8* %90, i32 %91)
  %92 = load i32, i32* %undef_row, align 4, !tbaa !31
  %inc = add i32 %92, 1
  store i32 %inc, i32* %undef_row, align 4, !tbaa !31
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %93 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  br label %if.end71

if.else62:                                        ; preds = %if.end53
  %94 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool63 = icmp ne i32 %94, 0
  br i1 %tobool63, label %if.end70, label %if.then64

if.then64:                                        ; preds = %if.else62
  %95 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err65 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %95, i32 0, i32 0
  %96 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err65, align 4, !tbaa !11
  %msg_code66 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %96, i32 0, i32 5
  store i32 22, i32* %msg_code66, align 4, !tbaa !12
  %97 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err67 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %97, i32 0, i32 0
  %98 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err67, align 4, !tbaa !11
  %error_exit68 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %98, i32 0, i32 0
  %error_exit69 = bitcast {}** %error_exit68 to void (%struct.jpeg_common_struct*)**
  %99 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit69, align 4, !tbaa !14
  %100 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %99(%struct.jpeg_common_struct* %100)
  br label %if.end70

if.end70:                                         ; preds = %if.then64, %if.else62
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %while.end
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %if.end33
  %101 = load i32, i32* %writable.addr, align 4, !tbaa !31
  %tobool73 = icmp ne i32 %101, 0
  br i1 %tobool73, label %if.then74, label %if.end76

if.then74:                                        ; preds = %if.end72
  %102 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %dirty75 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %102, i32 0, i32 9
  store i32 1, i32* %dirty75, align 4, !tbaa !70
  br label %if.end76

if.end76:                                         ; preds = %if.then74, %if.end72
  %103 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer77 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %103, i32 0, i32 0
  %104 = load [64 x i16]**, [64 x i16]*** %mem_buffer77, align 4, !tbaa !53
  %105 = load i32, i32* %start_row.addr, align 4, !tbaa !31
  %106 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row78 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %106, i32 0, i32 6
  %107 = load i32, i32* %cur_start_row78, align 4, !tbaa !68
  %sub79 = sub i32 %105, %107
  %add.ptr = getelementptr inbounds [64 x i16]*, [64 x i16]** %104, i32 %sub79
  %108 = bitcast i32* %undef_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %end_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  ret [64 x i16]** %add.ptr
}

; Function Attrs: nounwind
define internal void @free_pool(%struct.jpeg_common_struct* %cinfo, i32 %pool_id) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool_id.addr = alloca i32, align 4
  %mem = alloca %struct.my_memory_mgr*, align 4
  %shdr_ptr = alloca %struct.small_pool_struct*, align 4
  %lhdr_ptr = alloca %struct.large_pool_struct*, align 4
  %space_freed = alloca i32, align 4
  %sptr = alloca %struct.jvirt_sarray_control*, align 4
  %bptr = alloca %struct.jvirt_barray_control*, align 4
  %next_lhdr_ptr = alloca %struct.large_pool_struct*, align 4
  %next_shdr_ptr = alloca %struct.small_pool_struct*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !31
  %0 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %3 = bitcast %struct.jpeg_memory_mgr* %2 to %struct.my_memory_mgr*
  store %struct.my_memory_mgr* %3, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %4 = bitcast %struct.small_pool_struct** %shdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.large_pool_struct** %lhdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %space_freed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp = icmp slt i32 %7, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp2 = icmp sge i32 %8, 2
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %9 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 14, i32* %msg_code, align 4, !tbaa !12
  %11 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %11, i32* %arrayidx, align 4, !tbaa !15
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %error_exit5 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit5, align 4, !tbaa !14
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %16(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %18 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %cmp6 = icmp eq i32 %18, 1
  br i1 %cmp6, label %if.then7, label %if.end29

if.then7:                                         ; preds = %if.end
  %19 = bitcast %struct.jvirt_sarray_control** %sptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast %struct.jvirt_barray_control** %bptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %21, i32 0, i32 3
  %22 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %virt_sarray_list, align 4, !tbaa !32
  store %struct.jvirt_sarray_control* %22, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then7
  %23 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %cmp8 = icmp ne %struct.jvirt_sarray_control* %23, null
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_open = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %24, i32 0, i32 10
  %25 = load i32, i32* %b_s_open, align 4, !tbaa !51
  %tobool = icmp ne i32 %25, 0
  br i1 %tobool, label %if.then9, label %if.end12

if.then9:                                         ; preds = %for.body
  %26 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_open10 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %26, i32 0, i32 10
  store i32 0, i32* %b_s_open10, align 4, !tbaa !51
  %27 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_info = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %27, i32 0, i32 12
  %close_backing_store = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info, i32 0, i32 2
  %28 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)** %close_backing_store, align 4, !tbaa !71
  %29 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %30 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %b_s_info11 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %30, i32 0, i32 12
  call void %28(%struct.jpeg_common_struct* %29, %struct.backing_store_struct* %b_s_info11)
  br label %if.end12

if.end12:                                         ; preds = %if.then9, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %31 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %31, i32 0, i32 11
  %32 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %next, align 4, !tbaa !52
  store %struct.jvirt_sarray_control* %32, %struct.jvirt_sarray_control** %sptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %33 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_sarray_list13 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %33, i32 0, i32 3
  store %struct.jvirt_sarray_control* null, %struct.jvirt_sarray_control** %virt_sarray_list13, align 4, !tbaa !32
  %34 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %34, i32 0, i32 4
  %35 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %virt_barray_list, align 4, !tbaa !33
  store %struct.jvirt_barray_control* %35, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc25, %for.end
  %36 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %cmp15 = icmp ne %struct.jvirt_barray_control* %36, null
  br i1 %cmp15, label %for.body16, label %for.end27

for.body16:                                       ; preds = %for.cond14
  %37 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_open17 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %37, i32 0, i32 10
  %38 = load i32, i32* %b_s_open17, align 4, !tbaa !59
  %tobool18 = icmp ne i32 %38, 0
  br i1 %tobool18, label %if.then19, label %if.end24

if.then19:                                        ; preds = %for.body16
  %39 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_open20 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %39, i32 0, i32 10
  store i32 0, i32* %b_s_open20, align 4, !tbaa !59
  %40 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_info21 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %40, i32 0, i32 12
  %close_backing_store22 = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info21, i32 0, i32 2
  %41 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)** %close_backing_store22, align 4, !tbaa !72
  %42 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %43 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %b_s_info23 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %43, i32 0, i32 12
  call void %41(%struct.jpeg_common_struct* %42, %struct.backing_store_struct* %b_s_info23)
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %for.body16
  br label %for.inc25

for.inc25:                                        ; preds = %if.end24
  %44 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  %next26 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %44, i32 0, i32 11
  %45 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %next26, align 4, !tbaa !60
  store %struct.jvirt_barray_control* %45, %struct.jvirt_barray_control** %bptr, align 4, !tbaa !2
  br label %for.cond14

for.end27:                                        ; preds = %for.cond14
  %46 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %virt_barray_list28 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %46, i32 0, i32 4
  store %struct.jvirt_barray_control* null, %struct.jvirt_barray_control** %virt_barray_list28, align 4, !tbaa !33
  %47 = bitcast %struct.jvirt_barray_control** %bptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast %struct.jvirt_sarray_control** %sptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  br label %if.end29

if.end29:                                         ; preds = %for.end27, %if.end
  %49 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %large_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %49, i32 0, i32 2
  %50 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx30 = getelementptr inbounds [2 x %struct.large_pool_struct*], [2 x %struct.large_pool_struct*]* %large_list, i32 0, i32 %50
  %51 = load %struct.large_pool_struct*, %struct.large_pool_struct** %arrayidx30, align 4, !tbaa !2
  store %struct.large_pool_struct* %51, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %52 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %large_list31 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %52, i32 0, i32 2
  %53 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx32 = getelementptr inbounds [2 x %struct.large_pool_struct*], [2 x %struct.large_pool_struct*]* %large_list31, i32 0, i32 %53
  store %struct.large_pool_struct* null, %struct.large_pool_struct** %arrayidx32, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end29
  %54 = load %struct.large_pool_struct*, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %cmp33 = icmp ne %struct.large_pool_struct* %54, null
  br i1 %cmp33, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %55 = bitcast %struct.large_pool_struct** %next_lhdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = load %struct.large_pool_struct*, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %next34 = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %56, i32 0, i32 0
  %57 = load %struct.large_pool_struct*, %struct.large_pool_struct** %next34, align 4, !tbaa !39
  store %struct.large_pool_struct* %57, %struct.large_pool_struct** %next_lhdr_ptr, align 4, !tbaa !2
  %58 = load %struct.large_pool_struct*, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %bytes_used = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %58, i32 0, i32 1
  %59 = load i32, i32* %bytes_used, align 4, !tbaa !41
  %60 = load %struct.large_pool_struct*, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %bytes_left = getelementptr inbounds %struct.large_pool_struct, %struct.large_pool_struct* %60, i32 0, i32 2
  %61 = load i32, i32* %bytes_left, align 4, !tbaa !42
  %add = add i32 %59, %61
  %add35 = add i32 %add, 12
  store i32 %add35, i32* %space_freed, align 4, !tbaa !9
  %62 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %63 = load %struct.large_pool_struct*, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %64 = bitcast %struct.large_pool_struct* %63 to i8*
  %65 = load i32, i32* %space_freed, align 4, !tbaa !9
  call void @jpeg_free_large(%struct.jpeg_common_struct* %62, i8* %64, i32 %65)
  %66 = load i32, i32* %space_freed, align 4, !tbaa !9
  %67 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %67, i32 0, i32 5
  %68 = load i32, i32* %total_space_allocated, align 4, !tbaa !34
  %sub = sub i32 %68, %66
  store i32 %sub, i32* %total_space_allocated, align 4, !tbaa !34
  %69 = load %struct.large_pool_struct*, %struct.large_pool_struct** %next_lhdr_ptr, align 4, !tbaa !2
  store %struct.large_pool_struct* %69, %struct.large_pool_struct** %lhdr_ptr, align 4, !tbaa !2
  %70 = bitcast %struct.large_pool_struct** %next_lhdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %71 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %small_list = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %71, i32 0, i32 1
  %72 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx36 = getelementptr inbounds [2 x %struct.small_pool_struct*], [2 x %struct.small_pool_struct*]* %small_list, i32 0, i32 %72
  %73 = load %struct.small_pool_struct*, %struct.small_pool_struct** %arrayidx36, align 4, !tbaa !2
  store %struct.small_pool_struct* %73, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %74 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %small_list37 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %74, i32 0, i32 1
  %75 = load i32, i32* %pool_id.addr, align 4, !tbaa !31
  %arrayidx38 = getelementptr inbounds [2 x %struct.small_pool_struct*], [2 x %struct.small_pool_struct*]* %small_list37, i32 0, i32 %75
  store %struct.small_pool_struct* null, %struct.small_pool_struct** %arrayidx38, align 4, !tbaa !2
  br label %while.cond39

while.cond39:                                     ; preds = %while.body41, %while.end
  %76 = load %struct.small_pool_struct*, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %cmp40 = icmp ne %struct.small_pool_struct* %76, null
  br i1 %cmp40, label %while.body41, label %while.end49

while.body41:                                     ; preds = %while.cond39
  %77 = bitcast %struct.small_pool_struct** %next_shdr_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #3
  %78 = load %struct.small_pool_struct*, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %next42 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %78, i32 0, i32 0
  %79 = load %struct.small_pool_struct*, %struct.small_pool_struct** %next42, align 4, !tbaa !37
  store %struct.small_pool_struct* %79, %struct.small_pool_struct** %next_shdr_ptr, align 4, !tbaa !2
  %80 = load %struct.small_pool_struct*, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %bytes_used43 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %80, i32 0, i32 1
  %81 = load i32, i32* %bytes_used43, align 4, !tbaa !38
  %82 = load %struct.small_pool_struct*, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %bytes_left44 = getelementptr inbounds %struct.small_pool_struct, %struct.small_pool_struct* %82, i32 0, i32 2
  %83 = load i32, i32* %bytes_left44, align 4, !tbaa !35
  %add45 = add i32 %81, %83
  %add46 = add i32 %add45, 12
  store i32 %add46, i32* %space_freed, align 4, !tbaa !9
  %84 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %85 = load %struct.small_pool_struct*, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %86 = bitcast %struct.small_pool_struct* %85 to i8*
  %87 = load i32, i32* %space_freed, align 4, !tbaa !9
  call void @jpeg_free_small(%struct.jpeg_common_struct* %84, i8* %86, i32 %87)
  %88 = load i32, i32* %space_freed, align 4, !tbaa !9
  %89 = load %struct.my_memory_mgr*, %struct.my_memory_mgr** %mem, align 4, !tbaa !2
  %total_space_allocated47 = getelementptr inbounds %struct.my_memory_mgr, %struct.my_memory_mgr* %89, i32 0, i32 5
  %90 = load i32, i32* %total_space_allocated47, align 4, !tbaa !34
  %sub48 = sub i32 %90, %88
  store i32 %sub48, i32* %total_space_allocated47, align 4, !tbaa !34
  %91 = load %struct.small_pool_struct*, %struct.small_pool_struct** %next_shdr_ptr, align 4, !tbaa !2
  store %struct.small_pool_struct* %91, %struct.small_pool_struct** %shdr_ptr, align 4, !tbaa !2
  %92 = bitcast %struct.small_pool_struct** %next_shdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  br label %while.cond39

while.end49:                                      ; preds = %while.cond39
  %93 = bitcast i32* %space_freed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast %struct.large_pool_struct** %lhdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  %95 = bitcast %struct.small_pool_struct** %shdr_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast %struct.my_memory_mgr** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  ret void
}

; Function Attrs: nounwind
define internal void @self_destruct(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %pool = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 1, i32* %pool, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %pool, align 4, !tbaa !31
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i32, i32* %pool, align 4, !tbaa !31
  call void @free_pool(%struct.jpeg_common_struct* %2, i32 %3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %pool, align 4, !tbaa !31
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %pool, align 4, !tbaa !31
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %8 = bitcast %struct.jpeg_memory_mgr* %7 to i8*
  call void @jpeg_free_small(%struct.jpeg_common_struct* %5, i8* %8, i32 84)
  %9 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %9, i32 0, i32 1
  store %struct.jpeg_memory_mgr* null, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !6
  %10 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_mem_term(%struct.jpeg_common_struct* %10)
  %11 = bitcast i32* %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  ret void
}

declare i8* @getenv(i8*) #2

declare i32 @sscanf(i8*, i8*, ...) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @out_of_memory(%struct.jpeg_common_struct* %cinfo, i32 %which) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %which.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %which, i32* %which.addr, align 4, !tbaa !31
  %0 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %0, i32 0, i32 0
  %1 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %1, i32 0, i32 5
  store i32 54, i32* %msg_code, align 4, !tbaa !12
  %2 = load i32, i32* %which.addr, align 4, !tbaa !31
  %3 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 4, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %2, i32* %arrayidx, align 4, !tbaa !15
  %5 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 4, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 0
  %error_exit3 = bitcast {}** %error_exit to void (%struct.jpeg_common_struct*)**
  %7 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit3, align 4, !tbaa !14
  %8 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %7(%struct.jpeg_common_struct* %8)
  ret void
}

; Function Attrs: nounwind
define internal i32 @round_up_pow2(i32 %a, i32 %b) #0 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4, !tbaa !9
  store i32 %b, i32* %b.addr, align 4, !tbaa !9
  %0 = load i32, i32* %a.addr, align 4, !tbaa !9
  %1 = load i32, i32* %b.addr, align 4, !tbaa !9
  %add = add i32 %0, %1
  %sub = sub i32 %add, 1
  %2 = load i32, i32* %b.addr, align 4, !tbaa !9
  %sub1 = sub i32 %2, 1
  %neg = xor i32 %sub1, -1
  %and = and i32 %sub, %neg
  ret i32 %and
}

declare i8* @jpeg_get_large(%struct.jpeg_common_struct*, i32) #2

declare i32 @jpeg_mem_available(%struct.jpeg_common_struct*, i32, i32, i32) #2

declare void @jpeg_open_backing_store(%struct.jpeg_common_struct*, %struct.backing_store_struct*, i32) #2

; Function Attrs: nounwind
define internal void @do_sarray_io(%struct.jpeg_common_struct* %cinfo, %struct.jvirt_sarray_control* %ptr, i32 %writing) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %ptr.addr = alloca %struct.jvirt_sarray_control*, align 4
  %writing.addr = alloca i32, align 4
  %bytesperrow = alloca i32, align 4
  %file_offset = alloca i32, align 4
  %byte_count = alloca i32, align 4
  %rows = alloca i32, align 4
  %thisrow = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_sarray_control* %ptr, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  store i32 %writing, i32* %writing.addr, align 4, !tbaa !31
  %0 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %file_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %byte_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %thisrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %samplesperrow = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %6, i32 0, i32 2
  %7 = load i32, i32* %samplesperrow, align 4, !tbaa !48
  %mul = mul i32 %7, 1
  store i32 %mul, i32* %bytesperrow, align 4, !tbaa !9
  %8 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %8, i32 0, i32 6
  %9 = load i32, i32* %cur_start_row, align 4, !tbaa !63
  %10 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  %mul1 = mul i32 %9, %10
  store i32 %mul1, i32* %file_offset, align 4, !tbaa !9
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !9
  %12 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %12, i32 0, i32 4
  %13 = load i32, i32* %rows_in_mem, align 4, !tbaa !61
  %cmp = icmp slt i32 %11, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %14, i32 0, i32 5
  %15 = load i32, i32* %rowsperchunk, align 4, !tbaa !62
  %16 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem2 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %16, i32 0, i32 4
  %17 = load i32, i32* %rows_in_mem2, align 4, !tbaa !61
  %18 = load i32, i32* %i, align 4, !tbaa !9
  %sub = sub nsw i32 %17, %18
  %cmp3 = icmp slt i32 %15, %sub
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %19 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk4 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %19, i32 0, i32 5
  %20 = load i32, i32* %rowsperchunk4, align 4, !tbaa !62
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %21 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem5 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %21, i32 0, i32 4
  %22 = load i32, i32* %rows_in_mem5, align 4, !tbaa !61
  %23 = load i32, i32* %i, align 4, !tbaa !9
  %sub6 = sub nsw i32 %22, %23
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %20, %cond.true ], [ %sub6, %cond.false ]
  store i32 %cond, i32* %rows, align 4, !tbaa !9
  %24 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row7 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %24, i32 0, i32 6
  %25 = load i32, i32* %cur_start_row7, align 4, !tbaa !63
  %26 = load i32, i32* %i, align 4, !tbaa !9
  %add = add nsw i32 %25, %26
  store i32 %add, i32* %thisrow, align 4, !tbaa !9
  %27 = load i32, i32* %rows, align 4, !tbaa !9
  %28 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %28, i32 0, i32 7
  %29 = load i32, i32* %first_undef_row, align 4, !tbaa !64
  %30 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub8 = sub nsw i32 %29, %30
  %cmp9 = icmp slt i32 %27, %sub8
  br i1 %cmp9, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %31 = load i32, i32* %rows, align 4, !tbaa !9
  br label %cond.end14

cond.false11:                                     ; preds = %cond.end
  %32 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row12 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %32, i32 0, i32 7
  %33 = load i32, i32* %first_undef_row12, align 4, !tbaa !64
  %34 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub13 = sub nsw i32 %33, %34
  br label %cond.end14

cond.end14:                                       ; preds = %cond.false11, %cond.true10
  %cond15 = phi i32 [ %31, %cond.true10 ], [ %sub13, %cond.false11 ]
  store i32 %cond15, i32* %rows, align 4, !tbaa !9
  %35 = load i32, i32* %rows, align 4, !tbaa !9
  %36 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %36, i32 0, i32 1
  %37 = load i32, i32* %rows_in_array, align 4, !tbaa !47
  %38 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub16 = sub nsw i32 %37, %38
  %cmp17 = icmp slt i32 %35, %sub16
  br i1 %cmp17, label %cond.true18, label %cond.false19

cond.true18:                                      ; preds = %cond.end14
  %39 = load i32, i32* %rows, align 4, !tbaa !9
  br label %cond.end22

cond.false19:                                     ; preds = %cond.end14
  %40 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array20 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %40, i32 0, i32 1
  %41 = load i32, i32* %rows_in_array20, align 4, !tbaa !47
  %42 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub21 = sub nsw i32 %41, %42
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false19, %cond.true18
  %cond23 = phi i32 [ %39, %cond.true18 ], [ %sub21, %cond.false19 ]
  store i32 %cond23, i32* %rows, align 4, !tbaa !9
  %43 = load i32, i32* %rows, align 4, !tbaa !9
  %cmp24 = icmp sle i32 %43, 0
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end22
  br label %for.end

if.end:                                           ; preds = %cond.end22
  %44 = load i32, i32* %rows, align 4, !tbaa !9
  %45 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  %mul25 = mul nsw i32 %44, %45
  store i32 %mul25, i32* %byte_count, align 4, !tbaa !9
  %46 = load i32, i32* %writing.addr, align 4, !tbaa !31
  %tobool = icmp ne i32 %46, 0
  br i1 %tobool, label %if.then26, label %if.else

if.then26:                                        ; preds = %if.end
  %47 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %47, i32 0, i32 12
  %write_backing_store = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info, i32 0, i32 1
  %48 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)** %write_backing_store, align 4, !tbaa !73
  %49 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %50 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info27 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %50, i32 0, i32 12
  %51 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %51, i32 0, i32 0
  %52 = load i8**, i8*** %mem_buffer, align 4, !tbaa !44
  %53 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds i8*, i8** %52, i32 %53
  %54 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %55 = load i32, i32* %file_offset, align 4, !tbaa !9
  %56 = load i32, i32* %byte_count, align 4, !tbaa !9
  call void %48(%struct.jpeg_common_struct* %49, %struct.backing_store_struct* %b_s_info27, i8* %54, i32 %55, i32 %56)
  br label %if.end32

if.else:                                          ; preds = %if.end
  %57 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info28 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %57, i32 0, i32 12
  %read_backing_store = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info28, i32 0, i32 0
  %58 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)** %read_backing_store, align 4, !tbaa !74
  %59 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %60 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info29 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %60, i32 0, i32 12
  %61 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer30 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %61, i32 0, i32 0
  %62 = load i8**, i8*** %mem_buffer30, align 4, !tbaa !44
  %63 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx31 = getelementptr inbounds i8*, i8** %62, i32 %63
  %64 = load i8*, i8** %arrayidx31, align 4, !tbaa !2
  %65 = load i32, i32* %file_offset, align 4, !tbaa !9
  %66 = load i32, i32* %byte_count, align 4, !tbaa !9
  call void %58(%struct.jpeg_common_struct* %59, %struct.backing_store_struct* %b_s_info29, i8* %64, i32 %65, i32 %66)
  br label %if.end32

if.end32:                                         ; preds = %if.else, %if.then26
  %67 = load i32, i32* %byte_count, align 4, !tbaa !9
  %68 = load i32, i32* %file_offset, align 4, !tbaa !9
  %add33 = add nsw i32 %68, %67
  store i32 %add33, i32* %file_offset, align 4, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %69 = load %struct.jvirt_sarray_control*, %struct.jvirt_sarray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk34 = getelementptr inbounds %struct.jvirt_sarray_control, %struct.jvirt_sarray_control* %69, i32 0, i32 5
  %70 = load i32, i32* %rowsperchunk34, align 4, !tbaa !62
  %71 = load i32, i32* %i, align 4, !tbaa !9
  %add35 = add i32 %71, %70
  store i32 %add35, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %thisrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i32* %byte_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i32* %file_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  ret void
}

declare void @jzero_far(i8*, i32) #2

; Function Attrs: nounwind
define internal void @do_barray_io(%struct.jpeg_common_struct* %cinfo, %struct.jvirt_barray_control* %ptr, i32 %writing) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %ptr.addr = alloca %struct.jvirt_barray_control*, align 4
  %writing.addr = alloca i32, align 4
  %bytesperrow = alloca i32, align 4
  %file_offset = alloca i32, align 4
  %byte_count = alloca i32, align 4
  %rows = alloca i32, align 4
  %thisrow = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jvirt_barray_control* %ptr, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  store i32 %writing, i32* %writing.addr, align 4, !tbaa !31
  %0 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %file_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %byte_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %thisrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %blocksperrow = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %6, i32 0, i32 2
  %7 = load i32, i32* %blocksperrow, align 4, !tbaa !56
  %mul = mul i32 %7, 128
  store i32 %mul, i32* %bytesperrow, align 4, !tbaa !9
  %8 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %8, i32 0, i32 6
  %9 = load i32, i32* %cur_start_row, align 4, !tbaa !68
  %10 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  %mul1 = mul i32 %9, %10
  store i32 %mul1, i32* %file_offset, align 4, !tbaa !9
  store i32 0, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !9
  %12 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %12, i32 0, i32 4
  %13 = load i32, i32* %rows_in_mem, align 4, !tbaa !66
  %cmp = icmp slt i32 %11, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %14, i32 0, i32 5
  %15 = load i32, i32* %rowsperchunk, align 4, !tbaa !67
  %16 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem2 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %16, i32 0, i32 4
  %17 = load i32, i32* %rows_in_mem2, align 4, !tbaa !66
  %18 = load i32, i32* %i, align 4, !tbaa !9
  %sub = sub nsw i32 %17, %18
  %cmp3 = icmp slt i32 %15, %sub
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %19 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk4 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %19, i32 0, i32 5
  %20 = load i32, i32* %rowsperchunk4, align 4, !tbaa !67
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %21 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_mem5 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %21, i32 0, i32 4
  %22 = load i32, i32* %rows_in_mem5, align 4, !tbaa !66
  %23 = load i32, i32* %i, align 4, !tbaa !9
  %sub6 = sub nsw i32 %22, %23
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %20, %cond.true ], [ %sub6, %cond.false ]
  store i32 %cond, i32* %rows, align 4, !tbaa !9
  %24 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %cur_start_row7 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %24, i32 0, i32 6
  %25 = load i32, i32* %cur_start_row7, align 4, !tbaa !68
  %26 = load i32, i32* %i, align 4, !tbaa !9
  %add = add nsw i32 %25, %26
  store i32 %add, i32* %thisrow, align 4, !tbaa !9
  %27 = load i32, i32* %rows, align 4, !tbaa !9
  %28 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %28, i32 0, i32 7
  %29 = load i32, i32* %first_undef_row, align 4, !tbaa !69
  %30 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub8 = sub nsw i32 %29, %30
  %cmp9 = icmp slt i32 %27, %sub8
  br i1 %cmp9, label %cond.true10, label %cond.false11

cond.true10:                                      ; preds = %cond.end
  %31 = load i32, i32* %rows, align 4, !tbaa !9
  br label %cond.end14

cond.false11:                                     ; preds = %cond.end
  %32 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %first_undef_row12 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %32, i32 0, i32 7
  %33 = load i32, i32* %first_undef_row12, align 4, !tbaa !69
  %34 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub13 = sub nsw i32 %33, %34
  br label %cond.end14

cond.end14:                                       ; preds = %cond.false11, %cond.true10
  %cond15 = phi i32 [ %31, %cond.true10 ], [ %sub13, %cond.false11 ]
  store i32 %cond15, i32* %rows, align 4, !tbaa !9
  %35 = load i32, i32* %rows, align 4, !tbaa !9
  %36 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %36, i32 0, i32 1
  %37 = load i32, i32* %rows_in_array, align 4, !tbaa !55
  %38 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub16 = sub nsw i32 %37, %38
  %cmp17 = icmp slt i32 %35, %sub16
  br i1 %cmp17, label %cond.true18, label %cond.false19

cond.true18:                                      ; preds = %cond.end14
  %39 = load i32, i32* %rows, align 4, !tbaa !9
  br label %cond.end22

cond.false19:                                     ; preds = %cond.end14
  %40 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rows_in_array20 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %40, i32 0, i32 1
  %41 = load i32, i32* %rows_in_array20, align 4, !tbaa !55
  %42 = load i32, i32* %thisrow, align 4, !tbaa !9
  %sub21 = sub nsw i32 %41, %42
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false19, %cond.true18
  %cond23 = phi i32 [ %39, %cond.true18 ], [ %sub21, %cond.false19 ]
  store i32 %cond23, i32* %rows, align 4, !tbaa !9
  %43 = load i32, i32* %rows, align 4, !tbaa !9
  %cmp24 = icmp sle i32 %43, 0
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end22
  br label %for.end

if.end:                                           ; preds = %cond.end22
  %44 = load i32, i32* %rows, align 4, !tbaa !9
  %45 = load i32, i32* %bytesperrow, align 4, !tbaa !9
  %mul25 = mul nsw i32 %44, %45
  store i32 %mul25, i32* %byte_count, align 4, !tbaa !9
  %46 = load i32, i32* %writing.addr, align 4, !tbaa !31
  %tobool = icmp ne i32 %46, 0
  br i1 %tobool, label %if.then26, label %if.else

if.then26:                                        ; preds = %if.end
  %47 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %47, i32 0, i32 12
  %write_backing_store = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info, i32 0, i32 1
  %48 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)** %write_backing_store, align 4, !tbaa !75
  %49 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %50 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info27 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %50, i32 0, i32 12
  %51 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %51, i32 0, i32 0
  %52 = load [64 x i16]**, [64 x i16]*** %mem_buffer, align 4, !tbaa !53
  %53 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %52, i32 %53
  %54 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  %55 = bitcast [64 x i16]* %54 to i8*
  %56 = load i32, i32* %file_offset, align 4, !tbaa !9
  %57 = load i32, i32* %byte_count, align 4, !tbaa !9
  call void %48(%struct.jpeg_common_struct* %49, %struct.backing_store_struct* %b_s_info27, i8* %55, i32 %56, i32 %57)
  br label %if.end32

if.else:                                          ; preds = %if.end
  %58 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info28 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %58, i32 0, i32 12
  %read_backing_store = getelementptr inbounds %struct.backing_store_struct, %struct.backing_store_struct* %b_s_info28, i32 0, i32 0
  %59 = load void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)** %read_backing_store, align 4, !tbaa !76
  %60 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %61 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %b_s_info29 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %61, i32 0, i32 12
  %62 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %mem_buffer30 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %62, i32 0, i32 0
  %63 = load [64 x i16]**, [64 x i16]*** %mem_buffer30, align 4, !tbaa !53
  %64 = load i32, i32* %i, align 4, !tbaa !9
  %arrayidx31 = getelementptr inbounds [64 x i16]*, [64 x i16]** %63, i32 %64
  %65 = load [64 x i16]*, [64 x i16]** %arrayidx31, align 4, !tbaa !2
  %66 = bitcast [64 x i16]* %65 to i8*
  %67 = load i32, i32* %file_offset, align 4, !tbaa !9
  %68 = load i32, i32* %byte_count, align 4, !tbaa !9
  call void %59(%struct.jpeg_common_struct* %60, %struct.backing_store_struct* %b_s_info29, i8* %66, i32 %67, i32 %68)
  br label %if.end32

if.end32:                                         ; preds = %if.else, %if.then26
  %69 = load i32, i32* %byte_count, align 4, !tbaa !9
  %70 = load i32, i32* %file_offset, align 4, !tbaa !9
  %add33 = add nsw i32 %70, %69
  store i32 %add33, i32* %file_offset, align 4, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %71 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %ptr.addr, align 4, !tbaa !2
  %rowsperchunk34 = getelementptr inbounds %struct.jvirt_barray_control, %struct.jvirt_barray_control* %71, i32 0, i32 5
  %72 = load i32, i32* %rowsperchunk34, align 4, !tbaa !67
  %73 = load i32, i32* %i, align 4, !tbaa !9
  %add35 = add i32 %73, %72
  store i32 %add35, i32* %i, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i32* %thisrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i32* %byte_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i32* %file_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i32* %bytesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  ret void
}

declare void @jpeg_free_large(%struct.jpeg_common_struct*, i8*, i32) #2

declare void @jpeg_free_small(%struct.jpeg_common_struct*, i8*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_common_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"long", !4, i64 0}
!11 = !{!7, !3, i64 0}
!12 = !{!13, !8, i64 20}
!13 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !10, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!14 = !{!13, !3, i64 0}
!15 = !{!4, !4, i64 0}
!16 = !{!17, !3, i64 0}
!17 = !{!"", !18, i64 0, !4, i64 52, !4, i64 60, !3, i64 68, !3, i64 72, !10, i64 76, !8, i64 80}
!18 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !10, i64 44, !10, i64 48}
!19 = !{!17, !3, i64 4}
!20 = !{!17, !3, i64 8}
!21 = !{!17, !3, i64 12}
!22 = !{!17, !3, i64 16}
!23 = !{!17, !3, i64 20}
!24 = !{!17, !3, i64 24}
!25 = !{!17, !3, i64 28}
!26 = !{!17, !3, i64 32}
!27 = !{!17, !3, i64 36}
!28 = !{!17, !3, i64 40}
!29 = !{!17, !10, i64 48}
!30 = !{!17, !10, i64 44}
!31 = !{!8, !8, i64 0}
!32 = !{!17, !3, i64 68}
!33 = !{!17, !3, i64 72}
!34 = !{!17, !10, i64 76}
!35 = !{!36, !10, i64 8}
!36 = !{!"small_pool_struct", !3, i64 0, !10, i64 4, !10, i64 8}
!37 = !{!36, !3, i64 0}
!38 = !{!36, !10, i64 4}
!39 = !{!40, !3, i64 0}
!40 = !{!"large_pool_struct", !3, i64 0, !10, i64 4, !10, i64 8}
!41 = !{!40, !10, i64 4}
!42 = !{!40, !10, i64 8}
!43 = !{!17, !8, i64 80}
!44 = !{!45, !3, i64 0}
!45 = !{!"jvirt_sarray_control", !3, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !3, i64 44, !46, i64 48}
!46 = !{!"backing_store_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !4, i64 16}
!47 = !{!45, !8, i64 4}
!48 = !{!45, !8, i64 8}
!49 = !{!45, !8, i64 12}
!50 = !{!45, !8, i64 32}
!51 = !{!45, !8, i64 40}
!52 = !{!45, !3, i64 44}
!53 = !{!54, !3, i64 0}
!54 = !{!"jvirt_barray_control", !3, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !3, i64 44, !46, i64 48}
!55 = !{!54, !8, i64 4}
!56 = !{!54, !8, i64 8}
!57 = !{!54, !8, i64 12}
!58 = !{!54, !8, i64 32}
!59 = !{!54, !8, i64 40}
!60 = !{!54, !3, i64 44}
!61 = !{!45, !8, i64 16}
!62 = !{!45, !8, i64 20}
!63 = !{!45, !8, i64 24}
!64 = !{!45, !8, i64 28}
!65 = !{!45, !8, i64 36}
!66 = !{!54, !8, i64 16}
!67 = !{!54, !8, i64 20}
!68 = !{!54, !8, i64 24}
!69 = !{!54, !8, i64 28}
!70 = !{!54, !8, i64 36}
!71 = !{!45, !3, i64 56}
!72 = !{!54, !3, i64 56}
!73 = !{!45, !3, i64 52}
!74 = !{!45, !3, i64 48}
!75 = !{!54, !3, i64 52}
!76 = !{!54, !3, i64 48}
