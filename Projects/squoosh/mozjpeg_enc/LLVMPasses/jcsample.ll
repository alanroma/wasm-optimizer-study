; ModuleID = 'jcsample.c'
source_filename = "jcsample.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_compress_struct*)*, {}* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { {}*, {}*, {}*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { {}*, {}*, {}*, {}*, {}*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { {}*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { {}*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { {}*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, {}* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_downsampler = type { %struct.jpeg_downsampler, [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*] }

; Function Attrs: nounwind
define hidden void @jinit_downsampler(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %downsample = alloca %struct.my_downsampler*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %smoothok = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_downsampler** %downsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %smoothok to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 1, i32* %smoothok, align 4, !tbaa !6
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 1
  %5 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %5, i32 0, i32 0
  %6 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = bitcast %struct.jpeg_compress_struct* %7 to %struct.jpeg_common_struct*
  %call = call i8* %6(%struct.jpeg_common_struct* %8, i32 1, i32 52)
  %9 = bitcast i8* %call to %struct.my_downsampler*
  store %struct.my_downsampler* %9, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %10 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %11 = bitcast %struct.my_downsampler* %10 to %struct.jpeg_downsampler*
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 60
  store %struct.jpeg_downsampler* %11, %struct.jpeg_downsampler** %downsample1, align 4, !tbaa !15
  %13 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %13, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @start_pass_downsample, void (%struct.jpeg_compress_struct*)** %start_pass2, align 4, !tbaa !16
  %14 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %14, i32 0, i32 0
  %downsample4 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %pub3, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)* @sep_downsample, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)** %downsample4, align 4, !tbaa !19
  %15 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %15, i32 0, i32 0
  %need_context_rows = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %pub5, i32 0, i32 2
  store i32 0, i32* %need_context_rows, align 4, !tbaa !20
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 27
  %17 = load i32, i32* %CCIR601_sampling, align 4, !tbaa !21
  %tobool = icmp ne i32 %17, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !22
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 25, i32* %msg_code, align 4, !tbaa !23
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !22
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 0
  %22 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_compress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %ci, align 4, !tbaa !6
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 15
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !26
  store %struct.jpeg_component_info* %26, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %27 = load i32, i32* %ci, align 4, !tbaa !6
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 13
  %29 = load i32, i32* %num_components, align 4, !tbaa !27
  %cmp = icmp slt i32 %27, %29
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %30, i32 0, i32 2
  %31 = load i32, i32* %h_samp_factor, align 4, !tbaa !28
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 41
  %33 = load i32, i32* %max_h_samp_factor, align 8, !tbaa !30
  %cmp7 = icmp eq i32 %31, %33
  br i1 %cmp7, label %land.lhs.true, label %if.else17

land.lhs.true:                                    ; preds = %for.body
  %34 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %34, i32 0, i32 3
  %35 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 42
  %37 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %cmp8 = icmp eq i32 %35, %37
  br i1 %cmp8, label %if.then9, label %if.else17

if.then9:                                         ; preds = %land.lhs.true
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 28
  %39 = load i32, i32* %smoothing_factor, align 8, !tbaa !33
  %tobool10 = icmp ne i32 %39, 0
  br i1 %tobool10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.then9
  %40 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %40, i32 0, i32 1
  %41 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods, i32 0, i32 %41
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @fullsize_smooth_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx, align 4, !tbaa !2
  %42 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %42, i32 0, i32 0
  %need_context_rows13 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %pub12, i32 0, i32 2
  store i32 1, i32* %need_context_rows13, align 4, !tbaa !20
  br label %if.end16

if.else:                                          ; preds = %if.then9
  %43 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods14 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %43, i32 0, i32 1
  %44 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods14, i32 0, i32 %44
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @fullsize_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx15, align 4, !tbaa !2
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then11
  br label %if.end84

if.else17:                                        ; preds = %land.lhs.true, %for.body
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor18 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 0, i32 2
  %46 = load i32, i32* %h_samp_factor18, align 4, !tbaa !28
  %mul = mul nsw i32 %46, 2
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 41
  %48 = load i32, i32* %max_h_samp_factor19, align 8, !tbaa !30
  %cmp20 = icmp eq i32 %mul, %48
  br i1 %cmp20, label %land.lhs.true21, label %if.else35

land.lhs.true21:                                  ; preds = %if.else17
  %49 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor22 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %49, i32 0, i32 3
  %50 = load i32, i32* %v_samp_factor22, align 4, !tbaa !31
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 42
  %52 = load i32, i32* %max_v_samp_factor23, align 4, !tbaa !32
  %cmp24 = icmp eq i32 %50, %52
  br i1 %cmp24, label %if.then25, label %if.else35

if.then25:                                        ; preds = %land.lhs.true21
  store i32 0, i32* %smoothok, align 4, !tbaa !6
  %call26 = call i32 @jsimd_can_h2v1_downsample()
  %tobool27 = icmp ne i32 %call26, 0
  br i1 %tobool27, label %if.then28, label %if.else31

if.then28:                                        ; preds = %if.then25
  %53 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods29 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %53, i32 0, i32 1
  %54 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods29, i32 0, i32 %54
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @jsimd_h2v1_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx30, align 4, !tbaa !2
  br label %if.end34

if.else31:                                        ; preds = %if.then25
  %55 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods32 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %55, i32 0, i32 1
  %56 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods32, i32 0, i32 %56
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @h2v1_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx33, align 4, !tbaa !2
  br label %if.end34

if.end34:                                         ; preds = %if.else31, %if.then28
  br label %if.end83

if.else35:                                        ; preds = %land.lhs.true21, %if.else17
  %57 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %57, i32 0, i32 2
  %58 = load i32, i32* %h_samp_factor36, align 4, !tbaa !28
  %mul37 = mul nsw i32 %58, 2
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 41
  %60 = load i32, i32* %max_h_samp_factor38, align 8, !tbaa !30
  %cmp39 = icmp eq i32 %mul37, %60
  br i1 %cmp39, label %land.lhs.true40, label %if.else64

land.lhs.true40:                                  ; preds = %if.else35
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor41 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %61, i32 0, i32 3
  %62 = load i32, i32* %v_samp_factor41, align 4, !tbaa !31
  %mul42 = mul nsw i32 %62, 2
  %63 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor43 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %63, i32 0, i32 42
  %64 = load i32, i32* %max_v_samp_factor43, align 4, !tbaa !32
  %cmp44 = icmp eq i32 %mul42, %64
  br i1 %cmp44, label %if.then45, label %if.else64

if.then45:                                        ; preds = %land.lhs.true40
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 28
  %66 = load i32, i32* %smoothing_factor46, align 8, !tbaa !33
  %tobool47 = icmp ne i32 %66, 0
  br i1 %tobool47, label %if.then48, label %if.else53

if.then48:                                        ; preds = %if.then45
  %67 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods49 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %67, i32 0, i32 1
  %68 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods49, i32 0, i32 %68
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @h2v2_smooth_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx50, align 4, !tbaa !2
  %69 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %pub51 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %69, i32 0, i32 0
  %need_context_rows52 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %pub51, i32 0, i32 2
  store i32 1, i32* %need_context_rows52, align 4, !tbaa !20
  br label %if.end63

if.else53:                                        ; preds = %if.then45
  %call54 = call i32 @jsimd_can_h2v2_downsample()
  %tobool55 = icmp ne i32 %call54, 0
  br i1 %tobool55, label %if.then56, label %if.else59

if.then56:                                        ; preds = %if.else53
  %70 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods57 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %70, i32 0, i32 1
  %71 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods57, i32 0, i32 %71
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @jsimd_h2v2_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx58, align 4, !tbaa !2
  br label %if.end62

if.else59:                                        ; preds = %if.else53
  %72 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods60 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %72, i32 0, i32 1
  %73 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods60, i32 0, i32 %73
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @h2v2_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx61, align 4, !tbaa !2
  br label %if.end62

if.end62:                                         ; preds = %if.else59, %if.then56
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %if.then48
  br label %if.end82

if.else64:                                        ; preds = %land.lhs.true40, %if.else35
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor65 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 41
  %75 = load i32, i32* %max_h_samp_factor65, align 8, !tbaa !30
  %76 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor66 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %76, i32 0, i32 2
  %77 = load i32, i32* %h_samp_factor66, align 4, !tbaa !28
  %rem = srem i32 %75, %77
  %cmp67 = icmp eq i32 %rem, 0
  br i1 %cmp67, label %land.lhs.true68, label %if.else76

land.lhs.true68:                                  ; preds = %if.else64
  %78 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor69 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %78, i32 0, i32 42
  %79 = load i32, i32* %max_v_samp_factor69, align 4, !tbaa !32
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor70 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 3
  %81 = load i32, i32* %v_samp_factor70, align 4, !tbaa !31
  %rem71 = srem i32 %79, %81
  %cmp72 = icmp eq i32 %rem71, 0
  br i1 %cmp72, label %if.then73, label %if.else76

if.then73:                                        ; preds = %land.lhs.true68
  store i32 0, i32* %smoothok, align 4, !tbaa !6
  %82 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods74 = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %82, i32 0, i32 1
  %83 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods74, i32 0, i32 %83
  store void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)* @int_downsample, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx75, align 4, !tbaa !2
  br label %if.end81

if.else76:                                        ; preds = %land.lhs.true68, %if.else64
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err77 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %84, i32 0, i32 0
  %85 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err77, align 8, !tbaa !22
  %msg_code78 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %85, i32 0, i32 5
  store i32 38, i32* %msg_code78, align 4, !tbaa !23
  %86 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err79 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %86, i32 0, i32 0
  %87 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err79, align 8, !tbaa !22
  %error_exit80 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %87, i32 0, i32 0
  %88 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit80, align 4, !tbaa !25
  %89 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %90 = bitcast %struct.jpeg_compress_struct* %89 to %struct.jpeg_common_struct*
  call void %88(%struct.jpeg_common_struct* %90)
  br label %if.end81

if.end81:                                         ; preds = %if.else76, %if.then73
  br label %if.end82

if.end82:                                         ; preds = %if.end81, %if.end63
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %if.end34
  br label %if.end84

if.end84:                                         ; preds = %if.end83, %if.end16
  br label %for.inc

for.inc:                                          ; preds = %if.end84
  %91 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %91, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %92 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %92, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %93 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor85 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %93, i32 0, i32 28
  %94 = load i32, i32* %smoothing_factor85, align 8, !tbaa !33
  %tobool86 = icmp ne i32 %94, 0
  br i1 %tobool86, label %land.lhs.true87, label %if.end93

land.lhs.true87:                                  ; preds = %for.end
  %95 = load i32, i32* %smoothok, align 4, !tbaa !6
  %tobool88 = icmp ne i32 %95, 0
  br i1 %tobool88, label %if.end93, label %if.then89

if.then89:                                        ; preds = %land.lhs.true87
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err90 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %96, i32 0, i32 0
  %97 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err90, align 8, !tbaa !22
  %msg_code91 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %97, i32 0, i32 5
  store i32 99, i32* %msg_code91, align 4, !tbaa !23
  %98 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err92 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %98, i32 0, i32 0
  %99 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err92, align 8, !tbaa !22
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %99, i32 0, i32 1
  %100 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !34
  %101 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %102 = bitcast %struct.jpeg_compress_struct* %101 to %struct.jpeg_common_struct*
  call void %100(%struct.jpeg_common_struct* %102, i32 0)
  br label %if.end93

if.end93:                                         ; preds = %if.then89, %land.lhs.true87, %for.end
  %103 = bitcast i32* %smoothok to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #3
  %104 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  %105 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast %struct.my_downsampler** %downsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_downsample(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @sep_downsample(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf, i32 %in_row_index, i8*** %output_buf, i32 %out_row_group_index) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_index.addr = alloca i32, align 4
  %output_buf.addr = alloca i8***, align 4
  %out_row_group_index.addr = alloca i32, align 4
  %downsample = alloca %struct.my_downsampler*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %in_ptr = alloca i8**, align 4
  %out_ptr = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_index, i32* %in_row_index.addr, align 4, !tbaa !6
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %out_row_group_index, i32* %out_row_group_index.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_downsampler** %downsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 60
  %2 = load %struct.jpeg_downsampler*, %struct.jpeg_downsampler** %downsample1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_downsampler* %2 to %struct.my_downsampler*
  store %struct.my_downsampler* %3, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8*** %in_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8*** %out_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 15
  %9 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !26
  store %struct.jpeg_component_info* %9, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %ci, align 4, !tbaa !6
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 13
  %12 = load i32, i32* %num_components, align 4, !tbaa !27
  %cmp = icmp slt i32 %10, %12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8**, i8*** %13, i32 %14
  %15 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %16 = load i32, i32* %in_row_index.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8*, i8** %15, i32 %16
  store i8** %add.ptr, i8*** %in_ptr, align 4, !tbaa !2
  %17 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %18 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8**, i8*** %17, i32 %18
  %19 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %20 = load i32, i32* %out_row_group_index.addr, align 4, !tbaa !6
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 0, i32 3
  %22 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %mul = mul i32 %20, %22
  %add.ptr3 = getelementptr inbounds i8*, i8** %19, i32 %mul
  store i8** %add.ptr3, i8*** %out_ptr, align 4, !tbaa !2
  %23 = load %struct.my_downsampler*, %struct.my_downsampler** %downsample, align 4, !tbaa !2
  %methods = getelementptr inbounds %struct.my_downsampler, %struct.my_downsampler* %23, i32 0, i32 1
  %24 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*], [10 x void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*]* %methods, i32 0, i32 %24
  %25 = load void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**)** %arrayidx4, align 4, !tbaa !2
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %28 = load i8**, i8*** %in_ptr, align 4, !tbaa !2
  %29 = load i8**, i8*** %out_ptr, align 4, !tbaa !2
  call void %25(%struct.jpeg_compress_struct* %26, %struct.jpeg_component_info* %27, i8** %28, i8** %29)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %32 = bitcast i8*** %out_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i8*** %in_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  %34 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  %36 = bitcast %struct.my_downsampler** %downsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  ret void
}

; Function Attrs: nounwind
define internal void @fullsize_smooth_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  %outrow = alloca i32, align 4
  %colctr = alloca i32, align 4
  %output_cols = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %above_ptr = alloca i8*, align 4
  %below_ptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %membersum = alloca i32, align 4
  %neighsum = alloca i32, align 4
  %memberscale = alloca i32, align 4
  %neighscale = alloca i32, align 4
  %colsum = alloca i32, align 4
  %lastcolsum = alloca i32, align 4
  %nextcolsum = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %3, i32 0, i32 7
  %4 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %4, 8
  store i32 %mul, i32* %output_cols, align 4, !tbaa !6
  %5 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %above_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %below_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %membersum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %neighsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %memberscale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %neighscale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %colsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %lastcolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %nextcolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8*, i8** %16, i32 -1
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 42
  %18 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %add = add nsw i32 %18, 2
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 7
  %20 = load i32, i32* %image_width, align 4, !tbaa !36
  %21 = load i32, i32* %output_cols, align 4, !tbaa !6
  call void @expand_right_edge(i8** %add.ptr, i32 %add, i32 %20, i32 %21)
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 28
  %23 = load i32, i32* %smoothing_factor, align 8, !tbaa !33
  %mul1 = mul nsw i32 %23, 512
  %sub = sub nsw i32 65536, %mul1
  store i32 %sub, i32* %memberscale, align 4, !tbaa !37
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 28
  %25 = load i32, i32* %smoothing_factor2, align 8, !tbaa !33
  %mul3 = mul nsw i32 %25, 64
  store i32 %mul3, i32* %neighscale, align 4, !tbaa !37
  store i32 0, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc64, %entry
  %26 = load i32, i32* %outrow, align 4, !tbaa !6
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 3
  %28 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %cmp = icmp slt i32 %26, %28
  br i1 %cmp, label %for.body, label %for.end65

for.body:                                         ; preds = %for.cond
  %29 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %30 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %31, i8** %outptr, align 4, !tbaa !2
  %32 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %33 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %34, i8** %inptr, align 4, !tbaa !2
  %35 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %36 = load i32, i32* %outrow, align 4, !tbaa !6
  %sub5 = sub nsw i32 %36, 1
  %arrayidx6 = getelementptr inbounds i8*, i8** %35, i32 %sub5
  %37 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %37, i8** %above_ptr, align 4, !tbaa !2
  %38 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %39 = load i32, i32* %outrow, align 4, !tbaa !6
  %add7 = add nsw i32 %39, 1
  %arrayidx8 = getelementptr inbounds i8*, i8** %38, i32 %add7
  %40 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %40, i8** %below_ptr, align 4, !tbaa !2
  %41 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr, i8** %above_ptr, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !38
  %conv = zext i8 %42 to i32
  %43 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %43, i32 1
  store i8* %incdec.ptr9, i8** %below_ptr, align 4, !tbaa !2
  %44 = load i8, i8* %43, align 1, !tbaa !38
  %conv10 = zext i8 %44 to i32
  %add11 = add nsw i32 %conv, %conv10
  %45 = load i8*, i8** %inptr, align 4, !tbaa !2
  %46 = load i8, i8* %45, align 1, !tbaa !38
  %conv12 = zext i8 %46 to i32
  %add13 = add nsw i32 %add11, %conv12
  store i32 %add13, i32* %colsum, align 4, !tbaa !6
  %47 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr14, i8** %inptr, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !38
  %conv15 = zext i8 %48 to i32
  store i32 %conv15, i32* %membersum, align 4, !tbaa !37
  %49 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !38
  %conv16 = zext i8 %50 to i32
  %51 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !38
  %conv17 = zext i8 %52 to i32
  %add18 = add nsw i32 %conv16, %conv17
  %53 = load i8*, i8** %inptr, align 4, !tbaa !2
  %54 = load i8, i8* %53, align 1, !tbaa !38
  %conv19 = zext i8 %54 to i32
  %add20 = add nsw i32 %add18, %conv19
  store i32 %add20, i32* %nextcolsum, align 4, !tbaa !6
  %55 = load i32, i32* %colsum, align 4, !tbaa !6
  %56 = load i32, i32* %colsum, align 4, !tbaa !6
  %57 = load i32, i32* %membersum, align 4, !tbaa !37
  %sub21 = sub nsw i32 %56, %57
  %add22 = add nsw i32 %55, %sub21
  %58 = load i32, i32* %nextcolsum, align 4, !tbaa !6
  %add23 = add nsw i32 %add22, %58
  store i32 %add23, i32* %neighsum, align 4, !tbaa !37
  %59 = load i32, i32* %membersum, align 4, !tbaa !37
  %60 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul24 = mul nsw i32 %59, %60
  %61 = load i32, i32* %neighsum, align 4, !tbaa !37
  %62 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul25 = mul nsw i32 %61, %62
  %add26 = add nsw i32 %mul24, %mul25
  store i32 %add26, i32* %membersum, align 4, !tbaa !37
  %63 = load i32, i32* %membersum, align 4, !tbaa !37
  %add27 = add nsw i32 %63, 32768
  %shr = ashr i32 %add27, 16
  %conv28 = trunc i32 %shr to i8
  %64 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr29 = getelementptr inbounds i8, i8* %64, i32 1
  store i8* %incdec.ptr29, i8** %outptr, align 4, !tbaa !2
  store i8 %conv28, i8* %64, align 1, !tbaa !38
  %65 = load i32, i32* %colsum, align 4, !tbaa !6
  store i32 %65, i32* %lastcolsum, align 4, !tbaa !6
  %66 = load i32, i32* %nextcolsum, align 4, !tbaa !6
  store i32 %66, i32* %colsum, align 4, !tbaa !6
  %67 = load i32, i32* %output_cols, align 4, !tbaa !6
  %sub30 = sub i32 %67, 2
  store i32 %sub30, i32* %colctr, align 4, !tbaa !6
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc, %for.body
  %68 = load i32, i32* %colctr, align 4, !tbaa !6
  %cmp32 = icmp ugt i32 %68, 0
  br i1 %cmp32, label %for.body34, label %for.end

for.body34:                                       ; preds = %for.cond31
  %69 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr35 = getelementptr inbounds i8, i8* %69, i32 1
  store i8* %incdec.ptr35, i8** %inptr, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !38
  %conv36 = zext i8 %70 to i32
  store i32 %conv36, i32* %membersum, align 4, !tbaa !37
  %71 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr37, i8** %above_ptr, align 4, !tbaa !2
  %72 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %72, i32 1
  store i8* %incdec.ptr38, i8** %below_ptr, align 4, !tbaa !2
  %73 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !38
  %conv39 = zext i8 %74 to i32
  %75 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %76 = load i8, i8* %75, align 1, !tbaa !38
  %conv40 = zext i8 %76 to i32
  %add41 = add nsw i32 %conv39, %conv40
  %77 = load i8*, i8** %inptr, align 4, !tbaa !2
  %78 = load i8, i8* %77, align 1, !tbaa !38
  %conv42 = zext i8 %78 to i32
  %add43 = add nsw i32 %add41, %conv42
  store i32 %add43, i32* %nextcolsum, align 4, !tbaa !6
  %79 = load i32, i32* %lastcolsum, align 4, !tbaa !6
  %80 = load i32, i32* %colsum, align 4, !tbaa !6
  %81 = load i32, i32* %membersum, align 4, !tbaa !37
  %sub44 = sub nsw i32 %80, %81
  %add45 = add nsw i32 %79, %sub44
  %82 = load i32, i32* %nextcolsum, align 4, !tbaa !6
  %add46 = add nsw i32 %add45, %82
  store i32 %add46, i32* %neighsum, align 4, !tbaa !37
  %83 = load i32, i32* %membersum, align 4, !tbaa !37
  %84 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul47 = mul nsw i32 %83, %84
  %85 = load i32, i32* %neighsum, align 4, !tbaa !37
  %86 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul48 = mul nsw i32 %85, %86
  %add49 = add nsw i32 %mul47, %mul48
  store i32 %add49, i32* %membersum, align 4, !tbaa !37
  %87 = load i32, i32* %membersum, align 4, !tbaa !37
  %add50 = add nsw i32 %87, 32768
  %shr51 = ashr i32 %add50, 16
  %conv52 = trunc i32 %shr51 to i8
  %88 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr53 = getelementptr inbounds i8, i8* %88, i32 1
  store i8* %incdec.ptr53, i8** %outptr, align 4, !tbaa !2
  store i8 %conv52, i8* %88, align 1, !tbaa !38
  %89 = load i32, i32* %colsum, align 4, !tbaa !6
  store i32 %89, i32* %lastcolsum, align 4, !tbaa !6
  %90 = load i32, i32* %nextcolsum, align 4, !tbaa !6
  store i32 %90, i32* %colsum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body34
  %91 = load i32, i32* %colctr, align 4, !tbaa !6
  %dec = add i32 %91, -1
  store i32 %dec, i32* %colctr, align 4, !tbaa !6
  br label %for.cond31

for.end:                                          ; preds = %for.cond31
  %92 = load i8*, i8** %inptr, align 4, !tbaa !2
  %93 = load i8, i8* %92, align 1, !tbaa !38
  %conv54 = zext i8 %93 to i32
  store i32 %conv54, i32* %membersum, align 4, !tbaa !37
  %94 = load i32, i32* %lastcolsum, align 4, !tbaa !6
  %95 = load i32, i32* %colsum, align 4, !tbaa !6
  %96 = load i32, i32* %membersum, align 4, !tbaa !37
  %sub55 = sub nsw i32 %95, %96
  %add56 = add nsw i32 %94, %sub55
  %97 = load i32, i32* %colsum, align 4, !tbaa !6
  %add57 = add nsw i32 %add56, %97
  store i32 %add57, i32* %neighsum, align 4, !tbaa !37
  %98 = load i32, i32* %membersum, align 4, !tbaa !37
  %99 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul58 = mul nsw i32 %98, %99
  %100 = load i32, i32* %neighsum, align 4, !tbaa !37
  %101 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul59 = mul nsw i32 %100, %101
  %add60 = add nsw i32 %mul58, %mul59
  store i32 %add60, i32* %membersum, align 4, !tbaa !37
  %102 = load i32, i32* %membersum, align 4, !tbaa !37
  %add61 = add nsw i32 %102, 32768
  %shr62 = ashr i32 %add61, 16
  %conv63 = trunc i32 %shr62 to i8
  %103 = load i8*, i8** %outptr, align 4, !tbaa !2
  store i8 %conv63, i8* %103, align 1, !tbaa !38
  br label %for.inc64

for.inc64:                                        ; preds = %for.end
  %104 = load i32, i32* %outrow, align 4, !tbaa !6
  %inc = add nsw i32 %104, 1
  store i32 %inc, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.end65:                                        ; preds = %for.cond
  %105 = bitcast i32* %nextcolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast i32* %lastcolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast i32* %colsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast i32* %neighscale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %memberscale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %neighsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast i32* %membersum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast i8** %below_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  %114 = bitcast i8** %above_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #3
  %115 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  ret void
}

; Function Attrs: nounwind
define internal void @fullsize_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %1 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 42
  %3 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 7
  %5 = load i32, i32* %image_width, align 4, !tbaa !36
  call void @jcopy_sample_rows(i8** %0, i32 0, i8** %1, i32 0, i32 %3, i32 %5)
  %6 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 42
  %8 = load i32, i32* %max_v_samp_factor1, align 4, !tbaa !32
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 7
  %10 = load i32, i32* %image_width2, align 4, !tbaa !36
  %11 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %11, i32 0, i32 7
  %12 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %12, 8
  call void @expand_right_edge(i8** %6, i32 %8, i32 %10, i32 %mul)
  ret void
}

declare i32 @jsimd_can_h2v1_downsample() #2

declare void @jsimd_h2v1_downsample(%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**) #2

; Function Attrs: nounwind
define internal void @h2v1_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  %outrow = alloca i32, align 4
  %outcol = alloca i32, align 4
  %output_cols = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %bias = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %3, i32 0, i32 7
  %4 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %4, 8
  store i32 %mul, i32* %output_cols, align 4, !tbaa !6
  %5 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %bias to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 42
  %10 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 7
  %12 = load i32, i32* %image_width, align 4, !tbaa !36
  %13 = load i32, i32* %output_cols, align 4, !tbaa !6
  %mul1 = mul i32 %13, 2
  call void @expand_right_edge(i8** %8, i32 %10, i32 %12, i32 %mul1)
  store i32 0, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %14 = load i32, i32* %outrow, align 4, !tbaa !6
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %15, i32 0, i32 3
  %16 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %cmp = icmp slt i32 %14, %16
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  %17 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %18 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %19, i8** %outptr, align 4, !tbaa !2
  %20 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %21 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %inptr, align 4, !tbaa !2
  store i32 0, i32* %bias, align 4, !tbaa !6
  store i32 0, i32* %outcol, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %23 = load i32, i32* %outcol, align 4, !tbaa !6
  %24 = load i32, i32* %output_cols, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %23, %24
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %25 = load i8*, i8** %inptr, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !38
  %conv = zext i8 %26 to i32
  %27 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %27, i32 1
  %28 = load i8, i8* %arrayidx6, align 1, !tbaa !38
  %conv7 = zext i8 %28 to i32
  %add = add nsw i32 %conv, %conv7
  %29 = load i32, i32* %bias, align 4, !tbaa !6
  %add8 = add nsw i32 %add, %29
  %shr = ashr i32 %add8, 1
  %conv9 = trunc i32 %shr to i8
  %30 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr, i8** %outptr, align 4, !tbaa !2
  store i8 %conv9, i8* %30, align 1, !tbaa !38
  %31 = load i32, i32* %bias, align 4, !tbaa !6
  %xor = xor i32 %31, 1
  store i32 %xor, i32* %bias, align 4, !tbaa !6
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 2
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %33 = load i32, i32* %outcol, align 4, !tbaa !6
  %inc = add i32 %33, 1
  store i32 %inc, i32* %outcol, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %34 = load i32, i32* %outrow, align 4, !tbaa !6
  %inc11 = add nsw i32 %34, 1
  store i32 %inc11, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  %35 = bitcast i32* %bias to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  %36 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  ret void
}

; Function Attrs: nounwind
define internal void @h2v2_smooth_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  %colctr = alloca i32, align 4
  %output_cols = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %above_ptr = alloca i8*, align 4
  %below_ptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %membersum = alloca i32, align 4
  %neighsum = alloca i32, align 4
  %memberscale = alloca i32, align 4
  %neighscale = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %4, i32 0, i32 7
  %5 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %5, 8
  store i32 %mul, i32* %output_cols, align 4, !tbaa !6
  %6 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i8** %above_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i8** %below_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %membersum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %neighsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %memberscale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %neighscale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8*, i8** %15, i32 -1
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 42
  %17 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %add = add nsw i32 %17, 2
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 7
  %19 = load i32, i32* %image_width, align 4, !tbaa !36
  %20 = load i32, i32* %output_cols, align 4, !tbaa !6
  %mul1 = mul i32 %20, 2
  call void @expand_right_edge(i8** %add.ptr, i32 %add, i32 %19, i32 %mul1)
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 28
  %22 = load i32, i32* %smoothing_factor, align 8, !tbaa !33
  %mul2 = mul nsw i32 %22, 80
  %sub = sub nsw i32 16384, %mul2
  store i32 %sub, i32* %memberscale, align 4, !tbaa !37
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %smoothing_factor3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 28
  %24 = load i32, i32* %smoothing_factor3, align 8, !tbaa !33
  %mul4 = mul nsw i32 %24, 16
  store i32 %mul4, i32* %neighscale, align 4, !tbaa !37
  store i32 0, i32* %inrow, align 4, !tbaa !6
  store i32 0, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc168, %entry
  %25 = load i32, i32* %outrow, align 4, !tbaa !6
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 3
  %27 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %cmp = icmp slt i32 %25, %27
  br i1 %cmp, label %for.body, label %for.end169

for.body:                                         ; preds = %for.cond
  %28 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %29 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %28, i32 %29
  %30 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %30, i8** %outptr, align 4, !tbaa !2
  %31 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %32 = load i32, i32* %inrow, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %35 = load i32, i32* %inrow, align 4, !tbaa !6
  %add6 = add nsw i32 %35, 1
  %arrayidx7 = getelementptr inbounds i8*, i8** %34, i32 %add6
  %36 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %36, i8** %inptr1, align 4, !tbaa !2
  %37 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %38 = load i32, i32* %inrow, align 4, !tbaa !6
  %sub8 = sub nsw i32 %38, 1
  %arrayidx9 = getelementptr inbounds i8*, i8** %37, i32 %sub8
  %39 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %39, i8** %above_ptr, align 4, !tbaa !2
  %40 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %41 = load i32, i32* %inrow, align 4, !tbaa !6
  %add10 = add nsw i32 %41, 2
  %arrayidx11 = getelementptr inbounds i8*, i8** %40, i32 %add10
  %42 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %42, i8** %below_ptr, align 4, !tbaa !2
  %43 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %44 = load i8, i8* %43, align 1, !tbaa !38
  %conv = zext i8 %44 to i32
  %45 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %45, i32 1
  %46 = load i8, i8* %arrayidx12, align 1, !tbaa !38
  %conv13 = zext i8 %46 to i32
  %add14 = add nsw i32 %conv, %conv13
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !38
  %conv15 = zext i8 %48 to i32
  %add16 = add nsw i32 %add14, %conv15
  %49 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i8, i8* %49, i32 1
  %50 = load i8, i8* %arrayidx17, align 1, !tbaa !38
  %conv18 = zext i8 %50 to i32
  %add19 = add nsw i32 %add16, %conv18
  store i32 %add19, i32* %membersum, align 4, !tbaa !37
  %51 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !38
  %conv20 = zext i8 %52 to i32
  %53 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %53, i32 1
  %54 = load i8, i8* %arrayidx21, align 1, !tbaa !38
  %conv22 = zext i8 %54 to i32
  %add23 = add nsw i32 %conv20, %conv22
  %55 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !38
  %conv24 = zext i8 %56 to i32
  %add25 = add nsw i32 %add23, %conv24
  %57 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %57, i32 1
  %58 = load i8, i8* %arrayidx26, align 1, !tbaa !38
  %conv27 = zext i8 %58 to i32
  %add28 = add nsw i32 %add25, %conv27
  %59 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !38
  %conv29 = zext i8 %60 to i32
  %add30 = add nsw i32 %add28, %conv29
  %61 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %61, i32 2
  %62 = load i8, i8* %arrayidx31, align 1, !tbaa !38
  %conv32 = zext i8 %62 to i32
  %add33 = add nsw i32 %add30, %conv32
  %63 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !38
  %conv34 = zext i8 %64 to i32
  %add35 = add nsw i32 %add33, %conv34
  %65 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %65, i32 2
  %66 = load i8, i8* %arrayidx36, align 1, !tbaa !38
  %conv37 = zext i8 %66 to i32
  %add38 = add nsw i32 %add35, %conv37
  store i32 %add38, i32* %neighsum, align 4, !tbaa !37
  %67 = load i32, i32* %neighsum, align 4, !tbaa !37
  %68 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add39 = add nsw i32 %68, %67
  store i32 %add39, i32* %neighsum, align 4, !tbaa !37
  %69 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !38
  %conv40 = zext i8 %70 to i32
  %71 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %71, i32 2
  %72 = load i8, i8* %arrayidx41, align 1, !tbaa !38
  %conv42 = zext i8 %72 to i32
  %add43 = add nsw i32 %conv40, %conv42
  %73 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !38
  %conv44 = zext i8 %74 to i32
  %add45 = add nsw i32 %add43, %conv44
  %75 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i8, i8* %75, i32 2
  %76 = load i8, i8* %arrayidx46, align 1, !tbaa !38
  %conv47 = zext i8 %76 to i32
  %add48 = add nsw i32 %add45, %conv47
  %77 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add49 = add nsw i32 %77, %add48
  store i32 %add49, i32* %neighsum, align 4, !tbaa !37
  %78 = load i32, i32* %membersum, align 4, !tbaa !37
  %79 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul50 = mul nsw i32 %78, %79
  %80 = load i32, i32* %neighsum, align 4, !tbaa !37
  %81 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul51 = mul nsw i32 %80, %81
  %add52 = add nsw i32 %mul50, %mul51
  store i32 %add52, i32* %membersum, align 4, !tbaa !37
  %82 = load i32, i32* %membersum, align 4, !tbaa !37
  %add53 = add nsw i32 %82, 32768
  %shr = ashr i32 %add53, 16
  %conv54 = trunc i32 %shr to i8
  %83 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %83, i32 1
  store i8* %incdec.ptr, i8** %outptr, align 4, !tbaa !2
  store i8 %conv54, i8* %83, align 1, !tbaa !38
  %84 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %add.ptr55 = getelementptr inbounds i8, i8* %84, i32 2
  store i8* %add.ptr55, i8** %inptr0, align 4, !tbaa !2
  %85 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %add.ptr56 = getelementptr inbounds i8, i8* %85, i32 2
  store i8* %add.ptr56, i8** %inptr1, align 4, !tbaa !2
  %86 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %86, i32 2
  store i8* %add.ptr57, i8** %above_ptr, align 4, !tbaa !2
  %87 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %add.ptr58 = getelementptr inbounds i8, i8* %87, i32 2
  store i8* %add.ptr58, i8** %below_ptr, align 4, !tbaa !2
  %88 = load i32, i32* %output_cols, align 4, !tbaa !6
  %sub59 = sub i32 %88, 2
  store i32 %sub59, i32* %colctr, align 4, !tbaa !6
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc, %for.body
  %89 = load i32, i32* %colctr, align 4, !tbaa !6
  %cmp61 = icmp ugt i32 %89, 0
  br i1 %cmp61, label %for.body63, label %for.end

for.body63:                                       ; preds = %for.cond60
  %90 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !38
  %conv64 = zext i8 %91 to i32
  %92 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %92, i32 1
  %93 = load i8, i8* %arrayidx65, align 1, !tbaa !38
  %conv66 = zext i8 %93 to i32
  %add67 = add nsw i32 %conv64, %conv66
  %94 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %95 = load i8, i8* %94, align 1, !tbaa !38
  %conv68 = zext i8 %95 to i32
  %add69 = add nsw i32 %add67, %conv68
  %96 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i8, i8* %96, i32 1
  %97 = load i8, i8* %arrayidx70, align 1, !tbaa !38
  %conv71 = zext i8 %97 to i32
  %add72 = add nsw i32 %add69, %conv71
  store i32 %add72, i32* %membersum, align 4, !tbaa !37
  %98 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %99 = load i8, i8* %98, align 1, !tbaa !38
  %conv73 = zext i8 %99 to i32
  %100 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i8, i8* %100, i32 1
  %101 = load i8, i8* %arrayidx74, align 1, !tbaa !38
  %conv75 = zext i8 %101 to i32
  %add76 = add nsw i32 %conv73, %conv75
  %102 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %103 = load i8, i8* %102, align 1, !tbaa !38
  %conv77 = zext i8 %103 to i32
  %add78 = add nsw i32 %add76, %conv77
  %104 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %104, i32 1
  %105 = load i8, i8* %arrayidx79, align 1, !tbaa !38
  %conv80 = zext i8 %105 to i32
  %add81 = add nsw i32 %add78, %conv80
  %106 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i8, i8* %106, i32 -1
  %107 = load i8, i8* %arrayidx82, align 1, !tbaa !38
  %conv83 = zext i8 %107 to i32
  %add84 = add nsw i32 %add81, %conv83
  %108 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i8, i8* %108, i32 2
  %109 = load i8, i8* %arrayidx85, align 1, !tbaa !38
  %conv86 = zext i8 %109 to i32
  %add87 = add nsw i32 %add84, %conv86
  %110 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i8, i8* %110, i32 -1
  %111 = load i8, i8* %arrayidx88, align 1, !tbaa !38
  %conv89 = zext i8 %111 to i32
  %add90 = add nsw i32 %add87, %conv89
  %112 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i8, i8* %112, i32 2
  %113 = load i8, i8* %arrayidx91, align 1, !tbaa !38
  %conv92 = zext i8 %113 to i32
  %add93 = add nsw i32 %add90, %conv92
  store i32 %add93, i32* %neighsum, align 4, !tbaa !37
  %114 = load i32, i32* %neighsum, align 4, !tbaa !37
  %115 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add94 = add nsw i32 %115, %114
  store i32 %add94, i32* %neighsum, align 4, !tbaa !37
  %116 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i8, i8* %116, i32 -1
  %117 = load i8, i8* %arrayidx95, align 1, !tbaa !38
  %conv96 = zext i8 %117 to i32
  %118 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i8, i8* %118, i32 2
  %119 = load i8, i8* %arrayidx97, align 1, !tbaa !38
  %conv98 = zext i8 %119 to i32
  %add99 = add nsw i32 %conv96, %conv98
  %120 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %120, i32 -1
  %121 = load i8, i8* %arrayidx100, align 1, !tbaa !38
  %conv101 = zext i8 %121 to i32
  %add102 = add nsw i32 %add99, %conv101
  %122 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i8, i8* %122, i32 2
  %123 = load i8, i8* %arrayidx103, align 1, !tbaa !38
  %conv104 = zext i8 %123 to i32
  %add105 = add nsw i32 %add102, %conv104
  %124 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add106 = add nsw i32 %124, %add105
  store i32 %add106, i32* %neighsum, align 4, !tbaa !37
  %125 = load i32, i32* %membersum, align 4, !tbaa !37
  %126 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul107 = mul nsw i32 %125, %126
  %127 = load i32, i32* %neighsum, align 4, !tbaa !37
  %128 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul108 = mul nsw i32 %127, %128
  %add109 = add nsw i32 %mul107, %mul108
  store i32 %add109, i32* %membersum, align 4, !tbaa !37
  %129 = load i32, i32* %membersum, align 4, !tbaa !37
  %add110 = add nsw i32 %129, 32768
  %shr111 = ashr i32 %add110, 16
  %conv112 = trunc i32 %shr111 to i8
  %130 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr113 = getelementptr inbounds i8, i8* %130, i32 1
  store i8* %incdec.ptr113, i8** %outptr, align 4, !tbaa !2
  store i8 %conv112, i8* %130, align 1, !tbaa !38
  %131 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %add.ptr114 = getelementptr inbounds i8, i8* %131, i32 2
  store i8* %add.ptr114, i8** %inptr0, align 4, !tbaa !2
  %132 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %add.ptr115 = getelementptr inbounds i8, i8* %132, i32 2
  store i8* %add.ptr115, i8** %inptr1, align 4, !tbaa !2
  %133 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %add.ptr116 = getelementptr inbounds i8, i8* %133, i32 2
  store i8* %add.ptr116, i8** %above_ptr, align 4, !tbaa !2
  %134 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %add.ptr117 = getelementptr inbounds i8, i8* %134, i32 2
  store i8* %add.ptr117, i8** %below_ptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body63
  %135 = load i32, i32* %colctr, align 4, !tbaa !6
  %dec = add i32 %135, -1
  store i32 %dec, i32* %colctr, align 4, !tbaa !6
  br label %for.cond60

for.end:                                          ; preds = %for.cond60
  %136 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %137 = load i8, i8* %136, align 1, !tbaa !38
  %conv118 = zext i8 %137 to i32
  %138 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i8, i8* %138, i32 1
  %139 = load i8, i8* %arrayidx119, align 1, !tbaa !38
  %conv120 = zext i8 %139 to i32
  %add121 = add nsw i32 %conv118, %conv120
  %140 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %141 = load i8, i8* %140, align 1, !tbaa !38
  %conv122 = zext i8 %141 to i32
  %add123 = add nsw i32 %add121, %conv122
  %142 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i8, i8* %142, i32 1
  %143 = load i8, i8* %arrayidx124, align 1, !tbaa !38
  %conv125 = zext i8 %143 to i32
  %add126 = add nsw i32 %add123, %conv125
  store i32 %add126, i32* %membersum, align 4, !tbaa !37
  %144 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %145 = load i8, i8* %144, align 1, !tbaa !38
  %conv127 = zext i8 %145 to i32
  %146 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i8, i8* %146, i32 1
  %147 = load i8, i8* %arrayidx128, align 1, !tbaa !38
  %conv129 = zext i8 %147 to i32
  %add130 = add nsw i32 %conv127, %conv129
  %148 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %149 = load i8, i8* %148, align 1, !tbaa !38
  %conv131 = zext i8 %149 to i32
  %add132 = add nsw i32 %add130, %conv131
  %150 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i8, i8* %150, i32 1
  %151 = load i8, i8* %arrayidx133, align 1, !tbaa !38
  %conv134 = zext i8 %151 to i32
  %add135 = add nsw i32 %add132, %conv134
  %152 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i8, i8* %152, i32 -1
  %153 = load i8, i8* %arrayidx136, align 1, !tbaa !38
  %conv137 = zext i8 %153 to i32
  %add138 = add nsw i32 %add135, %conv137
  %154 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i8, i8* %154, i32 1
  %155 = load i8, i8* %arrayidx139, align 1, !tbaa !38
  %conv140 = zext i8 %155 to i32
  %add141 = add nsw i32 %add138, %conv140
  %156 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i8, i8* %156, i32 -1
  %157 = load i8, i8* %arrayidx142, align 1, !tbaa !38
  %conv143 = zext i8 %157 to i32
  %add144 = add nsw i32 %add141, %conv143
  %158 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i8, i8* %158, i32 1
  %159 = load i8, i8* %arrayidx145, align 1, !tbaa !38
  %conv146 = zext i8 %159 to i32
  %add147 = add nsw i32 %add144, %conv146
  store i32 %add147, i32* %neighsum, align 4, !tbaa !37
  %160 = load i32, i32* %neighsum, align 4, !tbaa !37
  %161 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add148 = add nsw i32 %161, %160
  store i32 %add148, i32* %neighsum, align 4, !tbaa !37
  %162 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i8, i8* %162, i32 -1
  %163 = load i8, i8* %arrayidx149, align 1, !tbaa !38
  %conv150 = zext i8 %163 to i32
  %164 = load i8*, i8** %above_ptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i8, i8* %164, i32 1
  %165 = load i8, i8* %arrayidx151, align 1, !tbaa !38
  %conv152 = zext i8 %165 to i32
  %add153 = add nsw i32 %conv150, %conv152
  %166 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i8, i8* %166, i32 -1
  %167 = load i8, i8* %arrayidx154, align 1, !tbaa !38
  %conv155 = zext i8 %167 to i32
  %add156 = add nsw i32 %add153, %conv155
  %168 = load i8*, i8** %below_ptr, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i8, i8* %168, i32 1
  %169 = load i8, i8* %arrayidx157, align 1, !tbaa !38
  %conv158 = zext i8 %169 to i32
  %add159 = add nsw i32 %add156, %conv158
  %170 = load i32, i32* %neighsum, align 4, !tbaa !37
  %add160 = add nsw i32 %170, %add159
  store i32 %add160, i32* %neighsum, align 4, !tbaa !37
  %171 = load i32, i32* %membersum, align 4, !tbaa !37
  %172 = load i32, i32* %memberscale, align 4, !tbaa !37
  %mul161 = mul nsw i32 %171, %172
  %173 = load i32, i32* %neighsum, align 4, !tbaa !37
  %174 = load i32, i32* %neighscale, align 4, !tbaa !37
  %mul162 = mul nsw i32 %173, %174
  %add163 = add nsw i32 %mul161, %mul162
  store i32 %add163, i32* %membersum, align 4, !tbaa !37
  %175 = load i32, i32* %membersum, align 4, !tbaa !37
  %add164 = add nsw i32 %175, 32768
  %shr165 = ashr i32 %add164, 16
  %conv166 = trunc i32 %shr165 to i8
  %176 = load i8*, i8** %outptr, align 4, !tbaa !2
  store i8 %conv166, i8* %176, align 1, !tbaa !38
  %177 = load i32, i32* %inrow, align 4, !tbaa !6
  %add167 = add nsw i32 %177, 2
  store i32 %add167, i32* %inrow, align 4, !tbaa !6
  br label %for.inc168

for.inc168:                                       ; preds = %for.end
  %178 = load i32, i32* %outrow, align 4, !tbaa !6
  %inc = add nsw i32 %178, 1
  store i32 %inc, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.end169:                                       ; preds = %for.cond
  %179 = bitcast i32* %neighscale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #3
  %180 = bitcast i32* %memberscale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #3
  %181 = bitcast i32* %neighsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #3
  %182 = bitcast i32* %membersum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #3
  %183 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  %184 = bitcast i8** %below_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast i8** %above_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  %188 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  %189 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #3
  %190 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  %191 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #3
  ret void
}

declare i32 @jsimd_can_h2v2_downsample() #2

declare void @jsimd_h2v2_downsample(%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, i8**) #2

; Function Attrs: nounwind
define internal void @h2v2_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  %outcol = alloca i32, align 4
  %output_cols = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %bias = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %4, i32 0, i32 7
  %5 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %5, 8
  store i32 %mul, i32* %output_cols, align 4, !tbaa !6
  %6 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %bias to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 42
  %12 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 7
  %14 = load i32, i32* %image_width, align 4, !tbaa !36
  %15 = load i32, i32* %output_cols, align 4, !tbaa !6
  %mul1 = mul i32 %15, 2
  call void @expand_right_edge(i8** %10, i32 %12, i32 %14, i32 %mul1)
  store i32 0, i32* %inrow, align 4, !tbaa !6
  store i32 0, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc19, %entry
  %16 = load i32, i32* %outrow, align 4, !tbaa !6
  %17 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %17, i32 0, i32 3
  %18 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %cmp = icmp slt i32 %16, %18
  br i1 %cmp, label %for.body, label %for.end21

for.body:                                         ; preds = %for.cond
  %19 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %20 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %19, i32 %20
  %21 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %21, i8** %outptr, align 4, !tbaa !2
  %22 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %23 = load i32, i32* %inrow, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %24, i8** %inptr0, align 4, !tbaa !2
  %25 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %26 = load i32, i32* %inrow, align 4, !tbaa !6
  %add = add nsw i32 %26, 1
  %arrayidx3 = getelementptr inbounds i8*, i8** %25, i32 %add
  %27 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %27, i8** %inptr1, align 4, !tbaa !2
  store i32 1, i32* %bias, align 4, !tbaa !6
  store i32 0, i32* %outcol, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %28 = load i32, i32* %outcol, align 4, !tbaa !6
  %29 = load i32, i32* %output_cols, align 4, !tbaa !6
  %cmp5 = icmp ult i32 %28, %29
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %30 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !38
  %conv = zext i8 %31 to i32
  %32 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %32, i32 1
  %33 = load i8, i8* %arrayidx7, align 1, !tbaa !38
  %conv8 = zext i8 %33 to i32
  %add9 = add nsw i32 %conv, %conv8
  %34 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !38
  %conv10 = zext i8 %35 to i32
  %add11 = add nsw i32 %add9, %conv10
  %36 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %36, i32 1
  %37 = load i8, i8* %arrayidx12, align 1, !tbaa !38
  %conv13 = zext i8 %37 to i32
  %add14 = add nsw i32 %add11, %conv13
  %38 = load i32, i32* %bias, align 4, !tbaa !6
  %add15 = add nsw i32 %add14, %38
  %shr = ashr i32 %add15, 2
  %conv16 = trunc i32 %shr to i8
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %39, i32 1
  store i8* %incdec.ptr, i8** %outptr, align 4, !tbaa !2
  store i8 %conv16, i8* %39, align 1, !tbaa !38
  %40 = load i32, i32* %bias, align 4, !tbaa !6
  %xor = xor i32 %40, 3
  store i32 %xor, i32* %bias, align 4, !tbaa !6
  %41 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %41, i32 2
  store i8* %add.ptr, i8** %inptr0, align 4, !tbaa !2
  %42 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %add.ptr17 = getelementptr inbounds i8, i8* %42, i32 2
  store i8* %add.ptr17, i8** %inptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %43 = load i32, i32* %outcol, align 4, !tbaa !6
  %inc = add i32 %43, 1
  store i32 %inc, i32* %outcol, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %44 = load i32, i32* %inrow, align 4, !tbaa !6
  %add18 = add nsw i32 %44, 2
  store i32 %add18, i32* %inrow, align 4, !tbaa !6
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %45 = load i32, i32* %outrow, align 4, !tbaa !6
  %inc20 = add nsw i32 %45, 1
  store i32 %inc20, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.end21:                                        ; preds = %for.cond
  %46 = bitcast i32* %bias to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  ret void
}

; Function Attrs: nounwind
define internal void @int_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  %h_expand = alloca i32, align 4
  %v_expand = alloca i32, align 4
  %numpix = alloca i32, align 4
  %numpix2 = alloca i32, align 4
  %h = alloca i32, align 4
  %v = alloca i32, align 4
  %outcol = alloca i32, align 4
  %outcol_h = alloca i32, align 4
  %output_cols = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %outvalue = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %h_expand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %v_expand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %numpix to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %numpix2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %outcol_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %11, i32 0, i32 7
  %12 = load i32, i32* %width_in_blocks, align 4, !tbaa !35
  %mul = mul i32 %12, 8
  store i32 %mul, i32* %output_cols, align 4, !tbaa !6
  %13 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %outvalue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 41
  %17 = load i32, i32* %max_h_samp_factor, align 8, !tbaa !30
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 2
  %19 = load i32, i32* %h_samp_factor, align 4, !tbaa !28
  %div = sdiv i32 %17, %19
  store i32 %div, i32* %h_expand, align 4, !tbaa !6
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 42
  %21 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !32
  %22 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %22, i32 0, i32 3
  %23 = load i32, i32* %v_samp_factor, align 4, !tbaa !31
  %div1 = sdiv i32 %21, %23
  store i32 %div1, i32* %v_expand, align 4, !tbaa !6
  %24 = load i32, i32* %h_expand, align 4, !tbaa !6
  %25 = load i32, i32* %v_expand, align 4, !tbaa !6
  %mul2 = mul nsw i32 %24, %25
  store i32 %mul2, i32* %numpix, align 4, !tbaa !6
  %26 = load i32, i32* %numpix, align 4, !tbaa !6
  %div3 = sdiv i32 %26, 2
  store i32 %div3, i32* %numpix2, align 4, !tbaa !6
  %27 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 42
  %29 = load i32, i32* %max_v_samp_factor4, align 4, !tbaa !32
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 7
  %31 = load i32, i32* %image_width, align 4, !tbaa !36
  %32 = load i32, i32* %output_cols, align 4, !tbaa !6
  %33 = load i32, i32* %h_expand, align 4, !tbaa !6
  %mul5 = mul i32 %32, %33
  call void @expand_right_edge(i8** %27, i32 %29, i32 %31, i32 %mul5)
  store i32 0, i32* %inrow, align 4, !tbaa !6
  store i32 0, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc30, %entry
  %34 = load i32, i32* %outrow, align 4, !tbaa !6
  %35 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %v_samp_factor6 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %35, i32 0, i32 3
  %36 = load i32, i32* %v_samp_factor6, align 4, !tbaa !31
  %cmp = icmp slt i32 %34, %36
  br i1 %cmp, label %for.body, label %for.end32

for.body:                                         ; preds = %for.cond
  %37 = load i8**, i8*** %output_data.addr, align 4, !tbaa !2
  %38 = load i32, i32* %outrow, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %39, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %outcol, align 4, !tbaa !6
  store i32 0, i32* %outcol_h, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc25, %for.body
  %40 = load i32, i32* %outcol, align 4, !tbaa !6
  %41 = load i32, i32* %output_cols, align 4, !tbaa !6
  %cmp8 = icmp ult i32 %40, %41
  br i1 %cmp8, label %for.body9, label %for.end28

for.body9:                                        ; preds = %for.cond7
  store i32 0, i32* %outvalue, align 4, !tbaa !37
  store i32 0, i32* %v, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc18, %for.body9
  %42 = load i32, i32* %v, align 4, !tbaa !6
  %43 = load i32, i32* %v_expand, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %42, %43
  br i1 %cmp11, label %for.body12, label %for.end20

for.body12:                                       ; preds = %for.cond10
  %44 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %45 = load i32, i32* %inrow, align 4, !tbaa !6
  %46 = load i32, i32* %v, align 4, !tbaa !6
  %add = add nsw i32 %45, %46
  %arrayidx13 = getelementptr inbounds i8*, i8** %44, i32 %add
  %47 = load i8*, i8** %arrayidx13, align 4, !tbaa !2
  %48 = load i32, i32* %outcol_h, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %47, i32 %48
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  store i32 0, i32* %h, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body12
  %49 = load i32, i32* %h, align 4, !tbaa !6
  %50 = load i32, i32* %h_expand, align 4, !tbaa !6
  %cmp15 = icmp slt i32 %49, %50
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %51 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !38
  %conv = zext i8 %52 to i32
  %53 = load i32, i32* %outvalue, align 4, !tbaa !37
  %add17 = add nsw i32 %53, %conv
  store i32 %add17, i32* %outvalue, align 4, !tbaa !37
  br label %for.inc

for.inc:                                          ; preds = %for.body16
  %54 = load i32, i32* %h, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %h, align 4, !tbaa !6
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc18

for.inc18:                                        ; preds = %for.end
  %55 = load i32, i32* %v, align 4, !tbaa !6
  %inc19 = add nsw i32 %55, 1
  store i32 %inc19, i32* %v, align 4, !tbaa !6
  br label %for.cond10

for.end20:                                        ; preds = %for.cond10
  %56 = load i32, i32* %outvalue, align 4, !tbaa !37
  %57 = load i32, i32* %numpix2, align 4, !tbaa !6
  %add21 = add nsw i32 %56, %57
  %58 = load i32, i32* %numpix, align 4, !tbaa !6
  %div22 = sdiv i32 %add21, %58
  %conv23 = trunc i32 %div22 to i8
  %59 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr24, i8** %outptr, align 4, !tbaa !2
  store i8 %conv23, i8* %59, align 1, !tbaa !38
  br label %for.inc25

for.inc25:                                        ; preds = %for.end20
  %60 = load i32, i32* %outcol, align 4, !tbaa !6
  %inc26 = add i32 %60, 1
  store i32 %inc26, i32* %outcol, align 4, !tbaa !6
  %61 = load i32, i32* %h_expand, align 4, !tbaa !6
  %62 = load i32, i32* %outcol_h, align 4, !tbaa !6
  %add27 = add i32 %62, %61
  store i32 %add27, i32* %outcol_h, align 4, !tbaa !6
  br label %for.cond7

for.end28:                                        ; preds = %for.cond7
  %63 = load i32, i32* %v_expand, align 4, !tbaa !6
  %64 = load i32, i32* %inrow, align 4, !tbaa !6
  %add29 = add nsw i32 %64, %63
  store i32 %add29, i32* %inrow, align 4, !tbaa !6
  br label %for.inc30

for.inc30:                                        ; preds = %for.end28
  %65 = load i32, i32* %outrow, align 4, !tbaa !6
  %inc31 = add nsw i32 %65, 1
  store i32 %inc31, i32* %outrow, align 4, !tbaa !6
  br label %for.cond

for.end32:                                        ; preds = %for.cond
  %66 = bitcast i32* %outvalue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  %67 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %68 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast i32* %output_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %outcol_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %outcol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i32* %numpix2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i32* %numpix to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i32* %v_expand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i32* %h_expand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @expand_right_edge(i8** %image_data, i32 %num_rows, i32 %input_cols, i32 %output_cols) #0 {
entry:
  %image_data.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %input_cols.addr = alloca i32, align 4
  %output_cols.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %pixval = alloca i8, align 1
  %count = alloca i32, align 4
  %row = alloca i32, align 4
  %numcols = alloca i32, align 4
  store i8** %image_data, i8*** %image_data.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  store i32 %input_cols, i32* %input_cols.addr, align 4, !tbaa !6
  store i32 %output_cols, i32* %output_cols.addr, align 4, !tbaa !6
  %0 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %pixval) #3
  %1 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %numcols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i32, i32* %output_cols.addr, align 4, !tbaa !6
  %5 = load i32, i32* %input_cols.addr, align 4, !tbaa !6
  %sub = sub i32 %4, %5
  store i32 %sub, i32* %numcols, align 4, !tbaa !6
  %6 = load i32, i32* %numcols, align 4, !tbaa !6
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc6, %if.then
  %7 = load i32, i32* %row, align 4, !tbaa !6
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %7, %8
  br i1 %cmp1, label %for.body, label %for.end7

for.body:                                         ; preds = %for.cond
  %9 = load i8**, i8*** %image_data.addr, align 4, !tbaa !2
  %10 = load i32, i32* %row, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %9, i32 %10
  %11 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %12 = load i32, i32* %input_cols.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %12
  store i8* %add.ptr, i8** %ptr, align 4, !tbaa !2
  %13 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %13, i32 -1
  %14 = load i8, i8* %arrayidx2, align 1, !tbaa !38
  store i8 %14, i8* %pixval, align 1, !tbaa !38
  %15 = load i32, i32* %numcols, align 4, !tbaa !6
  store i32 %15, i32* %count, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %16 = load i32, i32* %count, align 4, !tbaa !6
  %cmp4 = icmp sgt i32 %16, 0
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %17 = load i8, i8* %pixval, align 1, !tbaa !38
  %18 = load i8*, i8** %ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %18, i32 1
  store i8* %incdec.ptr, i8** %ptr, align 4, !tbaa !2
  store i8 %17, i8* %18, align 1, !tbaa !38
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %19 = load i32, i32* %count, align 4, !tbaa !6
  %dec = add nsw i32 %19, -1
  store i32 %dec, i32* %count, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc6

for.inc6:                                         ; preds = %for.end
  %20 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end7:                                         ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end7, %entry
  %21 = bitcast i32* %numcols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %pixval) #3
  %24 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  ret void
}

declare void @jcopy_sample_rows(i8**, i32, i8**, i32, i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !10, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !11, i64 216, !11, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!9, !3, i64 356}
!16 = !{!17, !3, i64 0}
!17 = !{!"", !18, i64 0, !4, i64 12}
!18 = !{!"jpeg_downsampler", !3, i64 0, !3, i64 4, !7, i64 8}
!19 = !{!17, !3, i64 4}
!20 = !{!17, !7, i64 8}
!21 = !{!9, !7, i64 188}
!22 = !{!9, !3, i64 0}
!23 = !{!24, !7, i64 20}
!24 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !14, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!25 = !{!24, !3, i64 0}
!26 = !{!9, !3, i64 68}
!27 = !{!9, !7, i64 60}
!28 = !{!29, !7, i64 8}
!29 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!30 = !{!9, !7, i64 232}
!31 = !{!29, !7, i64 12}
!32 = !{!9, !7, i64 236}
!33 = !{!9, !7, i64 192}
!34 = !{!24, !3, i64 4}
!35 = !{!29, !7, i64 28}
!36 = !{!9, !7, i64 28}
!37 = !{!14, !14, i64 0}
!38 = !{!4, !4, i64 0}
