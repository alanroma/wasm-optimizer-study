; ModuleID = 'jdhuff.c'
source_filename = "jdhuff.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.d_derived_tbl = type { [18 x i32], [18 x i32], %struct.JHUFF_TBL*, [256 x i32] }
%struct.bitread_working_state = type { i8*, i32, i32, i32, %struct.jpeg_decompress_struct* }
%struct.huff_entropy_decoder = type { %struct.jpeg_entropy_decoder, %struct.bitread_perm_state, %struct.savable_state, i32, [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*], [10 x i32], [10 x i32] }
%struct.bitread_perm_state = type { i32, i32 }
%struct.savable_state = type { [4 x i32] }
%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }

@std_huff_tables.bits_dc_luminance = internal constant [17 x i8] c"\00\00\01\05\01\01\01\01\01\01\00\00\00\00\00\00\00", align 16
@std_huff_tables.val_dc_luminance = internal constant [12 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B", align 1
@std_huff_tables.bits_dc_chrominance = internal constant [17 x i8] c"\00\00\03\01\01\01\01\01\01\01\01\01\00\00\00\00\00", align 16
@std_huff_tables.val_dc_chrominance = internal constant [12 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B", align 1
@std_huff_tables.bits_ac_luminance = internal constant [17 x i8] c"\00\00\02\01\03\03\02\04\03\05\05\04\04\00\00\01}", align 16
@std_huff_tables.val_ac_luminance = internal constant [162 x i8] c"\01\02\03\00\04\11\05\12!1A\06\13Qa\07\22q\142\81\91\A1\08#B\B1\C1\15R\D1\F0$3br\82\09\0A\16\17\18\19\1A%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\83\84\85\86\87\88\89\8A\92\93\94\95\96\97\98\99\9A\A2\A3\A4\A5\A6\A7\A8\A9\AA\B2\B3\B4\B5\B6\B7\B8\B9\BA\C2\C3\C4\C5\C6\C7\C8\C9\CA\D2\D3\D4\D5\D6\D7\D8\D9\DA\E1\E2\E3\E4\E5\E6\E7\E8\E9\EA\F1\F2\F3\F4\F5\F6\F7\F8\F9\FA", align 16
@std_huff_tables.bits_ac_chrominance = internal constant [17 x i8] c"\00\00\02\01\02\04\04\03\04\07\05\04\04\00\01\02w", align 16
@std_huff_tables.val_ac_chrominance = internal constant [162 x i8] c"\00\01\02\03\11\04\05!1\06\12AQ\07aq\13\222\81\08\14B\91\A1\B1\C1\09#3R\F0\15br\D1\0A\16$4\E1%\F1\17\18\19\1A&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\82\83\84\85\86\87\88\89\8A\92\93\94\95\96\97\98\99\9A\A2\A3\A4\A5\A6\A7\A8\A9\AA\B2\B3\B4\B5\B6\B7\B8\B9\BA\C2\C3\C4\C5\C6\C7\C8\C9\CA\D2\D3\D4\D5\D6\D7\D8\D9\DA\E2\E3\E4\E5\E6\E7\E8\E9\EA\F2\F3\F4\F5\F6\F7\F8\F9\FA", align 16
@jpeg_natural_order = external constant [0 x i32], align 4

; Function Attrs: nounwind
define hidden void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct* %cinfo, i32 %isDC, i32 %tblno, %struct.d_derived_tbl** %pdtbl) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %isDC.addr = alloca i32, align 4
  %tblno.addr = alloca i32, align 4
  %pdtbl.addr = alloca %struct.d_derived_tbl**, align 4
  %htbl = alloca %struct.JHUFF_TBL*, align 4
  %dtbl = alloca %struct.d_derived_tbl*, align 4
  %p = alloca i32, align 4
  %i = alloca i32, align 4
  %l = alloca i32, align 4
  %si = alloca i32, align 4
  %numsymbols = alloca i32, align 4
  %lookbits = alloca i32, align 4
  %ctr = alloca i32, align 4
  %huffsize = alloca [257 x i8], align 16
  %huffcode = alloca [257 x i32], align 16
  %code = alloca i32, align 4
  %sym = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %isDC, i32* %isDC.addr, align 4, !tbaa !6
  store i32 %tblno, i32* %tblno.addr, align 4, !tbaa !6
  store %struct.d_derived_tbl** %pdtbl, %struct.d_derived_tbl*** %pdtbl.addr, align 4, !tbaa !2
  %0 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.d_derived_tbl** %dtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %si to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %numsymbols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %lookbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast [257 x i8]* %huffsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 257, i8* %9) #4
  %10 = bitcast [257 x i32]* %huffcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 1028, i8* %10) #4
  %11 = bitcast i32* %code to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %12, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %13 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %cmp1 = icmp sge i32 %13, 4
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 5
  store i32 50, i32* %msg_code, align 4, !tbaa !12
  %16 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !8
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 6
  %i3 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i3, i32 0, i32 0
  store i32 %16, i32* %arrayidx, align 4, !tbaa !15
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !8
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_decompress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %24 = load i32, i32* %isDC.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %24, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 41
  %26 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %26
  %27 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx5, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 42
  %29 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %29
  %30 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx6, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.JHUFF_TBL* [ %27, %cond.true ], [ %30, %cond.false ]
  store %struct.JHUFF_TBL* %cond, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %31 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %cmp7 = icmp eq %struct.JHUFF_TBL* %31, null
  br i1 %cmp7, label %if.then8, label %if.end17

if.then8:                                         ; preds = %cond.end
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !8
  %msg_code10 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 5
  store i32 50, i32* %msg_code10, align 4, !tbaa !12
  %34 = load i32, i32* %tblno.addr, align 4, !tbaa !6
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 0
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !8
  %msg_parm12 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %36, i32 0, i32 6
  %i13 = bitcast %union.anon* %msg_parm12 to [8 x i32]*
  %arrayidx14 = getelementptr inbounds [8 x i32], [8 x i32]* %i13, i32 0, i32 0
  store i32 %34, i32* %arrayidx14, align 4, !tbaa !15
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  %38 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !8
  %error_exit16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %38, i32 0, i32 0
  %39 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit16, align 4, !tbaa !16
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = bitcast %struct.jpeg_decompress_struct* %40 to %struct.jpeg_common_struct*
  call void %39(%struct.jpeg_common_struct* %41)
  br label %if.end17

if.end17:                                         ; preds = %if.then8, %cond.end
  %42 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl.addr, align 4, !tbaa !2
  %43 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %42, align 4, !tbaa !2
  %cmp18 = icmp eq %struct.d_derived_tbl* %43, null
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end17
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 1
  %45 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !17
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %45, i32 0, i32 0
  %46 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !18
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %48 = bitcast %struct.jpeg_decompress_struct* %47 to %struct.jpeg_common_struct*
  %call = call i8* %46(%struct.jpeg_common_struct* %48, i32 1, i32 1172)
  %49 = bitcast i8* %call to %struct.d_derived_tbl*
  %50 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl.addr, align 4, !tbaa !2
  store %struct.d_derived_tbl* %49, %struct.d_derived_tbl** %50, align 4, !tbaa !2
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.end17
  %51 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl.addr, align 4, !tbaa !2
  %52 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %51, align 4, !tbaa !2
  store %struct.d_derived_tbl* %52, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %53 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %54 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %54, i32 0, i32 2
  store %struct.JHUFF_TBL* %53, %struct.JHUFF_TBL** %pub, align 4, !tbaa !20
  store i32 0, i32* %p, align 4, !tbaa !6
  store i32 1, i32* %l, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end20
  %55 = load i32, i32* %l, align 4, !tbaa !6
  %cmp21 = icmp sle i32 %55, 16
  br i1 %cmp21, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %56 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %56, i32 0, i32 0
  %57 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 %57
  %58 = load i8, i8* %arrayidx22, align 1, !tbaa !15
  %conv = zext i8 %58 to i32
  store i32 %conv, i32* %i, align 4, !tbaa !6
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %59, 0
  br i1 %cmp23, label %if.then28, label %lor.lhs.false25

lor.lhs.false25:                                  ; preds = %for.body
  %60 = load i32, i32* %p, align 4, !tbaa !6
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %60, %61
  %cmp26 = icmp sgt i32 %add, 256
  br i1 %cmp26, label %if.then28, label %if.end33

if.then28:                                        ; preds = %lor.lhs.false25, %for.body
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 0
  %63 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !8
  %msg_code30 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %63, i32 0, i32 5
  store i32 8, i32* %msg_code30, align 4, !tbaa !12
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 0
  %65 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err31, align 8, !tbaa !8
  %error_exit32 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %65, i32 0, i32 0
  %66 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit32, align 4, !tbaa !16
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %68 = bitcast %struct.jpeg_decompress_struct* %67 to %struct.jpeg_common_struct*
  call void %66(%struct.jpeg_common_struct* %68)
  br label %if.end33

if.end33:                                         ; preds = %if.then28, %lor.lhs.false25
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end33
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %dec = add nsw i32 %69, -1
  store i32 %dec, i32* %i, align 4, !tbaa !6
  %tobool34 = icmp ne i32 %69, 0
  br i1 %tobool34, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %70 = load i32, i32* %l, align 4, !tbaa !6
  %conv35 = trunc i32 %70 to i8
  %71 = load i32, i32* %p, align 4, !tbaa !6
  %inc = add nsw i32 %71, 1
  store i32 %inc, i32* %p, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [257 x i8], [257 x i8]* %huffsize, i32 0, i32 %71
  store i8 %conv35, i8* %arrayidx36, align 1, !tbaa !15
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %72 = load i32, i32* %l, align 4, !tbaa !6
  %inc37 = add nsw i32 %72, 1
  store i32 %inc37, i32* %l, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %73 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds [257 x i8], [257 x i8]* %huffsize, i32 0, i32 %73
  store i8 0, i8* %arrayidx38, align 1, !tbaa !15
  %74 = load i32, i32* %p, align 4, !tbaa !6
  store i32 %74, i32* %numsymbols, align 4, !tbaa !6
  store i32 0, i32* %code, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds [257 x i8], [257 x i8]* %huffsize, i32 0, i32 0
  %75 = load i8, i8* %arrayidx39, align 16, !tbaa !15
  %conv40 = sext i8 %75 to i32
  store i32 %conv40, i32* %si, align 4, !tbaa !6
  store i32 0, i32* %p, align 4, !tbaa !6
  br label %while.cond41

while.cond41:                                     ; preds = %if.end62, %for.end
  %76 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [257 x i8], [257 x i8]* %huffsize, i32 0, i32 %76
  %77 = load i8, i8* %arrayidx42, align 1, !tbaa !15
  %tobool43 = icmp ne i8 %77, 0
  br i1 %tobool43, label %while.body44, label %while.end65

while.body44:                                     ; preds = %while.cond41
  br label %while.cond45

while.cond45:                                     ; preds = %while.body50, %while.body44
  %78 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds [257 x i8], [257 x i8]* %huffsize, i32 0, i32 %78
  %79 = load i8, i8* %arrayidx46, align 1, !tbaa !15
  %conv47 = sext i8 %79 to i32
  %80 = load i32, i32* %si, align 4, !tbaa !6
  %cmp48 = icmp eq i32 %conv47, %80
  br i1 %cmp48, label %while.body50, label %while.end54

while.body50:                                     ; preds = %while.cond45
  %81 = load i32, i32* %code, align 4, !tbaa !6
  %82 = load i32, i32* %p, align 4, !tbaa !6
  %inc51 = add nsw i32 %82, 1
  store i32 %inc51, i32* %p, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [257 x i32], [257 x i32]* %huffcode, i32 0, i32 %82
  store i32 %81, i32* %arrayidx52, align 4, !tbaa !6
  %83 = load i32, i32* %code, align 4, !tbaa !6
  %inc53 = add i32 %83, 1
  store i32 %inc53, i32* %code, align 4, !tbaa !6
  br label %while.cond45

while.end54:                                      ; preds = %while.cond45
  %84 = load i32, i32* %code, align 4, !tbaa !6
  %85 = load i32, i32* %si, align 4, !tbaa !6
  %shl = shl i32 1, %85
  %cmp55 = icmp sge i32 %84, %shl
  br i1 %cmp55, label %if.then57, label %if.end62

if.then57:                                        ; preds = %while.end54
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 0
  %87 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err58, align 8, !tbaa !8
  %msg_code59 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %87, i32 0, i32 5
  store i32 8, i32* %msg_code59, align 4, !tbaa !12
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err60 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 0
  %89 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err60, align 8, !tbaa !8
  %error_exit61 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %89, i32 0, i32 0
  %90 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit61, align 4, !tbaa !16
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %92 = bitcast %struct.jpeg_decompress_struct* %91 to %struct.jpeg_common_struct*
  call void %90(%struct.jpeg_common_struct* %92)
  br label %if.end62

if.end62:                                         ; preds = %if.then57, %while.end54
  %93 = load i32, i32* %code, align 4, !tbaa !6
  %shl63 = shl i32 %93, 1
  store i32 %shl63, i32* %code, align 4, !tbaa !6
  %94 = load i32, i32* %si, align 4, !tbaa !6
  %inc64 = add nsw i32 %94, 1
  store i32 %inc64, i32* %si, align 4, !tbaa !6
  br label %while.cond41

while.end65:                                      ; preds = %while.cond41
  store i32 0, i32* %p, align 4, !tbaa !6
  store i32 1, i32* %l, align 4, !tbaa !6
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc86, %while.end65
  %95 = load i32, i32* %l, align 4, !tbaa !6
  %cmp67 = icmp sle i32 %95, 16
  br i1 %cmp67, label %for.body69, label %for.end88

for.body69:                                       ; preds = %for.cond66
  %96 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits70 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %96, i32 0, i32 0
  %97 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds [17 x i8], [17 x i8]* %bits70, i32 0, i32 %97
  %98 = load i8, i8* %arrayidx71, align 1, !tbaa !15
  %tobool72 = icmp ne i8 %98, 0
  br i1 %tobool72, label %if.then73, label %if.else

if.then73:                                        ; preds = %for.body69
  %99 = load i32, i32* %p, align 4, !tbaa !6
  %100 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [257 x i32], [257 x i32]* %huffcode, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx74, align 4, !tbaa !6
  %sub = sub nsw i32 %99, %101
  %102 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %valoffset = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %102, i32 0, i32 1
  %103 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset, i32 0, i32 %103
  store i32 %sub, i32* %arrayidx75, align 4, !tbaa !22
  %104 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits76 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %104, i32 0, i32 0
  %105 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx77 = getelementptr inbounds [17 x i8], [17 x i8]* %bits76, i32 0, i32 %105
  %106 = load i8, i8* %arrayidx77, align 1, !tbaa !15
  %conv78 = zext i8 %106 to i32
  %107 = load i32, i32* %p, align 4, !tbaa !6
  %add79 = add nsw i32 %107, %conv78
  store i32 %add79, i32* %p, align 4, !tbaa !6
  %108 = load i32, i32* %p, align 4, !tbaa !6
  %sub80 = sub nsw i32 %108, 1
  %arrayidx81 = getelementptr inbounds [257 x i32], [257 x i32]* %huffcode, i32 0, i32 %sub80
  %109 = load i32, i32* %arrayidx81, align 4, !tbaa !6
  %110 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %maxcode = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %110, i32 0, i32 0
  %111 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx82 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode, i32 0, i32 %111
  store i32 %109, i32* %arrayidx82, align 4, !tbaa !22
  br label %if.end85

if.else:                                          ; preds = %for.body69
  %112 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %maxcode83 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %112, i32 0, i32 0
  %113 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode83, i32 0, i32 %113
  store i32 -1, i32* %arrayidx84, align 4, !tbaa !22
  br label %if.end85

if.end85:                                         ; preds = %if.else, %if.then73
  br label %for.inc86

for.inc86:                                        ; preds = %if.end85
  %114 = load i32, i32* %l, align 4, !tbaa !6
  %inc87 = add nsw i32 %114, 1
  store i32 %inc87, i32* %l, align 4, !tbaa !6
  br label %for.cond66

for.end88:                                        ; preds = %for.cond66
  %115 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %valoffset89 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %115, i32 0, i32 1
  %arrayidx90 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset89, i32 0, i32 17
  store i32 0, i32* %arrayidx90, align 4, !tbaa !22
  %116 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %maxcode91 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %116, i32 0, i32 0
  %arrayidx92 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode91, i32 0, i32 17
  store i32 1048575, i32* %arrayidx92, align 4, !tbaa !22
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond93

for.cond93:                                       ; preds = %for.inc98, %for.end88
  %117 = load i32, i32* %i, align 4, !tbaa !6
  %cmp94 = icmp slt i32 %117, 256
  br i1 %cmp94, label %for.body96, label %for.end100

for.body96:                                       ; preds = %for.cond93
  %118 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %118, i32 0, i32 3
  %119 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx97 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %119
  store i32 2304, i32* %arrayidx97, align 4, !tbaa !6
  br label %for.inc98

for.inc98:                                        ; preds = %for.body96
  %120 = load i32, i32* %i, align 4, !tbaa !6
  %inc99 = add nsw i32 %120, 1
  store i32 %inc99, i32* %i, align 4, !tbaa !6
  br label %for.cond93

for.end100:                                       ; preds = %for.cond93
  store i32 0, i32* %p, align 4, !tbaa !6
  store i32 1, i32* %l, align 4, !tbaa !6
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc134, %for.end100
  %121 = load i32, i32* %l, align 4, !tbaa !6
  %cmp102 = icmp sle i32 %121, 8
  br i1 %cmp102, label %for.body104, label %for.end136

for.body104:                                      ; preds = %for.cond101
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond105

for.cond105:                                      ; preds = %for.inc130, %for.body104
  %122 = load i32, i32* %i, align 4, !tbaa !6
  %123 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits106 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %123, i32 0, i32 0
  %124 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds [17 x i8], [17 x i8]* %bits106, i32 0, i32 %124
  %125 = load i8, i8* %arrayidx107, align 1, !tbaa !15
  %conv108 = zext i8 %125 to i32
  %cmp109 = icmp sle i32 %122, %conv108
  br i1 %cmp109, label %for.body111, label %for.end133

for.body111:                                      ; preds = %for.cond105
  %126 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx112 = getelementptr inbounds [257 x i32], [257 x i32]* %huffcode, i32 0, i32 %126
  %127 = load i32, i32* %arrayidx112, align 4, !tbaa !6
  %128 = load i32, i32* %l, align 4, !tbaa !6
  %sub113 = sub nsw i32 8, %128
  %shl114 = shl i32 %127, %sub113
  store i32 %shl114, i32* %lookbits, align 4, !tbaa !6
  %129 = load i32, i32* %l, align 4, !tbaa !6
  %sub115 = sub nsw i32 8, %129
  %shl116 = shl i32 1, %sub115
  store i32 %shl116, i32* %ctr, align 4, !tbaa !6
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc127, %for.body111
  %130 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp118 = icmp sgt i32 %130, 0
  br i1 %cmp118, label %for.body120, label %for.end129

for.body120:                                      ; preds = %for.cond117
  %131 = load i32, i32* %l, align 4, !tbaa !6
  %shl121 = shl i32 %131, 8
  %132 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %132, i32 0, i32 1
  %133 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx122 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %133
  %134 = load i8, i8* %arrayidx122, align 1, !tbaa !15
  %conv123 = zext i8 %134 to i32
  %or = or i32 %shl121, %conv123
  %135 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dtbl, align 4, !tbaa !2
  %lookup124 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %135, i32 0, i32 3
  %136 = load i32, i32* %lookbits, align 4, !tbaa !6
  %arrayidx125 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup124, i32 0, i32 %136
  store i32 %or, i32* %arrayidx125, align 4, !tbaa !6
  %137 = load i32, i32* %lookbits, align 4, !tbaa !6
  %inc126 = add nsw i32 %137, 1
  store i32 %inc126, i32* %lookbits, align 4, !tbaa !6
  br label %for.inc127

for.inc127:                                       ; preds = %for.body120
  %138 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec128 = add nsw i32 %138, -1
  store i32 %dec128, i32* %ctr, align 4, !tbaa !6
  br label %for.cond117

for.end129:                                       ; preds = %for.cond117
  br label %for.inc130

for.inc130:                                       ; preds = %for.end129
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %inc131 = add nsw i32 %139, 1
  store i32 %inc131, i32* %i, align 4, !tbaa !6
  %140 = load i32, i32* %p, align 4, !tbaa !6
  %inc132 = add nsw i32 %140, 1
  store i32 %inc132, i32* %p, align 4, !tbaa !6
  br label %for.cond105

for.end133:                                       ; preds = %for.cond105
  br label %for.inc134

for.inc134:                                       ; preds = %for.end133
  %141 = load i32, i32* %l, align 4, !tbaa !6
  %inc135 = add nsw i32 %141, 1
  store i32 %inc135, i32* %l, align 4, !tbaa !6
  br label %for.cond101

for.end136:                                       ; preds = %for.cond101
  %142 = load i32, i32* %isDC.addr, align 4, !tbaa !6
  %tobool137 = icmp ne i32 %142, 0
  br i1 %tobool137, label %if.then138, label %if.end160

if.then138:                                       ; preds = %for.end136
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc157, %if.then138
  %143 = load i32, i32* %i, align 4, !tbaa !6
  %144 = load i32, i32* %numsymbols, align 4, !tbaa !6
  %cmp140 = icmp slt i32 %143, %144
  br i1 %cmp140, label %for.body142, label %for.end159

for.body142:                                      ; preds = %for.cond139
  %145 = bitcast i32* %sym to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #4
  %146 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %huffval143 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %146, i32 0, i32 1
  %147 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx144 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval143, i32 0, i32 %147
  %148 = load i8, i8* %arrayidx144, align 1, !tbaa !15
  %conv145 = zext i8 %148 to i32
  store i32 %conv145, i32* %sym, align 4, !tbaa !6
  %149 = load i32, i32* %sym, align 4, !tbaa !6
  %cmp146 = icmp slt i32 %149, 0
  br i1 %cmp146, label %if.then151, label %lor.lhs.false148

lor.lhs.false148:                                 ; preds = %for.body142
  %150 = load i32, i32* %sym, align 4, !tbaa !6
  %cmp149 = icmp sgt i32 %150, 15
  br i1 %cmp149, label %if.then151, label %if.end156

if.then151:                                       ; preds = %lor.lhs.false148, %for.body142
  %151 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err152 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %151, i32 0, i32 0
  %152 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err152, align 8, !tbaa !8
  %msg_code153 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %152, i32 0, i32 5
  store i32 8, i32* %msg_code153, align 4, !tbaa !12
  %153 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err154 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %153, i32 0, i32 0
  %154 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err154, align 8, !tbaa !8
  %error_exit155 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %154, i32 0, i32 0
  %155 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit155, align 4, !tbaa !16
  %156 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %157 = bitcast %struct.jpeg_decompress_struct* %156 to %struct.jpeg_common_struct*
  call void %155(%struct.jpeg_common_struct* %157)
  br label %if.end156

if.end156:                                        ; preds = %if.then151, %lor.lhs.false148
  %158 = bitcast i32* %sym to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  br label %for.inc157

for.inc157:                                       ; preds = %if.end156
  %159 = load i32, i32* %i, align 4, !tbaa !6
  %inc158 = add nsw i32 %159, 1
  store i32 %inc158, i32* %i, align 4, !tbaa !6
  br label %for.cond139

for.end159:                                       ; preds = %for.cond139
  br label %if.end160

if.end160:                                        ; preds = %for.end159, %for.end136
  %160 = bitcast i32* %code to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast [257 x i32]* %huffcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 1028, i8* %161) #4
  %162 = bitcast [257 x i8]* %huffsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 257, i8* %162) #4
  %163 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i32* %lookbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  %165 = bitcast i32* %numsymbols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast i32* %si to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast %struct.d_derived_tbl** %dtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %state, i32 %get_buffer, i32 %bits_left, i32 %nbits) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.bitread_working_state*, align 4
  %get_buffer.addr = alloca i32, align 4
  %bits_left.addr = alloca i32, align 4
  %nbits.addr = alloca i32, align 4
  %next_input_byte = alloca i8*, align 4
  %bytes_in_buffer = alloca i32, align 4
  %cinfo = alloca %struct.jpeg_decompress_struct*, align 4
  %c = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.bitread_working_state* %state, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  store i32 %get_buffer, i32* %get_buffer.addr, align 4, !tbaa !22
  store i32 %bits_left, i32* %bits_left.addr, align 4, !tbaa !6
  store i32 %nbits, i32* %nbits.addr, align 4, !tbaa !6
  %0 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %next_input_byte1 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %1, i32 0, i32 0
  %2 = load i8*, i8** %next_input_byte1, align 4, !tbaa !23
  store i8* %2, i8** %next_input_byte, align 4, !tbaa !2
  %3 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bytes_in_buffer2 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %4, i32 0, i32 1
  %5 = load i32, i32* %bytes_in_buffer2, align 4, !tbaa !25
  store i32 %5, i32* %bytes_in_buffer, align 4, !tbaa !22
  %6 = bitcast %struct.jpeg_decompress_struct** %cinfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %cinfo3 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %7, i32 0, i32 4
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo3, align 4, !tbaa !26
  store %struct.jpeg_decompress_struct* %8, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 76
  %10 = load i32, i32* %unread_marker, align 8, !tbaa !27
  %cmp = icmp eq i32 %10, 0
  br i1 %cmp, label %if.then, label %if.else41

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.then
  %11 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %11, 25
  br i1 %cmp4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp5 = icmp eq i32 %13, 0
  br i1 %cmp5, label %if.then6, label %if.end12

if.then6:                                         ; preds = %while.body
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 6
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !28
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 3
  %16 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !29
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %call = call i32 %16(%struct.jpeg_decompress_struct* %17)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then7

if.then7:                                         ; preds = %if.then6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then6
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 6
  %19 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src8, align 8, !tbaa !28
  %next_input_byte9 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %19, i32 0, i32 0
  %20 = load i8*, i8** %next_input_byte9, align 4, !tbaa !31
  store i8* %20, i8** %next_input_byte, align 4, !tbaa !2
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 6
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src10, align 8, !tbaa !28
  %bytes_in_buffer11 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 1
  %23 = load i32, i32* %bytes_in_buffer11, align 4, !tbaa !32
  store i32 %23, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end12

if.end12:                                         ; preds = %if.end, %while.body
  %24 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec = add i32 %24, -1
  store i32 %dec, i32* %bytes_in_buffer, align 4, !tbaa !22
  %25 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %25, i32 1
  store i8* %incdec.ptr, i8** %next_input_byte, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !15
  %conv = zext i8 %26 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !6
  %27 = load i32, i32* %c, align 4, !tbaa !6
  %cmp13 = icmp eq i32 %27, 255
  br i1 %cmp13, label %if.then15, label %if.end40

if.then15:                                        ; preds = %if.end12
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then15
  %28 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %cmp16 = icmp eq i32 %28, 0
  br i1 %cmp16, label %if.then18, label %if.end29

if.then18:                                        ; preds = %do.body
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 6
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src19, align 8, !tbaa !28
  %fill_input_buffer20 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %30, i32 0, i32 3
  %31 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer20, align 4, !tbaa !29
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %call21 = call i32 %31(%struct.jpeg_decompress_struct* %32)
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %if.then18
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %if.then18
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 6
  %34 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src25, align 8, !tbaa !28
  %next_input_byte26 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %34, i32 0, i32 0
  %35 = load i8*, i8** %next_input_byte26, align 4, !tbaa !31
  store i8* %35, i8** %next_input_byte, align 4, !tbaa !2
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %src27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 6
  %37 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src27, align 8, !tbaa !28
  %bytes_in_buffer28 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %37, i32 0, i32 1
  %38 = load i32, i32* %bytes_in_buffer28, align 4, !tbaa !32
  store i32 %38, i32* %bytes_in_buffer, align 4, !tbaa !22
  br label %if.end29

if.end29:                                         ; preds = %if.end24, %do.body
  %39 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %dec30 = add i32 %39, -1
  store i32 %dec30, i32* %bytes_in_buffer, align 4, !tbaa !22
  %40 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %incdec.ptr31 = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr31, i8** %next_input_byte, align 4, !tbaa !2
  %41 = load i8, i8* %40, align 1, !tbaa !15
  %conv32 = zext i8 %41 to i32
  store i32 %conv32, i32* %c, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %if.end29
  %42 = load i32, i32* %c, align 4, !tbaa !6
  %cmp33 = icmp eq i32 %42, 255
  br i1 %cmp33, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %43 = load i32, i32* %c, align 4, !tbaa !6
  %cmp35 = icmp eq i32 %43, 0
  br i1 %cmp35, label %if.then37, label %if.else

if.then37:                                        ; preds = %do.end
  store i32 255, i32* %c, align 4, !tbaa !6
  br label %if.end39

if.else:                                          ; preds = %do.end
  %44 = load i32, i32* %c, align 4, !tbaa !6
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %unread_marker38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 76
  store i32 %44, i32* %unread_marker38, align 8, !tbaa !27
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %if.then37
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end12
  %46 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %shl = shl i32 %46, 8
  %47 = load i32, i32* %c, align 4, !tbaa !6
  %or = or i32 %shl, %47
  store i32 %or, i32* %get_buffer.addr, align 4, !tbaa !22
  %48 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %add = add nsw i32 %48, 8
  store i32 %add, i32* %bits_left.addr, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.end40, %if.then23, %if.then7
  %49 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup58 [
    i32 0, label %cleanup.cont
    i32 6, label %no_more_bytes
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end53

if.else41:                                        ; preds = %entry
  br label %no_more_bytes

no_more_bytes:                                    ; preds = %if.else41, %cleanup
  %50 = load i32, i32* %nbits.addr, align 4, !tbaa !6
  %51 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %cmp42 = icmp sgt i32 %50, %51
  br i1 %cmp42, label %if.then44, label %if.end52

if.then44:                                        ; preds = %no_more_bytes
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 83
  %53 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy, align 4, !tbaa !33
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %53, i32 0, i32 2
  %54 = load i32, i32* %insufficient_data, align 4, !tbaa !34
  %tobool45 = icmp ne i32 %54, 0
  br i1 %tobool45, label %if.end50, label %if.then46

if.then46:                                        ; preds = %if.then44
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 5
  store i32 117, i32* %msg_code, align 4, !tbaa !12
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %err47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 0
  %58 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err47, align 8, !tbaa !8
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %58, i32 0, i32 1
  %59 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !36
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %61 = bitcast %struct.jpeg_decompress_struct* %60 to %struct.jpeg_common_struct*
  call void %59(%struct.jpeg_common_struct* %61, i32 -1)
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !2
  %entropy48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 83
  %63 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy48, align 4, !tbaa !33
  %insufficient_data49 = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %63, i32 0, i32 2
  store i32 1, i32* %insufficient_data49, align 4, !tbaa !34
  br label %if.end50

if.end50:                                         ; preds = %if.then46, %if.then44
  %64 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %sub = sub nsw i32 25, %64
  %65 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %shl51 = shl i32 %65, %sub
  store i32 %shl51, i32* %get_buffer.addr, align 4, !tbaa !22
  store i32 25, i32* %bits_left.addr, align 4, !tbaa !6
  br label %if.end52

if.end52:                                         ; preds = %if.end50, %no_more_bytes
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %while.end
  %66 = load i8*, i8** %next_input_byte, align 4, !tbaa !2
  %67 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %next_input_byte54 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %67, i32 0, i32 0
  store i8* %66, i8** %next_input_byte54, align 4, !tbaa !23
  %68 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !22
  %69 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bytes_in_buffer55 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %69, i32 0, i32 1
  store i32 %68, i32* %bytes_in_buffer55, align 4, !tbaa !25
  %70 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %71 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %get_buffer56 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %71, i32 0, i32 2
  store i32 %70, i32* %get_buffer56, align 4, !tbaa !37
  %72 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %73 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bits_left57 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %73, i32 0, i32 3
  store i32 %72, i32* %bits_left57, align 4, !tbaa !38
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

cleanup58:                                        ; preds = %if.end53, %cleanup
  %74 = bitcast %struct.jpeg_decompress_struct** %cinfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i32* %bytes_in_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  %76 = bitcast i8** %next_input_byte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %77 = load i32, i32* %retval, align 4
  ret i32 %77
}

; Function Attrs: nounwind
define hidden i32 @jpeg_huff_decode(%struct.bitread_working_state* %state, i32 %get_buffer, i32 %bits_left, %struct.d_derived_tbl* %htbl, i32 %min_bits) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.bitread_working_state*, align 4
  %get_buffer.addr = alloca i32, align 4
  %bits_left.addr = alloca i32, align 4
  %htbl.addr = alloca %struct.d_derived_tbl*, align 4
  %min_bits.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %code = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.bitread_working_state* %state, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  store i32 %get_buffer, i32* %get_buffer.addr, align 4, !tbaa !22
  store i32 %bits_left, i32* %bits_left.addr, align 4, !tbaa !6
  store %struct.d_derived_tbl* %htbl, %struct.d_derived_tbl** %htbl.addr, align 4, !tbaa !2
  store i32 %min_bits, i32* %min_bits.addr, align 4, !tbaa !6
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %min_bits.addr, align 4, !tbaa !6
  store i32 %1, i32* %l, align 4, !tbaa !6
  %2 = bitcast i32* %code to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %4 = load i32, i32* %l, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %5 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %6 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %7 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %8 = load i32, i32* %l, align 4, !tbaa !6
  %call = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %5, i32 %6, i32 %7, i32 %8)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then1

if.then1:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %9 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %get_buffer2 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %9, i32 0, i32 2
  %10 = load i32, i32* %get_buffer2, align 4, !tbaa !37
  store i32 %10, i32* %get_buffer.addr, align 4, !tbaa !22
  %11 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bits_left3 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %11, i32 0, i32 3
  %12 = load i32, i32* %bits_left3, align 4, !tbaa !38
  store i32 %12, i32* %bits_left.addr, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %13 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %14 = load i32, i32* %l, align 4, !tbaa !6
  %15 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %15, %14
  store i32 %sub, i32* %bits_left.addr, align 4, !tbaa !6
  %shr = lshr i32 %13, %sub
  %16 = load i32, i32* %l, align 4, !tbaa !6
  %shl = shl i32 1, %16
  %sub5 = sub nsw i32 %shl, 1
  %and = and i32 %shr, %sub5
  store i32 %and, i32* %code, align 4, !tbaa !22
  br label %while.cond

while.cond:                                       ; preds = %if.end16, %if.end4
  %17 = load i32, i32* %code, align 4, !tbaa !22
  %18 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %htbl.addr, align 4, !tbaa !2
  %maxcode = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %18, i32 0, i32 0
  %19 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx, align 4, !tbaa !22
  %cmp6 = icmp sgt i32 %17, %20
  br i1 %cmp6, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %21 = load i32, i32* %code, align 4, !tbaa !22
  %shl7 = shl i32 %21, 1
  store i32 %shl7, i32* %code, align 4, !tbaa !22
  %22 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %22, 1
  br i1 %cmp8, label %if.then9, label %if.end16

if.then9:                                         ; preds = %while.body
  %23 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %24 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %25 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %call10 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %23, i32 %24, i32 %25, i32 1)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %if.then9
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.then9
  %26 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %get_buffer14 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %26, i32 0, i32 2
  %27 = load i32, i32* %get_buffer14, align 4, !tbaa !37
  store i32 %27, i32* %get_buffer.addr, align 4, !tbaa !22
  %28 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bits_left15 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %28, i32 0, i32 3
  %29 = load i32, i32* %bits_left15, align 4, !tbaa !38
  store i32 %29, i32* %bits_left.addr, align 4, !tbaa !6
  br label %if.end16

if.end16:                                         ; preds = %if.end13, %while.body
  %30 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %31 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %sub17 = sub nsw i32 %31, 1
  store i32 %sub17, i32* %bits_left.addr, align 4, !tbaa !6
  %shr18 = lshr i32 %30, %sub17
  %and19 = and i32 %shr18, 1
  %32 = load i32, i32* %code, align 4, !tbaa !22
  %or = or i32 %32, %and19
  store i32 %or, i32* %code, align 4, !tbaa !22
  %33 = load i32, i32* %l, align 4, !tbaa !6
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %l, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %34 = load i32, i32* %get_buffer.addr, align 4, !tbaa !22
  %35 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %get_buffer20 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %35, i32 0, i32 2
  store i32 %34, i32* %get_buffer20, align 4, !tbaa !37
  %36 = load i32, i32* %bits_left.addr, align 4, !tbaa !6
  %37 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %bits_left21 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %37, i32 0, i32 3
  store i32 %36, i32* %bits_left21, align 4, !tbaa !38
  %38 = load i32, i32* %l, align 4, !tbaa !6
  %cmp22 = icmp sgt i32 %38, 16
  br i1 %cmp22, label %if.then23, label %if.end27

if.then23:                                        ; preds = %while.end
  %39 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %cinfo = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %39, i32 0, i32 4
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo, align 4, !tbaa !26
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 5
  store i32 118, i32* %msg_code, align 4, !tbaa !12
  %42 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %cinfo24 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %42, i32 0, i32 4
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo24, align 4, !tbaa !26
  %err25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 0
  %44 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !8
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %44, i32 0, i32 1
  %45 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !36
  %46 = load %struct.bitread_working_state*, %struct.bitread_working_state** %state.addr, align 4, !tbaa !2
  %cinfo26 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %46, i32 0, i32 4
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo26, align 4, !tbaa !26
  %48 = bitcast %struct.jpeg_decompress_struct* %47 to %struct.jpeg_common_struct*
  call void %45(%struct.jpeg_common_struct* %48, i32 -1)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %while.end
  %49 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %htbl.addr, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %49, i32 0, i32 2
  %50 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %pub, align 4, !tbaa !20
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %50, i32 0, i32 1
  %51 = load i32, i32* %code, align 4, !tbaa !22
  %52 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %htbl.addr, align 4, !tbaa !2
  %valoffset = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %52, i32 0, i32 1
  %53 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx28, align 4, !tbaa !22
  %add = add nsw i32 %51, %54
  %arrayidx29 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %add
  %55 = load i8, i8* %arrayidx29, align 1, !tbaa !15
  %conv = zext i8 %55 to i32
  store i32 %conv, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end27, %if.then23, %if.then12, %if.then1
  %56 = bitcast i32* %code to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = load i32, i32* %retval, align 4
  ret i32 %58
}

; Function Attrs: nounwind
define hidden void @jinit_huff_decoder(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = bitcast %struct.jpeg_decompress_struct* %2 to %struct.jpeg_common_struct*
  call void @std_huff_tables(%struct.jpeg_common_struct* %3)
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 1
  %5 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !17
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %5, i32 0, i32 0
  %6 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !18
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = bitcast %struct.jpeg_decompress_struct* %7 to %struct.jpeg_common_struct*
  %call = call i8* %6(%struct.jpeg_common_struct* %8, i32 1, i32 232)
  %9 = bitcast i8* %call to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %9, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %10 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %11 = bitcast %struct.huff_entropy_decoder* %10 to %struct.jpeg_entropy_decoder*
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 83
  store %struct.jpeg_entropy_decoder* %11, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %13 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %13, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*)* @start_pass_huff_decoder, void (%struct.jpeg_decompress_struct*)** %start_pass, align 4, !tbaa !39
  %14 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %14, i32 0, i32 0
  %decode_mcu = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub2, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)* @decode_mcu, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu, align 4, !tbaa !43
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %16, i32 0, i32 5
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %ac_derived_tbls, i32 0, i32 %17
  store %struct.d_derived_tbl* null, %struct.d_derived_tbl** %arrayidx, align 4, !tbaa !2
  %18 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_derived_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %18, i32 0, i32 4
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %dc_derived_tbls, i32 0, i32 %19
  store %struct.d_derived_tbl* null, %struct.d_derived_tbl** %arrayidx3, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  %22 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret void
}

; Function Attrs: nounwind
define internal void @std_huff_tables(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %dc_huff_tbl_ptrs = alloca %struct.JHUFF_TBL**, align 4
  %ac_huff_tbl_ptrs = alloca %struct.JHUFF_TBL**, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %is_decompressor = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %2, i32 0, i32 4
  %3 = load i32, i32* %is_decompressor, align 4, !tbaa !44
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_common_struct* %4 to %struct.jpeg_decompress_struct*
  %dc_huff_tbl_ptrs1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 41
  %arraydecay = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs1, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %6 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_common_struct* %6 to %struct.jpeg_decompress_struct*
  %ac_huff_tbl_ptrs2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 42
  %arraydecay3 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs2, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay3, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %8 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_common_struct* %8 to %struct.jpeg_compress_struct*
  %dc_huff_tbl_ptrs4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 17
  %arraydecay5 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs4, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay5, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %10 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_common_struct* %10 to %struct.jpeg_compress_struct*
  %ac_huff_tbl_ptrs6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 18
  %arraydecay7 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs6, i32 0, i32 0
  store %struct.JHUFF_TBL** %arraydecay7, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %13, i32 0
  call void @add_huff_table(%struct.jpeg_common_struct* %12, %struct.JHUFF_TBL** %arrayidx, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_dc_luminance, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @std_huff_tables.val_dc_luminance, i32 0, i32 0))
  %14 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %15, i32 0
  call void @add_huff_table(%struct.jpeg_common_struct* %14, %struct.JHUFF_TBL** %arrayidx8, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_ac_luminance, i32 0, i32 0), i8* getelementptr inbounds ([162 x i8], [162 x i8]* @std_huff_tables.val_ac_luminance, i32 0, i32 0))
  %16 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %17, i32 1
  call void @add_huff_table(%struct.jpeg_common_struct* %16, %struct.JHUFF_TBL** %arrayidx9, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_dc_chrominance, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @std_huff_tables.val_dc_chrominance, i32 0, i32 0))
  %18 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %19, i32 1
  call void @add_huff_table(%struct.jpeg_common_struct* %18, %struct.JHUFF_TBL** %arrayidx10, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @std_huff_tables.bits_ac_chrominance, i32 0, i32 0), i8* getelementptr inbounds ([162 x i8], [162 x i8]* @std_huff_tables.val_ac_chrominance, i32 0, i32 0))
  %20 = bitcast %struct.JHUFF_TBL*** %ac_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast %struct.JHUFF_TBL*** %dc_huff_tbl_ptrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  ret void
}

; Function Attrs: nounwind
define internal void @start_pass_huff_decoder(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %ci = alloca i32, align 4
  %blkn = alloca i32, align 4
  %dctbl = alloca i32, align 4
  %actbl = alloca i32, align 4
  %pdtbl = alloca %struct.d_derived_tbl**, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %3, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %dctbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %actbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast %struct.d_derived_tbl*** %pdtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 72
  %11 = load i32, i32* %Ss, align 8, !tbaa !46
  %cmp = icmp ne i32 %11, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 73
  %13 = load i32, i32* %Se, align 4, !tbaa !47
  %cmp2 = icmp ne i32 %13, 63
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 74
  %15 = load i32, i32* %Ah, align 8, !tbaa !48
  %cmp4 = icmp ne i32 %15, 0
  br i1 %cmp4, label %if.then, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %lor.lhs.false3
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 75
  %17 = load i32, i32* %Al, align 4, !tbaa !49
  %cmp6 = icmp ne i32 %17, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false5, %lor.lhs.false3, %lor.lhs.false, %entry
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 122, i32* %msg_code, align 4, !tbaa !12
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !8
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 1
  %22 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !36
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24, i32 -1)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false5
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %25 = load i32, i32* %ci, align 4, !tbaa !6
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 66
  %27 = load i32, i32* %comps_in_scan, align 8, !tbaa !50
  %cmp8 = icmp slt i32 %25, %27
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 67
  %29 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %29
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %30, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 0, i32 5
  %32 = load i32, i32* %dc_tbl_no, align 4, !tbaa !51
  store i32 %32, i32* %dctbl, align 4, !tbaa !6
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0, i32 6
  %34 = load i32, i32* %ac_tbl_no, align 4, !tbaa !53
  store i32 %34, i32* %actbl, align 4, !tbaa !6
  %35 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_derived_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %35, i32 0, i32 4
  %arraydecay = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %dc_derived_tbls, i32 0, i32 0
  %36 = load i32, i32* %dctbl, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds %struct.d_derived_tbl*, %struct.d_derived_tbl** %arraydecay, i32 %36
  store %struct.d_derived_tbl** %add.ptr, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %38 = load i32, i32* %dctbl, align 4, !tbaa !6
  %39 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  call void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct* %37, i32 1, i32 %38, %struct.d_derived_tbl** %39)
  %40 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %40, i32 0, i32 5
  %arraydecay9 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %ac_derived_tbls, i32 0, i32 0
  %41 = load i32, i32* %actbl, align 4, !tbaa !6
  %add.ptr10 = getelementptr inbounds %struct.d_derived_tbl*, %struct.d_derived_tbl** %arraydecay9, i32 %41
  store %struct.d_derived_tbl** %add.ptr10, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %43 = load i32, i32* %actbl, align 4, !tbaa !6
  %44 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  call void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct* %42, i32 0, i32 %43, %struct.d_derived_tbl** %44)
  %45 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %45, i32 0, i32 2
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 0
  %46 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %46
  store i32 0, i32* %arrayidx11, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %blkn, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc35, %for.end
  %48 = load i32, i32* %blkn, align 4, !tbaa !6
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 70
  %50 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !54
  %cmp13 = icmp slt i32 %48, %50
  br i1 %cmp13, label %for.body14, label %for.end37

for.body14:                                       ; preds = %for.cond12
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 71
  %52 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx15, align 4, !tbaa !6
  store i32 %53, i32* %ci, align 4, !tbaa !6
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 67
  %55 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info16, i32 0, i32 %55
  %56 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx17, align 4, !tbaa !2
  store %struct.jpeg_component_info* %56, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %57 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_derived_tbls18 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %57, i32 0, i32 4
  %58 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no19 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %58, i32 0, i32 5
  %59 = load i32, i32* %dc_tbl_no19, align 4, !tbaa !51
  %arrayidx20 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %dc_derived_tbls18, i32 0, i32 %59
  %60 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx20, align 4, !tbaa !2
  %61 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %61, i32 0, i32 6
  %62 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %dc_cur_tbls, i32 0, i32 %62
  store %struct.d_derived_tbl* %60, %struct.d_derived_tbl** %arrayidx21, align 4, !tbaa !2
  %63 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbls22 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %63, i32 0, i32 5
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no23 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 0, i32 6
  %65 = load i32, i32* %ac_tbl_no23, align 4, !tbaa !53
  %arrayidx24 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %ac_derived_tbls22, i32 0, i32 %65
  %66 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx24, align 4, !tbaa !2
  %67 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %67, i32 0, i32 7
  %68 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %ac_cur_tbls, i32 0, i32 %68
  store %struct.d_derived_tbl* %66, %struct.d_derived_tbl** %arrayidx25, align 4, !tbaa !2
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 12
  %70 = load i32, i32* %component_needed, align 4, !tbaa !55
  %tobool = icmp ne i32 %70, 0
  br i1 %tobool, label %if.then26, label %if.else

if.then26:                                        ; preds = %for.body14
  %71 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %71, i32 0, i32 8
  %72 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds [10 x i32], [10 x i32]* %dc_needed, i32 0, i32 %72
  store i32 1, i32* %arrayidx27, align 4, !tbaa !6
  %73 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %73, i32 0, i32 9
  %74 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !56
  %cmp28 = icmp sgt i32 %74, 1
  %conv = zext i1 %cmp28 to i32
  %75 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %75, i32 0, i32 9
  %76 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [10 x i32], [10 x i32]* %ac_needed, i32 0, i32 %76
  store i32 %conv, i32* %arrayidx29, align 4, !tbaa !6
  br label %if.end34

if.else:                                          ; preds = %for.body14
  %77 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_needed30 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %77, i32 0, i32 9
  %78 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds [10 x i32], [10 x i32]* %ac_needed30, i32 0, i32 %78
  store i32 0, i32* %arrayidx31, align 4, !tbaa !6
  %79 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_needed32 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %79, i32 0, i32 8
  %80 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [10 x i32], [10 x i32]* %dc_needed32, i32 0, i32 %80
  store i32 0, i32* %arrayidx33, align 4, !tbaa !6
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.then26
  br label %for.inc35

for.inc35:                                        ; preds = %if.end34
  %81 = load i32, i32* %blkn, align 4, !tbaa !6
  %inc36 = add nsw i32 %81, 1
  store i32 %inc36, i32* %blkn, align 4, !tbaa !6
  br label %for.cond12

for.end37:                                        ; preds = %for.cond12
  %82 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %82, i32 0, i32 1
  %bits_left = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 1
  store i32 0, i32* %bits_left, align 4, !tbaa !57
  %83 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate38 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %83, i32 0, i32 1
  %get_buffer = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate38, i32 0, i32 0
  store i32 0, i32* %get_buffer, align 4, !tbaa !58
  %84 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %84, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  store i32 0, i32* %insufficient_data, align 4, !tbaa !59
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 50
  %86 = load i32, i32* %restart_interval, align 4, !tbaa !60
  %87 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %87, i32 0, i32 3
  store i32 %86, i32* %restarts_to_go, align 4, !tbaa !61
  %88 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast %struct.d_derived_tbl*** %pdtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %actbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i32* %dctbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  ret void
}

; Function Attrs: nounwind
define internal i32 @decode_mcu(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %usefast = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %3, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %usefast to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 1, i32* %usefast, align 4, !tbaa !6
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 50
  %6 = load i32, i32* %restart_interval, align 4, !tbaa !60
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %7 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %7, i32 0, i32 3
  %8 = load i32, i32* %restarts_to_go, align 4, !tbaa !61
  %cmp = icmp eq i32 %8, 0
  br i1 %cmp, label %if.then2, label %if.end5

if.then2:                                         ; preds = %if.then
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @process_restart(%struct.jpeg_decompress_struct* %9)
  %tobool3 = icmp ne i32 %call, 0
  br i1 %tobool3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.then2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then2
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  store i32 0, i32* %usefast, align 4, !tbaa !6
  br label %if.end6

if.end6:                                          ; preds = %if.end5, %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !28
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 1
  %12 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !32
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 70
  %14 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !54
  %mul = mul i32 512, %14
  %cmp7 = icmp ult i32 %12, %mul
  br i1 %cmp7, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 76
  %16 = load i32, i32* %unread_marker, align 8, !tbaa !27
  %cmp8 = icmp ne i32 %16, 0
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false, %if.end6
  store i32 0, i32* %usefast, align 4, !tbaa !6
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %lor.lhs.false
  %17 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %17, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  %18 = load i32, i32* %insufficient_data, align 4, !tbaa !59
  %tobool11 = icmp ne i32 %18, 0
  br i1 %tobool11, label %if.end24, label %if.then12

if.then12:                                        ; preds = %if.end10
  %19 = load i32, i32* %usefast, align 4, !tbaa !6
  %tobool13 = icmp ne i32 %19, 0
  br i1 %tobool13, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.then12
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %call15 = call i32 @decode_mcu_fast(%struct.jpeg_decompress_struct* %20, [64 x i16]** %21)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.then14
  br label %use_slow

if.end18:                                         ; preds = %if.then14
  br label %if.end23

if.else:                                          ; preds = %if.then12
  br label %use_slow

use_slow:                                         ; preds = %if.else, %if.then17
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %call19 = call i32 @decode_mcu_slow(%struct.jpeg_decompress_struct* %22, [64 x i16]** %23)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.end22, label %if.then21

if.then21:                                        ; preds = %use_slow
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %use_slow
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end18
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.end10
  %24 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go25 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %24, i32 0, i32 3
  %25 = load i32, i32* %restarts_to_go25, align 4, !tbaa !61
  %dec = add i32 %25, -1
  store i32 %dec, i32* %restarts_to_go25, align 4, !tbaa !61
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end24, %if.then21, %if.then4
  %26 = bitcast i32* %usefast to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: nounwind
define internal void @add_huff_table(%struct.jpeg_common_struct* %cinfo, %struct.JHUFF_TBL** %htblptr, i8* %bits, i8* %val) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %htblptr.addr = alloca %struct.JHUFF_TBL**, align 4
  %bits.addr = alloca i8*, align 4
  %val.addr = alloca i8*, align 4
  %nsymbols = alloca i32, align 4
  %len = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.JHUFF_TBL** %htblptr, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  store i8* %bits, i8** %bits.addr, align 4, !tbaa !2
  store i8* %val, i8** %val.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nsymbols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %3 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %2, align 4, !tbaa !2
  %cmp = icmp eq %struct.JHUFF_TBL* %3, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct* %4)
  %5 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %call, %struct.JHUFF_TBL** %5, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %6 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %7 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %6, align 4, !tbaa !2
  %bits1 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %7, i32 0, i32 0
  %arraydecay = getelementptr inbounds [17 x i8], [17 x i8]* %bits1, i32 0, i32 0
  %8 = load i8*, i8** %bits.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay, i8* align 1 %8, i32 17, i1 false)
  store i32 0, i32* %nsymbols, align 4, !tbaa !6
  store i32 1, i32* %len, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %len, align 4, !tbaa !6
  %cmp2 = icmp sle i32 %9, 16
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8*, i8** %bits.addr, align 4, !tbaa !2
  %11 = load i32, i32* %len, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !15
  %conv = zext i8 %12 to i32
  %13 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %add = add nsw i32 %13, %conv
  store i32 %add, i32* %nsymbols, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %len, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %len, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %15, 1
  br i1 %cmp3, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %16 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %cmp5 = icmp sgt i32 %16, 256
  br i1 %cmp5, label %if.then7, label %if.end9

if.then7:                                         ; preds = %lor.lhs.false, %for.end
  %17 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !62
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 8, i32* %msg_code, align 4, !tbaa !12
  %19 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 4, !tbaa !62
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %22 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %21(%struct.jpeg_common_struct* %22)
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %lor.lhs.false
  %23 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %24 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %23, align 4, !tbaa !2
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %24, i32 0, i32 1
  %arraydecay10 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 0
  %25 = load i8*, i8** %val.addr, align 4, !tbaa !2
  %26 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %mul = mul i32 %26, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay10, i8* align 1 %25, i32 %mul, i1 false)
  %27 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %28 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %27, align 4, !tbaa !2
  %huffval11 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %28, i32 0, i32 1
  %29 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval11, i32 0, i32 %29
  %30 = load i32, i32* %nsymbols, align 4, !tbaa !6
  %sub = sub nsw i32 256, %30
  %mul13 = mul i32 %sub, 1
  call void @llvm.memset.p0i8.i32(i8* align 1 %arrayidx12, i8 0, i32 %mul13, i1 false)
  %31 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr.addr, align 4, !tbaa !2
  %32 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %31, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %32, i32 0, i32 2
  store i32 0, i32* %sent_table, align 4, !tbaa !63
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.else
  %33 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %nsymbols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal i32 @process_restart(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %ci = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %3, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %5, i32 0, i32 1
  %bits_left = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 1
  %6 = load i32, i32* %bits_left, align 4, !tbaa !57
  %div = sdiv i32 %6, 8
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 82
  %8 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !65
  %discarded_bytes = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %8, i32 0, i32 6
  %9 = load i32, i32* %discarded_bytes, align 4, !tbaa !66
  %add = add i32 %9, %div
  store i32 %add, i32* %discarded_bytes, align 4, !tbaa !66
  %10 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate2 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %10, i32 0, i32 1
  %bits_left3 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate2, i32 0, i32 1
  store i32 0, i32* %bits_left3, align 4, !tbaa !57
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 82
  %12 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker4, align 8, !tbaa !65
  %read_restart_marker = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %12, i32 0, i32 2
  %13 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %read_restart_marker, align 4, !tbaa !68
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %13(%struct.jpeg_decompress_struct* %14)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %ci, align 4, !tbaa !6
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 66
  %17 = load i32, i32* %comps_in_scan, align 8, !tbaa !50
  %cmp = icmp slt i32 %15, %17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %18, i32 0, i32 2
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 0
  %19 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %19
  store i32 0, i32* %arrayidx, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 50
  %22 = load i32, i32* %restart_interval, align 4, !tbaa !60
  %23 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %23, i32 0, i32 3
  store i32 %22, i32* %restarts_to_go, align 4, !tbaa !61
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 76
  %25 = load i32, i32* %unread_marker, align 8, !tbaa !27
  %cmp5 = icmp eq i32 %25, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %for.end
  %26 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %26, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  store i32 0, i32* %insufficient_data, align 4, !tbaa !59
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end7, %if.then
  %27 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %28 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

; Function Attrs: nounwind
define internal i32 @decode_mcu_fast(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %buffer = alloca i8*, align 4
  %blkn = alloca i32, align 4
  %state = alloca %struct.savable_state, align 4
  %block = alloca [64 x i16]*, align 4
  %dctbl = alloca %struct.d_derived_tbl*, align 4
  %actbl = alloca %struct.d_derived_tbl*, align 4
  %s = alloca i32, align 4
  %k = alloca i32, align 4
  %r = alloca i32, align 4
  %l = alloca i32, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c022 = alloca i32, align 4
  %c123 = alloca i32, align 4
  %c074 = alloca i32, align 4
  %c175 = alloca i32, align 4
  %c094 = alloca i32, align 4
  %c195 = alloca i32, align 4
  %ci = alloca i32, align 4
  %c0154 = alloca i32, align 4
  %c1155 = alloca i32, align 4
  %c0174 = alloca i32, align 4
  %c1175 = alloca i32, align 4
  %c0240 = alloca i32, align 4
  %c1241 = alloca i32, align 4
  %c0260 = alloca i32, align 4
  %c1261 = alloca i32, align 4
  %c0312 = alloca i32, align 4
  %c1313 = alloca i32, align 4
  %c0332 = alloca i32, align 4
  %c1333 = alloca i32, align 4
  %c0398 = alloca i32, align 4
  %c1399 = alloca i32, align 4
  %c0418 = alloca i32, align 4
  %c1419 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %3, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %6) #4
  %7 = bitcast i8** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #4
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo2 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %10, %struct.jpeg_decompress_struct** %cinfo2, align 4, !tbaa !26
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 6
  %12 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !28
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %12, i32 0, i32 0
  %13 = load i8*, i8** %next_input_byte, align 4, !tbaa !31
  %next_input_byte3 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %13, i8** %next_input_byte3, align 4, !tbaa !23
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 6
  %15 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src4, align 8, !tbaa !28
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %15, i32 0, i32 1
  %16 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !32
  %bytes_in_buffer5 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %16, i32* %bytes_in_buffer5, align 4, !tbaa !25
  %17 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %17, i32 0, i32 1
  %get_buffer6 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %18 = load i32, i32* %get_buffer6, align 4, !tbaa !58
  store i32 %18, i32* %get_buffer, align 4, !tbaa !22
  %19 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate7 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %19, i32 0, i32 1
  %bits_left8 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate7, i32 0, i32 1
  %20 = load i32, i32* %bits_left8, align 4, !tbaa !57
  store i32 %20, i32* %bits_left, align 4, !tbaa !6
  %next_input_byte9 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %21 = load i8*, i8** %next_input_byte9, align 4, !tbaa !23
  store i8* %21, i8** %buffer, align 4, !tbaa !2
  %22 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %22, i32 0, i32 2
  %23 = bitcast %struct.savable_state* %state to i8*
  %24 = bitcast %struct.savable_state* %saved to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !69
  store i32 0, i32* %blkn, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc451, %entry
  %25 = load i32, i32* %blkn, align 4, !tbaa !6
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 70
  %27 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !54
  %cmp = icmp slt i32 %25, %27
  br i1 %cmp, label %for.body, label %for.end453

for.body:                                         ; preds = %for.cond
  %28 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %tobool = icmp ne [64 x i16]** %29, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %30 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %31 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %30, i32 %31
  %32 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi [64 x i16]* [ %32, %cond.true ], [ null, %cond.false ]
  store [64 x i16]* %cond, [64 x i16]** %block, align 4, !tbaa !2
  %33 = bitcast %struct.d_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %34, i32 0, i32 6
  %35 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %dc_cur_tbls, i32 0, i32 %35
  %36 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx10, align 4, !tbaa !2
  store %struct.d_derived_tbl* %36, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %37 = bitcast %struct.d_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %38, i32 0, i32 7
  %39 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %ac_cur_tbls, i32 0, i32 %39
  %40 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx11, align 4, !tbaa !2
  store %struct.d_derived_tbl* %40, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %41 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp12 = icmp sle i32 %45, 16
  br i1 %cmp12, label %if.then, label %if.end42

if.then:                                          ; preds = %cond.end
  %46 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #4
  %47 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %48, i32 1
  store i8* %incdec.ptr, i8** %buffer, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !15
  %conv = zext i8 %49 to i32
  store i32 %conv, i32* %c0, align 4, !tbaa !6
  %50 = load i8*, i8** %buffer, align 4, !tbaa !2
  %51 = load i8, i8* %50, align 1, !tbaa !15
  %conv13 = zext i8 %51 to i32
  store i32 %conv13, i32* %c1, align 4, !tbaa !6
  %52 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl = shl i32 %52, 8
  %53 = load i32, i32* %c0, align 4, !tbaa !6
  %or = or i32 %shl, %53
  store i32 %or, i32* %get_buffer, align 4, !tbaa !22
  %54 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add = add nsw i32 %54, 8
  store i32 %add, i32* %bits_left, align 4, !tbaa !6
  %55 = load i32, i32* %c0, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %55, 255
  br i1 %cmp14, label %if.then16, label %if.end21

if.then16:                                        ; preds = %if.then
  %56 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr17 = getelementptr inbounds i8, i8* %56, i32 1
  store i8* %incdec.ptr17, i8** %buffer, align 4, !tbaa !2
  %57 = load i32, i32* %c1, align 4, !tbaa !6
  %cmp18 = icmp ne i32 %57, 0
  br i1 %cmp18, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then16
  %58 = load i32, i32* %c1, align 4, !tbaa !6
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 76
  store i32 %58, i32* %unread_marker, align 8, !tbaa !27
  %60 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %60, i32 -2
  store i8* %add.ptr, i8** %buffer, align 4, !tbaa !2
  %61 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and = and i32 %61, -256
  store i32 %and, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then16
  br label %if.end21

if.end21:                                         ; preds = %if.end, %if.then
  %62 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %63 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  %64 = bitcast i32* %c022 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #4
  %65 = bitcast i32* %c123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  %66 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %66, i32 1
  store i8* %incdec.ptr24, i8** %buffer, align 4, !tbaa !2
  %67 = load i8, i8* %66, align 1, !tbaa !15
  %conv25 = zext i8 %67 to i32
  store i32 %conv25, i32* %c022, align 4, !tbaa !6
  %68 = load i8*, i8** %buffer, align 4, !tbaa !2
  %69 = load i8, i8* %68, align 1, !tbaa !15
  %conv26 = zext i8 %69 to i32
  store i32 %conv26, i32* %c123, align 4, !tbaa !6
  %70 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl27 = shl i32 %70, 8
  %71 = load i32, i32* %c022, align 4, !tbaa !6
  %or28 = or i32 %shl27, %71
  store i32 %or28, i32* %get_buffer, align 4, !tbaa !22
  %72 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add29 = add nsw i32 %72, 8
  store i32 %add29, i32* %bits_left, align 4, !tbaa !6
  %73 = load i32, i32* %c022, align 4, !tbaa !6
  %cmp30 = icmp eq i32 %73, 255
  br i1 %cmp30, label %if.then32, label %if.end41

if.then32:                                        ; preds = %if.end21
  %74 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr33 = getelementptr inbounds i8, i8* %74, i32 1
  store i8* %incdec.ptr33, i8** %buffer, align 4, !tbaa !2
  %75 = load i32, i32* %c123, align 4, !tbaa !6
  %cmp34 = icmp ne i32 %75, 0
  br i1 %cmp34, label %if.then36, label %if.end40

if.then36:                                        ; preds = %if.then32
  %76 = load i32, i32* %c123, align 4, !tbaa !6
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 76
  store i32 %76, i32* %unread_marker37, align 8, !tbaa !27
  %78 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i8, i8* %78, i32 -2
  store i8* %add.ptr38, i8** %buffer, align 4, !tbaa !2
  %79 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and39 = and i32 %79, -256
  store i32 %and39, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end40

if.end40:                                         ; preds = %if.then36, %if.then32
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end21
  %80 = bitcast i32* %c123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i32* %c022 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %cond.end
  %82 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %83 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub = sub nsw i32 %83, 8
  %shr = lshr i32 %82, %sub
  %and43 = and i32 %shr, 255
  store i32 %and43, i32* %s, align 4, !tbaa !6
  %84 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %84, i32 0, i32 3
  %85 = load i32, i32* %s, align 4, !tbaa !6
  %arrayidx44 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %85
  %86 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  store i32 %86, i32* %s, align 4, !tbaa !6
  %87 = load i32, i32* %s, align 4, !tbaa !6
  %shr45 = ashr i32 %87, 8
  store i32 %shr45, i32* %l, align 4, !tbaa !6
  %88 = load i32, i32* %l, align 4, !tbaa !6
  %89 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub46 = sub nsw i32 %89, %88
  store i32 %sub46, i32* %bits_left, align 4, !tbaa !6
  %90 = load i32, i32* %s, align 4, !tbaa !6
  %and47 = and i32 %90, 255
  store i32 %and47, i32* %s, align 4, !tbaa !6
  %91 = load i32, i32* %l, align 4, !tbaa !6
  %cmp48 = icmp sgt i32 %91, 8
  br i1 %cmp48, label %if.then50, label %if.end68

if.then50:                                        ; preds = %if.end42
  %92 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %93 = load i32, i32* %bits_left, align 4, !tbaa !6
  %shr51 = lshr i32 %92, %93
  %94 = load i32, i32* %l, align 4, !tbaa !6
  %shl52 = shl i32 1, %94
  %sub53 = sub nsw i32 %shl52, 1
  %and54 = and i32 %shr51, %sub53
  store i32 %and54, i32* %s, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then50
  %95 = load i32, i32* %s, align 4, !tbaa !6
  %96 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %maxcode = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %96, i32 0, i32 0
  %97 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx55, align 4, !tbaa !22
  %cmp56 = icmp sgt i32 %95, %98
  br i1 %cmp56, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %99 = load i32, i32* %s, align 4, !tbaa !6
  %shl58 = shl i32 %99, 1
  store i32 %shl58, i32* %s, align 4, !tbaa !6
  %100 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %101 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub59 = sub nsw i32 %101, 1
  store i32 %sub59, i32* %bits_left, align 4, !tbaa !6
  %shr60 = lshr i32 %100, %sub59
  %and61 = and i32 %shr60, 1
  %102 = load i32, i32* %s, align 4, !tbaa !6
  %or62 = or i32 %102, %and61
  store i32 %or62, i32* %s, align 4, !tbaa !6
  %103 = load i32, i32* %l, align 4, !tbaa !6
  %inc = add nsw i32 %103, 1
  store i32 %inc, i32* %l, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %104 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %104, i32 0, i32 2
  %105 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %pub, align 4, !tbaa !20
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %105, i32 0, i32 1
  %106 = load i32, i32* %s, align 4, !tbaa !6
  %107 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %valoffset = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %107, i32 0, i32 1
  %108 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset, i32 0, i32 %108
  %109 = load i32, i32* %arrayidx63, align 4, !tbaa !22
  %add64 = add nsw i32 %106, %109
  %and65 = and i32 %add64, 255
  %arrayidx66 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %and65
  %110 = load i8, i8* %arrayidx66, align 1, !tbaa !15
  %conv67 = zext i8 %110 to i32
  store i32 %conv67, i32* %s, align 4, !tbaa !6
  br label %if.end68

if.end68:                                         ; preds = %while.end, %if.end42
  %111 = load i32, i32* %s, align 4, !tbaa !6
  %tobool69 = icmp ne i32 %111, 0
  br i1 %tobool69, label %if.then70, label %if.end128

if.then70:                                        ; preds = %if.end68
  %112 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp71 = icmp sle i32 %112, 16
  br i1 %cmp71, label %if.then73, label %if.end114

if.then73:                                        ; preds = %if.then70
  %113 = bitcast i32* %c074 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #4
  %114 = bitcast i32* %c175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #4
  %115 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr76 = getelementptr inbounds i8, i8* %115, i32 1
  store i8* %incdec.ptr76, i8** %buffer, align 4, !tbaa !2
  %116 = load i8, i8* %115, align 1, !tbaa !15
  %conv77 = zext i8 %116 to i32
  store i32 %conv77, i32* %c074, align 4, !tbaa !6
  %117 = load i8*, i8** %buffer, align 4, !tbaa !2
  %118 = load i8, i8* %117, align 1, !tbaa !15
  %conv78 = zext i8 %118 to i32
  store i32 %conv78, i32* %c175, align 4, !tbaa !6
  %119 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl79 = shl i32 %119, 8
  %120 = load i32, i32* %c074, align 4, !tbaa !6
  %or80 = or i32 %shl79, %120
  store i32 %or80, i32* %get_buffer, align 4, !tbaa !22
  %121 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add81 = add nsw i32 %121, 8
  store i32 %add81, i32* %bits_left, align 4, !tbaa !6
  %122 = load i32, i32* %c074, align 4, !tbaa !6
  %cmp82 = icmp eq i32 %122, 255
  br i1 %cmp82, label %if.then84, label %if.end93

if.then84:                                        ; preds = %if.then73
  %123 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr85 = getelementptr inbounds i8, i8* %123, i32 1
  store i8* %incdec.ptr85, i8** %buffer, align 4, !tbaa !2
  %124 = load i32, i32* %c175, align 4, !tbaa !6
  %cmp86 = icmp ne i32 %124, 0
  br i1 %cmp86, label %if.then88, label %if.end92

if.then88:                                        ; preds = %if.then84
  %125 = load i32, i32* %c175, align 4, !tbaa !6
  %126 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker89 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %126, i32 0, i32 76
  store i32 %125, i32* %unread_marker89, align 8, !tbaa !27
  %127 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr90 = getelementptr inbounds i8, i8* %127, i32 -2
  store i8* %add.ptr90, i8** %buffer, align 4, !tbaa !2
  %128 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and91 = and i32 %128, -256
  store i32 %and91, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end92

if.end92:                                         ; preds = %if.then88, %if.then84
  br label %if.end93

if.end93:                                         ; preds = %if.end92, %if.then73
  %129 = bitcast i32* %c175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #4
  %130 = bitcast i32* %c074 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %c094 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %c195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #4
  %133 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr96 = getelementptr inbounds i8, i8* %133, i32 1
  store i8* %incdec.ptr96, i8** %buffer, align 4, !tbaa !2
  %134 = load i8, i8* %133, align 1, !tbaa !15
  %conv97 = zext i8 %134 to i32
  store i32 %conv97, i32* %c094, align 4, !tbaa !6
  %135 = load i8*, i8** %buffer, align 4, !tbaa !2
  %136 = load i8, i8* %135, align 1, !tbaa !15
  %conv98 = zext i8 %136 to i32
  store i32 %conv98, i32* %c195, align 4, !tbaa !6
  %137 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl99 = shl i32 %137, 8
  %138 = load i32, i32* %c094, align 4, !tbaa !6
  %or100 = or i32 %shl99, %138
  store i32 %or100, i32* %get_buffer, align 4, !tbaa !22
  %139 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add101 = add nsw i32 %139, 8
  store i32 %add101, i32* %bits_left, align 4, !tbaa !6
  %140 = load i32, i32* %c094, align 4, !tbaa !6
  %cmp102 = icmp eq i32 %140, 255
  br i1 %cmp102, label %if.then104, label %if.end113

if.then104:                                       ; preds = %if.end93
  %141 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr105 = getelementptr inbounds i8, i8* %141, i32 1
  store i8* %incdec.ptr105, i8** %buffer, align 4, !tbaa !2
  %142 = load i32, i32* %c195, align 4, !tbaa !6
  %cmp106 = icmp ne i32 %142, 0
  br i1 %cmp106, label %if.then108, label %if.end112

if.then108:                                       ; preds = %if.then104
  %143 = load i32, i32* %c195, align 4, !tbaa !6
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker109 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 76
  store i32 %143, i32* %unread_marker109, align 8, !tbaa !27
  %145 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr110 = getelementptr inbounds i8, i8* %145, i32 -2
  store i8* %add.ptr110, i8** %buffer, align 4, !tbaa !2
  %146 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and111 = and i32 %146, -256
  store i32 %and111, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end112

if.end112:                                        ; preds = %if.then108, %if.then104
  br label %if.end113

if.end113:                                        ; preds = %if.end112, %if.end93
  %147 = bitcast i32* %c195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %c094 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  br label %if.end114

if.end114:                                        ; preds = %if.end113, %if.then70
  %149 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %150 = load i32, i32* %s, align 4, !tbaa !6
  %151 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub115 = sub nsw i32 %151, %150
  store i32 %sub115, i32* %bits_left, align 4, !tbaa !6
  %shr116 = lshr i32 %149, %sub115
  %152 = load i32, i32* %s, align 4, !tbaa !6
  %shl117 = shl i32 1, %152
  %sub118 = sub nsw i32 %shl117, 1
  %and119 = and i32 %shr116, %sub118
  store i32 %and119, i32* %r, align 4, !tbaa !6
  %153 = load i32, i32* %r, align 4, !tbaa !6
  %154 = load i32, i32* %r, align 4, !tbaa !6
  %155 = load i32, i32* %s, align 4, !tbaa !6
  %sub120 = sub nsw i32 %155, 1
  %shl121 = shl i32 1, %sub120
  %sub122 = sub nsw i32 %154, %shl121
  %shr123 = ashr i32 %sub122, 31
  %156 = load i32, i32* %s, align 4, !tbaa !6
  %shl124 = shl i32 -1, %156
  %add125 = add i32 %shl124, 1
  %and126 = and i32 %shr123, %add125
  %add127 = add i32 %153, %and126
  store i32 %add127, i32* %s, align 4, !tbaa !6
  br label %if.end128

if.end128:                                        ; preds = %if.end114, %if.end68
  %157 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %157, i32 0, i32 8
  %158 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx129 = getelementptr inbounds [10 x i32], [10 x i32]* %dc_needed, i32 0, i32 %158
  %159 = load i32, i32* %arrayidx129, align 4, !tbaa !6
  %tobool130 = icmp ne i32 %159, 0
  br i1 %tobool130, label %if.then131, label %if.end142

if.then131:                                       ; preds = %if.end128
  %160 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #4
  %161 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %161, i32 0, i32 71
  %162 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx132 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 %162
  %163 = load i32, i32* %arrayidx132, align 4, !tbaa !6
  store i32 %163, i32* %ci, align 4, !tbaa !6
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 0
  %164 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx133 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %164
  %165 = load i32, i32* %arrayidx133, align 4, !tbaa !6
  %166 = load i32, i32* %s, align 4, !tbaa !6
  %add134 = add nsw i32 %166, %165
  store i32 %add134, i32* %s, align 4, !tbaa !6
  %167 = load i32, i32* %s, align 4, !tbaa !6
  %last_dc_val135 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 0
  %168 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx136 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val135, i32 0, i32 %168
  store i32 %167, i32* %arrayidx136, align 4, !tbaa !6
  %169 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %tobool137 = icmp ne [64 x i16]* %169, null
  br i1 %tobool137, label %if.then138, label %if.end141

if.then138:                                       ; preds = %if.then131
  %170 = load i32, i32* %s, align 4, !tbaa !6
  %conv139 = trunc i32 %170 to i16
  %171 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds [64 x i16], [64 x i16]* %171, i32 0, i32 0
  store i16 %conv139, i16* %arrayidx140, align 2, !tbaa !70
  br label %if.end141

if.end141:                                        ; preds = %if.then138, %if.then131
  %172 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #4
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %if.end128
  %173 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %173, i32 0, i32 9
  %174 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx143 = getelementptr inbounds [10 x i32], [10 x i32]* %ac_needed, i32 0, i32 %174
  %175 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  %tobool144 = icmp ne i32 %175, 0
  br i1 %tobool144, label %land.lhs.true, label %if.else304

land.lhs.true:                                    ; preds = %if.end142
  %176 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %tobool145 = icmp ne [64 x i16]* %176, null
  br i1 %tobool145, label %if.then146, label %if.else304

if.then146:                                       ; preds = %land.lhs.true
  store i32 1, i32* %k, align 4, !tbaa !6
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc, %if.then146
  %177 = load i32, i32* %k, align 4, !tbaa !6
  %cmp148 = icmp slt i32 %177, 64
  br i1 %cmp148, label %for.body150, label %for.end

for.body150:                                      ; preds = %for.cond147
  %178 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp151 = icmp sle i32 %178, 16
  br i1 %cmp151, label %if.then153, label %if.end194

if.then153:                                       ; preds = %for.body150
  %179 = bitcast i32* %c0154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #4
  %180 = bitcast i32* %c1155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #4
  %181 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr156 = getelementptr inbounds i8, i8* %181, i32 1
  store i8* %incdec.ptr156, i8** %buffer, align 4, !tbaa !2
  %182 = load i8, i8* %181, align 1, !tbaa !15
  %conv157 = zext i8 %182 to i32
  store i32 %conv157, i32* %c0154, align 4, !tbaa !6
  %183 = load i8*, i8** %buffer, align 4, !tbaa !2
  %184 = load i8, i8* %183, align 1, !tbaa !15
  %conv158 = zext i8 %184 to i32
  store i32 %conv158, i32* %c1155, align 4, !tbaa !6
  %185 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl159 = shl i32 %185, 8
  %186 = load i32, i32* %c0154, align 4, !tbaa !6
  %or160 = or i32 %shl159, %186
  store i32 %or160, i32* %get_buffer, align 4, !tbaa !22
  %187 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add161 = add nsw i32 %187, 8
  store i32 %add161, i32* %bits_left, align 4, !tbaa !6
  %188 = load i32, i32* %c0154, align 4, !tbaa !6
  %cmp162 = icmp eq i32 %188, 255
  br i1 %cmp162, label %if.then164, label %if.end173

if.then164:                                       ; preds = %if.then153
  %189 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr165 = getelementptr inbounds i8, i8* %189, i32 1
  store i8* %incdec.ptr165, i8** %buffer, align 4, !tbaa !2
  %190 = load i32, i32* %c1155, align 4, !tbaa !6
  %cmp166 = icmp ne i32 %190, 0
  br i1 %cmp166, label %if.then168, label %if.end172

if.then168:                                       ; preds = %if.then164
  %191 = load i32, i32* %c1155, align 4, !tbaa !6
  %192 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker169 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %192, i32 0, i32 76
  store i32 %191, i32* %unread_marker169, align 8, !tbaa !27
  %193 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr170 = getelementptr inbounds i8, i8* %193, i32 -2
  store i8* %add.ptr170, i8** %buffer, align 4, !tbaa !2
  %194 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and171 = and i32 %194, -256
  store i32 %and171, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end172

if.end172:                                        ; preds = %if.then168, %if.then164
  br label %if.end173

if.end173:                                        ; preds = %if.end172, %if.then153
  %195 = bitcast i32* %c1155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i32* %c0154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32* %c0174 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32* %c1175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #4
  %199 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr176 = getelementptr inbounds i8, i8* %199, i32 1
  store i8* %incdec.ptr176, i8** %buffer, align 4, !tbaa !2
  %200 = load i8, i8* %199, align 1, !tbaa !15
  %conv177 = zext i8 %200 to i32
  store i32 %conv177, i32* %c0174, align 4, !tbaa !6
  %201 = load i8*, i8** %buffer, align 4, !tbaa !2
  %202 = load i8, i8* %201, align 1, !tbaa !15
  %conv178 = zext i8 %202 to i32
  store i32 %conv178, i32* %c1175, align 4, !tbaa !6
  %203 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl179 = shl i32 %203, 8
  %204 = load i32, i32* %c0174, align 4, !tbaa !6
  %or180 = or i32 %shl179, %204
  store i32 %or180, i32* %get_buffer, align 4, !tbaa !22
  %205 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add181 = add nsw i32 %205, 8
  store i32 %add181, i32* %bits_left, align 4, !tbaa !6
  %206 = load i32, i32* %c0174, align 4, !tbaa !6
  %cmp182 = icmp eq i32 %206, 255
  br i1 %cmp182, label %if.then184, label %if.end193

if.then184:                                       ; preds = %if.end173
  %207 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr185 = getelementptr inbounds i8, i8* %207, i32 1
  store i8* %incdec.ptr185, i8** %buffer, align 4, !tbaa !2
  %208 = load i32, i32* %c1175, align 4, !tbaa !6
  %cmp186 = icmp ne i32 %208, 0
  br i1 %cmp186, label %if.then188, label %if.end192

if.then188:                                       ; preds = %if.then184
  %209 = load i32, i32* %c1175, align 4, !tbaa !6
  %210 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker189 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %210, i32 0, i32 76
  store i32 %209, i32* %unread_marker189, align 8, !tbaa !27
  %211 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr190 = getelementptr inbounds i8, i8* %211, i32 -2
  store i8* %add.ptr190, i8** %buffer, align 4, !tbaa !2
  %212 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and191 = and i32 %212, -256
  store i32 %and191, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end192

if.end192:                                        ; preds = %if.then188, %if.then184
  br label %if.end193

if.end193:                                        ; preds = %if.end192, %if.end173
  %213 = bitcast i32* %c1175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %c0174 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  br label %if.end194

if.end194:                                        ; preds = %if.end193, %for.body150
  %215 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %216 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub195 = sub nsw i32 %216, 8
  %shr196 = lshr i32 %215, %sub195
  %and197 = and i32 %shr196, 255
  store i32 %and197, i32* %s, align 4, !tbaa !6
  %217 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup198 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %217, i32 0, i32 3
  %218 = load i32, i32* %s, align 4, !tbaa !6
  %arrayidx199 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup198, i32 0, i32 %218
  %219 = load i32, i32* %arrayidx199, align 4, !tbaa !6
  store i32 %219, i32* %s, align 4, !tbaa !6
  %220 = load i32, i32* %s, align 4, !tbaa !6
  %shr200 = ashr i32 %220, 8
  store i32 %shr200, i32* %l, align 4, !tbaa !6
  %221 = load i32, i32* %l, align 4, !tbaa !6
  %222 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub201 = sub nsw i32 %222, %221
  store i32 %sub201, i32* %bits_left, align 4, !tbaa !6
  %223 = load i32, i32* %s, align 4, !tbaa !6
  %and202 = and i32 %223, 255
  store i32 %and202, i32* %s, align 4, !tbaa !6
  %224 = load i32, i32* %l, align 4, !tbaa !6
  %cmp203 = icmp sgt i32 %224, 8
  br i1 %cmp203, label %if.then205, label %if.end231

if.then205:                                       ; preds = %if.end194
  %225 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %226 = load i32, i32* %bits_left, align 4, !tbaa !6
  %shr206 = lshr i32 %225, %226
  %227 = load i32, i32* %l, align 4, !tbaa !6
  %shl207 = shl i32 1, %227
  %sub208 = sub nsw i32 %shl207, 1
  %and209 = and i32 %shr206, %sub208
  store i32 %and209, i32* %s, align 4, !tbaa !6
  br label %while.cond210

while.cond210:                                    ; preds = %while.body215, %if.then205
  %228 = load i32, i32* %s, align 4, !tbaa !6
  %229 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %maxcode211 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %229, i32 0, i32 0
  %230 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx212 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode211, i32 0, i32 %230
  %231 = load i32, i32* %arrayidx212, align 4, !tbaa !22
  %cmp213 = icmp sgt i32 %228, %231
  br i1 %cmp213, label %while.body215, label %while.end222

while.body215:                                    ; preds = %while.cond210
  %232 = load i32, i32* %s, align 4, !tbaa !6
  %shl216 = shl i32 %232, 1
  store i32 %shl216, i32* %s, align 4, !tbaa !6
  %233 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %234 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub217 = sub nsw i32 %234, 1
  store i32 %sub217, i32* %bits_left, align 4, !tbaa !6
  %shr218 = lshr i32 %233, %sub217
  %and219 = and i32 %shr218, 1
  %235 = load i32, i32* %s, align 4, !tbaa !6
  %or220 = or i32 %235, %and219
  store i32 %or220, i32* %s, align 4, !tbaa !6
  %236 = load i32, i32* %l, align 4, !tbaa !6
  %inc221 = add nsw i32 %236, 1
  store i32 %inc221, i32* %l, align 4, !tbaa !6
  br label %while.cond210

while.end222:                                     ; preds = %while.cond210
  %237 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %pub223 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %237, i32 0, i32 2
  %238 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %pub223, align 4, !tbaa !20
  %huffval224 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %238, i32 0, i32 1
  %239 = load i32, i32* %s, align 4, !tbaa !6
  %240 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %valoffset225 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %240, i32 0, i32 1
  %241 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx226 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset225, i32 0, i32 %241
  %242 = load i32, i32* %arrayidx226, align 4, !tbaa !22
  %add227 = add nsw i32 %239, %242
  %and228 = and i32 %add227, 255
  %arrayidx229 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval224, i32 0, i32 %and228
  %243 = load i8, i8* %arrayidx229, align 1, !tbaa !15
  %conv230 = zext i8 %243 to i32
  store i32 %conv230, i32* %s, align 4, !tbaa !6
  br label %if.end231

if.end231:                                        ; preds = %while.end222, %if.end194
  %244 = load i32, i32* %s, align 4, !tbaa !6
  %shr232 = ashr i32 %244, 4
  store i32 %shr232, i32* %r, align 4, !tbaa !6
  %245 = load i32, i32* %s, align 4, !tbaa !6
  %and233 = and i32 %245, 15
  store i32 %and233, i32* %s, align 4, !tbaa !6
  %246 = load i32, i32* %s, align 4, !tbaa !6
  %tobool234 = icmp ne i32 %246, 0
  br i1 %tobool234, label %if.then235, label %if.else

if.then235:                                       ; preds = %if.end231
  %247 = load i32, i32* %r, align 4, !tbaa !6
  %248 = load i32, i32* %k, align 4, !tbaa !6
  %add236 = add nsw i32 %248, %247
  store i32 %add236, i32* %k, align 4, !tbaa !6
  %249 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp237 = icmp sle i32 %249, 16
  br i1 %cmp237, label %if.then239, label %if.end280

if.then239:                                       ; preds = %if.then235
  %250 = bitcast i32* %c0240 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %250) #4
  %251 = bitcast i32* %c1241 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #4
  %252 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr242 = getelementptr inbounds i8, i8* %252, i32 1
  store i8* %incdec.ptr242, i8** %buffer, align 4, !tbaa !2
  %253 = load i8, i8* %252, align 1, !tbaa !15
  %conv243 = zext i8 %253 to i32
  store i32 %conv243, i32* %c0240, align 4, !tbaa !6
  %254 = load i8*, i8** %buffer, align 4, !tbaa !2
  %255 = load i8, i8* %254, align 1, !tbaa !15
  %conv244 = zext i8 %255 to i32
  store i32 %conv244, i32* %c1241, align 4, !tbaa !6
  %256 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl245 = shl i32 %256, 8
  %257 = load i32, i32* %c0240, align 4, !tbaa !6
  %or246 = or i32 %shl245, %257
  store i32 %or246, i32* %get_buffer, align 4, !tbaa !22
  %258 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add247 = add nsw i32 %258, 8
  store i32 %add247, i32* %bits_left, align 4, !tbaa !6
  %259 = load i32, i32* %c0240, align 4, !tbaa !6
  %cmp248 = icmp eq i32 %259, 255
  br i1 %cmp248, label %if.then250, label %if.end259

if.then250:                                       ; preds = %if.then239
  %260 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr251 = getelementptr inbounds i8, i8* %260, i32 1
  store i8* %incdec.ptr251, i8** %buffer, align 4, !tbaa !2
  %261 = load i32, i32* %c1241, align 4, !tbaa !6
  %cmp252 = icmp ne i32 %261, 0
  br i1 %cmp252, label %if.then254, label %if.end258

if.then254:                                       ; preds = %if.then250
  %262 = load i32, i32* %c1241, align 4, !tbaa !6
  %263 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker255 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %263, i32 0, i32 76
  store i32 %262, i32* %unread_marker255, align 8, !tbaa !27
  %264 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr256 = getelementptr inbounds i8, i8* %264, i32 -2
  store i8* %add.ptr256, i8** %buffer, align 4, !tbaa !2
  %265 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and257 = and i32 %265, -256
  store i32 %and257, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end258

if.end258:                                        ; preds = %if.then254, %if.then250
  br label %if.end259

if.end259:                                        ; preds = %if.end258, %if.then239
  %266 = bitcast i32* %c1241 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #4
  %267 = bitcast i32* %c0240 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #4
  %268 = bitcast i32* %c0260 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %268) #4
  %269 = bitcast i32* %c1261 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %269) #4
  %270 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr262 = getelementptr inbounds i8, i8* %270, i32 1
  store i8* %incdec.ptr262, i8** %buffer, align 4, !tbaa !2
  %271 = load i8, i8* %270, align 1, !tbaa !15
  %conv263 = zext i8 %271 to i32
  store i32 %conv263, i32* %c0260, align 4, !tbaa !6
  %272 = load i8*, i8** %buffer, align 4, !tbaa !2
  %273 = load i8, i8* %272, align 1, !tbaa !15
  %conv264 = zext i8 %273 to i32
  store i32 %conv264, i32* %c1261, align 4, !tbaa !6
  %274 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl265 = shl i32 %274, 8
  %275 = load i32, i32* %c0260, align 4, !tbaa !6
  %or266 = or i32 %shl265, %275
  store i32 %or266, i32* %get_buffer, align 4, !tbaa !22
  %276 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add267 = add nsw i32 %276, 8
  store i32 %add267, i32* %bits_left, align 4, !tbaa !6
  %277 = load i32, i32* %c0260, align 4, !tbaa !6
  %cmp268 = icmp eq i32 %277, 255
  br i1 %cmp268, label %if.then270, label %if.end279

if.then270:                                       ; preds = %if.end259
  %278 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr271 = getelementptr inbounds i8, i8* %278, i32 1
  store i8* %incdec.ptr271, i8** %buffer, align 4, !tbaa !2
  %279 = load i32, i32* %c1261, align 4, !tbaa !6
  %cmp272 = icmp ne i32 %279, 0
  br i1 %cmp272, label %if.then274, label %if.end278

if.then274:                                       ; preds = %if.then270
  %280 = load i32, i32* %c1261, align 4, !tbaa !6
  %281 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker275 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %281, i32 0, i32 76
  store i32 %280, i32* %unread_marker275, align 8, !tbaa !27
  %282 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr276 = getelementptr inbounds i8, i8* %282, i32 -2
  store i8* %add.ptr276, i8** %buffer, align 4, !tbaa !2
  %283 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and277 = and i32 %283, -256
  store i32 %and277, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end278

if.end278:                                        ; preds = %if.then274, %if.then270
  br label %if.end279

if.end279:                                        ; preds = %if.end278, %if.end259
  %284 = bitcast i32* %c1261 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #4
  %285 = bitcast i32* %c0260 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #4
  br label %if.end280

if.end280:                                        ; preds = %if.end279, %if.then235
  %286 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %287 = load i32, i32* %s, align 4, !tbaa !6
  %288 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub281 = sub nsw i32 %288, %287
  store i32 %sub281, i32* %bits_left, align 4, !tbaa !6
  %shr282 = lshr i32 %286, %sub281
  %289 = load i32, i32* %s, align 4, !tbaa !6
  %shl283 = shl i32 1, %289
  %sub284 = sub nsw i32 %shl283, 1
  %and285 = and i32 %shr282, %sub284
  store i32 %and285, i32* %r, align 4, !tbaa !6
  %290 = load i32, i32* %r, align 4, !tbaa !6
  %291 = load i32, i32* %r, align 4, !tbaa !6
  %292 = load i32, i32* %s, align 4, !tbaa !6
  %sub286 = sub nsw i32 %292, 1
  %shl287 = shl i32 1, %sub286
  %sub288 = sub nsw i32 %291, %shl287
  %shr289 = ashr i32 %sub288, 31
  %293 = load i32, i32* %s, align 4, !tbaa !6
  %shl290 = shl i32 -1, %293
  %add291 = add i32 %shl290, 1
  %and292 = and i32 %shr289, %add291
  %add293 = add i32 %290, %and292
  store i32 %add293, i32* %s, align 4, !tbaa !6
  %294 = load i32, i32* %s, align 4, !tbaa !6
  %conv294 = trunc i32 %294 to i16
  %295 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %296 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx295 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %296
  %297 = load i32, i32* %arrayidx295, align 4, !tbaa !6
  %arrayidx296 = getelementptr inbounds [64 x i16], [64 x i16]* %295, i32 0, i32 %297
  store i16 %conv294, i16* %arrayidx296, align 2, !tbaa !70
  br label %if.end302

if.else:                                          ; preds = %if.end231
  %298 = load i32, i32* %r, align 4, !tbaa !6
  %cmp297 = icmp ne i32 %298, 15
  br i1 %cmp297, label %if.then299, label %if.end300

if.then299:                                       ; preds = %if.else
  br label %for.end

if.end300:                                        ; preds = %if.else
  %299 = load i32, i32* %k, align 4, !tbaa !6
  %add301 = add nsw i32 %299, 15
  store i32 %add301, i32* %k, align 4, !tbaa !6
  br label %if.end302

if.end302:                                        ; preds = %if.end300, %if.end280
  br label %for.inc

for.inc:                                          ; preds = %if.end302
  %300 = load i32, i32* %k, align 4, !tbaa !6
  %inc303 = add nsw i32 %300, 1
  store i32 %inc303, i32* %k, align 4, !tbaa !6
  br label %for.cond147

for.end:                                          ; preds = %if.then299, %for.cond147
  br label %if.end450

if.else304:                                       ; preds = %land.lhs.true, %if.end142
  store i32 1, i32* %k, align 4, !tbaa !6
  br label %for.cond305

for.cond305:                                      ; preds = %for.inc447, %if.else304
  %301 = load i32, i32* %k, align 4, !tbaa !6
  %cmp306 = icmp slt i32 %301, 64
  br i1 %cmp306, label %for.body308, label %for.end449

for.body308:                                      ; preds = %for.cond305
  %302 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp309 = icmp sle i32 %302, 16
  br i1 %cmp309, label %if.then311, label %if.end352

if.then311:                                       ; preds = %for.body308
  %303 = bitcast i32* %c0312 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #4
  %304 = bitcast i32* %c1313 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %304) #4
  %305 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr314 = getelementptr inbounds i8, i8* %305, i32 1
  store i8* %incdec.ptr314, i8** %buffer, align 4, !tbaa !2
  %306 = load i8, i8* %305, align 1, !tbaa !15
  %conv315 = zext i8 %306 to i32
  store i32 %conv315, i32* %c0312, align 4, !tbaa !6
  %307 = load i8*, i8** %buffer, align 4, !tbaa !2
  %308 = load i8, i8* %307, align 1, !tbaa !15
  %conv316 = zext i8 %308 to i32
  store i32 %conv316, i32* %c1313, align 4, !tbaa !6
  %309 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl317 = shl i32 %309, 8
  %310 = load i32, i32* %c0312, align 4, !tbaa !6
  %or318 = or i32 %shl317, %310
  store i32 %or318, i32* %get_buffer, align 4, !tbaa !22
  %311 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add319 = add nsw i32 %311, 8
  store i32 %add319, i32* %bits_left, align 4, !tbaa !6
  %312 = load i32, i32* %c0312, align 4, !tbaa !6
  %cmp320 = icmp eq i32 %312, 255
  br i1 %cmp320, label %if.then322, label %if.end331

if.then322:                                       ; preds = %if.then311
  %313 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr323 = getelementptr inbounds i8, i8* %313, i32 1
  store i8* %incdec.ptr323, i8** %buffer, align 4, !tbaa !2
  %314 = load i32, i32* %c1313, align 4, !tbaa !6
  %cmp324 = icmp ne i32 %314, 0
  br i1 %cmp324, label %if.then326, label %if.end330

if.then326:                                       ; preds = %if.then322
  %315 = load i32, i32* %c1313, align 4, !tbaa !6
  %316 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker327 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %316, i32 0, i32 76
  store i32 %315, i32* %unread_marker327, align 8, !tbaa !27
  %317 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr328 = getelementptr inbounds i8, i8* %317, i32 -2
  store i8* %add.ptr328, i8** %buffer, align 4, !tbaa !2
  %318 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and329 = and i32 %318, -256
  store i32 %and329, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end330

if.end330:                                        ; preds = %if.then326, %if.then322
  br label %if.end331

if.end331:                                        ; preds = %if.end330, %if.then311
  %319 = bitcast i32* %c1313 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #4
  %320 = bitcast i32* %c0312 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #4
  %321 = bitcast i32* %c0332 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %321) #4
  %322 = bitcast i32* %c1333 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %322) #4
  %323 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr334 = getelementptr inbounds i8, i8* %323, i32 1
  store i8* %incdec.ptr334, i8** %buffer, align 4, !tbaa !2
  %324 = load i8, i8* %323, align 1, !tbaa !15
  %conv335 = zext i8 %324 to i32
  store i32 %conv335, i32* %c0332, align 4, !tbaa !6
  %325 = load i8*, i8** %buffer, align 4, !tbaa !2
  %326 = load i8, i8* %325, align 1, !tbaa !15
  %conv336 = zext i8 %326 to i32
  store i32 %conv336, i32* %c1333, align 4, !tbaa !6
  %327 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl337 = shl i32 %327, 8
  %328 = load i32, i32* %c0332, align 4, !tbaa !6
  %or338 = or i32 %shl337, %328
  store i32 %or338, i32* %get_buffer, align 4, !tbaa !22
  %329 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add339 = add nsw i32 %329, 8
  store i32 %add339, i32* %bits_left, align 4, !tbaa !6
  %330 = load i32, i32* %c0332, align 4, !tbaa !6
  %cmp340 = icmp eq i32 %330, 255
  br i1 %cmp340, label %if.then342, label %if.end351

if.then342:                                       ; preds = %if.end331
  %331 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr343 = getelementptr inbounds i8, i8* %331, i32 1
  store i8* %incdec.ptr343, i8** %buffer, align 4, !tbaa !2
  %332 = load i32, i32* %c1333, align 4, !tbaa !6
  %cmp344 = icmp ne i32 %332, 0
  br i1 %cmp344, label %if.then346, label %if.end350

if.then346:                                       ; preds = %if.then342
  %333 = load i32, i32* %c1333, align 4, !tbaa !6
  %334 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker347 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %334, i32 0, i32 76
  store i32 %333, i32* %unread_marker347, align 8, !tbaa !27
  %335 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr348 = getelementptr inbounds i8, i8* %335, i32 -2
  store i8* %add.ptr348, i8** %buffer, align 4, !tbaa !2
  %336 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and349 = and i32 %336, -256
  store i32 %and349, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end350

if.end350:                                        ; preds = %if.then346, %if.then342
  br label %if.end351

if.end351:                                        ; preds = %if.end350, %if.end331
  %337 = bitcast i32* %c1333 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #4
  %338 = bitcast i32* %c0332 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #4
  br label %if.end352

if.end352:                                        ; preds = %if.end351, %for.body308
  %339 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %340 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub353 = sub nsw i32 %340, 8
  %shr354 = lshr i32 %339, %sub353
  %and355 = and i32 %shr354, 255
  store i32 %and355, i32* %s, align 4, !tbaa !6
  %341 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup356 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %341, i32 0, i32 3
  %342 = load i32, i32* %s, align 4, !tbaa !6
  %arrayidx357 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup356, i32 0, i32 %342
  %343 = load i32, i32* %arrayidx357, align 4, !tbaa !6
  store i32 %343, i32* %s, align 4, !tbaa !6
  %344 = load i32, i32* %s, align 4, !tbaa !6
  %shr358 = ashr i32 %344, 8
  store i32 %shr358, i32* %l, align 4, !tbaa !6
  %345 = load i32, i32* %l, align 4, !tbaa !6
  %346 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub359 = sub nsw i32 %346, %345
  store i32 %sub359, i32* %bits_left, align 4, !tbaa !6
  %347 = load i32, i32* %s, align 4, !tbaa !6
  %and360 = and i32 %347, 255
  store i32 %and360, i32* %s, align 4, !tbaa !6
  %348 = load i32, i32* %l, align 4, !tbaa !6
  %cmp361 = icmp sgt i32 %348, 8
  br i1 %cmp361, label %if.then363, label %if.end389

if.then363:                                       ; preds = %if.end352
  %349 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %350 = load i32, i32* %bits_left, align 4, !tbaa !6
  %shr364 = lshr i32 %349, %350
  %351 = load i32, i32* %l, align 4, !tbaa !6
  %shl365 = shl i32 1, %351
  %sub366 = sub nsw i32 %shl365, 1
  %and367 = and i32 %shr364, %sub366
  store i32 %and367, i32* %s, align 4, !tbaa !6
  br label %while.cond368

while.cond368:                                    ; preds = %while.body373, %if.then363
  %352 = load i32, i32* %s, align 4, !tbaa !6
  %353 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %maxcode369 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %353, i32 0, i32 0
  %354 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx370 = getelementptr inbounds [18 x i32], [18 x i32]* %maxcode369, i32 0, i32 %354
  %355 = load i32, i32* %arrayidx370, align 4, !tbaa !22
  %cmp371 = icmp sgt i32 %352, %355
  br i1 %cmp371, label %while.body373, label %while.end380

while.body373:                                    ; preds = %while.cond368
  %356 = load i32, i32* %s, align 4, !tbaa !6
  %shl374 = shl i32 %356, 1
  store i32 %shl374, i32* %s, align 4, !tbaa !6
  %357 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %358 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub375 = sub nsw i32 %358, 1
  store i32 %sub375, i32* %bits_left, align 4, !tbaa !6
  %shr376 = lshr i32 %357, %sub375
  %and377 = and i32 %shr376, 1
  %359 = load i32, i32* %s, align 4, !tbaa !6
  %or378 = or i32 %359, %and377
  store i32 %or378, i32* %s, align 4, !tbaa !6
  %360 = load i32, i32* %l, align 4, !tbaa !6
  %inc379 = add nsw i32 %360, 1
  store i32 %inc379, i32* %l, align 4, !tbaa !6
  br label %while.cond368

while.end380:                                     ; preds = %while.cond368
  %361 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %pub381 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %361, i32 0, i32 2
  %362 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %pub381, align 4, !tbaa !20
  %huffval382 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %362, i32 0, i32 1
  %363 = load i32, i32* %s, align 4, !tbaa !6
  %364 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %valoffset383 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %364, i32 0, i32 1
  %365 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx384 = getelementptr inbounds [18 x i32], [18 x i32]* %valoffset383, i32 0, i32 %365
  %366 = load i32, i32* %arrayidx384, align 4, !tbaa !22
  %add385 = add nsw i32 %363, %366
  %and386 = and i32 %add385, 255
  %arrayidx387 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval382, i32 0, i32 %and386
  %367 = load i8, i8* %arrayidx387, align 1, !tbaa !15
  %conv388 = zext i8 %367 to i32
  store i32 %conv388, i32* %s, align 4, !tbaa !6
  br label %if.end389

if.end389:                                        ; preds = %while.end380, %if.end352
  %368 = load i32, i32* %s, align 4, !tbaa !6
  %shr390 = ashr i32 %368, 4
  store i32 %shr390, i32* %r, align 4, !tbaa !6
  %369 = load i32, i32* %s, align 4, !tbaa !6
  %and391 = and i32 %369, 15
  store i32 %and391, i32* %s, align 4, !tbaa !6
  %370 = load i32, i32* %s, align 4, !tbaa !6
  %tobool392 = icmp ne i32 %370, 0
  br i1 %tobool392, label %if.then393, label %if.else440

if.then393:                                       ; preds = %if.end389
  %371 = load i32, i32* %r, align 4, !tbaa !6
  %372 = load i32, i32* %k, align 4, !tbaa !6
  %add394 = add nsw i32 %372, %371
  store i32 %add394, i32* %k, align 4, !tbaa !6
  %373 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp395 = icmp sle i32 %373, 16
  br i1 %cmp395, label %if.then397, label %if.end438

if.then397:                                       ; preds = %if.then393
  %374 = bitcast i32* %c0398 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %374) #4
  %375 = bitcast i32* %c1399 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %375) #4
  %376 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr400 = getelementptr inbounds i8, i8* %376, i32 1
  store i8* %incdec.ptr400, i8** %buffer, align 4, !tbaa !2
  %377 = load i8, i8* %376, align 1, !tbaa !15
  %conv401 = zext i8 %377 to i32
  store i32 %conv401, i32* %c0398, align 4, !tbaa !6
  %378 = load i8*, i8** %buffer, align 4, !tbaa !2
  %379 = load i8, i8* %378, align 1, !tbaa !15
  %conv402 = zext i8 %379 to i32
  store i32 %conv402, i32* %c1399, align 4, !tbaa !6
  %380 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl403 = shl i32 %380, 8
  %381 = load i32, i32* %c0398, align 4, !tbaa !6
  %or404 = or i32 %shl403, %381
  store i32 %or404, i32* %get_buffer, align 4, !tbaa !22
  %382 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add405 = add nsw i32 %382, 8
  store i32 %add405, i32* %bits_left, align 4, !tbaa !6
  %383 = load i32, i32* %c0398, align 4, !tbaa !6
  %cmp406 = icmp eq i32 %383, 255
  br i1 %cmp406, label %if.then408, label %if.end417

if.then408:                                       ; preds = %if.then397
  %384 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr409 = getelementptr inbounds i8, i8* %384, i32 1
  store i8* %incdec.ptr409, i8** %buffer, align 4, !tbaa !2
  %385 = load i32, i32* %c1399, align 4, !tbaa !6
  %cmp410 = icmp ne i32 %385, 0
  br i1 %cmp410, label %if.then412, label %if.end416

if.then412:                                       ; preds = %if.then408
  %386 = load i32, i32* %c1399, align 4, !tbaa !6
  %387 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker413 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %387, i32 0, i32 76
  store i32 %386, i32* %unread_marker413, align 8, !tbaa !27
  %388 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr414 = getelementptr inbounds i8, i8* %388, i32 -2
  store i8* %add.ptr414, i8** %buffer, align 4, !tbaa !2
  %389 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and415 = and i32 %389, -256
  store i32 %and415, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end416

if.end416:                                        ; preds = %if.then412, %if.then408
  br label %if.end417

if.end417:                                        ; preds = %if.end416, %if.then397
  %390 = bitcast i32* %c1399 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %390) #4
  %391 = bitcast i32* %c0398 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #4
  %392 = bitcast i32* %c0418 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %392) #4
  %393 = bitcast i32* %c1419 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %393) #4
  %394 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr420 = getelementptr inbounds i8, i8* %394, i32 1
  store i8* %incdec.ptr420, i8** %buffer, align 4, !tbaa !2
  %395 = load i8, i8* %394, align 1, !tbaa !15
  %conv421 = zext i8 %395 to i32
  store i32 %conv421, i32* %c0418, align 4, !tbaa !6
  %396 = load i8*, i8** %buffer, align 4, !tbaa !2
  %397 = load i8, i8* %396, align 1, !tbaa !15
  %conv422 = zext i8 %397 to i32
  store i32 %conv422, i32* %c1419, align 4, !tbaa !6
  %398 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %shl423 = shl i32 %398, 8
  %399 = load i32, i32* %c0418, align 4, !tbaa !6
  %or424 = or i32 %shl423, %399
  store i32 %or424, i32* %get_buffer, align 4, !tbaa !22
  %400 = load i32, i32* %bits_left, align 4, !tbaa !6
  %add425 = add nsw i32 %400, 8
  store i32 %add425, i32* %bits_left, align 4, !tbaa !6
  %401 = load i32, i32* %c0418, align 4, !tbaa !6
  %cmp426 = icmp eq i32 %401, 255
  br i1 %cmp426, label %if.then428, label %if.end437

if.then428:                                       ; preds = %if.end417
  %402 = load i8*, i8** %buffer, align 4, !tbaa !2
  %incdec.ptr429 = getelementptr inbounds i8, i8* %402, i32 1
  store i8* %incdec.ptr429, i8** %buffer, align 4, !tbaa !2
  %403 = load i32, i32* %c1419, align 4, !tbaa !6
  %cmp430 = icmp ne i32 %403, 0
  br i1 %cmp430, label %if.then432, label %if.end436

if.then432:                                       ; preds = %if.then428
  %404 = load i32, i32* %c1419, align 4, !tbaa !6
  %405 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker433 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %405, i32 0, i32 76
  store i32 %404, i32* %unread_marker433, align 8, !tbaa !27
  %406 = load i8*, i8** %buffer, align 4, !tbaa !2
  %add.ptr434 = getelementptr inbounds i8, i8* %406, i32 -2
  store i8* %add.ptr434, i8** %buffer, align 4, !tbaa !2
  %407 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %and435 = and i32 %407, -256
  store i32 %and435, i32* %get_buffer, align 4, !tbaa !22
  br label %if.end436

if.end436:                                        ; preds = %if.then432, %if.then428
  br label %if.end437

if.end437:                                        ; preds = %if.end436, %if.end417
  %408 = bitcast i32* %c1419 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %408) #4
  %409 = bitcast i32* %c0418 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %409) #4
  br label %if.end438

if.end438:                                        ; preds = %if.end437, %if.then393
  %410 = load i32, i32* %s, align 4, !tbaa !6
  %411 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub439 = sub nsw i32 %411, %410
  store i32 %sub439, i32* %bits_left, align 4, !tbaa !6
  br label %if.end446

if.else440:                                       ; preds = %if.end389
  %412 = load i32, i32* %r, align 4, !tbaa !6
  %cmp441 = icmp ne i32 %412, 15
  br i1 %cmp441, label %if.then443, label %if.end444

if.then443:                                       ; preds = %if.else440
  br label %for.end449

if.end444:                                        ; preds = %if.else440
  %413 = load i32, i32* %k, align 4, !tbaa !6
  %add445 = add nsw i32 %413, 15
  store i32 %add445, i32* %k, align 4, !tbaa !6
  br label %if.end446

if.end446:                                        ; preds = %if.end444, %if.end438
  br label %for.inc447

for.inc447:                                       ; preds = %if.end446
  %414 = load i32, i32* %k, align 4, !tbaa !6
  %inc448 = add nsw i32 %414, 1
  store i32 %inc448, i32* %k, align 4, !tbaa !6
  br label %for.cond305

for.end449:                                       ; preds = %if.then443, %for.cond305
  br label %if.end450

if.end450:                                        ; preds = %for.end449, %for.end
  %415 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #4
  %416 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %416) #4
  %417 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #4
  %418 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %418) #4
  %419 = bitcast %struct.d_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %419) #4
  %420 = bitcast %struct.d_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %420) #4
  %421 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #4
  br label %for.inc451

for.inc451:                                       ; preds = %if.end450
  %422 = load i32, i32* %blkn, align 4, !tbaa !6
  %inc452 = add nsw i32 %422, 1
  store i32 %inc452, i32* %blkn, align 4, !tbaa !6
  br label %for.cond

for.end453:                                       ; preds = %for.cond
  %423 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker454 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %423, i32 0, i32 76
  %424 = load i32, i32* %unread_marker454, align 8, !tbaa !27
  %cmp455 = icmp ne i32 %424, 0
  br i1 %cmp455, label %if.then457, label %if.end459

if.then457:                                       ; preds = %for.end453
  %425 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker458 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %425, i32 0, i32 76
  store i32 0, i32* %unread_marker458, align 8, !tbaa !27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end459:                                        ; preds = %for.end453
  %426 = load i8*, i8** %buffer, align 4, !tbaa !2
  %next_input_byte460 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %427 = load i8*, i8** %next_input_byte460, align 4, !tbaa !23
  %sub.ptr.lhs.cast = ptrtoint i8* %426 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %427 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %bytes_in_buffer461 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %428 = load i32, i32* %bytes_in_buffer461, align 4, !tbaa !25
  %sub462 = sub i32 %428, %sub.ptr.sub
  store i32 %sub462, i32* %bytes_in_buffer461, align 4, !tbaa !25
  %429 = load i8*, i8** %buffer, align 4, !tbaa !2
  %next_input_byte463 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %429, i8** %next_input_byte463, align 4, !tbaa !23
  %next_input_byte464 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %430 = load i8*, i8** %next_input_byte464, align 4, !tbaa !23
  %431 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src465 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %431, i32 0, i32 6
  %432 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src465, align 8, !tbaa !28
  %next_input_byte466 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %432, i32 0, i32 0
  store i8* %430, i8** %next_input_byte466, align 4, !tbaa !31
  %bytes_in_buffer467 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %433 = load i32, i32* %bytes_in_buffer467, align 4, !tbaa !25
  %434 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src468 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %434, i32 0, i32 6
  %435 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src468, align 8, !tbaa !28
  %bytes_in_buffer469 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %435, i32 0, i32 1
  store i32 %433, i32* %bytes_in_buffer469, align 4, !tbaa !32
  %436 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %437 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate470 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %437, i32 0, i32 1
  %get_buffer471 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate470, i32 0, i32 0
  store i32 %436, i32* %get_buffer471, align 4, !tbaa !58
  %438 = load i32, i32* %bits_left, align 4, !tbaa !6
  %439 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate472 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %439, i32 0, i32 1
  %bits_left473 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate472, i32 0, i32 1
  store i32 %438, i32* %bits_left473, align 4, !tbaa !57
  %440 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved474 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %440, i32 0, i32 2
  %441 = bitcast %struct.savable_state* %saved474 to i8*
  %442 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %441, i8* align 4 %442, i32 16, i1 false), !tbaa.struct !69
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end459, %if.then457
  %443 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %443) #4
  %444 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #4
  %445 = bitcast i8** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #4
  %446 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %446) #4
  %447 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %447) #4
  %448 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %448) #4
  %449 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %449) #4
  %450 = load i32, i32* %retval, align 4
  ret i32 %450
}

; Function Attrs: nounwind
define internal i32 @decode_mcu_slow(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.huff_entropy_decoder*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %blkn = alloca i32, align 4
  %state = alloca %struct.savable_state, align 4
  %block = alloca [64 x i16]*, align 4
  %dctbl = alloca %struct.d_derived_tbl*, align 4
  %actbl = alloca %struct.d_derived_tbl*, align 4
  %s = alloca i32, align 4
  %k = alloca i32, align 4
  %r = alloca i32, align 4
  %nb = alloca i32, align 4
  %look = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ci = alloca i32, align 4
  %nb80 = alloca i32, align 4
  %look81 = alloca i32, align 4
  %nb165 = alloca i32, align 4
  %look166 = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !33
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.huff_entropy_decoder*
  store %struct.huff_entropy_decoder* %3, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %6) #4
  %7 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #4
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo2 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %9, %struct.jpeg_decompress_struct** %cinfo2, align 4, !tbaa !26
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !28
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %11, i32 0, i32 0
  %12 = load i8*, i8** %next_input_byte, align 4, !tbaa !31
  %next_input_byte3 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %12, i8** %next_input_byte3, align 4, !tbaa !23
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 6
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src4, align 8, !tbaa !28
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 1
  %15 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !32
  %bytes_in_buffer5 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %15, i32* %bytes_in_buffer5, align 4, !tbaa !25
  %16 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %16, i32 0, i32 1
  %get_buffer6 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %17 = load i32, i32* %get_buffer6, align 4, !tbaa !58
  store i32 %17, i32* %get_buffer, align 4, !tbaa !22
  %18 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate7 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %18, i32 0, i32 1
  %bits_left8 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate7, i32 0, i32 1
  %19 = load i32, i32* %bits_left8, align 4, !tbaa !57
  store i32 %19, i32* %bits_left, align 4, !tbaa !6
  %20 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %20, i32 0, i32 2
  %21 = bitcast %struct.savable_state* %state to i8*
  %22 = bitcast %struct.savable_state* %saved to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !69
  store i32 0, i32* %blkn, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc242, %entry
  %23 = load i32, i32* %blkn, align 4, !tbaa !6
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 70
  %25 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !54
  %cmp = icmp slt i32 %23, %25
  br i1 %cmp, label %for.body, label %for.end244

for.body:                                         ; preds = %for.cond
  %26 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %tobool = icmp ne [64 x i16]** %27, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %28 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %29 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %28, i32 %29
  %30 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi [64 x i16]* [ %30, %cond.true ], [ null, %cond.false ]
  store [64 x i16]* %cond, [64 x i16]** %block, align 4, !tbaa !2
  %31 = bitcast %struct.d_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %32, i32 0, i32 6
  %33 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %dc_cur_tbls, i32 0, i32 %33
  %34 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx9, align 4, !tbaa !2
  store %struct.d_derived_tbl* %34, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %35 = bitcast %struct.d_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_cur_tbls = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %36, i32 0, i32 7
  %37 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [10 x %struct.d_derived_tbl*], [10 x %struct.d_derived_tbl*]* %ac_cur_tbls, i32 0, i32 %37
  %38 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx10, align 4, !tbaa !2
  store %struct.d_derived_tbl* %38, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %39 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i32* %look to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %44, 8
  br i1 %cmp11, label %if.then, label %if.end19

if.then:                                          ; preds = %cond.end
  %45 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %46 = load i32, i32* %bits_left, align 4, !tbaa !6
  %call = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %45, i32 %46, i32 0)
  %tobool12 = icmp ne i32 %call, 0
  br i1 %tobool12, label %if.end, label %if.then13

if.then13:                                        ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %get_buffer14 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %47 = load i32, i32* %get_buffer14, align 4, !tbaa !37
  store i32 %47, i32* %get_buffer, align 4, !tbaa !22
  %bits_left15 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %48 = load i32, i32* %bits_left15, align 4, !tbaa !38
  store i32 %48, i32* %bits_left, align 4, !tbaa !6
  %49 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %49, 8
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end
  store i32 1, i32* %nb, align 4, !tbaa !6
  br label %label1

if.end18:                                         ; preds = %if.end
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %cond.end
  %50 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %51 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub = sub nsw i32 %51, 8
  %shr = lshr i32 %50, %sub
  %and = and i32 %shr, 255
  store i32 %and, i32* %look, align 4, !tbaa !6
  %52 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %52, i32 0, i32 3
  %53 = load i32, i32* %look, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %shr21 = ashr i32 %54, 8
  store i32 %shr21, i32* %nb, align 4, !tbaa !6
  %cmp22 = icmp sle i32 %shr21, 8
  br i1 %cmp22, label %if.then23, label %if.else

if.then23:                                        ; preds = %if.end19
  %55 = load i32, i32* %nb, align 4, !tbaa !6
  %56 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub24 = sub nsw i32 %56, %55
  store i32 %sub24, i32* %bits_left, align 4, !tbaa !6
  %57 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %lookup25 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %57, i32 0, i32 3
  %58 = load i32, i32* %look, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup25, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %and27 = and i32 %59, 255
  store i32 %and27, i32* %s, align 4, !tbaa !6
  br label %if.end34

if.else:                                          ; preds = %if.end19
  br label %label1

label1:                                           ; preds = %if.else, %if.then17
  %60 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %61 = load i32, i32* %bits_left, align 4, !tbaa !6
  %62 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %dctbl, align 4, !tbaa !2
  %63 = load i32, i32* %nb, align 4, !tbaa !6
  %call28 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %60, i32 %61, %struct.d_derived_tbl* %62, i32 %63)
  store i32 %call28, i32* %s, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %call28, 0
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %label1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %label1
  %get_buffer32 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %64 = load i32, i32* %get_buffer32, align 4, !tbaa !37
  store i32 %64, i32* %get_buffer, align 4, !tbaa !22
  %bits_left33 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %65 = load i32, i32* %bits_left33, align 4, !tbaa !38
  store i32 %65, i32* %bits_left, align 4, !tbaa !6
  br label %if.end34

if.end34:                                         ; preds = %if.end31, %if.then23
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end34, %if.then30, %if.then13
  %66 = bitcast i32* %look to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %67 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup234 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %68 = load i32, i32* %s, align 4, !tbaa !6
  %tobool36 = icmp ne i32 %68, 0
  br i1 %tobool36, label %if.then37, label %if.end58

if.then37:                                        ; preds = %cleanup.cont
  %69 = load i32, i32* %bits_left, align 4, !tbaa !6
  %70 = load i32, i32* %s, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %69, %70
  br i1 %cmp38, label %if.then39, label %if.end46

if.then39:                                        ; preds = %if.then37
  %71 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %72 = load i32, i32* %bits_left, align 4, !tbaa !6
  %73 = load i32, i32* %s, align 4, !tbaa !6
  %call40 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %71, i32 %72, i32 %73)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %if.then39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup234

if.end43:                                         ; preds = %if.then39
  %get_buffer44 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %74 = load i32, i32* %get_buffer44, align 4, !tbaa !37
  store i32 %74, i32* %get_buffer, align 4, !tbaa !22
  %bits_left45 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %75 = load i32, i32* %bits_left45, align 4, !tbaa !38
  store i32 %75, i32* %bits_left, align 4, !tbaa !6
  br label %if.end46

if.end46:                                         ; preds = %if.end43, %if.then37
  %76 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %77 = load i32, i32* %s, align 4, !tbaa !6
  %78 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub47 = sub nsw i32 %78, %77
  store i32 %sub47, i32* %bits_left, align 4, !tbaa !6
  %shr48 = lshr i32 %76, %sub47
  %79 = load i32, i32* %s, align 4, !tbaa !6
  %shl = shl i32 1, %79
  %sub49 = sub nsw i32 %shl, 1
  %and50 = and i32 %shr48, %sub49
  store i32 %and50, i32* %r, align 4, !tbaa !6
  %80 = load i32, i32* %r, align 4, !tbaa !6
  %81 = load i32, i32* %r, align 4, !tbaa !6
  %82 = load i32, i32* %s, align 4, !tbaa !6
  %sub51 = sub nsw i32 %82, 1
  %shl52 = shl i32 1, %sub51
  %sub53 = sub nsw i32 %81, %shl52
  %shr54 = ashr i32 %sub53, 31
  %83 = load i32, i32* %s, align 4, !tbaa !6
  %shl55 = shl i32 -1, %83
  %add = add i32 %shl55, 1
  %and56 = and i32 %shr54, %add
  %add57 = add i32 %80, %and56
  store i32 %add57, i32* %s, align 4, !tbaa !6
  br label %if.end58

if.end58:                                         ; preds = %if.end46, %cleanup.cont
  %84 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %dc_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %84, i32 0, i32 8
  %85 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds [10 x i32], [10 x i32]* %dc_needed, i32 0, i32 %85
  %86 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %tobool60 = icmp ne i32 %86, 0
  br i1 %tobool60, label %if.then61, label %if.end71

if.then61:                                        ; preds = %if.end58
  %87 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #4
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 71
  %89 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 %89
  %90 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  store i32 %90, i32* %ci, align 4, !tbaa !6
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 0
  %91 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %93 = load i32, i32* %s, align 4, !tbaa !6
  %add64 = add nsw i32 %93, %92
  store i32 %add64, i32* %s, align 4, !tbaa !6
  %94 = load i32, i32* %s, align 4, !tbaa !6
  %last_dc_val65 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 0
  %95 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx66 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val65, i32 0, i32 %95
  store i32 %94, i32* %arrayidx66, align 4, !tbaa !6
  %96 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %tobool67 = icmp ne [64 x i16]* %96, null
  br i1 %tobool67, label %if.then68, label %if.end70

if.then68:                                        ; preds = %if.then61
  %97 = load i32, i32* %s, align 4, !tbaa !6
  %conv = trunc i32 %97 to i16
  %98 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds [64 x i16], [64 x i16]* %98, i32 0, i32 0
  store i16 %conv, i16* %arrayidx69, align 2, !tbaa !70
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %if.then61
  %99 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #4
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %if.end58
  %100 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_needed = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %100, i32 0, i32 9
  %101 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds [10 x i32], [10 x i32]* %ac_needed, i32 0, i32 %101
  %102 = load i32, i32* %arrayidx72, align 4, !tbaa !6
  %tobool73 = icmp ne i32 %102, 0
  br i1 %tobool73, label %land.lhs.true, label %if.else160

land.lhs.true:                                    ; preds = %if.end71
  %103 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %tobool74 = icmp ne [64 x i16]* %103, null
  br i1 %tobool74, label %if.then75, label %if.else160

if.then75:                                        ; preds = %land.lhs.true
  store i32 1, i32* %k, align 4, !tbaa !6
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc, %if.then75
  %104 = load i32, i32* %k, align 4, !tbaa !6
  %cmp77 = icmp slt i32 %104, 64
  br i1 %cmp77, label %for.body79, label %for.end

for.body79:                                       ; preds = %for.cond76
  %105 = bitcast i32* %nb80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #4
  %106 = bitcast i32* %look81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #4
  %107 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp82 = icmp slt i32 %107, 8
  br i1 %cmp82, label %if.then84, label %if.end95

if.then84:                                        ; preds = %for.body79
  %108 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %109 = load i32, i32* %bits_left, align 4, !tbaa !6
  %call85 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %108, i32 %109, i32 0)
  %tobool86 = icmp ne i32 %call85, 0
  br i1 %tobool86, label %if.end88, label %if.then87

if.then87:                                        ; preds = %if.then84
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

if.end88:                                         ; preds = %if.then84
  %get_buffer89 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %110 = load i32, i32* %get_buffer89, align 4, !tbaa !37
  store i32 %110, i32* %get_buffer, align 4, !tbaa !22
  %bits_left90 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %111 = load i32, i32* %bits_left90, align 4, !tbaa !38
  store i32 %111, i32* %bits_left, align 4, !tbaa !6
  %112 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp91 = icmp slt i32 %112, 8
  br i1 %cmp91, label %if.then93, label %if.end94

if.then93:                                        ; preds = %if.end88
  store i32 1, i32* %nb80, align 4, !tbaa !6
  br label %label2

if.end94:                                         ; preds = %if.end88
  br label %if.end95

if.end95:                                         ; preds = %if.end94, %for.body79
  %113 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %114 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub96 = sub nsw i32 %114, 8
  %shr97 = lshr i32 %113, %sub96
  %and98 = and i32 %shr97, 255
  store i32 %and98, i32* %look81, align 4, !tbaa !6
  %115 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup99 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %115, i32 0, i32 3
  %116 = load i32, i32* %look81, align 4, !tbaa !6
  %arrayidx100 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup99, i32 0, i32 %116
  %117 = load i32, i32* %arrayidx100, align 4, !tbaa !6
  %shr101 = ashr i32 %117, 8
  store i32 %shr101, i32* %nb80, align 4, !tbaa !6
  %cmp102 = icmp sle i32 %shr101, 8
  br i1 %cmp102, label %if.then104, label %if.else109

if.then104:                                       ; preds = %if.end95
  %118 = load i32, i32* %nb80, align 4, !tbaa !6
  %119 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub105 = sub nsw i32 %119, %118
  store i32 %sub105, i32* %bits_left, align 4, !tbaa !6
  %120 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup106 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %120, i32 0, i32 3
  %121 = load i32, i32* %look81, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup106, i32 0, i32 %121
  %122 = load i32, i32* %arrayidx107, align 4, !tbaa !6
  %and108 = and i32 %122, 255
  store i32 %and108, i32* %s, align 4, !tbaa !6
  br label %if.end117

if.else109:                                       ; preds = %if.end95
  br label %label2

label2:                                           ; preds = %if.else109, %if.then93
  %123 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %124 = load i32, i32* %bits_left, align 4, !tbaa !6
  %125 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %126 = load i32, i32* %nb80, align 4, !tbaa !6
  %call110 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %123, i32 %124, %struct.d_derived_tbl* %125, i32 %126)
  store i32 %call110, i32* %s, align 4, !tbaa !6
  %cmp111 = icmp slt i32 %call110, 0
  br i1 %cmp111, label %if.then113, label %if.end114

if.then113:                                       ; preds = %label2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

if.end114:                                        ; preds = %label2
  %get_buffer115 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %127 = load i32, i32* %get_buffer115, align 4, !tbaa !37
  store i32 %127, i32* %get_buffer, align 4, !tbaa !22
  %bits_left116 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %128 = load i32, i32* %bits_left116, align 4, !tbaa !38
  store i32 %128, i32* %bits_left, align 4, !tbaa !6
  br label %if.end117

if.end117:                                        ; preds = %if.end114, %if.then104
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

cleanup118:                                       ; preds = %if.end117, %if.then113, %if.then87
  %129 = bitcast i32* %look81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #4
  %130 = bitcast i32* %nb80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %cleanup.dest120 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest120, label %cleanup234 [
    i32 0, label %cleanup.cont121
  ]

cleanup.cont121:                                  ; preds = %cleanup118
  %131 = load i32, i32* %s, align 4, !tbaa !6
  %shr122 = ashr i32 %131, 4
  store i32 %shr122, i32* %r, align 4, !tbaa !6
  %132 = load i32, i32* %s, align 4, !tbaa !6
  %and123 = and i32 %132, 15
  store i32 %and123, i32* %s, align 4, !tbaa !6
  %133 = load i32, i32* %s, align 4, !tbaa !6
  %tobool124 = icmp ne i32 %133, 0
  br i1 %tobool124, label %if.then125, label %if.else153

if.then125:                                       ; preds = %cleanup.cont121
  %134 = load i32, i32* %r, align 4, !tbaa !6
  %135 = load i32, i32* %k, align 4, !tbaa !6
  %add126 = add nsw i32 %135, %134
  store i32 %add126, i32* %k, align 4, !tbaa !6
  %136 = load i32, i32* %bits_left, align 4, !tbaa !6
  %137 = load i32, i32* %s, align 4, !tbaa !6
  %cmp127 = icmp slt i32 %136, %137
  br i1 %cmp127, label %if.then129, label %if.end136

if.then129:                                       ; preds = %if.then125
  %138 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %139 = load i32, i32* %bits_left, align 4, !tbaa !6
  %140 = load i32, i32* %s, align 4, !tbaa !6
  %call130 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %138, i32 %139, i32 %140)
  %tobool131 = icmp ne i32 %call130, 0
  br i1 %tobool131, label %if.end133, label %if.then132

if.then132:                                       ; preds = %if.then129
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup234

if.end133:                                        ; preds = %if.then129
  %get_buffer134 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %141 = load i32, i32* %get_buffer134, align 4, !tbaa !37
  store i32 %141, i32* %get_buffer, align 4, !tbaa !22
  %bits_left135 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %142 = load i32, i32* %bits_left135, align 4, !tbaa !38
  store i32 %142, i32* %bits_left, align 4, !tbaa !6
  br label %if.end136

if.end136:                                        ; preds = %if.end133, %if.then125
  %143 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %144 = load i32, i32* %s, align 4, !tbaa !6
  %145 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub137 = sub nsw i32 %145, %144
  store i32 %sub137, i32* %bits_left, align 4, !tbaa !6
  %shr138 = lshr i32 %143, %sub137
  %146 = load i32, i32* %s, align 4, !tbaa !6
  %shl139 = shl i32 1, %146
  %sub140 = sub nsw i32 %shl139, 1
  %and141 = and i32 %shr138, %sub140
  store i32 %and141, i32* %r, align 4, !tbaa !6
  %147 = load i32, i32* %r, align 4, !tbaa !6
  %148 = load i32, i32* %r, align 4, !tbaa !6
  %149 = load i32, i32* %s, align 4, !tbaa !6
  %sub142 = sub nsw i32 %149, 1
  %shl143 = shl i32 1, %sub142
  %sub144 = sub nsw i32 %148, %shl143
  %shr145 = ashr i32 %sub144, 31
  %150 = load i32, i32* %s, align 4, !tbaa !6
  %shl146 = shl i32 -1, %150
  %add147 = add i32 %shl146, 1
  %and148 = and i32 %shr145, %add147
  %add149 = add i32 %147, %and148
  store i32 %add149, i32* %s, align 4, !tbaa !6
  %151 = load i32, i32* %s, align 4, !tbaa !6
  %conv150 = trunc i32 %151 to i16
  %152 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %153 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx151 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %153
  %154 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  %arrayidx152 = getelementptr inbounds [64 x i16], [64 x i16]* %152, i32 0, i32 %154
  store i16 %conv150, i16* %arrayidx152, align 2, !tbaa !70
  br label %if.end159

if.else153:                                       ; preds = %cleanup.cont121
  %155 = load i32, i32* %r, align 4, !tbaa !6
  %cmp154 = icmp ne i32 %155, 15
  br i1 %cmp154, label %if.then156, label %if.end157

if.then156:                                       ; preds = %if.else153
  br label %for.end

if.end157:                                        ; preds = %if.else153
  %156 = load i32, i32* %k, align 4, !tbaa !6
  %add158 = add nsw i32 %156, 15
  store i32 %add158, i32* %k, align 4, !tbaa !6
  br label %if.end159

if.end159:                                        ; preds = %if.end157, %if.end136
  br label %for.inc

for.inc:                                          ; preds = %if.end159
  %157 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %157, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond76

for.end:                                          ; preds = %if.then156, %for.cond76
  br label %if.end233

if.else160:                                       ; preds = %land.lhs.true, %if.end71
  store i32 1, i32* %k, align 4, !tbaa !6
  br label %for.cond161

for.cond161:                                      ; preds = %for.inc230, %if.else160
  %158 = load i32, i32* %k, align 4, !tbaa !6
  %cmp162 = icmp slt i32 %158, 64
  br i1 %cmp162, label %for.body164, label %for.end232

for.body164:                                      ; preds = %for.cond161
  %159 = bitcast i32* %nb165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i32* %look166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #4
  %161 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp167 = icmp slt i32 %161, 8
  br i1 %cmp167, label %if.then169, label %if.end180

if.then169:                                       ; preds = %for.body164
  %162 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %163 = load i32, i32* %bits_left, align 4, !tbaa !6
  %call170 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %162, i32 %163, i32 0)
  %tobool171 = icmp ne i32 %call170, 0
  br i1 %tobool171, label %if.end173, label %if.then172

if.then172:                                       ; preds = %if.then169
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup203

if.end173:                                        ; preds = %if.then169
  %get_buffer174 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %164 = load i32, i32* %get_buffer174, align 4, !tbaa !37
  store i32 %164, i32* %get_buffer, align 4, !tbaa !22
  %bits_left175 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %165 = load i32, i32* %bits_left175, align 4, !tbaa !38
  store i32 %165, i32* %bits_left, align 4, !tbaa !6
  %166 = load i32, i32* %bits_left, align 4, !tbaa !6
  %cmp176 = icmp slt i32 %166, 8
  br i1 %cmp176, label %if.then178, label %if.end179

if.then178:                                       ; preds = %if.end173
  store i32 1, i32* %nb165, align 4, !tbaa !6
  br label %label3

if.end179:                                        ; preds = %if.end173
  br label %if.end180

if.end180:                                        ; preds = %if.end179, %for.body164
  %167 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %168 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub181 = sub nsw i32 %168, 8
  %shr182 = lshr i32 %167, %sub181
  %and183 = and i32 %shr182, 255
  store i32 %and183, i32* %look166, align 4, !tbaa !6
  %169 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup184 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %169, i32 0, i32 3
  %170 = load i32, i32* %look166, align 4, !tbaa !6
  %arrayidx185 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup184, i32 0, i32 %170
  %171 = load i32, i32* %arrayidx185, align 4, !tbaa !6
  %shr186 = ashr i32 %171, 8
  store i32 %shr186, i32* %nb165, align 4, !tbaa !6
  %cmp187 = icmp sle i32 %shr186, 8
  br i1 %cmp187, label %if.then189, label %if.else194

if.then189:                                       ; preds = %if.end180
  %172 = load i32, i32* %nb165, align 4, !tbaa !6
  %173 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub190 = sub nsw i32 %173, %172
  store i32 %sub190, i32* %bits_left, align 4, !tbaa !6
  %174 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %lookup191 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %174, i32 0, i32 3
  %175 = load i32, i32* %look166, align 4, !tbaa !6
  %arrayidx192 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup191, i32 0, i32 %175
  %176 = load i32, i32* %arrayidx192, align 4, !tbaa !6
  %and193 = and i32 %176, 255
  store i32 %and193, i32* %s, align 4, !tbaa !6
  br label %if.end202

if.else194:                                       ; preds = %if.end180
  br label %label3

label3:                                           ; preds = %if.else194, %if.then178
  %177 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %178 = load i32, i32* %bits_left, align 4, !tbaa !6
  %179 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %actbl, align 4, !tbaa !2
  %180 = load i32, i32* %nb165, align 4, !tbaa !6
  %call195 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %177, i32 %178, %struct.d_derived_tbl* %179, i32 %180)
  store i32 %call195, i32* %s, align 4, !tbaa !6
  %cmp196 = icmp slt i32 %call195, 0
  br i1 %cmp196, label %if.then198, label %if.end199

if.then198:                                       ; preds = %label3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup203

if.end199:                                        ; preds = %label3
  %get_buffer200 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %181 = load i32, i32* %get_buffer200, align 4, !tbaa !37
  store i32 %181, i32* %get_buffer, align 4, !tbaa !22
  %bits_left201 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %182 = load i32, i32* %bits_left201, align 4, !tbaa !38
  store i32 %182, i32* %bits_left, align 4, !tbaa !6
  br label %if.end202

if.end202:                                        ; preds = %if.end199, %if.then189
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup203

cleanup203:                                       ; preds = %if.end202, %if.then198, %if.then172
  %183 = bitcast i32* %look166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #4
  %184 = bitcast i32* %nb165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #4
  %cleanup.dest205 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest205, label %cleanup234 [
    i32 0, label %cleanup.cont206
  ]

cleanup.cont206:                                  ; preds = %cleanup203
  %185 = load i32, i32* %s, align 4, !tbaa !6
  %shr207 = ashr i32 %185, 4
  store i32 %shr207, i32* %r, align 4, !tbaa !6
  %186 = load i32, i32* %s, align 4, !tbaa !6
  %and208 = and i32 %186, 15
  store i32 %and208, i32* %s, align 4, !tbaa !6
  %187 = load i32, i32* %s, align 4, !tbaa !6
  %tobool209 = icmp ne i32 %187, 0
  br i1 %tobool209, label %if.then210, label %if.else223

if.then210:                                       ; preds = %cleanup.cont206
  %188 = load i32, i32* %r, align 4, !tbaa !6
  %189 = load i32, i32* %k, align 4, !tbaa !6
  %add211 = add nsw i32 %189, %188
  store i32 %add211, i32* %k, align 4, !tbaa !6
  %190 = load i32, i32* %bits_left, align 4, !tbaa !6
  %191 = load i32, i32* %s, align 4, !tbaa !6
  %cmp212 = icmp slt i32 %190, %191
  br i1 %cmp212, label %if.then214, label %if.end221

if.then214:                                       ; preds = %if.then210
  %192 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %193 = load i32, i32* %bits_left, align 4, !tbaa !6
  %194 = load i32, i32* %s, align 4, !tbaa !6
  %call215 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %192, i32 %193, i32 %194)
  %tobool216 = icmp ne i32 %call215, 0
  br i1 %tobool216, label %if.end218, label %if.then217

if.then217:                                       ; preds = %if.then214
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup234

if.end218:                                        ; preds = %if.then214
  %get_buffer219 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %195 = load i32, i32* %get_buffer219, align 4, !tbaa !37
  store i32 %195, i32* %get_buffer, align 4, !tbaa !22
  %bits_left220 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %196 = load i32, i32* %bits_left220, align 4, !tbaa !38
  store i32 %196, i32* %bits_left, align 4, !tbaa !6
  br label %if.end221

if.end221:                                        ; preds = %if.end218, %if.then210
  %197 = load i32, i32* %s, align 4, !tbaa !6
  %198 = load i32, i32* %bits_left, align 4, !tbaa !6
  %sub222 = sub nsw i32 %198, %197
  store i32 %sub222, i32* %bits_left, align 4, !tbaa !6
  br label %if.end229

if.else223:                                       ; preds = %cleanup.cont206
  %199 = load i32, i32* %r, align 4, !tbaa !6
  %cmp224 = icmp ne i32 %199, 15
  br i1 %cmp224, label %if.then226, label %if.end227

if.then226:                                       ; preds = %if.else223
  br label %for.end232

if.end227:                                        ; preds = %if.else223
  %200 = load i32, i32* %k, align 4, !tbaa !6
  %add228 = add nsw i32 %200, 15
  store i32 %add228, i32* %k, align 4, !tbaa !6
  br label %if.end229

if.end229:                                        ; preds = %if.end227, %if.end221
  br label %for.inc230

for.inc230:                                       ; preds = %if.end229
  %201 = load i32, i32* %k, align 4, !tbaa !6
  %inc231 = add nsw i32 %201, 1
  store i32 %inc231, i32* %k, align 4, !tbaa !6
  br label %for.cond161

for.end232:                                       ; preds = %if.then226, %for.cond161
  br label %if.end233

if.end233:                                        ; preds = %for.end232, %for.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup234

cleanup234:                                       ; preds = %if.end233, %if.then217, %cleanup203, %if.then132, %cleanup118, %if.then42, %cleanup
  %202 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast %struct.d_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast %struct.d_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %cleanup.dest240 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest240, label %cleanup256 [
    i32 0, label %cleanup.cont241
  ]

cleanup.cont241:                                  ; preds = %cleanup234
  br label %for.inc242

for.inc242:                                       ; preds = %cleanup.cont241
  %208 = load i32, i32* %blkn, align 4, !tbaa !6
  %inc243 = add nsw i32 %208, 1
  store i32 %inc243, i32* %blkn, align 4, !tbaa !6
  br label %for.cond

for.end244:                                       ; preds = %for.cond
  %next_input_byte245 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %209 = load i8*, i8** %next_input_byte245, align 4, !tbaa !23
  %210 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src246 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %210, i32 0, i32 6
  %211 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src246, align 8, !tbaa !28
  %next_input_byte247 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %211, i32 0, i32 0
  store i8* %209, i8** %next_input_byte247, align 4, !tbaa !31
  %bytes_in_buffer248 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %212 = load i32, i32* %bytes_in_buffer248, align 4, !tbaa !25
  %213 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src249 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %213, i32 0, i32 6
  %214 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src249, align 8, !tbaa !28
  %bytes_in_buffer250 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %214, i32 0, i32 1
  store i32 %212, i32* %bytes_in_buffer250, align 4, !tbaa !32
  %215 = load i32, i32* %get_buffer, align 4, !tbaa !22
  %216 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate251 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %216, i32 0, i32 1
  %get_buffer252 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate251, i32 0, i32 0
  store i32 %215, i32* %get_buffer252, align 4, !tbaa !58
  %217 = load i32, i32* %bits_left, align 4, !tbaa !6
  %218 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate253 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %218, i32 0, i32 1
  %bits_left254 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate253, i32 0, i32 1
  store i32 %217, i32* %bits_left254, align 4, !tbaa !57
  %219 = load %struct.huff_entropy_decoder*, %struct.huff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved255 = getelementptr inbounds %struct.huff_entropy_decoder, %struct.huff_entropy_decoder* %219, i32 0, i32 2
  %220 = bitcast %struct.savable_state* %saved255 to i8*
  %221 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %220, i8* align 4 %221, i32 16, i1 false), !tbaa.struct !69
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup256

cleanup256:                                       ; preds = %for.end244, %cleanup234
  %222 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %222) #4
  %223 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %224 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %224) #4
  %225 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #4
  %226 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #4
  %227 = bitcast %struct.huff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  %228 = load i32, i32* %retval, align 4
  ret i32 %228
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !7, i64 20}
!13 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !14, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!14 = !{!"long", !4, i64 0}
!15 = !{!4, !4, i64 0}
!16 = !{!13, !3, i64 0}
!17 = !{!9, !3, i64 4}
!18 = !{!19, !3, i64 0}
!19 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!20 = !{!21, !3, i64 144}
!21 = !{!"", !4, i64 0, !4, i64 72, !3, i64 144, !4, i64 148}
!22 = !{!14, !14, i64 0}
!23 = !{!24, !3, i64 0}
!24 = !{!"", !3, i64 0, !14, i64 4, !14, i64 8, !7, i64 12, !3, i64 16}
!25 = !{!24, !14, i64 4}
!26 = !{!24, !3, i64 16}
!27 = !{!9, !7, i64 416}
!28 = !{!9, !3, i64 24}
!29 = !{!30, !3, i64 12}
!30 = !{!"jpeg_source_mgr", !3, i64 0, !14, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!31 = !{!30, !3, i64 0}
!32 = !{!30, !14, i64 4}
!33 = !{!9, !3, i64 444}
!34 = !{!35, !7, i64 8}
!35 = !{!"jpeg_entropy_decoder", !3, i64 0, !3, i64 4, !7, i64 8}
!36 = !{!13, !3, i64 4}
!37 = !{!24, !14, i64 8}
!38 = !{!24, !7, i64 12}
!39 = !{!40, !3, i64 0}
!40 = !{!"", !35, i64 0, !41, i64 12, !42, i64 20, !7, i64 36, !4, i64 40, !4, i64 56, !4, i64 72, !4, i64 112, !4, i64 152, !4, i64 192}
!41 = !{!"", !14, i64 0, !7, i64 4}
!42 = !{!"", !4, i64 0}
!43 = !{!40, !3, i64 4}
!44 = !{!45, !7, i64 16}
!45 = !{!"jpeg_common_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20}
!46 = !{!9, !7, i64 400}
!47 = !{!9, !7, i64 404}
!48 = !{!9, !7, i64 408}
!49 = !{!9, !7, i64 412}
!50 = !{!9, !7, i64 328}
!51 = !{!52, !7, i64 20}
!52 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!53 = !{!52, !7, i64 24}
!54 = !{!9, !7, i64 356}
!55 = !{!52, !7, i64 48}
!56 = !{!52, !7, i64 36}
!57 = !{!40, !7, i64 16}
!58 = !{!40, !14, i64 12}
!59 = !{!40, !7, i64 8}
!60 = !{!9, !7, i64 276}
!61 = !{!40, !7, i64 36}
!62 = !{!45, !3, i64 0}
!63 = !{!64, !7, i64 276}
!64 = !{!"", !4, i64 0, !4, i64 17, !7, i64 276}
!65 = !{!9, !3, i64 440}
!66 = !{!67, !7, i64 24}
!67 = !{!"jpeg_marker_reader", !3, i64 0, !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24}
!68 = !{!67, !3, i64 8}
!69 = !{i64 0, i64 16, !15}
!70 = !{!11, !11, i64 0}
