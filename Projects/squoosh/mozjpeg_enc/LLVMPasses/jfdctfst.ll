; ModuleID = 'jfdctfst.c'
source_filename = "jfdctfst.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @jpeg_fdct_ifast(i32* %data) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp4 = alloca i32, align 4
  %tmp5 = alloca i32, align 4
  %tmp6 = alloca i32, align 4
  %tmp7 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %z5 = alloca i32, align 4
  %z11 = alloca i32, align 4
  %z13 = alloca i32, align 4
  %dataptr = alloca i32*, align 4
  %ctr = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32* %z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32* %z13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i32** %dataptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = load i32*, i32** %data.addr, align 4, !tbaa !2
  store i32* %21, i32** %dataptr, align 4, !tbaa !2
  store i32 7, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %22 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sge i32 %22, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %23, i32 0
  %24 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %25 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %25, i32 7
  %26 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %add = add nsw i32 %24, %26
  store i32 %add, i32* %tmp0, align 4, !tbaa !6
  %27 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %27, i32 0
  %28 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %29 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %29, i32 7
  %30 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %sub = sub nsw i32 %28, %30
  store i32 %sub, i32* %tmp7, align 4, !tbaa !6
  %31 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %31, i32 1
  %32 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %33 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %33, i32 6
  %34 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %add6 = add nsw i32 %32, %34
  store i32 %add6, i32* %tmp1, align 4, !tbaa !6
  %35 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %35, i32 1
  %36 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %37 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %37, i32 6
  %38 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %sub9 = sub nsw i32 %36, %38
  store i32 %sub9, i32* %tmp6, align 4, !tbaa !6
  %39 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %39, i32 2
  %40 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %41 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %41, i32 5
  %42 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %add12 = add nsw i32 %40, %42
  store i32 %add12, i32* %tmp2, align 4, !tbaa !6
  %43 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %43, i32 2
  %44 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %45 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %45, i32 5
  %46 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %sub15 = sub nsw i32 %44, %46
  store i32 %sub15, i32* %tmp5, align 4, !tbaa !6
  %47 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %47, i32 3
  %48 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %49 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %49, i32 4
  %50 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %add18 = add nsw i32 %48, %50
  store i32 %add18, i32* %tmp3, align 4, !tbaa !6
  %51 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %51, i32 3
  %52 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %53 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %53, i32 4
  %54 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %sub21 = sub nsw i32 %52, %54
  store i32 %sub21, i32* %tmp4, align 4, !tbaa !6
  %55 = load i32, i32* %tmp0, align 4, !tbaa !6
  %56 = load i32, i32* %tmp3, align 4, !tbaa !6
  %add22 = add nsw i32 %55, %56
  store i32 %add22, i32* %tmp10, align 4, !tbaa !6
  %57 = load i32, i32* %tmp0, align 4, !tbaa !6
  %58 = load i32, i32* %tmp3, align 4, !tbaa !6
  %sub23 = sub nsw i32 %57, %58
  store i32 %sub23, i32* %tmp13, align 4, !tbaa !6
  %59 = load i32, i32* %tmp1, align 4, !tbaa !6
  %60 = load i32, i32* %tmp2, align 4, !tbaa !6
  %add24 = add nsw i32 %59, %60
  store i32 %add24, i32* %tmp11, align 4, !tbaa !6
  %61 = load i32, i32* %tmp1, align 4, !tbaa !6
  %62 = load i32, i32* %tmp2, align 4, !tbaa !6
  %sub25 = sub nsw i32 %61, %62
  store i32 %sub25, i32* %tmp12, align 4, !tbaa !6
  %63 = load i32, i32* %tmp10, align 4, !tbaa !6
  %64 = load i32, i32* %tmp11, align 4, !tbaa !6
  %add26 = add nsw i32 %63, %64
  %65 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %65, i32 0
  store i32 %add26, i32* %arrayidx27, align 4, !tbaa !6
  %66 = load i32, i32* %tmp10, align 4, !tbaa !6
  %67 = load i32, i32* %tmp11, align 4, !tbaa !6
  %sub28 = sub nsw i32 %66, %67
  %68 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %68, i32 4
  store i32 %sub28, i32* %arrayidx29, align 4, !tbaa !6
  %69 = load i32, i32* %tmp12, align 4, !tbaa !6
  %70 = load i32, i32* %tmp13, align 4, !tbaa !6
  %add30 = add nsw i32 %69, %70
  %mul = mul nsw i32 %add30, 181
  %shr = ashr i32 %mul, 8
  store i32 %shr, i32* %z1, align 4, !tbaa !6
  %71 = load i32, i32* %tmp13, align 4, !tbaa !6
  %72 = load i32, i32* %z1, align 4, !tbaa !6
  %add31 = add nsw i32 %71, %72
  %73 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %73, i32 2
  store i32 %add31, i32* %arrayidx32, align 4, !tbaa !6
  %74 = load i32, i32* %tmp13, align 4, !tbaa !6
  %75 = load i32, i32* %z1, align 4, !tbaa !6
  %sub33 = sub nsw i32 %74, %75
  %76 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %76, i32 6
  store i32 %sub33, i32* %arrayidx34, align 4, !tbaa !6
  %77 = load i32, i32* %tmp4, align 4, !tbaa !6
  %78 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add35 = add nsw i32 %77, %78
  store i32 %add35, i32* %tmp10, align 4, !tbaa !6
  %79 = load i32, i32* %tmp5, align 4, !tbaa !6
  %80 = load i32, i32* %tmp6, align 4, !tbaa !6
  %add36 = add nsw i32 %79, %80
  store i32 %add36, i32* %tmp11, align 4, !tbaa !6
  %81 = load i32, i32* %tmp6, align 4, !tbaa !6
  %82 = load i32, i32* %tmp7, align 4, !tbaa !6
  %add37 = add nsw i32 %81, %82
  store i32 %add37, i32* %tmp12, align 4, !tbaa !6
  %83 = load i32, i32* %tmp10, align 4, !tbaa !6
  %84 = load i32, i32* %tmp12, align 4, !tbaa !6
  %sub38 = sub nsw i32 %83, %84
  %mul39 = mul nsw i32 %sub38, 98
  %shr40 = ashr i32 %mul39, 8
  store i32 %shr40, i32* %z5, align 4, !tbaa !6
  %85 = load i32, i32* %tmp10, align 4, !tbaa !6
  %mul41 = mul nsw i32 %85, 139
  %shr42 = ashr i32 %mul41, 8
  %86 = load i32, i32* %z5, align 4, !tbaa !6
  %add43 = add nsw i32 %shr42, %86
  store i32 %add43, i32* %z2, align 4, !tbaa !6
  %87 = load i32, i32* %tmp12, align 4, !tbaa !6
  %mul44 = mul nsw i32 %87, 334
  %shr45 = ashr i32 %mul44, 8
  %88 = load i32, i32* %z5, align 4, !tbaa !6
  %add46 = add nsw i32 %shr45, %88
  store i32 %add46, i32* %z4, align 4, !tbaa !6
  %89 = load i32, i32* %tmp11, align 4, !tbaa !6
  %mul47 = mul nsw i32 %89, 181
  %shr48 = ashr i32 %mul47, 8
  store i32 %shr48, i32* %z3, align 4, !tbaa !6
  %90 = load i32, i32* %tmp7, align 4, !tbaa !6
  %91 = load i32, i32* %z3, align 4, !tbaa !6
  %add49 = add nsw i32 %90, %91
  store i32 %add49, i32* %z11, align 4, !tbaa !6
  %92 = load i32, i32* %tmp7, align 4, !tbaa !6
  %93 = load i32, i32* %z3, align 4, !tbaa !6
  %sub50 = sub nsw i32 %92, %93
  store i32 %sub50, i32* %z13, align 4, !tbaa !6
  %94 = load i32, i32* %z13, align 4, !tbaa !6
  %95 = load i32, i32* %z2, align 4, !tbaa !6
  %add51 = add nsw i32 %94, %95
  %96 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %96, i32 5
  store i32 %add51, i32* %arrayidx52, align 4, !tbaa !6
  %97 = load i32, i32* %z13, align 4, !tbaa !6
  %98 = load i32, i32* %z2, align 4, !tbaa !6
  %sub53 = sub nsw i32 %97, %98
  %99 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %99, i32 3
  store i32 %sub53, i32* %arrayidx54, align 4, !tbaa !6
  %100 = load i32, i32* %z11, align 4, !tbaa !6
  %101 = load i32, i32* %z4, align 4, !tbaa !6
  %add55 = add nsw i32 %100, %101
  %102 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %102, i32 1
  store i32 %add55, i32* %arrayidx56, align 4, !tbaa !6
  %103 = load i32, i32* %z11, align 4, !tbaa !6
  %104 = load i32, i32* %z4, align 4, !tbaa !6
  %sub57 = sub nsw i32 %103, %104
  %105 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i32, i32* %105, i32 7
  store i32 %sub57, i32* %arrayidx58, align 4, !tbaa !6
  %106 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %106, i32 8
  store i32* %add.ptr, i32** %dataptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %107 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %107, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %108 = load i32*, i32** %data.addr, align 4, !tbaa !2
  store i32* %108, i32** %dataptr, align 4, !tbaa !2
  store i32 7, i32* %ctr, align 4, !tbaa !6
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc125, %for.end
  %109 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp60 = icmp sge i32 %109, 0
  br i1 %cmp60, label %for.body61, label %for.end127

for.body61:                                       ; preds = %for.cond59
  %110 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i32, i32* %110, i32 0
  %111 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %112 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %112, i32 56
  %113 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %add64 = add nsw i32 %111, %113
  store i32 %add64, i32* %tmp0, align 4, !tbaa !6
  %114 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %114, i32 0
  %115 = load i32, i32* %arrayidx65, align 4, !tbaa !6
  %116 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %116, i32 56
  %117 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  %sub67 = sub nsw i32 %115, %117
  store i32 %sub67, i32* %tmp7, align 4, !tbaa !6
  %118 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %118, i32 8
  %119 = load i32, i32* %arrayidx68, align 4, !tbaa !6
  %120 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %120, i32 48
  %121 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  %add70 = add nsw i32 %119, %121
  store i32 %add70, i32* %tmp1, align 4, !tbaa !6
  %122 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %122, i32 8
  %123 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %124 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %124, i32 48
  %125 = load i32, i32* %arrayidx72, align 4, !tbaa !6
  %sub73 = sub nsw i32 %123, %125
  store i32 %sub73, i32* %tmp6, align 4, !tbaa !6
  %126 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %126, i32 16
  %127 = load i32, i32* %arrayidx74, align 4, !tbaa !6
  %128 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %128, i32 40
  %129 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %add76 = add nsw i32 %127, %129
  store i32 %add76, i32* %tmp2, align 4, !tbaa !6
  %130 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %130, i32 16
  %131 = load i32, i32* %arrayidx77, align 4, !tbaa !6
  %132 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %132, i32 40
  %133 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  %sub79 = sub nsw i32 %131, %133
  store i32 %sub79, i32* %tmp5, align 4, !tbaa !6
  %134 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %134, i32 24
  %135 = load i32, i32* %arrayidx80, align 4, !tbaa !6
  %136 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %136, i32 32
  %137 = load i32, i32* %arrayidx81, align 4, !tbaa !6
  %add82 = add nsw i32 %135, %137
  store i32 %add82, i32* %tmp3, align 4, !tbaa !6
  %138 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %138, i32 24
  %139 = load i32, i32* %arrayidx83, align 4, !tbaa !6
  %140 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %140, i32 32
  %141 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  %sub85 = sub nsw i32 %139, %141
  store i32 %sub85, i32* %tmp4, align 4, !tbaa !6
  %142 = load i32, i32* %tmp0, align 4, !tbaa !6
  %143 = load i32, i32* %tmp3, align 4, !tbaa !6
  %add86 = add nsw i32 %142, %143
  store i32 %add86, i32* %tmp10, align 4, !tbaa !6
  %144 = load i32, i32* %tmp0, align 4, !tbaa !6
  %145 = load i32, i32* %tmp3, align 4, !tbaa !6
  %sub87 = sub nsw i32 %144, %145
  store i32 %sub87, i32* %tmp13, align 4, !tbaa !6
  %146 = load i32, i32* %tmp1, align 4, !tbaa !6
  %147 = load i32, i32* %tmp2, align 4, !tbaa !6
  %add88 = add nsw i32 %146, %147
  store i32 %add88, i32* %tmp11, align 4, !tbaa !6
  %148 = load i32, i32* %tmp1, align 4, !tbaa !6
  %149 = load i32, i32* %tmp2, align 4, !tbaa !6
  %sub89 = sub nsw i32 %148, %149
  store i32 %sub89, i32* %tmp12, align 4, !tbaa !6
  %150 = load i32, i32* %tmp10, align 4, !tbaa !6
  %151 = load i32, i32* %tmp11, align 4, !tbaa !6
  %add90 = add nsw i32 %150, %151
  %152 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %152, i32 0
  store i32 %add90, i32* %arrayidx91, align 4, !tbaa !6
  %153 = load i32, i32* %tmp10, align 4, !tbaa !6
  %154 = load i32, i32* %tmp11, align 4, !tbaa !6
  %sub92 = sub nsw i32 %153, %154
  %155 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %155, i32 32
  store i32 %sub92, i32* %arrayidx93, align 4, !tbaa !6
  %156 = load i32, i32* %tmp12, align 4, !tbaa !6
  %157 = load i32, i32* %tmp13, align 4, !tbaa !6
  %add94 = add nsw i32 %156, %157
  %mul95 = mul nsw i32 %add94, 181
  %shr96 = ashr i32 %mul95, 8
  store i32 %shr96, i32* %z1, align 4, !tbaa !6
  %158 = load i32, i32* %tmp13, align 4, !tbaa !6
  %159 = load i32, i32* %z1, align 4, !tbaa !6
  %add97 = add nsw i32 %158, %159
  %160 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %160, i32 16
  store i32 %add97, i32* %arrayidx98, align 4, !tbaa !6
  %161 = load i32, i32* %tmp13, align 4, !tbaa !6
  %162 = load i32, i32* %z1, align 4, !tbaa !6
  %sub99 = sub nsw i32 %161, %162
  %163 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %163, i32 48
  store i32 %sub99, i32* %arrayidx100, align 4, !tbaa !6
  %164 = load i32, i32* %tmp4, align 4, !tbaa !6
  %165 = load i32, i32* %tmp5, align 4, !tbaa !6
  %add101 = add nsw i32 %164, %165
  store i32 %add101, i32* %tmp10, align 4, !tbaa !6
  %166 = load i32, i32* %tmp5, align 4, !tbaa !6
  %167 = load i32, i32* %tmp6, align 4, !tbaa !6
  %add102 = add nsw i32 %166, %167
  store i32 %add102, i32* %tmp11, align 4, !tbaa !6
  %168 = load i32, i32* %tmp6, align 4, !tbaa !6
  %169 = load i32, i32* %tmp7, align 4, !tbaa !6
  %add103 = add nsw i32 %168, %169
  store i32 %add103, i32* %tmp12, align 4, !tbaa !6
  %170 = load i32, i32* %tmp10, align 4, !tbaa !6
  %171 = load i32, i32* %tmp12, align 4, !tbaa !6
  %sub104 = sub nsw i32 %170, %171
  %mul105 = mul nsw i32 %sub104, 98
  %shr106 = ashr i32 %mul105, 8
  store i32 %shr106, i32* %z5, align 4, !tbaa !6
  %172 = load i32, i32* %tmp10, align 4, !tbaa !6
  %mul107 = mul nsw i32 %172, 139
  %shr108 = ashr i32 %mul107, 8
  %173 = load i32, i32* %z5, align 4, !tbaa !6
  %add109 = add nsw i32 %shr108, %173
  store i32 %add109, i32* %z2, align 4, !tbaa !6
  %174 = load i32, i32* %tmp12, align 4, !tbaa !6
  %mul110 = mul nsw i32 %174, 334
  %shr111 = ashr i32 %mul110, 8
  %175 = load i32, i32* %z5, align 4, !tbaa !6
  %add112 = add nsw i32 %shr111, %175
  store i32 %add112, i32* %z4, align 4, !tbaa !6
  %176 = load i32, i32* %tmp11, align 4, !tbaa !6
  %mul113 = mul nsw i32 %176, 181
  %shr114 = ashr i32 %mul113, 8
  store i32 %shr114, i32* %z3, align 4, !tbaa !6
  %177 = load i32, i32* %tmp7, align 4, !tbaa !6
  %178 = load i32, i32* %z3, align 4, !tbaa !6
  %add115 = add nsw i32 %177, %178
  store i32 %add115, i32* %z11, align 4, !tbaa !6
  %179 = load i32, i32* %tmp7, align 4, !tbaa !6
  %180 = load i32, i32* %z3, align 4, !tbaa !6
  %sub116 = sub nsw i32 %179, %180
  store i32 %sub116, i32* %z13, align 4, !tbaa !6
  %181 = load i32, i32* %z13, align 4, !tbaa !6
  %182 = load i32, i32* %z2, align 4, !tbaa !6
  %add117 = add nsw i32 %181, %182
  %183 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %183, i32 40
  store i32 %add117, i32* %arrayidx118, align 4, !tbaa !6
  %184 = load i32, i32* %z13, align 4, !tbaa !6
  %185 = load i32, i32* %z2, align 4, !tbaa !6
  %sub119 = sub nsw i32 %184, %185
  %186 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %186, i32 24
  store i32 %sub119, i32* %arrayidx120, align 4, !tbaa !6
  %187 = load i32, i32* %z11, align 4, !tbaa !6
  %188 = load i32, i32* %z4, align 4, !tbaa !6
  %add121 = add nsw i32 %187, %188
  %189 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %189, i32 8
  store i32 %add121, i32* %arrayidx122, align 4, !tbaa !6
  %190 = load i32, i32* %z11, align 4, !tbaa !6
  %191 = load i32, i32* %z4, align 4, !tbaa !6
  %sub123 = sub nsw i32 %190, %191
  %192 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %192, i32 56
  store i32 %sub123, i32* %arrayidx124, align 4, !tbaa !6
  %193 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %193, i32 1
  store i32* %incdec.ptr, i32** %dataptr, align 4, !tbaa !2
  br label %for.inc125

for.inc125:                                       ; preds = %for.body61
  %194 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec126 = add nsw i32 %194, -1
  store i32 %dec126, i32* %ctr, align 4, !tbaa !6
  br label %for.cond59

for.end127:                                       ; preds = %for.cond59
  %195 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #2
  %196 = bitcast i32** %dataptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #2
  %197 = bitcast i32* %z13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #2
  %198 = bitcast i32* %z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #2
  %199 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #2
  %200 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #2
  %201 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #2
  %202 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #2
  %203 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #2
  %204 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #2
  %205 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #2
  %206 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #2
  %207 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #2
  %208 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #2
  %209 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #2
  %210 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #2
  %211 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #2
  %212 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #2
  %213 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #2
  %214 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #2
  %215 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
