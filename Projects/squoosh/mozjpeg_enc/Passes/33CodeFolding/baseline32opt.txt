[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        1.4724e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000668007 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        2.0478e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00191112 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.0029379 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     0.000210293 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0512894 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0142009 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0141246 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00226482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.0107692 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.00309705 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.073843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.0707105 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.0047667 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0319785 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0185095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00453062 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0114855 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0602784 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0217927 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.0391283 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0175137 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00340997 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0171877 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00371032 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0168362 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.49719 seconds.
[PassRunner] (final validation)
