; ModuleID = 'pwgen.c'
source_filename = "pwgen.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.option = type { i8*, i32, i32*, i32 }
%struct._IO_FILE = type opaque

@pw_length = hidden global i32 8, align 4
@num_pw = hidden global i32 -1, align 4
@pwgen_flags = hidden global i32 0, align 4
@do_columns = hidden global i32 0, align 4
@.str = private unnamed_addr constant [12 x i8] c"alt-phonics\00", align 1
@.str.1 = private unnamed_addr constant [11 x i8] c"capitalize\00", align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"numerals\00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"symbols\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"num-passwords\00", align 1
@.str.5 = private unnamed_addr constant [13 x i8] c"remove-chars\00", align 1
@.str.6 = private unnamed_addr constant [7 x i8] c"secure\00", align 1
@.str.7 = private unnamed_addr constant [5 x i8] c"help\00", align 1
@.str.8 = private unnamed_addr constant [12 x i8] c"no-numerals\00", align 1
@.str.9 = private unnamed_addr constant [14 x i8] c"no-capitalize\00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"sha1\00", align 1
@.str.11 = private unnamed_addr constant [10 x i8] c"ambiguous\00", align 1
@.str.12 = private unnamed_addr constant [10 x i8] c"no-vowels\00", align 1
@pwgen_options = hidden global [14 x %struct.option] [%struct.option { i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 0, i32* null, i32 97 }, %struct.option { i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0), i32 0, i32* null, i32 99 }, %struct.option { i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0), i32 0, i32* null, i32 110 }, %struct.option { i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0), i32 0, i32* null, i32 121 }, %struct.option { i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i32 0, i32 0), i32 1, i32* null, i32 78 }, %struct.option { i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.5, i32 0, i32 0), i32 1, i32* null, i32 114 }, %struct.option { i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), i32 0, i32* null, i32 115 }, %struct.option { i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i32 0, i32 0), i32 0, i32* null, i32 104 }, %struct.option { i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.8, i32 0, i32 0), i32 0, i32* null, i32 48 }, %struct.option { i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.9, i32 0, i32 0), i32 0, i32* null, i32 65 }, %struct.option { i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0), i32 1, i32* null, i32 72 }, %struct.option { i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.11, i32 0, i32 0), i32 0, i32* null, i32 66 }, %struct.option { i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.12, i32 0, i32 0), i32 0, i32* null, i32 118 }, %struct.option zeroinitializer], align 16
@.str.13 = private unnamed_addr constant [19 x i8] c"01AaBCcnN:sr:hH:vy\00", align 1
@pw_options = hidden global i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.13, i32 0, i32 0), align 4
@pw_number = hidden global i32 (i32)* null, align 4
@optarg = external global i8*, align 4
@stderr = external constant %struct._IO_FILE*, align 4
@.str.14 = private unnamed_addr constant [33 x i8] c"Invalid number of passwords: %s\0A\00", align 1
@optind = external global i32, align 4
@.str.15 = private unnamed_addr constant [29 x i8] c"Invalid password length: %s\0A\00", align 1
@.str.16 = private unnamed_addr constant [34 x i8] c"Couldn't malloc password buffer.\0A\00", align 1
@.str.17 = private unnamed_addr constant [4 x i8] c"%s\0A\00", align 1
@.str.18 = private unnamed_addr constant [4 x i8] c"%s \00", align 1
@.str.19 = private unnamed_addr constant [52 x i8] c"Usage: pwgen [ OPTIONS ] [ pw_length ] [ num_pw ]\0A\0A\00", align 1
@.str.20 = private unnamed_addr constant [29 x i8] c"Options supported by pwgen:\0A\00", align 1
@.str.21 = private unnamed_addr constant [22 x i8] c"  -c or --capitalize\0A\00", align 1
@.str.22 = private unnamed_addr constant [54 x i8] c"\09Include at least one capital letter in the password\0A\00", align 1
@.str.23 = private unnamed_addr constant [25 x i8] c"  -A or --no-capitalize\0A\00", align 1
@.str.24 = private unnamed_addr constant [48 x i8] c"\09Don't include capital letters in the password\0A\00", align 1
@.str.25 = private unnamed_addr constant [20 x i8] c"  -n or --numerals\0A\00", align 1
@.str.26 = private unnamed_addr constant [46 x i8] c"\09Include at least one number in the password\0A\00", align 1
@.str.27 = private unnamed_addr constant [23 x i8] c"  -0 or --no-numerals\0A\00", align 1
@.str.28 = private unnamed_addr constant [40 x i8] c"\09Don't include numbers in the password\0A\00", align 1
@.str.29 = private unnamed_addr constant [19 x i8] c"  -y or --symbols\0A\00", align 1
@.str.30 = private unnamed_addr constant [54 x i8] c"\09Include at least one special symbol in the password\0A\00", align 1
@.str.31 = private unnamed_addr constant [40 x i8] c"  -r <chars> or --remove-chars=<chars>\0A\00", align 1
@.str.32 = private unnamed_addr constant [69 x i8] c"\09Remove characters from the set of characters to generate passwords\0A\00", align 1
@.str.33 = private unnamed_addr constant [18 x i8] c"  -s or --secure\0A\00", align 1
@.str.34 = private unnamed_addr constant [39 x i8] c"\09Generate completely random passwords\0A\00", align 1
@.str.35 = private unnamed_addr constant [21 x i8] c"  -B or --ambiguous\0A\00", align 1
@.str.36 = private unnamed_addr constant [53 x i8] c"\09Don't include ambiguous characters in the password\0A\00", align 1
@.str.37 = private unnamed_addr constant [16 x i8] c"  -h or --help\0A\00", align 1
@.str.38 = private unnamed_addr constant [23 x i8] c"\09Print a help message\0A\00", align 1
@.str.39 = private unnamed_addr constant [36 x i8] c"  -H or --sha1=path/to/file[#seed]\0A\00", align 1
@.str.40 = private unnamed_addr constant [61 x i8] c"\09Use sha1 hash of given file as a (not so) random generator\0A\00", align 1
@.str.41 = private unnamed_addr constant [48 x i8] c"  -C\0A\09Print the generated passwords in columns\0A\00", align 1
@.str.42 = private unnamed_addr constant [54 x i8] c"  -1\0A\09Don't print the generated passwords in columns\0A\00", align 1
@.str.43 = private unnamed_addr constant [21 x i8] c"  -v or --no-vowels\0A\00", align 1
@.str.44 = private unnamed_addr constant [62 x i8] c"\09Do not use any vowels so as to avoid accidental nasty words\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @main(i32 %argc, i8** %argv) #0 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  %term_width = alloca i32, align 4
  %c = alloca i32, align 4
  %i = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %buf = alloca i8*, align 4
  %tmp = alloca i8*, align 4
  %remove = alloca i8*, align 4
  %pwgen = alloca void (i8*, i32, i32, i8*)*, align 4
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 4
  store i32 80, i32* %term_width, align 4
  store i32 -1, i32* %num_cols, align 4
  store i8* null, i8** %remove, align 4
  store void (i8*, i32, i32, i8*)* @pw_phonemes, void (i8*, i32, i32, i8*)** %pwgen, align 4
  store i32 (i32)* @pw_random_number, i32 (i32)** @pw_number, align 4
  %call = call i32 @isatty(i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* @do_columns, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %0 = load i32, i32* @pwgen_flags, align 4
  %or = or i32 %0, 3
  store i32 %or, i32* @pwgen_flags, align 4
  br label %while.body

while.body:                                       ; preds = %if.end, %sw.epilog
  %1 = load i32, i32* %argc.addr, align 4
  %2 = load i8**, i8*** %argv.addr, align 4
  %3 = load i8*, i8** @pw_options, align 4
  %call1 = call i32 @getopt_long(i32 %1, i8** %2, i8* %3, %struct.option* getelementptr inbounds ([14 x %struct.option], [14 x %struct.option]* @pwgen_options, i32 0, i32 0), i32* null)
  store i32 %call1, i32* %c, align 4
  %4 = load i32, i32* %c, align 4
  %cmp = icmp eq i32 %4, -1
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %while.body
  br label %while.end

if.end3:                                          ; preds = %while.body
  %5 = load i32, i32* %c, align 4
  switch i32 %5, label %sw.epilog [
    i32 48, label %sw.bb
    i32 65, label %sw.bb4
    i32 97, label %sw.bb6
    i32 66, label %sw.bb7
    i32 99, label %sw.bb9
    i32 110, label %sw.bb11
    i32 78, label %sw.bb13
    i32 115, label %sw.bb19
    i32 67, label %sw.bb20
    i32 49, label %sw.bb21
    i32 72, label %sw.bb22
    i32 121, label %sw.bb23
    i32 118, label %sw.bb25
    i32 114, label %sw.bb27
    i32 104, label %sw.bb29
    i32 63, label %sw.bb29
  ]

sw.bb:                                            ; preds = %if.end3
  %6 = load i32, i32* @pwgen_flags, align 4
  %and = and i32 %6, -2
  store i32 %and, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb4:                                           ; preds = %if.end3
  %7 = load i32, i32* @pwgen_flags, align 4
  %and5 = and i32 %7, -3
  store i32 %and5, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.end3
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.end3
  %8 = load i32, i32* @pwgen_flags, align 4
  %or8 = or i32 %8, 8
  store i32 %or8, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb9:                                           ; preds = %if.end3
  %9 = load i32, i32* @pwgen_flags, align 4
  %or10 = or i32 %9, 2
  store i32 %or10, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.end3
  %10 = load i32, i32* @pwgen_flags, align 4
  %or12 = or i32 %10, 1
  store i32 %or12, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb13:                                          ; preds = %if.end3
  %11 = load i8*, i8** @optarg, align 4
  %call14 = call i32 @strtol(i8* %11, i8** %tmp, i32 0)
  store i32 %call14, i32* @num_pw, align 4
  %12 = load i8*, i8** %tmp, align 4
  %13 = load i8, i8* %12, align 1
  %tobool15 = icmp ne i8 %13, 0
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %sw.bb13
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %15 = load i8*, i8** @optarg, align 4
  %call17 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.14, i32 0, i32 0), i8* %15)
  call void @exit(i32 1) #3
  unreachable

if.end18:                                         ; preds = %sw.bb13
  br label %sw.epilog

sw.bb19:                                          ; preds = %if.end3
  store void (i8*, i32, i32, i8*)* @pw_rand, void (i8*, i32, i32, i8*)** %pwgen, align 4
  br label %sw.epilog

sw.bb20:                                          ; preds = %if.end3
  store i32 1, i32* @do_columns, align 4
  br label %sw.epilog

sw.bb21:                                          ; preds = %if.end3
  store i32 0, i32* @do_columns, align 4
  br label %sw.epilog

sw.bb22:                                          ; preds = %if.end3
  %16 = load i8*, i8** @optarg, align 4
  call void @pw_sha1_init(i8* %16)
  store i32 (i32)* @pw_sha1_number, i32 (i32)** @pw_number, align 4
  br label %sw.epilog

sw.bb23:                                          ; preds = %if.end3
  %17 = load i32, i32* @pwgen_flags, align 4
  %or24 = or i32 %17, 4
  store i32 %or24, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb25:                                          ; preds = %if.end3
  store void (i8*, i32, i32, i8*)* @pw_rand, void (i8*, i32, i32, i8*)** %pwgen, align 4
  %18 = load i32, i32* @pwgen_flags, align 4
  %or26 = or i32 %18, 16
  store i32 %or26, i32* @pwgen_flags, align 4
  br label %sw.epilog

sw.bb27:                                          ; preds = %if.end3
  %19 = load i8*, i8** @optarg, align 4
  %call28 = call i8* @strdup(i8* %19)
  store i8* %call28, i8** %remove, align 4
  store void (i8*, i32, i32, i8*)* @pw_rand, void (i8*, i32, i32, i8*)** %pwgen, align 4
  br label %sw.epilog

sw.bb29:                                          ; preds = %if.end3, %if.end3
  call void @usage()
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end3, %sw.bb29, %sw.bb27, %sw.bb25, %sw.bb23, %sw.bb22, %sw.bb21, %sw.bb20, %sw.bb19, %if.end18, %sw.bb11, %sw.bb9, %sw.bb7, %sw.bb6, %sw.bb4, %sw.bb
  br label %while.body

while.end:                                        ; preds = %if.then2
  %20 = load i32, i32* @optind, align 4
  %21 = load i32, i32* %argc.addr, align 4
  %cmp30 = icmp slt i32 %20, %21
  br i1 %cmp30, label %if.then31, label %if.end52

if.then31:                                        ; preds = %while.end
  %22 = load i8**, i8*** %argv.addr, align 4
  %23 = load i32, i32* @optind, align 4
  %arrayidx = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx, align 4
  %call32 = call i32 @strtol(i8* %24, i8** %tmp, i32 0)
  store i32 %call32, i32* @pw_length, align 4
  %25 = load i32, i32* @pw_length, align 4
  %cmp33 = icmp slt i32 %25, 5
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.then31
  store void (i8*, i32, i32, i8*)* @pw_rand, void (i8*, i32, i32, i8*)** %pwgen, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.then31
  %26 = load void (i8*, i32, i32, i8*)*, void (i8*, i32, i32, i8*)** %pwgen, align 4
  %cmp36 = icmp ne void (i8*, i32, i32, i8*)* %26, @pw_rand
  br i1 %cmp36, label %if.then37, label %if.end46

if.then37:                                        ; preds = %if.end35
  %27 = load i32, i32* @pw_length, align 4
  %cmp38 = icmp sle i32 %27, 2
  br i1 %cmp38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.then37
  %28 = load i32, i32* @pwgen_flags, align 4
  %and40 = and i32 %28, -3
  store i32 %and40, i32* @pwgen_flags, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.then37
  %29 = load i32, i32* @pw_length, align 4
  %cmp42 = icmp sle i32 %29, 1
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.end41
  %30 = load i32, i32* @pwgen_flags, align 4
  %and44 = and i32 %30, -2
  store i32 %and44, i32* @pwgen_flags, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.end41
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end35
  %31 = load i8*, i8** %tmp, align 4
  %32 = load i8, i8* %31, align 1
  %tobool47 = icmp ne i8 %32, 0
  br i1 %tobool47, label %if.then48, label %if.end51

if.then48:                                        ; preds = %if.end46
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %34 = load i8**, i8*** %argv.addr, align 4
  %35 = load i32, i32* @optind, align 4
  %arrayidx49 = getelementptr inbounds i8*, i8** %34, i32 %35
  %36 = load i8*, i8** %arrayidx49, align 4
  %call50 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.15, i32 0, i32 0), i8* %36)
  call void @exit(i32 1) #3
  unreachable

if.end51:                                         ; preds = %if.end46
  %37 = load i32, i32* @optind, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* @optind, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %while.end
  %38 = load i32, i32* @optind, align 4
  %39 = load i32, i32* %argc.addr, align 4
  %cmp53 = icmp slt i32 %38, %39
  br i1 %cmp53, label %if.then54, label %if.end62

if.then54:                                        ; preds = %if.end52
  %40 = load i8**, i8*** %argv.addr, align 4
  %41 = load i32, i32* @optind, align 4
  %arrayidx55 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx55, align 4
  %call56 = call i32 @strtol(i8* %42, i8** %tmp, i32 0)
  store i32 %call56, i32* @num_pw, align 4
  %43 = load i8*, i8** %tmp, align 4
  %44 = load i8, i8* %43, align 1
  %tobool57 = icmp ne i8 %44, 0
  br i1 %tobool57, label %if.then58, label %if.end61

if.then58:                                        ; preds = %if.then54
  %45 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %46 = load i8**, i8*** %argv.addr, align 4
  %47 = load i32, i32* @optind, align 4
  %arrayidx59 = getelementptr inbounds i8*, i8** %46, i32 %47
  %48 = load i8*, i8** %arrayidx59, align 4
  %call60 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %45, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.14, i32 0, i32 0), i8* %48)
  call void @exit(i32 1) #3
  unreachable

if.end61:                                         ; preds = %if.then54
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end52
  %49 = load i32, i32* @do_columns, align 4
  %tobool63 = icmp ne i32 %49, 0
  br i1 %tobool63, label %if.then64, label %if.end68

if.then64:                                        ; preds = %if.end62
  %50 = load i32, i32* %term_width, align 4
  %51 = load i32, i32* @pw_length, align 4
  %add = add nsw i32 %51, 1
  %div = sdiv i32 %50, %add
  store i32 %div, i32* %num_cols, align 4
  %52 = load i32, i32* %num_cols, align 4
  %cmp65 = icmp eq i32 %52, 0
  br i1 %cmp65, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.then64
  store i32 1, i32* %num_cols, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then66, %if.then64
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.end62
  %53 = load i32, i32* @num_pw, align 4
  %cmp69 = icmp slt i32 %53, 0
  br i1 %cmp69, label %if.then70, label %if.end72

if.then70:                                        ; preds = %if.end68
  %54 = load i32, i32* @do_columns, align 4
  %tobool71 = icmp ne i32 %54, 0
  br i1 %tobool71, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then70
  %55 = load i32, i32* %num_cols, align 4
  %mul = mul nsw i32 %55, 20
  br label %cond.end

cond.false:                                       ; preds = %if.then70
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* @num_pw, align 4
  br label %if.end72

if.end72:                                         ; preds = %cond.end, %if.end68
  %56 = load i32, i32* @pw_length, align 4
  %add73 = add nsw i32 %56, 1
  %call74 = call i8* @malloc(i32 %add73)
  store i8* %call74, i8** %buf, align 4
  %57 = load i8*, i8** %buf, align 4
  %tobool75 = icmp ne i8* %57, null
  br i1 %tobool75, label %if.end78, label %if.then76

if.then76:                                        ; preds = %if.end72
  %58 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call77 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %58, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.16, i32 0, i32 0))
  call void @exit(i32 1) #3
  unreachable

if.end78:                                         ; preds = %if.end72
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end78
  %59 = load i32, i32* %i, align 4
  %60 = load i32, i32* @num_pw, align 4
  %cmp79 = icmp slt i32 %59, %60
  br i1 %cmp79, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %61 = load void (i8*, i32, i32, i8*)*, void (i8*, i32, i32, i8*)** %pwgen, align 4
  %62 = load i8*, i8** %buf, align 4
  %63 = load i32, i32* @pw_length, align 4
  %64 = load i32, i32* @pwgen_flags, align 4
  %65 = load i8*, i8** %remove, align 4
  call void %61(i8* %62, i32 %63, i32 %64, i8* %65)
  %66 = load i32, i32* @do_columns, align 4
  %tobool80 = icmp ne i32 %66, 0
  br i1 %tobool80, label %lor.lhs.false, label %if.then85

lor.lhs.false:                                    ; preds = %for.body
  %67 = load i32, i32* %i, align 4
  %68 = load i32, i32* %num_cols, align 4
  %rem = srem i32 %67, %68
  %69 = load i32, i32* %num_cols, align 4
  %sub = sub nsw i32 %69, 1
  %cmp81 = icmp eq i32 %rem, %sub
  br i1 %cmp81, label %if.then85, label %lor.lhs.false82

lor.lhs.false82:                                  ; preds = %lor.lhs.false
  %70 = load i32, i32* %i, align 4
  %71 = load i32, i32* @num_pw, align 4
  %sub83 = sub nsw i32 %71, 1
  %cmp84 = icmp eq i32 %70, %sub83
  br i1 %cmp84, label %if.then85, label %if.else

if.then85:                                        ; preds = %lor.lhs.false82, %lor.lhs.false, %for.body
  %72 = load i8*, i8** %buf, align 4
  %call86 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.17, i32 0, i32 0), i8* %72)
  br label %if.end88

if.else:                                          ; preds = %lor.lhs.false82
  %73 = load i8*, i8** %buf, align 4
  %call87 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.18, i32 0, i32 0), i8* %73)
  br label %if.end88

if.end88:                                         ; preds = %if.else, %if.then85
  br label %for.inc

for.inc:                                          ; preds = %if.end88
  %74 = load i32, i32* %i, align 4
  %inc89 = add nsw i32 %74, 1
  store i32 %inc89, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %75 = load i8*, i8** %buf, align 4
  call void @free(i8* %75)
  ret i32 0
}

declare void @pw_phonemes(i8*, i32, i32, i8*) #1

declare i32 @pw_random_number(i32) #1

declare i32 @isatty(i32) #1

declare i32 @getopt_long(i32, i8**, i8*, %struct.option*, i32*) #1

declare i32 @strtol(i8*, i8**, i32) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

declare void @pw_rand(i8*, i32, i32, i8*) #1

declare void @pw_sha1_init(i8*) #1

declare i32 @pw_sha1_number(i32) #1

declare i8* @strdup(i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @usage() #0 {
entry:
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call = call i32 @fputs(i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.19, i32 0, i32 0), %struct._IO_FILE* %0)
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call1 = call i32 @fputs(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.20, i32 0, i32 0), %struct._IO_FILE* %1)
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call2 = call i32 @fputs(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.21, i32 0, i32 0), %struct._IO_FILE* %2)
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call3 = call i32 @fputs(i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.22, i32 0, i32 0), %struct._IO_FILE* %3)
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call4 = call i32 @fputs(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.23, i32 0, i32 0), %struct._IO_FILE* %4)
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call5 = call i32 @fputs(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.24, i32 0, i32 0), %struct._IO_FILE* %5)
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call6 = call i32 @fputs(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.25, i32 0, i32 0), %struct._IO_FILE* %6)
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call7 = call i32 @fputs(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.26, i32 0, i32 0), %struct._IO_FILE* %7)
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call8 = call i32 @fputs(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.27, i32 0, i32 0), %struct._IO_FILE* %8)
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call9 = call i32 @fputs(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.28, i32 0, i32 0), %struct._IO_FILE* %9)
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call10 = call i32 @fputs(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.29, i32 0, i32 0), %struct._IO_FILE* %10)
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call11 = call i32 @fputs(i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.30, i32 0, i32 0), %struct._IO_FILE* %11)
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call12 = call i32 @fputs(i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.31, i32 0, i32 0), %struct._IO_FILE* %12)
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call13 = call i32 @fputs(i8* getelementptr inbounds ([69 x i8], [69 x i8]* @.str.32, i32 0, i32 0), %struct._IO_FILE* %13)
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call14 = call i32 @fputs(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.33, i32 0, i32 0), %struct._IO_FILE* %14)
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call15 = call i32 @fputs(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.34, i32 0, i32 0), %struct._IO_FILE* %15)
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call16 = call i32 @fputs(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.35, i32 0, i32 0), %struct._IO_FILE* %16)
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call17 = call i32 @fputs(i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.36, i32 0, i32 0), %struct._IO_FILE* %17)
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call18 = call i32 @fputs(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.37, i32 0, i32 0), %struct._IO_FILE* %18)
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call19 = call i32 @fputs(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.38, i32 0, i32 0), %struct._IO_FILE* %19)
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call20 = call i32 @fputs(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.39, i32 0, i32 0), %struct._IO_FILE* %20)
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call21 = call i32 @fputs(i8* getelementptr inbounds ([61 x i8], [61 x i8]* @.str.40, i32 0, i32 0), %struct._IO_FILE* %21)
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call22 = call i32 @fputs(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.41, i32 0, i32 0), %struct._IO_FILE* %22)
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call23 = call i32 @fputs(i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.42, i32 0, i32 0), %struct._IO_FILE* %23)
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call24 = call i32 @fputs(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.43, i32 0, i32 0), %struct._IO_FILE* %24)
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call25 = call i32 @fputs(i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.44, i32 0, i32 0), %struct._IO_FILE* %25)
  call void @exit(i32 1) #3
  unreachable
}

declare i8* @malloc(i32) #1

declare i32 @printf(i8*, ...) #1

declare void @free(i8*) #1

declare i32 @fputs(i8*, %struct._IO_FILE*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
