; ModuleID = 'sha1num.c'
source_filename = "sha1num.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.sha1_context = type { [2 x i32], [5 x i32], [64 x i8] }

@.str = private unnamed_addr constant [6 x i8] c"pwgen\00", align 1
@sha1_magic = hidden global i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), align 4
@sha1sum_idx = hidden global i32 20, align 4
@sha1_seed = hidden global i8* null, align 4
@stderr = external constant %struct._IO_FILE*, align 4
@.str.1 = private unnamed_addr constant [35 x i8] c"Couldn't malloc sha1_seed buffer.\0A\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.3 = private unnamed_addr constant [25 x i8] c"Couldn't open file: %s.\0A\00", align 1
@sha1_ctx = hidden global %struct.sha1_context zeroinitializer, align 4
@sha1sum = hidden global [20 x i8] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone
define hidden void @pw_sha1_init(i8* %sha1) #0 {
entry:
  %sha1.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %seed = alloca i8*, align 4
  %f = alloca %struct._IO_FILE*, align 4
  %buf = alloca [1024 x i8], align 16
  store i8* %sha1, i8** %sha1.addr, align 4
  store i32 0, i32* %i, align 4
  %0 = load i8*, i8** %sha1.addr, align 4
  %call = call i8* @strchr(i8* %0, i32 35)
  store i8* %call, i8** %seed, align 4
  %tobool = icmp ne i8* %call, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %seed, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %1, i32 1
  store i8* %incdec.ptr, i8** %seed, align 4
  store i8 0, i8* %1, align 1
  %2 = load i8*, i8** %seed, align 4
  %call1 = call i32 @strlen(i8* %2)
  %add = add i32 %call1, 1
  %call2 = call i8* @malloc(i32 %add)
  store i8* %call2, i8** @sha1_seed, align 4
  %3 = load i8*, i8** @sha1_seed, align 4
  %tobool3 = icmp ne i8* %3, null
  br i1 %tobool3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.then
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.1, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end:                                           ; preds = %if.then
  %5 = load i8*, i8** @sha1_seed, align 4
  %6 = load i8*, i8** %seed, align 4
  %call6 = call i8* @strcpy(i8* %5, i8* %6)
  br label %if.end15

if.else:                                          ; preds = %entry
  %7 = load i8*, i8** @sha1_magic, align 4
  %call7 = call i32 @strlen(i8* %7)
  %add8 = add i32 %call7, 1
  %call9 = call i8* @malloc(i32 %add8)
  store i8* %call9, i8** @sha1_seed, align 4
  %8 = load i8*, i8** @sha1_seed, align 4
  %tobool10 = icmp ne i8* %8, null
  br i1 %tobool10, label %if.end13, label %if.then11

if.then11:                                        ; preds = %if.else
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.1, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end13:                                         ; preds = %if.else
  %10 = load i8*, i8** @sha1_seed, align 4
  %11 = load i8*, i8** @sha1_magic, align 4
  %call14 = call i8* @strcpy(i8* %10, i8* %11)
  br label %if.end15

if.end15:                                         ; preds = %if.end13, %if.end
  %12 = load i8*, i8** %sha1.addr, align 4
  %call16 = call %struct._IO_FILE* @fopen(i8* %12, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  store %struct._IO_FILE* %call16, %struct._IO_FILE** %f, align 4
  %tobool17 = icmp ne %struct._IO_FILE* %call16, null
  br i1 %tobool17, label %if.end20, label %if.then18

if.then18:                                        ; preds = %if.end15
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %14 = load i8*, i8** %sha1.addr, align 4
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i32 0, i32 0), i8* %14)
  call void @exit(i32 1) #4
  unreachable

if.end20:                                         ; preds = %if.end15
  call void @sha1_starts(%struct.sha1_context* @sha1_ctx)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end20
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %buf, i32 0, i32 0
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4
  %call21 = call i32 @fread(i8* %arraydecay, i32 1, i32 1024, %struct._IO_FILE* %15)
  store i32 %call21, i32* %i, align 4
  %cmp = icmp sgt i32 %call21, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay22 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buf, i32 0, i32 0
  %16 = load i32, i32* %i, align 4
  call void @sha1_update(%struct.sha1_context* @sha1_ctx, i8* %arraydecay22, i32 %16)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4
  %call23 = call i32 @fclose(%struct._IO_FILE* %17)
  ret void
}

declare i8* @strchr(i8*, i32) #1

declare i8* @malloc(i32) #1

declare i32 @strlen(i8*) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

declare i8* @strcpy(i8*, i8*) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #1

declare void @sha1_starts(%struct.sha1_context*) #1

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #1

declare void @sha1_update(%struct.sha1_context*, i8*, i32) #1

declare i32 @fclose(%struct._IO_FILE*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @pw_sha1_number(i32 %max_num) #0 {
entry:
  %max_num.addr = alloca i32, align 4
  %val = alloca i32, align 4
  %ctx = alloca %struct.sha1_context, align 4
  store i32 %max_num, i32* %max_num.addr, align 4
  %0 = load i32, i32* @sha1sum_idx, align 4
  %cmp = icmp sgt i32 %0, 19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* @sha1sum_idx, align 4
  %1 = load i8*, i8** @sha1_seed, align 4
  %2 = load i8*, i8** @sha1_seed, align 4
  %call = call i32 @strlen(i8* %2)
  call void @sha1_update(%struct.sha1_context* @sha1_ctx, i8* %1, i32 %call)
  %3 = bitcast %struct.sha1_context* %ctx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 bitcast (%struct.sha1_context* @sha1_ctx to i8*), i32 92, i1 false)
  call void @sha1_finish(%struct.sha1_context* %ctx, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @sha1sum, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i32, i32* @sha1sum_idx, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* @sha1sum_idx, align 4
  %arrayidx = getelementptr inbounds [20 x i8], [20 x i8]* @sha1sum, i32 0, i32 %4
  %5 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %5 to i32
  %conv1 = sitofp i32 %conv to float
  %div = fdiv float %conv1, 2.560000e+02
  %6 = load i32, i32* %max_num.addr, align 4
  %conv2 = sitofp i32 %6 to float
  %mul = fmul float %div, %conv2
  %conv3 = fptosi float %mul to i32
  store i32 %conv3, i32* %val, align 4
  %7 = load i32, i32* %val, align 4
  ret i32 %7
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare void @sha1_finish(%struct.sha1_context*, i8*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
