; ModuleID = 'randnum.c'
source_filename = "randnum.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque

@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [23 x i8] c"No entropy available!\0A\00", align 1
@get_random_fd.fd = internal global i32 -2, align 4
@.str.1 = private unnamed_addr constant [13 x i8] c"/dev/urandom\00", align 1
@.str.2 = private unnamed_addr constant [12 x i8] c"/dev/random\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @pw_random_number(i32 %max_num) #0 {
entry:
  %max_num.addr = alloca i32, align 4
  %rand_num = alloca i32, align 4
  %i = alloca i32, align 4
  %fd = alloca i32, align 4
  %lose_counter = alloca i32, align 4
  %nbytes = alloca i32, align 4
  %cp = alloca i8*, align 4
  store i32 %max_num, i32* %max_num.addr, align 4
  %call = call i32 @get_random_fd()
  store i32 %call, i32* %fd, align 4
  store i32 0, i32* %lose_counter, align 4
  store i32 4, i32* %nbytes, align 4
  %0 = bitcast i32* %rand_num to i8*
  store i8* %0, i8** %cp, align 4
  %1 = load i32, i32* %fd, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end15

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %if.end14, %if.end13, %if.then8, %if.then
  %2 = load i32, i32* %nbytes, align 4
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load i32, i32* %fd, align 4
  %4 = load i8*, i8** %cp, align 4
  %5 = load i32, i32* %nbytes, align 4
  %call2 = call i32 @read(i32 %3, i8* %4, i32 %5)
  store i32 %call2, i32* %i, align 4
  %6 = load i32, i32* %i, align 4
  %cmp3 = icmp slt i32 %6, 0
  br i1 %cmp3, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %call4 = call i32* @__errno_location()
  %7 = load i32, i32* %call4, align 4
  %cmp5 = icmp eq i32 %7, 27
  br i1 %cmp5, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %call6 = call i32* @__errno_location()
  %8 = load i32, i32* %call6, align 4
  %cmp7 = icmp eq i32 %8, 6
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %lor.lhs.false, %land.lhs.true
  br label %while.cond

if.end:                                           ; preds = %lor.lhs.false, %while.body
  %9 = load i32, i32* %i, align 4
  %cmp9 = icmp sle i32 %9, 0
  br i1 %cmp9, label %if.then10, label %if.end14

if.then10:                                        ; preds = %if.end
  %10 = load i32, i32* %lose_counter, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %lose_counter, align 4
  %cmp11 = icmp eq i32 %10, 8
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.then10
  br label %while.end

if.end13:                                         ; preds = %if.then10
  br label %while.cond

if.end14:                                         ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %nbytes, align 4
  %sub = sub nsw i32 %12, %11
  store i32 %sub, i32* %nbytes, align 4
  %13 = load i32, i32* %i, align 4
  %14 = load i8*, i8** %cp, align 4
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr, i8** %cp, align 4
  store i32 0, i32* %lose_counter, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then12, %while.cond
  br label %if.end15

if.end15:                                         ; preds = %while.end, %entry
  %15 = load i32, i32* %nbytes, align 4
  %cmp16 = icmp eq i32 %15, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %16 = load i32, i32* %rand_num, align 4
  %17 = load i32, i32* %max_num.addr, align 4
  %rem = urem i32 %16, %17
  ret i32 %rem

if.end18:                                         ; preds = %if.end15
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0))
  call void @exit(i32 1) #3
  unreachable
}

; Function Attrs: noinline nounwind optnone
define internal i32 @get_random_fd() #0 {
entry:
  %0 = load i32, i32* @get_random_fd.fd, align 4
  %cmp = icmp eq i32 %0, -2
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %call = call i32 (i8*, i32, ...) @open(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0), i32 0)
  store i32 %call, i32* @get_random_fd.fd, align 4
  %1 = load i32, i32* @get_random_fd.fd, align 4
  %cmp1 = icmp eq i32 %1, -1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %call3 = call i32 (i8*, i32, ...) @open(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 2048)
  store i32 %call3, i32* @get_random_fd.fd, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %2 = load i32, i32* @get_random_fd.fd, align 4
  ret i32 %2
}

declare i32 @read(i32, i8*, i32) #1

declare i32* @__errno_location() #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

declare i32 @open(i8*, i32, ...) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
