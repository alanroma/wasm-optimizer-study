; ModuleID = 'pw_phonemes.c'
source_filename = "pw_phonemes.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.pw_element = type { i8*, i32 }

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"ae\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"ah\00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"ai\00", align 1
@.str.4 = private unnamed_addr constant [2 x i8] c"b\00", align 1
@.str.5 = private unnamed_addr constant [2 x i8] c"c\00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"ch\00", align 1
@.str.7 = private unnamed_addr constant [2 x i8] c"d\00", align 1
@.str.8 = private unnamed_addr constant [2 x i8] c"e\00", align 1
@.str.9 = private unnamed_addr constant [3 x i8] c"ee\00", align 1
@.str.10 = private unnamed_addr constant [3 x i8] c"ei\00", align 1
@.str.11 = private unnamed_addr constant [2 x i8] c"f\00", align 1
@.str.12 = private unnamed_addr constant [2 x i8] c"g\00", align 1
@.str.13 = private unnamed_addr constant [3 x i8] c"gh\00", align 1
@.str.14 = private unnamed_addr constant [2 x i8] c"h\00", align 1
@.str.15 = private unnamed_addr constant [2 x i8] c"i\00", align 1
@.str.16 = private unnamed_addr constant [3 x i8] c"ie\00", align 1
@.str.17 = private unnamed_addr constant [2 x i8] c"j\00", align 1
@.str.18 = private unnamed_addr constant [2 x i8] c"k\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c"l\00", align 1
@.str.20 = private unnamed_addr constant [2 x i8] c"m\00", align 1
@.str.21 = private unnamed_addr constant [2 x i8] c"n\00", align 1
@.str.22 = private unnamed_addr constant [3 x i8] c"ng\00", align 1
@.str.23 = private unnamed_addr constant [2 x i8] c"o\00", align 1
@.str.24 = private unnamed_addr constant [3 x i8] c"oh\00", align 1
@.str.25 = private unnamed_addr constant [3 x i8] c"oo\00", align 1
@.str.26 = private unnamed_addr constant [2 x i8] c"p\00", align 1
@.str.27 = private unnamed_addr constant [3 x i8] c"ph\00", align 1
@.str.28 = private unnamed_addr constant [3 x i8] c"qu\00", align 1
@.str.29 = private unnamed_addr constant [2 x i8] c"r\00", align 1
@.str.30 = private unnamed_addr constant [2 x i8] c"s\00", align 1
@.str.31 = private unnamed_addr constant [3 x i8] c"sh\00", align 1
@.str.32 = private unnamed_addr constant [2 x i8] c"t\00", align 1
@.str.33 = private unnamed_addr constant [3 x i8] c"th\00", align 1
@.str.34 = private unnamed_addr constant [2 x i8] c"u\00", align 1
@.str.35 = private unnamed_addr constant [2 x i8] c"v\00", align 1
@.str.36 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str.37 = private unnamed_addr constant [2 x i8] c"x\00", align 1
@.str.38 = private unnamed_addr constant [2 x i8] c"y\00", align 1
@.str.39 = private unnamed_addr constant [2 x i8] c"z\00", align 1
@elements = hidden global [40 x %struct.pw_element] [%struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i32 2 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.5, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0), i32 5 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.7, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.8, i32 0, i32 0), i32 2 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.9, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.10, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.11, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.12, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.13, i32 0, i32 0), i32 13 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.15, i32 0, i32 0), i32 2 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.16, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.17, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.18, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.20, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.21, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.22, i32 0, i32 0), i32 13 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.23, i32 0, i32 0), i32 2 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.24, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.25, i32 0, i32 0), i32 6 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.26, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.27, i32 0, i32 0), i32 5 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.28, i32 0, i32 0), i32 5 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.29, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.30, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.31, i32 0, i32 0), i32 5 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.32, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.33, i32 0, i32 0), i32 5 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.34, i32 0, i32 0), i32 2 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.35, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.36, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.37, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i32 1 }, %struct.pw_element { i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.39, i32 0, i32 0), i32 1 }], align 16
@pw_number = external global i32 (i32)*, align 4
@pw_ambiguous = external global i8*, align 4
@pw_symbols = external global i8*, align 4

; Function Attrs: noinline nounwind optnone
define hidden void @pw_phonemes(i8* %buf, i32 %size, i32 %pw_flags, i8* %remove) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %pw_flags.addr = alloca i32, align 4
  %remove.addr = alloca i8*, align 4
  %c = alloca i32, align 4
  %i = alloca i32, align 4
  %len = alloca i32, align 4
  %flags = alloca i32, align 4
  %feature_flags = alloca i32, align 4
  %prev = alloca i32, align 4
  %should_be = alloca i32, align 4
  %first = alloca i32, align 4
  %str = alloca i8*, align 4
  %ch = alloca i8, align 1
  %cp = alloca i8*, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %pw_flags, i32* %pw_flags.addr, align 4
  store i8* %remove, i8** %remove.addr, align 4
  br label %try_again

try_again:                                        ; preds = %if.then128, %if.then49, %entry
  %0 = load i32, i32* %pw_flags.addr, align 4
  store i32 %0, i32* %feature_flags, align 4
  store i32 0, i32* %c, align 4
  store i32 0, i32* %prev, align 4
  store i32 0, i32* %should_be, align 4
  store i32 1, i32* %first, align 4
  %1 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call = call i32 %1(i32 2)
  %tobool = icmp ne i32 %call, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 2, i32 1
  store i32 %cond, i32* %should_be, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end125, %do.end, %if.then23, %if.then20, %if.then10, %if.then, %try_again
  %3 = load i32, i32* %c, align 4
  %4 = load i32, i32* %size.addr, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call1 = call i32 %5(i32 40)
  store i32 %call1, i32* %i, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [40 x %struct.pw_element], [40 x %struct.pw_element]* @elements, i32 0, i32 %6
  %str2 = getelementptr inbounds %struct.pw_element, %struct.pw_element* %arrayidx, i32 0, i32 0
  %7 = load i8*, i8** %str2, align 8
  store i8* %7, i8** %str, align 4
  %8 = load i8*, i8** %str, align 4
  %call3 = call i32 @strlen(i8* %8)
  store i32 %call3, i32* %len, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [40 x %struct.pw_element], [40 x %struct.pw_element]* @elements, i32 0, i32 %9
  %flags5 = getelementptr inbounds %struct.pw_element, %struct.pw_element* %arrayidx4, i32 0, i32 1
  %10 = load i32, i32* %flags5, align 4
  store i32 %10, i32* %flags, align 4
  %11 = load i32, i32* %flags, align 4
  %12 = load i32, i32* %should_be, align 4
  %and = and i32 %11, %12
  %cmp6 = icmp eq i32 %and, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  br label %while.cond

if.end:                                           ; preds = %while.body
  %13 = load i32, i32* %first, align 4
  %tobool7 = icmp ne i32 %13, 0
  br i1 %tobool7, label %land.lhs.true, label %if.end11

land.lhs.true:                                    ; preds = %if.end
  %14 = load i32, i32* %flags, align 4
  %and8 = and i32 %14, 8
  %tobool9 = icmp ne i32 %and8, 0
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %land.lhs.true
  br label %while.cond

if.end11:                                         ; preds = %land.lhs.true, %if.end
  %15 = load i32, i32* %prev, align 4
  %and12 = and i32 %15, 2
  %tobool13 = icmp ne i32 %and12, 0
  br i1 %tobool13, label %land.lhs.true14, label %if.end21

land.lhs.true14:                                  ; preds = %if.end11
  %16 = load i32, i32* %flags, align 4
  %and15 = and i32 %16, 2
  %tobool16 = icmp ne i32 %and15, 0
  br i1 %tobool16, label %land.lhs.true17, label %if.end21

land.lhs.true17:                                  ; preds = %land.lhs.true14
  %17 = load i32, i32* %flags, align 4
  %and18 = and i32 %17, 4
  %tobool19 = icmp ne i32 %and18, 0
  br i1 %tobool19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true17
  br label %while.cond

if.end21:                                         ; preds = %land.lhs.true17, %land.lhs.true14, %if.end11
  %18 = load i32, i32* %len, align 4
  %19 = load i32, i32* %size.addr, align 4
  %20 = load i32, i32* %c, align 4
  %sub = sub nsw i32 %19, %20
  %cmp22 = icmp sgt i32 %18, %sub
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end21
  br label %while.cond

if.end24:                                         ; preds = %if.end21
  %21 = load i8*, i8** %buf.addr, align 4
  %22 = load i32, i32* %c, align 4
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8*, i8** %str, align 4
  %call25 = call i8* @strcpy(i8* %add.ptr, i8* %23)
  %24 = load i32, i32* %pw_flags.addr, align 4
  %and26 = and i32 %24, 2
  %tobool27 = icmp ne i32 %and26, 0
  br i1 %tobool27, label %if.then28, label %if.end42

if.then28:                                        ; preds = %if.end24
  %25 = load i32, i32* %first, align 4
  %tobool29 = icmp ne i32 %25, 0
  br i1 %tobool29, label %land.lhs.true32, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then28
  %26 = load i32, i32* %flags, align 4
  %and30 = and i32 %26, 1
  %tobool31 = icmp ne i32 %and30, 0
  br i1 %tobool31, label %land.lhs.true32, label %if.end41

land.lhs.true32:                                  ; preds = %lor.lhs.false, %if.then28
  %27 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call33 = call i32 %27(i32 10)
  %cmp34 = icmp slt i32 %call33, 2
  br i1 %cmp34, label %if.then35, label %if.end41

if.then35:                                        ; preds = %land.lhs.true32
  %28 = load i8*, i8** %buf.addr, align 4
  %29 = load i32, i32* %c, align 4
  %arrayidx36 = getelementptr inbounds i8, i8* %28, i32 %29
  %30 = load i8, i8* %arrayidx36, align 1
  %conv = sext i8 %30 to i32
  %call37 = call i32 @toupper(i32 %conv) #3
  %conv38 = trunc i32 %call37 to i8
  %31 = load i8*, i8** %buf.addr, align 4
  %32 = load i32, i32* %c, align 4
  %arrayidx39 = getelementptr inbounds i8, i8* %31, i32 %32
  store i8 %conv38, i8* %arrayidx39, align 1
  %33 = load i32, i32* %feature_flags, align 4
  %and40 = and i32 %33, -3
  store i32 %and40, i32* %feature_flags, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then35, %land.lhs.true32, %lor.lhs.false
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end24
  %34 = load i32, i32* %pw_flags.addr, align 4
  %and43 = and i32 %34, 8
  %tobool44 = icmp ne i32 %and43, 0
  br i1 %tobool44, label %if.then45, label %if.end51

if.then45:                                        ; preds = %if.end42
  %35 = load i8*, i8** %buf.addr, align 4
  %36 = load i32, i32* %c, align 4
  %37 = load i32, i32* %len, align 4
  %add = add nsw i32 %36, %37
  %arrayidx46 = getelementptr inbounds i8, i8* %35, i32 %add
  store i8 0, i8* %arrayidx46, align 1
  %38 = load i8*, i8** %buf.addr, align 4
  %39 = load i8*, i8** @pw_ambiguous, align 4
  %call47 = call i8* @strpbrk(i8* %38, i8* %39)
  store i8* %call47, i8** %cp, align 4
  %40 = load i8*, i8** %cp, align 4
  %tobool48 = icmp ne i8* %40, null
  br i1 %tobool48, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.then45
  br label %try_again

if.end50:                                         ; preds = %if.then45
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end42
  %41 = load i32, i32* %len, align 4
  %42 = load i32, i32* %c, align 4
  %add52 = add nsw i32 %42, %41
  store i32 %add52, i32* %c, align 4
  %43 = load i32, i32* %c, align 4
  %44 = load i32, i32* %size.addr, align 4
  %cmp53 = icmp sge i32 %43, %44
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end51
  br label %while.end

if.end56:                                         ; preds = %if.end51
  %45 = load i32, i32* %pw_flags.addr, align 4
  %and57 = and i32 %45, 1
  %tobool58 = icmp ne i32 %and57, 0
  br i1 %tobool58, label %if.then59, label %if.end81

if.then59:                                        ; preds = %if.end56
  %46 = load i32, i32* %first, align 4
  %tobool60 = icmp ne i32 %46, 0
  br i1 %tobool60, label %if.end80, label %land.lhs.true61

land.lhs.true61:                                  ; preds = %if.then59
  %47 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call62 = call i32 %47(i32 10)
  %cmp63 = icmp slt i32 %call62, 3
  br i1 %cmp63, label %if.then65, label %if.end80

if.then65:                                        ; preds = %land.lhs.true61
  br label %do.body

do.body:                                          ; preds = %land.end, %if.then65
  %48 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call66 = call i32 %48(i32 10)
  %add67 = add nsw i32 %call66, 48
  %conv68 = trunc i32 %add67 to i8
  store i8 %conv68, i8* %ch, align 1
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %49 = load i32, i32* %pw_flags.addr, align 4
  %and69 = and i32 %49, 8
  %tobool70 = icmp ne i32 %and69, 0
  br i1 %tobool70, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %50 = load i8*, i8** @pw_ambiguous, align 4
  %51 = load i8, i8* %ch, align 1
  %conv71 = sext i8 %51 to i32
  %call72 = call i8* @strchr(i8* %50, i32 %conv71)
  %tobool73 = icmp ne i8* %call72, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %52 = phi i1 [ false, %do.cond ], [ %tobool73, %land.rhs ]
  br i1 %52, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %53 = load i8, i8* %ch, align 1
  %54 = load i8*, i8** %buf.addr, align 4
  %55 = load i32, i32* %c, align 4
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %c, align 4
  %arrayidx74 = getelementptr inbounds i8, i8* %54, i32 %55
  store i8 %53, i8* %arrayidx74, align 1
  %56 = load i8*, i8** %buf.addr, align 4
  %57 = load i32, i32* %c, align 4
  %arrayidx75 = getelementptr inbounds i8, i8* %56, i32 %57
  store i8 0, i8* %arrayidx75, align 1
  %58 = load i32, i32* %feature_flags, align 4
  %and76 = and i32 %58, -2
  store i32 %and76, i32* %feature_flags, align 4
  store i32 1, i32* %first, align 4
  store i32 0, i32* %prev, align 4
  %59 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call77 = call i32 %59(i32 2)
  %tobool78 = icmp ne i32 %call77, 0
  %60 = zext i1 %tobool78 to i64
  %cond79 = select i1 %tobool78, i32 2, i32 1
  store i32 %cond79, i32* %should_be, align 4
  br label %while.cond

if.end80:                                         ; preds = %land.lhs.true61, %if.then59
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %if.end56
  %61 = load i32, i32* %pw_flags.addr, align 4
  %and82 = and i32 %61, 4
  %tobool83 = icmp ne i32 %and82, 0
  br i1 %tobool83, label %if.then84, label %if.end109

if.then84:                                        ; preds = %if.end81
  %62 = load i32, i32* %first, align 4
  %tobool85 = icmp ne i32 %62, 0
  br i1 %tobool85, label %if.end108, label %land.lhs.true86

land.lhs.true86:                                  ; preds = %if.then84
  %63 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call87 = call i32 %63(i32 10)
  %cmp88 = icmp slt i32 %call87, 2
  br i1 %cmp88, label %if.then90, label %if.end108

if.then90:                                        ; preds = %land.lhs.true86
  br label %do.body91

do.body91:                                        ; preds = %land.end102, %if.then90
  %64 = load i8*, i8** @pw_symbols, align 4
  %65 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %66 = load i8*, i8** @pw_symbols, align 4
  %call92 = call i32 @strlen(i8* %66)
  %call93 = call i32 %65(i32 %call92)
  %arrayidx94 = getelementptr inbounds i8, i8* %64, i32 %call93
  %67 = load i8, i8* %arrayidx94, align 1
  store i8 %67, i8* %ch, align 1
  br label %do.cond95

do.cond95:                                        ; preds = %do.body91
  %68 = load i32, i32* %pw_flags.addr, align 4
  %and96 = and i32 %68, 8
  %tobool97 = icmp ne i32 %and96, 0
  br i1 %tobool97, label %land.rhs98, label %land.end102

land.rhs98:                                       ; preds = %do.cond95
  %69 = load i8*, i8** @pw_ambiguous, align 4
  %70 = load i8, i8* %ch, align 1
  %conv99 = sext i8 %70 to i32
  %call100 = call i8* @strchr(i8* %69, i32 %conv99)
  %tobool101 = icmp ne i8* %call100, null
  br label %land.end102

land.end102:                                      ; preds = %land.rhs98, %do.cond95
  %71 = phi i1 [ false, %do.cond95 ], [ %tobool101, %land.rhs98 ]
  br i1 %71, label %do.body91, label %do.end103

do.end103:                                        ; preds = %land.end102
  %72 = load i8, i8* %ch, align 1
  %73 = load i8*, i8** %buf.addr, align 4
  %74 = load i32, i32* %c, align 4
  %inc104 = add nsw i32 %74, 1
  store i32 %inc104, i32* %c, align 4
  %arrayidx105 = getelementptr inbounds i8, i8* %73, i32 %74
  store i8 %72, i8* %arrayidx105, align 1
  %75 = load i8*, i8** %buf.addr, align 4
  %76 = load i32, i32* %c, align 4
  %arrayidx106 = getelementptr inbounds i8, i8* %75, i32 %76
  store i8 0, i8* %arrayidx106, align 1
  %77 = load i32, i32* %feature_flags, align 4
  %and107 = and i32 %77, -5
  store i32 %and107, i32* %feature_flags, align 4
  br label %if.end108

if.end108:                                        ; preds = %do.end103, %land.lhs.true86, %if.then84
  br label %if.end109

if.end109:                                        ; preds = %if.end108, %if.end81
  %78 = load i32, i32* %should_be, align 4
  %cmp110 = icmp eq i32 %78, 1
  br i1 %cmp110, label %if.then112, label %if.else

if.then112:                                       ; preds = %if.end109
  store i32 2, i32* %should_be, align 4
  br label %if.end125

if.else:                                          ; preds = %if.end109
  %79 = load i32, i32* %prev, align 4
  %and113 = and i32 %79, 2
  %tobool114 = icmp ne i32 %and113, 0
  br i1 %tobool114, label %if.then122, label %lor.lhs.false115

lor.lhs.false115:                                 ; preds = %if.else
  %80 = load i32, i32* %flags, align 4
  %and116 = and i32 %80, 4
  %tobool117 = icmp ne i32 %and116, 0
  br i1 %tobool117, label %if.then122, label %lor.lhs.false118

lor.lhs.false118:                                 ; preds = %lor.lhs.false115
  %81 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %call119 = call i32 %81(i32 10)
  %cmp120 = icmp sgt i32 %call119, 3
  br i1 %cmp120, label %if.then122, label %if.else123

if.then122:                                       ; preds = %lor.lhs.false118, %lor.lhs.false115, %if.else
  store i32 1, i32* %should_be, align 4
  br label %if.end124

if.else123:                                       ; preds = %lor.lhs.false118
  store i32 2, i32* %should_be, align 4
  br label %if.end124

if.end124:                                        ; preds = %if.else123, %if.then122
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.then112
  %82 = load i32, i32* %flags, align 4
  store i32 %82, i32* %prev, align 4
  store i32 0, i32* %first, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then55, %while.cond
  %83 = load i32, i32* %feature_flags, align 4
  %and126 = and i32 %83, 7
  %tobool127 = icmp ne i32 %and126, 0
  br i1 %tobool127, label %if.then128, label %if.end129

if.then128:                                       ; preds = %while.end
  br label %try_again

if.end129:                                        ; preds = %while.end
  ret void
}

declare i32 @strlen(i8*) #1

declare i8* @strcpy(i8*, i8*) #1

; Function Attrs: nounwind readonly
declare i32 @toupper(i32) #2

declare i8* @strpbrk(i8*, i8*) #1

declare i8* @strchr(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
