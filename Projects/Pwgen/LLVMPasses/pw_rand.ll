; ModuleID = 'pw_rand.c'
source_filename = "pw_rand.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque

@.str = private unnamed_addr constant [11 x i8] c"0123456789\00", align 1
@pw_digits = hidden global i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0), align 4
@.str.1 = private unnamed_addr constant [27 x i8] c"ABCDEFGHIJKLMNOPQRSTUVWXYZ\00", align 1
@pw_uppers = hidden global i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0), align 4
@.str.2 = private unnamed_addr constant [27 x i8] c"abcdefghijklmnopqrstuvwxyz\00", align 1
@pw_lowers = hidden global i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0), align 4
@.str.3 = private unnamed_addr constant [33 x i8] c"!\22#$%&'()*+,-./:;<=>?@[\\]^_`{|}~\00", align 1
@pw_symbols = hidden global i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.3, i32 0, i32 0), align 4
@.str.4 = private unnamed_addr constant [16 x i8] c"B8G6I1l0OQDS5Z2\00", align 1
@pw_ambiguous = hidden global i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.4, i32 0, i32 0), align 4
@.str.5 = private unnamed_addr constant [15 x i8] c"01aeiouyAEIOUY\00", align 1
@pw_vowels = hidden global i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0), align 4
@stderr = external constant %struct._IO_FILE*, align 4
@.str.6 = private unnamed_addr constant [33 x i8] c"Couldn't malloc pw_rand buffer.\0A\00", align 1
@.str.7 = private unnamed_addr constant [40 x i8] c"Error: No digits left in the valid set\0A\00", align 1
@.str.8 = private unnamed_addr constant [52 x i8] c"Error: No upper case letters left in the valid set\0A\00", align 1
@.str.9 = private unnamed_addr constant [41 x i8] c"Error: No symbols left in the valid set\0A\00", align 1
@.str.10 = private unnamed_addr constant [44 x i8] c"Error: No characters left in the valid set\0A\00", align 1
@pw_number = external global i32 (i32)*, align 4

; Function Attrs: noinline nounwind optnone
define hidden void @pw_rand(i8* %buf, i32 %size, i32 %pw_flags, i8* %remove) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %pw_flags.addr = alloca i32, align 4
  %remove.addr = alloca i8*, align 4
  %ch = alloca i8, align 1
  %chars = alloca i8*, align 4
  %wchars = alloca i8*, align 4
  %i = alloca i32, align 4
  %len = alloca i32, align 4
  %feature_flags = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %pw_flags, i32* %pw_flags.addr, align 4
  store i8* %remove, i8** %remove.addr, align 4
  store i32 0, i32* %len, align 4
  %0 = load i32, i32* %pw_flags.addr, align 4
  %and = and i32 %0, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** @pw_digits, align 4
  %call = call i32 @strlen(i8* %1)
  %2 = load i32, i32* %len, align 4
  %add = add i32 %2, %call
  store i32 %add, i32* %len, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32, i32* %pw_flags.addr, align 4
  %and1 = and i32 %3, 2
  %tobool2 = icmp ne i32 %and1, 0
  br i1 %tobool2, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.end
  %4 = load i8*, i8** @pw_uppers, align 4
  %call4 = call i32 @strlen(i8* %4)
  %5 = load i32, i32* %len, align 4
  %add5 = add i32 %5, %call4
  store i32 %add5, i32* %len, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then3, %if.end
  %6 = load i8*, i8** @pw_lowers, align 4
  %call7 = call i32 @strlen(i8* %6)
  %7 = load i32, i32* %len, align 4
  %add8 = add i32 %7, %call7
  store i32 %add8, i32* %len, align 4
  %8 = load i32, i32* %pw_flags.addr, align 4
  %and9 = and i32 %8, 4
  %tobool10 = icmp ne i32 %and9, 0
  br i1 %tobool10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %if.end6
  %9 = load i8*, i8** @pw_symbols, align 4
  %call12 = call i32 @strlen(i8* %9)
  %10 = load i32, i32* %len, align 4
  %add13 = add i32 %10, %call12
  store i32 %add13, i32* %len, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %if.end6
  %11 = load i32, i32* %len, align 4
  %add15 = add nsw i32 %11, 1
  %call16 = call i8* @malloc(i32 %add15)
  store i8* %call16, i8** %chars, align 4
  %12 = load i8*, i8** %chars, align 4
  %tobool17 = icmp ne i8* %12, null
  br i1 %tobool17, label %if.end20, label %if.then18

if.then18:                                        ; preds = %if.end14
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.6, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end20:                                         ; preds = %if.end14
  %14 = load i8*, i8** %chars, align 4
  store i8* %14, i8** %wchars, align 4
  %15 = load i32, i32* %pw_flags.addr, align 4
  %and21 = and i32 %15, 1
  %tobool22 = icmp ne i32 %and21, 0
  br i1 %tobool22, label %if.then23, label %if.end26

if.then23:                                        ; preds = %if.end20
  %16 = load i8*, i8** %wchars, align 4
  %17 = load i8*, i8** @pw_digits, align 4
  %call24 = call i8* @strcpy(i8* %16, i8* %17)
  %18 = load i8*, i8** @pw_digits, align 4
  %call25 = call i32 @strlen(i8* %18)
  %19 = load i8*, i8** %wchars, align 4
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %call25
  store i8* %add.ptr, i8** %wchars, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.then23, %if.end20
  %20 = load i32, i32* %pw_flags.addr, align 4
  %and27 = and i32 %20, 2
  %tobool28 = icmp ne i32 %and27, 0
  br i1 %tobool28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end26
  %21 = load i8*, i8** %wchars, align 4
  %22 = load i8*, i8** @pw_uppers, align 4
  %call30 = call i8* @strcpy(i8* %21, i8* %22)
  %23 = load i8*, i8** @pw_uppers, align 4
  %call31 = call i32 @strlen(i8* %23)
  %24 = load i8*, i8** %wchars, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %24, i32 %call31
  store i8* %add.ptr32, i8** %wchars, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end26
  %25 = load i8*, i8** %wchars, align 4
  %26 = load i8*, i8** @pw_lowers, align 4
  %call34 = call i8* @strcpy(i8* %25, i8* %26)
  %27 = load i8*, i8** @pw_lowers, align 4
  %call35 = call i32 @strlen(i8* %27)
  %28 = load i8*, i8** %wchars, align 4
  %add.ptr36 = getelementptr inbounds i8, i8* %28, i32 %call35
  store i8* %add.ptr36, i8** %wchars, align 4
  %29 = load i32, i32* %pw_flags.addr, align 4
  %and37 = and i32 %29, 4
  %tobool38 = icmp ne i32 %and37, 0
  br i1 %tobool38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.end33
  %30 = load i8*, i8** %wchars, align 4
  %31 = load i8*, i8** @pw_symbols, align 4
  %call40 = call i8* @strcpy(i8* %30, i8* %31)
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.end33
  %32 = load i8*, i8** %remove.addr, align 4
  %tobool42 = icmp ne i8* %32, null
  br i1 %tobool42, label %if.then43, label %if.end79

if.then43:                                        ; preds = %if.end41
  %33 = load i32, i32* %pw_flags.addr, align 4
  %and44 = and i32 %33, 8
  %tobool45 = icmp ne i32 %and44, 0
  br i1 %tobool45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.then43
  %34 = load i8*, i8** %chars, align 4
  %35 = load i8*, i8** @pw_ambiguous, align 4
  call void @remove_chars(i8* %34, i8* %35)
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %if.then43
  %36 = load i32, i32* %pw_flags.addr, align 4
  %and48 = and i32 %36, 16
  %tobool49 = icmp ne i32 %and48, 0
  br i1 %tobool49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end47
  %37 = load i8*, i8** %chars, align 4
  %38 = load i8*, i8** @pw_vowels, align 4
  call void @remove_chars(i8* %37, i8* %38)
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %if.end47
  %39 = load i8*, i8** %chars, align 4
  %40 = load i8*, i8** %remove.addr, align 4
  call void @remove_chars(i8* %39, i8* %40)
  %41 = load i32, i32* %pw_flags.addr, align 4
  %and52 = and i32 %41, 1
  %tobool53 = icmp ne i32 %and52, 0
  br i1 %tobool53, label %land.lhs.true, label %if.end58

land.lhs.true:                                    ; preds = %if.end51
  %42 = load i8*, i8** %chars, align 4
  %43 = load i8*, i8** @pw_digits, align 4
  %call54 = call i32 @find_chars(i8* %42, i8* %43)
  %tobool55 = icmp ne i32 %call54, 0
  br i1 %tobool55, label %if.end58, label %if.then56

if.then56:                                        ; preds = %land.lhs.true
  %44 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call57 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %44, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.7, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end58:                                         ; preds = %land.lhs.true, %if.end51
  %45 = load i32, i32* %pw_flags.addr, align 4
  %and59 = and i32 %45, 2
  %tobool60 = icmp ne i32 %and59, 0
  br i1 %tobool60, label %land.lhs.true61, label %if.end66

land.lhs.true61:                                  ; preds = %if.end58
  %46 = load i8*, i8** %chars, align 4
  %47 = load i8*, i8** @pw_uppers, align 4
  %call62 = call i32 @find_chars(i8* %46, i8* %47)
  %tobool63 = icmp ne i32 %call62, 0
  br i1 %tobool63, label %if.end66, label %if.then64

if.then64:                                        ; preds = %land.lhs.true61
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call65 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.8, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end66:                                         ; preds = %land.lhs.true61, %if.end58
  %49 = load i32, i32* %pw_flags.addr, align 4
  %and67 = and i32 %49, 4
  %tobool68 = icmp ne i32 %and67, 0
  br i1 %tobool68, label %land.lhs.true69, label %if.end74

land.lhs.true69:                                  ; preds = %if.end66
  %50 = load i8*, i8** %chars, align 4
  %51 = load i8*, i8** @pw_symbols, align 4
  %call70 = call i32 @find_chars(i8* %50, i8* %51)
  %tobool71 = icmp ne i32 %call70, 0
  br i1 %tobool71, label %if.end74, label %if.then72

if.then72:                                        ; preds = %land.lhs.true69
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call73 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.9, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end74:                                         ; preds = %land.lhs.true69, %if.end66
  %53 = load i8*, i8** %chars, align 4
  %arrayidx = getelementptr inbounds i8, i8* %53, i32 0
  %54 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %54 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.end74
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call77 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %55, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.10, i32 0, i32 0))
  call void @exit(i32 1) #4
  unreachable

if.end78:                                         ; preds = %if.end74
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.end41
  %56 = load i8*, i8** %chars, align 4
  %call80 = call i32 @strlen(i8* %56)
  store i32 %call80, i32* %len, align 4
  br label %try_again

try_again:                                        ; preds = %if.then133, %if.end79
  %57 = load i32, i32* %size.addr, align 4
  %cmp81 = icmp sgt i32 %57, 2
  br i1 %cmp81, label %cond.true, label %cond.false

cond.true:                                        ; preds = %try_again
  %58 = load i32, i32* %pw_flags.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %try_again
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %58, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %feature_flags, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end130, %if.then101, %if.then93, %cond.end
  %59 = load i32, i32* %i, align 4
  %60 = load i32, i32* %size.addr, align 4
  %cmp83 = icmp slt i32 %59, %60
  br i1 %cmp83, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %61 = load i8*, i8** %chars, align 4
  %62 = load i32 (i32)*, i32 (i32)** @pw_number, align 4
  %63 = load i32, i32* %len, align 4
  %call85 = call i32 %62(i32 %63)
  %arrayidx86 = getelementptr inbounds i8, i8* %61, i32 %call85
  %64 = load i8, i8* %arrayidx86, align 1
  store i8 %64, i8* %ch, align 1
  %65 = load i32, i32* %pw_flags.addr, align 4
  %and87 = and i32 %65, 8
  %tobool88 = icmp ne i32 %and87, 0
  br i1 %tobool88, label %land.lhs.true89, label %if.end94

land.lhs.true89:                                  ; preds = %while.body
  %66 = load i8*, i8** @pw_ambiguous, align 4
  %67 = load i8, i8* %ch, align 1
  %conv90 = sext i8 %67 to i32
  %call91 = call i8* @strchr(i8* %66, i32 %conv90)
  %tobool92 = icmp ne i8* %call91, null
  br i1 %tobool92, label %if.then93, label %if.end94

if.then93:                                        ; preds = %land.lhs.true89
  br label %while.cond

if.end94:                                         ; preds = %land.lhs.true89, %while.body
  %68 = load i32, i32* %pw_flags.addr, align 4
  %and95 = and i32 %68, 16
  %tobool96 = icmp ne i32 %and95, 0
  br i1 %tobool96, label %land.lhs.true97, label %if.end102

land.lhs.true97:                                  ; preds = %if.end94
  %69 = load i8*, i8** @pw_vowels, align 4
  %70 = load i8, i8* %ch, align 1
  %conv98 = sext i8 %70 to i32
  %call99 = call i8* @strchr(i8* %69, i32 %conv98)
  %tobool100 = icmp ne i8* %call99, null
  br i1 %tobool100, label %if.then101, label %if.end102

if.then101:                                       ; preds = %land.lhs.true97
  br label %while.cond

if.end102:                                        ; preds = %land.lhs.true97, %if.end94
  %71 = load i8, i8* %ch, align 1
  %72 = load i8*, i8** %buf.addr, align 4
  %73 = load i32, i32* %i, align 4
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4
  %arrayidx103 = getelementptr inbounds i8, i8* %72, i32 %73
  store i8 %71, i8* %arrayidx103, align 1
  %74 = load i32, i32* %feature_flags, align 4
  %and104 = and i32 %74, 1
  %tobool105 = icmp ne i32 %and104, 0
  br i1 %tobool105, label %land.lhs.true106, label %if.end112

land.lhs.true106:                                 ; preds = %if.end102
  %75 = load i8*, i8** @pw_digits, align 4
  %76 = load i8, i8* %ch, align 1
  %conv107 = sext i8 %76 to i32
  %call108 = call i8* @strchr(i8* %75, i32 %conv107)
  %tobool109 = icmp ne i8* %call108, null
  br i1 %tobool109, label %if.then110, label %if.end112

if.then110:                                       ; preds = %land.lhs.true106
  %77 = load i32, i32* %feature_flags, align 4
  %and111 = and i32 %77, -2
  store i32 %and111, i32* %feature_flags, align 4
  br label %if.end112

if.end112:                                        ; preds = %if.then110, %land.lhs.true106, %if.end102
  %78 = load i32, i32* %feature_flags, align 4
  %and113 = and i32 %78, 2
  %tobool114 = icmp ne i32 %and113, 0
  br i1 %tobool114, label %land.lhs.true115, label %if.end121

land.lhs.true115:                                 ; preds = %if.end112
  %79 = load i8*, i8** @pw_uppers, align 4
  %80 = load i8, i8* %ch, align 1
  %conv116 = sext i8 %80 to i32
  %call117 = call i8* @strchr(i8* %79, i32 %conv116)
  %tobool118 = icmp ne i8* %call117, null
  br i1 %tobool118, label %if.then119, label %if.end121

if.then119:                                       ; preds = %land.lhs.true115
  %81 = load i32, i32* %feature_flags, align 4
  %and120 = and i32 %81, -3
  store i32 %and120, i32* %feature_flags, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then119, %land.lhs.true115, %if.end112
  %82 = load i32, i32* %feature_flags, align 4
  %and122 = and i32 %82, 4
  %tobool123 = icmp ne i32 %and122, 0
  br i1 %tobool123, label %land.lhs.true124, label %if.end130

land.lhs.true124:                                 ; preds = %if.end121
  %83 = load i8*, i8** @pw_symbols, align 4
  %84 = load i8, i8* %ch, align 1
  %conv125 = sext i8 %84 to i32
  %call126 = call i8* @strchr(i8* %83, i32 %conv125)
  %tobool127 = icmp ne i8* %call126, null
  br i1 %tobool127, label %if.then128, label %if.end130

if.then128:                                       ; preds = %land.lhs.true124
  %85 = load i32, i32* %feature_flags, align 4
  %and129 = and i32 %85, -5
  store i32 %and129, i32* %feature_flags, align 4
  br label %if.end130

if.end130:                                        ; preds = %if.then128, %land.lhs.true124, %if.end121
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %86 = load i32, i32* %feature_flags, align 4
  %and131 = and i32 %86, 7
  %tobool132 = icmp ne i32 %and131, 0
  br i1 %tobool132, label %if.then133, label %if.end134

if.then133:                                       ; preds = %while.end
  br label %try_again

if.end134:                                        ; preds = %while.end
  %87 = load i8*, i8** %buf.addr, align 4
  %88 = load i32, i32* %size.addr, align 4
  %arrayidx135 = getelementptr inbounds i8, i8* %87, i32 %88
  store i8 0, i8* %arrayidx135, align 1
  %89 = load i8*, i8** %chars, align 4
  call void @free(i8* %89)
  ret void
}

declare i32 @strlen(i8*) #1

declare i8* @malloc(i32) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

declare i8* @strcpy(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @remove_chars(i8* %buf, i8* %remove) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %remove.addr = alloca i8*, align 4
  %cp = alloca i8*, align 4
  %r = alloca i8*, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i8* %remove, i8** %remove.addr, align 4
  %0 = load i8*, i8** %remove.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %for.end

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %remove.addr, align 4
  store i8* %1, i8** %cp, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i8*, i8** %cp, align 4
  %3 = load i8, i8* %2, align 1
  %tobool1 = icmp ne i8 %3, 0
  br i1 %tobool1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %buf.addr, align 4
  %5 = load i8*, i8** %cp, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %call = call i8* @strchr(i8* %4, i32 %conv)
  store i8* %call, i8** %r, align 4
  %7 = load i8*, i8** %r, align 4
  %cmp = icmp eq i8* %7, null
  br i1 %cmp, label %if.then3, label %if.end4

if.then3:                                         ; preds = %for.body
  br label %for.inc

if.end4:                                          ; preds = %for.body
  %8 = load i8*, i8** %r, align 4
  %9 = load i8*, i8** %r, align 4
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 1
  %10 = load i8*, i8** %r, align 4
  %call5 = call i32 @strlen(i8* %10)
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %add.ptr, i32 %call5, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %if.end4, %if.then3
  %11 = load i8*, i8** %cp, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %11, i32 1
  store i8* %incdec.ptr, i8** %cp, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @find_chars(i8* %buf, i8* %set) #0 {
entry:
  %retval = alloca i32, align 4
  %buf.addr = alloca i8*, align 4
  %set.addr = alloca i8*, align 4
  %cp = alloca i8*, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i8* %set, i8** %set.addr, align 4
  %0 = load i8*, i8** %set.addr, align 4
  store i8* %0, i8** %cp, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i8*, i8** %cp, align 4
  %2 = load i8, i8* %1, align 1
  %tobool = icmp ne i8 %2, 0
  br i1 %tobool, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %buf.addr, align 4
  %4 = load i8*, i8** %cp, align 4
  %5 = load i8, i8* %4, align 1
  %conv = sext i8 %5 to i32
  %call = call i8* @strchr(i8* %3, i32 %conv)
  %tobool1 = icmp ne i8* %call, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i8*, i8** %cp, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %cp, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

declare i8* @strchr(i8*, i32) #1

declare void @free(i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
