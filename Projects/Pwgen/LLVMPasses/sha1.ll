; ModuleID = 'sha1.c'
source_filename = "sha1.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.sha1_context = type { [2 x i32], [5 x i32], [64 x i8] }

@sha1_padding = internal global <{ i8, [63 x i8] }> <{ i8 -128, [63 x i8] zeroinitializer }>, align 16

; Function Attrs: noinline nounwind optnone
define hidden void @sha1_starts(%struct.sha1_context* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.sha1_context*, align 4
  store %struct.sha1_context* %ctx, %struct.sha1_context** %ctx.addr, align 4
  %0 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %total, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4
  %1 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total1 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %total1, i32 0, i32 1
  store i32 0, i32* %arrayidx2, align 4
  %2 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %2, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [5 x i32], [5 x i32]* %state, i32 0, i32 0
  store i32 1732584193, i32* %arrayidx3, align 4
  %3 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state4 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %3, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [5 x i32], [5 x i32]* %state4, i32 0, i32 1
  store i32 -271733879, i32* %arrayidx5, align 4
  %4 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state6 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %4, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [5 x i32], [5 x i32]* %state6, i32 0, i32 2
  store i32 -1732584194, i32* %arrayidx7, align 4
  %5 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state8 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %5, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [5 x i32], [5 x i32]* %state8, i32 0, i32 3
  store i32 271733878, i32* %arrayidx9, align 4
  %6 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state10 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %6, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [5 x i32], [5 x i32]* %state10, i32 0, i32 4
  store i32 -1009589776, i32* %arrayidx11, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @sha1_process(%struct.sha1_context* %ctx, i8* %data) #0 {
entry:
  %ctx.addr = alloca %struct.sha1_context*, align 4
  %data.addr = alloca i8*, align 4
  %temp = alloca i32, align 4
  %W = alloca [16 x i32], align 16
  %A = alloca i32, align 4
  %B = alloca i32, align 4
  %C = alloca i32, align 4
  %D = alloca i32, align 4
  %E = alloca i32, align 4
  store %struct.sha1_context* %ctx, %struct.sha1_context** %ctx.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load i8*, i8** %data.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %shl = shl i32 %conv, 24
  %2 = load i8*, i8** %data.addr, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %2, i32 1
  %3 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %3 to i32
  %shl3 = shl i32 %conv2, 16
  %or = or i32 %shl, %shl3
  %4 = load i8*, i8** %data.addr, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %4, i32 2
  %5 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %5 to i32
  %shl6 = shl i32 %conv5, 8
  %or7 = or i32 %or, %shl6
  %6 = load i8*, i8** %data.addr, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %6, i32 3
  %7 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %7 to i32
  %or10 = or i32 %or7, %conv9
  %arrayidx11 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  store i32 %or10, i32* %arrayidx11, align 16
  %8 = load i8*, i8** %data.addr, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %8, i32 4
  %9 = load i8, i8* %arrayidx12, align 1
  %conv13 = zext i8 %9 to i32
  %shl14 = shl i32 %conv13, 24
  %10 = load i8*, i8** %data.addr, align 4
  %arrayidx15 = getelementptr inbounds i8, i8* %10, i32 5
  %11 = load i8, i8* %arrayidx15, align 1
  %conv16 = zext i8 %11 to i32
  %shl17 = shl i32 %conv16, 16
  %or18 = or i32 %shl14, %shl17
  %12 = load i8*, i8** %data.addr, align 4
  %arrayidx19 = getelementptr inbounds i8, i8* %12, i32 6
  %13 = load i8, i8* %arrayidx19, align 1
  %conv20 = zext i8 %13 to i32
  %shl21 = shl i32 %conv20, 8
  %or22 = or i32 %or18, %shl21
  %14 = load i8*, i8** %data.addr, align 4
  %arrayidx23 = getelementptr inbounds i8, i8* %14, i32 7
  %15 = load i8, i8* %arrayidx23, align 1
  %conv24 = zext i8 %15 to i32
  %or25 = or i32 %or22, %conv24
  %arrayidx26 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  store i32 %or25, i32* %arrayidx26, align 4
  %16 = load i8*, i8** %data.addr, align 4
  %arrayidx27 = getelementptr inbounds i8, i8* %16, i32 8
  %17 = load i8, i8* %arrayidx27, align 1
  %conv28 = zext i8 %17 to i32
  %shl29 = shl i32 %conv28, 24
  %18 = load i8*, i8** %data.addr, align 4
  %arrayidx30 = getelementptr inbounds i8, i8* %18, i32 9
  %19 = load i8, i8* %arrayidx30, align 1
  %conv31 = zext i8 %19 to i32
  %shl32 = shl i32 %conv31, 16
  %or33 = or i32 %shl29, %shl32
  %20 = load i8*, i8** %data.addr, align 4
  %arrayidx34 = getelementptr inbounds i8, i8* %20, i32 10
  %21 = load i8, i8* %arrayidx34, align 1
  %conv35 = zext i8 %21 to i32
  %shl36 = shl i32 %conv35, 8
  %or37 = or i32 %or33, %shl36
  %22 = load i8*, i8** %data.addr, align 4
  %arrayidx38 = getelementptr inbounds i8, i8* %22, i32 11
  %23 = load i8, i8* %arrayidx38, align 1
  %conv39 = zext i8 %23 to i32
  %or40 = or i32 %or37, %conv39
  %arrayidx41 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  store i32 %or40, i32* %arrayidx41, align 8
  %24 = load i8*, i8** %data.addr, align 4
  %arrayidx42 = getelementptr inbounds i8, i8* %24, i32 12
  %25 = load i8, i8* %arrayidx42, align 1
  %conv43 = zext i8 %25 to i32
  %shl44 = shl i32 %conv43, 24
  %26 = load i8*, i8** %data.addr, align 4
  %arrayidx45 = getelementptr inbounds i8, i8* %26, i32 13
  %27 = load i8, i8* %arrayidx45, align 1
  %conv46 = zext i8 %27 to i32
  %shl47 = shl i32 %conv46, 16
  %or48 = or i32 %shl44, %shl47
  %28 = load i8*, i8** %data.addr, align 4
  %arrayidx49 = getelementptr inbounds i8, i8* %28, i32 14
  %29 = load i8, i8* %arrayidx49, align 1
  %conv50 = zext i8 %29 to i32
  %shl51 = shl i32 %conv50, 8
  %or52 = or i32 %or48, %shl51
  %30 = load i8*, i8** %data.addr, align 4
  %arrayidx53 = getelementptr inbounds i8, i8* %30, i32 15
  %31 = load i8, i8* %arrayidx53, align 1
  %conv54 = zext i8 %31 to i32
  %or55 = or i32 %or52, %conv54
  %arrayidx56 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  store i32 %or55, i32* %arrayidx56, align 4
  %32 = load i8*, i8** %data.addr, align 4
  %arrayidx57 = getelementptr inbounds i8, i8* %32, i32 16
  %33 = load i8, i8* %arrayidx57, align 1
  %conv58 = zext i8 %33 to i32
  %shl59 = shl i32 %conv58, 24
  %34 = load i8*, i8** %data.addr, align 4
  %arrayidx60 = getelementptr inbounds i8, i8* %34, i32 17
  %35 = load i8, i8* %arrayidx60, align 1
  %conv61 = zext i8 %35 to i32
  %shl62 = shl i32 %conv61, 16
  %or63 = or i32 %shl59, %shl62
  %36 = load i8*, i8** %data.addr, align 4
  %arrayidx64 = getelementptr inbounds i8, i8* %36, i32 18
  %37 = load i8, i8* %arrayidx64, align 1
  %conv65 = zext i8 %37 to i32
  %shl66 = shl i32 %conv65, 8
  %or67 = or i32 %or63, %shl66
  %38 = load i8*, i8** %data.addr, align 4
  %arrayidx68 = getelementptr inbounds i8, i8* %38, i32 19
  %39 = load i8, i8* %arrayidx68, align 1
  %conv69 = zext i8 %39 to i32
  %or70 = or i32 %or67, %conv69
  %arrayidx71 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  store i32 %or70, i32* %arrayidx71, align 16
  %40 = load i8*, i8** %data.addr, align 4
  %arrayidx72 = getelementptr inbounds i8, i8* %40, i32 20
  %41 = load i8, i8* %arrayidx72, align 1
  %conv73 = zext i8 %41 to i32
  %shl74 = shl i32 %conv73, 24
  %42 = load i8*, i8** %data.addr, align 4
  %arrayidx75 = getelementptr inbounds i8, i8* %42, i32 21
  %43 = load i8, i8* %arrayidx75, align 1
  %conv76 = zext i8 %43 to i32
  %shl77 = shl i32 %conv76, 16
  %or78 = or i32 %shl74, %shl77
  %44 = load i8*, i8** %data.addr, align 4
  %arrayidx79 = getelementptr inbounds i8, i8* %44, i32 22
  %45 = load i8, i8* %arrayidx79, align 1
  %conv80 = zext i8 %45 to i32
  %shl81 = shl i32 %conv80, 8
  %or82 = or i32 %or78, %shl81
  %46 = load i8*, i8** %data.addr, align 4
  %arrayidx83 = getelementptr inbounds i8, i8* %46, i32 23
  %47 = load i8, i8* %arrayidx83, align 1
  %conv84 = zext i8 %47 to i32
  %or85 = or i32 %or82, %conv84
  %arrayidx86 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  store i32 %or85, i32* %arrayidx86, align 4
  %48 = load i8*, i8** %data.addr, align 4
  %arrayidx87 = getelementptr inbounds i8, i8* %48, i32 24
  %49 = load i8, i8* %arrayidx87, align 1
  %conv88 = zext i8 %49 to i32
  %shl89 = shl i32 %conv88, 24
  %50 = load i8*, i8** %data.addr, align 4
  %arrayidx90 = getelementptr inbounds i8, i8* %50, i32 25
  %51 = load i8, i8* %arrayidx90, align 1
  %conv91 = zext i8 %51 to i32
  %shl92 = shl i32 %conv91, 16
  %or93 = or i32 %shl89, %shl92
  %52 = load i8*, i8** %data.addr, align 4
  %arrayidx94 = getelementptr inbounds i8, i8* %52, i32 26
  %53 = load i8, i8* %arrayidx94, align 1
  %conv95 = zext i8 %53 to i32
  %shl96 = shl i32 %conv95, 8
  %or97 = or i32 %or93, %shl96
  %54 = load i8*, i8** %data.addr, align 4
  %arrayidx98 = getelementptr inbounds i8, i8* %54, i32 27
  %55 = load i8, i8* %arrayidx98, align 1
  %conv99 = zext i8 %55 to i32
  %or100 = or i32 %or97, %conv99
  %arrayidx101 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  store i32 %or100, i32* %arrayidx101, align 8
  %56 = load i8*, i8** %data.addr, align 4
  %arrayidx102 = getelementptr inbounds i8, i8* %56, i32 28
  %57 = load i8, i8* %arrayidx102, align 1
  %conv103 = zext i8 %57 to i32
  %shl104 = shl i32 %conv103, 24
  %58 = load i8*, i8** %data.addr, align 4
  %arrayidx105 = getelementptr inbounds i8, i8* %58, i32 29
  %59 = load i8, i8* %arrayidx105, align 1
  %conv106 = zext i8 %59 to i32
  %shl107 = shl i32 %conv106, 16
  %or108 = or i32 %shl104, %shl107
  %60 = load i8*, i8** %data.addr, align 4
  %arrayidx109 = getelementptr inbounds i8, i8* %60, i32 30
  %61 = load i8, i8* %arrayidx109, align 1
  %conv110 = zext i8 %61 to i32
  %shl111 = shl i32 %conv110, 8
  %or112 = or i32 %or108, %shl111
  %62 = load i8*, i8** %data.addr, align 4
  %arrayidx113 = getelementptr inbounds i8, i8* %62, i32 31
  %63 = load i8, i8* %arrayidx113, align 1
  %conv114 = zext i8 %63 to i32
  %or115 = or i32 %or112, %conv114
  %arrayidx116 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  store i32 %or115, i32* %arrayidx116, align 4
  %64 = load i8*, i8** %data.addr, align 4
  %arrayidx117 = getelementptr inbounds i8, i8* %64, i32 32
  %65 = load i8, i8* %arrayidx117, align 1
  %conv118 = zext i8 %65 to i32
  %shl119 = shl i32 %conv118, 24
  %66 = load i8*, i8** %data.addr, align 4
  %arrayidx120 = getelementptr inbounds i8, i8* %66, i32 33
  %67 = load i8, i8* %arrayidx120, align 1
  %conv121 = zext i8 %67 to i32
  %shl122 = shl i32 %conv121, 16
  %or123 = or i32 %shl119, %shl122
  %68 = load i8*, i8** %data.addr, align 4
  %arrayidx124 = getelementptr inbounds i8, i8* %68, i32 34
  %69 = load i8, i8* %arrayidx124, align 1
  %conv125 = zext i8 %69 to i32
  %shl126 = shl i32 %conv125, 8
  %or127 = or i32 %or123, %shl126
  %70 = load i8*, i8** %data.addr, align 4
  %arrayidx128 = getelementptr inbounds i8, i8* %70, i32 35
  %71 = load i8, i8* %arrayidx128, align 1
  %conv129 = zext i8 %71 to i32
  %or130 = or i32 %or127, %conv129
  %arrayidx131 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  store i32 %or130, i32* %arrayidx131, align 16
  %72 = load i8*, i8** %data.addr, align 4
  %arrayidx132 = getelementptr inbounds i8, i8* %72, i32 36
  %73 = load i8, i8* %arrayidx132, align 1
  %conv133 = zext i8 %73 to i32
  %shl134 = shl i32 %conv133, 24
  %74 = load i8*, i8** %data.addr, align 4
  %arrayidx135 = getelementptr inbounds i8, i8* %74, i32 37
  %75 = load i8, i8* %arrayidx135, align 1
  %conv136 = zext i8 %75 to i32
  %shl137 = shl i32 %conv136, 16
  %or138 = or i32 %shl134, %shl137
  %76 = load i8*, i8** %data.addr, align 4
  %arrayidx139 = getelementptr inbounds i8, i8* %76, i32 38
  %77 = load i8, i8* %arrayidx139, align 1
  %conv140 = zext i8 %77 to i32
  %shl141 = shl i32 %conv140, 8
  %or142 = or i32 %or138, %shl141
  %78 = load i8*, i8** %data.addr, align 4
  %arrayidx143 = getelementptr inbounds i8, i8* %78, i32 39
  %79 = load i8, i8* %arrayidx143, align 1
  %conv144 = zext i8 %79 to i32
  %or145 = or i32 %or142, %conv144
  %arrayidx146 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  store i32 %or145, i32* %arrayidx146, align 4
  %80 = load i8*, i8** %data.addr, align 4
  %arrayidx147 = getelementptr inbounds i8, i8* %80, i32 40
  %81 = load i8, i8* %arrayidx147, align 1
  %conv148 = zext i8 %81 to i32
  %shl149 = shl i32 %conv148, 24
  %82 = load i8*, i8** %data.addr, align 4
  %arrayidx150 = getelementptr inbounds i8, i8* %82, i32 41
  %83 = load i8, i8* %arrayidx150, align 1
  %conv151 = zext i8 %83 to i32
  %shl152 = shl i32 %conv151, 16
  %or153 = or i32 %shl149, %shl152
  %84 = load i8*, i8** %data.addr, align 4
  %arrayidx154 = getelementptr inbounds i8, i8* %84, i32 42
  %85 = load i8, i8* %arrayidx154, align 1
  %conv155 = zext i8 %85 to i32
  %shl156 = shl i32 %conv155, 8
  %or157 = or i32 %or153, %shl156
  %86 = load i8*, i8** %data.addr, align 4
  %arrayidx158 = getelementptr inbounds i8, i8* %86, i32 43
  %87 = load i8, i8* %arrayidx158, align 1
  %conv159 = zext i8 %87 to i32
  %or160 = or i32 %or157, %conv159
  %arrayidx161 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  store i32 %or160, i32* %arrayidx161, align 8
  %88 = load i8*, i8** %data.addr, align 4
  %arrayidx162 = getelementptr inbounds i8, i8* %88, i32 44
  %89 = load i8, i8* %arrayidx162, align 1
  %conv163 = zext i8 %89 to i32
  %shl164 = shl i32 %conv163, 24
  %90 = load i8*, i8** %data.addr, align 4
  %arrayidx165 = getelementptr inbounds i8, i8* %90, i32 45
  %91 = load i8, i8* %arrayidx165, align 1
  %conv166 = zext i8 %91 to i32
  %shl167 = shl i32 %conv166, 16
  %or168 = or i32 %shl164, %shl167
  %92 = load i8*, i8** %data.addr, align 4
  %arrayidx169 = getelementptr inbounds i8, i8* %92, i32 46
  %93 = load i8, i8* %arrayidx169, align 1
  %conv170 = zext i8 %93 to i32
  %shl171 = shl i32 %conv170, 8
  %or172 = or i32 %or168, %shl171
  %94 = load i8*, i8** %data.addr, align 4
  %arrayidx173 = getelementptr inbounds i8, i8* %94, i32 47
  %95 = load i8, i8* %arrayidx173, align 1
  %conv174 = zext i8 %95 to i32
  %or175 = or i32 %or172, %conv174
  %arrayidx176 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  store i32 %or175, i32* %arrayidx176, align 4
  %96 = load i8*, i8** %data.addr, align 4
  %arrayidx177 = getelementptr inbounds i8, i8* %96, i32 48
  %97 = load i8, i8* %arrayidx177, align 1
  %conv178 = zext i8 %97 to i32
  %shl179 = shl i32 %conv178, 24
  %98 = load i8*, i8** %data.addr, align 4
  %arrayidx180 = getelementptr inbounds i8, i8* %98, i32 49
  %99 = load i8, i8* %arrayidx180, align 1
  %conv181 = zext i8 %99 to i32
  %shl182 = shl i32 %conv181, 16
  %or183 = or i32 %shl179, %shl182
  %100 = load i8*, i8** %data.addr, align 4
  %arrayidx184 = getelementptr inbounds i8, i8* %100, i32 50
  %101 = load i8, i8* %arrayidx184, align 1
  %conv185 = zext i8 %101 to i32
  %shl186 = shl i32 %conv185, 8
  %or187 = or i32 %or183, %shl186
  %102 = load i8*, i8** %data.addr, align 4
  %arrayidx188 = getelementptr inbounds i8, i8* %102, i32 51
  %103 = load i8, i8* %arrayidx188, align 1
  %conv189 = zext i8 %103 to i32
  %or190 = or i32 %or187, %conv189
  %arrayidx191 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  store i32 %or190, i32* %arrayidx191, align 16
  %104 = load i8*, i8** %data.addr, align 4
  %arrayidx192 = getelementptr inbounds i8, i8* %104, i32 52
  %105 = load i8, i8* %arrayidx192, align 1
  %conv193 = zext i8 %105 to i32
  %shl194 = shl i32 %conv193, 24
  %106 = load i8*, i8** %data.addr, align 4
  %arrayidx195 = getelementptr inbounds i8, i8* %106, i32 53
  %107 = load i8, i8* %arrayidx195, align 1
  %conv196 = zext i8 %107 to i32
  %shl197 = shl i32 %conv196, 16
  %or198 = or i32 %shl194, %shl197
  %108 = load i8*, i8** %data.addr, align 4
  %arrayidx199 = getelementptr inbounds i8, i8* %108, i32 54
  %109 = load i8, i8* %arrayidx199, align 1
  %conv200 = zext i8 %109 to i32
  %shl201 = shl i32 %conv200, 8
  %or202 = or i32 %or198, %shl201
  %110 = load i8*, i8** %data.addr, align 4
  %arrayidx203 = getelementptr inbounds i8, i8* %110, i32 55
  %111 = load i8, i8* %arrayidx203, align 1
  %conv204 = zext i8 %111 to i32
  %or205 = or i32 %or202, %conv204
  %arrayidx206 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  store i32 %or205, i32* %arrayidx206, align 4
  %112 = load i8*, i8** %data.addr, align 4
  %arrayidx207 = getelementptr inbounds i8, i8* %112, i32 56
  %113 = load i8, i8* %arrayidx207, align 1
  %conv208 = zext i8 %113 to i32
  %shl209 = shl i32 %conv208, 24
  %114 = load i8*, i8** %data.addr, align 4
  %arrayidx210 = getelementptr inbounds i8, i8* %114, i32 57
  %115 = load i8, i8* %arrayidx210, align 1
  %conv211 = zext i8 %115 to i32
  %shl212 = shl i32 %conv211, 16
  %or213 = or i32 %shl209, %shl212
  %116 = load i8*, i8** %data.addr, align 4
  %arrayidx214 = getelementptr inbounds i8, i8* %116, i32 58
  %117 = load i8, i8* %arrayidx214, align 1
  %conv215 = zext i8 %117 to i32
  %shl216 = shl i32 %conv215, 8
  %or217 = or i32 %or213, %shl216
  %118 = load i8*, i8** %data.addr, align 4
  %arrayidx218 = getelementptr inbounds i8, i8* %118, i32 59
  %119 = load i8, i8* %arrayidx218, align 1
  %conv219 = zext i8 %119 to i32
  %or220 = or i32 %or217, %conv219
  %arrayidx221 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  store i32 %or220, i32* %arrayidx221, align 8
  %120 = load i8*, i8** %data.addr, align 4
  %arrayidx222 = getelementptr inbounds i8, i8* %120, i32 60
  %121 = load i8, i8* %arrayidx222, align 1
  %conv223 = zext i8 %121 to i32
  %shl224 = shl i32 %conv223, 24
  %122 = load i8*, i8** %data.addr, align 4
  %arrayidx225 = getelementptr inbounds i8, i8* %122, i32 61
  %123 = load i8, i8* %arrayidx225, align 1
  %conv226 = zext i8 %123 to i32
  %shl227 = shl i32 %conv226, 16
  %or228 = or i32 %shl224, %shl227
  %124 = load i8*, i8** %data.addr, align 4
  %arrayidx229 = getelementptr inbounds i8, i8* %124, i32 62
  %125 = load i8, i8* %arrayidx229, align 1
  %conv230 = zext i8 %125 to i32
  %shl231 = shl i32 %conv230, 8
  %or232 = or i32 %or228, %shl231
  %126 = load i8*, i8** %data.addr, align 4
  %arrayidx233 = getelementptr inbounds i8, i8* %126, i32 63
  %127 = load i8, i8* %arrayidx233, align 1
  %conv234 = zext i8 %127 to i32
  %or235 = or i32 %or232, %conv234
  %arrayidx236 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  store i32 %or235, i32* %arrayidx236, align 4
  %128 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %128, i32 0, i32 1
  %arrayidx237 = getelementptr inbounds [5 x i32], [5 x i32]* %state, i32 0, i32 0
  %129 = load i32, i32* %arrayidx237, align 4
  store i32 %129, i32* %A, align 4
  %130 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state238 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %130, i32 0, i32 1
  %arrayidx239 = getelementptr inbounds [5 x i32], [5 x i32]* %state238, i32 0, i32 1
  %131 = load i32, i32* %arrayidx239, align 4
  store i32 %131, i32* %B, align 4
  %132 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state240 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %132, i32 0, i32 1
  %arrayidx241 = getelementptr inbounds [5 x i32], [5 x i32]* %state240, i32 0, i32 2
  %133 = load i32, i32* %arrayidx241, align 4
  store i32 %133, i32* %C, align 4
  %134 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state242 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %134, i32 0, i32 1
  %arrayidx243 = getelementptr inbounds [5 x i32], [5 x i32]* %state242, i32 0, i32 3
  %135 = load i32, i32* %arrayidx243, align 4
  store i32 %135, i32* %D, align 4
  %136 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state244 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %136, i32 0, i32 1
  %arrayidx245 = getelementptr inbounds [5 x i32], [5 x i32]* %state244, i32 0, i32 4
  %137 = load i32, i32* %arrayidx245, align 4
  store i32 %137, i32* %E, align 4
  %138 = load i32, i32* %A, align 4
  %shl246 = shl i32 %138, 5
  %139 = load i32, i32* %A, align 4
  %shr = lshr i32 %139, 27
  %or247 = or i32 %shl246, %shr
  %140 = load i32, i32* %D, align 4
  %141 = load i32, i32* %B, align 4
  %142 = load i32, i32* %C, align 4
  %143 = load i32, i32* %D, align 4
  %xor = xor i32 %142, %143
  %and = and i32 %141, %xor
  %xor248 = xor i32 %140, %and
  %add = add i32 %or247, %xor248
  %add249 = add i32 %add, 1518500249
  %arrayidx250 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %144 = load i32, i32* %arrayidx250, align 16
  %add251 = add i32 %add249, %144
  %145 = load i32, i32* %E, align 4
  %add252 = add i32 %145, %add251
  store i32 %add252, i32* %E, align 4
  %146 = load i32, i32* %B, align 4
  %shl253 = shl i32 %146, 30
  %147 = load i32, i32* %B, align 4
  %shr254 = lshr i32 %147, 2
  %or255 = or i32 %shl253, %shr254
  store i32 %or255, i32* %B, align 4
  %148 = load i32, i32* %E, align 4
  %shl256 = shl i32 %148, 5
  %149 = load i32, i32* %E, align 4
  %shr257 = lshr i32 %149, 27
  %or258 = or i32 %shl256, %shr257
  %150 = load i32, i32* %C, align 4
  %151 = load i32, i32* %A, align 4
  %152 = load i32, i32* %B, align 4
  %153 = load i32, i32* %C, align 4
  %xor259 = xor i32 %152, %153
  %and260 = and i32 %151, %xor259
  %xor261 = xor i32 %150, %and260
  %add262 = add i32 %or258, %xor261
  %add263 = add i32 %add262, 1518500249
  %arrayidx264 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %154 = load i32, i32* %arrayidx264, align 4
  %add265 = add i32 %add263, %154
  %155 = load i32, i32* %D, align 4
  %add266 = add i32 %155, %add265
  store i32 %add266, i32* %D, align 4
  %156 = load i32, i32* %A, align 4
  %shl267 = shl i32 %156, 30
  %157 = load i32, i32* %A, align 4
  %shr268 = lshr i32 %157, 2
  %or269 = or i32 %shl267, %shr268
  store i32 %or269, i32* %A, align 4
  %158 = load i32, i32* %D, align 4
  %shl270 = shl i32 %158, 5
  %159 = load i32, i32* %D, align 4
  %shr271 = lshr i32 %159, 27
  %or272 = or i32 %shl270, %shr271
  %160 = load i32, i32* %B, align 4
  %161 = load i32, i32* %E, align 4
  %162 = load i32, i32* %A, align 4
  %163 = load i32, i32* %B, align 4
  %xor273 = xor i32 %162, %163
  %and274 = and i32 %161, %xor273
  %xor275 = xor i32 %160, %and274
  %add276 = add i32 %or272, %xor275
  %add277 = add i32 %add276, 1518500249
  %arrayidx278 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %164 = load i32, i32* %arrayidx278, align 8
  %add279 = add i32 %add277, %164
  %165 = load i32, i32* %C, align 4
  %add280 = add i32 %165, %add279
  store i32 %add280, i32* %C, align 4
  %166 = load i32, i32* %E, align 4
  %shl281 = shl i32 %166, 30
  %167 = load i32, i32* %E, align 4
  %shr282 = lshr i32 %167, 2
  %or283 = or i32 %shl281, %shr282
  store i32 %or283, i32* %E, align 4
  %168 = load i32, i32* %C, align 4
  %shl284 = shl i32 %168, 5
  %169 = load i32, i32* %C, align 4
  %shr285 = lshr i32 %169, 27
  %or286 = or i32 %shl284, %shr285
  %170 = load i32, i32* %A, align 4
  %171 = load i32, i32* %D, align 4
  %172 = load i32, i32* %E, align 4
  %173 = load i32, i32* %A, align 4
  %xor287 = xor i32 %172, %173
  %and288 = and i32 %171, %xor287
  %xor289 = xor i32 %170, %and288
  %add290 = add i32 %or286, %xor289
  %add291 = add i32 %add290, 1518500249
  %arrayidx292 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %174 = load i32, i32* %arrayidx292, align 4
  %add293 = add i32 %add291, %174
  %175 = load i32, i32* %B, align 4
  %add294 = add i32 %175, %add293
  store i32 %add294, i32* %B, align 4
  %176 = load i32, i32* %D, align 4
  %shl295 = shl i32 %176, 30
  %177 = load i32, i32* %D, align 4
  %shr296 = lshr i32 %177, 2
  %or297 = or i32 %shl295, %shr296
  store i32 %or297, i32* %D, align 4
  %178 = load i32, i32* %B, align 4
  %shl298 = shl i32 %178, 5
  %179 = load i32, i32* %B, align 4
  %shr299 = lshr i32 %179, 27
  %or300 = or i32 %shl298, %shr299
  %180 = load i32, i32* %E, align 4
  %181 = load i32, i32* %C, align 4
  %182 = load i32, i32* %D, align 4
  %183 = load i32, i32* %E, align 4
  %xor301 = xor i32 %182, %183
  %and302 = and i32 %181, %xor301
  %xor303 = xor i32 %180, %and302
  %add304 = add i32 %or300, %xor303
  %add305 = add i32 %add304, 1518500249
  %arrayidx306 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %184 = load i32, i32* %arrayidx306, align 16
  %add307 = add i32 %add305, %184
  %185 = load i32, i32* %A, align 4
  %add308 = add i32 %185, %add307
  store i32 %add308, i32* %A, align 4
  %186 = load i32, i32* %C, align 4
  %shl309 = shl i32 %186, 30
  %187 = load i32, i32* %C, align 4
  %shr310 = lshr i32 %187, 2
  %or311 = or i32 %shl309, %shr310
  store i32 %or311, i32* %C, align 4
  %188 = load i32, i32* %A, align 4
  %shl312 = shl i32 %188, 5
  %189 = load i32, i32* %A, align 4
  %shr313 = lshr i32 %189, 27
  %or314 = or i32 %shl312, %shr313
  %190 = load i32, i32* %D, align 4
  %191 = load i32, i32* %B, align 4
  %192 = load i32, i32* %C, align 4
  %193 = load i32, i32* %D, align 4
  %xor315 = xor i32 %192, %193
  %and316 = and i32 %191, %xor315
  %xor317 = xor i32 %190, %and316
  %add318 = add i32 %or314, %xor317
  %add319 = add i32 %add318, 1518500249
  %arrayidx320 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %194 = load i32, i32* %arrayidx320, align 4
  %add321 = add i32 %add319, %194
  %195 = load i32, i32* %E, align 4
  %add322 = add i32 %195, %add321
  store i32 %add322, i32* %E, align 4
  %196 = load i32, i32* %B, align 4
  %shl323 = shl i32 %196, 30
  %197 = load i32, i32* %B, align 4
  %shr324 = lshr i32 %197, 2
  %or325 = or i32 %shl323, %shr324
  store i32 %or325, i32* %B, align 4
  %198 = load i32, i32* %E, align 4
  %shl326 = shl i32 %198, 5
  %199 = load i32, i32* %E, align 4
  %shr327 = lshr i32 %199, 27
  %or328 = or i32 %shl326, %shr327
  %200 = load i32, i32* %C, align 4
  %201 = load i32, i32* %A, align 4
  %202 = load i32, i32* %B, align 4
  %203 = load i32, i32* %C, align 4
  %xor329 = xor i32 %202, %203
  %and330 = and i32 %201, %xor329
  %xor331 = xor i32 %200, %and330
  %add332 = add i32 %or328, %xor331
  %add333 = add i32 %add332, 1518500249
  %arrayidx334 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %204 = load i32, i32* %arrayidx334, align 8
  %add335 = add i32 %add333, %204
  %205 = load i32, i32* %D, align 4
  %add336 = add i32 %205, %add335
  store i32 %add336, i32* %D, align 4
  %206 = load i32, i32* %A, align 4
  %shl337 = shl i32 %206, 30
  %207 = load i32, i32* %A, align 4
  %shr338 = lshr i32 %207, 2
  %or339 = or i32 %shl337, %shr338
  store i32 %or339, i32* %A, align 4
  %208 = load i32, i32* %D, align 4
  %shl340 = shl i32 %208, 5
  %209 = load i32, i32* %D, align 4
  %shr341 = lshr i32 %209, 27
  %or342 = or i32 %shl340, %shr341
  %210 = load i32, i32* %B, align 4
  %211 = load i32, i32* %E, align 4
  %212 = load i32, i32* %A, align 4
  %213 = load i32, i32* %B, align 4
  %xor343 = xor i32 %212, %213
  %and344 = and i32 %211, %xor343
  %xor345 = xor i32 %210, %and344
  %add346 = add i32 %or342, %xor345
  %add347 = add i32 %add346, 1518500249
  %arrayidx348 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %214 = load i32, i32* %arrayidx348, align 4
  %add349 = add i32 %add347, %214
  %215 = load i32, i32* %C, align 4
  %add350 = add i32 %215, %add349
  store i32 %add350, i32* %C, align 4
  %216 = load i32, i32* %E, align 4
  %shl351 = shl i32 %216, 30
  %217 = load i32, i32* %E, align 4
  %shr352 = lshr i32 %217, 2
  %or353 = or i32 %shl351, %shr352
  store i32 %or353, i32* %E, align 4
  %218 = load i32, i32* %C, align 4
  %shl354 = shl i32 %218, 5
  %219 = load i32, i32* %C, align 4
  %shr355 = lshr i32 %219, 27
  %or356 = or i32 %shl354, %shr355
  %220 = load i32, i32* %A, align 4
  %221 = load i32, i32* %D, align 4
  %222 = load i32, i32* %E, align 4
  %223 = load i32, i32* %A, align 4
  %xor357 = xor i32 %222, %223
  %and358 = and i32 %221, %xor357
  %xor359 = xor i32 %220, %and358
  %add360 = add i32 %or356, %xor359
  %add361 = add i32 %add360, 1518500249
  %arrayidx362 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %224 = load i32, i32* %arrayidx362, align 16
  %add363 = add i32 %add361, %224
  %225 = load i32, i32* %B, align 4
  %add364 = add i32 %225, %add363
  store i32 %add364, i32* %B, align 4
  %226 = load i32, i32* %D, align 4
  %shl365 = shl i32 %226, 30
  %227 = load i32, i32* %D, align 4
  %shr366 = lshr i32 %227, 2
  %or367 = or i32 %shl365, %shr366
  store i32 %or367, i32* %D, align 4
  %228 = load i32, i32* %B, align 4
  %shl368 = shl i32 %228, 5
  %229 = load i32, i32* %B, align 4
  %shr369 = lshr i32 %229, 27
  %or370 = or i32 %shl368, %shr369
  %230 = load i32, i32* %E, align 4
  %231 = load i32, i32* %C, align 4
  %232 = load i32, i32* %D, align 4
  %233 = load i32, i32* %E, align 4
  %xor371 = xor i32 %232, %233
  %and372 = and i32 %231, %xor371
  %xor373 = xor i32 %230, %and372
  %add374 = add i32 %or370, %xor373
  %add375 = add i32 %add374, 1518500249
  %arrayidx376 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %234 = load i32, i32* %arrayidx376, align 4
  %add377 = add i32 %add375, %234
  %235 = load i32, i32* %A, align 4
  %add378 = add i32 %235, %add377
  store i32 %add378, i32* %A, align 4
  %236 = load i32, i32* %C, align 4
  %shl379 = shl i32 %236, 30
  %237 = load i32, i32* %C, align 4
  %shr380 = lshr i32 %237, 2
  %or381 = or i32 %shl379, %shr380
  store i32 %or381, i32* %C, align 4
  %238 = load i32, i32* %A, align 4
  %shl382 = shl i32 %238, 5
  %239 = load i32, i32* %A, align 4
  %shr383 = lshr i32 %239, 27
  %or384 = or i32 %shl382, %shr383
  %240 = load i32, i32* %D, align 4
  %241 = load i32, i32* %B, align 4
  %242 = load i32, i32* %C, align 4
  %243 = load i32, i32* %D, align 4
  %xor385 = xor i32 %242, %243
  %and386 = and i32 %241, %xor385
  %xor387 = xor i32 %240, %and386
  %add388 = add i32 %or384, %xor387
  %add389 = add i32 %add388, 1518500249
  %arrayidx390 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %244 = load i32, i32* %arrayidx390, align 8
  %add391 = add i32 %add389, %244
  %245 = load i32, i32* %E, align 4
  %add392 = add i32 %245, %add391
  store i32 %add392, i32* %E, align 4
  %246 = load i32, i32* %B, align 4
  %shl393 = shl i32 %246, 30
  %247 = load i32, i32* %B, align 4
  %shr394 = lshr i32 %247, 2
  %or395 = or i32 %shl393, %shr394
  store i32 %or395, i32* %B, align 4
  %248 = load i32, i32* %E, align 4
  %shl396 = shl i32 %248, 5
  %249 = load i32, i32* %E, align 4
  %shr397 = lshr i32 %249, 27
  %or398 = or i32 %shl396, %shr397
  %250 = load i32, i32* %C, align 4
  %251 = load i32, i32* %A, align 4
  %252 = load i32, i32* %B, align 4
  %253 = load i32, i32* %C, align 4
  %xor399 = xor i32 %252, %253
  %and400 = and i32 %251, %xor399
  %xor401 = xor i32 %250, %and400
  %add402 = add i32 %or398, %xor401
  %add403 = add i32 %add402, 1518500249
  %arrayidx404 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %254 = load i32, i32* %arrayidx404, align 4
  %add405 = add i32 %add403, %254
  %255 = load i32, i32* %D, align 4
  %add406 = add i32 %255, %add405
  store i32 %add406, i32* %D, align 4
  %256 = load i32, i32* %A, align 4
  %shl407 = shl i32 %256, 30
  %257 = load i32, i32* %A, align 4
  %shr408 = lshr i32 %257, 2
  %or409 = or i32 %shl407, %shr408
  store i32 %or409, i32* %A, align 4
  %258 = load i32, i32* %D, align 4
  %shl410 = shl i32 %258, 5
  %259 = load i32, i32* %D, align 4
  %shr411 = lshr i32 %259, 27
  %or412 = or i32 %shl410, %shr411
  %260 = load i32, i32* %B, align 4
  %261 = load i32, i32* %E, align 4
  %262 = load i32, i32* %A, align 4
  %263 = load i32, i32* %B, align 4
  %xor413 = xor i32 %262, %263
  %and414 = and i32 %261, %xor413
  %xor415 = xor i32 %260, %and414
  %add416 = add i32 %or412, %xor415
  %add417 = add i32 %add416, 1518500249
  %arrayidx418 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %264 = load i32, i32* %arrayidx418, align 16
  %add419 = add i32 %add417, %264
  %265 = load i32, i32* %C, align 4
  %add420 = add i32 %265, %add419
  store i32 %add420, i32* %C, align 4
  %266 = load i32, i32* %E, align 4
  %shl421 = shl i32 %266, 30
  %267 = load i32, i32* %E, align 4
  %shr422 = lshr i32 %267, 2
  %or423 = or i32 %shl421, %shr422
  store i32 %or423, i32* %E, align 4
  %268 = load i32, i32* %C, align 4
  %shl424 = shl i32 %268, 5
  %269 = load i32, i32* %C, align 4
  %shr425 = lshr i32 %269, 27
  %or426 = or i32 %shl424, %shr425
  %270 = load i32, i32* %A, align 4
  %271 = load i32, i32* %D, align 4
  %272 = load i32, i32* %E, align 4
  %273 = load i32, i32* %A, align 4
  %xor427 = xor i32 %272, %273
  %and428 = and i32 %271, %xor427
  %xor429 = xor i32 %270, %and428
  %add430 = add i32 %or426, %xor429
  %add431 = add i32 %add430, 1518500249
  %arrayidx432 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %274 = load i32, i32* %arrayidx432, align 4
  %add433 = add i32 %add431, %274
  %275 = load i32, i32* %B, align 4
  %add434 = add i32 %275, %add433
  store i32 %add434, i32* %B, align 4
  %276 = load i32, i32* %D, align 4
  %shl435 = shl i32 %276, 30
  %277 = load i32, i32* %D, align 4
  %shr436 = lshr i32 %277, 2
  %or437 = or i32 %shl435, %shr436
  store i32 %or437, i32* %D, align 4
  %278 = load i32, i32* %B, align 4
  %shl438 = shl i32 %278, 5
  %279 = load i32, i32* %B, align 4
  %shr439 = lshr i32 %279, 27
  %or440 = or i32 %shl438, %shr439
  %280 = load i32, i32* %E, align 4
  %281 = load i32, i32* %C, align 4
  %282 = load i32, i32* %D, align 4
  %283 = load i32, i32* %E, align 4
  %xor441 = xor i32 %282, %283
  %and442 = and i32 %281, %xor441
  %xor443 = xor i32 %280, %and442
  %add444 = add i32 %or440, %xor443
  %add445 = add i32 %add444, 1518500249
  %arrayidx446 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %284 = load i32, i32* %arrayidx446, align 8
  %add447 = add i32 %add445, %284
  %285 = load i32, i32* %A, align 4
  %add448 = add i32 %285, %add447
  store i32 %add448, i32* %A, align 4
  %286 = load i32, i32* %C, align 4
  %shl449 = shl i32 %286, 30
  %287 = load i32, i32* %C, align 4
  %shr450 = lshr i32 %287, 2
  %or451 = or i32 %shl449, %shr450
  store i32 %or451, i32* %C, align 4
  %288 = load i32, i32* %A, align 4
  %shl452 = shl i32 %288, 5
  %289 = load i32, i32* %A, align 4
  %shr453 = lshr i32 %289, 27
  %or454 = or i32 %shl452, %shr453
  %290 = load i32, i32* %D, align 4
  %291 = load i32, i32* %B, align 4
  %292 = load i32, i32* %C, align 4
  %293 = load i32, i32* %D, align 4
  %xor455 = xor i32 %292, %293
  %and456 = and i32 %291, %xor455
  %xor457 = xor i32 %290, %and456
  %add458 = add i32 %or454, %xor457
  %add459 = add i32 %add458, 1518500249
  %arrayidx460 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %294 = load i32, i32* %arrayidx460, align 4
  %add461 = add i32 %add459, %294
  %295 = load i32, i32* %E, align 4
  %add462 = add i32 %295, %add461
  store i32 %add462, i32* %E, align 4
  %296 = load i32, i32* %B, align 4
  %shl463 = shl i32 %296, 30
  %297 = load i32, i32* %B, align 4
  %shr464 = lshr i32 %297, 2
  %or465 = or i32 %shl463, %shr464
  store i32 %or465, i32* %B, align 4
  %298 = load i32, i32* %E, align 4
  %shl466 = shl i32 %298, 5
  %299 = load i32, i32* %E, align 4
  %shr467 = lshr i32 %299, 27
  %or468 = or i32 %shl466, %shr467
  %300 = load i32, i32* %C, align 4
  %301 = load i32, i32* %A, align 4
  %302 = load i32, i32* %B, align 4
  %303 = load i32, i32* %C, align 4
  %xor469 = xor i32 %302, %303
  %and470 = and i32 %301, %xor469
  %xor471 = xor i32 %300, %and470
  %add472 = add i32 %or468, %xor471
  %add473 = add i32 %add472, 1518500249
  %arrayidx474 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %304 = load i32, i32* %arrayidx474, align 4
  %arrayidx475 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %305 = load i32, i32* %arrayidx475, align 16
  %xor476 = xor i32 %304, %305
  %arrayidx477 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %306 = load i32, i32* %arrayidx477, align 8
  %xor478 = xor i32 %xor476, %306
  %arrayidx479 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %307 = load i32, i32* %arrayidx479, align 16
  %xor480 = xor i32 %xor478, %307
  store i32 %xor480, i32* %temp, align 4
  %308 = load i32, i32* %temp, align 4
  %shl481 = shl i32 %308, 1
  %309 = load i32, i32* %temp, align 4
  %shr482 = lshr i32 %309, 31
  %or483 = or i32 %shl481, %shr482
  %arrayidx484 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  store i32 %or483, i32* %arrayidx484, align 16
  %add485 = add i32 %add473, %or483
  %310 = load i32, i32* %D, align 4
  %add486 = add i32 %310, %add485
  store i32 %add486, i32* %D, align 4
  %311 = load i32, i32* %A, align 4
  %shl487 = shl i32 %311, 30
  %312 = load i32, i32* %A, align 4
  %shr488 = lshr i32 %312, 2
  %or489 = or i32 %shl487, %shr488
  store i32 %or489, i32* %A, align 4
  %313 = load i32, i32* %D, align 4
  %shl490 = shl i32 %313, 5
  %314 = load i32, i32* %D, align 4
  %shr491 = lshr i32 %314, 27
  %or492 = or i32 %shl490, %shr491
  %315 = load i32, i32* %B, align 4
  %316 = load i32, i32* %E, align 4
  %317 = load i32, i32* %A, align 4
  %318 = load i32, i32* %B, align 4
  %xor493 = xor i32 %317, %318
  %and494 = and i32 %316, %xor493
  %xor495 = xor i32 %315, %and494
  %add496 = add i32 %or492, %xor495
  %add497 = add i32 %add496, 1518500249
  %arrayidx498 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %319 = load i32, i32* %arrayidx498, align 8
  %arrayidx499 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %320 = load i32, i32* %arrayidx499, align 4
  %xor500 = xor i32 %319, %320
  %arrayidx501 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %321 = load i32, i32* %arrayidx501, align 4
  %xor502 = xor i32 %xor500, %321
  %arrayidx503 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %322 = load i32, i32* %arrayidx503, align 4
  %xor504 = xor i32 %xor502, %322
  store i32 %xor504, i32* %temp, align 4
  %323 = load i32, i32* %temp, align 4
  %shl505 = shl i32 %323, 1
  %324 = load i32, i32* %temp, align 4
  %shr506 = lshr i32 %324, 31
  %or507 = or i32 %shl505, %shr506
  %arrayidx508 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  store i32 %or507, i32* %arrayidx508, align 4
  %add509 = add i32 %add497, %or507
  %325 = load i32, i32* %C, align 4
  %add510 = add i32 %325, %add509
  store i32 %add510, i32* %C, align 4
  %326 = load i32, i32* %E, align 4
  %shl511 = shl i32 %326, 30
  %327 = load i32, i32* %E, align 4
  %shr512 = lshr i32 %327, 2
  %or513 = or i32 %shl511, %shr512
  store i32 %or513, i32* %E, align 4
  %328 = load i32, i32* %C, align 4
  %shl514 = shl i32 %328, 5
  %329 = load i32, i32* %C, align 4
  %shr515 = lshr i32 %329, 27
  %or516 = or i32 %shl514, %shr515
  %330 = load i32, i32* %A, align 4
  %331 = load i32, i32* %D, align 4
  %332 = load i32, i32* %E, align 4
  %333 = load i32, i32* %A, align 4
  %xor517 = xor i32 %332, %333
  %and518 = and i32 %331, %xor517
  %xor519 = xor i32 %330, %and518
  %add520 = add i32 %or516, %xor519
  %add521 = add i32 %add520, 1518500249
  %arrayidx522 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %334 = load i32, i32* %arrayidx522, align 4
  %arrayidx523 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %335 = load i32, i32* %arrayidx523, align 8
  %xor524 = xor i32 %334, %335
  %arrayidx525 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %336 = load i32, i32* %arrayidx525, align 16
  %xor526 = xor i32 %xor524, %336
  %arrayidx527 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %337 = load i32, i32* %arrayidx527, align 8
  %xor528 = xor i32 %xor526, %337
  store i32 %xor528, i32* %temp, align 4
  %338 = load i32, i32* %temp, align 4
  %shl529 = shl i32 %338, 1
  %339 = load i32, i32* %temp, align 4
  %shr530 = lshr i32 %339, 31
  %or531 = or i32 %shl529, %shr530
  %arrayidx532 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  store i32 %or531, i32* %arrayidx532, align 8
  %add533 = add i32 %add521, %or531
  %340 = load i32, i32* %B, align 4
  %add534 = add i32 %340, %add533
  store i32 %add534, i32* %B, align 4
  %341 = load i32, i32* %D, align 4
  %shl535 = shl i32 %341, 30
  %342 = load i32, i32* %D, align 4
  %shr536 = lshr i32 %342, 2
  %or537 = or i32 %shl535, %shr536
  store i32 %or537, i32* %D, align 4
  %343 = load i32, i32* %B, align 4
  %shl538 = shl i32 %343, 5
  %344 = load i32, i32* %B, align 4
  %shr539 = lshr i32 %344, 27
  %or540 = or i32 %shl538, %shr539
  %345 = load i32, i32* %E, align 4
  %346 = load i32, i32* %C, align 4
  %347 = load i32, i32* %D, align 4
  %348 = load i32, i32* %E, align 4
  %xor541 = xor i32 %347, %348
  %and542 = and i32 %346, %xor541
  %xor543 = xor i32 %345, %and542
  %add544 = add i32 %or540, %xor543
  %add545 = add i32 %add544, 1518500249
  %arrayidx546 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %349 = load i32, i32* %arrayidx546, align 16
  %arrayidx547 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %350 = load i32, i32* %arrayidx547, align 4
  %xor548 = xor i32 %349, %350
  %arrayidx549 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %351 = load i32, i32* %arrayidx549, align 4
  %xor550 = xor i32 %xor548, %351
  %arrayidx551 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %352 = load i32, i32* %arrayidx551, align 4
  %xor552 = xor i32 %xor550, %352
  store i32 %xor552, i32* %temp, align 4
  %353 = load i32, i32* %temp, align 4
  %shl553 = shl i32 %353, 1
  %354 = load i32, i32* %temp, align 4
  %shr554 = lshr i32 %354, 31
  %or555 = or i32 %shl553, %shr554
  %arrayidx556 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  store i32 %or555, i32* %arrayidx556, align 4
  %add557 = add i32 %add545, %or555
  %355 = load i32, i32* %A, align 4
  %add558 = add i32 %355, %add557
  store i32 %add558, i32* %A, align 4
  %356 = load i32, i32* %C, align 4
  %shl559 = shl i32 %356, 30
  %357 = load i32, i32* %C, align 4
  %shr560 = lshr i32 %357, 2
  %or561 = or i32 %shl559, %shr560
  store i32 %or561, i32* %C, align 4
  %358 = load i32, i32* %A, align 4
  %shl562 = shl i32 %358, 5
  %359 = load i32, i32* %A, align 4
  %shr563 = lshr i32 %359, 27
  %or564 = or i32 %shl562, %shr563
  %360 = load i32, i32* %B, align 4
  %361 = load i32, i32* %C, align 4
  %xor565 = xor i32 %360, %361
  %362 = load i32, i32* %D, align 4
  %xor566 = xor i32 %xor565, %362
  %add567 = add i32 %or564, %xor566
  %add568 = add i32 %add567, 1859775393
  %arrayidx569 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %363 = load i32, i32* %arrayidx569, align 4
  %arrayidx570 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %364 = load i32, i32* %arrayidx570, align 16
  %xor571 = xor i32 %363, %364
  %arrayidx572 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %365 = load i32, i32* %arrayidx572, align 8
  %xor573 = xor i32 %xor571, %365
  %arrayidx574 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %366 = load i32, i32* %arrayidx574, align 16
  %xor575 = xor i32 %xor573, %366
  store i32 %xor575, i32* %temp, align 4
  %367 = load i32, i32* %temp, align 4
  %shl576 = shl i32 %367, 1
  %368 = load i32, i32* %temp, align 4
  %shr577 = lshr i32 %368, 31
  %or578 = or i32 %shl576, %shr577
  %arrayidx579 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  store i32 %or578, i32* %arrayidx579, align 16
  %add580 = add i32 %add568, %or578
  %369 = load i32, i32* %E, align 4
  %add581 = add i32 %369, %add580
  store i32 %add581, i32* %E, align 4
  %370 = load i32, i32* %B, align 4
  %shl582 = shl i32 %370, 30
  %371 = load i32, i32* %B, align 4
  %shr583 = lshr i32 %371, 2
  %or584 = or i32 %shl582, %shr583
  store i32 %or584, i32* %B, align 4
  %372 = load i32, i32* %E, align 4
  %shl585 = shl i32 %372, 5
  %373 = load i32, i32* %E, align 4
  %shr586 = lshr i32 %373, 27
  %or587 = or i32 %shl585, %shr586
  %374 = load i32, i32* %A, align 4
  %375 = load i32, i32* %B, align 4
  %xor588 = xor i32 %374, %375
  %376 = load i32, i32* %C, align 4
  %xor589 = xor i32 %xor588, %376
  %add590 = add i32 %or587, %xor589
  %add591 = add i32 %add590, 1859775393
  %arrayidx592 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %377 = load i32, i32* %arrayidx592, align 8
  %arrayidx593 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %378 = load i32, i32* %arrayidx593, align 4
  %xor594 = xor i32 %377, %378
  %arrayidx595 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %379 = load i32, i32* %arrayidx595, align 4
  %xor596 = xor i32 %xor594, %379
  %arrayidx597 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %380 = load i32, i32* %arrayidx597, align 4
  %xor598 = xor i32 %xor596, %380
  store i32 %xor598, i32* %temp, align 4
  %381 = load i32, i32* %temp, align 4
  %shl599 = shl i32 %381, 1
  %382 = load i32, i32* %temp, align 4
  %shr600 = lshr i32 %382, 31
  %or601 = or i32 %shl599, %shr600
  %arrayidx602 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  store i32 %or601, i32* %arrayidx602, align 4
  %add603 = add i32 %add591, %or601
  %383 = load i32, i32* %D, align 4
  %add604 = add i32 %383, %add603
  store i32 %add604, i32* %D, align 4
  %384 = load i32, i32* %A, align 4
  %shl605 = shl i32 %384, 30
  %385 = load i32, i32* %A, align 4
  %shr606 = lshr i32 %385, 2
  %or607 = or i32 %shl605, %shr606
  store i32 %or607, i32* %A, align 4
  %386 = load i32, i32* %D, align 4
  %shl608 = shl i32 %386, 5
  %387 = load i32, i32* %D, align 4
  %shr609 = lshr i32 %387, 27
  %or610 = or i32 %shl608, %shr609
  %388 = load i32, i32* %E, align 4
  %389 = load i32, i32* %A, align 4
  %xor611 = xor i32 %388, %389
  %390 = load i32, i32* %B, align 4
  %xor612 = xor i32 %xor611, %390
  %add613 = add i32 %or610, %xor612
  %add614 = add i32 %add613, 1859775393
  %arrayidx615 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %391 = load i32, i32* %arrayidx615, align 4
  %arrayidx616 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %392 = load i32, i32* %arrayidx616, align 8
  %xor617 = xor i32 %391, %392
  %arrayidx618 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %393 = load i32, i32* %arrayidx618, align 16
  %xor619 = xor i32 %xor617, %393
  %arrayidx620 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %394 = load i32, i32* %arrayidx620, align 8
  %xor621 = xor i32 %xor619, %394
  store i32 %xor621, i32* %temp, align 4
  %395 = load i32, i32* %temp, align 4
  %shl622 = shl i32 %395, 1
  %396 = load i32, i32* %temp, align 4
  %shr623 = lshr i32 %396, 31
  %or624 = or i32 %shl622, %shr623
  %arrayidx625 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  store i32 %or624, i32* %arrayidx625, align 8
  %add626 = add i32 %add614, %or624
  %397 = load i32, i32* %C, align 4
  %add627 = add i32 %397, %add626
  store i32 %add627, i32* %C, align 4
  %398 = load i32, i32* %E, align 4
  %shl628 = shl i32 %398, 30
  %399 = load i32, i32* %E, align 4
  %shr629 = lshr i32 %399, 2
  %or630 = or i32 %shl628, %shr629
  store i32 %or630, i32* %E, align 4
  %400 = load i32, i32* %C, align 4
  %shl631 = shl i32 %400, 5
  %401 = load i32, i32* %C, align 4
  %shr632 = lshr i32 %401, 27
  %or633 = or i32 %shl631, %shr632
  %402 = load i32, i32* %D, align 4
  %403 = load i32, i32* %E, align 4
  %xor634 = xor i32 %402, %403
  %404 = load i32, i32* %A, align 4
  %xor635 = xor i32 %xor634, %404
  %add636 = add i32 %or633, %xor635
  %add637 = add i32 %add636, 1859775393
  %arrayidx638 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %405 = load i32, i32* %arrayidx638, align 16
  %arrayidx639 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %406 = load i32, i32* %arrayidx639, align 4
  %xor640 = xor i32 %405, %406
  %arrayidx641 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %407 = load i32, i32* %arrayidx641, align 4
  %xor642 = xor i32 %xor640, %407
  %arrayidx643 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %408 = load i32, i32* %arrayidx643, align 4
  %xor644 = xor i32 %xor642, %408
  store i32 %xor644, i32* %temp, align 4
  %409 = load i32, i32* %temp, align 4
  %shl645 = shl i32 %409, 1
  %410 = load i32, i32* %temp, align 4
  %shr646 = lshr i32 %410, 31
  %or647 = or i32 %shl645, %shr646
  %arrayidx648 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  store i32 %or647, i32* %arrayidx648, align 4
  %add649 = add i32 %add637, %or647
  %411 = load i32, i32* %B, align 4
  %add650 = add i32 %411, %add649
  store i32 %add650, i32* %B, align 4
  %412 = load i32, i32* %D, align 4
  %shl651 = shl i32 %412, 30
  %413 = load i32, i32* %D, align 4
  %shr652 = lshr i32 %413, 2
  %or653 = or i32 %shl651, %shr652
  store i32 %or653, i32* %D, align 4
  %414 = load i32, i32* %B, align 4
  %shl654 = shl i32 %414, 5
  %415 = load i32, i32* %B, align 4
  %shr655 = lshr i32 %415, 27
  %or656 = or i32 %shl654, %shr655
  %416 = load i32, i32* %C, align 4
  %417 = load i32, i32* %D, align 4
  %xor657 = xor i32 %416, %417
  %418 = load i32, i32* %E, align 4
  %xor658 = xor i32 %xor657, %418
  %add659 = add i32 %or656, %xor658
  %add660 = add i32 %add659, 1859775393
  %arrayidx661 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %419 = load i32, i32* %arrayidx661, align 4
  %arrayidx662 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %420 = load i32, i32* %arrayidx662, align 16
  %xor663 = xor i32 %419, %420
  %arrayidx664 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %421 = load i32, i32* %arrayidx664, align 8
  %xor665 = xor i32 %xor663, %421
  %arrayidx666 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %422 = load i32, i32* %arrayidx666, align 16
  %xor667 = xor i32 %xor665, %422
  store i32 %xor667, i32* %temp, align 4
  %423 = load i32, i32* %temp, align 4
  %shl668 = shl i32 %423, 1
  %424 = load i32, i32* %temp, align 4
  %shr669 = lshr i32 %424, 31
  %or670 = or i32 %shl668, %shr669
  %arrayidx671 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  store i32 %or670, i32* %arrayidx671, align 16
  %add672 = add i32 %add660, %or670
  %425 = load i32, i32* %A, align 4
  %add673 = add i32 %425, %add672
  store i32 %add673, i32* %A, align 4
  %426 = load i32, i32* %C, align 4
  %shl674 = shl i32 %426, 30
  %427 = load i32, i32* %C, align 4
  %shr675 = lshr i32 %427, 2
  %or676 = or i32 %shl674, %shr675
  store i32 %or676, i32* %C, align 4
  %428 = load i32, i32* %A, align 4
  %shl677 = shl i32 %428, 5
  %429 = load i32, i32* %A, align 4
  %shr678 = lshr i32 %429, 27
  %or679 = or i32 %shl677, %shr678
  %430 = load i32, i32* %B, align 4
  %431 = load i32, i32* %C, align 4
  %xor680 = xor i32 %430, %431
  %432 = load i32, i32* %D, align 4
  %xor681 = xor i32 %xor680, %432
  %add682 = add i32 %or679, %xor681
  %add683 = add i32 %add682, 1859775393
  %arrayidx684 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %433 = load i32, i32* %arrayidx684, align 8
  %arrayidx685 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %434 = load i32, i32* %arrayidx685, align 4
  %xor686 = xor i32 %433, %434
  %arrayidx687 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %435 = load i32, i32* %arrayidx687, align 4
  %xor688 = xor i32 %xor686, %435
  %arrayidx689 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %436 = load i32, i32* %arrayidx689, align 4
  %xor690 = xor i32 %xor688, %436
  store i32 %xor690, i32* %temp, align 4
  %437 = load i32, i32* %temp, align 4
  %shl691 = shl i32 %437, 1
  %438 = load i32, i32* %temp, align 4
  %shr692 = lshr i32 %438, 31
  %or693 = or i32 %shl691, %shr692
  %arrayidx694 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  store i32 %or693, i32* %arrayidx694, align 4
  %add695 = add i32 %add683, %or693
  %439 = load i32, i32* %E, align 4
  %add696 = add i32 %439, %add695
  store i32 %add696, i32* %E, align 4
  %440 = load i32, i32* %B, align 4
  %shl697 = shl i32 %440, 30
  %441 = load i32, i32* %B, align 4
  %shr698 = lshr i32 %441, 2
  %or699 = or i32 %shl697, %shr698
  store i32 %or699, i32* %B, align 4
  %442 = load i32, i32* %E, align 4
  %shl700 = shl i32 %442, 5
  %443 = load i32, i32* %E, align 4
  %shr701 = lshr i32 %443, 27
  %or702 = or i32 %shl700, %shr701
  %444 = load i32, i32* %A, align 4
  %445 = load i32, i32* %B, align 4
  %xor703 = xor i32 %444, %445
  %446 = load i32, i32* %C, align 4
  %xor704 = xor i32 %xor703, %446
  %add705 = add i32 %or702, %xor704
  %add706 = add i32 %add705, 1859775393
  %arrayidx707 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %447 = load i32, i32* %arrayidx707, align 4
  %arrayidx708 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %448 = load i32, i32* %arrayidx708, align 8
  %xor709 = xor i32 %447, %448
  %arrayidx710 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %449 = load i32, i32* %arrayidx710, align 16
  %xor711 = xor i32 %xor709, %449
  %arrayidx712 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %450 = load i32, i32* %arrayidx712, align 8
  %xor713 = xor i32 %xor711, %450
  store i32 %xor713, i32* %temp, align 4
  %451 = load i32, i32* %temp, align 4
  %shl714 = shl i32 %451, 1
  %452 = load i32, i32* %temp, align 4
  %shr715 = lshr i32 %452, 31
  %or716 = or i32 %shl714, %shr715
  %arrayidx717 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  store i32 %or716, i32* %arrayidx717, align 8
  %add718 = add i32 %add706, %or716
  %453 = load i32, i32* %D, align 4
  %add719 = add i32 %453, %add718
  store i32 %add719, i32* %D, align 4
  %454 = load i32, i32* %A, align 4
  %shl720 = shl i32 %454, 30
  %455 = load i32, i32* %A, align 4
  %shr721 = lshr i32 %455, 2
  %or722 = or i32 %shl720, %shr721
  store i32 %or722, i32* %A, align 4
  %456 = load i32, i32* %D, align 4
  %shl723 = shl i32 %456, 5
  %457 = load i32, i32* %D, align 4
  %shr724 = lshr i32 %457, 27
  %or725 = or i32 %shl723, %shr724
  %458 = load i32, i32* %E, align 4
  %459 = load i32, i32* %A, align 4
  %xor726 = xor i32 %458, %459
  %460 = load i32, i32* %B, align 4
  %xor727 = xor i32 %xor726, %460
  %add728 = add i32 %or725, %xor727
  %add729 = add i32 %add728, 1859775393
  %arrayidx730 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %461 = load i32, i32* %arrayidx730, align 16
  %arrayidx731 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %462 = load i32, i32* %arrayidx731, align 4
  %xor732 = xor i32 %461, %462
  %arrayidx733 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %463 = load i32, i32* %arrayidx733, align 4
  %xor734 = xor i32 %xor732, %463
  %arrayidx735 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %464 = load i32, i32* %arrayidx735, align 4
  %xor736 = xor i32 %xor734, %464
  store i32 %xor736, i32* %temp, align 4
  %465 = load i32, i32* %temp, align 4
  %shl737 = shl i32 %465, 1
  %466 = load i32, i32* %temp, align 4
  %shr738 = lshr i32 %466, 31
  %or739 = or i32 %shl737, %shr738
  %arrayidx740 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  store i32 %or739, i32* %arrayidx740, align 4
  %add741 = add i32 %add729, %or739
  %467 = load i32, i32* %C, align 4
  %add742 = add i32 %467, %add741
  store i32 %add742, i32* %C, align 4
  %468 = load i32, i32* %E, align 4
  %shl743 = shl i32 %468, 30
  %469 = load i32, i32* %E, align 4
  %shr744 = lshr i32 %469, 2
  %or745 = or i32 %shl743, %shr744
  store i32 %or745, i32* %E, align 4
  %470 = load i32, i32* %C, align 4
  %shl746 = shl i32 %470, 5
  %471 = load i32, i32* %C, align 4
  %shr747 = lshr i32 %471, 27
  %or748 = or i32 %shl746, %shr747
  %472 = load i32, i32* %D, align 4
  %473 = load i32, i32* %E, align 4
  %xor749 = xor i32 %472, %473
  %474 = load i32, i32* %A, align 4
  %xor750 = xor i32 %xor749, %474
  %add751 = add i32 %or748, %xor750
  %add752 = add i32 %add751, 1859775393
  %arrayidx753 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %475 = load i32, i32* %arrayidx753, align 4
  %arrayidx754 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %476 = load i32, i32* %arrayidx754, align 16
  %xor755 = xor i32 %475, %476
  %arrayidx756 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %477 = load i32, i32* %arrayidx756, align 8
  %xor757 = xor i32 %xor755, %477
  %arrayidx758 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %478 = load i32, i32* %arrayidx758, align 16
  %xor759 = xor i32 %xor757, %478
  store i32 %xor759, i32* %temp, align 4
  %479 = load i32, i32* %temp, align 4
  %shl760 = shl i32 %479, 1
  %480 = load i32, i32* %temp, align 4
  %shr761 = lshr i32 %480, 31
  %or762 = or i32 %shl760, %shr761
  %arrayidx763 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  store i32 %or762, i32* %arrayidx763, align 16
  %add764 = add i32 %add752, %or762
  %481 = load i32, i32* %B, align 4
  %add765 = add i32 %481, %add764
  store i32 %add765, i32* %B, align 4
  %482 = load i32, i32* %D, align 4
  %shl766 = shl i32 %482, 30
  %483 = load i32, i32* %D, align 4
  %shr767 = lshr i32 %483, 2
  %or768 = or i32 %shl766, %shr767
  store i32 %or768, i32* %D, align 4
  %484 = load i32, i32* %B, align 4
  %shl769 = shl i32 %484, 5
  %485 = load i32, i32* %B, align 4
  %shr770 = lshr i32 %485, 27
  %or771 = or i32 %shl769, %shr770
  %486 = load i32, i32* %C, align 4
  %487 = load i32, i32* %D, align 4
  %xor772 = xor i32 %486, %487
  %488 = load i32, i32* %E, align 4
  %xor773 = xor i32 %xor772, %488
  %add774 = add i32 %or771, %xor773
  %add775 = add i32 %add774, 1859775393
  %arrayidx776 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %489 = load i32, i32* %arrayidx776, align 8
  %arrayidx777 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %490 = load i32, i32* %arrayidx777, align 4
  %xor778 = xor i32 %489, %490
  %arrayidx779 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %491 = load i32, i32* %arrayidx779, align 4
  %xor780 = xor i32 %xor778, %491
  %arrayidx781 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %492 = load i32, i32* %arrayidx781, align 4
  %xor782 = xor i32 %xor780, %492
  store i32 %xor782, i32* %temp, align 4
  %493 = load i32, i32* %temp, align 4
  %shl783 = shl i32 %493, 1
  %494 = load i32, i32* %temp, align 4
  %shr784 = lshr i32 %494, 31
  %or785 = or i32 %shl783, %shr784
  %arrayidx786 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  store i32 %or785, i32* %arrayidx786, align 4
  %add787 = add i32 %add775, %or785
  %495 = load i32, i32* %A, align 4
  %add788 = add i32 %495, %add787
  store i32 %add788, i32* %A, align 4
  %496 = load i32, i32* %C, align 4
  %shl789 = shl i32 %496, 30
  %497 = load i32, i32* %C, align 4
  %shr790 = lshr i32 %497, 2
  %or791 = or i32 %shl789, %shr790
  store i32 %or791, i32* %C, align 4
  %498 = load i32, i32* %A, align 4
  %shl792 = shl i32 %498, 5
  %499 = load i32, i32* %A, align 4
  %shr793 = lshr i32 %499, 27
  %or794 = or i32 %shl792, %shr793
  %500 = load i32, i32* %B, align 4
  %501 = load i32, i32* %C, align 4
  %xor795 = xor i32 %500, %501
  %502 = load i32, i32* %D, align 4
  %xor796 = xor i32 %xor795, %502
  %add797 = add i32 %or794, %xor796
  %add798 = add i32 %add797, 1859775393
  %arrayidx799 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %503 = load i32, i32* %arrayidx799, align 4
  %arrayidx800 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %504 = load i32, i32* %arrayidx800, align 8
  %xor801 = xor i32 %503, %504
  %arrayidx802 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %505 = load i32, i32* %arrayidx802, align 16
  %xor803 = xor i32 %xor801, %505
  %arrayidx804 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %506 = load i32, i32* %arrayidx804, align 8
  %xor805 = xor i32 %xor803, %506
  store i32 %xor805, i32* %temp, align 4
  %507 = load i32, i32* %temp, align 4
  %shl806 = shl i32 %507, 1
  %508 = load i32, i32* %temp, align 4
  %shr807 = lshr i32 %508, 31
  %or808 = or i32 %shl806, %shr807
  %arrayidx809 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  store i32 %or808, i32* %arrayidx809, align 8
  %add810 = add i32 %add798, %or808
  %509 = load i32, i32* %E, align 4
  %add811 = add i32 %509, %add810
  store i32 %add811, i32* %E, align 4
  %510 = load i32, i32* %B, align 4
  %shl812 = shl i32 %510, 30
  %511 = load i32, i32* %B, align 4
  %shr813 = lshr i32 %511, 2
  %or814 = or i32 %shl812, %shr813
  store i32 %or814, i32* %B, align 4
  %512 = load i32, i32* %E, align 4
  %shl815 = shl i32 %512, 5
  %513 = load i32, i32* %E, align 4
  %shr816 = lshr i32 %513, 27
  %or817 = or i32 %shl815, %shr816
  %514 = load i32, i32* %A, align 4
  %515 = load i32, i32* %B, align 4
  %xor818 = xor i32 %514, %515
  %516 = load i32, i32* %C, align 4
  %xor819 = xor i32 %xor818, %516
  %add820 = add i32 %or817, %xor819
  %add821 = add i32 %add820, 1859775393
  %arrayidx822 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %517 = load i32, i32* %arrayidx822, align 16
  %arrayidx823 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %518 = load i32, i32* %arrayidx823, align 4
  %xor824 = xor i32 %517, %518
  %arrayidx825 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %519 = load i32, i32* %arrayidx825, align 4
  %xor826 = xor i32 %xor824, %519
  %arrayidx827 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %520 = load i32, i32* %arrayidx827, align 4
  %xor828 = xor i32 %xor826, %520
  store i32 %xor828, i32* %temp, align 4
  %521 = load i32, i32* %temp, align 4
  %shl829 = shl i32 %521, 1
  %522 = load i32, i32* %temp, align 4
  %shr830 = lshr i32 %522, 31
  %or831 = or i32 %shl829, %shr830
  %arrayidx832 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  store i32 %or831, i32* %arrayidx832, align 4
  %add833 = add i32 %add821, %or831
  %523 = load i32, i32* %D, align 4
  %add834 = add i32 %523, %add833
  store i32 %add834, i32* %D, align 4
  %524 = load i32, i32* %A, align 4
  %shl835 = shl i32 %524, 30
  %525 = load i32, i32* %A, align 4
  %shr836 = lshr i32 %525, 2
  %or837 = or i32 %shl835, %shr836
  store i32 %or837, i32* %A, align 4
  %526 = load i32, i32* %D, align 4
  %shl838 = shl i32 %526, 5
  %527 = load i32, i32* %D, align 4
  %shr839 = lshr i32 %527, 27
  %or840 = or i32 %shl838, %shr839
  %528 = load i32, i32* %E, align 4
  %529 = load i32, i32* %A, align 4
  %xor841 = xor i32 %528, %529
  %530 = load i32, i32* %B, align 4
  %xor842 = xor i32 %xor841, %530
  %add843 = add i32 %or840, %xor842
  %add844 = add i32 %add843, 1859775393
  %arrayidx845 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %531 = load i32, i32* %arrayidx845, align 4
  %arrayidx846 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %532 = load i32, i32* %arrayidx846, align 16
  %xor847 = xor i32 %531, %532
  %arrayidx848 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %533 = load i32, i32* %arrayidx848, align 8
  %xor849 = xor i32 %xor847, %533
  %arrayidx850 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %534 = load i32, i32* %arrayidx850, align 16
  %xor851 = xor i32 %xor849, %534
  store i32 %xor851, i32* %temp, align 4
  %535 = load i32, i32* %temp, align 4
  %shl852 = shl i32 %535, 1
  %536 = load i32, i32* %temp, align 4
  %shr853 = lshr i32 %536, 31
  %or854 = or i32 %shl852, %shr853
  %arrayidx855 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  store i32 %or854, i32* %arrayidx855, align 16
  %add856 = add i32 %add844, %or854
  %537 = load i32, i32* %C, align 4
  %add857 = add i32 %537, %add856
  store i32 %add857, i32* %C, align 4
  %538 = load i32, i32* %E, align 4
  %shl858 = shl i32 %538, 30
  %539 = load i32, i32* %E, align 4
  %shr859 = lshr i32 %539, 2
  %or860 = or i32 %shl858, %shr859
  store i32 %or860, i32* %E, align 4
  %540 = load i32, i32* %C, align 4
  %shl861 = shl i32 %540, 5
  %541 = load i32, i32* %C, align 4
  %shr862 = lshr i32 %541, 27
  %or863 = or i32 %shl861, %shr862
  %542 = load i32, i32* %D, align 4
  %543 = load i32, i32* %E, align 4
  %xor864 = xor i32 %542, %543
  %544 = load i32, i32* %A, align 4
  %xor865 = xor i32 %xor864, %544
  %add866 = add i32 %or863, %xor865
  %add867 = add i32 %add866, 1859775393
  %arrayidx868 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %545 = load i32, i32* %arrayidx868, align 8
  %arrayidx869 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %546 = load i32, i32* %arrayidx869, align 4
  %xor870 = xor i32 %545, %546
  %arrayidx871 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %547 = load i32, i32* %arrayidx871, align 4
  %xor872 = xor i32 %xor870, %547
  %arrayidx873 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %548 = load i32, i32* %arrayidx873, align 4
  %xor874 = xor i32 %xor872, %548
  store i32 %xor874, i32* %temp, align 4
  %549 = load i32, i32* %temp, align 4
  %shl875 = shl i32 %549, 1
  %550 = load i32, i32* %temp, align 4
  %shr876 = lshr i32 %550, 31
  %or877 = or i32 %shl875, %shr876
  %arrayidx878 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  store i32 %or877, i32* %arrayidx878, align 4
  %add879 = add i32 %add867, %or877
  %551 = load i32, i32* %B, align 4
  %add880 = add i32 %551, %add879
  store i32 %add880, i32* %B, align 4
  %552 = load i32, i32* %D, align 4
  %shl881 = shl i32 %552, 30
  %553 = load i32, i32* %D, align 4
  %shr882 = lshr i32 %553, 2
  %or883 = or i32 %shl881, %shr882
  store i32 %or883, i32* %D, align 4
  %554 = load i32, i32* %B, align 4
  %shl884 = shl i32 %554, 5
  %555 = load i32, i32* %B, align 4
  %shr885 = lshr i32 %555, 27
  %or886 = or i32 %shl884, %shr885
  %556 = load i32, i32* %C, align 4
  %557 = load i32, i32* %D, align 4
  %xor887 = xor i32 %556, %557
  %558 = load i32, i32* %E, align 4
  %xor888 = xor i32 %xor887, %558
  %add889 = add i32 %or886, %xor888
  %add890 = add i32 %add889, 1859775393
  %arrayidx891 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %559 = load i32, i32* %arrayidx891, align 4
  %arrayidx892 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %560 = load i32, i32* %arrayidx892, align 8
  %xor893 = xor i32 %559, %560
  %arrayidx894 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %561 = load i32, i32* %arrayidx894, align 16
  %xor895 = xor i32 %xor893, %561
  %arrayidx896 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %562 = load i32, i32* %arrayidx896, align 8
  %xor897 = xor i32 %xor895, %562
  store i32 %xor897, i32* %temp, align 4
  %563 = load i32, i32* %temp, align 4
  %shl898 = shl i32 %563, 1
  %564 = load i32, i32* %temp, align 4
  %shr899 = lshr i32 %564, 31
  %or900 = or i32 %shl898, %shr899
  %arrayidx901 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  store i32 %or900, i32* %arrayidx901, align 8
  %add902 = add i32 %add890, %or900
  %565 = load i32, i32* %A, align 4
  %add903 = add i32 %565, %add902
  store i32 %add903, i32* %A, align 4
  %566 = load i32, i32* %C, align 4
  %shl904 = shl i32 %566, 30
  %567 = load i32, i32* %C, align 4
  %shr905 = lshr i32 %567, 2
  %or906 = or i32 %shl904, %shr905
  store i32 %or906, i32* %C, align 4
  %568 = load i32, i32* %A, align 4
  %shl907 = shl i32 %568, 5
  %569 = load i32, i32* %A, align 4
  %shr908 = lshr i32 %569, 27
  %or909 = or i32 %shl907, %shr908
  %570 = load i32, i32* %B, align 4
  %571 = load i32, i32* %C, align 4
  %xor910 = xor i32 %570, %571
  %572 = load i32, i32* %D, align 4
  %xor911 = xor i32 %xor910, %572
  %add912 = add i32 %or909, %xor911
  %add913 = add i32 %add912, 1859775393
  %arrayidx914 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %573 = load i32, i32* %arrayidx914, align 16
  %arrayidx915 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %574 = load i32, i32* %arrayidx915, align 4
  %xor916 = xor i32 %573, %574
  %arrayidx917 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %575 = load i32, i32* %arrayidx917, align 4
  %xor918 = xor i32 %xor916, %575
  %arrayidx919 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %576 = load i32, i32* %arrayidx919, align 4
  %xor920 = xor i32 %xor918, %576
  store i32 %xor920, i32* %temp, align 4
  %577 = load i32, i32* %temp, align 4
  %shl921 = shl i32 %577, 1
  %578 = load i32, i32* %temp, align 4
  %shr922 = lshr i32 %578, 31
  %or923 = or i32 %shl921, %shr922
  %arrayidx924 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  store i32 %or923, i32* %arrayidx924, align 4
  %add925 = add i32 %add913, %or923
  %579 = load i32, i32* %E, align 4
  %add926 = add i32 %579, %add925
  store i32 %add926, i32* %E, align 4
  %580 = load i32, i32* %B, align 4
  %shl927 = shl i32 %580, 30
  %581 = load i32, i32* %B, align 4
  %shr928 = lshr i32 %581, 2
  %or929 = or i32 %shl927, %shr928
  store i32 %or929, i32* %B, align 4
  %582 = load i32, i32* %E, align 4
  %shl930 = shl i32 %582, 5
  %583 = load i32, i32* %E, align 4
  %shr931 = lshr i32 %583, 27
  %or932 = or i32 %shl930, %shr931
  %584 = load i32, i32* %A, align 4
  %585 = load i32, i32* %B, align 4
  %xor933 = xor i32 %584, %585
  %586 = load i32, i32* %C, align 4
  %xor934 = xor i32 %xor933, %586
  %add935 = add i32 %or932, %xor934
  %add936 = add i32 %add935, 1859775393
  %arrayidx937 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %587 = load i32, i32* %arrayidx937, align 4
  %arrayidx938 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %588 = load i32, i32* %arrayidx938, align 16
  %xor939 = xor i32 %587, %588
  %arrayidx940 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %589 = load i32, i32* %arrayidx940, align 8
  %xor941 = xor i32 %xor939, %589
  %arrayidx942 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %590 = load i32, i32* %arrayidx942, align 16
  %xor943 = xor i32 %xor941, %590
  store i32 %xor943, i32* %temp, align 4
  %591 = load i32, i32* %temp, align 4
  %shl944 = shl i32 %591, 1
  %592 = load i32, i32* %temp, align 4
  %shr945 = lshr i32 %592, 31
  %or946 = or i32 %shl944, %shr945
  %arrayidx947 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  store i32 %or946, i32* %arrayidx947, align 16
  %add948 = add i32 %add936, %or946
  %593 = load i32, i32* %D, align 4
  %add949 = add i32 %593, %add948
  store i32 %add949, i32* %D, align 4
  %594 = load i32, i32* %A, align 4
  %shl950 = shl i32 %594, 30
  %595 = load i32, i32* %A, align 4
  %shr951 = lshr i32 %595, 2
  %or952 = or i32 %shl950, %shr951
  store i32 %or952, i32* %A, align 4
  %596 = load i32, i32* %D, align 4
  %shl953 = shl i32 %596, 5
  %597 = load i32, i32* %D, align 4
  %shr954 = lshr i32 %597, 27
  %or955 = or i32 %shl953, %shr954
  %598 = load i32, i32* %E, align 4
  %599 = load i32, i32* %A, align 4
  %xor956 = xor i32 %598, %599
  %600 = load i32, i32* %B, align 4
  %xor957 = xor i32 %xor956, %600
  %add958 = add i32 %or955, %xor957
  %add959 = add i32 %add958, 1859775393
  %arrayidx960 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %601 = load i32, i32* %arrayidx960, align 8
  %arrayidx961 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %602 = load i32, i32* %arrayidx961, align 4
  %xor962 = xor i32 %601, %602
  %arrayidx963 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %603 = load i32, i32* %arrayidx963, align 4
  %xor964 = xor i32 %xor962, %603
  %arrayidx965 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %604 = load i32, i32* %arrayidx965, align 4
  %xor966 = xor i32 %xor964, %604
  store i32 %xor966, i32* %temp, align 4
  %605 = load i32, i32* %temp, align 4
  %shl967 = shl i32 %605, 1
  %606 = load i32, i32* %temp, align 4
  %shr968 = lshr i32 %606, 31
  %or969 = or i32 %shl967, %shr968
  %arrayidx970 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  store i32 %or969, i32* %arrayidx970, align 4
  %add971 = add i32 %add959, %or969
  %607 = load i32, i32* %C, align 4
  %add972 = add i32 %607, %add971
  store i32 %add972, i32* %C, align 4
  %608 = load i32, i32* %E, align 4
  %shl973 = shl i32 %608, 30
  %609 = load i32, i32* %E, align 4
  %shr974 = lshr i32 %609, 2
  %or975 = or i32 %shl973, %shr974
  store i32 %or975, i32* %E, align 4
  %610 = load i32, i32* %C, align 4
  %shl976 = shl i32 %610, 5
  %611 = load i32, i32* %C, align 4
  %shr977 = lshr i32 %611, 27
  %or978 = or i32 %shl976, %shr977
  %612 = load i32, i32* %D, align 4
  %613 = load i32, i32* %E, align 4
  %xor979 = xor i32 %612, %613
  %614 = load i32, i32* %A, align 4
  %xor980 = xor i32 %xor979, %614
  %add981 = add i32 %or978, %xor980
  %add982 = add i32 %add981, 1859775393
  %arrayidx983 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %615 = load i32, i32* %arrayidx983, align 4
  %arrayidx984 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %616 = load i32, i32* %arrayidx984, align 8
  %xor985 = xor i32 %615, %616
  %arrayidx986 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %617 = load i32, i32* %arrayidx986, align 16
  %xor987 = xor i32 %xor985, %617
  %arrayidx988 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %618 = load i32, i32* %arrayidx988, align 8
  %xor989 = xor i32 %xor987, %618
  store i32 %xor989, i32* %temp, align 4
  %619 = load i32, i32* %temp, align 4
  %shl990 = shl i32 %619, 1
  %620 = load i32, i32* %temp, align 4
  %shr991 = lshr i32 %620, 31
  %or992 = or i32 %shl990, %shr991
  %arrayidx993 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  store i32 %or992, i32* %arrayidx993, align 8
  %add994 = add i32 %add982, %or992
  %621 = load i32, i32* %B, align 4
  %add995 = add i32 %621, %add994
  store i32 %add995, i32* %B, align 4
  %622 = load i32, i32* %D, align 4
  %shl996 = shl i32 %622, 30
  %623 = load i32, i32* %D, align 4
  %shr997 = lshr i32 %623, 2
  %or998 = or i32 %shl996, %shr997
  store i32 %or998, i32* %D, align 4
  %624 = load i32, i32* %B, align 4
  %shl999 = shl i32 %624, 5
  %625 = load i32, i32* %B, align 4
  %shr1000 = lshr i32 %625, 27
  %or1001 = or i32 %shl999, %shr1000
  %626 = load i32, i32* %C, align 4
  %627 = load i32, i32* %D, align 4
  %xor1002 = xor i32 %626, %627
  %628 = load i32, i32* %E, align 4
  %xor1003 = xor i32 %xor1002, %628
  %add1004 = add i32 %or1001, %xor1003
  %add1005 = add i32 %add1004, 1859775393
  %arrayidx1006 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %629 = load i32, i32* %arrayidx1006, align 16
  %arrayidx1007 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %630 = load i32, i32* %arrayidx1007, align 4
  %xor1008 = xor i32 %629, %630
  %arrayidx1009 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %631 = load i32, i32* %arrayidx1009, align 4
  %xor1010 = xor i32 %xor1008, %631
  %arrayidx1011 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %632 = load i32, i32* %arrayidx1011, align 4
  %xor1012 = xor i32 %xor1010, %632
  store i32 %xor1012, i32* %temp, align 4
  %633 = load i32, i32* %temp, align 4
  %shl1013 = shl i32 %633, 1
  %634 = load i32, i32* %temp, align 4
  %shr1014 = lshr i32 %634, 31
  %or1015 = or i32 %shl1013, %shr1014
  %arrayidx1016 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  store i32 %or1015, i32* %arrayidx1016, align 4
  %add1017 = add i32 %add1005, %or1015
  %635 = load i32, i32* %A, align 4
  %add1018 = add i32 %635, %add1017
  store i32 %add1018, i32* %A, align 4
  %636 = load i32, i32* %C, align 4
  %shl1019 = shl i32 %636, 30
  %637 = load i32, i32* %C, align 4
  %shr1020 = lshr i32 %637, 2
  %or1021 = or i32 %shl1019, %shr1020
  store i32 %or1021, i32* %C, align 4
  %638 = load i32, i32* %A, align 4
  %shl1022 = shl i32 %638, 5
  %639 = load i32, i32* %A, align 4
  %shr1023 = lshr i32 %639, 27
  %or1024 = or i32 %shl1022, %shr1023
  %640 = load i32, i32* %B, align 4
  %641 = load i32, i32* %C, align 4
  %and1025 = and i32 %640, %641
  %642 = load i32, i32* %D, align 4
  %643 = load i32, i32* %B, align 4
  %644 = load i32, i32* %C, align 4
  %or1026 = or i32 %643, %644
  %and1027 = and i32 %642, %or1026
  %or1028 = or i32 %and1025, %and1027
  %add1029 = add i32 %or1024, %or1028
  %add1030 = add i32 %add1029, -1894007588
  %arrayidx1031 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %645 = load i32, i32* %arrayidx1031, align 4
  %arrayidx1032 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %646 = load i32, i32* %arrayidx1032, align 16
  %xor1033 = xor i32 %645, %646
  %arrayidx1034 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %647 = load i32, i32* %arrayidx1034, align 8
  %xor1035 = xor i32 %xor1033, %647
  %arrayidx1036 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %648 = load i32, i32* %arrayidx1036, align 16
  %xor1037 = xor i32 %xor1035, %648
  store i32 %xor1037, i32* %temp, align 4
  %649 = load i32, i32* %temp, align 4
  %shl1038 = shl i32 %649, 1
  %650 = load i32, i32* %temp, align 4
  %shr1039 = lshr i32 %650, 31
  %or1040 = or i32 %shl1038, %shr1039
  %arrayidx1041 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  store i32 %or1040, i32* %arrayidx1041, align 16
  %add1042 = add i32 %add1030, %or1040
  %651 = load i32, i32* %E, align 4
  %add1043 = add i32 %651, %add1042
  store i32 %add1043, i32* %E, align 4
  %652 = load i32, i32* %B, align 4
  %shl1044 = shl i32 %652, 30
  %653 = load i32, i32* %B, align 4
  %shr1045 = lshr i32 %653, 2
  %or1046 = or i32 %shl1044, %shr1045
  store i32 %or1046, i32* %B, align 4
  %654 = load i32, i32* %E, align 4
  %shl1047 = shl i32 %654, 5
  %655 = load i32, i32* %E, align 4
  %shr1048 = lshr i32 %655, 27
  %or1049 = or i32 %shl1047, %shr1048
  %656 = load i32, i32* %A, align 4
  %657 = load i32, i32* %B, align 4
  %and1050 = and i32 %656, %657
  %658 = load i32, i32* %C, align 4
  %659 = load i32, i32* %A, align 4
  %660 = load i32, i32* %B, align 4
  %or1051 = or i32 %659, %660
  %and1052 = and i32 %658, %or1051
  %or1053 = or i32 %and1050, %and1052
  %add1054 = add i32 %or1049, %or1053
  %add1055 = add i32 %add1054, -1894007588
  %arrayidx1056 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %661 = load i32, i32* %arrayidx1056, align 8
  %arrayidx1057 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %662 = load i32, i32* %arrayidx1057, align 4
  %xor1058 = xor i32 %661, %662
  %arrayidx1059 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %663 = load i32, i32* %arrayidx1059, align 4
  %xor1060 = xor i32 %xor1058, %663
  %arrayidx1061 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %664 = load i32, i32* %arrayidx1061, align 4
  %xor1062 = xor i32 %xor1060, %664
  store i32 %xor1062, i32* %temp, align 4
  %665 = load i32, i32* %temp, align 4
  %shl1063 = shl i32 %665, 1
  %666 = load i32, i32* %temp, align 4
  %shr1064 = lshr i32 %666, 31
  %or1065 = or i32 %shl1063, %shr1064
  %arrayidx1066 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  store i32 %or1065, i32* %arrayidx1066, align 4
  %add1067 = add i32 %add1055, %or1065
  %667 = load i32, i32* %D, align 4
  %add1068 = add i32 %667, %add1067
  store i32 %add1068, i32* %D, align 4
  %668 = load i32, i32* %A, align 4
  %shl1069 = shl i32 %668, 30
  %669 = load i32, i32* %A, align 4
  %shr1070 = lshr i32 %669, 2
  %or1071 = or i32 %shl1069, %shr1070
  store i32 %or1071, i32* %A, align 4
  %670 = load i32, i32* %D, align 4
  %shl1072 = shl i32 %670, 5
  %671 = load i32, i32* %D, align 4
  %shr1073 = lshr i32 %671, 27
  %or1074 = or i32 %shl1072, %shr1073
  %672 = load i32, i32* %E, align 4
  %673 = load i32, i32* %A, align 4
  %and1075 = and i32 %672, %673
  %674 = load i32, i32* %B, align 4
  %675 = load i32, i32* %E, align 4
  %676 = load i32, i32* %A, align 4
  %or1076 = or i32 %675, %676
  %and1077 = and i32 %674, %or1076
  %or1078 = or i32 %and1075, %and1077
  %add1079 = add i32 %or1074, %or1078
  %add1080 = add i32 %add1079, -1894007588
  %arrayidx1081 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %677 = load i32, i32* %arrayidx1081, align 4
  %arrayidx1082 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %678 = load i32, i32* %arrayidx1082, align 8
  %xor1083 = xor i32 %677, %678
  %arrayidx1084 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %679 = load i32, i32* %arrayidx1084, align 16
  %xor1085 = xor i32 %xor1083, %679
  %arrayidx1086 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %680 = load i32, i32* %arrayidx1086, align 8
  %xor1087 = xor i32 %xor1085, %680
  store i32 %xor1087, i32* %temp, align 4
  %681 = load i32, i32* %temp, align 4
  %shl1088 = shl i32 %681, 1
  %682 = load i32, i32* %temp, align 4
  %shr1089 = lshr i32 %682, 31
  %or1090 = or i32 %shl1088, %shr1089
  %arrayidx1091 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  store i32 %or1090, i32* %arrayidx1091, align 8
  %add1092 = add i32 %add1080, %or1090
  %683 = load i32, i32* %C, align 4
  %add1093 = add i32 %683, %add1092
  store i32 %add1093, i32* %C, align 4
  %684 = load i32, i32* %E, align 4
  %shl1094 = shl i32 %684, 30
  %685 = load i32, i32* %E, align 4
  %shr1095 = lshr i32 %685, 2
  %or1096 = or i32 %shl1094, %shr1095
  store i32 %or1096, i32* %E, align 4
  %686 = load i32, i32* %C, align 4
  %shl1097 = shl i32 %686, 5
  %687 = load i32, i32* %C, align 4
  %shr1098 = lshr i32 %687, 27
  %or1099 = or i32 %shl1097, %shr1098
  %688 = load i32, i32* %D, align 4
  %689 = load i32, i32* %E, align 4
  %and1100 = and i32 %688, %689
  %690 = load i32, i32* %A, align 4
  %691 = load i32, i32* %D, align 4
  %692 = load i32, i32* %E, align 4
  %or1101 = or i32 %691, %692
  %and1102 = and i32 %690, %or1101
  %or1103 = or i32 %and1100, %and1102
  %add1104 = add i32 %or1099, %or1103
  %add1105 = add i32 %add1104, -1894007588
  %arrayidx1106 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %693 = load i32, i32* %arrayidx1106, align 16
  %arrayidx1107 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %694 = load i32, i32* %arrayidx1107, align 4
  %xor1108 = xor i32 %693, %694
  %arrayidx1109 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %695 = load i32, i32* %arrayidx1109, align 4
  %xor1110 = xor i32 %xor1108, %695
  %arrayidx1111 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %696 = load i32, i32* %arrayidx1111, align 4
  %xor1112 = xor i32 %xor1110, %696
  store i32 %xor1112, i32* %temp, align 4
  %697 = load i32, i32* %temp, align 4
  %shl1113 = shl i32 %697, 1
  %698 = load i32, i32* %temp, align 4
  %shr1114 = lshr i32 %698, 31
  %or1115 = or i32 %shl1113, %shr1114
  %arrayidx1116 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  store i32 %or1115, i32* %arrayidx1116, align 4
  %add1117 = add i32 %add1105, %or1115
  %699 = load i32, i32* %B, align 4
  %add1118 = add i32 %699, %add1117
  store i32 %add1118, i32* %B, align 4
  %700 = load i32, i32* %D, align 4
  %shl1119 = shl i32 %700, 30
  %701 = load i32, i32* %D, align 4
  %shr1120 = lshr i32 %701, 2
  %or1121 = or i32 %shl1119, %shr1120
  store i32 %or1121, i32* %D, align 4
  %702 = load i32, i32* %B, align 4
  %shl1122 = shl i32 %702, 5
  %703 = load i32, i32* %B, align 4
  %shr1123 = lshr i32 %703, 27
  %or1124 = or i32 %shl1122, %shr1123
  %704 = load i32, i32* %C, align 4
  %705 = load i32, i32* %D, align 4
  %and1125 = and i32 %704, %705
  %706 = load i32, i32* %E, align 4
  %707 = load i32, i32* %C, align 4
  %708 = load i32, i32* %D, align 4
  %or1126 = or i32 %707, %708
  %and1127 = and i32 %706, %or1126
  %or1128 = or i32 %and1125, %and1127
  %add1129 = add i32 %or1124, %or1128
  %add1130 = add i32 %add1129, -1894007588
  %arrayidx1131 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %709 = load i32, i32* %arrayidx1131, align 4
  %arrayidx1132 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %710 = load i32, i32* %arrayidx1132, align 16
  %xor1133 = xor i32 %709, %710
  %arrayidx1134 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %711 = load i32, i32* %arrayidx1134, align 8
  %xor1135 = xor i32 %xor1133, %711
  %arrayidx1136 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %712 = load i32, i32* %arrayidx1136, align 16
  %xor1137 = xor i32 %xor1135, %712
  store i32 %xor1137, i32* %temp, align 4
  %713 = load i32, i32* %temp, align 4
  %shl1138 = shl i32 %713, 1
  %714 = load i32, i32* %temp, align 4
  %shr1139 = lshr i32 %714, 31
  %or1140 = or i32 %shl1138, %shr1139
  %arrayidx1141 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  store i32 %or1140, i32* %arrayidx1141, align 16
  %add1142 = add i32 %add1130, %or1140
  %715 = load i32, i32* %A, align 4
  %add1143 = add i32 %715, %add1142
  store i32 %add1143, i32* %A, align 4
  %716 = load i32, i32* %C, align 4
  %shl1144 = shl i32 %716, 30
  %717 = load i32, i32* %C, align 4
  %shr1145 = lshr i32 %717, 2
  %or1146 = or i32 %shl1144, %shr1145
  store i32 %or1146, i32* %C, align 4
  %718 = load i32, i32* %A, align 4
  %shl1147 = shl i32 %718, 5
  %719 = load i32, i32* %A, align 4
  %shr1148 = lshr i32 %719, 27
  %or1149 = or i32 %shl1147, %shr1148
  %720 = load i32, i32* %B, align 4
  %721 = load i32, i32* %C, align 4
  %and1150 = and i32 %720, %721
  %722 = load i32, i32* %D, align 4
  %723 = load i32, i32* %B, align 4
  %724 = load i32, i32* %C, align 4
  %or1151 = or i32 %723, %724
  %and1152 = and i32 %722, %or1151
  %or1153 = or i32 %and1150, %and1152
  %add1154 = add i32 %or1149, %or1153
  %add1155 = add i32 %add1154, -1894007588
  %arrayidx1156 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %725 = load i32, i32* %arrayidx1156, align 8
  %arrayidx1157 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %726 = load i32, i32* %arrayidx1157, align 4
  %xor1158 = xor i32 %725, %726
  %arrayidx1159 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %727 = load i32, i32* %arrayidx1159, align 4
  %xor1160 = xor i32 %xor1158, %727
  %arrayidx1161 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %728 = load i32, i32* %arrayidx1161, align 4
  %xor1162 = xor i32 %xor1160, %728
  store i32 %xor1162, i32* %temp, align 4
  %729 = load i32, i32* %temp, align 4
  %shl1163 = shl i32 %729, 1
  %730 = load i32, i32* %temp, align 4
  %shr1164 = lshr i32 %730, 31
  %or1165 = or i32 %shl1163, %shr1164
  %arrayidx1166 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  store i32 %or1165, i32* %arrayidx1166, align 4
  %add1167 = add i32 %add1155, %or1165
  %731 = load i32, i32* %E, align 4
  %add1168 = add i32 %731, %add1167
  store i32 %add1168, i32* %E, align 4
  %732 = load i32, i32* %B, align 4
  %shl1169 = shl i32 %732, 30
  %733 = load i32, i32* %B, align 4
  %shr1170 = lshr i32 %733, 2
  %or1171 = or i32 %shl1169, %shr1170
  store i32 %or1171, i32* %B, align 4
  %734 = load i32, i32* %E, align 4
  %shl1172 = shl i32 %734, 5
  %735 = load i32, i32* %E, align 4
  %shr1173 = lshr i32 %735, 27
  %or1174 = or i32 %shl1172, %shr1173
  %736 = load i32, i32* %A, align 4
  %737 = load i32, i32* %B, align 4
  %and1175 = and i32 %736, %737
  %738 = load i32, i32* %C, align 4
  %739 = load i32, i32* %A, align 4
  %740 = load i32, i32* %B, align 4
  %or1176 = or i32 %739, %740
  %and1177 = and i32 %738, %or1176
  %or1178 = or i32 %and1175, %and1177
  %add1179 = add i32 %or1174, %or1178
  %add1180 = add i32 %add1179, -1894007588
  %arrayidx1181 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %741 = load i32, i32* %arrayidx1181, align 4
  %arrayidx1182 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %742 = load i32, i32* %arrayidx1182, align 8
  %xor1183 = xor i32 %741, %742
  %arrayidx1184 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %743 = load i32, i32* %arrayidx1184, align 16
  %xor1185 = xor i32 %xor1183, %743
  %arrayidx1186 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %744 = load i32, i32* %arrayidx1186, align 8
  %xor1187 = xor i32 %xor1185, %744
  store i32 %xor1187, i32* %temp, align 4
  %745 = load i32, i32* %temp, align 4
  %shl1188 = shl i32 %745, 1
  %746 = load i32, i32* %temp, align 4
  %shr1189 = lshr i32 %746, 31
  %or1190 = or i32 %shl1188, %shr1189
  %arrayidx1191 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  store i32 %or1190, i32* %arrayidx1191, align 8
  %add1192 = add i32 %add1180, %or1190
  %747 = load i32, i32* %D, align 4
  %add1193 = add i32 %747, %add1192
  store i32 %add1193, i32* %D, align 4
  %748 = load i32, i32* %A, align 4
  %shl1194 = shl i32 %748, 30
  %749 = load i32, i32* %A, align 4
  %shr1195 = lshr i32 %749, 2
  %or1196 = or i32 %shl1194, %shr1195
  store i32 %or1196, i32* %A, align 4
  %750 = load i32, i32* %D, align 4
  %shl1197 = shl i32 %750, 5
  %751 = load i32, i32* %D, align 4
  %shr1198 = lshr i32 %751, 27
  %or1199 = or i32 %shl1197, %shr1198
  %752 = load i32, i32* %E, align 4
  %753 = load i32, i32* %A, align 4
  %and1200 = and i32 %752, %753
  %754 = load i32, i32* %B, align 4
  %755 = load i32, i32* %E, align 4
  %756 = load i32, i32* %A, align 4
  %or1201 = or i32 %755, %756
  %and1202 = and i32 %754, %or1201
  %or1203 = or i32 %and1200, %and1202
  %add1204 = add i32 %or1199, %or1203
  %add1205 = add i32 %add1204, -1894007588
  %arrayidx1206 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %757 = load i32, i32* %arrayidx1206, align 16
  %arrayidx1207 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %758 = load i32, i32* %arrayidx1207, align 4
  %xor1208 = xor i32 %757, %758
  %arrayidx1209 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %759 = load i32, i32* %arrayidx1209, align 4
  %xor1210 = xor i32 %xor1208, %759
  %arrayidx1211 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %760 = load i32, i32* %arrayidx1211, align 4
  %xor1212 = xor i32 %xor1210, %760
  store i32 %xor1212, i32* %temp, align 4
  %761 = load i32, i32* %temp, align 4
  %shl1213 = shl i32 %761, 1
  %762 = load i32, i32* %temp, align 4
  %shr1214 = lshr i32 %762, 31
  %or1215 = or i32 %shl1213, %shr1214
  %arrayidx1216 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  store i32 %or1215, i32* %arrayidx1216, align 4
  %add1217 = add i32 %add1205, %or1215
  %763 = load i32, i32* %C, align 4
  %add1218 = add i32 %763, %add1217
  store i32 %add1218, i32* %C, align 4
  %764 = load i32, i32* %E, align 4
  %shl1219 = shl i32 %764, 30
  %765 = load i32, i32* %E, align 4
  %shr1220 = lshr i32 %765, 2
  %or1221 = or i32 %shl1219, %shr1220
  store i32 %or1221, i32* %E, align 4
  %766 = load i32, i32* %C, align 4
  %shl1222 = shl i32 %766, 5
  %767 = load i32, i32* %C, align 4
  %shr1223 = lshr i32 %767, 27
  %or1224 = or i32 %shl1222, %shr1223
  %768 = load i32, i32* %D, align 4
  %769 = load i32, i32* %E, align 4
  %and1225 = and i32 %768, %769
  %770 = load i32, i32* %A, align 4
  %771 = load i32, i32* %D, align 4
  %772 = load i32, i32* %E, align 4
  %or1226 = or i32 %771, %772
  %and1227 = and i32 %770, %or1226
  %or1228 = or i32 %and1225, %and1227
  %add1229 = add i32 %or1224, %or1228
  %add1230 = add i32 %add1229, -1894007588
  %arrayidx1231 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %773 = load i32, i32* %arrayidx1231, align 4
  %arrayidx1232 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %774 = load i32, i32* %arrayidx1232, align 16
  %xor1233 = xor i32 %773, %774
  %arrayidx1234 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %775 = load i32, i32* %arrayidx1234, align 8
  %xor1235 = xor i32 %xor1233, %775
  %arrayidx1236 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %776 = load i32, i32* %arrayidx1236, align 16
  %xor1237 = xor i32 %xor1235, %776
  store i32 %xor1237, i32* %temp, align 4
  %777 = load i32, i32* %temp, align 4
  %shl1238 = shl i32 %777, 1
  %778 = load i32, i32* %temp, align 4
  %shr1239 = lshr i32 %778, 31
  %or1240 = or i32 %shl1238, %shr1239
  %arrayidx1241 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  store i32 %or1240, i32* %arrayidx1241, align 16
  %add1242 = add i32 %add1230, %or1240
  %779 = load i32, i32* %B, align 4
  %add1243 = add i32 %779, %add1242
  store i32 %add1243, i32* %B, align 4
  %780 = load i32, i32* %D, align 4
  %shl1244 = shl i32 %780, 30
  %781 = load i32, i32* %D, align 4
  %shr1245 = lshr i32 %781, 2
  %or1246 = or i32 %shl1244, %shr1245
  store i32 %or1246, i32* %D, align 4
  %782 = load i32, i32* %B, align 4
  %shl1247 = shl i32 %782, 5
  %783 = load i32, i32* %B, align 4
  %shr1248 = lshr i32 %783, 27
  %or1249 = or i32 %shl1247, %shr1248
  %784 = load i32, i32* %C, align 4
  %785 = load i32, i32* %D, align 4
  %and1250 = and i32 %784, %785
  %786 = load i32, i32* %E, align 4
  %787 = load i32, i32* %C, align 4
  %788 = load i32, i32* %D, align 4
  %or1251 = or i32 %787, %788
  %and1252 = and i32 %786, %or1251
  %or1253 = or i32 %and1250, %and1252
  %add1254 = add i32 %or1249, %or1253
  %add1255 = add i32 %add1254, -1894007588
  %arrayidx1256 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %789 = load i32, i32* %arrayidx1256, align 8
  %arrayidx1257 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %790 = load i32, i32* %arrayidx1257, align 4
  %xor1258 = xor i32 %789, %790
  %arrayidx1259 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %791 = load i32, i32* %arrayidx1259, align 4
  %xor1260 = xor i32 %xor1258, %791
  %arrayidx1261 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %792 = load i32, i32* %arrayidx1261, align 4
  %xor1262 = xor i32 %xor1260, %792
  store i32 %xor1262, i32* %temp, align 4
  %793 = load i32, i32* %temp, align 4
  %shl1263 = shl i32 %793, 1
  %794 = load i32, i32* %temp, align 4
  %shr1264 = lshr i32 %794, 31
  %or1265 = or i32 %shl1263, %shr1264
  %arrayidx1266 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  store i32 %or1265, i32* %arrayidx1266, align 4
  %add1267 = add i32 %add1255, %or1265
  %795 = load i32, i32* %A, align 4
  %add1268 = add i32 %795, %add1267
  store i32 %add1268, i32* %A, align 4
  %796 = load i32, i32* %C, align 4
  %shl1269 = shl i32 %796, 30
  %797 = load i32, i32* %C, align 4
  %shr1270 = lshr i32 %797, 2
  %or1271 = or i32 %shl1269, %shr1270
  store i32 %or1271, i32* %C, align 4
  %798 = load i32, i32* %A, align 4
  %shl1272 = shl i32 %798, 5
  %799 = load i32, i32* %A, align 4
  %shr1273 = lshr i32 %799, 27
  %or1274 = or i32 %shl1272, %shr1273
  %800 = load i32, i32* %B, align 4
  %801 = load i32, i32* %C, align 4
  %and1275 = and i32 %800, %801
  %802 = load i32, i32* %D, align 4
  %803 = load i32, i32* %B, align 4
  %804 = load i32, i32* %C, align 4
  %or1276 = or i32 %803, %804
  %and1277 = and i32 %802, %or1276
  %or1278 = or i32 %and1275, %and1277
  %add1279 = add i32 %or1274, %or1278
  %add1280 = add i32 %add1279, -1894007588
  %arrayidx1281 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %805 = load i32, i32* %arrayidx1281, align 4
  %arrayidx1282 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %806 = load i32, i32* %arrayidx1282, align 8
  %xor1283 = xor i32 %805, %806
  %arrayidx1284 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %807 = load i32, i32* %arrayidx1284, align 16
  %xor1285 = xor i32 %xor1283, %807
  %arrayidx1286 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %808 = load i32, i32* %arrayidx1286, align 8
  %xor1287 = xor i32 %xor1285, %808
  store i32 %xor1287, i32* %temp, align 4
  %809 = load i32, i32* %temp, align 4
  %shl1288 = shl i32 %809, 1
  %810 = load i32, i32* %temp, align 4
  %shr1289 = lshr i32 %810, 31
  %or1290 = or i32 %shl1288, %shr1289
  %arrayidx1291 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  store i32 %or1290, i32* %arrayidx1291, align 8
  %add1292 = add i32 %add1280, %or1290
  %811 = load i32, i32* %E, align 4
  %add1293 = add i32 %811, %add1292
  store i32 %add1293, i32* %E, align 4
  %812 = load i32, i32* %B, align 4
  %shl1294 = shl i32 %812, 30
  %813 = load i32, i32* %B, align 4
  %shr1295 = lshr i32 %813, 2
  %or1296 = or i32 %shl1294, %shr1295
  store i32 %or1296, i32* %B, align 4
  %814 = load i32, i32* %E, align 4
  %shl1297 = shl i32 %814, 5
  %815 = load i32, i32* %E, align 4
  %shr1298 = lshr i32 %815, 27
  %or1299 = or i32 %shl1297, %shr1298
  %816 = load i32, i32* %A, align 4
  %817 = load i32, i32* %B, align 4
  %and1300 = and i32 %816, %817
  %818 = load i32, i32* %C, align 4
  %819 = load i32, i32* %A, align 4
  %820 = load i32, i32* %B, align 4
  %or1301 = or i32 %819, %820
  %and1302 = and i32 %818, %or1301
  %or1303 = or i32 %and1300, %and1302
  %add1304 = add i32 %or1299, %or1303
  %add1305 = add i32 %add1304, -1894007588
  %arrayidx1306 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %821 = load i32, i32* %arrayidx1306, align 16
  %arrayidx1307 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %822 = load i32, i32* %arrayidx1307, align 4
  %xor1308 = xor i32 %821, %822
  %arrayidx1309 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %823 = load i32, i32* %arrayidx1309, align 4
  %xor1310 = xor i32 %xor1308, %823
  %arrayidx1311 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %824 = load i32, i32* %arrayidx1311, align 4
  %xor1312 = xor i32 %xor1310, %824
  store i32 %xor1312, i32* %temp, align 4
  %825 = load i32, i32* %temp, align 4
  %shl1313 = shl i32 %825, 1
  %826 = load i32, i32* %temp, align 4
  %shr1314 = lshr i32 %826, 31
  %or1315 = or i32 %shl1313, %shr1314
  %arrayidx1316 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  store i32 %or1315, i32* %arrayidx1316, align 4
  %add1317 = add i32 %add1305, %or1315
  %827 = load i32, i32* %D, align 4
  %add1318 = add i32 %827, %add1317
  store i32 %add1318, i32* %D, align 4
  %828 = load i32, i32* %A, align 4
  %shl1319 = shl i32 %828, 30
  %829 = load i32, i32* %A, align 4
  %shr1320 = lshr i32 %829, 2
  %or1321 = or i32 %shl1319, %shr1320
  store i32 %or1321, i32* %A, align 4
  %830 = load i32, i32* %D, align 4
  %shl1322 = shl i32 %830, 5
  %831 = load i32, i32* %D, align 4
  %shr1323 = lshr i32 %831, 27
  %or1324 = or i32 %shl1322, %shr1323
  %832 = load i32, i32* %E, align 4
  %833 = load i32, i32* %A, align 4
  %and1325 = and i32 %832, %833
  %834 = load i32, i32* %B, align 4
  %835 = load i32, i32* %E, align 4
  %836 = load i32, i32* %A, align 4
  %or1326 = or i32 %835, %836
  %and1327 = and i32 %834, %or1326
  %or1328 = or i32 %and1325, %and1327
  %add1329 = add i32 %or1324, %or1328
  %add1330 = add i32 %add1329, -1894007588
  %arrayidx1331 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %837 = load i32, i32* %arrayidx1331, align 4
  %arrayidx1332 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %838 = load i32, i32* %arrayidx1332, align 16
  %xor1333 = xor i32 %837, %838
  %arrayidx1334 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %839 = load i32, i32* %arrayidx1334, align 8
  %xor1335 = xor i32 %xor1333, %839
  %arrayidx1336 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %840 = load i32, i32* %arrayidx1336, align 16
  %xor1337 = xor i32 %xor1335, %840
  store i32 %xor1337, i32* %temp, align 4
  %841 = load i32, i32* %temp, align 4
  %shl1338 = shl i32 %841, 1
  %842 = load i32, i32* %temp, align 4
  %shr1339 = lshr i32 %842, 31
  %or1340 = or i32 %shl1338, %shr1339
  %arrayidx1341 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  store i32 %or1340, i32* %arrayidx1341, align 16
  %add1342 = add i32 %add1330, %or1340
  %843 = load i32, i32* %C, align 4
  %add1343 = add i32 %843, %add1342
  store i32 %add1343, i32* %C, align 4
  %844 = load i32, i32* %E, align 4
  %shl1344 = shl i32 %844, 30
  %845 = load i32, i32* %E, align 4
  %shr1345 = lshr i32 %845, 2
  %or1346 = or i32 %shl1344, %shr1345
  store i32 %or1346, i32* %E, align 4
  %846 = load i32, i32* %C, align 4
  %shl1347 = shl i32 %846, 5
  %847 = load i32, i32* %C, align 4
  %shr1348 = lshr i32 %847, 27
  %or1349 = or i32 %shl1347, %shr1348
  %848 = load i32, i32* %D, align 4
  %849 = load i32, i32* %E, align 4
  %and1350 = and i32 %848, %849
  %850 = load i32, i32* %A, align 4
  %851 = load i32, i32* %D, align 4
  %852 = load i32, i32* %E, align 4
  %or1351 = or i32 %851, %852
  %and1352 = and i32 %850, %or1351
  %or1353 = or i32 %and1350, %and1352
  %add1354 = add i32 %or1349, %or1353
  %add1355 = add i32 %add1354, -1894007588
  %arrayidx1356 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %853 = load i32, i32* %arrayidx1356, align 8
  %arrayidx1357 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %854 = load i32, i32* %arrayidx1357, align 4
  %xor1358 = xor i32 %853, %854
  %arrayidx1359 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %855 = load i32, i32* %arrayidx1359, align 4
  %xor1360 = xor i32 %xor1358, %855
  %arrayidx1361 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %856 = load i32, i32* %arrayidx1361, align 4
  %xor1362 = xor i32 %xor1360, %856
  store i32 %xor1362, i32* %temp, align 4
  %857 = load i32, i32* %temp, align 4
  %shl1363 = shl i32 %857, 1
  %858 = load i32, i32* %temp, align 4
  %shr1364 = lshr i32 %858, 31
  %or1365 = or i32 %shl1363, %shr1364
  %arrayidx1366 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  store i32 %or1365, i32* %arrayidx1366, align 4
  %add1367 = add i32 %add1355, %or1365
  %859 = load i32, i32* %B, align 4
  %add1368 = add i32 %859, %add1367
  store i32 %add1368, i32* %B, align 4
  %860 = load i32, i32* %D, align 4
  %shl1369 = shl i32 %860, 30
  %861 = load i32, i32* %D, align 4
  %shr1370 = lshr i32 %861, 2
  %or1371 = or i32 %shl1369, %shr1370
  store i32 %or1371, i32* %D, align 4
  %862 = load i32, i32* %B, align 4
  %shl1372 = shl i32 %862, 5
  %863 = load i32, i32* %B, align 4
  %shr1373 = lshr i32 %863, 27
  %or1374 = or i32 %shl1372, %shr1373
  %864 = load i32, i32* %C, align 4
  %865 = load i32, i32* %D, align 4
  %and1375 = and i32 %864, %865
  %866 = load i32, i32* %E, align 4
  %867 = load i32, i32* %C, align 4
  %868 = load i32, i32* %D, align 4
  %or1376 = or i32 %867, %868
  %and1377 = and i32 %866, %or1376
  %or1378 = or i32 %and1375, %and1377
  %add1379 = add i32 %or1374, %or1378
  %add1380 = add i32 %add1379, -1894007588
  %arrayidx1381 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %869 = load i32, i32* %arrayidx1381, align 4
  %arrayidx1382 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %870 = load i32, i32* %arrayidx1382, align 8
  %xor1383 = xor i32 %869, %870
  %arrayidx1384 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %871 = load i32, i32* %arrayidx1384, align 16
  %xor1385 = xor i32 %xor1383, %871
  %arrayidx1386 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %872 = load i32, i32* %arrayidx1386, align 8
  %xor1387 = xor i32 %xor1385, %872
  store i32 %xor1387, i32* %temp, align 4
  %873 = load i32, i32* %temp, align 4
  %shl1388 = shl i32 %873, 1
  %874 = load i32, i32* %temp, align 4
  %shr1389 = lshr i32 %874, 31
  %or1390 = or i32 %shl1388, %shr1389
  %arrayidx1391 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  store i32 %or1390, i32* %arrayidx1391, align 8
  %add1392 = add i32 %add1380, %or1390
  %875 = load i32, i32* %A, align 4
  %add1393 = add i32 %875, %add1392
  store i32 %add1393, i32* %A, align 4
  %876 = load i32, i32* %C, align 4
  %shl1394 = shl i32 %876, 30
  %877 = load i32, i32* %C, align 4
  %shr1395 = lshr i32 %877, 2
  %or1396 = or i32 %shl1394, %shr1395
  store i32 %or1396, i32* %C, align 4
  %878 = load i32, i32* %A, align 4
  %shl1397 = shl i32 %878, 5
  %879 = load i32, i32* %A, align 4
  %shr1398 = lshr i32 %879, 27
  %or1399 = or i32 %shl1397, %shr1398
  %880 = load i32, i32* %B, align 4
  %881 = load i32, i32* %C, align 4
  %and1400 = and i32 %880, %881
  %882 = load i32, i32* %D, align 4
  %883 = load i32, i32* %B, align 4
  %884 = load i32, i32* %C, align 4
  %or1401 = or i32 %883, %884
  %and1402 = and i32 %882, %or1401
  %or1403 = or i32 %and1400, %and1402
  %add1404 = add i32 %or1399, %or1403
  %add1405 = add i32 %add1404, -1894007588
  %arrayidx1406 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %885 = load i32, i32* %arrayidx1406, align 16
  %arrayidx1407 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %886 = load i32, i32* %arrayidx1407, align 4
  %xor1408 = xor i32 %885, %886
  %arrayidx1409 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %887 = load i32, i32* %arrayidx1409, align 4
  %xor1410 = xor i32 %xor1408, %887
  %arrayidx1411 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %888 = load i32, i32* %arrayidx1411, align 4
  %xor1412 = xor i32 %xor1410, %888
  store i32 %xor1412, i32* %temp, align 4
  %889 = load i32, i32* %temp, align 4
  %shl1413 = shl i32 %889, 1
  %890 = load i32, i32* %temp, align 4
  %shr1414 = lshr i32 %890, 31
  %or1415 = or i32 %shl1413, %shr1414
  %arrayidx1416 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  store i32 %or1415, i32* %arrayidx1416, align 4
  %add1417 = add i32 %add1405, %or1415
  %891 = load i32, i32* %E, align 4
  %add1418 = add i32 %891, %add1417
  store i32 %add1418, i32* %E, align 4
  %892 = load i32, i32* %B, align 4
  %shl1419 = shl i32 %892, 30
  %893 = load i32, i32* %B, align 4
  %shr1420 = lshr i32 %893, 2
  %or1421 = or i32 %shl1419, %shr1420
  store i32 %or1421, i32* %B, align 4
  %894 = load i32, i32* %E, align 4
  %shl1422 = shl i32 %894, 5
  %895 = load i32, i32* %E, align 4
  %shr1423 = lshr i32 %895, 27
  %or1424 = or i32 %shl1422, %shr1423
  %896 = load i32, i32* %A, align 4
  %897 = load i32, i32* %B, align 4
  %and1425 = and i32 %896, %897
  %898 = load i32, i32* %C, align 4
  %899 = load i32, i32* %A, align 4
  %900 = load i32, i32* %B, align 4
  %or1426 = or i32 %899, %900
  %and1427 = and i32 %898, %or1426
  %or1428 = or i32 %and1425, %and1427
  %add1429 = add i32 %or1424, %or1428
  %add1430 = add i32 %add1429, -1894007588
  %arrayidx1431 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %901 = load i32, i32* %arrayidx1431, align 4
  %arrayidx1432 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %902 = load i32, i32* %arrayidx1432, align 16
  %xor1433 = xor i32 %901, %902
  %arrayidx1434 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %903 = load i32, i32* %arrayidx1434, align 8
  %xor1435 = xor i32 %xor1433, %903
  %arrayidx1436 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %904 = load i32, i32* %arrayidx1436, align 16
  %xor1437 = xor i32 %xor1435, %904
  store i32 %xor1437, i32* %temp, align 4
  %905 = load i32, i32* %temp, align 4
  %shl1438 = shl i32 %905, 1
  %906 = load i32, i32* %temp, align 4
  %shr1439 = lshr i32 %906, 31
  %or1440 = or i32 %shl1438, %shr1439
  %arrayidx1441 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  store i32 %or1440, i32* %arrayidx1441, align 16
  %add1442 = add i32 %add1430, %or1440
  %907 = load i32, i32* %D, align 4
  %add1443 = add i32 %907, %add1442
  store i32 %add1443, i32* %D, align 4
  %908 = load i32, i32* %A, align 4
  %shl1444 = shl i32 %908, 30
  %909 = load i32, i32* %A, align 4
  %shr1445 = lshr i32 %909, 2
  %or1446 = or i32 %shl1444, %shr1445
  store i32 %or1446, i32* %A, align 4
  %910 = load i32, i32* %D, align 4
  %shl1447 = shl i32 %910, 5
  %911 = load i32, i32* %D, align 4
  %shr1448 = lshr i32 %911, 27
  %or1449 = or i32 %shl1447, %shr1448
  %912 = load i32, i32* %E, align 4
  %913 = load i32, i32* %A, align 4
  %and1450 = and i32 %912, %913
  %914 = load i32, i32* %B, align 4
  %915 = load i32, i32* %E, align 4
  %916 = load i32, i32* %A, align 4
  %or1451 = or i32 %915, %916
  %and1452 = and i32 %914, %or1451
  %or1453 = or i32 %and1450, %and1452
  %add1454 = add i32 %or1449, %or1453
  %add1455 = add i32 %add1454, -1894007588
  %arrayidx1456 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %917 = load i32, i32* %arrayidx1456, align 8
  %arrayidx1457 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %918 = load i32, i32* %arrayidx1457, align 4
  %xor1458 = xor i32 %917, %918
  %arrayidx1459 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %919 = load i32, i32* %arrayidx1459, align 4
  %xor1460 = xor i32 %xor1458, %919
  %arrayidx1461 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %920 = load i32, i32* %arrayidx1461, align 4
  %xor1462 = xor i32 %xor1460, %920
  store i32 %xor1462, i32* %temp, align 4
  %921 = load i32, i32* %temp, align 4
  %shl1463 = shl i32 %921, 1
  %922 = load i32, i32* %temp, align 4
  %shr1464 = lshr i32 %922, 31
  %or1465 = or i32 %shl1463, %shr1464
  %arrayidx1466 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  store i32 %or1465, i32* %arrayidx1466, align 4
  %add1467 = add i32 %add1455, %or1465
  %923 = load i32, i32* %C, align 4
  %add1468 = add i32 %923, %add1467
  store i32 %add1468, i32* %C, align 4
  %924 = load i32, i32* %E, align 4
  %shl1469 = shl i32 %924, 30
  %925 = load i32, i32* %E, align 4
  %shr1470 = lshr i32 %925, 2
  %or1471 = or i32 %shl1469, %shr1470
  store i32 %or1471, i32* %E, align 4
  %926 = load i32, i32* %C, align 4
  %shl1472 = shl i32 %926, 5
  %927 = load i32, i32* %C, align 4
  %shr1473 = lshr i32 %927, 27
  %or1474 = or i32 %shl1472, %shr1473
  %928 = load i32, i32* %D, align 4
  %929 = load i32, i32* %E, align 4
  %and1475 = and i32 %928, %929
  %930 = load i32, i32* %A, align 4
  %931 = load i32, i32* %D, align 4
  %932 = load i32, i32* %E, align 4
  %or1476 = or i32 %931, %932
  %and1477 = and i32 %930, %or1476
  %or1478 = or i32 %and1475, %and1477
  %add1479 = add i32 %or1474, %or1478
  %add1480 = add i32 %add1479, -1894007588
  %arrayidx1481 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %933 = load i32, i32* %arrayidx1481, align 4
  %arrayidx1482 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %934 = load i32, i32* %arrayidx1482, align 8
  %xor1483 = xor i32 %933, %934
  %arrayidx1484 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %935 = load i32, i32* %arrayidx1484, align 16
  %xor1485 = xor i32 %xor1483, %935
  %arrayidx1486 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %936 = load i32, i32* %arrayidx1486, align 8
  %xor1487 = xor i32 %xor1485, %936
  store i32 %xor1487, i32* %temp, align 4
  %937 = load i32, i32* %temp, align 4
  %shl1488 = shl i32 %937, 1
  %938 = load i32, i32* %temp, align 4
  %shr1489 = lshr i32 %938, 31
  %or1490 = or i32 %shl1488, %shr1489
  %arrayidx1491 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  store i32 %or1490, i32* %arrayidx1491, align 8
  %add1492 = add i32 %add1480, %or1490
  %939 = load i32, i32* %B, align 4
  %add1493 = add i32 %939, %add1492
  store i32 %add1493, i32* %B, align 4
  %940 = load i32, i32* %D, align 4
  %shl1494 = shl i32 %940, 30
  %941 = load i32, i32* %D, align 4
  %shr1495 = lshr i32 %941, 2
  %or1496 = or i32 %shl1494, %shr1495
  store i32 %or1496, i32* %D, align 4
  %942 = load i32, i32* %B, align 4
  %shl1497 = shl i32 %942, 5
  %943 = load i32, i32* %B, align 4
  %shr1498 = lshr i32 %943, 27
  %or1499 = or i32 %shl1497, %shr1498
  %944 = load i32, i32* %C, align 4
  %945 = load i32, i32* %D, align 4
  %and1500 = and i32 %944, %945
  %946 = load i32, i32* %E, align 4
  %947 = load i32, i32* %C, align 4
  %948 = load i32, i32* %D, align 4
  %or1501 = or i32 %947, %948
  %and1502 = and i32 %946, %or1501
  %or1503 = or i32 %and1500, %and1502
  %add1504 = add i32 %or1499, %or1503
  %add1505 = add i32 %add1504, -1894007588
  %arrayidx1506 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %949 = load i32, i32* %arrayidx1506, align 16
  %arrayidx1507 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %950 = load i32, i32* %arrayidx1507, align 4
  %xor1508 = xor i32 %949, %950
  %arrayidx1509 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %951 = load i32, i32* %arrayidx1509, align 4
  %xor1510 = xor i32 %xor1508, %951
  %arrayidx1511 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %952 = load i32, i32* %arrayidx1511, align 4
  %xor1512 = xor i32 %xor1510, %952
  store i32 %xor1512, i32* %temp, align 4
  %953 = load i32, i32* %temp, align 4
  %shl1513 = shl i32 %953, 1
  %954 = load i32, i32* %temp, align 4
  %shr1514 = lshr i32 %954, 31
  %or1515 = or i32 %shl1513, %shr1514
  %arrayidx1516 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  store i32 %or1515, i32* %arrayidx1516, align 4
  %add1517 = add i32 %add1505, %or1515
  %955 = load i32, i32* %A, align 4
  %add1518 = add i32 %955, %add1517
  store i32 %add1518, i32* %A, align 4
  %956 = load i32, i32* %C, align 4
  %shl1519 = shl i32 %956, 30
  %957 = load i32, i32* %C, align 4
  %shr1520 = lshr i32 %957, 2
  %or1521 = or i32 %shl1519, %shr1520
  store i32 %or1521, i32* %C, align 4
  %958 = load i32, i32* %A, align 4
  %shl1522 = shl i32 %958, 5
  %959 = load i32, i32* %A, align 4
  %shr1523 = lshr i32 %959, 27
  %or1524 = or i32 %shl1522, %shr1523
  %960 = load i32, i32* %B, align 4
  %961 = load i32, i32* %C, align 4
  %xor1525 = xor i32 %960, %961
  %962 = load i32, i32* %D, align 4
  %xor1526 = xor i32 %xor1525, %962
  %add1527 = add i32 %or1524, %xor1526
  %add1528 = add i32 %add1527, -899497514
  %arrayidx1529 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %963 = load i32, i32* %arrayidx1529, align 4
  %arrayidx1530 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %964 = load i32, i32* %arrayidx1530, align 16
  %xor1531 = xor i32 %963, %964
  %arrayidx1532 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %965 = load i32, i32* %arrayidx1532, align 8
  %xor1533 = xor i32 %xor1531, %965
  %arrayidx1534 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %966 = load i32, i32* %arrayidx1534, align 16
  %xor1535 = xor i32 %xor1533, %966
  store i32 %xor1535, i32* %temp, align 4
  %967 = load i32, i32* %temp, align 4
  %shl1536 = shl i32 %967, 1
  %968 = load i32, i32* %temp, align 4
  %shr1537 = lshr i32 %968, 31
  %or1538 = or i32 %shl1536, %shr1537
  %arrayidx1539 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  store i32 %or1538, i32* %arrayidx1539, align 16
  %add1540 = add i32 %add1528, %or1538
  %969 = load i32, i32* %E, align 4
  %add1541 = add i32 %969, %add1540
  store i32 %add1541, i32* %E, align 4
  %970 = load i32, i32* %B, align 4
  %shl1542 = shl i32 %970, 30
  %971 = load i32, i32* %B, align 4
  %shr1543 = lshr i32 %971, 2
  %or1544 = or i32 %shl1542, %shr1543
  store i32 %or1544, i32* %B, align 4
  %972 = load i32, i32* %E, align 4
  %shl1545 = shl i32 %972, 5
  %973 = load i32, i32* %E, align 4
  %shr1546 = lshr i32 %973, 27
  %or1547 = or i32 %shl1545, %shr1546
  %974 = load i32, i32* %A, align 4
  %975 = load i32, i32* %B, align 4
  %xor1548 = xor i32 %974, %975
  %976 = load i32, i32* %C, align 4
  %xor1549 = xor i32 %xor1548, %976
  %add1550 = add i32 %or1547, %xor1549
  %add1551 = add i32 %add1550, -899497514
  %arrayidx1552 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %977 = load i32, i32* %arrayidx1552, align 8
  %arrayidx1553 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %978 = load i32, i32* %arrayidx1553, align 4
  %xor1554 = xor i32 %977, %978
  %arrayidx1555 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %979 = load i32, i32* %arrayidx1555, align 4
  %xor1556 = xor i32 %xor1554, %979
  %arrayidx1557 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %980 = load i32, i32* %arrayidx1557, align 4
  %xor1558 = xor i32 %xor1556, %980
  store i32 %xor1558, i32* %temp, align 4
  %981 = load i32, i32* %temp, align 4
  %shl1559 = shl i32 %981, 1
  %982 = load i32, i32* %temp, align 4
  %shr1560 = lshr i32 %982, 31
  %or1561 = or i32 %shl1559, %shr1560
  %arrayidx1562 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  store i32 %or1561, i32* %arrayidx1562, align 4
  %add1563 = add i32 %add1551, %or1561
  %983 = load i32, i32* %D, align 4
  %add1564 = add i32 %983, %add1563
  store i32 %add1564, i32* %D, align 4
  %984 = load i32, i32* %A, align 4
  %shl1565 = shl i32 %984, 30
  %985 = load i32, i32* %A, align 4
  %shr1566 = lshr i32 %985, 2
  %or1567 = or i32 %shl1565, %shr1566
  store i32 %or1567, i32* %A, align 4
  %986 = load i32, i32* %D, align 4
  %shl1568 = shl i32 %986, 5
  %987 = load i32, i32* %D, align 4
  %shr1569 = lshr i32 %987, 27
  %or1570 = or i32 %shl1568, %shr1569
  %988 = load i32, i32* %E, align 4
  %989 = load i32, i32* %A, align 4
  %xor1571 = xor i32 %988, %989
  %990 = load i32, i32* %B, align 4
  %xor1572 = xor i32 %xor1571, %990
  %add1573 = add i32 %or1570, %xor1572
  %add1574 = add i32 %add1573, -899497514
  %arrayidx1575 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %991 = load i32, i32* %arrayidx1575, align 4
  %arrayidx1576 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %992 = load i32, i32* %arrayidx1576, align 8
  %xor1577 = xor i32 %991, %992
  %arrayidx1578 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %993 = load i32, i32* %arrayidx1578, align 16
  %xor1579 = xor i32 %xor1577, %993
  %arrayidx1580 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %994 = load i32, i32* %arrayidx1580, align 8
  %xor1581 = xor i32 %xor1579, %994
  store i32 %xor1581, i32* %temp, align 4
  %995 = load i32, i32* %temp, align 4
  %shl1582 = shl i32 %995, 1
  %996 = load i32, i32* %temp, align 4
  %shr1583 = lshr i32 %996, 31
  %or1584 = or i32 %shl1582, %shr1583
  %arrayidx1585 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  store i32 %or1584, i32* %arrayidx1585, align 8
  %add1586 = add i32 %add1574, %or1584
  %997 = load i32, i32* %C, align 4
  %add1587 = add i32 %997, %add1586
  store i32 %add1587, i32* %C, align 4
  %998 = load i32, i32* %E, align 4
  %shl1588 = shl i32 %998, 30
  %999 = load i32, i32* %E, align 4
  %shr1589 = lshr i32 %999, 2
  %or1590 = or i32 %shl1588, %shr1589
  store i32 %or1590, i32* %E, align 4
  %1000 = load i32, i32* %C, align 4
  %shl1591 = shl i32 %1000, 5
  %1001 = load i32, i32* %C, align 4
  %shr1592 = lshr i32 %1001, 27
  %or1593 = or i32 %shl1591, %shr1592
  %1002 = load i32, i32* %D, align 4
  %1003 = load i32, i32* %E, align 4
  %xor1594 = xor i32 %1002, %1003
  %1004 = load i32, i32* %A, align 4
  %xor1595 = xor i32 %xor1594, %1004
  %add1596 = add i32 %or1593, %xor1595
  %add1597 = add i32 %add1596, -899497514
  %arrayidx1598 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %1005 = load i32, i32* %arrayidx1598, align 16
  %arrayidx1599 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %1006 = load i32, i32* %arrayidx1599, align 4
  %xor1600 = xor i32 %1005, %1006
  %arrayidx1601 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %1007 = load i32, i32* %arrayidx1601, align 4
  %xor1602 = xor i32 %xor1600, %1007
  %arrayidx1603 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %1008 = load i32, i32* %arrayidx1603, align 4
  %xor1604 = xor i32 %xor1602, %1008
  store i32 %xor1604, i32* %temp, align 4
  %1009 = load i32, i32* %temp, align 4
  %shl1605 = shl i32 %1009, 1
  %1010 = load i32, i32* %temp, align 4
  %shr1606 = lshr i32 %1010, 31
  %or1607 = or i32 %shl1605, %shr1606
  %arrayidx1608 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  store i32 %or1607, i32* %arrayidx1608, align 4
  %add1609 = add i32 %add1597, %or1607
  %1011 = load i32, i32* %B, align 4
  %add1610 = add i32 %1011, %add1609
  store i32 %add1610, i32* %B, align 4
  %1012 = load i32, i32* %D, align 4
  %shl1611 = shl i32 %1012, 30
  %1013 = load i32, i32* %D, align 4
  %shr1612 = lshr i32 %1013, 2
  %or1613 = or i32 %shl1611, %shr1612
  store i32 %or1613, i32* %D, align 4
  %1014 = load i32, i32* %B, align 4
  %shl1614 = shl i32 %1014, 5
  %1015 = load i32, i32* %B, align 4
  %shr1615 = lshr i32 %1015, 27
  %or1616 = or i32 %shl1614, %shr1615
  %1016 = load i32, i32* %C, align 4
  %1017 = load i32, i32* %D, align 4
  %xor1617 = xor i32 %1016, %1017
  %1018 = load i32, i32* %E, align 4
  %xor1618 = xor i32 %xor1617, %1018
  %add1619 = add i32 %or1616, %xor1618
  %add1620 = add i32 %add1619, -899497514
  %arrayidx1621 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %1019 = load i32, i32* %arrayidx1621, align 4
  %arrayidx1622 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %1020 = load i32, i32* %arrayidx1622, align 16
  %xor1623 = xor i32 %1019, %1020
  %arrayidx1624 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %1021 = load i32, i32* %arrayidx1624, align 8
  %xor1625 = xor i32 %xor1623, %1021
  %arrayidx1626 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %1022 = load i32, i32* %arrayidx1626, align 16
  %xor1627 = xor i32 %xor1625, %1022
  store i32 %xor1627, i32* %temp, align 4
  %1023 = load i32, i32* %temp, align 4
  %shl1628 = shl i32 %1023, 1
  %1024 = load i32, i32* %temp, align 4
  %shr1629 = lshr i32 %1024, 31
  %or1630 = or i32 %shl1628, %shr1629
  %arrayidx1631 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  store i32 %or1630, i32* %arrayidx1631, align 16
  %add1632 = add i32 %add1620, %or1630
  %1025 = load i32, i32* %A, align 4
  %add1633 = add i32 %1025, %add1632
  store i32 %add1633, i32* %A, align 4
  %1026 = load i32, i32* %C, align 4
  %shl1634 = shl i32 %1026, 30
  %1027 = load i32, i32* %C, align 4
  %shr1635 = lshr i32 %1027, 2
  %or1636 = or i32 %shl1634, %shr1635
  store i32 %or1636, i32* %C, align 4
  %1028 = load i32, i32* %A, align 4
  %shl1637 = shl i32 %1028, 5
  %1029 = load i32, i32* %A, align 4
  %shr1638 = lshr i32 %1029, 27
  %or1639 = or i32 %shl1637, %shr1638
  %1030 = load i32, i32* %B, align 4
  %1031 = load i32, i32* %C, align 4
  %xor1640 = xor i32 %1030, %1031
  %1032 = load i32, i32* %D, align 4
  %xor1641 = xor i32 %xor1640, %1032
  %add1642 = add i32 %or1639, %xor1641
  %add1643 = add i32 %add1642, -899497514
  %arrayidx1644 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %1033 = load i32, i32* %arrayidx1644, align 8
  %arrayidx1645 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %1034 = load i32, i32* %arrayidx1645, align 4
  %xor1646 = xor i32 %1033, %1034
  %arrayidx1647 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %1035 = load i32, i32* %arrayidx1647, align 4
  %xor1648 = xor i32 %xor1646, %1035
  %arrayidx1649 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %1036 = load i32, i32* %arrayidx1649, align 4
  %xor1650 = xor i32 %xor1648, %1036
  store i32 %xor1650, i32* %temp, align 4
  %1037 = load i32, i32* %temp, align 4
  %shl1651 = shl i32 %1037, 1
  %1038 = load i32, i32* %temp, align 4
  %shr1652 = lshr i32 %1038, 31
  %or1653 = or i32 %shl1651, %shr1652
  %arrayidx1654 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  store i32 %or1653, i32* %arrayidx1654, align 4
  %add1655 = add i32 %add1643, %or1653
  %1039 = load i32, i32* %E, align 4
  %add1656 = add i32 %1039, %add1655
  store i32 %add1656, i32* %E, align 4
  %1040 = load i32, i32* %B, align 4
  %shl1657 = shl i32 %1040, 30
  %1041 = load i32, i32* %B, align 4
  %shr1658 = lshr i32 %1041, 2
  %or1659 = or i32 %shl1657, %shr1658
  store i32 %or1659, i32* %B, align 4
  %1042 = load i32, i32* %E, align 4
  %shl1660 = shl i32 %1042, 5
  %1043 = load i32, i32* %E, align 4
  %shr1661 = lshr i32 %1043, 27
  %or1662 = or i32 %shl1660, %shr1661
  %1044 = load i32, i32* %A, align 4
  %1045 = load i32, i32* %B, align 4
  %xor1663 = xor i32 %1044, %1045
  %1046 = load i32, i32* %C, align 4
  %xor1664 = xor i32 %xor1663, %1046
  %add1665 = add i32 %or1662, %xor1664
  %add1666 = add i32 %add1665, -899497514
  %arrayidx1667 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %1047 = load i32, i32* %arrayidx1667, align 4
  %arrayidx1668 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %1048 = load i32, i32* %arrayidx1668, align 8
  %xor1669 = xor i32 %1047, %1048
  %arrayidx1670 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %1049 = load i32, i32* %arrayidx1670, align 16
  %xor1671 = xor i32 %xor1669, %1049
  %arrayidx1672 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %1050 = load i32, i32* %arrayidx1672, align 8
  %xor1673 = xor i32 %xor1671, %1050
  store i32 %xor1673, i32* %temp, align 4
  %1051 = load i32, i32* %temp, align 4
  %shl1674 = shl i32 %1051, 1
  %1052 = load i32, i32* %temp, align 4
  %shr1675 = lshr i32 %1052, 31
  %or1676 = or i32 %shl1674, %shr1675
  %arrayidx1677 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  store i32 %or1676, i32* %arrayidx1677, align 8
  %add1678 = add i32 %add1666, %or1676
  %1053 = load i32, i32* %D, align 4
  %add1679 = add i32 %1053, %add1678
  store i32 %add1679, i32* %D, align 4
  %1054 = load i32, i32* %A, align 4
  %shl1680 = shl i32 %1054, 30
  %1055 = load i32, i32* %A, align 4
  %shr1681 = lshr i32 %1055, 2
  %or1682 = or i32 %shl1680, %shr1681
  store i32 %or1682, i32* %A, align 4
  %1056 = load i32, i32* %D, align 4
  %shl1683 = shl i32 %1056, 5
  %1057 = load i32, i32* %D, align 4
  %shr1684 = lshr i32 %1057, 27
  %or1685 = or i32 %shl1683, %shr1684
  %1058 = load i32, i32* %E, align 4
  %1059 = load i32, i32* %A, align 4
  %xor1686 = xor i32 %1058, %1059
  %1060 = load i32, i32* %B, align 4
  %xor1687 = xor i32 %xor1686, %1060
  %add1688 = add i32 %or1685, %xor1687
  %add1689 = add i32 %add1688, -899497514
  %arrayidx1690 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %1061 = load i32, i32* %arrayidx1690, align 16
  %arrayidx1691 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %1062 = load i32, i32* %arrayidx1691, align 4
  %xor1692 = xor i32 %1061, %1062
  %arrayidx1693 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %1063 = load i32, i32* %arrayidx1693, align 4
  %xor1694 = xor i32 %xor1692, %1063
  %arrayidx1695 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %1064 = load i32, i32* %arrayidx1695, align 4
  %xor1696 = xor i32 %xor1694, %1064
  store i32 %xor1696, i32* %temp, align 4
  %1065 = load i32, i32* %temp, align 4
  %shl1697 = shl i32 %1065, 1
  %1066 = load i32, i32* %temp, align 4
  %shr1698 = lshr i32 %1066, 31
  %or1699 = or i32 %shl1697, %shr1698
  %arrayidx1700 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  store i32 %or1699, i32* %arrayidx1700, align 4
  %add1701 = add i32 %add1689, %or1699
  %1067 = load i32, i32* %C, align 4
  %add1702 = add i32 %1067, %add1701
  store i32 %add1702, i32* %C, align 4
  %1068 = load i32, i32* %E, align 4
  %shl1703 = shl i32 %1068, 30
  %1069 = load i32, i32* %E, align 4
  %shr1704 = lshr i32 %1069, 2
  %or1705 = or i32 %shl1703, %shr1704
  store i32 %or1705, i32* %E, align 4
  %1070 = load i32, i32* %C, align 4
  %shl1706 = shl i32 %1070, 5
  %1071 = load i32, i32* %C, align 4
  %shr1707 = lshr i32 %1071, 27
  %or1708 = or i32 %shl1706, %shr1707
  %1072 = load i32, i32* %D, align 4
  %1073 = load i32, i32* %E, align 4
  %xor1709 = xor i32 %1072, %1073
  %1074 = load i32, i32* %A, align 4
  %xor1710 = xor i32 %xor1709, %1074
  %add1711 = add i32 %or1708, %xor1710
  %add1712 = add i32 %add1711, -899497514
  %arrayidx1713 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %1075 = load i32, i32* %arrayidx1713, align 4
  %arrayidx1714 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %1076 = load i32, i32* %arrayidx1714, align 16
  %xor1715 = xor i32 %1075, %1076
  %arrayidx1716 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %1077 = load i32, i32* %arrayidx1716, align 8
  %xor1717 = xor i32 %xor1715, %1077
  %arrayidx1718 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %1078 = load i32, i32* %arrayidx1718, align 16
  %xor1719 = xor i32 %xor1717, %1078
  store i32 %xor1719, i32* %temp, align 4
  %1079 = load i32, i32* %temp, align 4
  %shl1720 = shl i32 %1079, 1
  %1080 = load i32, i32* %temp, align 4
  %shr1721 = lshr i32 %1080, 31
  %or1722 = or i32 %shl1720, %shr1721
  %arrayidx1723 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  store i32 %or1722, i32* %arrayidx1723, align 16
  %add1724 = add i32 %add1712, %or1722
  %1081 = load i32, i32* %B, align 4
  %add1725 = add i32 %1081, %add1724
  store i32 %add1725, i32* %B, align 4
  %1082 = load i32, i32* %D, align 4
  %shl1726 = shl i32 %1082, 30
  %1083 = load i32, i32* %D, align 4
  %shr1727 = lshr i32 %1083, 2
  %or1728 = or i32 %shl1726, %shr1727
  store i32 %or1728, i32* %D, align 4
  %1084 = load i32, i32* %B, align 4
  %shl1729 = shl i32 %1084, 5
  %1085 = load i32, i32* %B, align 4
  %shr1730 = lshr i32 %1085, 27
  %or1731 = or i32 %shl1729, %shr1730
  %1086 = load i32, i32* %C, align 4
  %1087 = load i32, i32* %D, align 4
  %xor1732 = xor i32 %1086, %1087
  %1088 = load i32, i32* %E, align 4
  %xor1733 = xor i32 %xor1732, %1088
  %add1734 = add i32 %or1731, %xor1733
  %add1735 = add i32 %add1734, -899497514
  %arrayidx1736 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %1089 = load i32, i32* %arrayidx1736, align 8
  %arrayidx1737 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %1090 = load i32, i32* %arrayidx1737, align 4
  %xor1738 = xor i32 %1089, %1090
  %arrayidx1739 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %1091 = load i32, i32* %arrayidx1739, align 4
  %xor1740 = xor i32 %xor1738, %1091
  %arrayidx1741 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %1092 = load i32, i32* %arrayidx1741, align 4
  %xor1742 = xor i32 %xor1740, %1092
  store i32 %xor1742, i32* %temp, align 4
  %1093 = load i32, i32* %temp, align 4
  %shl1743 = shl i32 %1093, 1
  %1094 = load i32, i32* %temp, align 4
  %shr1744 = lshr i32 %1094, 31
  %or1745 = or i32 %shl1743, %shr1744
  %arrayidx1746 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  store i32 %or1745, i32* %arrayidx1746, align 4
  %add1747 = add i32 %add1735, %or1745
  %1095 = load i32, i32* %A, align 4
  %add1748 = add i32 %1095, %add1747
  store i32 %add1748, i32* %A, align 4
  %1096 = load i32, i32* %C, align 4
  %shl1749 = shl i32 %1096, 30
  %1097 = load i32, i32* %C, align 4
  %shr1750 = lshr i32 %1097, 2
  %or1751 = or i32 %shl1749, %shr1750
  store i32 %or1751, i32* %C, align 4
  %1098 = load i32, i32* %A, align 4
  %shl1752 = shl i32 %1098, 5
  %1099 = load i32, i32* %A, align 4
  %shr1753 = lshr i32 %1099, 27
  %or1754 = or i32 %shl1752, %shr1753
  %1100 = load i32, i32* %B, align 4
  %1101 = load i32, i32* %C, align 4
  %xor1755 = xor i32 %1100, %1101
  %1102 = load i32, i32* %D, align 4
  %xor1756 = xor i32 %xor1755, %1102
  %add1757 = add i32 %or1754, %xor1756
  %add1758 = add i32 %add1757, -899497514
  %arrayidx1759 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %1103 = load i32, i32* %arrayidx1759, align 4
  %arrayidx1760 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %1104 = load i32, i32* %arrayidx1760, align 8
  %xor1761 = xor i32 %1103, %1104
  %arrayidx1762 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %1105 = load i32, i32* %arrayidx1762, align 16
  %xor1763 = xor i32 %xor1761, %1105
  %arrayidx1764 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %1106 = load i32, i32* %arrayidx1764, align 8
  %xor1765 = xor i32 %xor1763, %1106
  store i32 %xor1765, i32* %temp, align 4
  %1107 = load i32, i32* %temp, align 4
  %shl1766 = shl i32 %1107, 1
  %1108 = load i32, i32* %temp, align 4
  %shr1767 = lshr i32 %1108, 31
  %or1768 = or i32 %shl1766, %shr1767
  %arrayidx1769 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  store i32 %or1768, i32* %arrayidx1769, align 8
  %add1770 = add i32 %add1758, %or1768
  %1109 = load i32, i32* %E, align 4
  %add1771 = add i32 %1109, %add1770
  store i32 %add1771, i32* %E, align 4
  %1110 = load i32, i32* %B, align 4
  %shl1772 = shl i32 %1110, 30
  %1111 = load i32, i32* %B, align 4
  %shr1773 = lshr i32 %1111, 2
  %or1774 = or i32 %shl1772, %shr1773
  store i32 %or1774, i32* %B, align 4
  %1112 = load i32, i32* %E, align 4
  %shl1775 = shl i32 %1112, 5
  %1113 = load i32, i32* %E, align 4
  %shr1776 = lshr i32 %1113, 27
  %or1777 = or i32 %shl1775, %shr1776
  %1114 = load i32, i32* %A, align 4
  %1115 = load i32, i32* %B, align 4
  %xor1778 = xor i32 %1114, %1115
  %1116 = load i32, i32* %C, align 4
  %xor1779 = xor i32 %xor1778, %1116
  %add1780 = add i32 %or1777, %xor1779
  %add1781 = add i32 %add1780, -899497514
  %arrayidx1782 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %1117 = load i32, i32* %arrayidx1782, align 16
  %arrayidx1783 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %1118 = load i32, i32* %arrayidx1783, align 4
  %xor1784 = xor i32 %1117, %1118
  %arrayidx1785 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %1119 = load i32, i32* %arrayidx1785, align 4
  %xor1786 = xor i32 %xor1784, %1119
  %arrayidx1787 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %1120 = load i32, i32* %arrayidx1787, align 4
  %xor1788 = xor i32 %xor1786, %1120
  store i32 %xor1788, i32* %temp, align 4
  %1121 = load i32, i32* %temp, align 4
  %shl1789 = shl i32 %1121, 1
  %1122 = load i32, i32* %temp, align 4
  %shr1790 = lshr i32 %1122, 31
  %or1791 = or i32 %shl1789, %shr1790
  %arrayidx1792 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  store i32 %or1791, i32* %arrayidx1792, align 4
  %add1793 = add i32 %add1781, %or1791
  %1123 = load i32, i32* %D, align 4
  %add1794 = add i32 %1123, %add1793
  store i32 %add1794, i32* %D, align 4
  %1124 = load i32, i32* %A, align 4
  %shl1795 = shl i32 %1124, 30
  %1125 = load i32, i32* %A, align 4
  %shr1796 = lshr i32 %1125, 2
  %or1797 = or i32 %shl1795, %shr1796
  store i32 %or1797, i32* %A, align 4
  %1126 = load i32, i32* %D, align 4
  %shl1798 = shl i32 %1126, 5
  %1127 = load i32, i32* %D, align 4
  %shr1799 = lshr i32 %1127, 27
  %or1800 = or i32 %shl1798, %shr1799
  %1128 = load i32, i32* %E, align 4
  %1129 = load i32, i32* %A, align 4
  %xor1801 = xor i32 %1128, %1129
  %1130 = load i32, i32* %B, align 4
  %xor1802 = xor i32 %xor1801, %1130
  %add1803 = add i32 %or1800, %xor1802
  %add1804 = add i32 %add1803, -899497514
  %arrayidx1805 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %1131 = load i32, i32* %arrayidx1805, align 4
  %arrayidx1806 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %1132 = load i32, i32* %arrayidx1806, align 16
  %xor1807 = xor i32 %1131, %1132
  %arrayidx1808 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %1133 = load i32, i32* %arrayidx1808, align 8
  %xor1809 = xor i32 %xor1807, %1133
  %arrayidx1810 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %1134 = load i32, i32* %arrayidx1810, align 16
  %xor1811 = xor i32 %xor1809, %1134
  store i32 %xor1811, i32* %temp, align 4
  %1135 = load i32, i32* %temp, align 4
  %shl1812 = shl i32 %1135, 1
  %1136 = load i32, i32* %temp, align 4
  %shr1813 = lshr i32 %1136, 31
  %or1814 = or i32 %shl1812, %shr1813
  %arrayidx1815 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  store i32 %or1814, i32* %arrayidx1815, align 16
  %add1816 = add i32 %add1804, %or1814
  %1137 = load i32, i32* %C, align 4
  %add1817 = add i32 %1137, %add1816
  store i32 %add1817, i32* %C, align 4
  %1138 = load i32, i32* %E, align 4
  %shl1818 = shl i32 %1138, 30
  %1139 = load i32, i32* %E, align 4
  %shr1819 = lshr i32 %1139, 2
  %or1820 = or i32 %shl1818, %shr1819
  store i32 %or1820, i32* %E, align 4
  %1140 = load i32, i32* %C, align 4
  %shl1821 = shl i32 %1140, 5
  %1141 = load i32, i32* %C, align 4
  %shr1822 = lshr i32 %1141, 27
  %or1823 = or i32 %shl1821, %shr1822
  %1142 = load i32, i32* %D, align 4
  %1143 = load i32, i32* %E, align 4
  %xor1824 = xor i32 %1142, %1143
  %1144 = load i32, i32* %A, align 4
  %xor1825 = xor i32 %xor1824, %1144
  %add1826 = add i32 %or1823, %xor1825
  %add1827 = add i32 %add1826, -899497514
  %arrayidx1828 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %1145 = load i32, i32* %arrayidx1828, align 8
  %arrayidx1829 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %1146 = load i32, i32* %arrayidx1829, align 4
  %xor1830 = xor i32 %1145, %1146
  %arrayidx1831 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %1147 = load i32, i32* %arrayidx1831, align 4
  %xor1832 = xor i32 %xor1830, %1147
  %arrayidx1833 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %1148 = load i32, i32* %arrayidx1833, align 4
  %xor1834 = xor i32 %xor1832, %1148
  store i32 %xor1834, i32* %temp, align 4
  %1149 = load i32, i32* %temp, align 4
  %shl1835 = shl i32 %1149, 1
  %1150 = load i32, i32* %temp, align 4
  %shr1836 = lshr i32 %1150, 31
  %or1837 = or i32 %shl1835, %shr1836
  %arrayidx1838 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  store i32 %or1837, i32* %arrayidx1838, align 4
  %add1839 = add i32 %add1827, %or1837
  %1151 = load i32, i32* %B, align 4
  %add1840 = add i32 %1151, %add1839
  store i32 %add1840, i32* %B, align 4
  %1152 = load i32, i32* %D, align 4
  %shl1841 = shl i32 %1152, 30
  %1153 = load i32, i32* %D, align 4
  %shr1842 = lshr i32 %1153, 2
  %or1843 = or i32 %shl1841, %shr1842
  store i32 %or1843, i32* %D, align 4
  %1154 = load i32, i32* %B, align 4
  %shl1844 = shl i32 %1154, 5
  %1155 = load i32, i32* %B, align 4
  %shr1845 = lshr i32 %1155, 27
  %or1846 = or i32 %shl1844, %shr1845
  %1156 = load i32, i32* %C, align 4
  %1157 = load i32, i32* %D, align 4
  %xor1847 = xor i32 %1156, %1157
  %1158 = load i32, i32* %E, align 4
  %xor1848 = xor i32 %xor1847, %1158
  %add1849 = add i32 %or1846, %xor1848
  %add1850 = add i32 %add1849, -899497514
  %arrayidx1851 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %1159 = load i32, i32* %arrayidx1851, align 4
  %arrayidx1852 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 2
  %1160 = load i32, i32* %arrayidx1852, align 8
  %xor1853 = xor i32 %1159, %1160
  %arrayidx1854 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %1161 = load i32, i32* %arrayidx1854, align 16
  %xor1855 = xor i32 %xor1853, %1161
  %arrayidx1856 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %1162 = load i32, i32* %arrayidx1856, align 8
  %xor1857 = xor i32 %xor1855, %1162
  store i32 %xor1857, i32* %temp, align 4
  %1163 = load i32, i32* %temp, align 4
  %shl1858 = shl i32 %1163, 1
  %1164 = load i32, i32* %temp, align 4
  %shr1859 = lshr i32 %1164, 31
  %or1860 = or i32 %shl1858, %shr1859
  %arrayidx1861 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  store i32 %or1860, i32* %arrayidx1861, align 8
  %add1862 = add i32 %add1850, %or1860
  %1165 = load i32, i32* %A, align 4
  %add1863 = add i32 %1165, %add1862
  store i32 %add1863, i32* %A, align 4
  %1166 = load i32, i32* %C, align 4
  %shl1864 = shl i32 %1166, 30
  %1167 = load i32, i32* %C, align 4
  %shr1865 = lshr i32 %1167, 2
  %or1866 = or i32 %shl1864, %shr1865
  store i32 %or1866, i32* %C, align 4
  %1168 = load i32, i32* %A, align 4
  %shl1867 = shl i32 %1168, 5
  %1169 = load i32, i32* %A, align 4
  %shr1868 = lshr i32 %1169, 27
  %or1869 = or i32 %shl1867, %shr1868
  %1170 = load i32, i32* %B, align 4
  %1171 = load i32, i32* %C, align 4
  %xor1870 = xor i32 %1170, %1171
  %1172 = load i32, i32* %D, align 4
  %xor1871 = xor i32 %xor1870, %1172
  %add1872 = add i32 %or1869, %xor1871
  %add1873 = add i32 %add1872, -899497514
  %arrayidx1874 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 8
  %1173 = load i32, i32* %arrayidx1874, align 16
  %arrayidx1875 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 3
  %1174 = load i32, i32* %arrayidx1875, align 4
  %xor1876 = xor i32 %1173, %1174
  %arrayidx1877 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %1175 = load i32, i32* %arrayidx1877, align 4
  %xor1878 = xor i32 %xor1876, %1175
  %arrayidx1879 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %1176 = load i32, i32* %arrayidx1879, align 4
  %xor1880 = xor i32 %xor1878, %1176
  store i32 %xor1880, i32* %temp, align 4
  %1177 = load i32, i32* %temp, align 4
  %shl1881 = shl i32 %1177, 1
  %1178 = load i32, i32* %temp, align 4
  %shr1882 = lshr i32 %1178, 31
  %or1883 = or i32 %shl1881, %shr1882
  %arrayidx1884 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  store i32 %or1883, i32* %arrayidx1884, align 4
  %add1885 = add i32 %add1873, %or1883
  %1179 = load i32, i32* %E, align 4
  %add1886 = add i32 %1179, %add1885
  store i32 %add1886, i32* %E, align 4
  %1180 = load i32, i32* %B, align 4
  %shl1887 = shl i32 %1180, 30
  %1181 = load i32, i32* %B, align 4
  %shr1888 = lshr i32 %1181, 2
  %or1889 = or i32 %shl1887, %shr1888
  store i32 %or1889, i32* %B, align 4
  %1182 = load i32, i32* %E, align 4
  %shl1890 = shl i32 %1182, 5
  %1183 = load i32, i32* %E, align 4
  %shr1891 = lshr i32 %1183, 27
  %or1892 = or i32 %shl1890, %shr1891
  %1184 = load i32, i32* %A, align 4
  %1185 = load i32, i32* %B, align 4
  %xor1893 = xor i32 %1184, %1185
  %1186 = load i32, i32* %C, align 4
  %xor1894 = xor i32 %xor1893, %1186
  %add1895 = add i32 %or1892, %xor1894
  %add1896 = add i32 %add1895, -899497514
  %arrayidx1897 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 9
  %1187 = load i32, i32* %arrayidx1897, align 4
  %arrayidx1898 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 4
  %1188 = load i32, i32* %arrayidx1898, align 16
  %xor1899 = xor i32 %1187, %1188
  %arrayidx1900 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %1189 = load i32, i32* %arrayidx1900, align 8
  %xor1901 = xor i32 %xor1899, %1189
  %arrayidx1902 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %1190 = load i32, i32* %arrayidx1902, align 16
  %xor1903 = xor i32 %xor1901, %1190
  store i32 %xor1903, i32* %temp, align 4
  %1191 = load i32, i32* %temp, align 4
  %shl1904 = shl i32 %1191, 1
  %1192 = load i32, i32* %temp, align 4
  %shr1905 = lshr i32 %1192, 31
  %or1906 = or i32 %shl1904, %shr1905
  %arrayidx1907 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  store i32 %or1906, i32* %arrayidx1907, align 16
  %add1908 = add i32 %add1896, %or1906
  %1193 = load i32, i32* %D, align 4
  %add1909 = add i32 %1193, %add1908
  store i32 %add1909, i32* %D, align 4
  %1194 = load i32, i32* %A, align 4
  %shl1910 = shl i32 %1194, 30
  %1195 = load i32, i32* %A, align 4
  %shr1911 = lshr i32 %1195, 2
  %or1912 = or i32 %shl1910, %shr1911
  store i32 %or1912, i32* %A, align 4
  %1196 = load i32, i32* %D, align 4
  %shl1913 = shl i32 %1196, 5
  %1197 = load i32, i32* %D, align 4
  %shr1914 = lshr i32 %1197, 27
  %or1915 = or i32 %shl1913, %shr1914
  %1198 = load i32, i32* %E, align 4
  %1199 = load i32, i32* %A, align 4
  %xor1916 = xor i32 %1198, %1199
  %1200 = load i32, i32* %B, align 4
  %xor1917 = xor i32 %xor1916, %1200
  %add1918 = add i32 %or1915, %xor1917
  %add1919 = add i32 %add1918, -899497514
  %arrayidx1920 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 10
  %1201 = load i32, i32* %arrayidx1920, align 8
  %arrayidx1921 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 5
  %1202 = load i32, i32* %arrayidx1921, align 4
  %xor1922 = xor i32 %1201, %1202
  %arrayidx1923 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %1203 = load i32, i32* %arrayidx1923, align 4
  %xor1924 = xor i32 %xor1922, %1203
  %arrayidx1925 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  %1204 = load i32, i32* %arrayidx1925, align 4
  %xor1926 = xor i32 %xor1924, %1204
  store i32 %xor1926, i32* %temp, align 4
  %1205 = load i32, i32* %temp, align 4
  %shl1927 = shl i32 %1205, 1
  %1206 = load i32, i32* %temp, align 4
  %shr1928 = lshr i32 %1206, 31
  %or1929 = or i32 %shl1927, %shr1928
  %arrayidx1930 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 13
  store i32 %or1929, i32* %arrayidx1930, align 4
  %add1931 = add i32 %add1919, %or1929
  %1207 = load i32, i32* %C, align 4
  %add1932 = add i32 %1207, %add1931
  store i32 %add1932, i32* %C, align 4
  %1208 = load i32, i32* %E, align 4
  %shl1933 = shl i32 %1208, 30
  %1209 = load i32, i32* %E, align 4
  %shr1934 = lshr i32 %1209, 2
  %or1935 = or i32 %shl1933, %shr1934
  store i32 %or1935, i32* %E, align 4
  %1210 = load i32, i32* %C, align 4
  %shl1936 = shl i32 %1210, 5
  %1211 = load i32, i32* %C, align 4
  %shr1937 = lshr i32 %1211, 27
  %or1938 = or i32 %shl1936, %shr1937
  %1212 = load i32, i32* %D, align 4
  %1213 = load i32, i32* %E, align 4
  %xor1939 = xor i32 %1212, %1213
  %1214 = load i32, i32* %A, align 4
  %xor1940 = xor i32 %xor1939, %1214
  %add1941 = add i32 %or1938, %xor1940
  %add1942 = add i32 %add1941, -899497514
  %arrayidx1943 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 11
  %1215 = load i32, i32* %arrayidx1943, align 4
  %arrayidx1944 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 6
  %1216 = load i32, i32* %arrayidx1944, align 8
  %xor1945 = xor i32 %1215, %1216
  %arrayidx1946 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 0
  %1217 = load i32, i32* %arrayidx1946, align 16
  %xor1947 = xor i32 %xor1945, %1217
  %arrayidx1948 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  %1218 = load i32, i32* %arrayidx1948, align 8
  %xor1949 = xor i32 %xor1947, %1218
  store i32 %xor1949, i32* %temp, align 4
  %1219 = load i32, i32* %temp, align 4
  %shl1950 = shl i32 %1219, 1
  %1220 = load i32, i32* %temp, align 4
  %shr1951 = lshr i32 %1220, 31
  %or1952 = or i32 %shl1950, %shr1951
  %arrayidx1953 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 14
  store i32 %or1952, i32* %arrayidx1953, align 8
  %add1954 = add i32 %add1942, %or1952
  %1221 = load i32, i32* %B, align 4
  %add1955 = add i32 %1221, %add1954
  store i32 %add1955, i32* %B, align 4
  %1222 = load i32, i32* %D, align 4
  %shl1956 = shl i32 %1222, 30
  %1223 = load i32, i32* %D, align 4
  %shr1957 = lshr i32 %1223, 2
  %or1958 = or i32 %shl1956, %shr1957
  store i32 %or1958, i32* %D, align 4
  %1224 = load i32, i32* %B, align 4
  %shl1959 = shl i32 %1224, 5
  %1225 = load i32, i32* %B, align 4
  %shr1960 = lshr i32 %1225, 27
  %or1961 = or i32 %shl1959, %shr1960
  %1226 = load i32, i32* %C, align 4
  %1227 = load i32, i32* %D, align 4
  %xor1962 = xor i32 %1226, %1227
  %1228 = load i32, i32* %E, align 4
  %xor1963 = xor i32 %xor1962, %1228
  %add1964 = add i32 %or1961, %xor1963
  %add1965 = add i32 %add1964, -899497514
  %arrayidx1966 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 12
  %1229 = load i32, i32* %arrayidx1966, align 16
  %arrayidx1967 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 7
  %1230 = load i32, i32* %arrayidx1967, align 4
  %xor1968 = xor i32 %1229, %1230
  %arrayidx1969 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 1
  %1231 = load i32, i32* %arrayidx1969, align 4
  %xor1970 = xor i32 %xor1968, %1231
  %arrayidx1971 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  %1232 = load i32, i32* %arrayidx1971, align 4
  %xor1972 = xor i32 %xor1970, %1232
  store i32 %xor1972, i32* %temp, align 4
  %1233 = load i32, i32* %temp, align 4
  %shl1973 = shl i32 %1233, 1
  %1234 = load i32, i32* %temp, align 4
  %shr1974 = lshr i32 %1234, 31
  %or1975 = or i32 %shl1973, %shr1974
  %arrayidx1976 = getelementptr inbounds [16 x i32], [16 x i32]* %W, i32 0, i32 15
  store i32 %or1975, i32* %arrayidx1976, align 4
  %add1977 = add i32 %add1965, %or1975
  %1235 = load i32, i32* %A, align 4
  %add1978 = add i32 %1235, %add1977
  store i32 %add1978, i32* %A, align 4
  %1236 = load i32, i32* %C, align 4
  %shl1979 = shl i32 %1236, 30
  %1237 = load i32, i32* %C, align 4
  %shr1980 = lshr i32 %1237, 2
  %or1981 = or i32 %shl1979, %shr1980
  store i32 %or1981, i32* %C, align 4
  %1238 = load i32, i32* %A, align 4
  %1239 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state1982 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1239, i32 0, i32 1
  %arrayidx1983 = getelementptr inbounds [5 x i32], [5 x i32]* %state1982, i32 0, i32 0
  %1240 = load i32, i32* %arrayidx1983, align 4
  %add1984 = add i32 %1240, %1238
  store i32 %add1984, i32* %arrayidx1983, align 4
  %1241 = load i32, i32* %B, align 4
  %1242 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state1985 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1242, i32 0, i32 1
  %arrayidx1986 = getelementptr inbounds [5 x i32], [5 x i32]* %state1985, i32 0, i32 1
  %1243 = load i32, i32* %arrayidx1986, align 4
  %add1987 = add i32 %1243, %1241
  store i32 %add1987, i32* %arrayidx1986, align 4
  %1244 = load i32, i32* %C, align 4
  %1245 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state1988 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1245, i32 0, i32 1
  %arrayidx1989 = getelementptr inbounds [5 x i32], [5 x i32]* %state1988, i32 0, i32 2
  %1246 = load i32, i32* %arrayidx1989, align 4
  %add1990 = add i32 %1246, %1244
  store i32 %add1990, i32* %arrayidx1989, align 4
  %1247 = load i32, i32* %D, align 4
  %1248 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state1991 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1248, i32 0, i32 1
  %arrayidx1992 = getelementptr inbounds [5 x i32], [5 x i32]* %state1991, i32 0, i32 3
  %1249 = load i32, i32* %arrayidx1992, align 4
  %add1993 = add i32 %1249, %1247
  store i32 %add1993, i32* %arrayidx1992, align 4
  %1250 = load i32, i32* %E, align 4
  %1251 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state1994 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1251, i32 0, i32 1
  %arrayidx1995 = getelementptr inbounds [5 x i32], [5 x i32]* %state1994, i32 0, i32 4
  %1252 = load i32, i32* %arrayidx1995, align 4
  %add1996 = add i32 %1252, %1250
  store i32 %add1996, i32* %arrayidx1995, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @sha1_update(%struct.sha1_context* %ctx, i8* %input, i32 %length) #0 {
entry:
  %ctx.addr = alloca %struct.sha1_context*, align 4
  %input.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  store %struct.sha1_context* %ctx, %struct.sha1_context** %ctx.addr, align 4
  store i8* %input, i8** %input.addr, align 4
  store i32 %length, i32* %length.addr, align 4
  %0 = load i32, i32* %length.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end27

if.end:                                           ; preds = %entry
  %1 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %total, i32 0, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  %and = and i32 %2, 63
  store i32 %and, i32* %left, align 4
  %3 = load i32, i32* %left, align 4
  %sub = sub i32 64, %3
  store i32 %sub, i32* %fill, align 4
  %4 = load i32, i32* %length.addr, align 4
  %5 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total1 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %5, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %total1, i32 0, i32 0
  %6 = load i32, i32* %arrayidx2, align 4
  %add = add i32 %6, %4
  store i32 %add, i32* %arrayidx2, align 4
  %7 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total3 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %7, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %total3, i32 0, i32 0
  %8 = load i32, i32* %arrayidx4, align 4
  store i32 %8, i32* %arrayidx4, align 4
  %9 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total5 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %9, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %total5, i32 0, i32 0
  %10 = load i32, i32* %arrayidx6, align 4
  %11 = load i32, i32* %length.addr, align 4
  %cmp = icmp ult i32 %10, %11
  br i1 %cmp, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end
  %12 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total8 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %12, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %total8, i32 0, i32 1
  %13 = load i32, i32* %arrayidx9, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %arrayidx9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.end
  %14 = load i32, i32* %left, align 4
  %tobool11 = icmp ne i32 %14, 0
  br i1 %tobool11, label %land.lhs.true, label %if.end18

land.lhs.true:                                    ; preds = %if.end10
  %15 = load i32, i32* %length.addr, align 4
  %16 = load i32, i32* %fill, align 4
  %cmp12 = icmp uge i32 %15, %16
  br i1 %cmp12, label %if.then13, label %if.end18

if.then13:                                        ; preds = %land.lhs.true
  %17 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %buffer = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %17, i32 0, i32 2
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %18 = load i32, i32* %left, align 4
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 %18
  %19 = load i8*, i8** %input.addr, align 4
  %20 = load i32, i32* %fill, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %19, i32 %20, i1 false)
  %21 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %22 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %buffer14 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %22, i32 0, i32 2
  %arraydecay15 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer14, i32 0, i32 0
  call void @sha1_process(%struct.sha1_context* %21, i8* %arraydecay15)
  %23 = load i32, i32* %fill, align 4
  %24 = load i32, i32* %length.addr, align 4
  %sub16 = sub i32 %24, %23
  store i32 %sub16, i32* %length.addr, align 4
  %25 = load i32, i32* %fill, align 4
  %26 = load i8*, i8** %input.addr, align 4
  %add.ptr17 = getelementptr inbounds i8, i8* %26, i32 %25
  store i8* %add.ptr17, i8** %input.addr, align 4
  store i32 0, i32* %left, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then13, %land.lhs.true, %if.end10
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end18
  %27 = load i32, i32* %length.addr, align 4
  %cmp19 = icmp uge i32 %27, 64
  br i1 %cmp19, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %29 = load i8*, i8** %input.addr, align 4
  call void @sha1_process(%struct.sha1_context* %28, i8* %29)
  %30 = load i32, i32* %length.addr, align 4
  %sub20 = sub i32 %30, 64
  store i32 %sub20, i32* %length.addr, align 4
  %31 = load i8*, i8** %input.addr, align 4
  %add.ptr21 = getelementptr inbounds i8, i8* %31, i32 64
  store i8* %add.ptr21, i8** %input.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %32 = load i32, i32* %length.addr, align 4
  %tobool22 = icmp ne i32 %32, 0
  br i1 %tobool22, label %if.then23, label %if.end27

if.then23:                                        ; preds = %while.end
  %33 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %buffer24 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %33, i32 0, i32 2
  %arraydecay25 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer24, i32 0, i32 0
  %34 = load i32, i32* %left, align 4
  %add.ptr26 = getelementptr inbounds i8, i8* %arraydecay25, i32 %34
  %35 = load i8*, i8** %input.addr, align 4
  %36 = load i32, i32* %length.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr26, i8* align 1 %35, i32 %36, i1 false)
  br label %if.end27

if.end27:                                         ; preds = %if.then, %if.then23, %while.end
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define hidden void @sha1_finish(%struct.sha1_context* %ctx, i8* %digest) #0 {
entry:
  %ctx.addr = alloca %struct.sha1_context*, align 4
  %digest.addr = alloca i8*, align 4
  %last = alloca i32, align 4
  %padn = alloca i32, align 4
  %high = alloca i32, align 4
  %low = alloca i32, align 4
  %msglen = alloca [8 x i8], align 1
  store %struct.sha1_context* %ctx, %struct.sha1_context** %ctx.addr, align 4
  store i8* %digest, i8** %digest.addr, align 4
  %0 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %total, i32 0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %shr = lshr i32 %1, 29
  %2 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total1 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %total1, i32 0, i32 1
  %3 = load i32, i32* %arrayidx2, align 4
  %shl = shl i32 %3, 3
  %or = or i32 %shr, %shl
  store i32 %or, i32* %high, align 4
  %4 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total3 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [2 x i32], [2 x i32]* %total3, i32 0, i32 0
  %5 = load i32, i32* %arrayidx4, align 4
  %shl5 = shl i32 %5, 3
  store i32 %shl5, i32* %low, align 4
  %6 = load i32, i32* %high, align 4
  %shr6 = lshr i32 %6, 24
  %conv = trunc i32 %shr6 to i8
  %arrayidx7 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 0
  store i8 %conv, i8* %arrayidx7, align 1
  %7 = load i32, i32* %high, align 4
  %shr8 = lshr i32 %7, 16
  %conv9 = trunc i32 %shr8 to i8
  %arrayidx10 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 1
  store i8 %conv9, i8* %arrayidx10, align 1
  %8 = load i32, i32* %high, align 4
  %shr11 = lshr i32 %8, 8
  %conv12 = trunc i32 %shr11 to i8
  %arrayidx13 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 2
  store i8 %conv12, i8* %arrayidx13, align 1
  %9 = load i32, i32* %high, align 4
  %conv14 = trunc i32 %9 to i8
  %arrayidx15 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 3
  store i8 %conv14, i8* %arrayidx15, align 1
  %10 = load i32, i32* %low, align 4
  %shr16 = lshr i32 %10, 24
  %conv17 = trunc i32 %shr16 to i8
  %arrayidx18 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 4
  store i8 %conv17, i8* %arrayidx18, align 1
  %11 = load i32, i32* %low, align 4
  %shr19 = lshr i32 %11, 16
  %conv20 = trunc i32 %shr19 to i8
  %arrayidx21 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 5
  store i8 %conv20, i8* %arrayidx21, align 1
  %12 = load i32, i32* %low, align 4
  %shr22 = lshr i32 %12, 8
  %conv23 = trunc i32 %shr22 to i8
  %arrayidx24 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 6
  store i8 %conv23, i8* %arrayidx24, align 1
  %13 = load i32, i32* %low, align 4
  %conv25 = trunc i32 %13 to i8
  %arrayidx26 = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 7
  store i8 %conv25, i8* %arrayidx26, align 1
  %14 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %total27 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %14, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %total27, i32 0, i32 0
  %15 = load i32, i32* %arrayidx28, align 4
  %and = and i32 %15, 63
  store i32 %and, i32* %last, align 4
  %16 = load i32, i32* %last, align 4
  %cmp = icmp ult i32 %16, 56
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %17 = load i32, i32* %last, align 4
  %sub = sub i32 56, %17
  br label %cond.end

cond.false:                                       ; preds = %entry
  %18 = load i32, i32* %last, align 4
  %sub30 = sub i32 120, %18
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %sub30, %cond.false ]
  store i32 %cond, i32* %padn, align 4
  %19 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %20 = load i32, i32* %padn, align 4
  call void @sha1_update(%struct.sha1_context* %19, i8* getelementptr inbounds ([64 x i8], [64 x i8]* bitcast (<{ i8, [63 x i8] }>* @sha1_padding to [64 x i8]*), i32 0, i32 0), i32 %20)
  %21 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %msglen, i32 0, i32 0
  call void @sha1_update(%struct.sha1_context* %21, i8* %arraydecay, i32 8)
  %22 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %22, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [5 x i32], [5 x i32]* %state, i32 0, i32 0
  %23 = load i32, i32* %arrayidx31, align 4
  %shr32 = lshr i32 %23, 24
  %conv33 = trunc i32 %shr32 to i8
  %24 = load i8*, i8** %digest.addr, align 4
  %arrayidx34 = getelementptr inbounds i8, i8* %24, i32 0
  store i8 %conv33, i8* %arrayidx34, align 1
  %25 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state35 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %25, i32 0, i32 1
  %arrayidx36 = getelementptr inbounds [5 x i32], [5 x i32]* %state35, i32 0, i32 0
  %26 = load i32, i32* %arrayidx36, align 4
  %shr37 = lshr i32 %26, 16
  %conv38 = trunc i32 %shr37 to i8
  %27 = load i8*, i8** %digest.addr, align 4
  %arrayidx39 = getelementptr inbounds i8, i8* %27, i32 1
  store i8 %conv38, i8* %arrayidx39, align 1
  %28 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state40 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %28, i32 0, i32 1
  %arrayidx41 = getelementptr inbounds [5 x i32], [5 x i32]* %state40, i32 0, i32 0
  %29 = load i32, i32* %arrayidx41, align 4
  %shr42 = lshr i32 %29, 8
  %conv43 = trunc i32 %shr42 to i8
  %30 = load i8*, i8** %digest.addr, align 4
  %arrayidx44 = getelementptr inbounds i8, i8* %30, i32 2
  store i8 %conv43, i8* %arrayidx44, align 1
  %31 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state45 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %31, i32 0, i32 1
  %arrayidx46 = getelementptr inbounds [5 x i32], [5 x i32]* %state45, i32 0, i32 0
  %32 = load i32, i32* %arrayidx46, align 4
  %conv47 = trunc i32 %32 to i8
  %33 = load i8*, i8** %digest.addr, align 4
  %arrayidx48 = getelementptr inbounds i8, i8* %33, i32 3
  store i8 %conv47, i8* %arrayidx48, align 1
  %34 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state49 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %34, i32 0, i32 1
  %arrayidx50 = getelementptr inbounds [5 x i32], [5 x i32]* %state49, i32 0, i32 1
  %35 = load i32, i32* %arrayidx50, align 4
  %shr51 = lshr i32 %35, 24
  %conv52 = trunc i32 %shr51 to i8
  %36 = load i8*, i8** %digest.addr, align 4
  %arrayidx53 = getelementptr inbounds i8, i8* %36, i32 4
  store i8 %conv52, i8* %arrayidx53, align 1
  %37 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state54 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %37, i32 0, i32 1
  %arrayidx55 = getelementptr inbounds [5 x i32], [5 x i32]* %state54, i32 0, i32 1
  %38 = load i32, i32* %arrayidx55, align 4
  %shr56 = lshr i32 %38, 16
  %conv57 = trunc i32 %shr56 to i8
  %39 = load i8*, i8** %digest.addr, align 4
  %arrayidx58 = getelementptr inbounds i8, i8* %39, i32 5
  store i8 %conv57, i8* %arrayidx58, align 1
  %40 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state59 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %40, i32 0, i32 1
  %arrayidx60 = getelementptr inbounds [5 x i32], [5 x i32]* %state59, i32 0, i32 1
  %41 = load i32, i32* %arrayidx60, align 4
  %shr61 = lshr i32 %41, 8
  %conv62 = trunc i32 %shr61 to i8
  %42 = load i8*, i8** %digest.addr, align 4
  %arrayidx63 = getelementptr inbounds i8, i8* %42, i32 6
  store i8 %conv62, i8* %arrayidx63, align 1
  %43 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state64 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %43, i32 0, i32 1
  %arrayidx65 = getelementptr inbounds [5 x i32], [5 x i32]* %state64, i32 0, i32 1
  %44 = load i32, i32* %arrayidx65, align 4
  %conv66 = trunc i32 %44 to i8
  %45 = load i8*, i8** %digest.addr, align 4
  %arrayidx67 = getelementptr inbounds i8, i8* %45, i32 7
  store i8 %conv66, i8* %arrayidx67, align 1
  %46 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state68 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %46, i32 0, i32 1
  %arrayidx69 = getelementptr inbounds [5 x i32], [5 x i32]* %state68, i32 0, i32 2
  %47 = load i32, i32* %arrayidx69, align 4
  %shr70 = lshr i32 %47, 24
  %conv71 = trunc i32 %shr70 to i8
  %48 = load i8*, i8** %digest.addr, align 4
  %arrayidx72 = getelementptr inbounds i8, i8* %48, i32 8
  store i8 %conv71, i8* %arrayidx72, align 1
  %49 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state73 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %49, i32 0, i32 1
  %arrayidx74 = getelementptr inbounds [5 x i32], [5 x i32]* %state73, i32 0, i32 2
  %50 = load i32, i32* %arrayidx74, align 4
  %shr75 = lshr i32 %50, 16
  %conv76 = trunc i32 %shr75 to i8
  %51 = load i8*, i8** %digest.addr, align 4
  %arrayidx77 = getelementptr inbounds i8, i8* %51, i32 9
  store i8 %conv76, i8* %arrayidx77, align 1
  %52 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state78 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %52, i32 0, i32 1
  %arrayidx79 = getelementptr inbounds [5 x i32], [5 x i32]* %state78, i32 0, i32 2
  %53 = load i32, i32* %arrayidx79, align 4
  %shr80 = lshr i32 %53, 8
  %conv81 = trunc i32 %shr80 to i8
  %54 = load i8*, i8** %digest.addr, align 4
  %arrayidx82 = getelementptr inbounds i8, i8* %54, i32 10
  store i8 %conv81, i8* %arrayidx82, align 1
  %55 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state83 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %55, i32 0, i32 1
  %arrayidx84 = getelementptr inbounds [5 x i32], [5 x i32]* %state83, i32 0, i32 2
  %56 = load i32, i32* %arrayidx84, align 4
  %conv85 = trunc i32 %56 to i8
  %57 = load i8*, i8** %digest.addr, align 4
  %arrayidx86 = getelementptr inbounds i8, i8* %57, i32 11
  store i8 %conv85, i8* %arrayidx86, align 1
  %58 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state87 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %58, i32 0, i32 1
  %arrayidx88 = getelementptr inbounds [5 x i32], [5 x i32]* %state87, i32 0, i32 3
  %59 = load i32, i32* %arrayidx88, align 4
  %shr89 = lshr i32 %59, 24
  %conv90 = trunc i32 %shr89 to i8
  %60 = load i8*, i8** %digest.addr, align 4
  %arrayidx91 = getelementptr inbounds i8, i8* %60, i32 12
  store i8 %conv90, i8* %arrayidx91, align 1
  %61 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state92 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %61, i32 0, i32 1
  %arrayidx93 = getelementptr inbounds [5 x i32], [5 x i32]* %state92, i32 0, i32 3
  %62 = load i32, i32* %arrayidx93, align 4
  %shr94 = lshr i32 %62, 16
  %conv95 = trunc i32 %shr94 to i8
  %63 = load i8*, i8** %digest.addr, align 4
  %arrayidx96 = getelementptr inbounds i8, i8* %63, i32 13
  store i8 %conv95, i8* %arrayidx96, align 1
  %64 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state97 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %64, i32 0, i32 1
  %arrayidx98 = getelementptr inbounds [5 x i32], [5 x i32]* %state97, i32 0, i32 3
  %65 = load i32, i32* %arrayidx98, align 4
  %shr99 = lshr i32 %65, 8
  %conv100 = trunc i32 %shr99 to i8
  %66 = load i8*, i8** %digest.addr, align 4
  %arrayidx101 = getelementptr inbounds i8, i8* %66, i32 14
  store i8 %conv100, i8* %arrayidx101, align 1
  %67 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state102 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %67, i32 0, i32 1
  %arrayidx103 = getelementptr inbounds [5 x i32], [5 x i32]* %state102, i32 0, i32 3
  %68 = load i32, i32* %arrayidx103, align 4
  %conv104 = trunc i32 %68 to i8
  %69 = load i8*, i8** %digest.addr, align 4
  %arrayidx105 = getelementptr inbounds i8, i8* %69, i32 15
  store i8 %conv104, i8* %arrayidx105, align 1
  %70 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state106 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %70, i32 0, i32 1
  %arrayidx107 = getelementptr inbounds [5 x i32], [5 x i32]* %state106, i32 0, i32 4
  %71 = load i32, i32* %arrayidx107, align 4
  %shr108 = lshr i32 %71, 24
  %conv109 = trunc i32 %shr108 to i8
  %72 = load i8*, i8** %digest.addr, align 4
  %arrayidx110 = getelementptr inbounds i8, i8* %72, i32 16
  store i8 %conv109, i8* %arrayidx110, align 1
  %73 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state111 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %73, i32 0, i32 1
  %arrayidx112 = getelementptr inbounds [5 x i32], [5 x i32]* %state111, i32 0, i32 4
  %74 = load i32, i32* %arrayidx112, align 4
  %shr113 = lshr i32 %74, 16
  %conv114 = trunc i32 %shr113 to i8
  %75 = load i8*, i8** %digest.addr, align 4
  %arrayidx115 = getelementptr inbounds i8, i8* %75, i32 17
  store i8 %conv114, i8* %arrayidx115, align 1
  %76 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state116 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %76, i32 0, i32 1
  %arrayidx117 = getelementptr inbounds [5 x i32], [5 x i32]* %state116, i32 0, i32 4
  %77 = load i32, i32* %arrayidx117, align 4
  %shr118 = lshr i32 %77, 8
  %conv119 = trunc i32 %shr118 to i8
  %78 = load i8*, i8** %digest.addr, align 4
  %arrayidx120 = getelementptr inbounds i8, i8* %78, i32 18
  store i8 %conv119, i8* %arrayidx120, align 1
  %79 = load %struct.sha1_context*, %struct.sha1_context** %ctx.addr, align 4
  %state121 = getelementptr inbounds %struct.sha1_context, %struct.sha1_context* %79, i32 0, i32 1
  %arrayidx122 = getelementptr inbounds [5 x i32], [5 x i32]* %state121, i32 0, i32 4
  %80 = load i32, i32* %arrayidx122, align 4
  %conv123 = trunc i32 %80 to i8
  %81 = load i8*, i8** %digest.addr, align 4
  %arrayidx124 = getelementptr inbounds i8, i8* %81, i32 19
  store i8 %conv123, i8* %arrayidx124, align 1
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
