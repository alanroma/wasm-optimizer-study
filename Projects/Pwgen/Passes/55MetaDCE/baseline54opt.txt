[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    7.929e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000284164 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.0327e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000617621 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00123214 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 3.3941e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00521453 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00578078 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000661012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00286467 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00115438 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00311418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000582823 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00184573 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0109964 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00588361 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00241873 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00299618 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00376911 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00908242 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00419474 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000938052 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00379462 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000927856 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00406155 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00223536 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00331232 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000505141 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00210118 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00224289 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00233165 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00280105 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00395696 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0272475 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0151796 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000737985 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.2628e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000738887 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00056576 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      3.218e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000444072 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.003769 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-debug...                    0.00133648 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-producers...                3.667e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.141993 seconds.
[PassRunner] (final validation)
