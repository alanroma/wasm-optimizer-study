[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    7.647e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000382364 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.0067e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000607553 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00130975 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 3.3824e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00520589 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00575199 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000642919 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00285521 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00117669 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00312707 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000604127 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00184973 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0110454 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00584713 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00242076 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00299878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00369862 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0090482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00423123 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000961571 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00363108 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000944629 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00407191 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00222909 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00329678 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000504314 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00208799 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00223901 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00231901 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00279743 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00395799 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0271588 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0157421 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000741981 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.3481e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.135552 seconds.
[PassRunner] (final validation)
