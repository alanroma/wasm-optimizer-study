[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    7.377e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000273674 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    9.823e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000593896 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00120504 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 3.3553e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00516975 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00581934 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000645695 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0028506 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00116765 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00314517 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.000581272 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00184243 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0110041 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00583036 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00241989 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00302348 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00371012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00905825 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00418796 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000935651 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00358492 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000927027 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00404709 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00221562 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00335299 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000530145 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00208808 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00227641 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00232714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00282079 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0039359 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0916212 seconds.
[PassRunner] (final validation)
