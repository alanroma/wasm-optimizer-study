Note: This option follows the *-propagate trail
33 -> 37
--merge-blocks --remove-unused-brs --remove-unused-names --merge-blocks
wasm-opt baseline33.wasm -o baseline37.wasm --mvp-features --pass-arg=stack-pointer@5246656 --pass-arg=emscripten-sbrk-ptr@3616 2> baseline37opt.txt

39
--precompute-propagate
wasm-opt baseline37.wasm -o c39.wasm --precompute-propagate --mvp-features --pass-arg=stack-pointer@5246656 --pass-arg=emscripten-sbrk-ptr@3616 2> c39opt.txt
