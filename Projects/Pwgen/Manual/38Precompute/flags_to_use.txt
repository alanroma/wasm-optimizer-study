20b18 -> 37
--simplify-locals-nostructure --vacuum --reorder-locals --remove-unused-brs --coalesce-locals --simplify-locals --vacuum --reorder-locals --coalesce-locals --reorder-locals --vacuum --merge-blocks --remove-unused-brs --remove-unused-names --merge-blocks

wasm-opt baseline20b18.wasm -o baseline37.wasm --simplify-locals-nostructure --vacuum --reorder-locals --remove-unused-brs --coalesce-locals --simplify-locals --vacuum --reorder-locals --coalesce-locals --reorder-locals --vacuum --merge-blocks --remove-unused-brs --remove-unused-names --merge-blocks --mvp-features --pass-arg=stack-pointer@5246656 --pass-arg=emscripten-sbrk-ptr@3616 2> baseline37opt.txt

38
--precompute

wasm-opt baseline37.wasm -o c38.wasm --precompute --mvp-features --pass-arg=stack-pointer@5246656 --pass-arg=emscripten-sbrk-ptr@3616 2> c38opt.txt
