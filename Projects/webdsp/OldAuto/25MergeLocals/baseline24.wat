(module
  (type (;0;) (func (param i32) (result i32)))
  (type (;1;) (func (result i32)))
  (type (;2;) (func (param i32)))
  (type (;3;) (func))
  (type (;4;) (func (param i32 i32)))
  (type (;5;) (func (param i32) (result f64)))
  (type (;6;) (func (param i32 i32 i32)))
  (type (;7;) (func (param i32 i32 i32 i32)))
  (type (;8;) (func (param i32 i32 i32 i32 i32 i32 i32)))
  (type (;9;) (func (param i32 i32 i32 i32 i32 i32 f32 f32 i32)))
  (type (;10;) (func (param i32 i32 i32) (result i32)))
  (type (;11;) (func (param i32 i32 i32 i32 i32) (result i32)))
  (type (;12;) (func (param i32 i64 i32) (result i64)))
  (import "env" "emscripten_resize_heap" (func (;0;) (type 0)))
  (import "env" "__handle_stack_overflow" (func (;1;) (type 3)))
  (import "env" "memory" (memory (;0;) 256 32768))
  (import "env" "table" (table (;0;) 1 funcref))
  (func (;2;) (type 1) (result i32)
    i32.const 1552)
  (func (;3;) (type 3)
    nop)
  (func (;4;) (type 11) (param i32 i32 i32 i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 5
    local.get 0
    i32.store offset=24
    local.get 5
    local.get 1
    i32.store offset=20
    local.get 5
    local.get 2
    i32.store offset=16
    local.get 5
    local.get 3
    i32.store offset=12
    local.get 5
    local.get 4
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        local.get 5
        i32.load offset=24
        i32.const 0
        i32.lt_s
        i32.const 1
        i32.and
        i32.eqz
        if  ;; label = @3
          local.get 5
          i32.load offset=20
          i32.const 0
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 5
        i32.const 0
        i32.store offset=28
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 5
        i32.load offset=24
        local.get 5
        i32.load offset=12
        i32.ge_s
        i32.const 1
        i32.and
        i32.eqz
        if  ;; label = @3
          local.get 5
          i32.load offset=20
          local.get 5
          i32.load offset=8
          i32.ge_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 5
        i32.const 0
        i32.store offset=28
        br 1 (;@1;)
      end
      local.get 5
      local.get 5
      i32.load offset=16
      local.get 5
      i32.load offset=12
      local.get 5
      i32.load offset=20
      i32.mul
      local.get 5
      i32.load offset=24
      i32.add
      i32.const 2
      i32.shl
      i32.add
      i32.load
      i32.store offset=28
    end
    local.get 5
    i32.load offset=28)
  (func (;5;) (type 4) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    local.get 0
    i32.store offset=28
    local.get 2
    local.get 1
    i32.store offset=24
    local.get 2
    i32.const 0
    i32.store offset=20
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load offset=20
          local.get 2
          i32.load offset=24
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.store offset=16
          local.get 2
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 1
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.store offset=12
          local.get 2
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 2
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.store offset=8
          local.get 2
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 3
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.store offset=4
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.add
          local.get 2
          i32.load offset=16
          i32.store8
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 1
          i32.add
          i32.add
          local.get 2
          i32.load offset=16
          i32.store8
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 2
          i32.add
          i32.add
          local.get 2
          i32.load offset=16
          i32.store8
          local.get 2
          i32.load offset=28
          local.get 2
          i32.load offset=20
          i32.const 3
          i32.add
          i32.add
          local.get 2
          i32.load offset=4
          i32.store8
          local.get 2
          local.get 2
          i32.load offset=20
          i32.const 4
          i32.add
          i32.store offset=20
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end)
  (func (;6;) (type 6) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    local.get 0
    i32.store offset=12
    local.get 3
    local.get 1
    i32.store offset=8
    local.get 3
    local.get 2
    i32.store offset=4
    local.get 3
    i32.const 0
    i32.store
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 3
          i32.load
          local.get 3
          i32.load offset=8
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          local.get 3
          i32.load offset=4
          i32.add
          i32.const 255
          i32.gt_s
          i32.const 1
          i32.and
          i32.eqz
          if  ;; label = @4
            nop
            local.get 3
            i32.load offset=4
            local.get 3
            i32.load offset=12
            local.get 3
            i32.load
            i32.add
            local.tee 4
            i32.load8_u
            i32.const 255
            i32.and
            i32.add
            local.set 5
            local.get 4
            local.get 5
            i32.store8
          end
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.const 1
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          local.get 3
          i32.load offset=4
          i32.add
          i32.const 255
          i32.gt_s
          i32.const 1
          i32.and
          i32.eqz
          if  ;; label = @4
            nop
            local.get 3
            i32.load offset=4
            local.get 3
            i32.load offset=12
            local.get 3
            i32.load
            i32.const 1
            i32.add
            i32.add
            local.tee 6
            i32.load8_u
            i32.const 255
            i32.and
            i32.add
            local.set 7
            local.get 6
            local.get 7
            i32.store8
          end
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.const 2
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          local.get 3
          i32.load offset=4
          i32.add
          i32.const 255
          i32.gt_s
          i32.const 1
          i32.and
          i32.eqz
          if  ;; label = @4
            nop
            local.get 3
            i32.load offset=4
            local.get 3
            i32.load offset=12
            local.get 3
            i32.load
            i32.const 2
            i32.add
            i32.add
            local.tee 8
            i32.load8_u
            i32.const 255
            i32.and
            i32.add
            local.set 9
            local.get 8
            local.get 9
            i32.store8
          end
          local.get 3
          local.get 3
          i32.load
          i32.const 4
          i32.add
          i32.store
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end)
  (func (;7;) (type 4) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 1
    i32.store offset=8
    local.get 2
    i32.const 0
    i32.store offset=4
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load offset=4
          local.get 2
          i32.load offset=8
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.add
          i32.const 255
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.sub
          i32.store8
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.const 1
          i32.add
          i32.add
          i32.const 255
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.const 1
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.sub
          i32.store8
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.const 2
          i32.add
          i32.add
          i32.const 255
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load offset=4
          i32.const 2
          i32.add
          i32.add
          i32.load8_u
          i32.const 255
          i32.and
          i32.sub
          i32.store8
          local.get 2
          local.get 2
          i32.load offset=4
          i32.const 4
          i32.add
          i32.store offset=4
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end)
  (func (;8;) (type 4) (param i32 i32)
    (local i32 i32 i32)
    block  ;; label = @1
      global.get 0
      i32.const 16
      i32.sub
      local.tee 2
      local.tee 3
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 3
      global.set 0
    end
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 1
    i32.store offset=8
    local.get 2
    i32.const 0
    i32.store
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load
          local.get 2
          i32.load offset=8
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          call 14
          i32.const 70
          i32.rem_s
          i32.const 35
          i32.sub
          i32.store offset=4
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 2
          i32.shl
          i32.add
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 2
          i32.shl
          i32.add
          f32.load
          local.get 2
          i32.load offset=4
          f32.convert_i32_s
          f32.add
          f32.store
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 1
          i32.add
          i32.const 2
          i32.shl
          i32.add
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 1
          i32.add
          i32.const 2
          i32.shl
          i32.add
          f32.load
          local.get 2
          i32.load offset=4
          f32.convert_i32_s
          f32.add
          f32.store
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 2
          i32.add
          i32.const 2
          i32.shl
          i32.add
          local.get 2
          i32.load offset=12
          local.get 2
          i32.load
          i32.const 2
          i32.add
          i32.const 2
          i32.shl
          i32.add
          f32.load
          local.get 2
          i32.load offset=4
          f32.convert_i32_s
          f32.add
          f32.store
          local.get 2
          local.get 2
          i32.load
          i32.const 4
          i32.add
          i32.store
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end
    block  ;; label = @1
      local.get 2
      i32.const 16
      i32.add
      local.tee 4
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 4
      global.set 0
    end)
  (func (;9;) (type 8) (param i32 i32 i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 7
    local.get 0
    i32.store offset=28
    local.get 7
    local.get 1
    i32.store offset=24
    local.get 7
    local.get 2
    i32.store offset=20
    local.get 7
    local.get 3
    i32.store offset=16
    local.get 7
    local.get 4
    i32.store offset=12
    local.get 7
    local.get 5
    i32.store offset=8
    local.get 7
    local.get 6
    i32.store offset=4
    local.get 7
    i32.const 0
    i32.store
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 7
          i32.load
          local.get 7
          i32.load offset=24
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 7
          i32.load
          i32.const 4
          i32.rem_s
          i32.const 3
          i32.ne
          i32.const 1
          i32.and
          if  ;; label = @4
            local.get 7
            i32.load offset=28
            local.get 7
            i32.load
            i32.add
            local.get 7
            i32.load offset=12
            local.get 7
            i32.load offset=8
            local.get 7
            i32.load offset=28
            local.get 7
            i32.load
            i32.add
            i32.load8_u
            i32.const 255
            i32.and
            i32.mul
            i32.add
            local.get 7
            i32.load offset=28
            local.get 7
            i32.load
            local.get 7
            i32.load offset=4
            i32.add
            i32.add
            i32.load8_u
            i32.const 255
            i32.and
            i32.sub
            local.get 7
            i32.load offset=28
            local.get 7
            i32.load
            local.get 7
            i32.load offset=20
            i32.const 2
            i32.shl
            i32.add
            i32.add
            i32.load8_u
            i32.const 255
            i32.and
            i32.sub
            i32.store8
          end
          local.get 7
          local.get 7
          i32.load offset=16
          local.get 7
          i32.load
          i32.add
          i32.store
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end)
  (func (;10;) (type 7) (param i32 i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 f64)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 6
    local.set 4
    block  ;; label = @1
      local.get 6
      local.tee 9
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 9
      global.set 0
    end
    local.get 4
    local.get 0
    i32.store offset=76
    local.get 4
    local.get 1
    i32.store offset=72
    local.get 4
    local.get 2
    i32.store offset=68
    local.get 4
    local.get 3
    i32.const 1
    i32.and
    i32.store8 offset=67
    local.get 4
    i32.load offset=72
    local.get 4
    i32.load offset=68
    i32.mul
    local.set 7
    local.get 4
    local.get 6
    i32.store offset=60
    block  ;; label = @1
      local.get 6
      local.get 7
      i32.const 2
      i32.shl
      i32.const 15
      i32.add
      i32.const -16
      i32.and
      i32.sub
      local.tee 5
      local.tee 10
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 10
      global.set 0
    end
    local.get 4
    local.get 7
    i32.store offset=56
    local.get 4
    i32.const 0
    i32.store offset=52
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 4
          i32.load offset=52
          local.get 4
          i32.load offset=68
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 4
          i32.const 0
          i32.store offset=48
          loop  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 4
                i32.load offset=48
                local.get 4
                i32.load offset=72
                i32.lt_s
                i32.const 1
                i32.and
                i32.eqz
                br_if 1 (;@5;)
                local.get 4
                local.get 4
                i32.load offset=72
                local.get 4
                i32.load offset=52
                i32.mul
                local.get 4
                i32.load offset=48
                i32.add
                i32.const 2
                i32.shl
                i32.store offset=44
                local.get 4
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=44
                i32.add
                i32.load8_u
                i32.const 255
                i32.and
                i32.store offset=40
                local.get 4
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=44
                i32.const 1
                i32.add
                i32.add
                i32.load8_u
                i32.const 255
                i32.and
                i32.store offset=36
                local.get 4
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=44
                i32.const 2
                i32.add
                i32.add
                i32.load8_u
                i32.const 255
                i32.and
                i32.store offset=32
                local.get 4
                local.get 4
                i32.load offset=40
                i32.const 2
                i32.shr_s
                local.get 4
                i32.load offset=36
                i32.const 1
                i32.shr_s
                i32.add
                local.get 4
                i32.load offset=32
                i32.const 3
                i32.shr_s
                i32.add
                i32.store offset=28
                local.get 5
                local.get 4
                i32.load offset=72
                local.get 4
                i32.load offset=52
                i32.mul
                local.get 4
                i32.load offset=48
                i32.add
                i32.const 2
                i32.shl
                i32.add
                local.get 4
                i32.load offset=28
                i32.store
                local.get 4
                local.get 4
                i32.load offset=72
                local.get 4
                i32.load offset=52
                i32.mul
                local.get 4
                i32.load offset=48
                i32.add
                i32.const 2
                i32.shl
                i32.store offset=24
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=24
                i32.add
                local.get 4
                i32.load offset=28
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=24
                i32.const 1
                i32.add
                i32.add
                local.get 4
                i32.load offset=28
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=24
                i32.const 2
                i32.add
                i32.add
                local.get 4
                i32.load offset=28
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load offset=24
                i32.const 3
                i32.add
                i32.add
                i32.const 255
                i32.store8
                local.get 4
                local.get 4
                i32.load offset=48
                i32.const 1
                i32.add
                i32.store offset=48
                br 2 (;@4;)
                unreachable
              end
              unreachable
            end
          end
          local.get 4
          local.get 4
          i32.load offset=52
          i32.const 1
          i32.add
          i32.store offset=52
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end
    local.get 4
    i32.const 0
    i32.store offset=20
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 4
          i32.load offset=20
          local.get 4
          i32.load offset=68
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 4
          i32.const 0
          i32.store offset=16
          loop  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 4
                i32.load offset=16
                local.get 4
                i32.load offset=72
                i32.lt_s
                i32.const 1
                i32.and
                i32.eqz
                br_if 1 (;@5;)
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 4
                      i32.load offset=16
                      i32.const 0
                      i32.le_s
                      i32.const 1
                      i32.and
                      br_if 0 (;@9;)
                      local.get 4
                      i32.load offset=16
                      local.get 4
                      i32.load offset=72
                      i32.const 1
                      i32.sub
                      i32.ge_s
                      i32.const 1
                      i32.and
                      br_if 0 (;@9;)
                      local.get 4
                      i32.load offset=20
                      i32.const 0
                      i32.le_s
                      i32.const 1
                      i32.and
                      br_if 0 (;@9;)
                      local.get 4
                      i32.load offset=20
                      local.get 4
                      i32.load offset=68
                      i32.const 1
                      i32.sub
                      i32.ge_s
                      i32.const 1
                      i32.and
                      i32.eqz
                      br_if 1 (;@8;)
                    end
                    local.get 4
                    i32.const 0
                    i32.store offset=12
                    local.get 4
                    i32.const 0
                    i32.store offset=8
                    br 1 (;@7;)
                  end
                  local.get 4
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.sub
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.sub
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const -1
                  i32.mul
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.add
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.sub
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.sub
                  local.get 4
                  i32.load offset=20
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const 1
                  i32.shl
                  i32.const -1
                  i32.mul
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.add
                  local.get 4
                  i32.load offset=20
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const 1
                  i32.shl
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.sub
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.add
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const -1
                  i32.mul
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.add
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.add
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.add
                  i32.store offset=12
                  local.get 4
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.sub
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.sub
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const -1
                  i32.mul
                  local.get 4
                  i32.load offset=16
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.sub
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const 1
                  i32.shl
                  i32.const -1
                  i32.mul
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.add
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.sub
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const -1
                  i32.mul
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.sub
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.add
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.add
                  local.get 4
                  i32.load offset=16
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.add
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.const 1
                  i32.shl
                  i32.add
                  local.get 4
                  i32.load offset=16
                  i32.const 1
                  i32.add
                  local.get 4
                  i32.load offset=20
                  i32.const 1
                  i32.add
                  local.get 5
                  local.get 4
                  i32.load offset=72
                  local.get 4
                  i32.load offset=68
                  call 4
                  i32.add
                  i32.store offset=8
                end
                block  ;; label = @7
                  local.get 4
                  i32.load offset=12
                  local.get 4
                  i32.load offset=12
                  i32.mul
                  local.get 4
                  i32.load offset=8
                  local.get 4
                  i32.load offset=8
                  i32.mul
                  i32.add
                  call 11
                  local.tee 12
                  f64.abs
                  f64.const 0x1p+31 (;=2.14748e+09;)
                  f64.lt
                  i32.eqz
                  i32.eqz
                  if  ;; label = @8
                    local.get 12
                    i32.trunc_f64_s
                    local.set 8
                    br 1 (;@7;)
                  end
                  i32.const -2147483648
                  local.set 8
                end
                local.get 4
                local.get 8
                i32.store offset=4
                local.get 4
                i32.load offset=4
                i32.const 255
                i32.gt_s
                i32.const 1
                i32.and
                if  ;; label = @7
                  local.get 4
                  i32.const 255
                  i32.store offset=4
                end
                local.get 4
                local.get 4
                i32.load offset=72
                local.get 4
                i32.load offset=20
                i32.mul
                local.get 4
                i32.load offset=16
                i32.add
                i32.const 2
                i32.shl
                i32.store
                local.get 4
                i32.load8_u offset=67
                i32.const 1
                i32.and
                i32.const 1
                i32.eq
                i32.const 1
                i32.and
                if  ;; label = @7
                  local.get 4
                  i32.const 255
                  local.get 4
                  i32.load offset=4
                  i32.sub
                  i32.store offset=4
                end
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load
                i32.add
                local.get 4
                i32.load offset=4
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load
                i32.const 1
                i32.add
                i32.add
                local.get 4
                i32.load offset=4
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load
                i32.const 2
                i32.add
                i32.add
                local.get 4
                i32.load offset=4
                i32.store8
                local.get 4
                i32.load offset=76
                local.get 4
                i32.load
                i32.const 3
                i32.add
                i32.add
                i32.const 255
                i32.store8
                local.get 4
                local.get 4
                i32.load offset=16
                i32.const 1
                i32.add
                i32.store offset=16
                br 2 (;@4;)
                unreachable
              end
              unreachable
            end
          end
          local.get 4
          local.get 4
          i32.load offset=20
          i32.const 1
          i32.add
          i32.store offset=20
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end
    local.get 4
    i32.load offset=60
    drop
    block  ;; label = @1
      local.get 4
      i32.const 80
      i32.add
      local.tee 11
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 11
      global.set 0
    end)
  (func (;11;) (type 5) (param i32) (result f64)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    local.get 0
    i32.store offset=12
    local.get 1
    i32.load offset=12
    f64.convert_i32_s
    f64.sqrt)
  (func (;12;) (type 9) (param i32 i32 i32 i32 i32 i32 f32 f32 i32)
    (local i32 i32 i32 i32 i32 f64 f64 f64 f64 f64 f64 f64 f64)
    block  ;; label = @1
      global.get 0
      i32.const 96
      i32.sub
      local.tee 9
      local.tee 12
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 12
      global.set 0
    end
    local.get 9
    local.get 0
    i32.store offset=92
    local.get 9
    local.get 1
    i32.store offset=88
    local.get 9
    local.get 2
    i32.store offset=84
    local.get 9
    local.get 3
    i32.store offset=80
    local.get 9
    local.get 4
    i32.store offset=76
    local.get 9
    local.get 5
    i32.store offset=72
    local.get 9
    local.get 6
    f32.store offset=68
    local.get 9
    local.get 7
    f32.store offset=64
    local.get 9
    local.get 8
    i32.store offset=60
    block  ;; label = @1
      local.get 9
      i32.load offset=72
      i32.const 2
      i32.div_s
      call 13
      local.tee 20
      f64.abs
      f64.const 0x1p+31 (;=2.14748e+09;)
      f64.lt
      i32.eqz
      i32.eqz
      if  ;; label = @2
        local.get 20
        i32.trunc_f64_s
        local.set 10
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 10
    end
    local.get 9
    local.get 10
    i32.store offset=24
    block  ;; label = @1
      local.get 9
      i32.load offset=76
      i32.const 2
      i32.div_s
      call 13
      local.tee 21
      f64.abs
      f64.const 0x1p+31 (;=2.14748e+09;)
      f64.lt
      i32.eqz
      i32.eqz
      if  ;; label = @2
        local.get 21
        i32.trunc_f64_s
        local.set 11
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 11
    end
    local.get 9
    local.get 11
    i32.store offset=20
    local.get 9
    i32.const 0
    i32.store offset=16
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 9
          i32.load offset=16
          local.get 9
          i32.load offset=60
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          br_if 1 (;@2;)
          local.get 9
          local.get 9
          i32.load offset=24
          i32.store offset=12
          loop  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 9
                i32.load offset=12
                local.get 9
                i32.load offset=84
                local.get 9
                i32.load offset=24
                i32.sub
                i32.lt_s
                i32.const 1
                i32.and
                i32.eqz
                br_if 1 (;@5;)
                local.get 9
                local.get 9
                i32.load offset=20
                i32.store offset=8
                loop  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 9
                      i32.load offset=8
                      local.get 9
                      i32.load offset=88
                      local.get 9
                      i32.load offset=20
                      i32.sub
                      i32.lt_s
                      i32.const 1
                      i32.and
                      i32.eqz
                      br_if 1 (;@8;)
                      local.get 9
                      f32.const 0x0p+0 (;=0;)
                      f32.store offset=56
                      local.get 9
                      f32.const 0x0p+0 (;=0;)
                      f32.store offset=52
                      local.get 9
                      f32.const 0x0p+0 (;=0;)
                      f32.store offset=48
                      local.get 9
                      i32.const 0
                      i32.store offset=4
                      loop  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            local.get 9
                            i32.load offset=4
                            local.get 9
                            i32.load offset=72
                            i32.lt_s
                            i32.const 1
                            i32.and
                            i32.eqz
                            br_if 1 (;@11;)
                            local.get 9
                            i32.const 0
                            i32.store
                            loop  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  local.get 9
                                  i32.load
                                  local.get 9
                                  i32.load offset=76
                                  i32.lt_s
                                  i32.const 1
                                  i32.and
                                  i32.eqz
                                  br_if 1 (;@14;)
                                  local.get 9
                                  local.get 9
                                  i32.load offset=88
                                  local.get 9
                                  i32.load offset=12
                                  local.get 9
                                  i32.load offset=24
                                  i32.sub
                                  local.get 9
                                  i32.load offset=4
                                  i32.add
                                  i32.mul
                                  local.get 9
                                  i32.load offset=8
                                  local.get 9
                                  i32.load offset=20
                                  i32.sub
                                  local.get 9
                                  i32.load
                                  i32.add
                                  i32.add
                                  i32.const 2
                                  i32.shl
                                  i32.store offset=36
                                  local.get 9
                                  local.get 9
                                  i32.load offset=76
                                  local.get 9
                                  i32.load offset=4
                                  i32.mul
                                  local.get 9
                                  i32.load
                                  i32.add
                                  i32.store offset=32
                                  local.get 9
                                  local.get 9
                                  f32.load offset=56
                                  local.get 9
                                  i32.load offset=92
                                  local.get 9
                                  i32.load offset=36
                                  i32.const 0
                                  i32.add
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  local.get 9
                                  i32.load offset=80
                                  local.get 9
                                  i32.load offset=32
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  f32.mul
                                  f32.add
                                  f32.store offset=56
                                  local.get 9
                                  local.get 9
                                  f32.load offset=52
                                  local.get 9
                                  i32.load offset=92
                                  local.get 9
                                  i32.load offset=36
                                  i32.const 1
                                  i32.add
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  local.get 9
                                  i32.load offset=80
                                  local.get 9
                                  i32.load offset=32
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  f32.mul
                                  f32.add
                                  f32.store offset=52
                                  local.get 9
                                  local.get 9
                                  f32.load offset=48
                                  local.get 9
                                  i32.load offset=92
                                  local.get 9
                                  i32.load offset=36
                                  i32.const 2
                                  i32.add
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  local.get 9
                                  i32.load offset=80
                                  local.get 9
                                  i32.load offset=32
                                  i32.const 2
                                  i32.shl
                                  i32.add
                                  f32.load
                                  f32.mul
                                  f32.add
                                  f32.store offset=48
                                  local.get 9
                                  local.get 9
                                  i32.load
                                  i32.const 1
                                  i32.add
                                  i32.store
                                  br 2 (;@13;)
                                  unreachable
                                end
                                unreachable
                              end
                            end
                            local.get 9
                            local.get 9
                            i32.load offset=4
                            i32.const 1
                            i32.add
                            i32.store offset=4
                            br 2 (;@10;)
                            unreachable
                          end
                          unreachable
                        end
                      end
                      local.get 9
                      local.get 9
                      i32.load offset=88
                      local.get 9
                      i32.load offset=12
                      i32.mul
                      local.get 9
                      i32.load offset=8
                      i32.add
                      i32.const 2
                      i32.shl
                      i32.store offset=28
                      block  ;; label = @10
                        local.get 9
                        f32.load offset=56
                        local.get 9
                        f32.load offset=68
                        f32.div
                        f64.promote_f32
                        f64.const 0x1.fep+7 (;=255;)
                        f64.gt
                        i32.const 1
                        i32.and
                        if  ;; label = @11
                          f64.const 0x1.fep+7 (;=255;)
                          local.set 14
                          br 1 (;@10;)
                        end
                        block  ;; label = @11
                          local.get 9
                          f32.load offset=56
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          f64.const 0x0p+0 (;=0;)
                          f64.lt
                          i32.const 1
                          i32.and
                          if  ;; label = @12
                            f64.const 0x0p+0 (;=0;)
                            local.set 15
                            br 1 (;@11;)
                          end
                          local.get 9
                          f32.load offset=56
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          local.set 15
                        end
                        local.get 15
                        local.set 14
                      end
                      local.get 9
                      i32.load offset=92
                      local.get 9
                      i32.load offset=28
                      i32.const 2
                      i32.shl
                      i32.add
                      local.get 14
                      f32.demote_f64
                      f32.store
                      block  ;; label = @10
                        local.get 9
                        f32.load offset=52
                        local.get 9
                        f32.load offset=68
                        f32.div
                        f64.promote_f32
                        f64.const 0x1.fep+7 (;=255;)
                        f64.gt
                        i32.const 1
                        i32.and
                        if  ;; label = @11
                          f64.const 0x1.fep+7 (;=255;)
                          local.set 16
                          br 1 (;@10;)
                        end
                        block  ;; label = @11
                          local.get 9
                          f32.load offset=52
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          f64.const 0x0p+0 (;=0;)
                          f64.lt
                          i32.const 1
                          i32.and
                          if  ;; label = @12
                            f64.const 0x0p+0 (;=0;)
                            local.set 17
                            br 1 (;@11;)
                          end
                          local.get 9
                          f32.load offset=52
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          local.set 17
                        end
                        local.get 17
                        local.set 16
                      end
                      local.get 9
                      i32.load offset=92
                      local.get 9
                      i32.load offset=28
                      i32.const 2
                      i32.shl
                      i32.add
                      local.get 16
                      f32.demote_f64
                      f32.store offset=4
                      block  ;; label = @10
                        local.get 9
                        f32.load offset=48
                        local.get 9
                        f32.load offset=68
                        f32.div
                        f64.promote_f32
                        f64.const 0x1.fep+7 (;=255;)
                        f64.gt
                        i32.const 1
                        i32.and
                        if  ;; label = @11
                          f64.const 0x1.fep+7 (;=255;)
                          local.set 18
                          br 1 (;@10;)
                        end
                        block  ;; label = @11
                          local.get 9
                          f32.load offset=48
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          f64.const 0x0p+0 (;=0;)
                          f64.lt
                          i32.const 1
                          i32.and
                          if  ;; label = @12
                            f64.const 0x0p+0 (;=0;)
                            local.set 19
                            br 1 (;@11;)
                          end
                          local.get 9
                          f32.load offset=48
                          local.get 9
                          f32.load offset=68
                          f32.div
                          f64.promote_f32
                          local.set 19
                        end
                        local.get 19
                        local.set 18
                      end
                      local.get 9
                      i32.load offset=92
                      local.get 9
                      i32.load offset=28
                      i32.const 2
                      i32.add
                      i32.const 2
                      i32.shl
                      i32.add
                      local.get 18
                      f32.demote_f64
                      f32.store
                      local.get 9
                      local.get 9
                      i32.load offset=8
                      i32.const 1
                      i32.add
                      i32.store offset=8
                      br 2 (;@7;)
                      unreachable
                    end
                    unreachable
                  end
                end
                local.get 9
                local.get 9
                i32.load offset=12
                i32.const 1
                i32.add
                i32.store offset=12
                br 2 (;@4;)
                unreachable
              end
              unreachable
            end
          end
          local.get 9
          local.get 9
          i32.load offset=16
          i32.const 1
          i32.add
          i32.store offset=16
          br 2 (;@1;)
          unreachable
        end
        unreachable
      end
    end
    block  ;; label = @1
      local.get 9
      i32.const 96
      i32.add
      local.tee 13
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 13
      global.set 0
    end)
  (func (;13;) (type 5) (param i32) (result f64)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    local.get 0
    i32.store offset=12
    local.get 1
    i32.load offset=12
    f64.convert_i32_s
    f64.floor)
  (func (;14;) (type 1) (result i32)
    (local i64)
    i32.const 1024
    i32.const 1024
    i64.load
    i64.const 6364136223846793005
    i64.mul
    i64.const 1
    i64.add
    local.tee 0
    i64.store
    local.get 0
    i64.const 33
    i64.shr_u
    i32.wrap_i64)
  (func (;15;) (type 1) (result i32)
    i32.const 1032)
  (func (;16;) (type 0) (param i32) (result i32)
    (local i32 i32 i32 i32)
    call 2
    local.tee 3
    i32.load
    local.tee 2
    local.get 0
    i32.const 3
    i32.add
    i32.const -4
    i32.and
    local.tee 4
    i32.add
    local.set 1
    block  ;; label = @1
      local.get 4
      i32.const 1
      i32.ge_s
      i32.const 0
      local.get 1
      local.get 2
      i32.le_u
      select
      br_if 0 (;@1;)
      local.get 1
      memory.size
      i32.const 16
      i32.shl
      i32.gt_u
      if  ;; label = @2
        local.get 1
        call 0
        i32.eqz
        br_if 1 (;@1;)
      end
      local.get 3
      local.get 1
      i32.store
      local.get 2
      return
    end
    call 15
    i32.const 48
    i32.store
    i32.const -1)
  (func (;17;) (type 0) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      global.get 0
      i32.const 16
      i32.sub
      local.tee 50
      local.tee 81
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 81
      global.set 0
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          i32.const 244
                          i32.le_u
                          if  ;; label = @12
                            i32.const 1036
                            i32.load
                            local.tee 5
                            i32.const 16
                            local.get 0
                            i32.const 11
                            i32.add
                            i32.const -8
                            i32.and
                            local.get 0
                            i32.const 11
                            i32.lt_u
                            select
                            local.tee 4
                            i32.const 3
                            i32.shr_u
                            local.tee 35
                            i32.shr_u
                            local.tee 36
                            i32.const 3
                            i32.and
                            if  ;; label = @13
                              local.get 35
                              local.get 36
                              i32.const -1
                              i32.xor
                              i32.const 1
                              i32.and
                              i32.add
                              local.tee 51
                              i32.const 3
                              i32.shl
                              local.tee 82
                              i32.const 1084
                              i32.add
                              i32.load
                              local.tee 37
                              i32.const 8
                              i32.add
                              local.set 0
                              block  ;; label = @14
                                local.get 37
                                i32.load offset=8
                                local.tee 52
                                local.get 82
                                i32.const 1076
                                i32.add
                                local.tee 53
                                i32.eq
                                if  ;; label = @15
                                  i32.const 1036
                                  i32.const -2
                                  local.get 51
                                  i32.rotl
                                  local.get 5
                                  i32.and
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 1052
                                i32.load
                                drop
                                local.get 52
                                local.get 53
                                i32.store offset=12
                                local.get 53
                                local.get 52
                                i32.store offset=8
                              end
                              local.get 37
                              local.get 51
                              i32.const 3
                              i32.shl
                              local.tee 83
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 37
                              local.get 83
                              i32.add
                              local.tee 84
                              local.get 84
                              i32.load offset=4
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              br 12 (;@1;)
                            end
                            local.get 4
                            i32.const 1044
                            i32.load
                            local.tee 19
                            i32.le_u
                            br_if 1 (;@11;)
                            local.get 36
                            if  ;; label = @13
                              block  ;; label = @14
                                i32.const 2
                                local.get 35
                                i32.shl
                                local.tee 85
                                i32.const 0
                                local.get 85
                                i32.sub
                                i32.or
                                local.get 36
                                local.get 35
                                i32.shl
                                i32.and
                                local.tee 86
                                i32.const 0
                                local.get 86
                                i32.sub
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 87
                                local.get 87
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 88
                                i32.shr_u
                                local.tee 89
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 90
                                local.get 88
                                i32.or
                                local.get 89
                                local.get 90
                                i32.shr_u
                                local.tee 91
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 92
                                i32.or
                                local.get 91
                                local.get 92
                                i32.shr_u
                                local.tee 93
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 94
                                i32.or
                                local.get 93
                                local.get 94
                                i32.shr_u
                                local.tee 95
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 96
                                i32.or
                                local.get 95
                                local.get 96
                                i32.shr_u
                                i32.add
                                local.tee 54
                                i32.const 3
                                i32.shl
                                local.tee 97
                                i32.const 1084
                                i32.add
                                i32.load
                                local.tee 20
                                i32.load offset=8
                                local.tee 55
                                local.get 97
                                i32.const 1076
                                i32.add
                                local.tee 56
                                i32.eq
                                if  ;; label = @15
                                  i32.const 1036
                                  i32.const -2
                                  local.get 54
                                  i32.rotl
                                  local.get 5
                                  i32.and
                                  local.tee 5
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 1052
                                i32.load
                                drop
                                local.get 55
                                local.get 56
                                i32.store offset=12
                                local.get 56
                                local.get 55
                                i32.store offset=8
                              end
                              local.get 20
                              i32.const 8
                              i32.add
                              local.set 0
                              local.get 20
                              local.get 4
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 4
                              local.get 20
                              i32.add
                              local.tee 98
                              local.get 54
                              i32.const 3
                              i32.shl
                              local.tee 99
                              local.get 4
                              i32.sub
                              local.tee 57
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 20
                              local.get 99
                              i32.add
                              local.get 57
                              i32.store
                              local.get 19
                              if  ;; label = @14
                                local.get 19
                                i32.const 3
                                i32.shr_u
                                local.tee 100
                                i32.const 3
                                i32.shl
                                i32.const 1076
                                i32.add
                                local.set 21
                                i32.const 1056
                                i32.load
                                local.set 22
                                block  ;; label = @15
                                  i32.const 1
                                  local.get 100
                                  i32.shl
                                  local.tee 101
                                  local.get 5
                                  i32.and
                                  i32.eqz
                                  if  ;; label = @16
                                    i32.const 1036
                                    local.get 5
                                    local.get 101
                                    i32.or
                                    i32.store
                                    local.get 21
                                    local.set 3
                                    br 1 (;@15;)
                                  end
                                  local.get 21
                                  i32.load offset=8
                                  local.set 3
                                end
                                local.get 21
                                local.get 22
                                i32.store offset=8
                                local.get 3
                                local.get 22
                                i32.store offset=12
                                local.get 22
                                local.get 21
                                i32.store offset=12
                                local.get 22
                                local.get 3
                                i32.store offset=8
                              end
                              i32.const 1056
                              local.get 98
                              i32.store
                              i32.const 1044
                              local.get 57
                              i32.store
                              br 12 (;@1;)
                            end
                            i32.const 1040
                            i32.load
                            local.tee 38
                            i32.eqz
                            br_if 1 (;@11;)
                            i32.const 0
                            local.get 38
                            i32.sub
                            local.get 38
                            i32.and
                            i32.const -1
                            i32.add
                            local.tee 102
                            local.get 102
                            i32.const 12
                            i32.shr_u
                            i32.const 16
                            i32.and
                            local.tee 103
                            i32.shr_u
                            local.tee 104
                            i32.const 5
                            i32.shr_u
                            i32.const 8
                            i32.and
                            local.tee 105
                            local.get 103
                            i32.or
                            local.get 104
                            local.get 105
                            i32.shr_u
                            local.tee 106
                            i32.const 2
                            i32.shr_u
                            i32.const 4
                            i32.and
                            local.tee 107
                            i32.or
                            local.get 106
                            local.get 107
                            i32.shr_u
                            local.tee 108
                            i32.const 1
                            i32.shr_u
                            i32.const 2
                            i32.and
                            local.tee 109
                            i32.or
                            local.get 108
                            local.get 109
                            i32.shr_u
                            local.tee 110
                            i32.const 1
                            i32.shr_u
                            i32.const 1
                            i32.and
                            local.tee 111
                            i32.or
                            local.get 110
                            local.get 111
                            i32.shr_u
                            i32.add
                            i32.const 2
                            i32.shl
                            i32.const 1340
                            i32.add
                            i32.load
                            local.tee 1
                            i32.load offset=4
                            i32.const -8
                            i32.and
                            local.get 4
                            i32.sub
                            local.set 2
                            local.get 1
                            local.set 7
                            loop  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  local.get 7
                                  i32.load offset=16
                                  local.tee 0
                                  i32.eqz
                                  if  ;; label = @16
                                    local.get 7
                                    i32.load offset=20
                                    local.tee 0
                                    i32.eqz
                                    br_if 2 (;@14;)
                                  end
                                  local.get 0
                                  i32.load offset=4
                                  i32.const -8
                                  i32.and
                                  local.get 4
                                  i32.sub
                                  local.tee 112
                                  local.get 2
                                  local.get 112
                                  local.get 2
                                  i32.lt_u
                                  local.tee 113
                                  select
                                  local.set 2
                                  local.get 0
                                  local.get 1
                                  local.get 113
                                  select
                                  local.set 1
                                  local.get 0
                                  local.set 7
                                  br 2 (;@13;)
                                  unreachable
                                end
                                unreachable
                              end
                            end
                            local.get 1
                            i32.load offset=24
                            local.set 23
                            local.get 1
                            i32.load offset=12
                            local.tee 3
                            local.get 1
                            i32.ne
                            if  ;; label = @13
                              i32.const 1052
                              i32.load
                              local.get 1
                              i32.load offset=8
                              local.tee 39
                              i32.le_u
                              if  ;; label = @14
                                local.get 39
                                i32.load offset=12
                                drop
                              end
                              local.get 39
                              local.get 3
                              i32.store offset=12
                              local.get 3
                              local.get 39
                              i32.store offset=8
                              br 11 (;@2;)
                            end
                            local.get 1
                            i32.const 20
                            i32.add
                            local.tee 7
                            i32.load
                            local.tee 0
                            i32.eqz
                            if  ;; label = @13
                              local.get 1
                              i32.load offset=16
                              local.tee 0
                              i32.eqz
                              br_if 3 (;@10;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.set 7
                            end
                            loop  ;; label = @13
                              local.get 7
                              local.set 114
                              local.get 0
                              local.tee 3
                              i32.const 20
                              i32.add
                              local.tee 7
                              i32.load
                              local.tee 0
                              br_if 0 (;@13;)
                              local.get 3
                              i32.const 16
                              i32.add
                              local.set 7
                              local.get 3
                              i32.load offset=16
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                            local.get 114
                            i32.const 0
                            i32.store
                            br 10 (;@2;)
                          end
                          i32.const -1
                          local.set 4
                          local.get 0
                          i32.const -65
                          i32.gt_u
                          br_if 0 (;@11;)
                          local.get 0
                          i32.const 11
                          i32.add
                          local.tee 115
                          i32.const -8
                          i32.and
                          local.set 4
                          i32.const 1040
                          i32.load
                          local.tee 14
                          i32.eqz
                          br_if 0 (;@11;)
                          i32.const 0
                          local.set 12
                          block  ;; label = @12
                            local.get 115
                            i32.const 8
                            i32.shr_u
                            local.tee 58
                            i32.eqz
                            br_if 0 (;@12;)
                            i32.const 31
                            local.set 12
                            local.get 4
                            i32.const 16777215
                            i32.gt_u
                            br_if 0 (;@12;)
                            local.get 58
                            local.get 58
                            i32.const 1048320
                            i32.add
                            i32.const 16
                            i32.shr_u
                            i32.const 8
                            i32.and
                            local.tee 116
                            i32.shl
                            local.tee 117
                            local.get 117
                            i32.const 520192
                            i32.add
                            i32.const 16
                            i32.shr_u
                            i32.const 4
                            i32.and
                            local.tee 118
                            i32.shl
                            local.tee 119
                            local.get 119
                            i32.const 245760
                            i32.add
                            i32.const 16
                            i32.shr_u
                            i32.const 2
                            i32.and
                            local.tee 120
                            i32.shl
                            i32.const 15
                            i32.shr_u
                            local.get 120
                            local.get 116
                            local.get 118
                            i32.or
                            i32.or
                            i32.sub
                            local.tee 121
                            i32.const 1
                            i32.shl
                            local.get 4
                            local.get 121
                            i32.const 21
                            i32.add
                            i32.shr_u
                            i32.const 1
                            i32.and
                            i32.or
                            i32.const 28
                            i32.add
                            local.set 12
                          end
                          i32.const 0
                          local.get 4
                          i32.sub
                          local.set 7
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                local.get 12
                                i32.const 2
                                i32.shl
                                i32.const 1340
                                i32.add
                                i32.load
                                local.tee 2
                                i32.eqz
                                if  ;; label = @15
                                  i32.const 0
                                  local.set 0
                                  i32.const 0
                                  local.set 3
                                  br 1 (;@14;)
                                end
                                local.get 4
                                i32.const 0
                                i32.const 25
                                local.get 12
                                i32.const 1
                                i32.shr_u
                                i32.sub
                                local.get 12
                                i32.const 31
                                i32.eq
                                select
                                i32.shl
                                local.set 1
                                i32.const 0
                                local.set 0
                                i32.const 0
                                local.set 3
                                loop  ;; label = @15
                                  block  ;; label = @16
                                    local.get 2
                                    i32.load offset=4
                                    i32.const -8
                                    i32.and
                                    local.get 4
                                    i32.sub
                                    local.tee 122
                                    local.get 7
                                    i32.ge_u
                                    br_if 0 (;@16;)
                                    local.get 2
                                    local.set 3
                                    local.get 122
                                    local.tee 7
                                    br_if 0 (;@16;)
                                    i32.const 0
                                    local.set 7
                                    local.get 2
                                    local.tee 3
                                    local.set 0
                                    br 3 (;@13;)
                                  end
                                  local.get 0
                                  local.get 2
                                  i32.load offset=20
                                  local.tee 59
                                  local.get 1
                                  i32.const 29
                                  i32.shr_u
                                  i32.const 4
                                  i32.and
                                  local.get 2
                                  i32.add
                                  i32.load offset=16
                                  local.tee 2
                                  local.get 59
                                  i32.eq
                                  select
                                  local.get 0
                                  local.get 59
                                  select
                                  local.set 0
                                  local.get 1
                                  local.get 2
                                  i32.const 0
                                  i32.ne
                                  i32.shl
                                  local.set 1
                                  local.get 2
                                  br_if 0 (;@15;)
                                end
                              end
                              local.get 0
                              local.get 3
                              i32.or
                              i32.eqz
                              if  ;; label = @14
                                local.get 14
                                i32.const 2
                                local.get 12
                                i32.shl
                                local.tee 123
                                i32.const 0
                                local.get 123
                                i32.sub
                                i32.or
                                i32.and
                                local.tee 60
                                i32.eqz
                                br_if 3 (;@11;)
                                i32.const 0
                                local.get 60
                                i32.sub
                                local.get 60
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 124
                                local.get 124
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 125
                                i32.shr_u
                                local.tee 126
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 127
                                local.get 125
                                i32.or
                                local.get 126
                                local.get 127
                                i32.shr_u
                                local.tee 128
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 129
                                i32.or
                                local.get 128
                                local.get 129
                                i32.shr_u
                                local.tee 130
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 131
                                i32.or
                                local.get 130
                                local.get 131
                                i32.shr_u
                                local.tee 132
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 133
                                i32.or
                                local.get 132
                                local.get 133
                                i32.shr_u
                                i32.add
                                i32.const 2
                                i32.shl
                                i32.const 1340
                                i32.add
                                i32.load
                                local.set 0
                              end
                              local.get 0
                              i32.eqz
                              br_if 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 0
                              i32.load offset=4
                              i32.const -8
                              i32.and
                              local.get 4
                              i32.sub
                              local.tee 134
                              local.get 7
                              i32.lt_u
                              local.set 61
                              local.get 0
                              i32.load offset=16
                              local.tee 2
                              i32.eqz
                              if  ;; label = @14
                                local.get 0
                                i32.load offset=20
                                local.set 2
                              end
                              local.get 134
                              local.get 7
                              local.get 61
                              select
                              local.set 7
                              local.get 0
                              local.get 3
                              local.get 61
                              select
                              local.set 3
                              local.get 2
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                          end
                          local.get 3
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 7
                          i32.const 1044
                          i32.load
                          local.get 4
                          i32.sub
                          i32.ge_u
                          br_if 0 (;@11;)
                          local.get 3
                          i32.load offset=24
                          local.set 24
                          local.get 3
                          i32.load offset=12
                          local.tee 1
                          local.get 3
                          i32.ne
                          if  ;; label = @12
                            i32.const 1052
                            i32.load
                            local.get 3
                            i32.load offset=8
                            local.tee 40
                            i32.le_u
                            if  ;; label = @13
                              local.get 40
                              i32.load offset=12
                              drop
                            end
                            local.get 40
                            local.get 1
                            i32.store offset=12
                            local.get 1
                            local.get 40
                            i32.store offset=8
                            br 9 (;@3;)
                          end
                          local.get 3
                          i32.const 20
                          i32.add
                          local.tee 2
                          i32.load
                          local.tee 0
                          i32.eqz
                          if  ;; label = @12
                            local.get 3
                            i32.load offset=16
                            local.tee 0
                            i32.eqz
                            br_if 3 (;@9;)
                            local.get 3
                            i32.const 16
                            i32.add
                            local.set 2
                          end
                          loop  ;; label = @12
                            local.get 2
                            local.set 135
                            local.get 0
                            local.tee 1
                            i32.const 20
                            i32.add
                            local.tee 2
                            i32.load
                            local.tee 0
                            br_if 0 (;@12;)
                            local.get 1
                            i32.const 16
                            i32.add
                            local.set 2
                            local.get 1
                            i32.load offset=16
                            local.tee 0
                            br_if 0 (;@12;)
                          end
                          local.get 135
                          i32.const 0
                          i32.store
                          br 8 (;@3;)
                        end
                        i32.const 1044
                        i32.load
                        local.tee 25
                        local.get 4
                        i32.ge_u
                        if  ;; label = @11
                          i32.const 1056
                          i32.load
                          local.set 13
                          block  ;; label = @12
                            local.get 25
                            local.get 4
                            i32.sub
                            local.tee 41
                            i32.const 16
                            i32.ge_u
                            if  ;; label = @13
                              i32.const 1044
                              local.get 41
                              i32.store
                              i32.const 1056
                              local.get 4
                              local.get 13
                              i32.add
                              local.tee 136
                              i32.store
                              local.get 136
                              local.get 41
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 25
                              local.get 13
                              i32.add
                              local.get 41
                              i32.store
                              local.get 13
                              local.get 4
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              br 1 (;@12;)
                            end
                            i32.const 1056
                            i32.const 0
                            i32.store
                            i32.const 1044
                            i32.const 0
                            i32.store
                            local.get 13
                            local.get 25
                            i32.const 3
                            i32.or
                            i32.store offset=4
                            local.get 25
                            local.get 13
                            i32.add
                            local.tee 137
                            local.get 137
                            i32.load offset=4
                            i32.const 1
                            i32.or
                            i32.store offset=4
                          end
                          local.get 13
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        i32.const 1048
                        i32.load
                        local.tee 62
                        local.get 4
                        i32.gt_u
                        if  ;; label = @11
                          i32.const 1048
                          local.get 62
                          local.get 4
                          i32.sub
                          local.tee 138
                          i32.store
                          i32.const 1060
                          local.get 4
                          i32.const 1060
                          i32.load
                          local.tee 63
                          i32.add
                          local.tee 139
                          i32.store
                          local.get 139
                          local.get 138
                          i32.const 1
                          i32.or
                          i32.store offset=4
                          local.get 63
                          local.get 4
                          i32.const 3
                          i32.or
                          i32.store offset=4
                          local.get 63
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        block  ;; label = @11
                          i32.const 1508
                          i32.load
                          if  ;; label = @12
                            i32.const 1516
                            i32.load
                            local.set 2
                            br 1 (;@11;)
                          end
                          i32.const 1520
                          i64.const -1
                          i64.store align=4
                          i32.const 1512
                          i64.const 17592186048512
                          i64.store align=4
                          i32.const 1508
                          local.get 50
                          i32.const 12
                          i32.add
                          i32.const -16
                          i32.and
                          i32.const 1431655768
                          i32.xor
                          i32.store
                          i32.const 1528
                          i32.const 0
                          i32.store
                          i32.const 1480
                          i32.const 0
                          i32.store
                          i32.const 4096
                          local.set 2
                        end
                        i32.const 0
                        local.set 0
                        local.get 4
                        i32.const 47
                        i32.add
                        local.tee 140
                        local.get 2
                        i32.add
                        local.tee 141
                        i32.const 0
                        local.get 2
                        i32.sub
                        local.tee 142
                        i32.and
                        local.tee 15
                        local.get 4
                        i32.le_u
                        br_if 9 (;@1;)
                        i32.const 0
                        local.set 0
                        i32.const 1476
                        i32.load
                        local.tee 143
                        if  ;; label = @11
                          local.get 15
                          i32.const 1468
                          i32.load
                          local.tee 144
                          i32.add
                          local.tee 145
                          local.get 144
                          i32.le_u
                          br_if 10 (;@1;)
                          local.get 145
                          local.get 143
                          i32.gt_u
                          br_if 10 (;@1;)
                        end
                        i32.const 1480
                        i32.load8_u
                        i32.const 4
                        i32.and
                        br_if 4 (;@6;)
                        block  ;; label = @11
                          block  ;; label = @12
                            i32.const 1060
                            i32.load
                            local.tee 64
                            if  ;; label = @13
                              i32.const 1484
                              local.set 0
                              loop  ;; label = @14
                                local.get 0
                                i32.load
                                local.tee 146
                                local.get 64
                                i32.le_u
                                if  ;; label = @15
                                  local.get 0
                                  i32.load offset=4
                                  local.get 146
                                  i32.add
                                  local.get 64
                                  i32.gt_u
                                  br_if 3 (;@12;)
                                end
                                local.get 0
                                i32.load offset=8
                                local.tee 0
                                br_if 0 (;@14;)
                              end
                            end
                            i32.const 0
                            call 16
                            local.tee 1
                            i32.const -1
                            i32.eq
                            br_if 5 (;@7;)
                            local.get 15
                            local.set 5
                            local.get 1
                            i32.const 1512
                            i32.load
                            local.tee 147
                            i32.const -1
                            i32.add
                            local.tee 148
                            i32.and
                            if  ;; label = @13
                              local.get 15
                              local.get 1
                              i32.sub
                              local.get 1
                              local.get 148
                              i32.add
                              i32.const 0
                              local.get 147
                              i32.sub
                              i32.and
                              i32.add
                              local.set 5
                            end
                            local.get 5
                            local.get 4
                            i32.le_u
                            br_if 5 (;@7;)
                            local.get 5
                            i32.const 2147483646
                            i32.gt_u
                            br_if 5 (;@7;)
                            i32.const 1476
                            i32.load
                            local.tee 149
                            if  ;; label = @13
                              local.get 5
                              i32.const 1468
                              i32.load
                              local.tee 150
                              i32.add
                              local.tee 151
                              local.get 150
                              i32.le_u
                              br_if 6 (;@7;)
                              local.get 151
                              local.get 149
                              i32.gt_u
                              br_if 6 (;@7;)
                            end
                            local.get 1
                            local.get 5
                            call 16
                            local.tee 0
                            i32.ne
                            br_if 1 (;@11;)
                            br 7 (;@5;)
                          end
                          local.get 142
                          local.get 141
                          local.get 62
                          i32.sub
                          i32.and
                          local.tee 5
                          i32.const 2147483646
                          i32.gt_u
                          br_if 4 (;@7;)
                          local.get 5
                          call 16
                          local.tee 1
                          local.get 0
                          i32.load
                          local.get 0
                          i32.load offset=4
                          i32.add
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 1
                          local.set 0
                        end
                        block  ;; label = @11
                          local.get 4
                          i32.const 48
                          i32.add
                          local.get 5
                          i32.le_u
                          br_if 0 (;@11;)
                          local.get 0
                          i32.const -1
                          i32.eq
                          br_if 0 (;@11;)
                          i32.const 1516
                          i32.load
                          local.tee 152
                          local.get 140
                          local.get 5
                          i32.sub
                          i32.add
                          i32.const 0
                          local.get 152
                          i32.sub
                          i32.and
                          local.tee 65
                          i32.const 2147483646
                          i32.gt_u
                          if  ;; label = @12
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          local.get 65
                          call 16
                          i32.const -1
                          i32.ne
                          if  ;; label = @12
                            local.get 5
                            local.get 65
                            i32.add
                            local.set 5
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          i32.const 0
                          local.get 5
                          i32.sub
                          call 16
                          drop
                          br 4 (;@7;)
                        end
                        local.get 0
                        local.tee 1
                        i32.const -1
                        i32.ne
                        br_if 5 (;@5;)
                        br 3 (;@7;)
                      end
                      i32.const 0
                      local.set 3
                      br 7 (;@2;)
                    end
                    i32.const 0
                    local.set 1
                    br 5 (;@3;)
                  end
                  local.get 1
                  i32.const -1
                  i32.ne
                  br_if 2 (;@5;)
                end
                i32.const 1480
                i32.const 1480
                i32.load
                i32.const 4
                i32.or
                i32.store
              end
              local.get 15
              i32.const 2147483646
              i32.gt_u
              br_if 1 (;@4;)
              local.get 15
              call 16
              local.tee 1
              i32.const 0
              call 16
              local.tee 66
              i32.ge_u
              br_if 1 (;@4;)
              local.get 1
              i32.const -1
              i32.eq
              br_if 1 (;@4;)
              local.get 66
              i32.const -1
              i32.eq
              br_if 1 (;@4;)
              local.get 66
              local.get 1
              i32.sub
              local.tee 5
              local.get 4
              i32.const 40
              i32.add
              i32.le_u
              br_if 1 (;@4;)
            end
            i32.const 1468
            local.get 5
            i32.const 1468
            i32.load
            i32.add
            local.tee 67
            i32.store
            local.get 67
            i32.const 1472
            i32.load
            i32.gt_u
            if  ;; label = @5
              i32.const 1472
              local.get 67
              i32.store
            end
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 1060
                  i32.load
                  local.tee 6
                  if  ;; label = @8
                    i32.const 1484
                    local.set 0
                    loop  ;; label = @9
                      local.get 0
                      i32.load
                      local.tee 153
                      local.get 0
                      i32.load offset=4
                      local.tee 154
                      i32.add
                      local.get 1
                      i32.eq
                      br_if 2 (;@7;)
                      local.get 0
                      i32.load offset=8
                      local.tee 0
                      br_if 0 (;@9;)
                      br 3 (;@6;)
                      unreachable
                    end
                    unreachable
                  end
                  i32.const 1052
                  i32.load
                  local.tee 155
                  i32.const 0
                  local.get 1
                  local.get 155
                  i32.ge_u
                  select
                  i32.eqz
                  if  ;; label = @8
                    nop
                    i32.const 1052
                    local.get 1
                    i32.store
                  end
                  i32.const 0
                  local.set 0
                  i32.const 1488
                  local.get 5
                  i32.store
                  i32.const 1484
                  local.get 1
                  i32.store
                  i32.const 1068
                  i32.const -1
                  i32.store
                  i32.const 1072
                  i32.const 1508
                  i32.load
                  i32.store
                  i32.const 1496
                  i32.const 0
                  i32.store
                  loop  ;; label = @8
                    local.get 0
                    i32.const 3
                    i32.shl
                    local.tee 68
                    i32.const 1084
                    i32.add
                    local.get 68
                    i32.const 1076
                    i32.add
                    local.tee 156
                    i32.store
                    local.get 68
                    i32.const 1088
                    i32.add
                    local.get 156
                    i32.store
                    local.get 0
                    i32.const 1
                    i32.add
                    local.tee 0
                    i32.const 32
                    i32.ne
                    br_if 0 (;@8;)
                  end
                  i32.const 1048
                  local.get 5
                  i32.const -40
                  i32.add
                  local.tee 157
                  i32.const -8
                  local.get 1
                  i32.sub
                  i32.const 7
                  i32.and
                  i32.const 0
                  local.get 1
                  i32.const 8
                  i32.add
                  i32.const 7
                  i32.and
                  select
                  local.tee 158
                  i32.sub
                  local.tee 159
                  i32.store
                  i32.const 1060
                  local.get 1
                  local.get 158
                  i32.add
                  local.tee 160
                  i32.store
                  local.get 160
                  local.get 159
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  local.get 1
                  local.get 157
                  i32.add
                  i32.const 40
                  i32.store offset=4
                  i32.const 1064
                  i32.const 1524
                  i32.load
                  i32.store
                  br 2 (;@5;)
                end
                local.get 0
                i32.load8_u offset=12
                i32.const 8
                i32.and
                br_if 0 (;@6;)
                local.get 1
                local.get 6
                i32.le_u
                br_if 0 (;@6;)
                local.get 153
                local.get 6
                i32.gt_u
                br_if 0 (;@6;)
                local.get 0
                local.get 5
                local.get 154
                i32.add
                i32.store offset=4
                i32.const 1060
                i32.const -8
                local.get 6
                i32.sub
                i32.const 7
                i32.and
                i32.const 0
                local.get 6
                i32.const 8
                i32.add
                i32.const 7
                i32.and
                select
                local.tee 161
                local.get 6
                i32.add
                local.tee 162
                i32.store
                i32.const 1048
                local.get 5
                i32.const 1048
                i32.load
                i32.add
                local.tee 163
                local.get 161
                i32.sub
                local.tee 164
                i32.store
                local.get 162
                local.get 164
                i32.const 1
                i32.or
                i32.store offset=4
                local.get 6
                local.get 163
                i32.add
                i32.const 40
                i32.store offset=4
                i32.const 1064
                i32.const 1524
                i32.load
                i32.store
                br 1 (;@5;)
              end
              local.get 1
              i32.const 1052
              i32.load
              local.tee 3
              i32.lt_u
              if  ;; label = @6
                i32.const 1052
                local.get 1
                i32.store
                local.get 1
                local.set 3
              end
              local.get 5
              local.get 1
              i32.add
              local.set 26
              i32.const 1484
              local.set 0
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          loop  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                local.get 26
                                local.get 0
                                i32.load
                                i32.eq
                                br_if 1 (;@13;)
                                local.get 0
                                i32.load offset=8
                                local.tee 0
                                br_if 2 (;@12;)
                                br 3 (;@11;)
                                unreachable
                              end
                              unreachable
                            end
                          end
                          local.get 0
                          i32.load8_u offset=12
                          i32.const 8
                          i32.and
                          i32.eqz
                          br_if 1 (;@10;)
                        end
                        i32.const 1484
                        local.set 0
                        loop  ;; label = @11
                          local.get 0
                          i32.load
                          local.tee 165
                          local.get 6
                          i32.le_u
                          if  ;; label = @12
                            local.get 0
                            i32.load offset=4
                            local.get 165
                            i32.add
                            local.tee 27
                            local.get 6
                            i32.gt_u
                            br_if 3 (;@9;)
                          end
                          local.get 0
                          i32.load offset=8
                          local.set 0
                          br 0 (;@11;)
                          unreachable
                        end
                        unreachable
                      end
                      local.get 0
                      local.get 1
                      i32.store
                      local.get 0
                      local.get 5
                      local.get 0
                      i32.load offset=4
                      i32.add
                      i32.store offset=4
                      i32.const -8
                      local.get 1
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 1
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      local.get 1
                      i32.add
                      local.tee 42
                      local.get 4
                      i32.const 3
                      i32.or
                      i32.store offset=4
                      i32.const -8
                      local.get 26
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 26
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      local.get 26
                      i32.add
                      local.tee 1
                      local.get 42
                      i32.sub
                      local.get 4
                      i32.sub
                      local.set 0
                      local.get 4
                      local.get 42
                      i32.add
                      local.set 8
                      local.get 1
                      local.get 6
                      i32.eq
                      if  ;; label = @10
                        i32.const 1060
                        local.get 8
                        i32.store
                        i32.const 1048
                        local.get 0
                        i32.const 1048
                        i32.load
                        i32.add
                        local.tee 166
                        i32.store
                        local.get 8
                        local.get 166
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        br 3 (;@7;)
                      end
                      i32.const 1056
                      i32.load
                      local.get 1
                      i32.eq
                      if  ;; label = @10
                        i32.const 1056
                        local.get 8
                        i32.store
                        i32.const 1044
                        local.get 0
                        i32.const 1044
                        i32.load
                        i32.add
                        local.tee 43
                        i32.store
                        local.get 8
                        local.get 43
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        local.get 8
                        local.get 43
                        i32.add
                        local.get 43
                        i32.store
                        br 3 (;@7;)
                      end
                      local.get 1
                      i32.load offset=4
                      local.tee 44
                      i32.const 3
                      i32.and
                      i32.const 1
                      i32.eq
                      if  ;; label = @10
                        local.get 44
                        i32.const -8
                        i32.and
                        local.set 69
                        block  ;; label = @11
                          local.get 44
                          i32.const 255
                          i32.le_u
                          if  ;; label = @12
                            local.get 1
                            i32.load offset=12
                            local.set 28
                            local.get 1
                            i32.load offset=8
                            local.tee 45
                            local.get 44
                            i32.const 3
                            i32.shr_u
                            local.tee 167
                            i32.const 3
                            i32.shl
                            i32.const 1076
                            i32.add
                            local.tee 168
                            i32.ne
                            drop
                            local.get 28
                            local.get 45
                            i32.eq
                            if  ;; label = @13
                              i32.const 1036
                              i32.const 1036
                              i32.load
                              i32.const -2
                              local.get 167
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            local.get 28
                            local.get 168
                            i32.ne
                            drop
                            local.get 45
                            local.get 28
                            i32.store offset=12
                            local.get 28
                            local.get 45
                            i32.store offset=8
                            br 1 (;@11;)
                          end
                          local.get 1
                          i32.load offset=24
                          local.set 29
                          block  ;; label = @12
                            local.get 1
                            i32.load offset=12
                            local.tee 5
                            local.get 1
                            i32.ne
                            if  ;; label = @13
                              local.get 3
                              local.get 1
                              i32.load offset=8
                              local.tee 46
                              i32.le_u
                              if  ;; label = @14
                                local.get 46
                                i32.load offset=12
                                drop
                              end
                              local.get 46
                              local.get 5
                              i32.store offset=12
                              local.get 5
                              local.get 46
                              i32.store offset=8
                              br 1 (;@12;)
                            end
                            block  ;; label = @13
                              local.get 1
                              i32.const 20
                              i32.add
                              local.tee 2
                              i32.load
                              local.tee 4
                              br_if 0 (;@13;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.tee 2
                              i32.load
                              local.tee 4
                              br_if 0 (;@13;)
                              i32.const 0
                              local.set 5
                              br 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 2
                              local.set 169
                              local.get 4
                              local.tee 5
                              i32.const 20
                              i32.add
                              local.tee 2
                              i32.load
                              local.tee 4
                              br_if 0 (;@13;)
                              local.get 5
                              i32.const 16
                              i32.add
                              local.set 2
                              local.get 5
                              i32.load offset=16
                              local.tee 4
                              br_if 0 (;@13;)
                            end
                            local.get 169
                            i32.const 0
                            i32.store
                          end
                          local.get 29
                          i32.eqz
                          br_if 0 (;@11;)
                          block  ;; label = @12
                            local.get 1
                            i32.load offset=28
                            local.tee 170
                            i32.const 2
                            i32.shl
                            i32.const 1340
                            i32.add
                            local.tee 171
                            i32.load
                            local.get 1
                            i32.eq
                            if  ;; label = @13
                              local.get 171
                              local.get 5
                              i32.store
                              local.get 5
                              br_if 1 (;@12;)
                              i32.const 1040
                              i32.const 1040
                              i32.load
                              i32.const -2
                              local.get 170
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            i32.const 16
                            i32.const 20
                            local.get 1
                            local.get 29
                            i32.load offset=16
                            i32.eq
                            select
                            local.get 29
                            i32.add
                            local.get 5
                            i32.store
                            local.get 5
                            i32.eqz
                            br_if 1 (;@11;)
                          end
                          local.get 5
                          local.get 29
                          i32.store offset=24
                          local.get 1
                          i32.load offset=16
                          local.tee 70
                          if  ;; label = @12
                            local.get 5
                            local.get 70
                            i32.store offset=16
                            local.get 70
                            local.get 5
                            i32.store offset=24
                          end
                          local.get 1
                          i32.load offset=20
                          local.tee 71
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 71
                          i32.store offset=20
                          local.get 71
                          local.get 5
                          i32.store offset=24
                        end
                        local.get 0
                        local.get 69
                        i32.add
                        local.set 0
                        local.get 1
                        local.get 69
                        i32.add
                        local.set 1
                      end
                      local.get 1
                      local.get 1
                      i32.load offset=4
                      i32.const -2
                      i32.and
                      i32.store offset=4
                      local.get 8
                      local.get 0
                      i32.const 1
                      i32.or
                      i32.store offset=4
                      local.get 0
                      local.get 8
                      i32.add
                      local.get 0
                      i32.store
                      local.get 0
                      i32.const 255
                      i32.le_u
                      if  ;; label = @10
                        local.get 0
                        i32.const 3
                        i32.shr_u
                        local.tee 172
                        i32.const 3
                        i32.shl
                        i32.const 1076
                        i32.add
                        local.set 30
                        block  ;; label = @11
                          i32.const 1036
                          i32.load
                          local.tee 173
                          i32.const 1
                          local.get 172
                          i32.shl
                          local.tee 174
                          i32.and
                          i32.eqz
                          if  ;; label = @12
                            i32.const 1036
                            local.get 173
                            local.get 174
                            i32.or
                            i32.store
                            local.get 30
                            local.set 2
                            br 1 (;@11;)
                          end
                          local.get 30
                          i32.load offset=8
                          local.set 2
                        end
                        local.get 30
                        local.get 8
                        i32.store offset=8
                        local.get 2
                        local.get 8
                        i32.store offset=12
                        local.get 8
                        local.get 30
                        i32.store offset=12
                        local.get 8
                        local.get 2
                        i32.store offset=8
                        br 3 (;@7;)
                      end
                      i32.const 0
                      local.set 2
                      block  ;; label = @10
                        local.get 0
                        i32.const 8
                        i32.shr_u
                        local.tee 72
                        i32.eqz
                        br_if 0 (;@10;)
                        i32.const 31
                        local.set 2
                        local.get 0
                        i32.const 16777215
                        i32.gt_u
                        br_if 0 (;@10;)
                        local.get 72
                        local.get 72
                        i32.const 1048320
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 8
                        i32.and
                        local.tee 175
                        i32.shl
                        local.tee 176
                        local.get 176
                        i32.const 520192
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 4
                        i32.and
                        local.tee 177
                        i32.shl
                        local.tee 178
                        local.get 178
                        i32.const 245760
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 2
                        i32.and
                        local.tee 179
                        i32.shl
                        i32.const 15
                        i32.shr_u
                        local.get 179
                        local.get 175
                        local.get 177
                        i32.or
                        i32.or
                        i32.sub
                        local.tee 180
                        i32.const 1
                        i32.shl
                        local.get 0
                        local.get 180
                        i32.const 21
                        i32.add
                        i32.shr_u
                        i32.const 1
                        i32.and
                        i32.or
                        i32.const 28
                        i32.add
                        local.set 2
                      end
                      local.get 8
                      local.get 2
                      i32.store offset=28
                      local.get 8
                      i64.const 0
                      i64.store offset=16 align=4
                      local.get 2
                      i32.const 2
                      i32.shl
                      i32.const 1340
                      i32.add
                      local.set 47
                      block  ;; label = @10
                        i32.const 1040
                        i32.load
                        local.tee 181
                        i32.const 1
                        local.get 2
                        i32.shl
                        local.tee 182
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 1040
                          local.get 181
                          local.get 182
                          i32.or
                          i32.store
                          local.get 47
                          local.get 8
                          i32.store
                          local.get 8
                          local.get 47
                          i32.store offset=24
                          br 1 (;@10;)
                        end
                        local.get 0
                        i32.const 0
                        i32.const 25
                        local.get 2
                        i32.const 1
                        i32.shr_u
                        i32.sub
                        local.get 2
                        i32.const 31
                        i32.eq
                        select
                        i32.shl
                        local.set 2
                        local.get 47
                        i32.load
                        local.set 1
                        loop  ;; label = @11
                          local.get 0
                          local.get 1
                          local.tee 16
                          i32.load offset=4
                          i32.const -8
                          i32.and
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 2
                          i32.const 29
                          i32.shr_u
                          local.set 183
                          local.get 2
                          i32.const 1
                          i32.shl
                          local.set 2
                          local.get 183
                          i32.const 4
                          i32.and
                          local.get 16
                          i32.add
                          local.tee 184
                          i32.const 16
                          i32.add
                          i32.load
                          local.tee 1
                          br_if 0 (;@11;)
                        end
                        local.get 184
                        local.get 8
                        i32.store offset=16
                        local.get 8
                        local.get 16
                        i32.store offset=24
                      end
                      local.get 8
                      local.get 8
                      i32.store offset=12
                      local.get 8
                      local.get 8
                      i32.store offset=8
                      br 2 (;@7;)
                    end
                    i32.const 1048
                    local.get 5
                    i32.const -40
                    i32.add
                    local.tee 185
                    i32.const -8
                    local.get 1
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 1
                    i32.const 8
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    local.tee 186
                    i32.sub
                    local.tee 187
                    i32.store
                    i32.const 1060
                    local.get 1
                    local.get 186
                    i32.add
                    local.tee 188
                    i32.store
                    local.get 188
                    local.get 187
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 1
                    local.get 185
                    i32.add
                    i32.const 40
                    i32.store offset=4
                    i32.const 1064
                    i32.const 1524
                    i32.load
                    i32.store
                    local.get 6
                    i32.const 39
                    local.get 27
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 27
                    i32.const -39
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    local.get 27
                    i32.add
                    i32.const -47
                    i32.add
                    local.tee 189
                    local.get 189
                    local.get 6
                    i32.const 16
                    i32.add
                    i32.lt_u
                    select
                    local.tee 10
                    i32.const 27
                    i32.store offset=4
                    local.get 10
                    i32.const 1492
                    i64.load align=4
                    i64.store offset=16 align=4
                    local.get 10
                    i32.const 1484
                    i64.load align=4
                    i64.store offset=8 align=4
                    i32.const 1492
                    local.get 10
                    i32.const 8
                    i32.add
                    i32.store
                    i32.const 1488
                    local.get 5
                    i32.store
                    i32.const 1484
                    local.get 1
                    i32.store
                    i32.const 1496
                    i32.const 0
                    i32.store
                    local.get 10
                    i32.const 24
                    i32.add
                    local.set 0
                    loop  ;; label = @9
                      local.get 0
                      i32.const 7
                      i32.store offset=4
                      local.get 0
                      i32.const 8
                      i32.add
                      local.set 190
                      local.get 0
                      i32.const 4
                      i32.add
                      local.set 0
                      local.get 27
                      local.get 190
                      i32.gt_u
                      br_if 0 (;@9;)
                    end
                    local.get 6
                    local.get 10
                    i32.eq
                    br_if 3 (;@5;)
                    local.get 10
                    local.get 10
                    i32.load offset=4
                    i32.const -2
                    i32.and
                    i32.store offset=4
                    local.get 6
                    local.get 10
                    local.get 6
                    i32.sub
                    local.tee 11
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 10
                    local.get 11
                    i32.store
                    local.get 11
                    i32.const 255
                    i32.le_u
                    if  ;; label = @9
                      local.get 11
                      i32.const 3
                      i32.shr_u
                      local.tee 191
                      i32.const 3
                      i32.shl
                      i32.const 1076
                      i32.add
                      local.set 31
                      block  ;; label = @10
                        i32.const 1036
                        i32.load
                        local.tee 192
                        i32.const 1
                        local.get 191
                        i32.shl
                        local.tee 193
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 1036
                          local.get 192
                          local.get 193
                          i32.or
                          i32.store
                          local.get 31
                          local.set 7
                          br 1 (;@10;)
                        end
                        local.get 31
                        i32.load offset=8
                        local.set 7
                      end
                      local.get 31
                      local.get 6
                      i32.store offset=8
                      local.get 7
                      local.get 6
                      i32.store offset=12
                      local.get 6
                      local.get 31
                      i32.store offset=12
                      local.get 6
                      local.get 7
                      i32.store offset=8
                      br 4 (;@5;)
                    end
                    i32.const 0
                    local.set 0
                    block  ;; label = @9
                      local.get 11
                      i32.const 8
                      i32.shr_u
                      local.tee 73
                      i32.eqz
                      br_if 0 (;@9;)
                      i32.const 31
                      local.set 0
                      local.get 11
                      i32.const 16777215
                      i32.gt_u
                      br_if 0 (;@9;)
                      local.get 73
                      local.get 73
                      i32.const 1048320
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 8
                      i32.and
                      local.tee 194
                      i32.shl
                      local.tee 195
                      local.get 195
                      i32.const 520192
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 4
                      i32.and
                      local.tee 196
                      i32.shl
                      local.tee 197
                      local.get 197
                      i32.const 245760
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 2
                      i32.and
                      local.tee 198
                      i32.shl
                      i32.const 15
                      i32.shr_u
                      local.get 198
                      local.get 194
                      local.get 196
                      i32.or
                      i32.or
                      i32.sub
                      local.tee 199
                      i32.const 1
                      i32.shl
                      local.get 11
                      local.get 199
                      i32.const 21
                      i32.add
                      i32.shr_u
                      i32.const 1
                      i32.and
                      i32.or
                      i32.const 28
                      i32.add
                      local.set 0
                    end
                    local.get 6
                    i64.const 0
                    i64.store offset=16 align=4
                    local.get 6
                    local.get 0
                    i32.store offset=28
                    local.get 0
                    i32.const 2
                    i32.shl
                    i32.const 1340
                    i32.add
                    local.set 48
                    block  ;; label = @9
                      i32.const 1040
                      i32.load
                      local.tee 200
                      i32.const 1
                      local.get 0
                      i32.shl
                      local.tee 201
                      i32.and
                      i32.eqz
                      if  ;; label = @10
                        i32.const 1040
                        local.get 200
                        local.get 201
                        i32.or
                        i32.store
                        local.get 48
                        local.get 6
                        i32.store
                        local.get 6
                        local.get 48
                        i32.store offset=24
                        br 1 (;@9;)
                      end
                      local.get 11
                      i32.const 0
                      i32.const 25
                      local.get 0
                      i32.const 1
                      i32.shr_u
                      i32.sub
                      local.get 0
                      i32.const 31
                      i32.eq
                      select
                      i32.shl
                      local.set 0
                      local.get 48
                      i32.load
                      local.set 1
                      loop  ;; label = @10
                        local.get 11
                        local.get 1
                        local.tee 17
                        i32.load offset=4
                        i32.const -8
                        i32.and
                        i32.eq
                        br_if 4 (;@6;)
                        local.get 0
                        i32.const 29
                        i32.shr_u
                        local.set 202
                        local.get 0
                        i32.const 1
                        i32.shl
                        local.set 0
                        local.get 202
                        i32.const 4
                        i32.and
                        local.get 17
                        i32.add
                        local.tee 203
                        i32.const 16
                        i32.add
                        i32.load
                        local.tee 1
                        br_if 0 (;@10;)
                      end
                      local.get 203
                      local.get 6
                      i32.store offset=16
                      local.get 6
                      local.get 17
                      i32.store offset=24
                    end
                    local.get 6
                    local.get 6
                    i32.store offset=12
                    local.get 6
                    local.get 6
                    i32.store offset=8
                    br 3 (;@5;)
                  end
                  local.get 16
                  i32.load offset=8
                  local.tee 204
                  local.get 8
                  i32.store offset=12
                  local.get 16
                  local.get 8
                  i32.store offset=8
                  local.get 8
                  i32.const 0
                  i32.store offset=24
                  local.get 8
                  local.get 16
                  i32.store offset=12
                  local.get 8
                  local.get 204
                  i32.store offset=8
                end
                local.get 42
                i32.const 8
                i32.add
                local.set 0
                br 5 (;@1;)
              end
              local.get 17
              i32.load offset=8
              local.tee 205
              local.get 6
              i32.store offset=12
              local.get 17
              local.get 6
              i32.store offset=8
              local.get 6
              i32.const 0
              i32.store offset=24
              local.get 6
              local.get 17
              i32.store offset=12
              local.get 6
              local.get 205
              i32.store offset=8
            end
            i32.const 1048
            i32.load
            local.tee 206
            local.get 4
            i32.le_u
            br_if 0 (;@4;)
            i32.const 1048
            local.get 206
            local.get 4
            i32.sub
            local.tee 207
            i32.store
            i32.const 1060
            local.get 4
            i32.const 1060
            i32.load
            local.tee 74
            i32.add
            local.tee 208
            i32.store
            local.get 208
            local.get 207
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 74
            local.get 4
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 74
            i32.const 8
            i32.add
            local.set 0
            br 3 (;@1;)
          end
          call 15
          i32.const 48
          i32.store
          i32.const 0
          local.set 0
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 24
          i32.eqz
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 3
            local.get 3
            i32.load offset=28
            local.tee 209
            i32.const 2
            i32.shl
            i32.const 1340
            i32.add
            local.tee 210
            i32.load
            i32.eq
            if  ;; label = @5
              local.get 210
              local.get 1
              i32.store
              local.get 1
              br_if 1 (;@4;)
              i32.const 1040
              i32.const -2
              local.get 209
              i32.rotl
              local.get 14
              i32.and
              local.tee 14
              i32.store
              br 2 (;@3;)
            end
            i32.const 16
            i32.const 20
            local.get 3
            local.get 24
            i32.load offset=16
            i32.eq
            select
            local.get 24
            i32.add
            local.get 1
            i32.store
            local.get 1
            i32.eqz
            br_if 1 (;@3;)
          end
          local.get 1
          local.get 24
          i32.store offset=24
          local.get 3
          i32.load offset=16
          local.tee 75
          if  ;; label = @4
            local.get 1
            local.get 75
            i32.store offset=16
            local.get 75
            local.get 1
            i32.store offset=24
          end
          local.get 3
          i32.load offset=20
          local.tee 76
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          local.get 76
          i32.store offset=20
          local.get 76
          local.get 1
          i32.store offset=24
        end
        block  ;; label = @3
          local.get 7
          i32.const 15
          i32.le_u
          if  ;; label = @4
            local.get 3
            local.get 4
            local.get 7
            i32.add
            local.tee 211
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 3
            local.get 211
            i32.add
            local.tee 212
            local.get 212
            i32.load offset=4
            i32.const 1
            i32.or
            i32.store offset=4
            br 1 (;@3;)
          end
          local.get 3
          local.get 4
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 4
          local.get 3
          i32.add
          local.tee 9
          local.get 7
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 7
          local.get 9
          i32.add
          local.get 7
          i32.store
          local.get 7
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 7
            i32.const 3
            i32.shr_u
            local.tee 213
            i32.const 3
            i32.shl
            i32.const 1076
            i32.add
            local.set 32
            block  ;; label = @5
              i32.const 1036
              i32.load
              local.tee 214
              i32.const 1
              local.get 213
              i32.shl
              local.tee 215
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 1036
                local.get 214
                local.get 215
                i32.or
                i32.store
                local.get 32
                local.set 2
                br 1 (;@5;)
              end
              local.get 32
              i32.load offset=8
              local.set 2
            end
            local.get 32
            local.get 9
            i32.store offset=8
            local.get 2
            local.get 9
            i32.store offset=12
            local.get 9
            local.get 32
            i32.store offset=12
            local.get 9
            local.get 2
            i32.store offset=8
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 7
            i32.const 8
            i32.shr_u
            local.tee 77
            i32.eqz
            if  ;; label = @5
              i32.const 0
              local.set 0
              br 1 (;@4;)
            end
            i32.const 31
            local.set 0
            local.get 7
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            local.get 77
            local.get 77
            i32.const 1048320
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 8
            i32.and
            local.tee 216
            i32.shl
            local.tee 217
            local.get 217
            i32.const 520192
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 4
            i32.and
            local.tee 218
            i32.shl
            local.tee 219
            local.get 219
            i32.const 245760
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 2
            i32.and
            local.tee 220
            i32.shl
            i32.const 15
            i32.shr_u
            local.get 220
            local.get 216
            local.get 218
            i32.or
            i32.or
            i32.sub
            local.tee 221
            i32.const 1
            i32.shl
            local.get 7
            local.get 221
            i32.const 21
            i32.add
            i32.shr_u
            i32.const 1
            i32.and
            i32.or
            i32.const 28
            i32.add
            local.set 0
          end
          local.get 9
          local.get 0
          i32.store offset=28
          local.get 9
          i64.const 0
          i64.store offset=16 align=4
          local.get 0
          i32.const 2
          i32.shl
          i32.const 1340
          i32.add
          local.set 49
          block  ;; label = @4
            block  ;; label = @5
              i32.const 1
              local.get 0
              i32.shl
              local.tee 222
              local.get 14
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 1040
                local.get 14
                local.get 222
                i32.or
                i32.store
                local.get 49
                local.get 9
                i32.store
                local.get 9
                local.get 49
                i32.store offset=24
                br 1 (;@5;)
              end
              local.get 7
              i32.const 0
              i32.const 25
              local.get 0
              i32.const 1
              i32.shr_u
              i32.sub
              local.get 0
              i32.const 31
              i32.eq
              select
              i32.shl
              local.set 0
              local.get 49
              i32.load
              local.set 4
              loop  ;; label = @6
                local.get 7
                local.get 4
                local.tee 18
                i32.load offset=4
                i32.const -8
                i32.and
                i32.eq
                br_if 2 (;@4;)
                local.get 0
                i32.const 29
                i32.shr_u
                local.set 223
                local.get 0
                i32.const 1
                i32.shl
                local.set 0
                local.get 223
                i32.const 4
                i32.and
                local.get 18
                i32.add
                local.tee 224
                i32.const 16
                i32.add
                i32.load
                local.tee 4
                br_if 0 (;@6;)
              end
              local.get 224
              local.get 9
              i32.store offset=16
              local.get 9
              local.get 18
              i32.store offset=24
            end
            local.get 9
            local.get 9
            i32.store offset=12
            local.get 9
            local.get 9
            i32.store offset=8
            br 1 (;@3;)
          end
          local.get 18
          i32.load offset=8
          local.tee 225
          local.get 9
          i32.store offset=12
          local.get 18
          local.get 9
          i32.store offset=8
          local.get 9
          i32.const 0
          i32.store offset=24
          local.get 9
          local.get 18
          i32.store offset=12
          local.get 9
          local.get 225
          i32.store offset=8
        end
        local.get 3
        i32.const 8
        i32.add
        local.set 0
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 23
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 1
          local.get 1
          i32.load offset=28
          local.tee 226
          i32.const 2
          i32.shl
          i32.const 1340
          i32.add
          local.tee 227
          i32.load
          i32.eq
          if  ;; label = @4
            local.get 227
            local.get 3
            i32.store
            local.get 3
            br_if 1 (;@3;)
            i32.const 1040
            i32.const -2
            local.get 226
            i32.rotl
            local.get 38
            i32.and
            i32.store
            br 2 (;@2;)
          end
          i32.const 16
          i32.const 20
          local.get 1
          local.get 23
          i32.load offset=16
          i32.eq
          select
          local.get 23
          i32.add
          local.get 3
          i32.store
          local.get 3
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 3
        local.get 23
        i32.store offset=24
        local.get 1
        i32.load offset=16
        local.tee 78
        if  ;; label = @3
          local.get 3
          local.get 78
          i32.store offset=16
          local.get 78
          local.get 3
          i32.store offset=24
        end
        local.get 1
        i32.load offset=20
        local.tee 79
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 79
        i32.store offset=20
        local.get 79
        local.get 3
        i32.store offset=24
      end
      block  ;; label = @2
        local.get 2
        i32.const 15
        i32.le_u
        if  ;; label = @3
          local.get 1
          local.get 4
          local.get 2
          i32.add
          local.tee 228
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 1
          local.get 228
          i32.add
          local.tee 229
          local.get 229
          i32.load offset=4
          i32.const 1
          i32.or
          i32.store offset=4
          br 1 (;@2;)
        end
        local.get 1
        local.get 4
        i32.const 3
        i32.or
        i32.store offset=4
        local.get 4
        local.get 1
        i32.add
        local.tee 80
        local.get 2
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 2
        local.get 80
        i32.add
        local.get 2
        i32.store
        local.get 19
        if  ;; label = @3
          local.get 19
          i32.const 3
          i32.shr_u
          local.tee 230
          i32.const 3
          i32.shl
          i32.const 1076
          i32.add
          local.set 33
          i32.const 1056
          i32.load
          local.set 34
          block  ;; label = @4
            local.get 5
            i32.const 1
            local.get 230
            i32.shl
            local.tee 231
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 1036
              local.get 5
              local.get 231
              i32.or
              i32.store
              local.get 33
              local.set 3
              br 1 (;@4;)
            end
            local.get 33
            i32.load offset=8
            local.set 3
          end
          local.get 33
          local.get 34
          i32.store offset=8
          local.get 3
          local.get 34
          i32.store offset=12
          local.get 34
          local.get 33
          i32.store offset=12
          local.get 34
          local.get 3
          i32.store offset=8
        end
        i32.const 1056
        local.get 80
        i32.store
        i32.const 1044
        local.get 2
        i32.store
      end
      local.get 1
      i32.const 8
      i32.add
      local.set 0
    end
    block  ;; label = @1
      local.get 50
      i32.const 16
      i32.add
      local.tee 232
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 232
      global.set 0
    end
    local.get 0)
  (func (;18;) (type 2) (param i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const -8
      i32.add
      local.tee 1
      local.get 0
      i32.const -4
      i32.add
      i32.load
      local.tee 20
      i32.const -8
      i32.and
      local.tee 0
      i32.add
      local.set 4
      block  ;; label = @2
        local.get 20
        i32.const 1
        i32.and
        br_if 0 (;@2;)
        local.get 20
        i32.const 3
        i32.and
        i32.eqz
        br_if 1 (;@1;)
        local.get 1
        local.get 1
        i32.load
        local.tee 13
        i32.sub
        local.tee 1
        i32.const 1052
        i32.load
        local.tee 26
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 13
        i32.add
        local.set 0
        i32.const 1056
        i32.load
        local.get 1
        i32.ne
        if  ;; label = @3
          local.get 13
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 1
            i32.load offset=12
            local.set 8
            local.get 1
            i32.load offset=8
            local.tee 14
            local.get 13
            i32.const 3
            i32.shr_u
            local.tee 27
            i32.const 3
            i32.shl
            i32.const 1076
            i32.add
            local.tee 28
            i32.ne
            drop
            local.get 8
            local.get 14
            i32.eq
            if  ;; label = @5
              i32.const 1036
              i32.const 1036
              i32.load
              i32.const -2
              local.get 27
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            local.get 8
            local.get 28
            i32.ne
            drop
            local.get 14
            local.get 8
            i32.store offset=12
            local.get 8
            local.get 14
            i32.store offset=8
            br 2 (;@2;)
          end
          local.get 1
          i32.load offset=24
          local.set 9
          block  ;; label = @4
            local.get 1
            i32.load offset=12
            local.tee 2
            local.get 1
            i32.ne
            if  ;; label = @5
              local.get 26
              local.get 1
              i32.load offset=8
              local.tee 15
              i32.le_u
              if  ;; label = @6
                local.get 15
                i32.load offset=12
                drop
              end
              local.get 15
              local.get 2
              i32.store offset=12
              local.get 2
              local.get 15
              i32.store offset=8
              br 1 (;@4;)
            end
            block  ;; label = @5
              local.get 1
              i32.const 20
              i32.add
              local.tee 3
              i32.load
              local.tee 5
              br_if 0 (;@5;)
              local.get 1
              i32.const 16
              i32.add
              local.tee 3
              i32.load
              local.tee 5
              br_if 0 (;@5;)
              i32.const 0
              local.set 2
              br 1 (;@4;)
            end
            loop  ;; label = @5
              local.get 3
              local.set 29
              local.get 5
              local.tee 2
              i32.const 20
              i32.add
              local.tee 3
              i32.load
              local.tee 5
              br_if 0 (;@5;)
              local.get 2
              i32.const 16
              i32.add
              local.set 3
              local.get 2
              i32.load offset=16
              local.tee 5
              br_if 0 (;@5;)
            end
            local.get 29
            i32.const 0
            i32.store
          end
          local.get 9
          i32.eqz
          br_if 1 (;@2;)
          block  ;; label = @4
            local.get 1
            i32.load offset=28
            local.tee 30
            i32.const 2
            i32.shl
            i32.const 1340
            i32.add
            local.tee 31
            i32.load
            local.get 1
            i32.eq
            if  ;; label = @5
              local.get 31
              local.get 2
              i32.store
              local.get 2
              br_if 1 (;@4;)
              i32.const 1040
              i32.const 1040
              i32.load
              i32.const -2
              local.get 30
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            i32.const 16
            i32.const 20
            local.get 1
            local.get 9
            i32.load offset=16
            i32.eq
            select
            local.get 9
            i32.add
            local.get 2
            i32.store
            local.get 2
            i32.eqz
            br_if 2 (;@2;)
          end
          local.get 2
          local.get 9
          i32.store offset=24
          local.get 1
          i32.load offset=16
          local.tee 21
          if  ;; label = @4
            local.get 2
            local.get 21
            i32.store offset=16
            local.get 21
            local.get 2
            i32.store offset=24
          end
          local.get 1
          i32.load offset=20
          local.tee 22
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          local.get 22
          i32.store offset=20
          local.get 22
          local.get 2
          i32.store offset=24
          br 1 (;@2;)
        end
        local.get 4
        i32.load offset=4
        local.tee 32
        i32.const 3
        i32.and
        i32.const 3
        i32.ne
        br_if 0 (;@2;)
        i32.const 1044
        local.get 0
        i32.store
        local.get 4
        local.get 32
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 1
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 1
        i32.add
        local.get 0
        i32.store
        return
      end
      local.get 4
      local.get 1
      i32.le_u
      br_if 0 (;@1;)
      local.get 4
      i32.load offset=4
      local.tee 6
      i32.const 1
      i32.and
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 6
        i32.const 2
        i32.and
        i32.eqz
        if  ;; label = @3
          i32.const 1060
          i32.load
          local.get 4
          i32.eq
          if  ;; label = @4
            i32.const 1060
            local.get 1
            i32.store
            i32.const 1048
            local.get 0
            i32.const 1048
            i32.load
            i32.add
            local.tee 33
            i32.store
            local.get 1
            local.get 33
            i32.const 1
            i32.or
            i32.store offset=4
            i32.const 1056
            i32.load
            local.get 1
            i32.ne
            br_if 3 (;@1;)
            i32.const 1044
            i32.const 0
            i32.store
            i32.const 1056
            i32.const 0
            i32.store
            return
          end
          i32.const 1056
          i32.load
          local.get 4
          i32.eq
          if  ;; label = @4
            i32.const 1056
            local.get 1
            i32.store
            i32.const 1044
            local.get 0
            i32.const 1044
            i32.load
            i32.add
            local.tee 16
            i32.store
            local.get 1
            local.get 16
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 1
            local.get 16
            i32.add
            local.get 16
            i32.store
            return
          end
          local.get 0
          local.get 6
          i32.const -8
          i32.and
          i32.add
          local.set 0
          block  ;; label = @4
            local.get 6
            i32.const 255
            i32.le_u
            if  ;; label = @5
              local.get 4
              i32.load offset=12
              local.set 10
              local.get 4
              i32.load offset=8
              local.tee 17
              local.get 6
              i32.const 3
              i32.shr_u
              local.tee 34
              i32.const 3
              i32.shl
              i32.const 1076
              i32.add
              local.tee 35
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                drop
              end
              local.get 10
              local.get 17
              i32.eq
              if  ;; label = @6
                i32.const 1036
                i32.const 1036
                i32.load
                i32.const -2
                local.get 34
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              local.get 10
              local.get 35
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                drop
              end
              local.get 17
              local.get 10
              i32.store offset=12
              local.get 10
              local.get 17
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 4
            i32.load offset=24
            local.set 11
            block  ;; label = @5
              local.get 4
              i32.load offset=12
              local.tee 2
              local.get 4
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                local.get 4
                i32.load offset=8
                local.tee 18
                i32.le_u
                if  ;; label = @7
                  local.get 18
                  i32.load offset=12
                  drop
                end
                local.get 18
                local.get 2
                i32.store offset=12
                local.get 2
                local.get 18
                i32.store offset=8
                br 1 (;@5;)
              end
              block  ;; label = @6
                local.get 4
                i32.const 20
                i32.add
                local.tee 3
                i32.load
                local.tee 5
                br_if 0 (;@6;)
                local.get 4
                i32.const 16
                i32.add
                local.tee 3
                i32.load
                local.tee 5
                br_if 0 (;@6;)
                i32.const 0
                local.set 2
                br 1 (;@5;)
              end
              loop  ;; label = @6
                local.get 3
                local.set 36
                local.get 5
                local.tee 2
                i32.const 20
                i32.add
                local.tee 3
                i32.load
                local.tee 5
                br_if 0 (;@6;)
                local.get 2
                i32.const 16
                i32.add
                local.set 3
                local.get 2
                i32.load offset=16
                local.tee 5
                br_if 0 (;@6;)
              end
              local.get 36
              i32.const 0
              i32.store
            end
            local.get 11
            i32.eqz
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 4
              i32.load offset=28
              local.tee 37
              i32.const 2
              i32.shl
              i32.const 1340
              i32.add
              local.tee 38
              i32.load
              local.get 4
              i32.eq
              if  ;; label = @6
                local.get 38
                local.get 2
                i32.store
                local.get 2
                br_if 1 (;@5;)
                i32.const 1040
                i32.const 1040
                i32.load
                i32.const -2
                local.get 37
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              i32.const 16
              i32.const 20
              local.get 4
              local.get 11
              i32.load offset=16
              i32.eq
              select
              local.get 11
              i32.add
              local.get 2
              i32.store
              local.get 2
              i32.eqz
              br_if 1 (;@4;)
            end
            local.get 2
            local.get 11
            i32.store offset=24
            local.get 4
            i32.load offset=16
            local.tee 23
            if  ;; label = @5
              local.get 2
              local.get 23
              i32.store offset=16
              local.get 23
              local.get 2
              i32.store offset=24
            end
            local.get 4
            i32.load offset=20
            local.tee 24
            i32.eqz
            br_if 0 (;@4;)
            local.get 2
            local.get 24
            i32.store offset=20
            local.get 24
            local.get 2
            i32.store offset=24
          end
          local.get 1
          local.get 0
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          local.get 1
          i32.add
          local.get 0
          i32.store
          i32.const 1056
          i32.load
          local.get 1
          i32.ne
          br_if 1 (;@2;)
          i32.const 1044
          local.get 0
          i32.store
          return
        end
        local.get 4
        local.get 6
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 1
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 1
        i32.add
        local.get 0
        i32.store
      end
      local.get 0
      i32.const 255
      i32.le_u
      if  ;; label = @2
        local.get 0
        i32.const 3
        i32.shr_u
        local.tee 39
        i32.const 3
        i32.shl
        i32.const 1076
        i32.add
        local.set 12
        block  ;; label = @3
          i32.const 1036
          i32.load
          local.tee 40
          i32.const 1
          local.get 39
          i32.shl
          local.tee 41
          i32.and
          i32.eqz
          if  ;; label = @4
            i32.const 1036
            local.get 40
            local.get 41
            i32.or
            i32.store
            local.get 12
            local.set 3
            br 1 (;@3;)
          end
          local.get 12
          i32.load offset=8
          local.set 3
        end
        local.get 12
        local.get 1
        i32.store offset=8
        local.get 3
        local.get 1
        i32.store offset=12
        local.get 1
        local.get 12
        i32.store offset=12
        local.get 1
        local.get 3
        i32.store offset=8
        return
      end
      i32.const 0
      local.set 3
      block  ;; label = @2
        local.get 0
        i32.const 8
        i32.shr_u
        local.tee 25
        i32.eqz
        br_if 0 (;@2;)
        i32.const 31
        local.set 3
        local.get 0
        i32.const 16777215
        i32.gt_u
        br_if 0 (;@2;)
        local.get 25
        local.get 25
        i32.const 1048320
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 8
        i32.and
        local.tee 42
        i32.shl
        local.tee 43
        local.get 43
        i32.const 520192
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 4
        i32.and
        local.tee 44
        i32.shl
        local.tee 45
        local.get 45
        i32.const 245760
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 2
        i32.and
        local.tee 46
        i32.shl
        i32.const 15
        i32.shr_u
        local.get 46
        local.get 42
        local.get 44
        i32.or
        i32.or
        i32.sub
        local.tee 47
        i32.const 1
        i32.shl
        local.get 0
        local.get 47
        i32.const 21
        i32.add
        i32.shr_u
        i32.const 1
        i32.and
        i32.or
        i32.const 28
        i32.add
        local.set 3
      end
      local.get 1
      i64.const 0
      i64.store offset=16 align=4
      local.get 1
      local.get 3
      i32.store offset=28
      local.get 3
      i32.const 2
      i32.shl
      i32.const 1340
      i32.add
      local.set 19
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            i32.const 1040
            i32.load
            local.tee 48
            i32.const 1
            local.get 3
            i32.shl
            local.tee 49
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 1040
              local.get 48
              local.get 49
              i32.or
              i32.store
              local.get 19
              local.get 1
              i32.store
              local.get 1
              local.get 19
              i32.store offset=24
              br 1 (;@4;)
            end
            local.get 0
            i32.const 0
            i32.const 25
            local.get 3
            i32.const 1
            i32.shr_u
            i32.sub
            local.get 3
            i32.const 31
            i32.eq
            select
            i32.shl
            local.set 3
            local.get 19
            i32.load
            local.set 2
            loop  ;; label = @5
              local.get 0
              local.get 2
              local.tee 7
              i32.load offset=4
              i32.const -8
              i32.and
              i32.eq
              br_if 2 (;@3;)
              local.get 3
              i32.const 29
              i32.shr_u
              local.set 50
              local.get 3
              i32.const 1
              i32.shl
              local.set 3
              local.get 50
              i32.const 4
              i32.and
              local.get 7
              i32.add
              local.tee 51
              i32.const 16
              i32.add
              i32.load
              local.tee 2
              br_if 0 (;@5;)
            end
            local.get 51
            local.get 1
            i32.store offset=16
            local.get 1
            local.get 7
            i32.store offset=24
          end
          local.get 1
          local.get 1
          i32.store offset=12
          local.get 1
          local.get 1
          i32.store offset=8
          br 1 (;@2;)
        end
        local.get 7
        i32.load offset=8
        local.tee 52
        local.get 1
        i32.store offset=12
        local.get 7
        local.get 1
        i32.store offset=8
        local.get 1
        i32.const 0
        i32.store offset=24
        local.get 1
        local.get 7
        i32.store offset=12
        local.get 1
        local.get 52
        i32.store offset=8
      end
      i32.const 1068
      i32.const 1068
      i32.load
      i32.const -1
      i32.add
      local.tee 53
      i32.store
      local.get 53
      br_if 0 (;@1;)
      i32.const 1492
      local.set 1
      loop  ;; label = @2
        local.get 1
        i32.load
        local.tee 54
        i32.const 8
        i32.add
        local.set 1
        local.get 54
        br_if 0 (;@2;)
      end
      i32.const 1068
      i32.const -1
      i32.store
    end)
  (func (;19;) (type 1) (result i32)
    global.get 0)
  (func (;20;) (type 2) (param i32)
    (local i32)
    local.get 0
    local.tee 1
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 1
    global.set 0)
  (func (;21;) (type 0) (param i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      global.get 0
      local.get 0
      i32.sub
      i32.const -16
      i32.and
      local.tee 1
      local.tee 2
      global.get 2
      i32.lt_u
      if  ;; label = @2
        call 1
      end
      local.get 2
      global.set 0
    end
    local.get 1)
  (func (;22;) (type 0) (param i32) (result i32)
    i32.const 1)
  (func (;23;) (type 2) (param i32)
    nop)
  (func (;24;) (type 1) (result i32)
    i32.const 1532
    call 23
    i32.const 1540)
  (func (;25;) (type 3)
    i32.const 1532
    call 23)
  (func (;26;) (type 0) (param i32) (result i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 0
      if  ;; label = @2
        local.get 0
        i32.load offset=76
        i32.const -1
        i32.le_s
        if  ;; label = @3
          local.get 0
          call 27
          return
        end
        local.get 0
        call 22
        local.set 3
        local.get 0
        call 27
        local.set 1
        local.get 3
        i32.eqz
        br_if 1 (;@1;)
        local.get 0
        call 23
        local.get 1
        return
      end
      i32.const 0
      local.set 1
      i32.const 1544
      i32.load
      if  ;; label = @2
        i32.const 1544
        i32.load
        call 26
        local.set 1
      end
      call 24
      i32.load
      local.tee 0
      if  ;; label = @2
        loop  ;; label = @3
          i32.const 0
          local.set 2
          local.get 0
          i32.load offset=76
          i32.const 0
          i32.ge_s
          if  ;; label = @4
            local.get 0
            call 22
            local.set 2
          end
          local.get 0
          i32.load offset=20
          local.get 0
          i32.load offset=28
          i32.gt_u
          if  ;; label = @4
            local.get 0
            call 27
            local.get 1
            i32.or
            local.set 1
          end
          local.get 2
          if  ;; label = @4
            local.get 0
            call 23
          end
          local.get 0
          i32.load offset=56
          local.tee 0
          br_if 0 (;@3;)
        end
      end
      call 25
    end
    local.get 1)
  (func (;27;) (type 0) (param i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=20
      local.get 0
      i32.load offset=28
      i32.le_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 0
      i32.const 0
      local.get 0
      i32.load offset=36
      call_indirect (type 10)
      drop
      local.get 0
      i32.load offset=20
      br_if 0 (;@1;)
      i32.const -1
      return
    end
    local.get 0
    i32.load offset=4
    local.tee 1
    local.get 0
    i32.load offset=8
    local.tee 2
    i32.lt_u
    if  ;; label = @1
      local.get 0
      local.get 1
      local.get 2
      i32.sub
      i64.extend_i32_s
      i32.const 1
      local.get 0
      i32.load offset=40
      call_indirect (type 12)
      drop
    end
    local.get 0
    i32.const 0
    i32.store offset=28
    local.get 0
    i64.const 0
    i64.store offset=16
    local.get 0
    i64.const 0
    i64.store offset=4 align=4
    i32.const 0)
  (func (;28;) (type 2) (param i32)
    local.get 0
    global.set 2)
  (func (;29;) (type 0) (param i32) (result i32)
    local.get 0
    memory.grow)
  (global (;0;) (mut i32) (i32.const 5244592))
  (global (;1;) i32 (i32.const 1548))
  (global (;2;) (mut i32) (i32.const 0))
  (export "__wasm_call_ctors" (func 3))
  (export "grayScale" (func 5))
  (export "brighten" (func 6))
  (export "invert" (func 7))
  (export "noise" (func 8))
  (export "multiFilter" (func 9))
  (export "sobelFilter" (func 10))
  (export "convFilter" (func 12))
  (export "fflush" (func 26))
  (export "__errno_location" (func 15))
  (export "stackSave" (func 19))
  (export "stackRestore" (func 20))
  (export "stackAlloc" (func 21))
  (export "malloc" (func 17))
  (export "free" (func 18))
  (export "__data_end" (global 1))
  (export "__set_stack_limit" (func 28))
  (export "__growWasmMemory" (func 29)))
