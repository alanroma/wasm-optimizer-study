; ModuleID = './cpp/webdsp.cpp'
source_filename = "./cpp/webdsp.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

$_Z4sqrtIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_ = comdat any

$_Z5floorIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_ = comdat any

; Function Attrs: noinline nounwind optnone
define hidden i32 @getPixel(i32 %x, i32 %y, i32* %arr, i32 %width, i32 %height) #0 {
entry:
  %retval = alloca i32, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %arr.addr = alloca i32*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  store i32* %arr, i32** %arr.addr, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %y.addr, align 4
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %width.addr, align 4
  %cmp2 = icmp sge i32 %2, %3
  br i1 %cmp2, label %if.then5, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %if.end
  %4 = load i32, i32* %y.addr, align 4
  %5 = load i32, i32* %height.addr, align 4
  %cmp4 = icmp sge i32 %4, %5
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %lor.lhs.false3, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %lor.lhs.false3
  %6 = load i32*, i32** %arr.addr, align 4
  %7 = load i32, i32* %width.addr, align 4
  %8 = load i32, i32* %y.addr, align 4
  %mul = mul nsw i32 %7, %8
  %9 = load i32, i32* %x.addr, align 4
  %add = add nsw i32 %mul, %9
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %add
  %10 = load i32, i32* %arrayidx, align 4
  store i32 %10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define hidden void @grayScale(i8* %data, i32 %len) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %data.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %r, align 4
  %5 = load i8*, i8** %data.addr, align 4
  %6 = load i32, i32* %i, align 4
  %add = add nsw i32 %6, 1
  %arrayidx1 = getelementptr inbounds i8, i8* %5, i32 %add
  %7 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %7 to i32
  store i32 %conv2, i32* %g, align 4
  %8 = load i8*, i8** %data.addr, align 4
  %9 = load i32, i32* %i, align 4
  %add3 = add nsw i32 %9, 2
  %arrayidx4 = getelementptr inbounds i8, i8* %8, i32 %add3
  %10 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %10 to i32
  store i32 %conv5, i32* %b, align 4
  %11 = load i8*, i8** %data.addr, align 4
  %12 = load i32, i32* %i, align 4
  %add6 = add nsw i32 %12, 3
  %arrayidx7 = getelementptr inbounds i8, i8* %11, i32 %add6
  %13 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %13 to i32
  store i32 %conv8, i32* %a, align 4
  %14 = load i32, i32* %r, align 4
  %conv9 = trunc i32 %14 to i8
  %15 = load i8*, i8** %data.addr, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %15, i32 %16
  store i8 %conv9, i8* %arrayidx10, align 1
  %17 = load i32, i32* %r, align 4
  %conv11 = trunc i32 %17 to i8
  %18 = load i8*, i8** %data.addr, align 4
  %19 = load i32, i32* %i, align 4
  %add12 = add nsw i32 %19, 1
  %arrayidx13 = getelementptr inbounds i8, i8* %18, i32 %add12
  store i8 %conv11, i8* %arrayidx13, align 1
  %20 = load i32, i32* %r, align 4
  %conv14 = trunc i32 %20 to i8
  %21 = load i8*, i8** %data.addr, align 4
  %22 = load i32, i32* %i, align 4
  %add15 = add nsw i32 %22, 2
  %arrayidx16 = getelementptr inbounds i8, i8* %21, i32 %add15
  store i8 %conv14, i8* %arrayidx16, align 1
  %23 = load i32, i32* %a, align 4
  %conv17 = trunc i32 %23 to i8
  %24 = load i8*, i8** %data.addr, align 4
  %25 = load i32, i32* %i, align 4
  %add18 = add nsw i32 %25, 3
  %arrayidx19 = getelementptr inbounds i8, i8* %24, i32 %add18
  store i8 %conv17, i8* %arrayidx19, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4
  %add20 = add nsw i32 %26, 4
  store i32 %add20, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @brighten(i8* %data, i32 %len, i32 %brightness) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %brightness.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 %brightness, i32* %brightness.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %data.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i32, i32* %brightness.addr, align 4
  %add = add nsw i32 %conv, %5
  %cmp1 = icmp sgt i32 %add, 255
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %6 = load i32, i32* %brightness.addr, align 4
  %7 = load i8*, i8** %data.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %9 to i32
  %add4 = add nsw i32 %conv3, %6
  %conv5 = trunc i32 %add4 to i8
  store i8 %conv5, i8* %arrayidx2, align 1
  %conv6 = zext i8 %conv5 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 255, %cond.true ], [ %conv6, %cond.false ]
  %10 = load i8*, i8** %data.addr, align 4
  %11 = load i32, i32* %i, align 4
  %add7 = add nsw i32 %11, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %10, i32 %add7
  %12 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %12 to i32
  %13 = load i32, i32* %brightness.addr, align 4
  %add10 = add nsw i32 %conv9, %13
  %cmp11 = icmp sgt i32 %add10, 255
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %cond.end
  br label %cond.end20

cond.false13:                                     ; preds = %cond.end
  %14 = load i32, i32* %brightness.addr, align 4
  %15 = load i8*, i8** %data.addr, align 4
  %16 = load i32, i32* %i, align 4
  %add14 = add nsw i32 %16, 1
  %arrayidx15 = getelementptr inbounds i8, i8* %15, i32 %add14
  %17 = load i8, i8* %arrayidx15, align 1
  %conv16 = zext i8 %17 to i32
  %add17 = add nsw i32 %conv16, %14
  %conv18 = trunc i32 %add17 to i8
  store i8 %conv18, i8* %arrayidx15, align 1
  %conv19 = zext i8 %conv18 to i32
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false13, %cond.true12
  %cond21 = phi i32 [ 255, %cond.true12 ], [ %conv19, %cond.false13 ]
  %18 = load i8*, i8** %data.addr, align 4
  %19 = load i32, i32* %i, align 4
  %add22 = add nsw i32 %19, 2
  %arrayidx23 = getelementptr inbounds i8, i8* %18, i32 %add22
  %20 = load i8, i8* %arrayidx23, align 1
  %conv24 = zext i8 %20 to i32
  %21 = load i32, i32* %brightness.addr, align 4
  %add25 = add nsw i32 %conv24, %21
  %cmp26 = icmp sgt i32 %add25, 255
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %cond.end20
  br label %cond.end35

cond.false28:                                     ; preds = %cond.end20
  %22 = load i32, i32* %brightness.addr, align 4
  %23 = load i8*, i8** %data.addr, align 4
  %24 = load i32, i32* %i, align 4
  %add29 = add nsw i32 %24, 2
  %arrayidx30 = getelementptr inbounds i8, i8* %23, i32 %add29
  %25 = load i8, i8* %arrayidx30, align 1
  %conv31 = zext i8 %25 to i32
  %add32 = add nsw i32 %conv31, %22
  %conv33 = trunc i32 %add32 to i8
  store i8 %conv33, i8* %arrayidx30, align 1
  %conv34 = zext i8 %conv33 to i32
  br label %cond.end35

cond.end35:                                       ; preds = %cond.false28, %cond.true27
  %cond36 = phi i32 [ 255, %cond.true27 ], [ %conv34, %cond.false28 ]
  br label %for.inc

for.inc:                                          ; preds = %cond.end35
  %26 = load i32, i32* %i, align 4
  %add37 = add nsw i32 %26, 4
  store i32 %add37, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @invert(i8* %data, i32 %len) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %data.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %sub = sub nsw i32 255, %conv
  %conv1 = trunc i32 %sub to i8
  %5 = load i8*, i8** %data.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 %6
  store i8 %conv1, i8* %arrayidx2, align 1
  %7 = load i8*, i8** %data.addr, align 4
  %8 = load i32, i32* %i, align 4
  %add = add nsw i32 %8, 1
  %arrayidx3 = getelementptr inbounds i8, i8* %7, i32 %add
  %9 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %9 to i32
  %sub5 = sub nsw i32 255, %conv4
  %conv6 = trunc i32 %sub5 to i8
  %10 = load i8*, i8** %data.addr, align 4
  %11 = load i32, i32* %i, align 4
  %add7 = add nsw i32 %11, 1
  %arrayidx8 = getelementptr inbounds i8, i8* %10, i32 %add7
  store i8 %conv6, i8* %arrayidx8, align 1
  %12 = load i8*, i8** %data.addr, align 4
  %13 = load i32, i32* %i, align 4
  %add9 = add nsw i32 %13, 2
  %arrayidx10 = getelementptr inbounds i8, i8* %12, i32 %add9
  %14 = load i8, i8* %arrayidx10, align 1
  %conv11 = zext i8 %14 to i32
  %sub12 = sub nsw i32 255, %conv11
  %conv13 = trunc i32 %sub12 to i8
  %15 = load i8*, i8** %data.addr, align 4
  %16 = load i32, i32* %i, align 4
  %add14 = add nsw i32 %16, 2
  %arrayidx15 = getelementptr inbounds i8, i8* %15, i32 %add14
  store i8 %conv13, i8* %arrayidx15, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4
  %add16 = add nsw i32 %17, 4
  store i32 %add16, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @noise(float* %data, i32 %len) #1 {
entry:
  %data.addr = alloca float*, align 4
  %len.addr = alloca i32, align 4
  %random = alloca i32, align 4
  %i = alloca i32, align 4
  store float* %data, float** %data.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call = call i32 @rand()
  %rem = srem i32 %call, 70
  %sub = sub nsw i32 %rem, 35
  store i32 %sub, i32* %random, align 4
  %2 = load float*, float** %data.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load i32, i32* %random, align 4
  %conv = sitofp i32 %5 to float
  %add = fadd float %4, %conv
  %6 = load float*, float** %data.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds float, float* %6, i32 %7
  store float %add, float* %arrayidx1, align 4
  %8 = load float*, float** %data.addr, align 4
  %9 = load i32, i32* %i, align 4
  %add2 = add nsw i32 %9, 1
  %arrayidx3 = getelementptr inbounds float, float* %8, i32 %add2
  %10 = load float, float* %arrayidx3, align 4
  %11 = load i32, i32* %random, align 4
  %conv4 = sitofp i32 %11 to float
  %add5 = fadd float %10, %conv4
  %12 = load float*, float** %data.addr, align 4
  %13 = load i32, i32* %i, align 4
  %add6 = add nsw i32 %13, 1
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 %add6
  store float %add5, float* %arrayidx7, align 4
  %14 = load float*, float** %data.addr, align 4
  %15 = load i32, i32* %i, align 4
  %add8 = add nsw i32 %15, 2
  %arrayidx9 = getelementptr inbounds float, float* %14, i32 %add8
  %16 = load float, float* %arrayidx9, align 4
  %17 = load i32, i32* %random, align 4
  %conv10 = sitofp i32 %17 to float
  %add11 = fadd float %16, %conv10
  %18 = load float*, float** %data.addr, align 4
  %19 = load i32, i32* %i, align 4
  %add12 = add nsw i32 %19, 2
  %arrayidx13 = getelementptr inbounds float, float* %18, i32 %add12
  store float %add11, float* %arrayidx13, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %add14 = add nsw i32 %20, 4
  store i32 %add14, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare i32 @rand() #2

; Function Attrs: noinline nounwind optnone
define hidden void @multiFilter(i8* %data, i32 %len, i32 %width, i32 %filterType, i32 %mag, i32 %mult, i32 %adj) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %filterType.addr = alloca i32, align 4
  %mag.addr = alloca i32, align 4
  %mult.addr = alloca i32, align 4
  %adj.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %filterType, i32* %filterType.addr, align 4
  store i32 %mag, i32* %mag.addr, align 4
  store i32 %mult, i32* %mult.addr, align 4
  store i32 %adj, i32* %adj.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %rem = srem i32 %2, 4
  %cmp1 = icmp ne i32 %rem, 3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %3 = load i32, i32* %mag.addr, align 4
  %4 = load i32, i32* %mult.addr, align 4
  %5 = load i8*, i8** %data.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %7 to i32
  %mul = mul nsw i32 %4, %conv
  %add = add nsw i32 %3, %mul
  %8 = load i8*, i8** %data.addr, align 4
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %adj.addr, align 4
  %add2 = add nsw i32 %9, %10
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 %add2
  %11 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %11 to i32
  %sub = sub nsw i32 %add, %conv4
  %12 = load i8*, i8** %data.addr, align 4
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %width.addr, align 4
  %mul5 = mul nsw i32 %14, 4
  %add6 = add nsw i32 %13, %mul5
  %arrayidx7 = getelementptr inbounds i8, i8* %12, i32 %add6
  %15 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %15 to i32
  %sub9 = sub nsw i32 %sub, %conv8
  %conv10 = trunc i32 %sub9 to i8
  %16 = load i8*, i8** %data.addr, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %16, i32 %17
  store i8 %conv10, i8* %arrayidx11, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %filterType.addr, align 4
  %19 = load i32, i32* %i, align 4
  %add12 = add nsw i32 %19, %18
  store i32 %add12, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @sobelFilter(i8* %data, i32 %width, i32 %height, i1 zeroext %invert) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %invert.addr = alloca i8, align 1
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %goffset = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %avg = alloca i32, align 4
  %doffset = alloca i32, align 4
  %y34 = alloca i32, align 4
  %x38 = alloca i32, align 4
  %newX = alloca i32, align 4
  %newY = alloca i32, align 4
  %mag = alloca i32, align 4
  %offset = alloca i32, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  %frombool = zext i1 %invert to i8
  store i8 %frombool, i8* %invert.addr, align 1
  %0 = load i32, i32* %width.addr, align 4
  %1 = load i32, i32* %height.addr, align 4
  %mul = mul nsw i32 %0, %1
  %2 = call i8* @llvm.stacksave()
  store i8* %2, i8** %saved_stack, align 4
  %vla = alloca i32, i32 %mul, align 16
  store i32 %mul, i32* %__vla_expr0, align 4
  store i32 0, i32* %y, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc31, %entry
  %3 = load i32, i32* %y, align 4
  %4 = load i32, i32* %height.addr, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end33

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %x, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %x, align 4
  %6 = load i32, i32* %width.addr, align 4
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %7 = load i32, i32* %width.addr, align 4
  %8 = load i32, i32* %y, align 4
  %mul4 = mul nsw i32 %7, %8
  %9 = load i32, i32* %x, align 4
  %add = add nsw i32 %mul4, %9
  %shl = shl i32 %add, 2
  store i32 %shl, i32* %goffset, align 4
  %10 = load i8*, i8** %data.addr, align 4
  %11 = load i32, i32* %goffset, align 4
  %arrayidx = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %12 to i32
  store i32 %conv, i32* %r, align 4
  %13 = load i8*, i8** %data.addr, align 4
  %14 = load i32, i32* %goffset, align 4
  %add5 = add nsw i32 %14, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %13, i32 %add5
  %15 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %15 to i32
  store i32 %conv7, i32* %g, align 4
  %16 = load i8*, i8** %data.addr, align 4
  %17 = load i32, i32* %goffset, align 4
  %add8 = add nsw i32 %17, 2
  %arrayidx9 = getelementptr inbounds i8, i8* %16, i32 %add8
  %18 = load i8, i8* %arrayidx9, align 1
  %conv10 = zext i8 %18 to i32
  store i32 %conv10, i32* %b, align 4
  %19 = load i32, i32* %r, align 4
  %shr = ashr i32 %19, 2
  %20 = load i32, i32* %g, align 4
  %shr11 = ashr i32 %20, 1
  %add12 = add nsw i32 %shr, %shr11
  %21 = load i32, i32* %b, align 4
  %shr13 = ashr i32 %21, 3
  %add14 = add nsw i32 %add12, %shr13
  store i32 %add14, i32* %avg, align 4
  %22 = load i32, i32* %avg, align 4
  %23 = load i32, i32* %width.addr, align 4
  %24 = load i32, i32* %y, align 4
  %mul15 = mul nsw i32 %23, %24
  %25 = load i32, i32* %x, align 4
  %add16 = add nsw i32 %mul15, %25
  %arrayidx17 = getelementptr inbounds i32, i32* %vla, i32 %add16
  store i32 %22, i32* %arrayidx17, align 4
  %26 = load i32, i32* %width.addr, align 4
  %27 = load i32, i32* %y, align 4
  %mul18 = mul nsw i32 %26, %27
  %28 = load i32, i32* %x, align 4
  %add19 = add nsw i32 %mul18, %28
  %shl20 = shl i32 %add19, 2
  store i32 %shl20, i32* %doffset, align 4
  %29 = load i32, i32* %avg, align 4
  %conv21 = trunc i32 %29 to i8
  %30 = load i8*, i8** %data.addr, align 4
  %31 = load i32, i32* %doffset, align 4
  %arrayidx22 = getelementptr inbounds i8, i8* %30, i32 %31
  store i8 %conv21, i8* %arrayidx22, align 1
  %32 = load i32, i32* %avg, align 4
  %conv23 = trunc i32 %32 to i8
  %33 = load i8*, i8** %data.addr, align 4
  %34 = load i32, i32* %doffset, align 4
  %add24 = add nsw i32 %34, 1
  %arrayidx25 = getelementptr inbounds i8, i8* %33, i32 %add24
  store i8 %conv23, i8* %arrayidx25, align 1
  %35 = load i32, i32* %avg, align 4
  %conv26 = trunc i32 %35 to i8
  %36 = load i8*, i8** %data.addr, align 4
  %37 = load i32, i32* %doffset, align 4
  %add27 = add nsw i32 %37, 2
  %arrayidx28 = getelementptr inbounds i8, i8* %36, i32 %add27
  store i8 %conv26, i8* %arrayidx28, align 1
  %38 = load i8*, i8** %data.addr, align 4
  %39 = load i32, i32* %doffset, align 4
  %add29 = add nsw i32 %39, 3
  %arrayidx30 = getelementptr inbounds i8, i8* %38, i32 %add29
  store i8 -1, i8* %arrayidx30, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %40 = load i32, i32* %x, align 4
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %x, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc31

for.inc31:                                        ; preds = %for.end
  %41 = load i32, i32* %y, align 4
  %inc32 = add nsw i32 %41, 1
  store i32 %inc32, i32* %y, align 4
  br label %for.cond

for.end33:                                        ; preds = %for.cond
  store i32 0, i32* %y34, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc129, %for.end33
  %42 = load i32, i32* %y34, align 4
  %43 = load i32, i32* %height.addr, align 4
  %cmp36 = icmp slt i32 %42, %43
  br i1 %cmp36, label %for.body37, label %for.end131

for.body37:                                       ; preds = %for.cond35
  store i32 0, i32* %x38, align 4
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc126, %for.body37
  %44 = load i32, i32* %x38, align 4
  %45 = load i32, i32* %width.addr, align 4
  %cmp40 = icmp slt i32 %44, %45
  br i1 %cmp40, label %for.body41, label %for.end128

for.body41:                                       ; preds = %for.cond39
  %46 = load i32, i32* %x38, align 4
  %cmp42 = icmp sle i32 %46, 0
  br i1 %cmp42, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body41
  %47 = load i32, i32* %x38, align 4
  %48 = load i32, i32* %width.addr, align 4
  %sub = sub nsw i32 %48, 1
  %cmp43 = icmp sge i32 %47, %sub
  br i1 %cmp43, label %if.then, label %lor.lhs.false44

lor.lhs.false44:                                  ; preds = %lor.lhs.false
  %49 = load i32, i32* %y34, align 4
  %cmp45 = icmp sle i32 %49, 0
  br i1 %cmp45, label %if.then, label %lor.lhs.false46

lor.lhs.false46:                                  ; preds = %lor.lhs.false44
  %50 = load i32, i32* %y34, align 4
  %51 = load i32, i32* %height.addr, align 4
  %sub47 = sub nsw i32 %51, 1
  %cmp48 = icmp sge i32 %50, %sub47
  br i1 %cmp48, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false46, %lor.lhs.false44, %lor.lhs.false, %for.body41
  store i32 0, i32* %newX, align 4
  store i32 0, i32* %newY, align 4
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false46
  %52 = load i32, i32* %x38, align 4
  %sub49 = sub nsw i32 %52, 1
  %53 = load i32, i32* %y34, align 4
  %sub50 = sub nsw i32 %53, 1
  %54 = load i32, i32* %width.addr, align 4
  %55 = load i32, i32* %height.addr, align 4
  %call = call i32 @getPixel(i32 %sub49, i32 %sub50, i32* %vla, i32 %54, i32 %55)
  %mul51 = mul nsw i32 -1, %call
  %56 = load i32, i32* %x38, align 4
  %add52 = add nsw i32 %56, 1
  %57 = load i32, i32* %y34, align 4
  %sub53 = sub nsw i32 %57, 1
  %58 = load i32, i32* %width.addr, align 4
  %59 = load i32, i32* %height.addr, align 4
  %call54 = call i32 @getPixel(i32 %add52, i32 %sub53, i32* %vla, i32 %58, i32 %59)
  %add55 = add nsw i32 %mul51, %call54
  %60 = load i32, i32* %x38, align 4
  %sub56 = sub nsw i32 %60, 1
  %61 = load i32, i32* %y34, align 4
  %62 = load i32, i32* %width.addr, align 4
  %63 = load i32, i32* %height.addr, align 4
  %call57 = call i32 @getPixel(i32 %sub56, i32 %61, i32* %vla, i32 %62, i32 %63)
  %shl58 = shl i32 %call57, 1
  %mul59 = mul nsw i32 -1, %shl58
  %add60 = add nsw i32 %add55, %mul59
  %64 = load i32, i32* %x38, align 4
  %add61 = add nsw i32 %64, 1
  %65 = load i32, i32* %y34, align 4
  %66 = load i32, i32* %width.addr, align 4
  %67 = load i32, i32* %height.addr, align 4
  %call62 = call i32 @getPixel(i32 %add61, i32 %65, i32* %vla, i32 %66, i32 %67)
  %shl63 = shl i32 %call62, 1
  %add64 = add nsw i32 %add60, %shl63
  %68 = load i32, i32* %x38, align 4
  %sub65 = sub nsw i32 %68, 1
  %69 = load i32, i32* %y34, align 4
  %add66 = add nsw i32 %69, 1
  %70 = load i32, i32* %width.addr, align 4
  %71 = load i32, i32* %height.addr, align 4
  %call67 = call i32 @getPixel(i32 %sub65, i32 %add66, i32* %vla, i32 %70, i32 %71)
  %mul68 = mul nsw i32 -1, %call67
  %add69 = add nsw i32 %add64, %mul68
  %72 = load i32, i32* %x38, align 4
  %add70 = add nsw i32 %72, 1
  %73 = load i32, i32* %y34, align 4
  %add71 = add nsw i32 %73, 1
  %74 = load i32, i32* %width.addr, align 4
  %75 = load i32, i32* %height.addr, align 4
  %call72 = call i32 @getPixel(i32 %add70, i32 %add71, i32* %vla, i32 %74, i32 %75)
  %add73 = add nsw i32 %add69, %call72
  store i32 %add73, i32* %newX, align 4
  %76 = load i32, i32* %x38, align 4
  %sub74 = sub nsw i32 %76, 1
  %77 = load i32, i32* %y34, align 4
  %sub75 = sub nsw i32 %77, 1
  %78 = load i32, i32* %width.addr, align 4
  %79 = load i32, i32* %height.addr, align 4
  %call76 = call i32 @getPixel(i32 %sub74, i32 %sub75, i32* %vla, i32 %78, i32 %79)
  %mul77 = mul nsw i32 -1, %call76
  %80 = load i32, i32* %x38, align 4
  %81 = load i32, i32* %y34, align 4
  %sub78 = sub nsw i32 %81, 1
  %82 = load i32, i32* %width.addr, align 4
  %83 = load i32, i32* %height.addr, align 4
  %call79 = call i32 @getPixel(i32 %80, i32 %sub78, i32* %vla, i32 %82, i32 %83)
  %shl80 = shl i32 %call79, 1
  %mul81 = mul nsw i32 -1, %shl80
  %add82 = add nsw i32 %mul77, %mul81
  %84 = load i32, i32* %x38, align 4
  %add83 = add nsw i32 %84, 1
  %85 = load i32, i32* %y34, align 4
  %sub84 = sub nsw i32 %85, 1
  %86 = load i32, i32* %width.addr, align 4
  %87 = load i32, i32* %height.addr, align 4
  %call85 = call i32 @getPixel(i32 %add83, i32 %sub84, i32* %vla, i32 %86, i32 %87)
  %mul86 = mul nsw i32 -1, %call85
  %add87 = add nsw i32 %add82, %mul86
  %88 = load i32, i32* %x38, align 4
  %sub88 = sub nsw i32 %88, 1
  %89 = load i32, i32* %y34, align 4
  %add89 = add nsw i32 %89, 1
  %90 = load i32, i32* %width.addr, align 4
  %91 = load i32, i32* %height.addr, align 4
  %call90 = call i32 @getPixel(i32 %sub88, i32 %add89, i32* %vla, i32 %90, i32 %91)
  %add91 = add nsw i32 %add87, %call90
  %92 = load i32, i32* %x38, align 4
  %93 = load i32, i32* %y34, align 4
  %add92 = add nsw i32 %93, 1
  %94 = load i32, i32* %width.addr, align 4
  %95 = load i32, i32* %height.addr, align 4
  %call93 = call i32 @getPixel(i32 %92, i32 %add92, i32* %vla, i32 %94, i32 %95)
  %shl94 = shl i32 %call93, 1
  %add95 = add nsw i32 %add91, %shl94
  %96 = load i32, i32* %x38, align 4
  %add96 = add nsw i32 %96, 1
  %97 = load i32, i32* %y34, align 4
  %add97 = add nsw i32 %97, 1
  %98 = load i32, i32* %width.addr, align 4
  %99 = load i32, i32* %height.addr, align 4
  %call98 = call i32 @getPixel(i32 %add96, i32 %add97, i32* %vla, i32 %98, i32 %99)
  %add99 = add nsw i32 %add95, %call98
  store i32 %add99, i32* %newY, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %100 = load i32, i32* %newX, align 4
  %101 = load i32, i32* %newX, align 4
  %mul100 = mul nsw i32 %100, %101
  %102 = load i32, i32* %newY, align 4
  %103 = load i32, i32* %newY, align 4
  %mul101 = mul nsw i32 %102, %103
  %add102 = add nsw i32 %mul100, %mul101
  %call103 = call double @_Z4sqrtIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %add102) #3
  %conv104 = fptosi double %call103 to i32
  store i32 %conv104, i32* %mag, align 4
  %104 = load i32, i32* %mag, align 4
  %cmp105 = icmp sgt i32 %104, 255
  br i1 %cmp105, label %if.then106, label %if.end107

if.then106:                                       ; preds = %if.end
  store i32 255, i32* %mag, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.then106, %if.end
  %105 = load i32, i32* %width.addr, align 4
  %106 = load i32, i32* %y34, align 4
  %mul108 = mul nsw i32 %105, %106
  %107 = load i32, i32* %x38, align 4
  %add109 = add nsw i32 %mul108, %107
  %shl110 = shl i32 %add109, 2
  store i32 %shl110, i32* %offset, align 4
  %108 = load i8, i8* %invert.addr, align 1
  %tobool = trunc i8 %108 to i1
  %conv111 = zext i1 %tobool to i32
  %cmp112 = icmp eq i32 %conv111, 1
  br i1 %cmp112, label %if.then113, label %if.end115

if.then113:                                       ; preds = %if.end107
  %109 = load i32, i32* %mag, align 4
  %sub114 = sub nsw i32 255, %109
  store i32 %sub114, i32* %mag, align 4
  br label %if.end115

if.end115:                                        ; preds = %if.then113, %if.end107
  %110 = load i32, i32* %mag, align 4
  %conv116 = trunc i32 %110 to i8
  %111 = load i8*, i8** %data.addr, align 4
  %112 = load i32, i32* %offset, align 4
  %arrayidx117 = getelementptr inbounds i8, i8* %111, i32 %112
  store i8 %conv116, i8* %arrayidx117, align 1
  %113 = load i32, i32* %mag, align 4
  %conv118 = trunc i32 %113 to i8
  %114 = load i8*, i8** %data.addr, align 4
  %115 = load i32, i32* %offset, align 4
  %add119 = add nsw i32 %115, 1
  %arrayidx120 = getelementptr inbounds i8, i8* %114, i32 %add119
  store i8 %conv118, i8* %arrayidx120, align 1
  %116 = load i32, i32* %mag, align 4
  %conv121 = trunc i32 %116 to i8
  %117 = load i8*, i8** %data.addr, align 4
  %118 = load i32, i32* %offset, align 4
  %add122 = add nsw i32 %118, 2
  %arrayidx123 = getelementptr inbounds i8, i8* %117, i32 %add122
  store i8 %conv121, i8* %arrayidx123, align 1
  %119 = load i8*, i8** %data.addr, align 4
  %120 = load i32, i32* %offset, align 4
  %add124 = add nsw i32 %120, 3
  %arrayidx125 = getelementptr inbounds i8, i8* %119, i32 %add124
  store i8 -1, i8* %arrayidx125, align 1
  br label %for.inc126

for.inc126:                                       ; preds = %if.end115
  %121 = load i32, i32* %x38, align 4
  %inc127 = add nsw i32 %121, 1
  store i32 %inc127, i32* %x38, align 4
  br label %for.cond39

for.end128:                                       ; preds = %for.cond39
  br label %for.inc129

for.inc129:                                       ; preds = %for.end128
  %122 = load i32, i32* %y34, align 4
  %inc130 = add nsw i32 %122, 1
  store i32 %inc130, i32* %y34, align 4
  br label %for.cond35

for.end131:                                       ; preds = %for.cond35
  %123 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %123)
  ret void
}

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_Z4sqrtIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %__lcpp_x) #0 comdat {
entry:
  %__lcpp_x.addr = alloca i32, align 4
  store i32 %__lcpp_x, i32* %__lcpp_x.addr, align 4
  %0 = load i32, i32* %__lcpp_x.addr, align 4
  %conv = sitofp i32 %0 to double
  %1 = call double @llvm.sqrt.f64(double %conv)
  ret double %1
}

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @convFilter(float* %data, i32 %width, i32 %height, float* %kern, i32 %kWidth, i32 %kHeight, float %divisor, float %bias, i32 %count) #0 {
entry:
  %data.addr = alloca float*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %kern.addr = alloca float*, align 4
  %kWidth.addr = alloca i32, align 4
  %kHeight.addr = alloca i32, align 4
  %divisor.addr = alloca float, align 4
  %bias.addr = alloca float, align 4
  %count.addr = alloca i32, align 4
  %r = alloca float, align 4
  %g = alloca float, align 4
  %b = alloca float, align 4
  %yy = alloca i32, align 4
  %xx = alloca i32, align 4
  %imageOffset = alloca i32, align 4
  %kernelOffset = alloca i32, align 4
  %pix = alloca i32, align 4
  %kCenterY = alloca i32, align 4
  %kCenterX = alloca i32, align 4
  %i = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %ky = alloca i32, align 4
  %kx = alloca i32, align 4
  store float* %data, float** %data.addr, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  store float* %kern, float** %kern.addr, align 4
  store i32 %kWidth, i32* %kWidth.addr, align 4
  store i32 %kHeight, i32* %kHeight.addr, align 4
  store float %divisor, float* %divisor.addr, align 4
  store float %bias, float* %bias.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  %0 = load i32, i32* %kHeight.addr, align 4
  %div = sdiv i32 %0, 2
  %call = call double @_Z5floorIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %div) #3
  %conv = fptosi double %call to i32
  store i32 %conv, i32* %kCenterY, align 4
  %1 = load i32, i32* %kWidth.addr, align 4
  %div1 = sdiv i32 %1, 2
  %call2 = call double @_Z5floorIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %div1) #3
  %conv3 = fptosi double %call2 to i32
  store i32 %conv3, i32* %kCenterX, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc103, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %count.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end105

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %kCenterY, align 4
  store i32 %4, i32* %y, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc100, %for.body
  %5 = load i32, i32* %y, align 4
  %6 = load i32, i32* %height.addr, align 4
  %7 = load i32, i32* %kCenterY, align 4
  %sub = sub nsw i32 %6, %7
  %cmp5 = icmp slt i32 %5, %sub
  br i1 %cmp5, label %for.body6, label %for.end102

for.body6:                                        ; preds = %for.cond4
  %8 = load i32, i32* %kCenterX, align 4
  store i32 %8, i32* %x, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc97, %for.body6
  %9 = load i32, i32* %x, align 4
  %10 = load i32, i32* %width.addr, align 4
  %11 = load i32, i32* %kCenterX, align 4
  %sub8 = sub nsw i32 %10, %11
  %cmp9 = icmp slt i32 %9, %sub8
  br i1 %cmp9, label %for.body10, label %for.end99

for.body10:                                       ; preds = %for.cond7
  store float 0.000000e+00, float* %r, align 4
  store float 0.000000e+00, float* %g, align 4
  store float 0.000000e+00, float* %b, align 4
  store i32 0, i32* %ky, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc38, %for.body10
  %12 = load i32, i32* %ky, align 4
  %13 = load i32, i32* %kHeight.addr, align 4
  %cmp12 = icmp slt i32 %12, %13
  br i1 %cmp12, label %for.body13, label %for.end40

for.body13:                                       ; preds = %for.cond11
  store i32 0, i32* %kx, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body13
  %14 = load i32, i32* %kx, align 4
  %15 = load i32, i32* %kWidth.addr, align 4
  %cmp15 = icmp slt i32 %14, %15
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %16 = load i32, i32* %width.addr, align 4
  %17 = load i32, i32* %y, align 4
  %18 = load i32, i32* %kCenterY, align 4
  %sub17 = sub nsw i32 %17, %18
  %19 = load i32, i32* %ky, align 4
  %add = add nsw i32 %sub17, %19
  %mul = mul nsw i32 %16, %add
  %20 = load i32, i32* %x, align 4
  %21 = load i32, i32* %kCenterX, align 4
  %sub18 = sub nsw i32 %20, %21
  %22 = load i32, i32* %kx, align 4
  %add19 = add nsw i32 %sub18, %22
  %add20 = add nsw i32 %mul, %add19
  %mul21 = mul nsw i32 %add20, 4
  store i32 %mul21, i32* %imageOffset, align 4
  %23 = load i32, i32* %kWidth.addr, align 4
  %24 = load i32, i32* %ky, align 4
  %mul22 = mul nsw i32 %23, %24
  %25 = load i32, i32* %kx, align 4
  %add23 = add nsw i32 %mul22, %25
  store i32 %add23, i32* %kernelOffset, align 4
  %26 = load float*, float** %data.addr, align 4
  %27 = load i32, i32* %imageOffset, align 4
  %add24 = add nsw i32 %27, 0
  %arrayidx = getelementptr inbounds float, float* %26, i32 %add24
  %28 = load float, float* %arrayidx, align 4
  %29 = load float*, float** %kern.addr, align 4
  %30 = load i32, i32* %kernelOffset, align 4
  %arrayidx25 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %28, %31
  %32 = load float, float* %r, align 4
  %add27 = fadd float %32, %mul26
  store float %add27, float* %r, align 4
  %33 = load float*, float** %data.addr, align 4
  %34 = load i32, i32* %imageOffset, align 4
  %add28 = add nsw i32 %34, 1
  %arrayidx29 = getelementptr inbounds float, float* %33, i32 %add28
  %35 = load float, float* %arrayidx29, align 4
  %36 = load float*, float** %kern.addr, align 4
  %37 = load i32, i32* %kernelOffset, align 4
  %arrayidx30 = getelementptr inbounds float, float* %36, i32 %37
  %38 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %35, %38
  %39 = load float, float* %g, align 4
  %add32 = fadd float %39, %mul31
  store float %add32, float* %g, align 4
  %40 = load float*, float** %data.addr, align 4
  %41 = load i32, i32* %imageOffset, align 4
  %add33 = add nsw i32 %41, 2
  %arrayidx34 = getelementptr inbounds float, float* %40, i32 %add33
  %42 = load float, float* %arrayidx34, align 4
  %43 = load float*, float** %kern.addr, align 4
  %44 = load i32, i32* %kernelOffset, align 4
  %arrayidx35 = getelementptr inbounds float, float* %43, i32 %44
  %45 = load float, float* %arrayidx35, align 4
  %mul36 = fmul float %42, %45
  %46 = load float, float* %b, align 4
  %add37 = fadd float %46, %mul36
  store float %add37, float* %b, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body16
  %47 = load i32, i32* %kx, align 4
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %kx, align 4
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc38

for.inc38:                                        ; preds = %for.end
  %48 = load i32, i32* %ky, align 4
  %inc39 = add nsw i32 %48, 1
  store i32 %inc39, i32* %ky, align 4
  br label %for.cond11

for.end40:                                        ; preds = %for.cond11
  %49 = load i32, i32* %width.addr, align 4
  %50 = load i32, i32* %y, align 4
  %mul41 = mul nsw i32 %49, %50
  %51 = load i32, i32* %x, align 4
  %add42 = add nsw i32 %mul41, %51
  %mul43 = mul nsw i32 %add42, 4
  store i32 %mul43, i32* %pix, align 4
  %52 = load float, float* %r, align 4
  %53 = load float, float* %divisor.addr, align 4
  %div44 = fdiv float %52, %53
  %conv45 = fpext float %div44 to double
  %cmp46 = fcmp ogt double %conv45, 2.550000e+02
  br i1 %cmp46, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end40
  br label %cond.end54

cond.false:                                       ; preds = %for.end40
  %54 = load float, float* %r, align 4
  %55 = load float, float* %divisor.addr, align 4
  %div47 = fdiv float %54, %55
  %conv48 = fpext float %div47 to double
  %cmp49 = fcmp olt double %conv48, 0.000000e+00
  br i1 %cmp49, label %cond.true50, label %cond.false51

cond.true50:                                      ; preds = %cond.false
  br label %cond.end

cond.false51:                                     ; preds = %cond.false
  %56 = load float, float* %r, align 4
  %57 = load float, float* %divisor.addr, align 4
  %div52 = fdiv float %56, %57
  %conv53 = fpext float %div52 to double
  br label %cond.end

cond.end:                                         ; preds = %cond.false51, %cond.true50
  %cond = phi double [ 0.000000e+00, %cond.true50 ], [ %conv53, %cond.false51 ]
  br label %cond.end54

cond.end54:                                       ; preds = %cond.end, %cond.true
  %cond55 = phi double [ 2.550000e+02, %cond.true ], [ %cond, %cond.end ]
  %conv56 = fptrunc double %cond55 to float
  %58 = load float*, float** %data.addr, align 4
  %59 = load i32, i32* %pix, align 4
  %add57 = add nsw i32 %59, 0
  %arrayidx58 = getelementptr inbounds float, float* %58, i32 %add57
  store float %conv56, float* %arrayidx58, align 4
  %60 = load float, float* %g, align 4
  %61 = load float, float* %divisor.addr, align 4
  %div59 = fdiv float %60, %61
  %conv60 = fpext float %div59 to double
  %cmp61 = fcmp ogt double %conv60, 2.550000e+02
  br i1 %cmp61, label %cond.true62, label %cond.false63

cond.true62:                                      ; preds = %cond.end54
  br label %cond.end73

cond.false63:                                     ; preds = %cond.end54
  %62 = load float, float* %g, align 4
  %63 = load float, float* %divisor.addr, align 4
  %div64 = fdiv float %62, %63
  %conv65 = fpext float %div64 to double
  %cmp66 = fcmp olt double %conv65, 0.000000e+00
  br i1 %cmp66, label %cond.true67, label %cond.false68

cond.true67:                                      ; preds = %cond.false63
  br label %cond.end71

cond.false68:                                     ; preds = %cond.false63
  %64 = load float, float* %g, align 4
  %65 = load float, float* %divisor.addr, align 4
  %div69 = fdiv float %64, %65
  %conv70 = fpext float %div69 to double
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false68, %cond.true67
  %cond72 = phi double [ 0.000000e+00, %cond.true67 ], [ %conv70, %cond.false68 ]
  br label %cond.end73

cond.end73:                                       ; preds = %cond.end71, %cond.true62
  %cond74 = phi double [ 2.550000e+02, %cond.true62 ], [ %cond72, %cond.end71 ]
  %conv75 = fptrunc double %cond74 to float
  %66 = load float*, float** %data.addr, align 4
  %67 = load i32, i32* %pix, align 4
  %add76 = add nsw i32 %67, 1
  %arrayidx77 = getelementptr inbounds float, float* %66, i32 %add76
  store float %conv75, float* %arrayidx77, align 4
  %68 = load float, float* %b, align 4
  %69 = load float, float* %divisor.addr, align 4
  %div78 = fdiv float %68, %69
  %conv79 = fpext float %div78 to double
  %cmp80 = fcmp ogt double %conv79, 2.550000e+02
  br i1 %cmp80, label %cond.true81, label %cond.false82

cond.true81:                                      ; preds = %cond.end73
  br label %cond.end92

cond.false82:                                     ; preds = %cond.end73
  %70 = load float, float* %b, align 4
  %71 = load float, float* %divisor.addr, align 4
  %div83 = fdiv float %70, %71
  %conv84 = fpext float %div83 to double
  %cmp85 = fcmp olt double %conv84, 0.000000e+00
  br i1 %cmp85, label %cond.true86, label %cond.false87

cond.true86:                                      ; preds = %cond.false82
  br label %cond.end90

cond.false87:                                     ; preds = %cond.false82
  %72 = load float, float* %b, align 4
  %73 = load float, float* %divisor.addr, align 4
  %div88 = fdiv float %72, %73
  %conv89 = fpext float %div88 to double
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false87, %cond.true86
  %cond91 = phi double [ 0.000000e+00, %cond.true86 ], [ %conv89, %cond.false87 ]
  br label %cond.end92

cond.end92:                                       ; preds = %cond.end90, %cond.true81
  %cond93 = phi double [ 2.550000e+02, %cond.true81 ], [ %cond91, %cond.end90 ]
  %conv94 = fptrunc double %cond93 to float
  %74 = load float*, float** %data.addr, align 4
  %75 = load i32, i32* %pix, align 4
  %add95 = add nsw i32 %75, 2
  %arrayidx96 = getelementptr inbounds float, float* %74, i32 %add95
  store float %conv94, float* %arrayidx96, align 4
  br label %for.inc97

for.inc97:                                        ; preds = %cond.end92
  %76 = load i32, i32* %x, align 4
  %inc98 = add nsw i32 %76, 1
  store i32 %inc98, i32* %x, align 4
  br label %for.cond7

for.end99:                                        ; preds = %for.cond7
  br label %for.inc100

for.inc100:                                       ; preds = %for.end99
  %77 = load i32, i32* %y, align 4
  %inc101 = add nsw i32 %77, 1
  store i32 %inc101, i32* %y, align 4
  br label %for.cond4

for.end102:                                       ; preds = %for.cond4
  br label %for.inc103

for.inc103:                                       ; preds = %for.end102
  %78 = load i32, i32* %i, align 4
  %inc104 = add nsw i32 %78, 1
  store i32 %inc104, i32* %i, align 4
  br label %for.cond

for.end105:                                       ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_Z5floorIiENSt3__29enable_ifIXsr3std11is_integralIT_EE5valueEdE4typeES2_(i32 %__lcpp_x) #0 comdat {
entry:
  %__lcpp_x.addr = alloca i32, align 4
  store i32 %__lcpp_x, i32* %__lcpp_x.addr, align 4
  %0 = load i32, i32* %__lcpp_x.addr, align 4
  %conv = sitofp i32 %0 to double
  %1 = call double @llvm.floor.f64(double %conv)
  ret double %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #4

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
