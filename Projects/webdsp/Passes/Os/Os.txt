[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.000811108 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 5.721e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.00396189 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00153295 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00223441 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000202605 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.000981295 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000305973 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.000863933 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.000517675 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00368974 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00170478 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000498716 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00118167 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00142118 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00294292 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00135681 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000257782 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00111412 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000255338 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00127389 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.000514333 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00116746 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0014507 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00015317 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00103577 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.000644883 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.000766347 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.000749095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00116998 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.000790944 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0202325 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000299874 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   5.437e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000420307 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000143047 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.143e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000135161 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00397945 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0607742 seconds.
[PassRunner] (final validation)
