(module
  (type (;0;) (func (param i32) (result i32)))
  (type (;1;) (func (param i32)))
  (type (;2;) (func (param i32 i32)))
  (type (;3;) (func))
  (type (;4;) (func (result i32)))
  (type (;5;) (func (param i32 i32 i32)))
  (type (;6;) (func (param i32 i32 i32 i32)))
  (type (;7;) (func (param i32 i32 i32 i32 i32 i32 i32)))
  (type (;8;) (func (param i32 i32 i32 i32 i32 i32 f32 f32 i32)))
  (type (;9;) (func (param i32 i32 i32) (result i32)))
  (type (;10;) (func (param i32 i32 i32 i32 i32) (result i32)))
  (type (;11;) (func (param i32 i64 i32) (result i64)))
  (type (;12;) (func (param i32) (result f64)))
  (import "env" "emscripten_resize_heap" (func (;0;) (type 0)))
  (import "env" "__handle_stack_overflow" (func (;1;) (type 3)))
  (import "env" "memory" (memory (;0;) 256 32768))
  (import "env" "table" (table (;0;) 1 funcref))
  (func (;2;) (type 3)
    nop)
  (func (;3;) (type 10) (param i32 i32 i32 i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 5
    local.get 0
    i32.store offset=24
    local.get 5
    local.get 1
    i32.store offset=20
    local.get 5
    local.get 2
    i32.store offset=16
    local.get 5
    local.get 3
    i32.store offset=12
    local.get 5
    local.get 4
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        local.get 5
        i32.load offset=24
        i32.const 0
        i32.ge_s
        if  ;; label = @3
          local.get 5
          i32.load offset=20
          i32.const 0
          i32.ge_s
          br_if 1 (;@2;)
        end
        local.get 5
        i32.const 0
        i32.store offset=28
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 5
        i32.load offset=24
        local.get 5
        i32.load offset=12
        i32.lt_s
        if  ;; label = @3
          local.get 5
          i32.load offset=20
          local.get 5
          i32.load offset=8
          i32.lt_s
          br_if 1 (;@2;)
        end
        local.get 5
        i32.const 0
        i32.store offset=28
        br 1 (;@1;)
      end
      local.get 5
      local.get 5
      i32.load offset=16
      local.get 5
      i32.load offset=24
      local.get 5
      i32.load offset=12
      local.get 5
      i32.load offset=20
      i32.mul
      i32.add
      i32.const 2
      i32.shl
      i32.add
      i32.load
      i32.store offset=28
    end
    local.get 5
    i32.load offset=28)
  (func (;4;) (type 2) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    local.get 0
    i32.store offset=28
    local.get 2
    local.get 1
    i32.store offset=24
    local.get 2
    i32.const 0
    i32.store offset=20
    loop  ;; label = @1
      local.get 2
      i32.load offset=20
      local.get 2
      i32.load offset=24
      i32.ge_s
      i32.eqz
      if  ;; label = @2
        local.get 2
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.add
        i32.load8_u
        i32.store offset=16
        local.get 2
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 1
        i32.add
        i32.add
        i32.load8_u
        i32.store offset=12
        local.get 2
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 2
        i32.add
        i32.add
        i32.load8_u
        i32.store offset=8
        local.get 2
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 3
        i32.add
        i32.add
        i32.load8_u
        i32.store offset=4
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.add
        local.get 2
        i32.load offset=16
        i32.store8
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 1
        i32.add
        i32.add
        local.get 2
        i32.load offset=16
        i32.store8
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 2
        i32.add
        i32.add
        local.get 2
        i32.load offset=16
        i32.store8
        local.get 2
        i32.load offset=28
        local.get 2
        i32.load offset=20
        i32.const 3
        i32.add
        i32.add
        local.get 2
        i32.load offset=4
        i32.store8
        local.get 2
        local.get 2
        i32.load offset=20
        i32.const 4
        i32.add
        i32.store offset=20
        br 1 (;@1;)
      end
    end)
  (func (;5;) (type 5) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    local.get 0
    i32.store offset=12
    local.get 3
    local.get 1
    i32.store offset=8
    local.get 3
    local.get 2
    i32.store offset=4
    local.get 3
    i32.const 0
    i32.store
    loop  ;; label = @1
      local.get 3
      i32.load
      local.get 3
      i32.load offset=8
      i32.lt_s
      if  ;; label = @2
        local.get 3
        i32.load offset=12
        local.get 3
        i32.load
        i32.add
        i32.load8_u
        local.get 3
        i32.load offset=4
        i32.add
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 3
          i32.load offset=4
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.add
          local.tee 0
          i32.load8_u
          i32.add
          local.set 1
          local.get 0
          local.get 1
          i32.store8
        end
        local.get 3
        i32.load offset=12
        local.get 3
        i32.load
        i32.const 1
        i32.add
        i32.add
        i32.load8_u
        local.get 3
        i32.load offset=4
        i32.add
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 3
          i32.load offset=4
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.const 1
          i32.add
          i32.add
          local.tee 0
          i32.load8_u
          i32.add
          local.set 1
          local.get 0
          local.get 1
          i32.store8
        end
        local.get 3
        i32.load offset=12
        local.get 3
        i32.load
        i32.const 2
        i32.add
        i32.add
        i32.load8_u
        local.get 3
        i32.load offset=4
        i32.add
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 3
          i32.load offset=4
          local.get 3
          i32.load offset=12
          local.get 3
          i32.load
          i32.const 2
          i32.add
          i32.add
          local.tee 0
          i32.load8_u
          i32.add
          local.set 1
          local.get 0
          local.get 1
          i32.store8
        end
        local.get 3
        local.get 3
        i32.load
        i32.const 4
        i32.add
        i32.store
        br 1 (;@1;)
      end
    end)
  (func (;6;) (type 2) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 1
    i32.store offset=8
    local.get 2
    i32.const 0
    i32.store offset=4
    loop  ;; label = @1
      local.get 2
      i32.load offset=4
      local.get 2
      i32.load offset=8
      i32.ge_s
      i32.eqz
      if  ;; label = @2
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.add
        i32.const 255
        local.tee 0
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.add
        i32.load8_u
        i32.sub
        i32.store8
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.const 1
        i32.add
        i32.add
        local.get 0
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.const 1
        i32.add
        i32.add
        i32.load8_u
        i32.sub
        i32.store8
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.const 2
        i32.add
        i32.add
        local.get 0
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load offset=4
        i32.const 2
        i32.add
        i32.add
        i32.load8_u
        i32.sub
        i32.store8
        local.get 2
        local.get 2
        i32.load offset=4
        i32.const 4
        i32.add
        i32.store offset=4
        br 1 (;@1;)
      end
    end)
  (func (;7;) (type 2) (param i32 i32)
    (local i32 i32 i64)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    local.tee 3
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 3
    global.set 0
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 2
    local.get 1
    i32.store offset=8
    local.get 2
    i32.const 0
    i32.store
    loop  ;; label = @1
      local.get 2
      i32.load
      local.get 2
      i32.load offset=8
      i32.lt_s
      if  ;; label = @2
        i32.const 1024
        i32.const 1024
        i64.load
        i64.const 6364136223846793005
        i64.mul
        i64.const 1
        i64.add
        local.tee 4
        i64.store
        local.get 2
        local.get 4
        i64.const 33
        i64.shr_u
        i32.wrap_i64
        i32.const 70
        i32.rem_s
        i32.const 35
        i32.sub
        i32.store offset=4
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 2
        i32.shl
        i32.add
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 2
        i32.shl
        i32.add
        f32.load
        local.get 2
        i32.load offset=4
        f32.convert_i32_s
        f32.add
        f32.store
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 1
        i32.add
        i32.const 2
        i32.shl
        i32.add
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 1
        i32.add
        i32.const 2
        i32.shl
        i32.add
        f32.load
        local.get 2
        i32.load offset=4
        f32.convert_i32_s
        f32.add
        f32.store
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 2
        i32.add
        i32.const 2
        i32.shl
        i32.add
        local.get 2
        i32.load offset=12
        local.get 2
        i32.load
        i32.const 2
        i32.add
        i32.const 2
        i32.shl
        i32.add
        f32.load
        local.get 2
        i32.load offset=4
        f32.convert_i32_s
        f32.add
        f32.store
        local.get 2
        local.get 2
        i32.load
        i32.const 4
        i32.add
        i32.store
        br 1 (;@1;)
      end
    end
    local.get 2
    i32.const 16
    i32.add
    local.tee 0
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 0
    global.set 0)
  (func (;8;) (type 7) (param i32 i32 i32 i32 i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 7
    local.get 0
    i32.store offset=28
    local.get 7
    local.get 1
    i32.store offset=24
    local.get 7
    local.get 2
    i32.store offset=20
    local.get 7
    local.get 3
    i32.store offset=16
    local.get 7
    local.get 4
    i32.store offset=12
    local.get 7
    local.get 5
    i32.store offset=8
    local.get 7
    local.get 6
    i32.store offset=4
    local.get 7
    i32.const 0
    i32.store
    loop  ;; label = @1
      local.get 7
      i32.load
      local.get 7
      i32.load offset=24
      i32.lt_s
      if  ;; label = @2
        local.get 7
        i32.load
        i32.const 4
        i32.rem_s
        i32.const 3
        i32.ne
        if  ;; label = @3
          local.get 7
          i32.load offset=28
          local.get 7
          i32.load
          i32.add
          local.get 7
          i32.load offset=12
          local.get 7
          i32.load offset=8
          local.get 7
          i32.load offset=28
          local.get 7
          i32.load
          i32.add
          i32.load8_u
          i32.mul
          i32.add
          local.get 7
          i32.load offset=28
          local.get 7
          i32.load
          local.get 7
          i32.load offset=4
          i32.add
          i32.add
          i32.load8_u
          i32.sub
          local.get 7
          i32.load offset=28
          local.get 7
          i32.load
          local.get 7
          i32.load offset=20
          i32.const 2
          i32.shl
          i32.add
          i32.add
          i32.load8_u
          i32.sub
          i32.store8
        end
        local.get 7
        local.get 7
        i32.load offset=16
        local.get 7
        i32.load
        i32.add
        i32.store
        br 1 (;@1;)
      end
    end)
  (func (;9;) (type 6) (param i32 i32 i32 i32)
    (local i32 i32 i32 f64)
    global.get 0
    i32.const 80
    i32.sub
    local.tee 5
    local.set 4
    local.get 5
    local.tee 6
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 6
    global.set 0
    local.get 4
    local.get 0
    i32.store offset=76
    local.get 4
    local.get 1
    i32.store offset=72
    local.get 4
    local.get 2
    i32.store offset=68
    local.get 4
    local.get 3
    i32.const 1
    i32.and
    i32.store8 offset=67
    local.get 4
    i32.load offset=72
    local.get 4
    i32.load offset=68
    i32.mul
    local.set 1
    local.get 4
    local.get 5
    i32.store offset=60
    local.get 5
    local.get 1
    i32.const 2
    i32.shl
    i32.const 15
    i32.add
    i32.const -16
    i32.and
    i32.sub
    local.tee 0
    local.tee 2
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 2
    global.set 0
    local.get 4
    local.get 1
    i32.store offset=56
    local.get 4
    i32.const 0
    i32.store offset=52
    loop  ;; label = @1
      local.get 4
      i32.load offset=52
      local.get 4
      i32.load offset=68
      i32.lt_s
      if  ;; label = @2
        local.get 4
        i32.const 0
        i32.store offset=48
        loop  ;; label = @3
          local.get 4
          i32.load offset=48
          local.get 4
          i32.load offset=72
          i32.lt_s
          if  ;; label = @4
            local.get 4
            local.get 4
            i32.load offset=48
            local.get 4
            i32.load offset=72
            local.get 4
            i32.load offset=52
            i32.mul
            i32.add
            i32.const 2
            i32.shl
            i32.store offset=44
            local.get 4
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=44
            i32.add
            i32.load8_u
            i32.store offset=40
            local.get 4
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=44
            i32.const 1
            i32.add
            i32.add
            i32.load8_u
            i32.store offset=36
            local.get 4
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=44
            i32.const 2
            i32.add
            i32.add
            i32.load8_u
            i32.store offset=32
            local.get 4
            local.get 4
            i32.load offset=40
            i32.const 2
            i32.shr_s
            local.get 4
            i32.load offset=36
            i32.const 1
            i32.shr_s
            i32.add
            local.get 4
            i32.load offset=32
            i32.const 3
            i32.shr_s
            i32.add
            i32.store offset=28
            local.get 4
            i32.load offset=48
            local.get 4
            i32.load offset=72
            local.get 4
            i32.load offset=52
            i32.mul
            i32.add
            i32.const 2
            i32.shl
            local.get 0
            i32.add
            local.get 4
            i32.load offset=28
            i32.store
            local.get 4
            local.get 4
            i32.load offset=48
            local.get 4
            i32.load offset=72
            local.get 4
            i32.load offset=52
            i32.mul
            i32.add
            i32.const 2
            i32.shl
            i32.store offset=24
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=24
            i32.add
            local.get 4
            i32.load offset=28
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=24
            i32.const 1
            i32.add
            i32.add
            local.get 4
            i32.load offset=28
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=24
            i32.const 2
            i32.add
            i32.add
            local.get 4
            i32.load offset=28
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load offset=24
            i32.const 3
            i32.add
            i32.add
            i32.const 255
            i32.store8
            local.get 4
            local.get 4
            i32.load offset=48
            i32.const 1
            i32.add
            i32.store offset=48
            br 1 (;@3;)
          end
        end
        local.get 4
        local.get 4
        i32.load offset=52
        i32.const 1
        i32.add
        i32.store offset=52
        br 1 (;@1;)
      end
    end
    local.get 4
    i32.const 0
    i32.store offset=20
    loop  ;; label = @1
      local.get 4
      i32.load offset=20
      local.get 4
      i32.load offset=68
      i32.lt_s
      if  ;; label = @2
        local.get 4
        i32.const 0
        i32.store offset=16
        loop  ;; label = @3
          local.get 4
          i32.load offset=16
          local.get 4
          i32.load offset=72
          i32.lt_s
          if  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 4
                  i32.load offset=16
                  i32.const 0
                  i32.le_s
                  br_if 0 (;@7;)
                  local.get 4
                  i32.load offset=16
                  local.get 4
                  i32.load offset=72
                  i32.const 1
                  i32.sub
                  i32.ge_s
                  br_if 0 (;@7;)
                  local.get 4
                  i32.load offset=20
                  i32.const 0
                  i32.le_s
                  br_if 0 (;@7;)
                  local.get 4
                  i32.load offset=20
                  local.get 4
                  i32.load offset=68
                  i32.const 1
                  i32.sub
                  i32.lt_s
                  br_if 1 (;@6;)
                end
                local.get 4
                i32.const 0
                i32.store offset=12
                local.get 4
                i32.const 0
                i32.store offset=8
                br 1 (;@5;)
              end
              local.get 4
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.sub
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.sub
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const -1
              i32.mul
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.add
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.sub
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.sub
              local.get 4
              i32.load offset=20
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const 1
              i32.shl
              i32.const -1
              i32.mul
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.add
              local.get 4
              i32.load offset=20
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const 1
              i32.shl
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.sub
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.add
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const -1
              i32.mul
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.add
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.add
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.add
              i32.store offset=12
              local.get 4
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.sub
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.sub
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const -1
              i32.mul
              local.get 4
              i32.load offset=16
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.sub
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const 1
              i32.shl
              i32.const -1
              i32.mul
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.add
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.sub
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const -1
              i32.mul
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.sub
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.add
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.add
              local.get 4
              i32.load offset=16
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.add
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.const 1
              i32.shl
              i32.add
              local.get 4
              i32.load offset=16
              i32.const 1
              i32.add
              local.get 4
              i32.load offset=20
              i32.const 1
              i32.add
              local.get 0
              local.get 4
              i32.load offset=72
              local.get 4
              i32.load offset=68
              call 3
              i32.add
              i32.store offset=8
            end
            local.get 4
            block (result i32)  ;; label = @5
              global.get 0
              i32.const 16
              i32.sub
              local.tee 1
              local.get 4
              i32.load offset=12
              local.get 4
              i32.load offset=12
              i32.mul
              local.get 4
              i32.load offset=8
              local.get 4
              i32.load offset=8
              i32.mul
              i32.add
              i32.store offset=12
              local.get 1
              i32.load offset=12
              f64.convert_i32_s
              f64.sqrt
              local.tee 7
              f64.abs
              f64.const 0x1p+31 (;=2.14748e+09;)
              f64.lt
              if  ;; label = @6
                local.get 7
                i32.trunc_f64_s
                br 1 (;@5;)
              end
              i32.const -2147483648
            end
            i32.store offset=4
            local.get 4
            i32.load offset=4
            i32.const 255
            i32.gt_s
            if  ;; label = @5
              local.get 4
              i32.const 255
              i32.store offset=4
            end
            local.get 4
            local.get 4
            i32.load offset=16
            local.get 4
            i32.load offset=72
            local.get 4
            i32.load offset=20
            i32.mul
            i32.add
            i32.const 2
            i32.shl
            i32.store
            local.get 4
            i32.load8_u offset=67
            i32.const 1
            i32.and
            i32.const 1
            i32.eq
            if  ;; label = @5
              local.get 4
              i32.const 255
              local.get 4
              i32.load offset=4
              i32.sub
              i32.store offset=4
            end
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load
            i32.add
            local.get 4
            i32.load offset=4
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load
            i32.const 1
            i32.add
            i32.add
            local.get 4
            i32.load offset=4
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load
            i32.const 2
            i32.add
            i32.add
            local.get 4
            i32.load offset=4
            i32.store8
            local.get 4
            i32.load offset=76
            local.get 4
            i32.load
            i32.const 3
            i32.add
            i32.add
            i32.const 255
            i32.store8
            local.get 4
            local.get 4
            i32.load offset=16
            i32.const 1
            i32.add
            i32.store offset=16
            br 1 (;@3;)
          end
        end
        local.get 4
        local.get 4
        i32.load offset=20
        i32.const 1
        i32.add
        i32.store offset=20
        br 1 (;@1;)
      end
    end
    local.get 4
    i32.load offset=60
    drop
    local.get 4
    i32.const 80
    i32.add
    local.tee 0
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 0
    global.set 0)
  (func (;10;) (type 8) (param i32 i32 i32 i32 i32 i32 f32 f32 i32)
    (local i32 i32 f64)
    global.get 0
    i32.const 96
    i32.sub
    local.tee 9
    local.tee 10
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 10
    global.set 0
    i32.const 0
    local.set 10
    local.get 9
    local.get 0
    i32.store offset=92
    local.get 9
    local.get 1
    i32.store offset=88
    local.get 9
    local.get 2
    i32.store offset=84
    local.get 9
    local.get 3
    i32.store offset=80
    local.get 9
    local.get 4
    i32.store offset=76
    local.get 9
    local.get 5
    i32.store offset=72
    local.get 9
    local.get 6
    f32.store offset=68
    local.get 9
    local.get 7
    f32.store offset=64
    local.get 9
    local.get 8
    i32.store offset=60
    local.get 9
    block (result i32)  ;; label = @1
      local.get 9
      i32.load offset=72
      i32.const 2
      i32.div_s
      call 11
      local.tee 11
      f64.abs
      f64.const 0x1p+31 (;=2.14748e+09;)
      f64.lt
      if  ;; label = @2
        local.get 11
        i32.trunc_f64_s
        br 1 (;@1;)
      end
      i32.const -2147483648
    end
    i32.store offset=24
    local.get 9
    block (result i32)  ;; label = @1
      local.get 9
      i32.load offset=76
      i32.const 2
      i32.div_s
      call 11
      local.tee 11
      f64.abs
      f64.const 0x1p+31 (;=2.14748e+09;)
      f64.lt
      if  ;; label = @2
        local.get 11
        i32.trunc_f64_s
        br 1 (;@1;)
      end
      i32.const -2147483648
    end
    i32.store offset=20
    local.get 9
    local.get 10
    i32.store offset=16
    loop  ;; label = @1
      local.get 9
      i32.load offset=16
      local.get 9
      i32.load offset=60
      i32.ge_s
      i32.eqz
      if  ;; label = @2
        local.get 9
        local.get 9
        i32.load offset=24
        i32.store offset=12
        loop  ;; label = @3
          local.get 9
          i32.load offset=12
          local.get 9
          i32.load offset=84
          local.get 9
          i32.load offset=24
          i32.sub
          i32.ge_s
          i32.eqz
          if  ;; label = @4
            local.get 9
            local.get 9
            i32.load offset=20
            i32.store offset=8
            loop  ;; label = @5
              local.get 9
              i32.load offset=8
              local.get 9
              i32.load offset=88
              local.get 9
              i32.load offset=20
              i32.sub
              i32.ge_s
              i32.eqz
              if  ;; label = @6
                local.get 9
                i32.const 0
                local.tee 0
                f32.convert_i32_s
                local.tee 6
                f32.store offset=56
                local.get 9
                local.get 6
                f32.store offset=52
                local.get 9
                local.get 6
                f32.store offset=48
                local.get 9
                local.get 0
                i32.store offset=4
                loop  ;; label = @7
                  local.get 9
                  i32.load offset=4
                  local.get 9
                  i32.load offset=72
                  i32.ge_s
                  i32.eqz
                  if  ;; label = @8
                    local.get 9
                    i32.const 0
                    i32.store
                    loop  ;; label = @9
                      local.get 9
                      i32.load
                      local.get 9
                      i32.load offset=76
                      i32.ge_s
                      i32.eqz
                      if  ;; label = @10
                        local.get 9
                        local.get 9
                        i32.load
                        local.get 9
                        i32.load offset=8
                        local.get 9
                        i32.load offset=20
                        i32.sub
                        i32.add
                        local.get 9
                        i32.load offset=88
                        local.get 9
                        i32.load offset=4
                        local.get 9
                        i32.load offset=12
                        local.get 9
                        i32.load offset=24
                        i32.sub
                        i32.add
                        i32.mul
                        i32.add
                        i32.const 2
                        i32.shl
                        i32.store offset=36
                        local.get 9
                        local.get 9
                        i32.load
                        local.get 9
                        i32.load offset=76
                        local.get 9
                        i32.load offset=4
                        i32.mul
                        i32.add
                        i32.store offset=32
                        local.get 9
                        local.get 9
                        f32.load offset=56
                        local.get 9
                        i32.load offset=92
                        local.get 9
                        i32.load offset=36
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        local.get 9
                        i32.load offset=80
                        local.get 9
                        i32.load offset=32
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        f32.mul
                        f32.add
                        f32.store offset=56
                        local.get 9
                        local.get 9
                        f32.load offset=52
                        local.get 9
                        i32.load offset=92
                        local.get 9
                        i32.load offset=36
                        i32.const 1
                        i32.add
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        local.get 9
                        i32.load offset=80
                        local.get 9
                        i32.load offset=32
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        f32.mul
                        f32.add
                        f32.store offset=52
                        local.get 9
                        local.get 9
                        f32.load offset=48
                        local.get 9
                        i32.load offset=92
                        local.get 9
                        i32.load offset=36
                        i32.const 2
                        i32.add
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        local.get 9
                        i32.load offset=80
                        local.get 9
                        i32.load offset=32
                        i32.const 2
                        i32.shl
                        i32.add
                        f32.load
                        f32.mul
                        f32.add
                        f32.store offset=48
                        local.get 9
                        local.get 9
                        i32.load
                        i32.const 1
                        i32.add
                        i32.store
                        br 1 (;@9;)
                      end
                    end
                    local.get 9
                    local.get 9
                    i32.load offset=4
                    i32.const 1
                    i32.add
                    i32.store offset=4
                    br 1 (;@7;)
                  end
                end
                local.get 9
                local.get 9
                i32.load offset=8
                local.get 9
                i32.load offset=88
                local.get 9
                i32.load offset=12
                i32.mul
                i32.add
                i32.const 2
                i32.shl
                i32.store offset=28
                local.get 9
                i32.load offset=92
                local.get 9
                i32.load offset=28
                i32.const 2
                i32.shl
                i32.add
                block (result f64)  ;; label = @7
                  f64.const 0x1.fep+7 (;=255;)
                  local.get 9
                  f32.load offset=56
                  local.get 9
                  f32.load offset=68
                  f32.div
                  f64.promote_f32
                  f64.const 0x1.fep+7 (;=255;)
                  f64.gt
                  br_if 0 (;@7;)
                  drop
                  block (result f64)  ;; label = @8
                    f64.const 0x0p+0 (;=0;)
                    local.get 9
                    f32.load offset=56
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                    f64.const 0x0p+0 (;=0;)
                    f64.lt
                    br_if 0 (;@8;)
                    drop
                    local.get 9
                    f32.load offset=56
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                  end
                end
                f32.demote_f64
                f32.store
                local.get 9
                i32.load offset=92
                local.get 9
                i32.load offset=28
                i32.const 2
                i32.shl
                i32.add
                i32.const 4
                i32.add
                block (result f64)  ;; label = @7
                  f64.const 0x1.fep+7 (;=255;)
                  local.get 9
                  f32.load offset=52
                  local.get 9
                  f32.load offset=68
                  f32.div
                  f64.promote_f32
                  f64.const 0x1.fep+7 (;=255;)
                  f64.gt
                  br_if 0 (;@7;)
                  drop
                  block (result f64)  ;; label = @8
                    f64.const 0x0p+0 (;=0;)
                    local.get 9
                    f32.load offset=52
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                    f64.const 0x0p+0 (;=0;)
                    f64.lt
                    br_if 0 (;@8;)
                    drop
                    local.get 9
                    f32.load offset=52
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                  end
                end
                f32.demote_f64
                f32.store
                local.get 9
                i32.load offset=92
                local.get 9
                i32.load offset=28
                i32.const 2
                i32.add
                i32.const 2
                i32.shl
                i32.add
                block (result f64)  ;; label = @7
                  f64.const 0x1.fep+7 (;=255;)
                  local.get 9
                  f32.load offset=48
                  local.get 9
                  f32.load offset=68
                  f32.div
                  f64.promote_f32
                  f64.const 0x1.fep+7 (;=255;)
                  f64.gt
                  br_if 0 (;@7;)
                  drop
                  block (result f64)  ;; label = @8
                    f64.const 0x0p+0 (;=0;)
                    local.get 9
                    f32.load offset=48
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                    f64.const 0x0p+0 (;=0;)
                    f64.lt
                    br_if 0 (;@8;)
                    drop
                    local.get 9
                    f32.load offset=48
                    local.get 9
                    f32.load offset=68
                    f32.div
                    f64.promote_f32
                  end
                end
                f32.demote_f64
                f32.store
                local.get 9
                local.get 9
                i32.load offset=8
                i32.const 1
                i32.add
                i32.store offset=8
                br 1 (;@5;)
              end
            end
            local.get 9
            local.get 9
            i32.load offset=12
            i32.const 1
            i32.add
            i32.store offset=12
            br 1 (;@3;)
          end
        end
        local.get 9
        local.get 9
        i32.load offset=16
        i32.const 1
        i32.add
        i32.store offset=16
        br 1 (;@1;)
      end
    end
    local.get 9
    i32.const 96
    i32.add
    local.tee 0
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 0
    global.set 0)
  (func (;11;) (type 12) (param i32) (result f64)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    local.get 0
    i32.store offset=12
    local.get 1
    i32.load offset=12
    f64.convert_i32_s
    f64.floor)
  (func (;12;) (type 4) (result i32)
    i32.const 1032)
  (func (;13;) (type 0) (param i32) (result i32)
    (local i32 i32)
    i32.const 1552
    i32.load
    local.tee 1
    local.get 0
    i32.const 3
    i32.add
    i32.const -4
    i32.and
    local.tee 2
    i32.add
    local.set 0
    block  ;; label = @1
      local.get 2
      i32.const 1
      i32.ge_s
      i32.const 0
      local.get 0
      local.get 1
      i32.le_u
      select
      br_if 0 (;@1;)
      local.get 0
      memory.size
      i32.const 16
      i32.shl
      i32.gt_u
      if  ;; label = @2
        local.get 0
        call 0
        i32.eqz
        br_if 1 (;@1;)
      end
      i32.const 1552
      local.get 0
      i32.store
      local.get 1
      return
    end
    i32.const 1032
    i32.const 48
    i32.store
    i32.const -1)
  (func (;14;) (type 0) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 11
    local.tee 1
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 1
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          i32.const 244
                          i32.le_u
                          if  ;; label = @12
                            i32.const 1036
                            i32.load
                            local.tee 6
                            i32.const 16
                            local.get 0
                            i32.const 11
                            i32.add
                            i32.const -8
                            i32.and
                            local.get 0
                            i32.const 11
                            i32.lt_u
                            select
                            local.tee 5
                            i32.const 3
                            i32.shr_u
                            local.tee 0
                            i32.shr_u
                            local.tee 1
                            i32.const 3
                            i32.and
                            if  ;; label = @13
                              local.get 1
                              i32.const -1
                              i32.xor
                              i32.const 1
                              i32.and
                              local.get 0
                              i32.add
                              local.tee 2
                              i32.const 3
                              i32.shl
                              local.tee 4
                              i32.const 1084
                              i32.add
                              i32.load
                              local.tee 1
                              i32.const 8
                              i32.add
                              local.set 0
                              block  ;; label = @14
                                local.get 1
                                i32.load offset=8
                                local.tee 3
                                local.get 4
                                i32.const 1076
                                i32.add
                                local.tee 4
                                i32.eq
                                if  ;; label = @15
                                  i32.const 1036
                                  local.get 6
                                  i32.const -2
                                  local.get 2
                                  i32.rotl
                                  i32.and
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 1052
                                i32.load
                                drop
                                local.get 3
                                local.get 4
                                i32.store offset=12
                                local.get 4
                                local.get 3
                                i32.store offset=8
                              end
                              local.get 1
                              local.get 2
                              i32.const 3
                              i32.shl
                              local.tee 2
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 2
                              i32.add
                              local.tee 1
                              local.get 1
                              i32.load offset=4
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              br 12 (;@1;)
                            end
                            local.get 5
                            i32.const 1044
                            i32.load
                            local.tee 8
                            i32.le_u
                            br_if 1 (;@11;)
                            local.get 1
                            if  ;; label = @13
                              block  ;; label = @14
                                i32.const 2
                                local.get 0
                                i32.shl
                                local.tee 2
                                i32.const 0
                                local.get 2
                                i32.sub
                                i32.or
                                local.get 1
                                local.get 0
                                i32.shl
                                i32.and
                                local.tee 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 0
                                local.get 0
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 0
                                i32.shr_u
                                local.tee 1
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 2
                                local.get 0
                                i32.or
                                local.get 1
                                local.get 2
                                i32.shr_u
                                local.tee 0
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                i32.add
                                local.tee 2
                                i32.const 3
                                i32.shl
                                local.tee 3
                                i32.const 1084
                                i32.add
                                i32.load
                                local.tee 1
                                i32.load offset=8
                                local.tee 0
                                local.get 3
                                i32.const 1076
                                i32.add
                                local.tee 3
                                i32.eq
                                if  ;; label = @15
                                  i32.const 1036
                                  local.get 6
                                  i32.const -2
                                  local.get 2
                                  i32.rotl
                                  i32.and
                                  local.tee 6
                                  i32.store
                                  br 1 (;@14;)
                                end
                                i32.const 1052
                                i32.load
                                drop
                                local.get 0
                                local.get 3
                                i32.store offset=12
                                local.get 3
                                local.get 0
                                i32.store offset=8
                              end
                              local.get 1
                              i32.const 8
                              i32.add
                              local.set 0
                              local.get 1
                              local.get 5
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 5
                              i32.add
                              local.tee 7
                              local.get 2
                              i32.const 3
                              i32.shl
                              local.tee 2
                              local.get 5
                              i32.sub
                              local.tee 3
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 1
                              local.get 2
                              i32.add
                              local.get 3
                              i32.store
                              local.get 8
                              if  ;; label = @14
                                local.get 8
                                i32.const 3
                                i32.shr_u
                                local.tee 4
                                i32.const 3
                                i32.shl
                                i32.const 1076
                                i32.add
                                local.set 1
                                i32.const 1056
                                i32.load
                                local.set 2
                                block (result i32)  ;; label = @15
                                  local.get 6
                                  i32.const 1
                                  local.get 4
                                  i32.shl
                                  local.tee 4
                                  i32.and
                                  i32.eqz
                                  if  ;; label = @16
                                    i32.const 1036
                                    local.get 4
                                    local.get 6
                                    i32.or
                                    i32.store
                                    local.get 1
                                    br 1 (;@15;)
                                  end
                                  local.get 1
                                  i32.load offset=8
                                end
                                local.set 4
                                local.get 1
                                local.get 2
                                i32.store offset=8
                                local.get 4
                                local.get 2
                                i32.store offset=12
                                local.get 2
                                local.get 1
                                i32.store offset=12
                                local.get 2
                                local.get 4
                                i32.store offset=8
                              end
                              i32.const 1056
                              local.get 7
                              i32.store
                              i32.const 1044
                              local.get 3
                              i32.store
                              br 12 (;@1;)
                            end
                            i32.const 1040
                            i32.load
                            local.tee 10
                            i32.eqz
                            br_if 1 (;@11;)
                            local.get 10
                            i32.const 0
                            local.get 10
                            i32.sub
                            i32.and
                            i32.const -1
                            i32.add
                            local.tee 0
                            local.get 0
                            i32.const 12
                            i32.shr_u
                            i32.const 16
                            i32.and
                            local.tee 0
                            i32.shr_u
                            local.tee 1
                            i32.const 5
                            i32.shr_u
                            i32.const 8
                            i32.and
                            local.tee 2
                            local.get 0
                            i32.or
                            local.get 1
                            local.get 2
                            i32.shr_u
                            local.tee 0
                            i32.const 2
                            i32.shr_u
                            i32.const 4
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            local.tee 0
                            i32.const 1
                            i32.shr_u
                            i32.const 2
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            local.tee 0
                            i32.const 1
                            i32.shr_u
                            i32.const 1
                            i32.and
                            local.tee 1
                            i32.or
                            local.get 0
                            local.get 1
                            i32.shr_u
                            i32.add
                            i32.const 2
                            i32.shl
                            i32.const 1340
                            i32.add
                            i32.load
                            local.tee 1
                            i32.load offset=4
                            i32.const -8
                            i32.and
                            local.get 5
                            i32.sub
                            local.set 3
                            local.get 1
                            local.set 2
                            loop  ;; label = @13
                              block  ;; label = @14
                                local.get 2
                                i32.load offset=16
                                local.tee 0
                                i32.eqz
                                if  ;; label = @15
                                  local.get 2
                                  i32.const 20
                                  i32.add
                                  i32.load
                                  local.tee 0
                                  i32.eqz
                                  br_if 1 (;@14;)
                                end
                                local.get 0
                                i32.load offset=4
                                i32.const -8
                                i32.and
                                local.get 5
                                i32.sub
                                local.tee 2
                                local.get 3
                                local.get 2
                                local.get 3
                                i32.lt_u
                                local.tee 2
                                select
                                local.set 3
                                local.get 0
                                local.get 1
                                local.get 2
                                select
                                local.set 1
                                local.get 0
                                local.set 2
                                br 1 (;@13;)
                              end
                            end
                            local.get 1
                            i32.load offset=24
                            local.set 9
                            local.get 1
                            local.get 1
                            i32.load offset=12
                            local.tee 4
                            i32.ne
                            if  ;; label = @13
                              i32.const 1052
                              i32.load
                              local.get 1
                              i32.load offset=8
                              local.tee 0
                              i32.le_u
                              if  ;; label = @14
                                local.get 0
                                i32.load offset=12
                                drop
                              end
                              local.get 0
                              local.get 4
                              i32.store offset=12
                              local.get 4
                              local.get 0
                              i32.store offset=8
                              br 11 (;@2;)
                            end
                            local.get 1
                            i32.const 20
                            i32.add
                            local.tee 2
                            i32.load
                            local.tee 0
                            i32.eqz
                            if  ;; label = @13
                              local.get 1
                              i32.load offset=16
                              local.tee 0
                              i32.eqz
                              br_if 3 (;@10;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.set 2
                            end
                            loop  ;; label = @13
                              local.get 2
                              local.set 7
                              local.get 0
                              local.tee 4
                              i32.const 20
                              i32.add
                              local.tee 2
                              i32.load
                              local.tee 0
                              br_if 0 (;@13;)
                              local.get 4
                              i32.const 16
                              i32.add
                              local.set 2
                              local.get 4
                              i32.load offset=16
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                            local.get 7
                            i32.const 0
                            i32.store
                            br 10 (;@2;)
                          end
                          i32.const -1
                          local.set 5
                          local.get 0
                          i32.const -65
                          i32.gt_u
                          br_if 0 (;@11;)
                          local.get 0
                          i32.const 11
                          i32.add
                          local.tee 0
                          i32.const -8
                          i32.and
                          local.set 5
                          i32.const 1040
                          i32.load
                          local.tee 7
                          i32.eqz
                          br_if 0 (;@11;)
                          i32.const 0
                          local.get 5
                          i32.sub
                          local.set 2
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block (result i32)  ;; label = @15
                                  i32.const 0
                                  local.get 0
                                  i32.const 8
                                  i32.shr_u
                                  local.tee 0
                                  i32.eqz
                                  br_if 0 (;@15;)
                                  drop
                                  i32.const 31
                                  local.get 5
                                  i32.const 16777215
                                  i32.gt_u
                                  br_if 0 (;@15;)
                                  drop
                                  local.get 0
                                  local.get 0
                                  i32.const 1048320
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 8
                                  i32.and
                                  local.tee 0
                                  i32.shl
                                  local.tee 1
                                  local.get 1
                                  i32.const 520192
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 4
                                  i32.and
                                  local.tee 1
                                  i32.shl
                                  local.tee 3
                                  local.get 3
                                  i32.const 245760
                                  i32.add
                                  i32.const 16
                                  i32.shr_u
                                  i32.const 2
                                  i32.and
                                  local.tee 3
                                  i32.shl
                                  i32.const 15
                                  i32.shr_u
                                  local.get 0
                                  local.get 1
                                  i32.or
                                  local.get 3
                                  i32.or
                                  i32.sub
                                  local.tee 0
                                  i32.const 1
                                  i32.shl
                                  local.get 5
                                  local.get 0
                                  i32.const 21
                                  i32.add
                                  i32.shr_u
                                  i32.const 1
                                  i32.and
                                  i32.or
                                  i32.const 28
                                  i32.add
                                end
                                local.tee 8
                                i32.const 2
                                i32.shl
                                i32.const 1340
                                i32.add
                                i32.load
                                local.tee 3
                                i32.eqz
                                if  ;; label = @15
                                  i32.const 0
                                  local.set 0
                                  br 1 (;@14;)
                                end
                                local.get 5
                                i32.const 0
                                i32.const 25
                                local.get 8
                                i32.const 1
                                i32.shr_u
                                i32.sub
                                local.get 8
                                i32.const 31
                                i32.eq
                                select
                                i32.shl
                                local.set 1
                                i32.const 0
                                local.set 0
                                loop  ;; label = @15
                                  block  ;; label = @16
                                    local.get 3
                                    i32.load offset=4
                                    i32.const -8
                                    i32.and
                                    local.get 5
                                    i32.sub
                                    local.tee 6
                                    local.get 2
                                    i32.ge_u
                                    br_if 0 (;@16;)
                                    local.get 3
                                    local.set 4
                                    local.get 6
                                    local.tee 2
                                    br_if 0 (;@16;)
                                    i32.const 0
                                    local.set 2
                                    local.get 3
                                    local.set 0
                                    br 3 (;@13;)
                                  end
                                  local.get 0
                                  local.get 3
                                  i32.const 20
                                  i32.add
                                  i32.load
                                  local.tee 6
                                  local.get 6
                                  local.get 3
                                  local.get 1
                                  i32.const 29
                                  i32.shr_u
                                  i32.const 4
                                  i32.and
                                  i32.add
                                  i32.const 16
                                  i32.add
                                  i32.load
                                  local.tee 3
                                  i32.eq
                                  select
                                  local.get 0
                                  local.get 6
                                  select
                                  local.set 0
                                  local.get 1
                                  local.get 3
                                  i32.const 0
                                  i32.ne
                                  i32.shl
                                  local.set 1
                                  local.get 3
                                  br_if 0 (;@15;)
                                end
                              end
                              local.get 0
                              local.get 4
                              i32.or
                              i32.eqz
                              if  ;; label = @14
                                i32.const 2
                                local.get 8
                                i32.shl
                                local.tee 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.or
                                local.get 7
                                i32.and
                                local.tee 0
                                i32.eqz
                                br_if 3 (;@11;)
                                local.get 0
                                i32.const 0
                                local.get 0
                                i32.sub
                                i32.and
                                i32.const -1
                                i32.add
                                local.tee 0
                                local.get 0
                                i32.const 12
                                i32.shr_u
                                i32.const 16
                                i32.and
                                local.tee 0
                                i32.shr_u
                                local.tee 1
                                i32.const 5
                                i32.shr_u
                                i32.const 8
                                i32.and
                                local.tee 3
                                local.get 0
                                i32.or
                                local.get 1
                                local.get 3
                                i32.shr_u
                                local.tee 0
                                i32.const 2
                                i32.shr_u
                                i32.const 4
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 2
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                local.tee 0
                                i32.const 1
                                i32.shr_u
                                i32.const 1
                                i32.and
                                local.tee 1
                                i32.or
                                local.get 0
                                local.get 1
                                i32.shr_u
                                i32.add
                                i32.const 2
                                i32.shl
                                i32.const 1340
                                i32.add
                                i32.load
                                local.set 0
                              end
                              local.get 0
                              i32.eqz
                              br_if 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 0
                              i32.load offset=4
                              i32.const -8
                              i32.and
                              local.get 5
                              i32.sub
                              local.tee 3
                              local.get 2
                              i32.lt_u
                              local.set 1
                              local.get 3
                              local.get 2
                              local.get 1
                              select
                              local.set 2
                              local.get 0
                              local.get 4
                              local.get 1
                              select
                              local.set 4
                              local.get 0
                              i32.load offset=16
                              local.tee 1
                              if (result i32)  ;; label = @14
                                local.get 1
                              else
                                local.get 0
                                i32.const 20
                                i32.add
                                i32.load
                              end
                              local.tee 0
                              br_if 0 (;@13;)
                            end
                          end
                          local.get 4
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 2
                          i32.const 1044
                          i32.load
                          local.get 5
                          i32.sub
                          i32.ge_u
                          br_if 0 (;@11;)
                          local.get 4
                          i32.load offset=24
                          local.set 8
                          local.get 4
                          local.get 4
                          i32.load offset=12
                          local.tee 1
                          i32.ne
                          if  ;; label = @12
                            i32.const 1052
                            i32.load
                            local.get 4
                            i32.load offset=8
                            local.tee 0
                            i32.le_u
                            if  ;; label = @13
                              local.get 0
                              i32.load offset=12
                              drop
                            end
                            local.get 0
                            local.get 1
                            i32.store offset=12
                            local.get 1
                            local.get 0
                            i32.store offset=8
                            br 9 (;@3;)
                          end
                          local.get 4
                          i32.const 20
                          i32.add
                          local.tee 3
                          i32.load
                          local.tee 0
                          i32.eqz
                          if  ;; label = @12
                            local.get 4
                            i32.load offset=16
                            local.tee 0
                            i32.eqz
                            br_if 3 (;@9;)
                            local.get 4
                            i32.const 16
                            i32.add
                            local.set 3
                          end
                          loop  ;; label = @12
                            local.get 3
                            local.set 6
                            local.get 0
                            local.tee 1
                            i32.const 20
                            i32.add
                            local.tee 3
                            i32.load
                            local.tee 0
                            br_if 0 (;@12;)
                            local.get 1
                            i32.const 16
                            i32.add
                            local.set 3
                            local.get 1
                            i32.load offset=16
                            local.tee 0
                            br_if 0 (;@12;)
                          end
                          local.get 6
                          i32.const 0
                          i32.store
                          br 8 (;@3;)
                        end
                        i32.const 1044
                        i32.load
                        local.tee 1
                        local.get 5
                        i32.ge_u
                        if  ;; label = @11
                          i32.const 1056
                          i32.load
                          local.set 0
                          block  ;; label = @12
                            local.get 1
                            local.get 5
                            i32.sub
                            local.tee 2
                            i32.const 16
                            i32.ge_u
                            if  ;; label = @13
                              i32.const 1044
                              local.get 2
                              i32.store
                              i32.const 1056
                              local.get 0
                              local.get 5
                              i32.add
                              local.tee 3
                              i32.store
                              local.get 3
                              local.get 2
                              i32.const 1
                              i32.or
                              i32.store offset=4
                              local.get 0
                              local.get 1
                              i32.add
                              local.get 2
                              i32.store
                              local.get 0
                              local.get 5
                              i32.const 3
                              i32.or
                              i32.store offset=4
                              br 1 (;@12;)
                            end
                            i32.const 1056
                            i32.const 0
                            i32.store
                            i32.const 1044
                            i32.const 0
                            i32.store
                            local.get 0
                            local.get 1
                            i32.const 3
                            i32.or
                            i32.store offset=4
                            local.get 0
                            local.get 1
                            i32.add
                            local.tee 1
                            local.get 1
                            i32.load offset=4
                            i32.const 1
                            i32.or
                            i32.store offset=4
                          end
                          local.get 0
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        i32.const 1048
                        i32.load
                        local.tee 1
                        local.get 5
                        i32.gt_u
                        if  ;; label = @11
                          i32.const 1048
                          local.get 1
                          local.get 5
                          i32.sub
                          local.tee 1
                          i32.store
                          i32.const 1060
                          i32.const 1060
                          i32.load
                          local.tee 0
                          local.get 5
                          i32.add
                          local.tee 2
                          i32.store
                          local.get 2
                          local.get 1
                          i32.const 1
                          i32.or
                          i32.store offset=4
                          local.get 0
                          local.get 5
                          i32.const 3
                          i32.or
                          i32.store offset=4
                          local.get 0
                          i32.const 8
                          i32.add
                          local.set 0
                          br 10 (;@1;)
                        end
                        i32.const 0
                        local.set 0
                        local.get 5
                        i32.const 47
                        i32.add
                        local.tee 4
                        block (result i32)  ;; label = @11
                          i32.const 1508
                          i32.load
                          if  ;; label = @12
                            i32.const 1516
                            i32.load
                            br 1 (;@11;)
                          end
                          i32.const 1520
                          i64.const -1
                          i64.store align=4
                          i32.const 1512
                          i64.const 17592186048512
                          i64.store align=4
                          i32.const 1508
                          local.get 11
                          i32.const 12
                          i32.add
                          i32.const -16
                          i32.and
                          i32.const 1431655768
                          i32.xor
                          i32.store
                          i32.const 1528
                          i32.const 0
                          i32.store
                          i32.const 1480
                          i32.const 0
                          i32.store
                          i32.const 4096
                        end
                        local.tee 2
                        i32.add
                        local.tee 6
                        i32.const 0
                        local.get 2
                        i32.sub
                        local.tee 7
                        i32.and
                        local.tee 2
                        local.get 5
                        i32.le_u
                        br_if 9 (;@1;)
                        i32.const 1476
                        i32.load
                        local.tee 3
                        if  ;; label = @11
                          i32.const 1468
                          i32.load
                          local.tee 8
                          local.get 2
                          i32.add
                          local.tee 9
                          local.get 8
                          i32.le_u
                          local.get 9
                          local.get 3
                          i32.gt_u
                          i32.or
                          br_if 10 (;@1;)
                        end
                        i32.const 1480
                        i32.load8_u
                        i32.const 4
                        i32.and
                        br_if 4 (;@6;)
                        block  ;; label = @11
                          block  ;; label = @12
                            i32.const 1060
                            i32.load
                            local.tee 3
                            if  ;; label = @13
                              i32.const 1484
                              local.set 0
                              loop  ;; label = @14
                                local.get 0
                                i32.load
                                local.tee 8
                                local.get 3
                                i32.le_u
                                if  ;; label = @15
                                  local.get 8
                                  local.get 0
                                  i32.load offset=4
                                  i32.add
                                  local.get 3
                                  i32.gt_u
                                  br_if 3 (;@12;)
                                end
                                local.get 0
                                i32.load offset=8
                                local.tee 0
                                br_if 0 (;@14;)
                              end
                            end
                            i32.const 0
                            call 13
                            local.tee 1
                            i32.const -1
                            i32.eq
                            br_if 5 (;@7;)
                            local.get 2
                            local.set 6
                            i32.const 1512
                            i32.load
                            local.tee 0
                            i32.const -1
                            i32.add
                            local.tee 3
                            local.get 1
                            i32.and
                            if  ;; label = @13
                              local.get 2
                              local.get 1
                              i32.sub
                              local.get 1
                              local.get 3
                              i32.add
                              i32.const 0
                              local.get 0
                              i32.sub
                              i32.and
                              i32.add
                              local.set 6
                            end
                            local.get 6
                            local.get 5
                            i32.le_u
                            local.get 6
                            i32.const 2147483646
                            i32.gt_u
                            i32.or
                            br_if 5 (;@7;)
                            i32.const 1476
                            i32.load
                            local.tee 0
                            if  ;; label = @13
                              i32.const 1468
                              i32.load
                              local.tee 3
                              local.get 6
                              i32.add
                              local.tee 7
                              local.get 3
                              i32.le_u
                              local.get 7
                              local.get 0
                              i32.gt_u
                              i32.or
                              br_if 6 (;@7;)
                            end
                            local.get 6
                            call 13
                            local.tee 0
                            local.get 1
                            i32.ne
                            br_if 1 (;@11;)
                            br 7 (;@5;)
                          end
                          local.get 6
                          local.get 1
                          i32.sub
                          local.get 7
                          i32.and
                          local.tee 6
                          i32.const 2147483646
                          i32.gt_u
                          br_if 4 (;@7;)
                          local.get 6
                          call 13
                          local.tee 1
                          local.get 0
                          i32.load
                          local.get 0
                          i32.load offset=4
                          i32.add
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 1
                          local.set 0
                        end
                        local.get 0
                        i32.const -1
                        i32.eq
                        local.get 5
                        i32.const 48
                        i32.add
                        local.get 6
                        i32.le_u
                        i32.or
                        i32.eqz
                        if  ;; label = @11
                          i32.const 1516
                          i32.load
                          local.tee 1
                          local.get 4
                          local.get 6
                          i32.sub
                          i32.add
                          i32.const 0
                          local.get 1
                          i32.sub
                          i32.and
                          local.tee 1
                          i32.const 2147483646
                          i32.gt_u
                          if  ;; label = @12
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          local.get 1
                          call 13
                          i32.const -1
                          i32.ne
                          if  ;; label = @12
                            local.get 1
                            local.get 6
                            i32.add
                            local.set 6
                            local.get 0
                            local.set 1
                            br 7 (;@5;)
                          end
                          i32.const 0
                          local.get 6
                          i32.sub
                          call 13
                          drop
                          br 4 (;@7;)
                        end
                        local.get 0
                        local.tee 1
                        i32.const -1
                        i32.ne
                        br_if 5 (;@5;)
                        br 3 (;@7;)
                      end
                      i32.const 0
                      local.set 4
                      br 7 (;@2;)
                    end
                    i32.const 0
                    local.set 1
                    br 5 (;@3;)
                  end
                  local.get 1
                  i32.const -1
                  i32.ne
                  br_if 2 (;@5;)
                end
                i32.const 1480
                i32.const 1480
                i32.load
                i32.const 4
                i32.or
                i32.store
              end
              local.get 2
              i32.const 2147483646
              i32.gt_u
              br_if 1 (;@4;)
              local.get 2
              call 13
              local.tee 1
              i32.const 0
              call 13
              local.tee 0
              i32.ge_u
              local.get 1
              i32.const -1
              i32.eq
              i32.or
              local.get 0
              i32.const -1
              i32.eq
              i32.or
              br_if 1 (;@4;)
              local.get 0
              local.get 1
              i32.sub
              local.tee 6
              local.get 5
              i32.const 40
              i32.add
              i32.le_u
              br_if 1 (;@4;)
            end
            i32.const 1468
            i32.const 1468
            i32.load
            local.get 6
            i32.add
            local.tee 0
            i32.store
            local.get 0
            i32.const 1472
            i32.load
            i32.gt_u
            if  ;; label = @5
              i32.const 1472
              local.get 0
              i32.store
            end
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 1060
                  i32.load
                  local.tee 3
                  if  ;; label = @8
                    i32.const 1484
                    local.set 0
                    loop  ;; label = @9
                      local.get 1
                      local.get 0
                      i32.load
                      local.tee 2
                      local.get 0
                      i32.load offset=4
                      local.tee 4
                      i32.add
                      i32.eq
                      br_if 2 (;@7;)
                      local.get 0
                      i32.load offset=8
                      local.tee 0
                      br_if 0 (;@9;)
                    end
                    br 2 (;@6;)
                  end
                  i32.const 1052
                  i32.load
                  local.tee 0
                  i32.const 0
                  local.get 1
                  local.get 0
                  i32.ge_u
                  select
                  i32.eqz
                  if  ;; label = @8
                    i32.const 1052
                    local.get 1
                    i32.store
                  end
                  i32.const 0
                  local.set 0
                  i32.const 1488
                  local.get 6
                  i32.store
                  i32.const 1484
                  local.get 1
                  i32.store
                  i32.const 1068
                  i32.const -1
                  i32.store
                  i32.const 1072
                  i32.const 1508
                  i32.load
                  i32.store
                  i32.const 1496
                  i32.const 0
                  i32.store
                  loop  ;; label = @8
                    local.get 0
                    i32.const 3
                    i32.shl
                    local.tee 2
                    i32.const 1084
                    i32.add
                    local.get 2
                    i32.const 1076
                    i32.add
                    local.tee 3
                    i32.store
                    local.get 2
                    i32.const 1088
                    i32.add
                    local.get 3
                    i32.store
                    local.get 0
                    i32.const 1
                    i32.add
                    local.tee 0
                    i32.const 32
                    i32.ne
                    br_if 0 (;@8;)
                  end
                  i32.const 1048
                  local.get 6
                  i32.const -40
                  i32.add
                  local.tee 0
                  i32.const -8
                  local.get 1
                  i32.sub
                  i32.const 7
                  i32.and
                  i32.const 0
                  local.get 1
                  i32.const 8
                  i32.add
                  i32.const 7
                  i32.and
                  select
                  local.tee 2
                  i32.sub
                  local.tee 3
                  i32.store
                  i32.const 1060
                  local.get 1
                  local.get 2
                  i32.add
                  local.tee 2
                  i32.store
                  local.get 2
                  local.get 3
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  local.get 0
                  local.get 1
                  i32.add
                  i32.const 40
                  i32.store offset=4
                  i32.const 1064
                  i32.const 1524
                  i32.load
                  i32.store
                  br 2 (;@5;)
                end
                local.get 0
                i32.load8_u offset=12
                i32.const 8
                i32.and
                local.get 1
                local.get 3
                i32.le_u
                i32.or
                local.get 2
                local.get 3
                i32.gt_u
                i32.or
                br_if 0 (;@6;)
                local.get 0
                local.get 4
                local.get 6
                i32.add
                i32.store offset=4
                i32.const 1060
                local.get 3
                i32.const -8
                local.get 3
                i32.sub
                i32.const 7
                i32.and
                i32.const 0
                local.get 3
                i32.const 8
                i32.add
                i32.const 7
                i32.and
                select
                local.tee 0
                i32.add
                local.tee 1
                i32.store
                i32.const 1048
                i32.const 1048
                i32.load
                local.get 6
                i32.add
                local.tee 2
                local.get 0
                i32.sub
                local.tee 0
                i32.store
                local.get 1
                local.get 0
                i32.const 1
                i32.or
                i32.store offset=4
                local.get 2
                local.get 3
                i32.add
                i32.const 40
                i32.store offset=4
                i32.const 1064
                i32.const 1524
                i32.load
                i32.store
                br 1 (;@5;)
              end
              local.get 1
              i32.const 1052
              i32.load
              local.tee 4
              i32.lt_u
              if  ;; label = @6
                i32.const 1052
                local.get 1
                i32.store
                local.get 1
                local.set 4
              end
              local.get 1
              local.get 6
              i32.add
              local.set 2
              i32.const 1484
              local.set 0
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          loop  ;; label = @12
                            local.get 2
                            local.get 0
                            i32.load
                            i32.ne
                            if  ;; label = @13
                              local.get 0
                              i32.load offset=8
                              local.tee 0
                              br_if 1 (;@12;)
                              br 2 (;@11;)
                            end
                          end
                          local.get 0
                          i32.load8_u offset=12
                          i32.const 8
                          i32.and
                          i32.eqz
                          br_if 1 (;@10;)
                        end
                        i32.const 1484
                        local.set 0
                        loop  ;; label = @11
                          local.get 0
                          i32.load
                          local.tee 2
                          local.get 3
                          i32.le_u
                          if  ;; label = @12
                            local.get 2
                            local.get 0
                            i32.load offset=4
                            i32.add
                            local.tee 4
                            local.get 3
                            i32.gt_u
                            br_if 3 (;@9;)
                          end
                          local.get 0
                          i32.load offset=8
                          local.set 0
                          br 0 (;@11;)
                        end
                        unreachable
                      end
                      local.get 0
                      local.get 1
                      i32.store
                      local.get 0
                      local.get 0
                      i32.load offset=4
                      local.get 6
                      i32.add
                      i32.store offset=4
                      local.get 1
                      i32.const -8
                      local.get 1
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 1
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      i32.add
                      local.tee 9
                      local.get 5
                      i32.const 3
                      i32.or
                      i32.store offset=4
                      local.get 2
                      i32.const -8
                      local.get 2
                      i32.sub
                      i32.const 7
                      i32.and
                      i32.const 0
                      local.get 2
                      i32.const 8
                      i32.add
                      i32.const 7
                      i32.and
                      select
                      i32.add
                      local.tee 1
                      local.get 9
                      i32.sub
                      local.get 5
                      i32.sub
                      local.set 0
                      local.get 5
                      local.get 9
                      i32.add
                      local.set 7
                      local.get 1
                      local.get 3
                      i32.eq
                      if  ;; label = @10
                        i32.const 1060
                        local.get 7
                        i32.store
                        i32.const 1048
                        i32.const 1048
                        i32.load
                        local.get 0
                        i32.add
                        local.tee 0
                        i32.store
                        local.get 7
                        local.get 0
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        br 3 (;@7;)
                      end
                      local.get 1
                      i32.const 1056
                      i32.load
                      i32.eq
                      if  ;; label = @10
                        i32.const 1056
                        local.get 7
                        i32.store
                        i32.const 1044
                        i32.const 1044
                        i32.load
                        local.get 0
                        i32.add
                        local.tee 0
                        i32.store
                        local.get 7
                        local.get 0
                        i32.const 1
                        i32.or
                        i32.store offset=4
                        local.get 0
                        local.get 7
                        i32.add
                        local.get 0
                        i32.store
                        br 3 (;@7;)
                      end
                      local.get 1
                      i32.load offset=4
                      local.tee 2
                      i32.const 3
                      i32.and
                      i32.const 1
                      i32.eq
                      if  ;; label = @10
                        local.get 2
                        i32.const -8
                        i32.and
                        local.set 10
                        block  ;; label = @11
                          local.get 2
                          i32.const 255
                          i32.le_u
                          if  ;; label = @12
                            local.get 1
                            i32.load offset=8
                            local.tee 3
                            local.get 2
                            i32.const 3
                            i32.shr_u
                            local.tee 4
                            i32.const 3
                            i32.shl
                            i32.const 1076
                            i32.add
                            i32.ne
                            drop
                            local.get 3
                            local.get 1
                            i32.load offset=12
                            local.tee 2
                            i32.eq
                            if  ;; label = @13
                              i32.const 1036
                              i32.const 1036
                              i32.load
                              i32.const -2
                              local.get 4
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            local.get 3
                            local.get 2
                            i32.store offset=12
                            local.get 2
                            local.get 3
                            i32.store offset=8
                            br 1 (;@11;)
                          end
                          local.get 1
                          i32.load offset=24
                          local.set 8
                          block  ;; label = @12
                            local.get 1
                            local.get 1
                            i32.load offset=12
                            local.tee 6
                            i32.ne
                            if  ;; label = @13
                              local.get 4
                              local.get 1
                              i32.load offset=8
                              local.tee 2
                              i32.le_u
                              if  ;; label = @14
                                local.get 2
                                i32.load offset=12
                                drop
                              end
                              local.get 2
                              local.get 6
                              i32.store offset=12
                              local.get 6
                              local.get 2
                              i32.store offset=8
                              br 1 (;@12;)
                            end
                            block  ;; label = @13
                              local.get 1
                              i32.const 20
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              local.get 1
                              i32.const 16
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              i32.const 0
                              local.set 6
                              br 1 (;@12;)
                            end
                            loop  ;; label = @13
                              local.get 3
                              local.set 2
                              local.get 5
                              local.tee 6
                              i32.const 20
                              i32.add
                              local.tee 3
                              i32.load
                              local.tee 5
                              br_if 0 (;@13;)
                              local.get 6
                              i32.const 16
                              i32.add
                              local.set 3
                              local.get 6
                              i32.load offset=16
                              local.tee 5
                              br_if 0 (;@13;)
                            end
                            local.get 2
                            i32.const 0
                            i32.store
                          end
                          local.get 8
                          i32.eqz
                          br_if 0 (;@11;)
                          block  ;; label = @12
                            local.get 1
                            local.get 1
                            i32.load offset=28
                            local.tee 2
                            i32.const 2
                            i32.shl
                            i32.const 1340
                            i32.add
                            local.tee 3
                            i32.load
                            i32.eq
                            if  ;; label = @13
                              local.get 3
                              local.get 6
                              i32.store
                              local.get 6
                              br_if 1 (;@12;)
                              i32.const 1040
                              i32.const 1040
                              i32.load
                              i32.const -2
                              local.get 2
                              i32.rotl
                              i32.and
                              i32.store
                              br 2 (;@11;)
                            end
                            local.get 8
                            i32.const 16
                            i32.const 20
                            local.get 8
                            i32.load offset=16
                            local.get 1
                            i32.eq
                            select
                            i32.add
                            local.get 6
                            i32.store
                            local.get 6
                            i32.eqz
                            br_if 1 (;@11;)
                          end
                          local.get 6
                          local.get 8
                          i32.store offset=24
                          local.get 1
                          i32.load offset=16
                          local.tee 2
                          if  ;; label = @12
                            local.get 6
                            local.get 2
                            i32.store offset=16
                            local.get 2
                            local.get 6
                            i32.store offset=24
                          end
                          local.get 1
                          i32.load offset=20
                          local.tee 2
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 6
                          i32.const 20
                          i32.add
                          local.get 2
                          i32.store
                          local.get 2
                          local.get 6
                          i32.store offset=24
                        end
                        local.get 1
                        local.get 10
                        i32.add
                        local.set 1
                        local.get 0
                        local.get 10
                        i32.add
                        local.set 0
                      end
                      local.get 1
                      local.get 1
                      i32.load offset=4
                      i32.const -2
                      i32.and
                      i32.store offset=4
                      local.get 7
                      local.get 0
                      i32.const 1
                      i32.or
                      i32.store offset=4
                      local.get 0
                      local.get 7
                      i32.add
                      local.get 0
                      i32.store
                      local.get 0
                      i32.const 255
                      i32.le_u
                      if  ;; label = @10
                        local.get 0
                        i32.const 3
                        i32.shr_u
                        local.tee 1
                        i32.const 3
                        i32.shl
                        i32.const 1076
                        i32.add
                        local.set 0
                        block (result i32)  ;; label = @11
                          i32.const 1036
                          i32.load
                          local.tee 2
                          i32.const 1
                          local.get 1
                          i32.shl
                          local.tee 1
                          i32.and
                          i32.eqz
                          if  ;; label = @12
                            i32.const 1036
                            local.get 1
                            local.get 2
                            i32.or
                            i32.store
                            local.get 0
                            br 1 (;@11;)
                          end
                          local.get 0
                          i32.load offset=8
                        end
                        local.set 1
                        local.get 0
                        local.get 7
                        i32.store offset=8
                        local.get 1
                        local.get 7
                        i32.store offset=12
                        local.get 7
                        local.get 0
                        i32.store offset=12
                        local.get 7
                        local.get 1
                        i32.store offset=8
                        br 3 (;@7;)
                      end
                      local.get 7
                      block (result i32)  ;; label = @10
                        i32.const 0
                        local.get 0
                        i32.const 8
                        i32.shr_u
                        local.tee 1
                        i32.eqz
                        br_if 0 (;@10;)
                        drop
                        i32.const 31
                        local.get 0
                        i32.const 16777215
                        i32.gt_u
                        br_if 0 (;@10;)
                        drop
                        local.get 1
                        local.get 1
                        i32.const 1048320
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 8
                        i32.and
                        local.tee 1
                        i32.shl
                        local.tee 2
                        local.get 2
                        i32.const 520192
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 4
                        i32.and
                        local.tee 2
                        i32.shl
                        local.tee 3
                        local.get 3
                        i32.const 245760
                        i32.add
                        i32.const 16
                        i32.shr_u
                        i32.const 2
                        i32.and
                        local.tee 3
                        i32.shl
                        i32.const 15
                        i32.shr_u
                        local.get 1
                        local.get 2
                        i32.or
                        local.get 3
                        i32.or
                        i32.sub
                        local.tee 1
                        i32.const 1
                        i32.shl
                        local.get 0
                        local.get 1
                        i32.const 21
                        i32.add
                        i32.shr_u
                        i32.const 1
                        i32.and
                        i32.or
                        i32.const 28
                        i32.add
                      end
                      local.tee 1
                      i32.store offset=28
                      local.get 7
                      i64.const 0
                      i64.store offset=16 align=4
                      local.get 1
                      i32.const 2
                      i32.shl
                      i32.const 1340
                      i32.add
                      local.set 2
                      block  ;; label = @10
                        i32.const 1040
                        i32.load
                        local.tee 3
                        i32.const 1
                        local.get 1
                        i32.shl
                        local.tee 4
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 1040
                          local.get 3
                          local.get 4
                          i32.or
                          i32.store
                          local.get 2
                          local.get 7
                          i32.store
                          br 1 (;@10;)
                        end
                        local.get 0
                        i32.const 0
                        i32.const 25
                        local.get 1
                        i32.const 1
                        i32.shr_u
                        i32.sub
                        local.get 1
                        i32.const 31
                        i32.eq
                        select
                        i32.shl
                        local.set 3
                        local.get 2
                        i32.load
                        local.set 1
                        loop  ;; label = @11
                          local.get 1
                          local.tee 2
                          i32.load offset=4
                          i32.const -8
                          i32.and
                          local.get 0
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 3
                          i32.const 29
                          i32.shr_u
                          local.set 1
                          local.get 3
                          i32.const 1
                          i32.shl
                          local.set 3
                          local.get 2
                          local.get 1
                          i32.const 4
                          i32.and
                          i32.add
                          i32.const 16
                          i32.add
                          local.tee 4
                          i32.load
                          local.tee 1
                          br_if 0 (;@11;)
                        end
                        local.get 4
                        local.get 7
                        i32.store
                      end
                      local.get 7
                      local.get 2
                      i32.store offset=24
                      local.get 7
                      local.get 7
                      i32.store offset=12
                      local.get 7
                      local.get 7
                      i32.store offset=8
                      br 2 (;@7;)
                    end
                    i32.const 1048
                    local.get 6
                    i32.const -40
                    i32.add
                    local.tee 0
                    i32.const -8
                    local.get 1
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 1
                    i32.const 8
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    local.tee 2
                    i32.sub
                    local.tee 7
                    i32.store
                    i32.const 1060
                    local.get 1
                    local.get 2
                    i32.add
                    local.tee 2
                    i32.store
                    local.get 2
                    local.get 7
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 0
                    local.get 1
                    i32.add
                    i32.const 40
                    i32.store offset=4
                    i32.const 1064
                    i32.const 1524
                    i32.load
                    i32.store
                    local.get 3
                    local.get 4
                    i32.const 39
                    local.get 4
                    i32.sub
                    i32.const 7
                    i32.and
                    i32.const 0
                    local.get 4
                    i32.const -39
                    i32.add
                    i32.const 7
                    i32.and
                    select
                    i32.add
                    i32.const -47
                    i32.add
                    local.tee 0
                    local.get 0
                    local.get 3
                    i32.const 16
                    i32.add
                    i32.lt_u
                    select
                    local.tee 2
                    i32.const 27
                    i32.store offset=4
                    local.get 2
                    i32.const 16
                    i32.add
                    i32.const 1492
                    i64.load align=4
                    i64.store align=4
                    local.get 2
                    i32.const 1484
                    i64.load align=4
                    i64.store offset=8 align=4
                    i32.const 1492
                    local.get 2
                    i32.const 8
                    i32.add
                    i32.store
                    i32.const 1488
                    local.get 6
                    i32.store
                    i32.const 1484
                    local.get 1
                    i32.store
                    i32.const 1496
                    i32.const 0
                    i32.store
                    local.get 2
                    i32.const 24
                    i32.add
                    local.set 0
                    loop  ;; label = @9
                      local.get 0
                      i32.const 7
                      i32.store offset=4
                      local.get 0
                      i32.const 8
                      i32.add
                      local.set 1
                      local.get 0
                      i32.const 4
                      i32.add
                      local.set 0
                      local.get 4
                      local.get 1
                      i32.gt_u
                      br_if 0 (;@9;)
                    end
                    local.get 2
                    local.get 3
                    i32.eq
                    br_if 3 (;@5;)
                    local.get 2
                    local.get 2
                    i32.load offset=4
                    i32.const -2
                    i32.and
                    i32.store offset=4
                    local.get 3
                    local.get 2
                    local.get 3
                    i32.sub
                    local.tee 4
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 2
                    local.get 4
                    i32.store
                    local.get 4
                    i32.const 255
                    i32.le_u
                    if  ;; label = @9
                      local.get 4
                      i32.const 3
                      i32.shr_u
                      local.tee 1
                      i32.const 3
                      i32.shl
                      i32.const 1076
                      i32.add
                      local.set 0
                      block (result i32)  ;; label = @10
                        i32.const 1036
                        i32.load
                        local.tee 2
                        i32.const 1
                        local.get 1
                        i32.shl
                        local.tee 1
                        i32.and
                        i32.eqz
                        if  ;; label = @11
                          i32.const 1036
                          local.get 1
                          local.get 2
                          i32.or
                          i32.store
                          local.get 0
                          br 1 (;@10;)
                        end
                        local.get 0
                        i32.load offset=8
                      end
                      local.set 1
                      local.get 0
                      local.get 3
                      i32.store offset=8
                      local.get 1
                      local.get 3
                      i32.store offset=12
                      local.get 3
                      local.get 0
                      i32.store offset=12
                      local.get 3
                      local.get 1
                      i32.store offset=8
                      br 4 (;@5;)
                    end
                    local.get 3
                    i64.const 0
                    i64.store offset=16 align=4
                    local.get 3
                    i32.const 28
                    i32.add
                    block (result i32)  ;; label = @9
                      i32.const 0
                      local.get 4
                      i32.const 8
                      i32.shr_u
                      local.tee 0
                      i32.eqz
                      br_if 0 (;@9;)
                      drop
                      i32.const 31
                      local.get 4
                      i32.const 16777215
                      i32.gt_u
                      br_if 0 (;@9;)
                      drop
                      local.get 0
                      local.get 0
                      i32.const 1048320
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 8
                      i32.and
                      local.tee 0
                      i32.shl
                      local.tee 1
                      local.get 1
                      i32.const 520192
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 4
                      i32.and
                      local.tee 1
                      i32.shl
                      local.tee 2
                      local.get 2
                      i32.const 245760
                      i32.add
                      i32.const 16
                      i32.shr_u
                      i32.const 2
                      i32.and
                      local.tee 2
                      i32.shl
                      i32.const 15
                      i32.shr_u
                      local.get 0
                      local.get 1
                      i32.or
                      local.get 2
                      i32.or
                      i32.sub
                      local.tee 0
                      i32.const 1
                      i32.shl
                      local.get 4
                      local.get 0
                      i32.const 21
                      i32.add
                      i32.shr_u
                      i32.const 1
                      i32.and
                      i32.or
                      i32.const 28
                      i32.add
                    end
                    local.tee 0
                    i32.store
                    local.get 0
                    i32.const 2
                    i32.shl
                    i32.const 1340
                    i32.add
                    local.set 1
                    block  ;; label = @9
                      i32.const 1040
                      i32.load
                      local.tee 2
                      i32.const 1
                      local.get 0
                      i32.shl
                      local.tee 6
                      i32.and
                      i32.eqz
                      if  ;; label = @10
                        i32.const 1040
                        local.get 2
                        local.get 6
                        i32.or
                        i32.store
                        local.get 1
                        local.get 3
                        i32.store
                        local.get 3
                        i32.const 24
                        i32.add
                        local.get 1
                        i32.store
                        br 1 (;@9;)
                      end
                      local.get 4
                      i32.const 0
                      i32.const 25
                      local.get 0
                      i32.const 1
                      i32.shr_u
                      i32.sub
                      local.get 0
                      i32.const 31
                      i32.eq
                      select
                      i32.shl
                      local.set 0
                      local.get 1
                      i32.load
                      local.set 1
                      loop  ;; label = @10
                        local.get 1
                        local.tee 2
                        i32.load offset=4
                        i32.const -8
                        i32.and
                        local.get 4
                        i32.eq
                        br_if 4 (;@6;)
                        local.get 0
                        i32.const 29
                        i32.shr_u
                        local.set 1
                        local.get 0
                        i32.const 1
                        i32.shl
                        local.set 0
                        local.get 2
                        local.get 1
                        i32.const 4
                        i32.and
                        i32.add
                        i32.const 16
                        i32.add
                        local.tee 6
                        i32.load
                        local.tee 1
                        br_if 0 (;@10;)
                      end
                      local.get 6
                      local.get 3
                      i32.store
                      local.get 3
                      i32.const 24
                      i32.add
                      local.get 2
                      i32.store
                    end
                    local.get 3
                    local.get 3
                    i32.store offset=12
                    local.get 3
                    local.get 3
                    i32.store offset=8
                    br 3 (;@5;)
                  end
                  local.get 2
                  i32.load offset=8
                  local.tee 0
                  local.get 7
                  i32.store offset=12
                  local.get 2
                  local.get 7
                  i32.store offset=8
                  local.get 7
                  i32.const 0
                  i32.store offset=24
                  local.get 7
                  local.get 2
                  i32.store offset=12
                  local.get 7
                  local.get 0
                  i32.store offset=8
                end
                local.get 9
                i32.const 8
                i32.add
                local.set 0
                br 5 (;@1;)
              end
              local.get 2
              i32.load offset=8
              local.tee 0
              local.get 3
              i32.store offset=12
              local.get 2
              local.get 3
              i32.store offset=8
              local.get 3
              i32.const 24
              i32.add
              i32.const 0
              i32.store
              local.get 3
              local.get 2
              i32.store offset=12
              local.get 3
              local.get 0
              i32.store offset=8
            end
            i32.const 1048
            i32.load
            local.tee 0
            local.get 5
            i32.le_u
            br_if 0 (;@4;)
            i32.const 1048
            local.get 0
            local.get 5
            i32.sub
            local.tee 1
            i32.store
            i32.const 1060
            i32.const 1060
            i32.load
            local.tee 0
            local.get 5
            i32.add
            local.tee 2
            i32.store
            local.get 2
            local.get 1
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 0
            local.get 5
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 0
            i32.const 8
            i32.add
            local.set 0
            br 3 (;@1;)
          end
          i32.const 1032
          i32.const 48
          i32.store
          i32.const 0
          local.set 0
          br 2 (;@1;)
        end
        block  ;; label = @3
          local.get 8
          i32.eqz
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 4
            i32.load offset=28
            local.tee 0
            i32.const 2
            i32.shl
            i32.const 1340
            i32.add
            local.tee 3
            i32.load
            local.get 4
            i32.eq
            if  ;; label = @5
              local.get 3
              local.get 1
              i32.store
              local.get 1
              br_if 1 (;@4;)
              i32.const 1040
              local.get 7
              i32.const -2
              local.get 0
              i32.rotl
              i32.and
              local.tee 7
              i32.store
              br 2 (;@3;)
            end
            local.get 8
            i32.const 16
            i32.const 20
            local.get 8
            i32.load offset=16
            local.get 4
            i32.eq
            select
            i32.add
            local.get 1
            i32.store
            local.get 1
            i32.eqz
            br_if 1 (;@3;)
          end
          local.get 1
          local.get 8
          i32.store offset=24
          local.get 4
          i32.load offset=16
          local.tee 0
          if  ;; label = @4
            local.get 1
            local.get 0
            i32.store offset=16
            local.get 0
            local.get 1
            i32.store offset=24
          end
          local.get 4
          i32.const 20
          i32.add
          i32.load
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.const 20
          i32.add
          local.get 0
          i32.store
          local.get 0
          local.get 1
          i32.store offset=24
        end
        block  ;; label = @3
          local.get 2
          i32.const 15
          i32.le_u
          if  ;; label = @4
            local.get 4
            local.get 2
            local.get 5
            i32.add
            local.tee 0
            i32.const 3
            i32.or
            i32.store offset=4
            local.get 0
            local.get 4
            i32.add
            local.tee 0
            local.get 0
            i32.load offset=4
            i32.const 1
            i32.or
            i32.store offset=4
            br 1 (;@3;)
          end
          local.get 4
          local.get 5
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 4
          local.get 5
          i32.add
          local.tee 3
          local.get 2
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 2
          local.get 3
          i32.add
          local.get 2
          i32.store
          local.get 2
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 2
            i32.const 3
            i32.shr_u
            local.tee 1
            i32.const 3
            i32.shl
            i32.const 1076
            i32.add
            local.set 0
            block (result i32)  ;; label = @5
              i32.const 1036
              i32.load
              local.tee 2
              i32.const 1
              local.get 1
              i32.shl
              local.tee 1
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 1036
                local.get 1
                local.get 2
                i32.or
                i32.store
                local.get 0
                br 1 (;@5;)
              end
              local.get 0
              i32.load offset=8
            end
            local.set 1
            local.get 0
            local.get 3
            i32.store offset=8
            local.get 1
            local.get 3
            i32.store offset=12
            local.get 3
            local.get 0
            i32.store offset=12
            local.get 3
            local.get 1
            i32.store offset=8
            br 1 (;@3;)
          end
          local.get 3
          block (result i32)  ;; label = @4
            i32.const 0
            local.get 2
            i32.const 8
            i32.shr_u
            local.tee 0
            i32.eqz
            br_if 0 (;@4;)
            drop
            i32.const 31
            local.get 2
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            drop
            local.get 0
            local.get 0
            i32.const 1048320
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 8
            i32.and
            local.tee 0
            i32.shl
            local.tee 1
            local.get 1
            i32.const 520192
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 4
            i32.and
            local.tee 1
            i32.shl
            local.tee 5
            local.get 5
            i32.const 245760
            i32.add
            i32.const 16
            i32.shr_u
            i32.const 2
            i32.and
            local.tee 5
            i32.shl
            i32.const 15
            i32.shr_u
            local.get 0
            local.get 1
            i32.or
            local.get 5
            i32.or
            i32.sub
            local.tee 0
            i32.const 1
            i32.shl
            local.get 2
            local.get 0
            i32.const 21
            i32.add
            i32.shr_u
            i32.const 1
            i32.and
            i32.or
            i32.const 28
            i32.add
          end
          local.tee 0
          i32.store offset=28
          local.get 3
          i64.const 0
          i64.store offset=16 align=4
          local.get 0
          i32.const 2
          i32.shl
          i32.const 1340
          i32.add
          local.set 1
          block  ;; label = @4
            block  ;; label = @5
              local.get 7
              i32.const 1
              local.get 0
              i32.shl
              local.tee 5
              i32.and
              i32.eqz
              if  ;; label = @6
                i32.const 1040
                local.get 5
                local.get 7
                i32.or
                i32.store
                local.get 1
                local.get 3
                i32.store
                br 1 (;@5;)
              end
              local.get 2
              i32.const 0
              i32.const 25
              local.get 0
              i32.const 1
              i32.shr_u
              i32.sub
              local.get 0
              i32.const 31
              i32.eq
              select
              i32.shl
              local.set 0
              local.get 1
              i32.load
              local.set 5
              loop  ;; label = @6
                local.get 5
                local.tee 1
                i32.load offset=4
                i32.const -8
                i32.and
                local.get 2
                i32.eq
                br_if 2 (;@4;)
                local.get 0
                i32.const 29
                i32.shr_u
                local.set 5
                local.get 0
                i32.const 1
                i32.shl
                local.set 0
                local.get 1
                local.get 5
                i32.const 4
                i32.and
                i32.add
                i32.const 16
                i32.add
                local.tee 6
                i32.load
                local.tee 5
                br_if 0 (;@6;)
              end
              local.get 6
              local.get 3
              i32.store
            end
            local.get 3
            local.get 1
            i32.store offset=24
            local.get 3
            local.get 3
            i32.store offset=12
            local.get 3
            local.get 3
            i32.store offset=8
            br 1 (;@3;)
          end
          local.get 1
          i32.load offset=8
          local.tee 0
          local.get 3
          i32.store offset=12
          local.get 1
          local.get 3
          i32.store offset=8
          local.get 3
          i32.const 0
          i32.store offset=24
          local.get 3
          local.get 1
          i32.store offset=12
          local.get 3
          local.get 0
          i32.store offset=8
        end
        local.get 4
        i32.const 8
        i32.add
        local.set 0
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 9
        i32.eqz
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 1
          i32.load offset=28
          local.tee 0
          i32.const 2
          i32.shl
          i32.const 1340
          i32.add
          local.tee 2
          i32.load
          local.get 1
          i32.eq
          if  ;; label = @4
            local.get 2
            local.get 4
            i32.store
            local.get 4
            br_if 1 (;@3;)
            i32.const 1040
            local.get 10
            i32.const -2
            local.get 0
            i32.rotl
            i32.and
            i32.store
            br 2 (;@2;)
          end
          local.get 9
          i32.const 16
          i32.const 20
          local.get 9
          i32.load offset=16
          local.get 1
          i32.eq
          select
          i32.add
          local.get 4
          i32.store
          local.get 4
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 4
        local.get 9
        i32.store offset=24
        local.get 1
        i32.load offset=16
        local.tee 0
        if  ;; label = @3
          local.get 4
          local.get 0
          i32.store offset=16
          local.get 0
          local.get 4
          i32.store offset=24
        end
        local.get 1
        i32.const 20
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 0 (;@2;)
        local.get 4
        i32.const 20
        i32.add
        local.get 0
        i32.store
        local.get 0
        local.get 4
        i32.store offset=24
      end
      block  ;; label = @2
        local.get 3
        i32.const 15
        i32.le_u
        if  ;; label = @3
          local.get 1
          local.get 3
          local.get 5
          i32.add
          local.tee 0
          i32.const 3
          i32.or
          i32.store offset=4
          local.get 0
          local.get 1
          i32.add
          local.tee 0
          local.get 0
          i32.load offset=4
          i32.const 1
          i32.or
          i32.store offset=4
          br 1 (;@2;)
        end
        local.get 1
        local.get 5
        i32.const 3
        i32.or
        i32.store offset=4
        local.get 1
        local.get 5
        i32.add
        local.tee 4
        local.get 3
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 3
        local.get 4
        i32.add
        local.get 3
        i32.store
        local.get 8
        if  ;; label = @3
          local.get 8
          i32.const 3
          i32.shr_u
          local.tee 5
          i32.const 3
          i32.shl
          i32.const 1076
          i32.add
          local.set 0
          i32.const 1056
          i32.load
          local.set 2
          block (result i32)  ;; label = @4
            i32.const 1
            local.get 5
            i32.shl
            local.tee 5
            local.get 6
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 1036
              local.get 5
              local.get 6
              i32.or
              i32.store
              local.get 0
              br 1 (;@4;)
            end
            local.get 0
            i32.load offset=8
          end
          local.set 5
          local.get 0
          local.get 2
          i32.store offset=8
          local.get 5
          local.get 2
          i32.store offset=12
          local.get 2
          local.get 0
          i32.store offset=12
          local.get 2
          local.get 5
          i32.store offset=8
        end
        i32.const 1056
        local.get 4
        i32.store
        i32.const 1044
        local.get 3
        i32.store
      end
      local.get 1
      i32.const 8
      i32.add
      local.set 0
    end
    local.get 11
    i32.const 16
    i32.add
    local.tee 1
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 1
    global.set 0
    local.get 0)
  (func (;15;) (type 1) (param i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const -8
      i32.add
      local.tee 3
      local.get 0
      i32.const -4
      i32.add
      i32.load
      local.tee 1
      i32.const -8
      i32.and
      local.tee 0
      i32.add
      local.set 5
      block  ;; label = @2
        local.get 1
        i32.const 1
        i32.and
        br_if 0 (;@2;)
        local.get 1
        i32.const 3
        i32.and
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        local.get 3
        i32.load
        local.tee 2
        i32.sub
        local.tee 3
        i32.const 1052
        i32.load
        local.tee 4
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 2
        i32.add
        local.set 0
        local.get 3
        i32.const 1056
        i32.load
        i32.ne
        if  ;; label = @3
          local.get 2
          i32.const 255
          i32.le_u
          if  ;; label = @4
            local.get 3
            i32.load offset=8
            local.tee 4
            local.get 2
            i32.const 3
            i32.shr_u
            local.tee 2
            i32.const 3
            i32.shl
            i32.const 1076
            i32.add
            i32.ne
            drop
            local.get 4
            local.get 3
            i32.load offset=12
            local.tee 1
            i32.eq
            if  ;; label = @5
              i32.const 1036
              i32.const 1036
              i32.load
              i32.const -2
              local.get 2
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            local.get 4
            local.get 1
            i32.store offset=12
            local.get 1
            local.get 4
            i32.store offset=8
            br 2 (;@2;)
          end
          local.get 3
          i32.load offset=24
          local.set 6
          block  ;; label = @4
            local.get 3
            local.get 3
            i32.load offset=12
            local.tee 1
            i32.ne
            if  ;; label = @5
              local.get 4
              local.get 3
              i32.load offset=8
              local.tee 2
              i32.le_u
              if  ;; label = @6
                local.get 2
                i32.load offset=12
                drop
              end
              local.get 2
              local.get 1
              i32.store offset=12
              local.get 1
              local.get 2
              i32.store offset=8
              br 1 (;@4;)
            end
            block  ;; label = @5
              local.get 3
              i32.const 20
              i32.add
              local.tee 2
              i32.load
              local.tee 4
              br_if 0 (;@5;)
              local.get 3
              i32.const 16
              i32.add
              local.tee 2
              i32.load
              local.tee 4
              br_if 0 (;@5;)
              i32.const 0
              local.set 1
              br 1 (;@4;)
            end
            loop  ;; label = @5
              local.get 2
              local.set 7
              local.get 4
              local.tee 1
              i32.const 20
              i32.add
              local.tee 2
              i32.load
              local.tee 4
              br_if 0 (;@5;)
              local.get 1
              i32.const 16
              i32.add
              local.set 2
              local.get 1
              i32.load offset=16
              local.tee 4
              br_if 0 (;@5;)
            end
            local.get 7
            i32.const 0
            i32.store
          end
          local.get 6
          i32.eqz
          br_if 1 (;@2;)
          block  ;; label = @4
            local.get 3
            local.get 3
            i32.load offset=28
            local.tee 2
            i32.const 2
            i32.shl
            i32.const 1340
            i32.add
            local.tee 4
            i32.load
            i32.eq
            if  ;; label = @5
              local.get 4
              local.get 1
              i32.store
              local.get 1
              br_if 1 (;@4;)
              i32.const 1040
              i32.const 1040
              i32.load
              i32.const -2
              local.get 2
              i32.rotl
              i32.and
              i32.store
              br 3 (;@2;)
            end
            local.get 6
            i32.const 16
            i32.const 20
            local.get 6
            i32.load offset=16
            local.get 3
            i32.eq
            select
            i32.add
            local.get 1
            i32.store
            local.get 1
            i32.eqz
            br_if 2 (;@2;)
          end
          local.get 1
          local.get 6
          i32.store offset=24
          local.get 3
          i32.load offset=16
          local.tee 2
          if  ;; label = @4
            local.get 1
            local.get 2
            i32.store offset=16
            local.get 2
            local.get 1
            i32.store offset=24
          end
          local.get 3
          i32.load offset=20
          local.tee 2
          i32.eqz
          br_if 1 (;@2;)
          local.get 1
          i32.const 20
          i32.add
          local.get 2
          i32.store
          local.get 2
          local.get 1
          i32.store offset=24
          br 1 (;@2;)
        end
        local.get 5
        i32.load offset=4
        local.tee 1
        i32.const 3
        i32.and
        i32.const 3
        i32.ne
        br_if 0 (;@2;)
        i32.const 1044
        local.get 0
        i32.store
        local.get 5
        local.get 1
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 3
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 3
        i32.add
        local.get 0
        i32.store
        return
      end
      local.get 5
      local.get 3
      i32.le_u
      br_if 0 (;@1;)
      local.get 5
      i32.load offset=4
      local.tee 1
      i32.const 1
      i32.and
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 1
        i32.const 2
        i32.and
        i32.eqz
        if  ;; label = @3
          local.get 5
          i32.const 1060
          i32.load
          i32.eq
          if  ;; label = @4
            i32.const 1060
            local.get 3
            i32.store
            i32.const 1048
            i32.const 1048
            i32.load
            local.get 0
            i32.add
            local.tee 0
            i32.store
            local.get 3
            local.get 0
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 3
            i32.const 1056
            i32.load
            i32.ne
            br_if 3 (;@1;)
            i32.const 1044
            i32.const 0
            i32.store
            i32.const 1056
            i32.const 0
            i32.store
            return
          end
          local.get 5
          i32.const 1056
          i32.load
          i32.eq
          if  ;; label = @4
            i32.const 1056
            local.get 3
            i32.store
            i32.const 1044
            i32.const 1044
            i32.load
            local.get 0
            i32.add
            local.tee 0
            i32.store
            local.get 3
            local.get 0
            i32.const 1
            i32.or
            i32.store offset=4
            local.get 0
            local.get 3
            i32.add
            local.get 0
            i32.store
            return
          end
          local.get 1
          i32.const -8
          i32.and
          local.get 0
          i32.add
          local.set 0
          block  ;; label = @4
            local.get 1
            i32.const 255
            i32.le_u
            if  ;; label = @5
              local.get 5
              i32.load offset=12
              local.set 2
              local.get 5
              i32.load offset=8
              local.tee 4
              local.get 1
              i32.const 3
              i32.shr_u
              local.tee 1
              i32.const 3
              i32.shl
              i32.const 1076
              i32.add
              local.tee 7
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                drop
              end
              local.get 2
              local.get 4
              i32.eq
              if  ;; label = @6
                i32.const 1036
                i32.const 1036
                i32.load
                i32.const -2
                local.get 1
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              local.get 2
              local.get 7
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                drop
              end
              local.get 4
              local.get 2
              i32.store offset=12
              local.get 2
              local.get 4
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 5
            i32.load offset=24
            local.set 6
            block  ;; label = @5
              local.get 5
              local.get 5
              i32.load offset=12
              local.tee 1
              i32.ne
              if  ;; label = @6
                i32.const 1052
                i32.load
                local.get 5
                i32.load offset=8
                local.tee 2
                i32.le_u
                if  ;; label = @7
                  local.get 2
                  i32.load offset=12
                  drop
                end
                local.get 2
                local.get 1
                i32.store offset=12
                local.get 1
                local.get 2
                i32.store offset=8
                br 1 (;@5;)
              end
              block  ;; label = @6
                local.get 5
                i32.const 20
                i32.add
                local.tee 2
                i32.load
                local.tee 4
                br_if 0 (;@6;)
                local.get 5
                i32.const 16
                i32.add
                local.tee 2
                i32.load
                local.tee 4
                br_if 0 (;@6;)
                i32.const 0
                local.set 1
                br 1 (;@5;)
              end
              loop  ;; label = @6
                local.get 2
                local.set 7
                local.get 4
                local.tee 1
                i32.const 20
                i32.add
                local.tee 2
                i32.load
                local.tee 4
                br_if 0 (;@6;)
                local.get 1
                i32.const 16
                i32.add
                local.set 2
                local.get 1
                i32.load offset=16
                local.tee 4
                br_if 0 (;@6;)
              end
              local.get 7
              i32.const 0
              i32.store
            end
            local.get 6
            i32.eqz
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 5
              local.get 5
              i32.load offset=28
              local.tee 2
              i32.const 2
              i32.shl
              i32.const 1340
              i32.add
              local.tee 4
              i32.load
              i32.eq
              if  ;; label = @6
                local.get 4
                local.get 1
                i32.store
                local.get 1
                br_if 1 (;@5;)
                i32.const 1040
                i32.const 1040
                i32.load
                i32.const -2
                local.get 2
                i32.rotl
                i32.and
                i32.store
                br 2 (;@4;)
              end
              local.get 6
              i32.const 16
              i32.const 20
              local.get 6
              i32.load offset=16
              local.get 5
              i32.eq
              select
              i32.add
              local.get 1
              i32.store
              local.get 1
              i32.eqz
              br_if 1 (;@4;)
            end
            local.get 1
            local.get 6
            i32.store offset=24
            local.get 5
            i32.load offset=16
            local.tee 2
            if  ;; label = @5
              local.get 1
              local.get 2
              i32.store offset=16
              local.get 2
              local.get 1
              i32.store offset=24
            end
            local.get 5
            i32.load offset=20
            local.tee 2
            i32.eqz
            br_if 0 (;@4;)
            local.get 1
            i32.const 20
            i32.add
            local.get 2
            i32.store
            local.get 2
            local.get 1
            i32.store offset=24
          end
          local.get 3
          local.get 0
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          local.get 3
          i32.add
          local.get 0
          i32.store
          local.get 3
          i32.const 1056
          i32.load
          i32.ne
          br_if 1 (;@2;)
          i32.const 1044
          local.get 0
          i32.store
          return
        end
        local.get 5
        local.get 1
        i32.const -2
        i32.and
        i32.store offset=4
        local.get 3
        local.get 0
        i32.const 1
        i32.or
        i32.store offset=4
        local.get 0
        local.get 3
        i32.add
        local.get 0
        i32.store
      end
      local.get 0
      i32.const 255
      i32.le_u
      if  ;; label = @2
        local.get 0
        i32.const 3
        i32.shr_u
        local.tee 1
        i32.const 3
        i32.shl
        i32.const 1076
        i32.add
        local.set 0
        block (result i32)  ;; label = @3
          i32.const 1036
          i32.load
          local.tee 2
          i32.const 1
          local.get 1
          i32.shl
          local.tee 1
          i32.and
          i32.eqz
          if  ;; label = @4
            i32.const 1036
            local.get 1
            local.get 2
            i32.or
            i32.store
            local.get 0
            br 1 (;@3;)
          end
          local.get 0
          i32.load offset=8
        end
        local.set 2
        local.get 0
        local.get 3
        i32.store offset=8
        local.get 2
        local.get 3
        i32.store offset=12
        local.get 3
        local.get 0
        i32.store offset=12
        local.get 3
        local.get 2
        i32.store offset=8
        return
      end
      local.get 3
      i64.const 0
      i64.store offset=16 align=4
      local.get 3
      i32.const 28
      i32.add
      block (result i32)  ;; label = @2
        i32.const 0
        local.get 0
        i32.const 8
        i32.shr_u
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        drop
        i32.const 31
        local.get 0
        i32.const 16777215
        i32.gt_u
        br_if 0 (;@2;)
        drop
        local.get 1
        local.get 1
        i32.const 1048320
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 8
        i32.and
        local.tee 1
        i32.shl
        local.tee 2
        local.get 2
        i32.const 520192
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 4
        i32.and
        local.tee 2
        i32.shl
        local.tee 4
        local.get 4
        i32.const 245760
        i32.add
        i32.const 16
        i32.shr_u
        i32.const 2
        i32.and
        local.tee 4
        i32.shl
        i32.const 15
        i32.shr_u
        local.get 1
        local.get 2
        i32.or
        local.get 4
        i32.or
        i32.sub
        local.tee 1
        i32.const 1
        i32.shl
        local.get 0
        local.get 1
        i32.const 21
        i32.add
        i32.shr_u
        i32.const 1
        i32.and
        i32.or
        i32.const 28
        i32.add
      end
      local.tee 2
      i32.store
      local.get 2
      i32.const 2
      i32.shl
      i32.const 1340
      i32.add
      local.set 1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            i32.const 1040
            i32.load
            local.tee 4
            i32.const 1
            local.get 2
            i32.shl
            local.tee 7
            i32.and
            i32.eqz
            if  ;; label = @5
              i32.const 1040
              local.get 4
              local.get 7
              i32.or
              i32.store
              local.get 1
              local.get 3
              i32.store
              local.get 3
              i32.const 24
              i32.add
              local.get 1
              i32.store
              br 1 (;@4;)
            end
            local.get 0
            i32.const 0
            i32.const 25
            local.get 2
            i32.const 1
            i32.shr_u
            i32.sub
            local.get 2
            i32.const 31
            i32.eq
            select
            i32.shl
            local.set 2
            local.get 1
            i32.load
            local.set 1
            loop  ;; label = @5
              local.get 1
              local.tee 4
              i32.load offset=4
              i32.const -8
              i32.and
              local.get 0
              i32.eq
              br_if 2 (;@3;)
              local.get 2
              i32.const 29
              i32.shr_u
              local.set 1
              local.get 2
              i32.const 1
              i32.shl
              local.set 2
              local.get 4
              local.get 1
              i32.const 4
              i32.and
              i32.add
              i32.const 16
              i32.add
              local.tee 7
              i32.load
              local.tee 1
              br_if 0 (;@5;)
            end
            local.get 7
            local.get 3
            i32.store
            local.get 3
            i32.const 24
            i32.add
            local.get 4
            i32.store
          end
          local.get 3
          local.get 3
          i32.store offset=12
          local.get 3
          local.get 3
          i32.store offset=8
          br 1 (;@2;)
        end
        local.get 4
        i32.load offset=8
        local.tee 0
        local.get 3
        i32.store offset=12
        local.get 4
        local.get 3
        i32.store offset=8
        local.get 3
        i32.const 24
        i32.add
        i32.const 0
        i32.store
        local.get 3
        local.get 4
        i32.store offset=12
        local.get 3
        local.get 0
        i32.store offset=8
      end
      i32.const 1068
      i32.const 1068
      i32.load
      i32.const -1
      i32.add
      local.tee 0
      i32.store
      local.get 0
      br_if 0 (;@1;)
      i32.const 1492
      local.set 3
      loop  ;; label = @2
        local.get 3
        i32.load
        local.tee 0
        i32.const 8
        i32.add
        local.set 3
        local.get 0
        br_if 0 (;@2;)
      end
      i32.const 1068
      i32.const -1
      i32.store
    end)
  (func (;16;) (type 4) (result i32)
    global.get 0)
  (func (;17;) (type 1) (param i32)
    local.get 0
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 0
    global.set 0)
  (func (;18;) (type 0) (param i32) (result i32)
    (local i32)
    global.get 0
    local.get 0
    i32.sub
    i32.const -16
    i32.and
    local.tee 0
    local.tee 1
    global.get 2
    i32.lt_u
    if  ;; label = @1
      call 1
    end
    local.get 1
    global.set 0
    local.get 0)
  (func (;19;) (type 0) (param i32) (result i32)
    (local i32)
    local.get 0
    if  ;; label = @1
      local.get 0
      i32.load offset=76
      i32.const -1
      i32.le_s
      if  ;; label = @2
        local.get 0
        call 20
        return
      end
      local.get 0
      call 20
      return
    end
    i32.const 1544
    i32.load
    if  ;; label = @1
      i32.const 1544
      i32.load
      call 19
      local.set 1
    end
    i32.const 1540
    i32.load
    local.tee 0
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        i32.load offset=76
        i32.const 0
        i32.ge_s
        if (result i32)  ;; label = @3
          i32.const 1
        else
          i32.const 0
        end
        drop
        local.get 0
        i32.load offset=20
        local.get 0
        i32.load offset=28
        i32.gt_u
        if  ;; label = @3
          local.get 0
          call 20
          local.get 1
          i32.or
          local.set 1
        end
        local.get 0
        i32.load offset=56
        local.tee 0
        br_if 0 (;@2;)
      end
    end
    local.get 1)
  (func (;20;) (type 0) (param i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=20
      local.get 0
      i32.load offset=28
      i32.le_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 0
      i32.const 0
      local.get 0
      i32.load offset=36
      call_indirect (type 9)
      drop
      local.get 0
      i32.load offset=20
      br_if 0 (;@1;)
      i32.const -1
      return
    end
    local.get 0
    i32.load offset=4
    local.tee 1
    local.get 0
    i32.load offset=8
    local.tee 2
    i32.lt_u
    if  ;; label = @1
      local.get 0
      local.get 1
      local.get 2
      i32.sub
      i64.extend_i32_s
      i32.const 1
      local.get 0
      i32.load offset=40
      call_indirect (type 11)
      drop
    end
    local.get 0
    i32.const 0
    i32.store offset=28
    local.get 0
    i64.const 0
    i64.store offset=16
    local.get 0
    i64.const 0
    i64.store offset=4 align=4
    i32.const 0)
  (func (;21;) (type 1) (param i32)
    local.get 0
    global.set 2)
  (func (;22;) (type 0) (param i32) (result i32)
    local.get 0
    memory.grow)
  (global (;0;) (mut i32) (i32.const 5244592))
  (global (;1;) i32 (i32.const 1548))
  (global (;2;) (mut i32) (i32.const 0))
  (export "__wasm_call_ctors" (func 2))
  (export "grayScale" (func 4))
  (export "brighten" (func 5))
  (export "invert" (func 6))
  (export "noise" (func 7))
  (export "multiFilter" (func 8))
  (export "sobelFilter" (func 9))
  (export "convFilter" (func 10))
  (export "fflush" (func 19))
  (export "__errno_location" (func 12))
  (export "stackSave" (func 16))
  (export "stackRestore" (func 17))
  (export "stackAlloc" (func 18))
  (export "malloc" (func 14))
  (export "free" (func 15))
  (export "__data_end" (global 1))
  (export "__set_stack_limit" (func 21))
  (export "__growWasmMemory" (func 22)))
