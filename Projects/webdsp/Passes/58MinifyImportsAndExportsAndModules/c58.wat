(module
  (type (;0;) (func (param i32 i32)))
  (type (;1;) (func))
  (type (;2;) (func (param i32 i32 i32)))
  (type (;3;) (func (param i32 i32 i32 i32)))
  (type (;4;) (func (param i32 i32 i32 i32 i32 i32 i32)))
  (type (;5;) (func (param i32 i32 i32 i32 i32 i32 f32 f32 i32)))
  (import "a" "memory" (memory (;0;) 256 32768))
  (func (;0;) (type 5) (param i32 i32 i32 i32 i32 i32 f32 f32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f32 f32 f32 f64)
    local.get 4
    i32.const 2
    i32.div_s
    local.set 9
    local.get 5
    i32.const 2
    i32.div_s
    local.set 10
    local.get 8
    i32.const 1
    i32.ge_s
    if  ;; label = @1
      local.get 1
      local.get 9
      i32.sub
      local.set 14
      local.get 2
      local.get 10
      i32.sub
      local.set 15
      local.get 5
      i32.const 1
      i32.lt_s
      local.set 18
      loop  ;; label = @2
        local.get 10
        local.set 11
        local.get 10
        local.get 15
        i32.lt_s
        if  ;; label = @3
          loop  ;; label = @4
            local.get 9
            local.get 14
            i32.lt_s
            if  ;; label = @5
              local.get 1
              local.get 11
              i32.mul
              local.set 19
              local.get 11
              local.get 10
              i32.sub
              local.set 20
              local.get 9
              local.set 12
              loop  ;; label = @6
                block  ;; label = @7
                  local.get 18
                  if  ;; label = @8
                    f32.const 0x0p+0 (;=0;)
                    local.set 7
                    f32.const 0x0p+0 (;=0;)
                    local.set 24
                    f32.const 0x0p+0 (;=0;)
                    local.set 25
                    br 1 (;@7;)
                  end
                  f32.const 0x0p+0 (;=0;)
                  local.set 25
                  local.get 4
                  i32.const 1
                  i32.lt_s
                  if  ;; label = @8
                    f32.const 0x0p+0 (;=0;)
                    local.set 7
                    f32.const 0x0p+0 (;=0;)
                    local.set 24
                    br 1 (;@7;)
                  end
                  local.get 12
                  local.get 9
                  i32.sub
                  local.set 21
                  i32.const 0
                  local.set 13
                  f32.const 0x0p+0 (;=0;)
                  local.set 24
                  f32.const 0x0p+0 (;=0;)
                  local.set 7
                  loop  ;; label = @8
                    local.get 4
                    local.get 13
                    i32.mul
                    local.set 22
                    local.get 21
                    local.get 13
                    local.get 20
                    i32.add
                    local.get 1
                    i32.mul
                    i32.add
                    local.set 23
                    i32.const 0
                    local.set 2
                    loop  ;; label = @9
                      local.get 25
                      local.get 0
                      local.get 2
                      local.get 23
                      i32.add
                      i32.const 4
                      i32.shl
                      local.tee 17
                      i32.add
                      f32.load
                      local.get 3
                      local.get 2
                      local.get 22
                      i32.add
                      i32.const 2
                      i32.shl
                      i32.add
                      f32.load
                      local.tee 26
                      f32.mul
                      f32.add
                      local.set 25
                      local.get 7
                      local.get 26
                      local.get 0
                      local.get 17
                      i32.const 8
                      i32.or
                      i32.add
                      f32.load
                      f32.mul
                      f32.add
                      local.set 7
                      local.get 24
                      local.get 26
                      local.get 0
                      local.get 17
                      i32.const 4
                      i32.or
                      i32.add
                      f32.load
                      f32.mul
                      f32.add
                      local.set 24
                      local.get 2
                      i32.const 1
                      i32.add
                      local.tee 2
                      local.get 4
                      i32.ne
                      br_if 0 (;@9;)
                    end
                    local.get 13
                    i32.const 1
                    i32.add
                    local.tee 13
                    local.get 5
                    i32.ne
                    br_if 0 (;@8;)
                  end
                end
                f64.const 0x1.fep+7 (;=255;)
                local.set 27
                local.get 0
                local.get 12
                local.get 19
                i32.add
                i32.const 4
                i32.shl
                local.tee 2
                i32.add
                block (result f64)  ;; label = @7
                  f64.const 0x1.fep+7 (;=255;)
                  local.get 25
                  local.get 6
                  f32.div
                  local.tee 25
                  f32.const 0x1.fep+7 (;=255;)
                  f32.gt
                  br_if 0 (;@7;)
                  drop
                  f64.const 0x0p+0 (;=0;)
                  local.get 25
                  f32.const 0x0p+0 (;=0;)
                  f32.lt
                  br_if 0 (;@7;)
                  drop
                  local.get 25
                  f64.promote_f32
                end
                f32.demote_f64
                f32.store
                block  ;; label = @7
                  local.get 24
                  local.get 6
                  f32.div
                  local.tee 24
                  f32.const 0x1.fep+7 (;=255;)
                  f32.gt
                  br_if 0 (;@7;)
                  f64.const 0x0p+0 (;=0;)
                  local.set 27
                  local.get 24
                  f32.const 0x0p+0 (;=0;)
                  f32.lt
                  br_if 0 (;@7;)
                  local.get 24
                  f64.promote_f32
                  local.set 27
                end
                local.get 0
                local.get 2
                i32.const 4
                i32.or
                i32.add
                local.get 27
                f32.demote_f64
                f32.store
                local.get 0
                local.get 2
                i32.const 8
                i32.or
                i32.add
                block (result f64)  ;; label = @7
                  f64.const 0x1.fep+7 (;=255;)
                  local.get 7
                  local.get 6
                  f32.div
                  local.tee 7
                  f32.const 0x1.fep+7 (;=255;)
                  f32.gt
                  br_if 0 (;@7;)
                  drop
                  f64.const 0x0p+0 (;=0;)
                  local.get 7
                  f32.const 0x0p+0 (;=0;)
                  f32.lt
                  br_if 0 (;@7;)
                  drop
                  local.get 7
                  f64.promote_f32
                end
                f32.demote_f64
                f32.store
                local.get 12
                i32.const 1
                i32.add
                local.tee 12
                local.get 14
                i32.ne
                br_if 0 (;@6;)
              end
            end
            local.get 11
            i32.const 1
            i32.add
            local.tee 11
            local.get 15
            i32.ne
            br_if 0 (;@4;)
          end
        end
        local.get 16
        i32.const 1
        i32.add
        local.tee 16
        local.get 8
        i32.ne
        br_if 0 (;@2;)
      end
    end)
  (func (;1;) (type 3) (param i32 i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f64)
    global.get 0
    local.get 1
    local.get 2
    i32.mul
    i32.const 2
    i32.shl
    i32.const 15
    i32.add
    i32.const -16
    i32.and
    i32.sub
    local.set 7
    block  ;; label = @1
      local.get 2
      i32.const 1
      i32.lt_s
      br_if 0 (;@1;)
      local.get 1
      i32.const 1
      i32.ge_s
      if  ;; label = @2
        loop  ;; label = @3
          local.get 1
          local.get 5
          i32.mul
          local.set 10
          i32.const 0
          local.set 4
          loop  ;; label = @4
            local.get 7
            local.get 4
            local.get 10
            i32.add
            i32.const 2
            i32.shl
            local.tee 8
            i32.add
            local.get 0
            local.get 8
            i32.const 1
            i32.or
            i32.add
            local.tee 6
            i32.load8_u
            i32.const 1
            i32.shr_u
            local.get 0
            local.get 8
            i32.add
            local.tee 11
            i32.load8_u
            i32.const 2
            i32.shr_u
            i32.add
            local.get 0
            local.get 8
            i32.const 2
            i32.or
            i32.add
            local.tee 12
            i32.load8_u
            i32.const 3
            i32.shr_u
            i32.add
            local.tee 9
            i32.store
            local.get 11
            local.get 9
            i32.store8
            local.get 6
            local.get 9
            i32.store8
            local.get 12
            local.get 9
            i32.store8
            local.get 0
            local.get 8
            i32.const 3
            i32.or
            i32.add
            i32.const 255
            i32.store8
            local.get 4
            i32.const 1
            i32.add
            local.tee 4
            local.get 1
            i32.ne
            br_if 0 (;@4;)
          end
          local.get 5
          i32.const 1
          i32.add
          local.tee 5
          local.get 2
          i32.ne
          br_if 0 (;@3;)
        end
        local.get 2
        i32.const 1
        i32.lt_s
        br_if 1 (;@1;)
      end
      i32.const 0
      local.get 3
      i32.sub
      local.set 16
      local.get 2
      i32.const -1
      i32.add
      local.set 24
      local.get 1
      i32.const -1
      i32.add
      local.set 25
      i32.const 1
      local.set 17
      i32.const 0
      local.set 8
      loop  ;; label = @2
        local.get 1
        i32.const 0
        i32.le_s
        if (result i32)  ;; label = @3
          local.get 8
          i32.const 1
          i32.add
        else
          local.get 0
          local.get 1
          local.get 8
          i32.mul
          local.tee 18
          i32.const 2
          i32.shl
          local.tee 9
          i32.add
          local.get 16
          i32.store8
          i32.const 1
          local.set 10
          local.get 0
          local.get 9
          i32.const 1
          i32.or
          i32.add
          local.get 16
          i32.store8
          local.get 0
          local.get 9
          i32.const 2
          i32.or
          i32.add
          local.get 16
          i32.store8
          local.get 0
          local.get 9
          i32.const 3
          i32.or
          i32.add
          i32.const 255
          i32.store8
          local.get 8
          i32.const 1
          i32.add
          local.set 9
          local.get 1
          i32.const 1
          i32.ne
          if  ;; label = @4
            local.get 1
            local.get 9
            i32.mul
            local.set 11
            local.get 8
            i32.const -1
            i32.add
            local.tee 26
            local.get 1
            i32.mul
            local.set 12
            loop  ;; label = @5
              i32.const 0
              local.set 6
              block (result i32)  ;; label = @6
                block (result i32)  ;; label = @7
                  i32.const 0
                  local.get 8
                  local.get 24
                  i32.ge_s
                  br_if 0 (;@7;)
                  drop
                  i32.const 0
                  local.get 8
                  i32.eqz
                  br_if 0 (;@7;)
                  drop
                  i32.const 0
                  local.get 10
                  local.get 25
                  i32.ge_s
                  br_if 0 (;@7;)
                  drop
                  i32.const 0
                  local.set 5
                  local.get 10
                  i32.const -1
                  i32.add
                  local.tee 4
                  local.get 26
                  i32.or
                  local.tee 15
                  i32.const 0
                  i32.ge_s
                  if  ;; label = @8
                    local.get 7
                    local.get 4
                    local.get 12
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 5
                  end
                  local.get 10
                  i32.const 1
                  i32.add
                  local.tee 13
                  local.get 1
                  i32.ge_s
                  local.tee 14
                  i32.eqz
                  if  ;; label = @8
                    local.get 7
                    local.get 12
                    local.get 13
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 6
                  end
                  i32.const 0
                  local.set 19
                  i32.const 0
                  local.set 22
                  block (result i32)  ;; label = @8
                    i32.const 0
                    local.get 17
                    i32.eqz
                    br_if 0 (;@8;)
                    drop
                    local.get 7
                    local.get 4
                    local.get 18
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 22
                    i32.const 0
                    local.get 17
                    i32.const -1
                    i32.xor
                    local.get 14
                    i32.or
                    i32.const 1
                    i32.and
                    br_if 0 (;@8;)
                    drop
                    local.get 7
                    local.get 13
                    local.get 18
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                  end
                  local.set 27
                  i32.const 0
                  local.set 23
                  block (result i32)  ;; label = @8
                    i32.const 0
                    local.get 9
                    local.get 2
                    i32.ge_s
                    local.tee 20
                    br_if 0 (;@8;)
                    drop
                    local.get 7
                    local.get 4
                    local.get 11
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 23
                    i32.const 0
                    local.get 20
                    br_if 0 (;@8;)
                    drop
                    i32.const 0
                    local.get 14
                    br_if 0 (;@8;)
                    drop
                    i32.const 1
                    local.set 19
                    local.get 7
                    local.get 11
                    local.get 13
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                  end
                  local.set 21
                  local.get 6
                  local.get 5
                  i32.sub
                  local.set 20
                  i32.const 0
                  local.set 6
                  i32.const 0
                  local.set 5
                  local.get 15
                  i32.const 0
                  i32.ge_s
                  if  ;; label = @8
                    local.get 7
                    local.get 4
                    local.get 12
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 5
                  end
                  local.get 7
                  local.get 10
                  local.get 12
                  i32.add
                  i32.const 2
                  i32.shl
                  i32.add
                  i32.load
                  local.set 15
                  local.get 14
                  i32.eqz
                  if  ;; label = @8
                    local.get 7
                    local.get 12
                    local.get 13
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.set 6
                  end
                  local.get 27
                  local.get 22
                  i32.sub
                  i32.const 1
                  i32.shl
                  local.set 14
                  local.get 20
                  local.get 23
                  i32.sub
                  local.get 21
                  i32.add
                  local.set 21
                  block (result i32)  ;; label = @8
                    local.get 9
                    local.get 2
                    i32.ge_s
                    if  ;; label = @9
                      i32.const 0
                      local.get 15
                      i32.const 1
                      i32.shl
                      local.get 5
                      i32.add
                      local.get 6
                      i32.add
                      i32.sub
                      local.set 5
                      i32.const 0
                      br 1 (;@8;)
                    end
                    local.get 7
                    local.get 4
                    local.get 11
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    local.get 15
                    i32.const 1
                    i32.shl
                    local.get 5
                    i32.add
                    local.get 6
                    i32.add
                    i32.sub
                    local.get 7
                    local.get 10
                    local.get 11
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                    i32.const 1
                    i32.shl
                    i32.add
                    local.set 5
                    i32.const 0
                    local.get 19
                    i32.eqz
                    br_if 0 (;@8;)
                    drop
                    local.get 7
                    local.get 11
                    local.get 13
                    i32.add
                    i32.const 2
                    i32.shl
                    i32.add
                    i32.load
                  end
                  local.set 4
                  local.get 14
                  local.get 21
                  i32.add
                  local.set 6
                  local.get 4
                  local.get 5
                  i32.add
                end
                local.tee 4
                local.get 4
                i32.mul
                local.get 6
                local.get 6
                i32.mul
                i32.add
                f64.convert_i32_s
                f64.sqrt
                local.tee 28
                f64.abs
                f64.const 0x1p+31 (;=2.14748e+09;)
                f64.lt
                if  ;; label = @7
                  local.get 28
                  i32.trunc_f64_s
                  br 1 (;@6;)
                end
                i32.const -2147483648
              end
              local.set 4
              local.get 0
              local.get 10
              local.get 18
              i32.add
              i32.const 2
              i32.shl
              local.tee 5
              i32.add
              i32.const 255
              local.get 4
              i32.const 255
              local.get 4
              i32.const 255
              i32.lt_s
              select
              local.tee 4
              i32.sub
              local.get 4
              local.get 3
              select
              local.tee 4
              i32.store8
              local.get 0
              local.get 5
              i32.const 1
              i32.or
              i32.add
              local.get 4
              i32.store8
              local.get 0
              local.get 5
              i32.const 2
              i32.or
              i32.add
              local.get 4
              i32.store8
              local.get 0
              local.get 5
              i32.const 3
              i32.or
              i32.add
              i32.const 255
              i32.store8
              local.get 10
              i32.const 1
              i32.add
              local.tee 10
              local.get 1
              i32.ne
              br_if 0 (;@5;)
            end
          end
          local.get 9
        end
        local.tee 8
        local.get 2
        i32.lt_s
        local.set 17
        local.get 2
        local.get 8
        i32.ne
        br_if 0 (;@2;)
      end
    end)
  (func (;2;) (type 4) (param i32 i32 i32 i32 i32 i32 i32)
    (local i32 i32)
    local.get 1
    i32.const 1
    i32.ge_s
    if  ;; label = @1
      local.get 2
      i32.const 2
      i32.shl
      local.set 7
      i32.const 0
      local.set 2
      loop  ;; label = @2
        local.get 2
        i32.const 4
        i32.rem_s
        i32.const 3
        i32.ne
        if  ;; label = @3
          local.get 0
          local.get 2
          i32.add
          local.tee 8
          local.get 8
          i32.load8_u
          local.get 5
          i32.mul
          local.get 4
          i32.add
          local.get 0
          local.get 2
          local.get 7
          i32.add
          i32.add
          i32.load8_u
          local.get 0
          local.get 2
          local.get 6
          i32.add
          i32.add
          i32.load8_u
          i32.add
          i32.sub
          i32.store8
        end
        local.get 2
        local.get 3
        i32.add
        local.tee 2
        local.get 1
        i32.lt_s
        br_if 0 (;@2;)
      end
    end)
  (func (;3;) (type 0) (param i32 i32)
    (local i32 i32 i32 i32 i64 f32)
    local.get 1
    i32.const 0
    i32.gt_s
    if  ;; label = @1
      loop  ;; label = @2
        i32.const 1024
        i32.const 1024
        i64.load
        i64.const 6364136223846793005
        i64.mul
        i64.const 1
        i64.add
        local.tee 6
        i64.store
        local.get 6
        i64.const 33
        i64.shr_u
        i32.wrap_i64
        local.set 3
        local.get 0
        local.get 4
        i32.const 2
        i32.shl
        local.tee 2
        i32.add
        local.tee 5
        local.get 5
        f32.load
        local.get 3
        i32.const 70
        i32.rem_s
        i32.const -35
        i32.add
        f32.convert_i32_s
        local.tee 7
        f32.add
        f32.store
        local.get 0
        local.get 2
        i32.const 4
        i32.or
        i32.add
        local.tee 3
        local.get 3
        f32.load
        local.get 7
        f32.add
        f32.store
        local.get 0
        local.get 2
        i32.const 8
        i32.or
        i32.add
        local.tee 2
        local.get 2
        f32.load
        local.get 7
        f32.add
        f32.store
        local.get 4
        i32.const 4
        i32.add
        local.tee 4
        local.get 1
        i32.lt_s
        br_if 0 (;@2;)
      end
    end)
  (func (;4;) (type 0) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.const 0
    i32.gt_s
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        local.get 2
        i32.add
        local.tee 3
        local.get 3
        i32.load8_u
        i32.const -1
        i32.xor
        i32.store8
        local.get 0
        local.get 2
        i32.const 1
        i32.or
        i32.add
        local.tee 3
        local.get 3
        i32.load8_u
        i32.const -1
        i32.xor
        i32.store8
        local.get 0
        local.get 2
        i32.const 2
        i32.or
        i32.add
        local.tee 3
        local.get 3
        i32.load8_u
        i32.const -1
        i32.xor
        i32.store8
        local.get 2
        i32.const 4
        i32.add
        local.tee 2
        local.get 1
        i32.lt_s
        br_if 0 (;@2;)
      end
    end)
  (func (;5;) (type 2) (param i32 i32 i32)
    (local i32 i32 i32)
    local.get 1
    i32.const 0
    i32.gt_s
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        local.get 3
        i32.add
        local.tee 4
        i32.load8_u
        local.get 2
        i32.add
        local.tee 5
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 4
          local.get 5
          i32.store8
        end
        local.get 0
        local.get 3
        i32.const 1
        i32.or
        i32.add
        local.tee 4
        i32.load8_u
        local.get 2
        i32.add
        local.tee 5
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 4
          local.get 5
          i32.store8
        end
        local.get 0
        local.get 3
        i32.const 2
        i32.or
        i32.add
        local.tee 4
        i32.load8_u
        local.get 2
        i32.add
        local.tee 5
        i32.const 255
        i32.le_s
        if  ;; label = @3
          local.get 4
          local.get 5
          i32.store8
        end
        local.get 3
        i32.const 4
        i32.add
        local.tee 3
        local.get 1
        i32.lt_s
        br_if 0 (;@2;)
      end
    end)
  (func (;6;) (type 0) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.const 0
    i32.gt_s
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        local.get 2
        i32.const 1
        i32.or
        i32.add
        local.get 0
        local.get 2
        i32.add
        i32.load8_u
        local.tee 3
        i32.store8
        local.get 0
        local.get 2
        i32.const 2
        i32.or
        i32.add
        local.get 3
        i32.store8
        local.get 2
        i32.const 4
        i32.add
        local.tee 2
        local.get 1
        i32.lt_s
        br_if 0 (;@2;)
      end
    end)
  (func (;7;) (type 1)
    nop)
  (global (;0;) (mut i32) (i32.const 5244576))
  (export "a" (func 7))
  (export "b" (func 6))
  (export "c" (func 5))
  (export "d" (func 4))
  (export "e" (func 3))
  (export "f" (func 2))
  (export "g" (func 1))
  (export "h" (func 0)))
