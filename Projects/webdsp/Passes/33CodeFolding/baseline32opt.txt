[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        4.282e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000133322 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        9.242e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.000143669 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.000525417 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     9.794e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.00393608 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.00155749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.00221907 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.000191859 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.000928824 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.000312466 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.00881956 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.00792293 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.000502444 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0047244 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00173131 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000495393 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.000994273 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.00194668 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00144418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.00316092 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00160771 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000380073 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00106788 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000376194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.00129145 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0464369 seconds.
[PassRunner] (final validation)
