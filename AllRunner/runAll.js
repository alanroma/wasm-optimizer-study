const shelljs = require('shelljs');
const fs = require('fs');
const path = require('path');

function readFolder(directory){
    return fs.readdirSync(directory)
    .map( dir => path.resolve(directory, dir));
}


function handlePossibleProjectFolder(directory){
    const subfolders = readFolder(directory).filter(dirPath => fs.existsSync(dirPath) && fs.lstatSync(dirPath).isDirectory());
    const passesFolder = subfolders.find(dir => dir.includes('Passes'))
    const emccTempFolder = subfolders.find(dir => dir.includes('emcc_temp'));
        if(passesFolder  && emccTempFolder){
            const passesDirContents = readFolder(passesFolder)
            const baselineWasmFile = passesDirContents.find(dir => dir.endsWith('.wasm'));
            const foldersInProject = passesDirContents.filter(dir => !dir.endsWith('.wasm'));
            if(baselineWasmFile){
                //Valid project to scan, now check if already scanned
                const outputFolder1 = foldersInProject.find(dir => dir.includes('10SsaNomerge'))
                const outputFolder2 = foldersInProject.find(dir => dir.includes('58MinifyImportsAndExportsAndModules'))
                const outputFolder3 = foldersInProject.find(dir => dir.includes('Oz'))
                const outputFolder4 = foldersInProject.find(dir => dir.includes('52OptimizeStackIrOptLevel3'))
                if(outputFolder1 && outputFolder2 && outputFolder3 && outputFolder4){
                    //Already scanned, continue
                    return;
                }
                const emccTempContents = readFolder(emccTempFolder)
                const precleanFile = emccTempContents.find(dir => dir.includes('preclean') && dir.endsWith('.wasm'));
                const graphFile = emccTempContents.find(dir => dir.includes('graphFile') && dir.endsWith('.txt'));
                const binaryGeneratorPath = '/data/Code/wasm-optimizer-study/WasmBinaryGenerator/wasm_binary_generator.js';
                const commandToRun = `node ${binaryGeneratorPath} -a -b ${baselineWasmFile} -g ${graphFile} -p ${precleanFile}`
                console.log(commandToRun)
                console.log('')
                shelljs.exec(commandToRun);
            }
        } else {
            for(const subfolder of subfolders){
                handlePossibleProjectFolder(subfolder)
            }
        }
}

const projectDir = '../Projects'
const projects = readFolder(projectDir);
for(const project of projects){
    try{
        handlePossibleProjectFolder(project)
    } catch(err){
        console.log(err);
    }
}