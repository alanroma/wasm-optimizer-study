const fs = require('fs');
const path = require('path');

function readFolder(directory){
    return fs.readdirSync(directory)
    .map( dir => path.resolve(directory, dir));
}

function cleanAll(directory){
    const subfolders = readFolder(directory).filter(dirPath => fs.existsSync(dirPath) && fs.lstatSync(dirPath).isDirectory());
    const passesFolder = subfolders.find(dir => dir.includes('Passes'))
    if(passesFolder){
        const passesDirContents = readFolder(passesFolder)
        const foldersInProject = passesDirContents.filter(dir => !dir.endsWith('.wasm'));
        for(const folder of foldersInProject){
            fs.rmdirSync(folder, { recursive: true, force: true})
        }
    } else {
        for(const subfolder of subfolders){
            cleanAll(subfolder)
        }
    }
}


const projectDir = '../Projects'
const projects = readFolder(projectDir);
for(const project of projects){
    try{
        cleanAll(project)
    } catch(err){
        console.log(err);
    }
}