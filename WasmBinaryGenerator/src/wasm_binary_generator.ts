import { group } from 'console';
import path from 'path';
import shelljs from 'shelljs';
import {o1,o2,o3,OptimizationFlags, llvmGroupIndices, OptimizationLevels, llvmFlagsByOptimizationLevel} from './optimizationPassDetails';
const argv = require('yargs')
.option('passIndex', {
    alias: 'i',
    type: 'number',
    description: 'Index of optimzation pass to build',
})
.option('optimizationFlag', {
    alias: 'O',
    type: 'string',
    description: 'Optimization flag to use instead of indivdual passes',
})
.option('baseline', {
    alias: 'b',
    type: 'string',
    demand: true,
    description: 'Baseline WebAssembly module to use in the optimization passes',
    default: './pwgen.wasm'
})
.option('runAll', {
    alias: 'a',
    type: 'boolean',
    description: 'Run all optimization passes and optimization flags'
})
.option('llvm', {
    alias: 'l',
    type: 'boolean',
    default: false,
    description: 'Whether to generate LLVM IR or .wasm using Binaryen'
})
.option('group', {
    alias: 'r',
    type: 'number',
    description: 'Whether to use flags in an LLVM group'
})
.option('graphFile', {
    alias: 'g',
    type: 'string',
    description: 'Path to graph file for use in metadce (Indices 55 & 56)'
})
.option('preclean', {
    alias: 'p',
    type: 'string',
    description: 'Path to preclean wasm file for use in metadce (Indices 55 & 56)'
})
.option('opt-path', {
    type: 'string',
    default: '/data/Code/wasm-optimizer-study/llvm-project/build/bin/opt',
    description: 'Path to modified opt binary'
})
.argv;

declare interface LLVMIndexValue{
    group: number,
    flagLevel: OptimizationLevels,
    index: number,
    flag: string
}

const wasmBasePath = path.resolve(argv.baseline);
const parentDirectoryToOutput = path.dirname(wasmBasePath);

let SPECIAL_FLAGS = '';
const OPT_PATH = argv.optPath;

function convertHyphensToUpperCase(name: string){
    //@ts-ignore
    const camelCase = name.replace(/-([a-z])/g, (g: string[]) => { return g[1].toUpperCase(); });
    return camelCase.charAt(0).toUpperCase() + camelCase.substring(1,);
}

function getFlagsUpToIndex(flagType: any[], flagIndex: number){
    const flags = [];
    let currentIndex = 4;
    const includedPassIndices =[];
    for(; currentIndex <= flagIndex; currentIndex++){
        const optimizationPass = flagType[currentIndex];
        if(optimizationPass){
            //Optimize-Adder flags need the -lmu flag set
            if([18,19].includes(currentIndex)){
                SPECIAL_FLAGS = '-lmu';
            }
            includedPassIndices.push(currentIndex);
            flags.push(optimizationPass);
        }
    }
    const lastIndex = includedPassIndices.length > 0 ?
                        includedPassIndices[includedPassIndices.length - 2]
                        : 4;
    
    return {flags, lastIndex};
}

function getLLVMFlagsInGroup(group: number, optimizationLevel: OptimizationLevels): [number, string][]{
    const convertFlagObjectToArray = (obj: any): [number, string][] => {
        const result: [number, string][] = Object.keys(obj).map(function (key) { 
            return [Number(key), obj[key]]; 
        });
        return result;
    }
    const rebuildFlagObject = (keyValuePairs: any[]) => {
        const result:OptimizationFlags = {};
        for(const [key, value] of keyValuePairs){
            result[key] = value
        }
        return result;
    }
    const {min, max} = llvmGroupIndices[group];
    const flagsInOptimizationLevel = llvmFlagsByOptimizationLevel[optimizationLevel]
    const flagValuesDecomposed: [number, string][] = convertFlagObjectToArray(flagsInOptimizationLevel)
                                .filter(flagObjAr => flagObjAr[0] >= min && flagObjAr[0] <= max);
    // const flagValues = rebuildFlagObject(flagValuesDecomposed);
    return flagValuesDecomposed
}

function getFlagsFoundInLLVM(llvmGroup: number, optimizationFlag: OptimizationLevels){
    const llvmPassesToUse = getLLVMFlagsInGroup(llvmGroup, optimizationFlag)
    const flagsToUse = llvmPassesToUse
    return flagsToUse
}

function getFirstFlagFound(optimizationIndex: number){
    const optimizationOptions: any[] = [o2,o3,o1];
    let optimizationFlagType = null;
    for(const tempOptimizationFlagType of optimizationOptions){
        if(tempOptimizationFlagType[optimizationIndex] != null){
            optimizationFlagType = tempOptimizationFlagType;
            break;
        }
    }
    if(optimizationFlagType == null){
        console.log(`Could not find index ${optimizationIndex} among optimization flag types.`)
        process.exit(-1);
    }
    const targetFlagName = optimizationFlagType[optimizationIndex];

    const {flags, lastIndex } = getFlagsUpToIndex(optimizationFlagType, optimizationIndex);
    const previousFlags  = flags.map(flag => `--${flag}`);
    const targetFlag = previousFlags.pop();
    return { previousFlags, targetFlag, lastIndex, targetFlagName}
}

function handleOptimizationIndex(optimizationIndex: number){
    const indicesChangedByOptimizationLevelFlag = [44,52]
    const {previousFlags, lastIndex,targetFlag, targetFlagName} = getFirstFlagFound(optimizationIndex);
    const targetFlagDirectory = path.resolve(parentDirectoryToOutput,`${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}`);
    shelljs.mkdir(`${targetFlagDirectory}`);
    const originalWasmName = `original.wasm`
    shelljs.cp(wasmBasePath, path.resolve(targetFlagDirectory, originalWasmName));
    if(indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)){
        //Special cases affected by the optimization level
        const specialFlagDirectory = path.resolve(parentDirectoryToOutput,`${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}OptLevel3`);
        shelljs.mkdir(`${specialFlagDirectory}`);
        shelljs.cp(wasmBasePath, path.resolve(specialFlagDirectory, originalWasmName));
    }

    shelljs.cd(targetFlagDirectory);

    //First path, make up to, but not including, target optimization pass
    //Indices 55 - 58 are special cases
    //55 Is handled by the separate handleMetaDCE function
    //56 is in 55, they must be run together
    //57 is handled by lastIndex being 55
    //58 is handled by lastIndex being 57
    const previousBaselineName = `baseline${lastIndex}`
    const previousBaselineNameWasm = `baseline${lastIndex}.wasm`
    const previousBaselineNameWat = `baseline${lastIndex}.wat`

    if(lastIndex == 55){
        //MetaDCE Output
        const metaDCEWasm = path.resolve(parentDirectoryToOutput,'55MetaDCE', 'c55.wasm')
        shelljs.cp(metaDCEWasm, path.resolve(targetFlagDirectory, 'baseline55.wasm'));
        shelljs.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`)
    } else if( lastIndex == 57){
        const previousWasm = path.resolve(parentDirectoryToOutput,'57StripDwarf', 'c57.wasm')
        shelljs.cp(previousWasm, path.resolve(targetFlagDirectory, 'baseline57.wasm'));
        shelljs.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`)
    }
    else {
        const baselineOptCommand = `wasm-opt ${originalWasmName} -o ${previousBaselineNameWasm} ${previousFlags.join(' ')} ${SPECIAL_FLAGS} 2> ${previousBaselineName}opt.txt`;
        console.log(baselineOptCommand);
        shelljs.exec(baselineOptCommand);
        shelljs.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`)
    }

    let optimizationLevel = ''
    if(indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)){
        optimizationLevel = '-ol 2 -s 0'
    }
    
    //Pass target optimization function on baseline wasm
    const targetOptCommand =  `wasm-opt  ${previousBaselineNameWasm} ${optimizationLevel} -o c${optimizationIndex}.wasm ${targetFlag} ${SPECIAL_FLAGS} 2> c${optimizationIndex}opt.txt`;
    console.log(targetOptCommand);
    shelljs.exec(targetOptCommand);
    shelljs.exec(`wasm2wat c${optimizationIndex}.wasm -o c${optimizationIndex}.wat`);

    shelljs.cd(targetFlagDirectory);
    // Remove un-opted wasm file
    shelljs.rm(originalWasmName);
    if(indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)){
        //Special cases affected by the optimization level
        const specialFlagDirectory = path.resolve(parentDirectoryToOutput,`${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}OptLevel3`);
        shelljs.cd(specialFlagDirectory);
        shelljs.cp(path.resolve(targetFlagDirectory, previousBaselineNameWasm), path.resolve(specialFlagDirectory, previousBaselineNameWasm));
        shelljs.cp(path.resolve(targetFlagDirectory, previousBaselineNameWat), path.resolve(specialFlagDirectory, previousBaselineNameWat));

        //Pass target optimization function on baseline wasm
        const specialWasmName = `c${optimizationIndex}OptLevel3`
        const specialOptCommand =  `wasm-opt ${previousBaselineNameWasm} -ol 3 -s 0 -o ${specialWasmName}.wasm ${targetFlag} ${SPECIAL_FLAGS} 2> ${specialWasmName}.txt`;
        console.log(specialOptCommand);
        shelljs.exec(specialOptCommand);
        shelljs.exec(`wasm2wat ${specialWasmName}.wasm -o ${specialWasmName}.wat`)

        // Remove un-opted wasm file
        shelljs.rm(originalWasmName);
    }

    SPECIAL_FLAGS = ''
}

function handleOptimizationFlag(flagType: string){
    if(!['0','1','2','3','s','z','S','Z'].includes(flagType)){
        flagType = '0'
    }

    const targetFlagDirectory = path.resolve(parentDirectoryToOutput,`O${flagType}`);
    const originalWasmName = `original.wasm`
    shelljs.mkdir(`${targetFlagDirectory}`);
    shelljs.cp(wasmBasePath, path.resolve(targetFlagDirectory, originalWasmName));
    shelljs.cd(targetFlagDirectory);
    shelljs.exec(`wasm2wat original.wasm -o original.wat`)


    const baselineOptCommand = `wasm-opt -O${flagType} -o O${flagType}.wasm original.wasm  2> O${flagType}.txt`;
    console.log(baselineOptCommand);
    shelljs.exec(baselineOptCommand);
    shelljs.exec(`wasm2wat O${flagType}.wasm -o O${flagType}.wat`)

}

function handleMetaDCESteps(graphFile: string, preclean: string){
    console.log(graphFile, preclean)
    //Index 55 & 56

    const targetFlagDirectory = path.resolve(parentDirectoryToOutput,`55MetaDCE`);
    const originalWasmName = `original.wasm`

    const previousPassIndex = 54; 

    const {previousFlags,targetFlag} = getFirstFlagFound(previousPassIndex);

    const graphFileFullPath = path.resolve(__dirname,graphFile);
    const precleanFullPath = path.resolve(__dirname, preclean);

    shelljs.mkdir(`${targetFlagDirectory}`);
    shelljs.cp(wasmBasePath, path.resolve(targetFlagDirectory, originalWasmName));
    shelljs.cd(targetFlagDirectory);
    //First path, make up to, but not including, target optimization pass
    const baselineOptCommand = `wasm-opt ${originalWasmName} -o baseline${previousPassIndex}.wasm ${previousFlags.join(' ')} ${targetFlag} ${SPECIAL_FLAGS} 2> baseline${previousPassIndex}opt.txt`;
    console.log(baselineOptCommand,'\n');
    shelljs.exec(baselineOptCommand);
    shelljs.exec(`wasm2wat baseline${previousPassIndex}.wasm -o baseline${previousPassIndex}.wat`)

    const outputName = "c55.wasm";
    const outputPath = path.resolve(targetFlagDirectory, outputName)
    const wasmMetaDCECommand = `wasm-metadce --graph-file=${graphFileFullPath} ${precleanFullPath} -o ${outputPath} --mvp-features > c55.txt 2>&1`
    console.log(wasmMetaDCECommand);
    shelljs.exec(wasmMetaDCECommand);
    shelljs.exec(`wasm2wat c55.wasm -o c55.wat`)



}

function handleIndexRun(optimizationIndex: number, graphFile: any, precleanWasm: any){
    if(optimizationIndex < 4 || optimizationIndex > 58){
        optimizationIndex = 4;
    }

    if(optimizationIndex === 55){
        handleMetaDCESteps(graphFile, precleanWasm);
    } else {
        handleOptimizationIndex(optimizationIndex);
    }
}

function handleBinaryen(optimizationIndex: number, graphFile: string, precleanWasm: string, runAllPassesAndFlags: boolean, optimizationFlag: string){
    const oldEmccEnv = process.env.EMCC_DEBUG;
    const oldBinaryenEnv = process.env.BINARYEN_PASS_DEBUG;

    process.env.EMCC_DEBUG = "2";
    process.env.BINARYEN_PASS_DEBUG = "2";
    if((graphFile && !precleanWasm) || (!graphFile && precleanWasm)){
        console.log('If running metadce, both the --graphFile and --precleanWasm paths need to be specified!');
        console.log('Run node wasm_binary_generator.js --help for more info.');
    }

    if(runAllPassesAndFlags){
        const passIndicesToRun = [10,15,16,17,18,19,20,25,33,38,39,41,43,44,47,48,49,51,52,55,57,58];
        const flagTypesToRun = ['0','1','2','3','s', 'z'];

        for(const passIndex of passIndicesToRun){
            handleIndexRun(passIndex, graphFile, precleanWasm);
        }
        for(const flagType of flagTypesToRun){
            handleOptimizationFlag(flagType)
        }
    } else {
        if(optimizationIndex != null){
            handleIndexRun(optimizationIndex, graphFile, precleanWasm);
        }
        else if(optimizationFlag != null){
            handleOptimizationFlag(optimizationFlag)
        }
    }

    process.env.EMCC_DEBUG = oldEmccEnv;
    process.env.BINARYEN_PASS_DEBUG = oldBinaryenEnv;
}

function fixFlagValues(flag: string){
    switch(flag){
        case 'scoped-noalias':
            return 'scoped-noalias-aa';
        default:
            return flag
    }
}

function handleLLVMOpt(optimizationIndex: number, originalFlagsList: [number, string][]){
    const flagsList: [number, string][]  = originalFlagsList.map(flagListVal => [flagListVal[0], fixFlagValues(flagListVal[1])]);
    const passIndexInList = flagsList.findIndex((flagObj: any[]) => flagObj[0] === optimizationIndex);
    const passDetails = flagsList[passIndexInList];
    const targetFlagName = passDetails[1]
    const targetFlag = `-${targetFlagName}`
    const optimizationLevel = 1;
    const sizeLevel = 0;
    const flagsListWithHyphenAdded = flagsList.map(passObj => [passObj[0], `-${passObj[1]}`])
    if( passIndexInList !== 0){
        const priorPassObj = flagsListWithHyphenAdded[passIndexInList - 1];
        const previousIndex = priorPassObj[0];
        const previousFlagsList = flagsListWithHyphenAdded.filter((flagObj: any[]) => flagObj[0] < optimizationIndex).map((flagObj: any[]) => flagObj[1]).join(' ');

        const targetFlagDirectory = path.resolve(parentDirectoryToOutput,`${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}`);
        shelljs.mkdir(`${targetFlagDirectory}`);
        const originalWasmName = `original.ll`
        const originalWasmPath = path.resolve(targetFlagDirectory, originalWasmName)
        shelljs.cp(wasmBasePath, originalWasmPath);

        //Create baseline .ll file
        const previousBaselineLLPath = path.resolve(targetFlagDirectory, `baseline${previousIndex}.ll`)
        const baselineOptCommand = `${OPT_PATH} -S ${previousFlagsList} --size-level=${sizeLevel} --opt-level=${optimizationLevel} -o ${previousBaselineLLPath} ${originalWasmPath}`
        console.log(baselineOptCommand)
        shelljs.exec(baselineOptCommand);

        //Create file with targeted flags through opt
        const targetLLPath = path.resolve(targetFlagDirectory, `c${optimizationIndex}.ll`)
        const targetOptCommand = `${OPT_PATH} -S ${targetFlag} --size-level=${sizeLevel} --opt-level=${optimizationLevel} -o ${targetLLPath} ${previousBaselineLLPath}`
        console.log(targetOptCommand)
        shelljs.exec(targetOptCommand);
        shelljs.rm(originalWasmPath);
        console.log('')
    }
}

function getAllLLVMIndicies(): LLVMIndexValue[] {
    const llvmIndiciesWithInfo: LLVMIndexValue[] = [];
    const allLLVMIndices = new Set<number>();
    const optimizationLevelsToCheck: OptimizationLevels[] = [OptimizationLevels.O3, OptimizationLevels.O2, OptimizationLevels.Oz,OptimizationLevels.Os, OptimizationLevels.O1, OptimizationLevels.O0] 
    for(let groupnumber = 1; groupnumber <= 7; groupnumber++){
        for(const optLevel of optimizationLevelsToCheck){
            const llvmPassesToUse = getLLVMFlagsInGroup(groupnumber, optLevel)
            for(const [flagIdx, flagString] of llvmPassesToUse){
                if(!allLLVMIndices.has(flagIdx)){
                    llvmIndiciesWithInfo.push({index: flagIdx, group: groupnumber, flagLevel: optLevel, flag: flagString});
                    allLLVMIndices.add(flagIdx);
                }
            }
        }
    }
    return llvmIndiciesWithInfo
    .sort((a: LLVMIndexValue,b: LLVMIndexValue) => {
        return a.index - b.index
    });

} 

function handleLLVM(llvmGroup: number, optimizationFlag: OptimizationLevels, 
    optimizationIndex: number, runAllPassesAndFlags: boolean){
    if(runAllPassesAndFlags){
        const allIndicesToUse = getAllLLVMIndicies()
        for(let idx = 0; idx < allIndicesToUse.length; idx++){
            const indexDetails = allIndicesToUse[idx];
            const {flagLevel, group, index, flag} = indexDetails;
            // const {min, max} = llvmGroupIndices[group];
            const flagListUpToIndex: [number, string][] = allIndicesToUse.slice(0, idx +1).map(idxDetails => [idxDetails.index, idxDetails.flag])
            handleLLVMOpt(index,flagListUpToIndex);
            
        }
    } else {
        const flagsToUse = getFlagsFoundInLLVM(llvmGroup, optimizationFlag);
        handleLLVMOpt(optimizationIndex, flagsToUse);
    }
  

    
}

(async function main(){
    const optimizationIndex = argv.passIndex;
    const optimizationFlag: OptimizationLevels = argv.optimizationFlag;
    const runAllPassesAndFlags = argv.runAll;
    const graphFile = argv.graphFile;
    const precleanWasm = argv.preclean;
    const useLLVM = argv.llvm;
    const llvmGroup = argv.group ?? 1;
    if(useLLVM){
        handleLLVM(llvmGroup, optimizationFlag, optimizationIndex, runAllPassesAndFlags)
    } else {
        handleBinaryen(optimizationIndex, graphFile, precleanWasm, runAllPassesAndFlags,optimizationFlag)
    }
})()