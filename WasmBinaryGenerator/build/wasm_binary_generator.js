"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const shelljs_1 = __importDefault(require("shelljs"));
const optimizationPassDetails_1 = require("./optimizationPassDetails");
const argv = require('yargs')
    .option('passIndex', {
    alias: 'i',
    type: 'number',
    description: 'Index of optimzation pass to build',
})
    .option('optimizationFlag', {
    alias: 'O',
    type: 'string',
    description: 'Optimization flag to use instead of indivdual passes',
})
    .option('baseline', {
    alias: 'b',
    type: 'string',
    demand: true,
    description: 'Baseline WebAssembly module to use in the optimization passes',
    default: './pwgen.wasm'
})
    .option('runAll', {
    alias: 'a',
    type: 'boolean',
    description: 'Run all optimization passes and optimization flags'
})
    .option('llvm', {
    alias: 'l',
    type: 'boolean',
    default: false,
    description: 'Whether to generate LLVM IR or .wasm using Binaryen'
})
    .option('group', {
    alias: 'r',
    type: 'number',
    description: 'Whether to use flags in an LLVM group'
})
    .option('graphFile', {
    alias: 'g',
    type: 'string',
    description: 'Path to graph file for use in metadce (Indices 55 & 56)'
})
    .option('preclean', {
    alias: 'p',
    type: 'string',
    description: 'Path to preclean wasm file for use in metadce (Indices 55 & 56)'
})
    .option('opt-path', {
    type: 'string',
    default: '/data/Code/wasm-optimizer-study/llvm-project/build/bin/opt',
    description: 'Path to modified opt binary'
})
    .argv;
const wasmBasePath = path_1.default.resolve(argv.baseline);
const parentDirectoryToOutput = path_1.default.dirname(wasmBasePath);
let SPECIAL_FLAGS = '';
const OPT_PATH = argv.optPath;
function convertHyphensToUpperCase(name) {
    //@ts-ignore
    const camelCase = name.replace(/-([a-z])/g, (g) => { return g[1].toUpperCase(); });
    return camelCase.charAt(0).toUpperCase() + camelCase.substring(1);
}
function getFlagsUpToIndex(flagType, flagIndex) {
    const flags = [];
    let currentIndex = 4;
    const includedPassIndices = [];
    for (; currentIndex <= flagIndex; currentIndex++) {
        const optimizationPass = flagType[currentIndex];
        if (optimizationPass) {
            //Optimize-Adder flags need the -lmu flag set
            if ([18, 19].includes(currentIndex)) {
                SPECIAL_FLAGS = '-lmu';
            }
            includedPassIndices.push(currentIndex);
            flags.push(optimizationPass);
        }
    }
    const lastIndex = includedPassIndices.length > 0 ?
        includedPassIndices[includedPassIndices.length - 2]
        : 4;
    return { flags, lastIndex };
}
function getLLVMFlagsInGroup(group, optimizationLevel) {
    const convertFlagObjectToArray = (obj) => {
        const result = Object.keys(obj).map(function (key) {
            return [Number(key), obj[key]];
        });
        return result;
    };
    const rebuildFlagObject = (keyValuePairs) => {
        const result = {};
        for (const [key, value] of keyValuePairs) {
            result[key] = value;
        }
        return result;
    };
    const { min, max } = optimizationPassDetails_1.llvmGroupIndices[group];
    const flagsInOptimizationLevel = optimizationPassDetails_1.llvmFlagsByOptimizationLevel[optimizationLevel];
    const flagValuesDecomposed = convertFlagObjectToArray(flagsInOptimizationLevel)
        .filter(flagObjAr => flagObjAr[0] >= min && flagObjAr[0] <= max);
    // const flagValues = rebuildFlagObject(flagValuesDecomposed);
    return flagValuesDecomposed;
}
function getFlagsFoundInLLVM(llvmGroup, optimizationFlag) {
    const llvmPassesToUse = getLLVMFlagsInGroup(llvmGroup, optimizationFlag);
    const flagsToUse = llvmPassesToUse;
    return flagsToUse;
}
function getFirstFlagFound(optimizationIndex) {
    const optimizationOptions = [optimizationPassDetails_1.o2, optimizationPassDetails_1.o3, optimizationPassDetails_1.o1];
    let optimizationFlagType = null;
    for (const tempOptimizationFlagType of optimizationOptions) {
        if (tempOptimizationFlagType[optimizationIndex] != null) {
            optimizationFlagType = tempOptimizationFlagType;
            break;
        }
    }
    if (optimizationFlagType == null) {
        console.log(`Could not find index ${optimizationIndex} among optimization flag types.`);
        process.exit(-1);
    }
    const targetFlagName = optimizationFlagType[optimizationIndex];
    const { flags, lastIndex } = getFlagsUpToIndex(optimizationFlagType, optimizationIndex);
    const previousFlags = flags.map(flag => `--${flag}`);
    const targetFlag = previousFlags.pop();
    return { previousFlags, targetFlag, lastIndex, targetFlagName };
}
function handleOptimizationIndex(optimizationIndex) {
    const indicesChangedByOptimizationLevelFlag = [44, 52];
    const { previousFlags, lastIndex, targetFlag, targetFlagName } = getFirstFlagFound(optimizationIndex);
    const targetFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}`);
    shelljs_1.default.mkdir(`${targetFlagDirectory}`);
    const originalWasmName = `original.wasm`;
    shelljs_1.default.cp(wasmBasePath, path_1.default.resolve(targetFlagDirectory, originalWasmName));
    if (indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)) {
        //Special cases affected by the optimization level
        const specialFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}OptLevel3`);
        shelljs_1.default.mkdir(`${specialFlagDirectory}`);
        shelljs_1.default.cp(wasmBasePath, path_1.default.resolve(specialFlagDirectory, originalWasmName));
    }
    shelljs_1.default.cd(targetFlagDirectory);
    //First path, make up to, but not including, target optimization pass
    //Indices 55 - 58 are special cases
    //55 Is handled by the separate handleMetaDCE function
    //56 is in 55, they must be run together
    //57 is handled by lastIndex being 55
    //58 is handled by lastIndex being 57
    const previousBaselineName = `baseline${lastIndex}`;
    const previousBaselineNameWasm = `baseline${lastIndex}.wasm`;
    const previousBaselineNameWat = `baseline${lastIndex}.wat`;
    if (lastIndex == 55) {
        //MetaDCE Output
        const metaDCEWasm = path_1.default.resolve(parentDirectoryToOutput, '55MetaDCE', 'c55.wasm');
        shelljs_1.default.cp(metaDCEWasm, path_1.default.resolve(targetFlagDirectory, 'baseline55.wasm'));
        shelljs_1.default.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`);
    }
    else if (lastIndex == 57) {
        const previousWasm = path_1.default.resolve(parentDirectoryToOutput, '57StripDwarf', 'c57.wasm');
        shelljs_1.default.cp(previousWasm, path_1.default.resolve(targetFlagDirectory, 'baseline57.wasm'));
        shelljs_1.default.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`);
    }
    else {
        const baselineOptCommand = `wasm-opt ${originalWasmName} -o ${previousBaselineNameWasm} ${previousFlags.join(' ')} ${SPECIAL_FLAGS} 2> ${previousBaselineName}opt.txt`;
        console.log(baselineOptCommand);
        shelljs_1.default.exec(baselineOptCommand);
        shelljs_1.default.exec(`wasm2wat ${previousBaselineNameWasm} -o ${previousBaselineNameWat}`);
    }
    let optimizationLevel = '';
    if (indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)) {
        optimizationLevel = '-ol 2 -s 0';
    }
    //Pass target optimization function on baseline wasm
    const targetOptCommand = `wasm-opt  ${previousBaselineNameWasm} ${optimizationLevel} -o c${optimizationIndex}.wasm ${targetFlag} ${SPECIAL_FLAGS} 2> c${optimizationIndex}opt.txt`;
    console.log(targetOptCommand);
    shelljs_1.default.exec(targetOptCommand);
    shelljs_1.default.exec(`wasm2wat c${optimizationIndex}.wasm -o c${optimizationIndex}.wat`);
    shelljs_1.default.cd(targetFlagDirectory);
    // Remove un-opted wasm file
    shelljs_1.default.rm(originalWasmName);
    if (indicesChangedByOptimizationLevelFlag.includes(optimizationIndex)) {
        //Special cases affected by the optimization level
        const specialFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}OptLevel3`);
        shelljs_1.default.cd(specialFlagDirectory);
        shelljs_1.default.cp(path_1.default.resolve(targetFlagDirectory, previousBaselineNameWasm), path_1.default.resolve(specialFlagDirectory, previousBaselineNameWasm));
        shelljs_1.default.cp(path_1.default.resolve(targetFlagDirectory, previousBaselineNameWat), path_1.default.resolve(specialFlagDirectory, previousBaselineNameWat));
        //Pass target optimization function on baseline wasm
        const specialWasmName = `c${optimizationIndex}OptLevel3`;
        const specialOptCommand = `wasm-opt ${previousBaselineNameWasm} -ol 3 -s 0 -o ${specialWasmName}.wasm ${targetFlag} ${SPECIAL_FLAGS} 2> ${specialWasmName}.txt`;
        console.log(specialOptCommand);
        shelljs_1.default.exec(specialOptCommand);
        shelljs_1.default.exec(`wasm2wat ${specialWasmName}.wasm -o ${specialWasmName}.wat`);
        // Remove un-opted wasm file
        shelljs_1.default.rm(originalWasmName);
    }
    SPECIAL_FLAGS = '';
}
function handleOptimizationFlag(flagType) {
    if (!['0', '1', '2', '3', 's', 'z', 'S', 'Z'].includes(flagType)) {
        flagType = '0';
    }
    const targetFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `O${flagType}`);
    const originalWasmName = `original.wasm`;
    shelljs_1.default.mkdir(`${targetFlagDirectory}`);
    shelljs_1.default.cp(wasmBasePath, path_1.default.resolve(targetFlagDirectory, originalWasmName));
    shelljs_1.default.cd(targetFlagDirectory);
    shelljs_1.default.exec(`wasm2wat original.wasm -o original.wat`);
    const baselineOptCommand = `wasm-opt -O${flagType} -o O${flagType}.wasm original.wasm  2> O${flagType}.txt`;
    console.log(baselineOptCommand);
    shelljs_1.default.exec(baselineOptCommand);
    shelljs_1.default.exec(`wasm2wat O${flagType}.wasm -o O${flagType}.wat`);
}
function handleMetaDCESteps(graphFile, preclean) {
    console.log(graphFile, preclean);
    //Index 55 & 56
    const targetFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `55MetaDCE`);
    const originalWasmName = `original.wasm`;
    const previousPassIndex = 54;
    const { previousFlags, targetFlag } = getFirstFlagFound(previousPassIndex);
    const graphFileFullPath = path_1.default.resolve(__dirname, graphFile);
    const precleanFullPath = path_1.default.resolve(__dirname, preclean);
    shelljs_1.default.mkdir(`${targetFlagDirectory}`);
    shelljs_1.default.cp(wasmBasePath, path_1.default.resolve(targetFlagDirectory, originalWasmName));
    shelljs_1.default.cd(targetFlagDirectory);
    //First path, make up to, but not including, target optimization pass
    const baselineOptCommand = `wasm-opt ${originalWasmName} -o baseline${previousPassIndex}.wasm ${previousFlags.join(' ')} ${targetFlag} ${SPECIAL_FLAGS} 2> baseline${previousPassIndex}opt.txt`;
    console.log(baselineOptCommand, '\n');
    shelljs_1.default.exec(baselineOptCommand);
    shelljs_1.default.exec(`wasm2wat baseline${previousPassIndex}.wasm -o baseline${previousPassIndex}.wat`);
    const outputName = "c55.wasm";
    const outputPath = path_1.default.resolve(targetFlagDirectory, outputName);
    const wasmMetaDCECommand = `wasm-metadce --graph-file=${graphFileFullPath} ${precleanFullPath} -o ${outputPath} --mvp-features > c55.txt 2>&1`;
    console.log(wasmMetaDCECommand);
    shelljs_1.default.exec(wasmMetaDCECommand);
    shelljs_1.default.exec(`wasm2wat c55.wasm -o c55.wat`);
}
function handleIndexRun(optimizationIndex, graphFile, precleanWasm) {
    if (optimizationIndex < 4 || optimizationIndex > 58) {
        optimizationIndex = 4;
    }
    if (optimizationIndex === 55) {
        handleMetaDCESteps(graphFile, precleanWasm);
    }
    else {
        handleOptimizationIndex(optimizationIndex);
    }
}
function handleBinaryen(optimizationIndex, graphFile, precleanWasm, runAllPassesAndFlags, optimizationFlag) {
    const oldEmccEnv = process.env.EMCC_DEBUG;
    const oldBinaryenEnv = process.env.BINARYEN_PASS_DEBUG;
    process.env.EMCC_DEBUG = "2";
    process.env.BINARYEN_PASS_DEBUG = "2";
    if ((graphFile && !precleanWasm) || (!graphFile && precleanWasm)) {
        console.log('If running metadce, both the --graphFile and --precleanWasm paths need to be specified!');
        console.log('Run node wasm_binary_generator.js --help for more info.');
    }
    if (runAllPassesAndFlags) {
        const passIndicesToRun = [10, 15, 16, 17, 18, 19, 20, 25, 33, 38, 39, 41, 43, 44, 47, 48, 49, 51, 52, 55, 57, 58];
        const flagTypesToRun = ['0', '1', '2', '3', 's', 'z'];
        for (const passIndex of passIndicesToRun) {
            handleIndexRun(passIndex, graphFile, precleanWasm);
        }
        for (const flagType of flagTypesToRun) {
            handleOptimizationFlag(flagType);
        }
    }
    else {
        if (optimizationIndex != null) {
            handleIndexRun(optimizationIndex, graphFile, precleanWasm);
        }
        else if (optimizationFlag != null) {
            handleOptimizationFlag(optimizationFlag);
        }
    }
    process.env.EMCC_DEBUG = oldEmccEnv;
    process.env.BINARYEN_PASS_DEBUG = oldBinaryenEnv;
}
function fixFlagValues(flag) {
    switch (flag) {
        case 'scoped-noalias':
            return 'scoped-noalias-aa';
        default:
            return flag;
    }
}
function handleLLVMOpt(optimizationIndex, originalFlagsList) {
    const flagsList = originalFlagsList.map(flagListVal => [flagListVal[0], fixFlagValues(flagListVal[1])]);
    const passIndexInList = flagsList.findIndex((flagObj) => flagObj[0] === optimizationIndex);
    const passDetails = flagsList[passIndexInList];
    const targetFlagName = passDetails[1];
    const targetFlag = `-${targetFlagName}`;
    const optimizationLevel = 1;
    const sizeLevel = 0;
    const flagsListWithHyphenAdded = flagsList.map(passObj => [passObj[0], `-${passObj[1]}`]);
    if (passIndexInList !== 0) {
        const priorPassObj = flagsListWithHyphenAdded[passIndexInList - 1];
        const previousIndex = priorPassObj[0];
        const previousFlagsList = flagsListWithHyphenAdded.filter((flagObj) => flagObj[0] < optimizationIndex).map((flagObj) => flagObj[1]).join(' ');
        const targetFlagDirectory = path_1.default.resolve(parentDirectoryToOutput, `${optimizationIndex}${convertHyphensToUpperCase(targetFlagName)}`);
        shelljs_1.default.mkdir(`${targetFlagDirectory}`);
        const originalWasmName = `original.ll`;
        const originalWasmPath = path_1.default.resolve(targetFlagDirectory, originalWasmName);
        shelljs_1.default.cp(wasmBasePath, originalWasmPath);
        //Create baseline .ll file
        const previousBaselineLLPath = path_1.default.resolve(targetFlagDirectory, `baseline${previousIndex}.ll`);
        const baselineOptCommand = `${OPT_PATH} -S ${previousFlagsList} --size-level=${sizeLevel} --opt-level=${optimizationLevel} -o ${previousBaselineLLPath} ${originalWasmPath}`;
        console.log(baselineOptCommand);
        shelljs_1.default.exec(baselineOptCommand);
        //Create file with targeted flags through opt
        const targetLLPath = path_1.default.resolve(targetFlagDirectory, `c${optimizationIndex}.ll`);
        const targetOptCommand = `${OPT_PATH} -S ${targetFlag} --size-level=${sizeLevel} --opt-level=${optimizationLevel} -o ${targetLLPath} ${previousBaselineLLPath}`;
        console.log(targetOptCommand);
        shelljs_1.default.exec(targetOptCommand);
        shelljs_1.default.rm(originalWasmPath);
        console.log('');
    }
}
function getAllLLVMIndicies() {
    const llvmIndiciesWithInfo = [];
    const allLLVMIndices = new Set();
    const optimizationLevelsToCheck = [optimizationPassDetails_1.OptimizationLevels.O3, optimizationPassDetails_1.OptimizationLevels.O2, optimizationPassDetails_1.OptimizationLevels.Oz, optimizationPassDetails_1.OptimizationLevels.Os, optimizationPassDetails_1.OptimizationLevels.O1, optimizationPassDetails_1.OptimizationLevels.O0];
    for (let groupnumber = 1; groupnumber <= 7; groupnumber++) {
        for (const optLevel of optimizationLevelsToCheck) {
            const llvmPassesToUse = getLLVMFlagsInGroup(groupnumber, optLevel);
            for (const [flagIdx, flagString] of llvmPassesToUse) {
                if (!allLLVMIndices.has(flagIdx)) {
                    llvmIndiciesWithInfo.push({ index: flagIdx, group: groupnumber, flagLevel: optLevel, flag: flagString });
                    allLLVMIndices.add(flagIdx);
                }
            }
        }
    }
    return llvmIndiciesWithInfo
        .sort((a, b) => {
        return a.index - b.index;
    });
}
function handleLLVM(llvmGroup, optimizationFlag, optimizationIndex, runAllPassesAndFlags) {
    if (runAllPassesAndFlags) {
        const allIndicesToUse = getAllLLVMIndicies();
        for (let idx = 0; idx < allIndicesToUse.length; idx++) {
            const indexDetails = allIndicesToUse[idx];
            const { flagLevel, group, index, flag } = indexDetails;
            // const {min, max} = llvmGroupIndices[group];
            const flagListUpToIndex = allIndicesToUse.slice(0, idx + 1).map(idxDetails => [idxDetails.index, idxDetails.flag]);
            handleLLVMOpt(index, flagListUpToIndex);
        }
    }
    else {
        const flagsToUse = getFlagsFoundInLLVM(llvmGroup, optimizationFlag);
        handleLLVMOpt(optimizationIndex, flagsToUse);
    }
}
(function main() {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const optimizationIndex = argv.passIndex;
        const optimizationFlag = argv.optimizationFlag;
        const runAllPassesAndFlags = argv.runAll;
        const graphFile = argv.graphFile;
        const precleanWasm = argv.preclean;
        const useLLVM = argv.llvm;
        const llvmGroup = (_a = argv.group) !== null && _a !== void 0 ? _a : 1;
        if (useLLVM) {
            handleLLVM(llvmGroup, optimizationFlag, optimizationIndex, runAllPassesAndFlags);
        }
        else {
            handleBinaryen(optimizationIndex, graphFile, precleanWasm, runAllPassesAndFlags, optimizationFlag);
        }
    });
})();
