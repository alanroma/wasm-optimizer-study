#! /bin/bash

"/home/alan/emsdk/upstream/bin/clang-11" -cc1 -S -disable-llvm-passes -triple wasm32-unknown-emscripten -emit-llvm -main-file-name gcc-loops.cpp -target-cpu generic -fvisibility hidden -v -resource-dir /home/alan/emsdk/upstream/lib/clang/11.0.0 -D __EMSCRIPTEN_major__=1 -D __EMSCRIPTEN_minor__=39 -D __EMSCRIPTEN_tiny__=18 -D _LIBCPP_ABI_VERSION=2 -D unix -D __unix -D __unix__ -D EMSCRIPTEN -internal-isystem /include/wasm32-emscripten/c++/v1 -internal-isystem /include/c++/v1 -internal-isystem /home/alan/emsdk/upstream/lib/clang/11.0.0/include -internal-isystem /include/wasm32-emscripten -internal-isystem /include -Werror=implicit-function-declaration -fdebug-compilation-dir /data/Code/wasm-optimizer-study/LLVMTestGCCLoops -ferror-limit 19 -fgnuc-version=4.2.1 -isystem/home/alan/emsdk/upstream/emscripten/system/include/libcxx -isystem/home/alan/emsdk/upstream/emscripten/system/lib/libcxxabi/include -isystem/home/alan/emsdk/upstream/emscripten/system/lib/libunwind/include -isystem/home/alan/emsdk/upstream/emscripten/system/include/compat -isystem/home/alan/emsdk/upstream/emscripten/system/include -isystem/home/alan/emsdk/upstream/emscripten/system/include/libc -isystem/home/alan/emsdk/upstream/emscripten/system/lib/libc/musl/arch/emscripten -isystem/home/alan/emsdk/upstream/emscripten/system/local/include -isystem/home/alan/emsdk/upstream/emscripten/system/include/SSE -isystem/home/alan/emsdk/upstream/emscripten/cache/wasm/include -isystem/home/alan/emsdk/upstream/emscripten/system/include/SDL -o gcc-loops.ll -x c++ gcc-loops.cpp

opt gcc-loops.ll -O0 -S -o gcc-loops-opt0.ll
opt gcc-loops.ll -O3 -S -o gcc-loops-opt3.ll

llc -mtriple=wasm32-unknown-unknown -O0 -filetype=obj gcc-loops.ll -o gcc-loops.o
llc -mtriple=wasm32-unknown-unknown -O0 -filetype=obj gcc-loops-opt0.ll -o gcc-loops-opt0.o
llc -mtriple=wasm32-unknown-unknown -O0 -filetype=obj gcc-loops-opt3.ll -o gcc-loops-opt3.o

wasm-ld gcc-loops.o -o gcc-loop-ld.wasm --export-all --no-entry --allow-undefined
wasm-ld gcc-loops-opt0.o -o gcc-loops-opt0.wasm --export-all --no-entry --allow-undefined
wasm-ld gcc-loops-opt3.o -o gcc-loops-opt3.wasm --export-all --no-entry --allow-undefined
