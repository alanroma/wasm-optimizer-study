	.text
	.file	"gcc-loops.cpp"
	.section	.text._Z8example1v,"",@
	.globl	_Z8example1v            # -- Begin function _Z8example1v
	.type	_Z8example1v,@function
_Z8example1v:                           # @_Z8example1v
	.functype	_Z8example1v () -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	0
	i32.const	16
	local.set	1
	local.get	0
	local.get	1
	i32.sub 
	local.set	2
	i32.const	0
	local.set	3
	local.get	2
	local.get	3
	i32.store	12
.LBB0_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label1:
	i32.const	256
	local.set	4
	local.get	2
	i32.load	12
	local.set	5
	local.get	5
	local.set	6
	local.get	4
	local.set	7
	local.get	6
	local.get	7
	i32.lt_s
	local.set	8
	i32.const	1
	local.set	9
	local.get	8
	local.get	9
	i32.and 
	local.set	10
	local.get	10
	i32.eqz
	br_if   	1               # 1: down to label0
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB0_1 Depth=1
	local.get	2
	i32.load	12
	local.set	11
	i32.const	2
	local.set	12
	local.get	11
	local.get	12
	i32.shl 
	local.set	13
	global.get	b@GOT
	local.set	14
	local.get	14
	local.get	13
	i32.add 
	local.set	15
	local.get	15
	i32.load	0
	local.set	16
	global.get	c@GOT
	local.set	17
	local.get	17
	local.get	13
	i32.add 
	local.set	18
	local.get	18
	i32.load	0
	local.set	19
	local.get	16
	local.get	19
	i32.add 
	local.set	20
	global.get	a@GOT
	local.set	21
	local.get	21
	local.get	13
	i32.add 
	local.set	22
	local.get	22
	local.get	20
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB0_1 Depth=1
	local.get	2
	i32.load	12
	local.set	23
	i32.const	1
	local.set	24
	local.get	23
	local.get	24
	i32.add 
	local.set	25
	local.get	2
	local.get	25
	i32.store	12
	br      	0               # 0: up to label1
.LBB0_4:                                # %for.end
	end_loop
	end_block                       # label0:
	return
	end_function
.Lfunc_end0:
	.size	_Z8example1v, .Lfunc_end0-_Z8example1v
                                        # -- End function
	.section	.text._Z9example2aii,"",@
	.globl	_Z9example2aii          # -- Begin function _Z9example2aii
	.type	_Z9example2aii,@function
_Z9example2aii:                         # @_Z9example2aii
	.functype	_Z9example2aii (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	4
.LBB1_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label3:
	local.get	4
	i32.load	4
	local.set	6
	local.get	4
	i32.load	12
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.lt_s
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label2
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB1_1 Depth=1
	local.get	4
	i32.load	8
	local.set	13
	local.get	4
	i32.load	4
	local.set	14
	i32.const	2
	local.set	15
	local.get	14
	local.get	15
	i32.shl 
	local.set	16
	global.get	b@GOT
	local.set	17
	local.get	17
	local.get	16
	i32.add 
	local.set	18
	local.get	18
	local.get	13
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB1_1 Depth=1
	local.get	4
	i32.load	4
	local.set	19
	i32.const	1
	local.set	20
	local.get	19
	local.get	20
	i32.add 
	local.set	21
	local.get	4
	local.get	21
	i32.store	4
	br      	0               # 0: up to label3
.LBB1_4:                                # %for.end
	end_loop
	end_block                       # label2:
	return
	end_function
.Lfunc_end1:
	.size	_Z9example2aii, .Lfunc_end1-_Z9example2aii
                                        # -- End function
	.section	.text._Z9example2bii,"",@
	.globl	_Z9example2bii          # -- Begin function _Z9example2bii
	.type	_Z9example2bii,@function
_Z9example2bii:                         # @_Z9example2bii
	.functype	_Z9example2bii (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	4
.LBB2_1:                                # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label5:
	local.get	4
	i32.load	12
	local.set	6
	i32.const	-1
	local.set	7
	local.get	6
	local.get	7
	i32.add 
	local.set	8
	local.get	4
	local.get	8
	i32.store	12
	local.get	6
	i32.eqz
	br_if   	1               # 1: down to label4
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB2_1 Depth=1
	local.get	4
	i32.load	4
	local.set	9
	i32.const	2
	local.set	10
	local.get	9
	local.get	10
	i32.shl 
	local.set	11
	global.get	b@GOT
	local.set	12
	local.get	12
	local.get	11
	i32.add 
	local.set	13
	local.get	13
	i32.load	0
	local.set	14
	global.get	c@GOT
	local.set	15
	local.get	15
	local.get	11
	i32.add 
	local.set	16
	local.get	16
	i32.load	0
	local.set	17
	local.get	14
	local.get	17
	i32.and 
	local.set	18
	global.get	a@GOT
	local.set	19
	local.get	19
	local.get	11
	i32.add 
	local.set	20
	local.get	20
	local.get	18
	i32.store	0
	local.get	4
	i32.load	4
	local.set	21
	i32.const	1
	local.set	22
	local.get	21
	local.get	22
	i32.add 
	local.set	23
	local.get	4
	local.get	23
	i32.store	4
	br      	0               # 0: up to label5
.LBB2_3:                                # %while.end
	end_loop
	end_block                       # label4:
	return
	end_function
.Lfunc_end2:
	.size	_Z9example2bii, .Lfunc_end2-_Z9example2bii
                                        # -- End function
	.section	.text._Z8example3iPiS_,"",@
	.globl	_Z8example3iPiS_        # -- Begin function _Z8example3iPiS_
	.type	_Z8example3iPiS_,@function
_Z8example3iPiS_:                       # @_Z8example3iPiS_
	.functype	_Z8example3iPiS_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
.LBB3_1:                                # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label7:
	local.get	5
	i32.load	12
	local.set	6
	i32.const	-1
	local.set	7
	local.get	6
	local.get	7
	i32.add 
	local.set	8
	local.get	5
	local.get	8
	i32.store	12
	local.get	6
	i32.eqz
	br_if   	1               # 1: down to label6
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB3_1 Depth=1
	local.get	5
	i32.load	4
	local.set	9
	i32.const	4
	local.set	10
	local.get	9
	local.get	10
	i32.add 
	local.set	11
	local.get	5
	local.get	11
	i32.store	4
	local.get	9
	i32.load	0
	local.set	12
	local.get	5
	i32.load	8
	local.set	13
	i32.const	4
	local.set	14
	local.get	13
	local.get	14
	i32.add 
	local.set	15
	local.get	5
	local.get	15
	i32.store	8
	local.get	13
	local.get	12
	i32.store	0
	br      	0               # 0: up to label7
.LBB3_3:                                # %while.end
	end_loop
	end_block                       # label6:
	return
	end_function
.Lfunc_end3:
	.size	_Z8example3iPiS_, .Lfunc_end3-_Z8example3iPiS_
                                        # -- End function
	.section	.text._Z9example4aiPiS_,"",@
	.globl	_Z9example4aiPiS_       # -- Begin function _Z9example4aiPiS_
	.type	_Z9example4aiPiS_,@function
_Z9example4aiPiS_:                      # @_Z9example4aiPiS_
	.functype	_Z9example4aiPiS_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
.LBB4_1:                                # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label9:
	local.get	5
	i32.load	12
	local.set	6
	i32.const	-1
	local.set	7
	local.get	6
	local.get	7
	i32.add 
	local.set	8
	local.get	5
	local.get	8
	i32.store	12
	local.get	6
	i32.eqz
	br_if   	1               # 1: down to label8
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB4_1 Depth=1
	local.get	5
	i32.load	4
	local.set	9
	i32.const	4
	local.set	10
	local.get	9
	local.get	10
	i32.add 
	local.set	11
	local.get	5
	local.get	11
	i32.store	4
	local.get	9
	i32.load	0
	local.set	12
	i32.const	5
	local.set	13
	local.get	12
	local.get	13
	i32.add 
	local.set	14
	local.get	5
	i32.load	8
	local.set	15
	i32.const	4
	local.set	16
	local.get	15
	local.get	16
	i32.add 
	local.set	17
	local.get	5
	local.get	17
	i32.store	8
	local.get	15
	local.get	14
	i32.store	0
	br      	0               # 0: up to label9
.LBB4_3:                                # %while.end
	end_loop
	end_block                       # label8:
	return
	end_function
.Lfunc_end4:
	.size	_Z9example4aiPiS_, .Lfunc_end4-_Z9example4aiPiS_
                                        # -- End function
	.section	.text._Z9example4biPiS_,"",@
	.globl	_Z9example4biPiS_       # -- Begin function _Z9example4biPiS_
	.type	_Z9example4biPiS_,@function
_Z9example4biPiS_:                      # @_Z9example4biPiS_
	.functype	_Z9example4biPiS_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	i32.const	0
	local.set	6
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	local.get	6
	i32.store	0
.LBB5_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label11:
	local.get	5
	i32.load	0
	local.set	7
	local.get	5
	i32.load	12
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.lt_s
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	local.get	13
	i32.eqz
	br_if   	1               # 1: down to label10
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB5_1 Depth=1
	local.get	5
	i32.load	0
	local.set	14
	global.get	b@GOT
	local.set	15
	i32.const	2
	local.set	16
	local.get	14
	local.get	16
	i32.shl 
	local.set	17
	local.get	17
	local.get	15
	i32.add 
	local.set	18
	i32.const	4
	local.set	19
	local.get	18
	local.get	19
	i32.add 
	local.set	20
	local.get	20
	i32.load	0
	local.set	21
	global.get	c@GOT
	local.set	22
	local.get	17
	local.get	22
	i32.add 
	local.set	23
	i32.const	12
	local.set	24
	local.get	23
	local.get	24
	i32.add 
	local.set	25
	local.get	25
	i32.load	0
	local.set	26
	local.get	21
	local.get	26
	i32.add 
	local.set	27
	global.get	a@GOT
	local.set	28
	local.get	28
	local.get	17
	i32.add 
	local.set	29
	local.get	29
	local.get	27
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB5_1 Depth=1
	local.get	5
	i32.load	0
	local.set	30
	i32.const	1
	local.set	31
	local.get	30
	local.get	31
	i32.add 
	local.set	32
	local.get	5
	local.get	32
	i32.store	0
	br      	0               # 0: up to label11
.LBB5_4:                                # %for.end
	end_loop
	end_block                       # label10:
	return
	end_function
.Lfunc_end5:
	.size	_Z9example4biPiS_, .Lfunc_end5-_Z9example4biPiS_
                                        # -- End function
	.section	.text._Z9example4ciPiS_,"",@
	.globl	_Z9example4ciPiS_       # -- Begin function _Z9example4ciPiS_
	.type	_Z9example4ciPiS_,@function
_Z9example4ciPiS_:                      # @_Z9example4ciPiS_
	.functype	_Z9example4ciPiS_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	i32.const	0
	local.set	6
	i32.const	4
	local.set	7
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store	20
	local.get	5
	local.get	7
	i32.store	12
	local.get	5
	local.get	6
	i32.store	16
.LBB6_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label13:
	local.get	5
	i32.load	16
	local.set	8
	local.get	5
	i32.load	28
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.set	11
	local.get	10
	local.get	11
	i32.lt_s
	local.set	12
	i32.const	1
	local.set	13
	local.get	12
	local.get	13
	i32.and 
	local.set	14
	local.get	14
	i32.eqz
	br_if   	1               # 1: down to label12
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB6_1 Depth=1
	local.get	5
	i32.load	16
	local.set	15
	i32.const	2
	local.set	16
	local.get	15
	local.get	16
	i32.shl 
	local.set	17
	global.get	a@GOT
	local.set	18
	local.get	18
	local.get	17
	i32.add 
	local.set	19
	local.get	19
	i32.load	0
	local.set	20
	local.get	5
	local.get	20
	i32.store	8
	local.get	5
	i32.load	8
	local.set	21
	i32.const	4
	local.set	22
	local.get	21
	local.get	22
	i32.gt_s
	local.set	23
	local.get	23
	local.get	16
	i32.shl 
	local.set	24
	local.get	5
	i32.load	16
	local.set	25
	local.get	25
	local.get	16
	i32.shl 
	local.set	26
	global.get	b@GOT
	local.set	27
	local.get	27
	local.get	26
	i32.add 
	local.set	28
	local.get	28
	local.get	24
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB6_1 Depth=1
	local.get	5
	i32.load	16
	local.set	29
	i32.const	1
	local.set	30
	local.get	29
	local.get	30
	i32.add 
	local.set	31
	local.get	5
	local.get	31
	i32.store	16
	br      	0               # 0: up to label13
.LBB6_4:                                # %for.end
	end_loop
	end_block                       # label12:
	return
	end_function
.Lfunc_end6:
	.size	_Z9example4ciPiS_, .Lfunc_end6-_Z9example4ciPiS_
                                        # -- End function
	.section	.text._Z8example5iP1A,"",@
	.globl	_Z8example5iP1A         # -- Begin function _Z8example5iP1A
	.type	_Z8example5iP1A,@function
_Z8example5iP1A:                        # @_Z8example5iP1A
	.functype	_Z8example5iP1A (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	4
.LBB7_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label15:
	local.get	4
	i32.load	4
	local.set	6
	local.get	4
	i32.load	12
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.lt_s
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label14
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB7_1 Depth=1
	i32.const	5
	local.set	13
	local.get	4
	i32.load	8
	local.set	14
	local.get	4
	i32.load	4
	local.set	15
	i32.const	2
	local.set	16
	local.get	15
	local.get	16
	i32.shl 
	local.set	17
	local.get	14
	local.get	17
	i32.add 
	local.set	18
	local.get	18
	local.get	13
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB7_1 Depth=1
	local.get	4
	i32.load	4
	local.set	19
	i32.const	1
	local.set	20
	local.get	19
	local.get	20
	i32.add 
	local.set	21
	local.get	4
	local.get	21
	i32.store	4
	br      	0               # 0: up to label15
.LBB7_4:                                # %for.end
	end_loop
	end_block                       # label14:
	return
	end_function
.Lfunc_end7:
	.size	_Z8example5iP1A, .Lfunc_end7-_Z8example5iP1A
                                        # -- End function
	.section	.text._Z8example7i,"",@
	.globl	_Z8example7i            # -- Begin function _Z8example7i
	.type	_Z8example7i,@function
_Z8example7i:                           # @_Z8example7i
	.functype	_Z8example7i (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	local.get	4
	i32.store	8
.LBB8_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label17:
	i32.const	1024
	local.set	5
	local.get	3
	i32.load	8
	local.set	6
	local.get	6
	local.set	7
	local.get	5
	local.set	8
	local.get	7
	local.get	8
	i32.lt_s
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	i32.eqz
	br_if   	1               # 1: down to label16
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB8_1 Depth=1
	local.get	3
	i32.load	8
	local.set	12
	local.get	3
	i32.load	12
	local.set	13
	local.get	12
	local.get	13
	i32.add 
	local.set	14
	i32.const	2
	local.set	15
	local.get	14
	local.get	15
	i32.shl 
	local.set	16
	global.get	b@GOT
	local.set	17
	local.get	17
	local.get	16
	i32.add 
	local.set	18
	local.get	18
	i32.load	0
	local.set	19
	local.get	12
	local.get	15
	i32.shl 
	local.set	20
	global.get	a@GOT
	local.set	21
	local.get	21
	local.get	20
	i32.add 
	local.set	22
	local.get	22
	local.get	19
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB8_1 Depth=1
	local.get	3
	i32.load	8
	local.set	23
	i32.const	1
	local.set	24
	local.get	23
	local.get	24
	i32.add 
	local.set	25
	local.get	3
	local.get	25
	i32.store	8
	br      	0               # 0: up to label17
.LBB8_4:                                # %for.end
	end_loop
	end_block                       # label16:
	return
	end_function
.Lfunc_end8:
	.size	_Z8example7i, .Lfunc_end8-_Z8example7i
                                        # -- End function
	.section	.text._Z8example8i,"",@
	.globl	_Z8example8i            # -- Begin function _Z8example8i
	.type	_Z8example8i,@function
_Z8example8i:                           # @_Z8example8i
	.functype	_Z8example8i (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	local.get	4
	i32.store	8
.LBB9_1:                                # %for.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_3 Depth 2
	block   	
	loop    	                # label19:
	i32.const	32
	local.set	5
	local.get	3
	i32.load	8
	local.set	6
	local.get	6
	local.set	7
	local.get	5
	local.set	8
	local.get	7
	local.get	8
	i32.lt_s
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	i32.eqz
	br_if   	1               # 1: down to label18
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB9_1 Depth=1
	i32.const	0
	local.set	12
	local.get	3
	local.get	12
	i32.store	4
.LBB9_3:                                # %for.cond1
                                        #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	block   	
	loop    	                # label21:
	i32.const	1024
	local.set	13
	local.get	3
	i32.load	4
	local.set	14
	local.get	14
	local.set	15
	local.get	13
	local.set	16
	local.get	15
	local.get	16
	i32.lt_s
	local.set	17
	i32.const	1
	local.set	18
	local.get	17
	local.get	18
	i32.and 
	local.set	19
	local.get	19
	i32.eqz
	br_if   	1               # 1: down to label20
# %bb.4:                                # %for.body3
                                        #   in Loop: Header=BB9_3 Depth=2
	local.get	3
	i32.load	12
	local.set	20
	local.get	3
	i32.load	8
	local.set	21
	i32.const	12
	local.set	22
	local.get	21
	local.get	22
	i32.shl 
	local.set	23
	global.get	G@GOT
	local.set	24
	local.get	24
	local.get	23
	i32.add 
	local.set	25
	local.get	3
	i32.load	4
	local.set	26
	i32.const	2
	local.set	27
	local.get	26
	local.get	27
	i32.shl 
	local.set	28
	local.get	25
	local.get	28
	i32.add 
	local.set	29
	local.get	29
	local.get	20
	i32.store	0
# %bb.5:                                # %for.inc
                                        #   in Loop: Header=BB9_3 Depth=2
	local.get	3
	i32.load	4
	local.set	30
	i32.const	1
	local.set	31
	local.get	30
	local.get	31
	i32.add 
	local.set	32
	local.get	3
	local.get	32
	i32.store	4
	br      	0               # 0: up to label21
.LBB9_6:                                # %for.end
                                        #   in Loop: Header=BB9_1 Depth=1
	end_loop
	end_block                       # label20:
# %bb.7:                                # %for.inc5
                                        #   in Loop: Header=BB9_1 Depth=1
	local.get	3
	i32.load	8
	local.set	33
	i32.const	1
	local.set	34
	local.get	33
	local.get	34
	i32.add 
	local.set	35
	local.get	3
	local.get	35
	i32.store	8
	br      	0               # 0: up to label19
.LBB9_8:                                # %for.end7
	end_loop
	end_block                       # label18:
	return
	end_function
.Lfunc_end9:
	.size	_Z8example8i, .Lfunc_end9-_Z8example8i
                                        # -- End function
	.section	.text._Z8example9Pj,"",@
	.globl	_Z8example9Pj           # -- Begin function _Z8example9Pj
	.type	_Z8example9Pj,@function
_Z8example9Pj:                          # @_Z8example9Pj
	.functype	_Z8example9Pj (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	local.get	4
	i32.store	4
	local.get	3
	local.get	4
	i32.store	8
.LBB10_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label23:
	i32.const	1024
	local.set	5
	local.get	3
	i32.load	8
	local.set	6
	local.get	6
	local.set	7
	local.get	5
	local.set	8
	local.get	7
	local.get	8
	i32.lt_s
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	i32.eqz
	br_if   	1               # 1: down to label22
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB10_1 Depth=1
	local.get	3
	i32.load	8
	local.set	12
	i32.const	2
	local.set	13
	local.get	12
	local.get	13
	i32.shl 
	local.set	14
	global.get	ub@GOT
	local.set	15
	local.get	15
	local.get	14
	i32.add 
	local.set	16
	local.get	16
	i32.load	0
	local.set	17
	global.get	uc@GOT
	local.set	18
	local.get	18
	local.get	14
	i32.add 
	local.set	19
	local.get	19
	i32.load	0
	local.set	20
	local.get	17
	local.get	20
	i32.sub 
	local.set	21
	local.get	3
	i32.load	4
	local.set	22
	local.get	22
	local.get	21
	i32.add 
	local.set	23
	local.get	3
	local.get	23
	i32.store	4
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB10_1 Depth=1
	local.get	3
	i32.load	8
	local.set	24
	i32.const	1
	local.set	25
	local.get	24
	local.get	25
	i32.add 
	local.set	26
	local.get	3
	local.get	26
	i32.store	8
	br      	0               # 0: up to label23
.LBB10_4:                               # %for.end
	end_loop
	end_block                       # label22:
	local.get	3
	i32.load	4
	local.set	27
	local.get	3
	i32.load	12
	local.set	28
	local.get	28
	local.get	27
	i32.store	0
	return
	end_function
.Lfunc_end10:
	.size	_Z8example9Pj, .Lfunc_end10-_Z8example9Pj
                                        # -- End function
	.section	.text._Z10example10aPsS_S_PiS0_S0_,"",@
	.globl	_Z10example10aPsS_S_PiS0_S0_ # -- Begin function _Z10example10aPsS_S_PiS0_S0_
	.type	_Z10example10aPsS_S_PiS0_S0_,@function
_Z10example10aPsS_S_PiS0_S0_:           # @_Z10example10aPsS_S_PiS0_S0_
	.functype	_Z10example10aPsS_S_PiS0_S0_ (i32, i32, i32, i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	6
	i32.const	32
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	i32.const	0
	local.set	9
	local.get	8
	local.get	0
	i32.store	28
	local.get	8
	local.get	1
	i32.store	24
	local.get	8
	local.get	2
	i32.store	20
	local.get	8
	local.get	3
	i32.store	16
	local.get	8
	local.get	4
	i32.store	12
	local.get	8
	local.get	5
	i32.store	8
	local.get	8
	local.get	9
	i32.store	4
.LBB11_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label25:
	i32.const	1024
	local.set	10
	local.get	8
	i32.load	4
	local.set	11
	local.get	11
	local.set	12
	local.get	10
	local.set	13
	local.get	12
	local.get	13
	i32.lt_s
	local.set	14
	i32.const	1
	local.set	15
	local.get	14
	local.get	15
	i32.and 
	local.set	16
	local.get	16
	i32.eqz
	br_if   	1               # 1: down to label24
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB11_1 Depth=1
	local.get	8
	i32.load	12
	local.set	17
	local.get	8
	i32.load	4
	local.set	18
	i32.const	2
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	17
	local.get	20
	i32.add 
	local.set	21
	local.get	21
	i32.load	0
	local.set	22
	local.get	8
	i32.load	8
	local.set	23
	local.get	8
	i32.load	4
	local.set	24
	i32.const	2
	local.set	25
	local.get	24
	local.get	25
	i32.shl 
	local.set	26
	local.get	23
	local.get	26
	i32.add 
	local.set	27
	local.get	27
	i32.load	0
	local.set	28
	local.get	22
	local.get	28
	i32.add 
	local.set	29
	local.get	8
	i32.load	16
	local.set	30
	local.get	8
	i32.load	4
	local.set	31
	i32.const	2
	local.set	32
	local.get	31
	local.get	32
	i32.shl 
	local.set	33
	local.get	30
	local.get	33
	i32.add 
	local.set	34
	local.get	34
	local.get	29
	i32.store	0
	local.get	8
	i32.load	24
	local.set	35
	local.get	8
	i32.load	4
	local.set	36
	i32.const	1
	local.set	37
	local.get	36
	local.get	37
	i32.shl 
	local.set	38
	local.get	35
	local.get	38
	i32.add 
	local.set	39
	local.get	39
	i32.load16_u	0
	local.set	40
	i32.const	16
	local.set	41
	local.get	40
	local.get	41
	i32.shl 
	local.set	42
	local.get	42
	local.get	41
	i32.shr_s
	local.set	43
	local.get	8
	i32.load	20
	local.set	44
	local.get	8
	i32.load	4
	local.set	45
	i32.const	1
	local.set	46
	local.get	45
	local.get	46
	i32.shl 
	local.set	47
	local.get	44
	local.get	47
	i32.add 
	local.set	48
	local.get	48
	i32.load16_u	0
	local.set	49
	i32.const	16
	local.set	50
	local.get	49
	local.get	50
	i32.shl 
	local.set	51
	local.get	51
	local.get	50
	i32.shr_s
	local.set	52
	local.get	43
	local.get	52
	i32.add 
	local.set	53
	local.get	8
	i32.load	28
	local.set	54
	local.get	8
	i32.load	4
	local.set	55
	i32.const	1
	local.set	56
	local.get	55
	local.get	56
	i32.shl 
	local.set	57
	local.get	54
	local.get	57
	i32.add 
	local.set	58
	local.get	58
	local.get	53
	i32.store16	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB11_1 Depth=1
	local.get	8
	i32.load	4
	local.set	59
	i32.const	1
	local.set	60
	local.get	59
	local.get	60
	i32.add 
	local.set	61
	local.get	8
	local.get	61
	i32.store	4
	br      	0               # 0: up to label25
.LBB11_4:                               # %for.end
	end_loop
	end_block                       # label24:
	return
	end_function
.Lfunc_end11:
	.size	_Z10example10aPsS_S_PiS0_S0_, .Lfunc_end11-_Z10example10aPsS_S_PiS0_S0_
                                        # -- End function
	.section	.text._Z10example10bPsS_S_PiS0_S0_,"",@
	.globl	_Z10example10bPsS_S_PiS0_S0_ # -- Begin function _Z10example10bPsS_S_PiS0_S0_
	.type	_Z10example10bPsS_S_PiS0_S0_,@function
_Z10example10bPsS_S_PiS0_S0_:           # @_Z10example10bPsS_S_PiS0_S0_
	.functype	_Z10example10bPsS_S_PiS0_S0_ (i32, i32, i32, i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	6
	i32.const	32
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	i32.const	0
	local.set	9
	local.get	8
	local.get	0
	i32.store	28
	local.get	8
	local.get	1
	i32.store	24
	local.get	8
	local.get	2
	i32.store	20
	local.get	8
	local.get	3
	i32.store	16
	local.get	8
	local.get	4
	i32.store	12
	local.get	8
	local.get	5
	i32.store	8
	local.get	8
	local.get	9
	i32.store	4
.LBB12_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label27:
	i32.const	1024
	local.set	10
	local.get	8
	i32.load	4
	local.set	11
	local.get	11
	local.set	12
	local.get	10
	local.set	13
	local.get	12
	local.get	13
	i32.lt_s
	local.set	14
	i32.const	1
	local.set	15
	local.get	14
	local.get	15
	i32.and 
	local.set	16
	local.get	16
	i32.eqz
	br_if   	1               # 1: down to label26
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB12_1 Depth=1
	local.get	8
	i32.load	24
	local.set	17
	local.get	8
	i32.load	4
	local.set	18
	i32.const	1
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	17
	local.get	20
	i32.add 
	local.set	21
	local.get	21
	i32.load16_u	0
	local.set	22
	i32.const	16
	local.set	23
	local.get	22
	local.get	23
	i32.shl 
	local.set	24
	local.get	24
	local.get	23
	i32.shr_s
	local.set	25
	local.get	8
	i32.load	16
	local.set	26
	local.get	8
	i32.load	4
	local.set	27
	i32.const	2
	local.set	28
	local.get	27
	local.get	28
	i32.shl 
	local.set	29
	local.get	26
	local.get	29
	i32.add 
	local.set	30
	local.get	30
	local.get	25
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB12_1 Depth=1
	local.get	8
	i32.load	4
	local.set	31
	i32.const	1
	local.set	32
	local.get	31
	local.get	32
	i32.add 
	local.set	33
	local.get	8
	local.get	33
	i32.store	4
	br      	0               # 0: up to label27
.LBB12_4:                               # %for.end
	end_loop
	end_block                       # label26:
	return
	end_function
.Lfunc_end12:
	.size	_Z10example10bPsS_S_PiS0_S0_, .Lfunc_end12-_Z10example10bPsS_S_PiS0_S0_
                                        # -- End function
	.section	.text._Z9example11v,"",@
	.globl	_Z9example11v           # -- Begin function _Z9example11v
	.type	_Z9example11v,@function
_Z9example11v:                          # @_Z9example11v
	.functype	_Z9example11v () -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	0
	i32.const	16
	local.set	1
	local.get	0
	local.get	1
	i32.sub 
	local.set	2
	i32.const	0
	local.set	3
	local.get	2
	local.get	3
	i32.store	12
.LBB13_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label29:
	i32.const	512
	local.set	4
	local.get	2
	i32.load	12
	local.set	5
	local.get	5
	local.set	6
	local.get	4
	local.set	7
	local.get	6
	local.get	7
	i32.lt_s
	local.set	8
	i32.const	1
	local.set	9
	local.get	8
	local.get	9
	i32.and 
	local.set	10
	local.get	10
	i32.eqz
	br_if   	1               # 1: down to label28
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB13_1 Depth=1
	local.get	2
	i32.load	12
	local.set	11
	i32.const	3
	local.set	12
	local.get	11
	local.get	12
	i32.shl 
	local.set	13
	i32.const	4
	local.set	14
	local.get	13
	local.get	14
	i32.or  
	local.set	15
	global.get	b@GOT
	local.set	16
	local.get	16
	local.get	15
	i32.add 
	local.set	17
	local.get	17
	i32.load	0
	local.set	18
	global.get	c@GOT
	local.set	19
	local.get	19
	local.get	15
	i32.add 
	local.set	20
	local.get	20
	i32.load	0
	local.set	21
	local.get	18
	local.get	21
	i32.mul 
	local.set	22
	local.get	16
	local.get	13
	i32.add 
	local.set	23
	local.get	23
	i32.load	0
	local.set	24
	local.get	19
	local.get	13
	i32.add 
	local.set	25
	local.get	25
	i32.load	0
	local.set	26
	local.get	24
	local.get	26
	i32.mul 
	local.set	27
	local.get	22
	local.get	27
	i32.sub 
	local.set	28
	i32.const	2
	local.set	29
	local.get	11
	local.get	29
	i32.shl 
	local.set	30
	global.get	a@GOT
	local.set	31
	local.get	31
	local.get	30
	i32.add 
	local.set	32
	local.get	32
	local.get	28
	i32.store	0
	local.get	2
	i32.load	12
	local.set	33
	local.get	33
	local.get	12
	i32.shl 
	local.set	34
	local.get	16
	local.get	34
	i32.add 
	local.set	35
	local.get	35
	i32.load	0
	local.set	36
	local.get	34
	local.get	14
	i32.or  
	local.set	37
	local.get	19
	local.get	37
	i32.add 
	local.set	38
	local.get	38
	i32.load	0
	local.set	39
	local.get	36
	local.get	39
	i32.mul 
	local.set	40
	local.get	16
	local.get	37
	i32.add 
	local.set	41
	local.get	41
	i32.load	0
	local.set	42
	local.get	19
	local.get	34
	i32.add 
	local.set	43
	local.get	43
	i32.load	0
	local.set	44
	local.get	42
	local.get	44
	i32.mul 
	local.set	45
	local.get	40
	local.get	45
	i32.add 
	local.set	46
	local.get	33
	local.get	29
	i32.shl 
	local.set	47
	global.get	d@GOT
	local.set	48
	local.get	48
	local.get	47
	i32.add 
	local.set	49
	local.get	49
	local.get	46
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB13_1 Depth=1
	local.get	2
	i32.load	12
	local.set	50
	i32.const	1
	local.set	51
	local.get	50
	local.get	51
	i32.add 
	local.set	52
	local.get	2
	local.get	52
	i32.store	12
	br      	0               # 0: up to label29
.LBB13_4:                               # %for.end
	end_loop
	end_block                       # label28:
	return
	end_function
.Lfunc_end13:
	.size	_Z9example11v, .Lfunc_end13-_Z9example11v
                                        # -- End function
	.section	.text._Z9example12v,"",@
	.globl	_Z9example12v           # -- Begin function _Z9example12v
	.type	_Z9example12v,@function
_Z9example12v:                          # @_Z9example12v
	.functype	_Z9example12v () -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	0
	i32.const	16
	local.set	1
	local.get	0
	local.get	1
	i32.sub 
	local.set	2
	i32.const	0
	local.set	3
	local.get	2
	local.get	3
	i32.store	12
.LBB14_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label31:
	i32.const	1024
	local.set	4
	local.get	2
	i32.load	12
	local.set	5
	local.get	5
	local.set	6
	local.get	4
	local.set	7
	local.get	6
	local.get	7
	i32.lt_s
	local.set	8
	i32.const	1
	local.set	9
	local.get	8
	local.get	9
	i32.and 
	local.set	10
	local.get	10
	i32.eqz
	br_if   	1               # 1: down to label30
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB14_1 Depth=1
	local.get	2
	i32.load	12
	local.set	11
	i32.const	2
	local.set	12
	local.get	11
	local.get	12
	i32.shl 
	local.set	13
	global.get	a@GOT
	local.set	14
	local.get	14
	local.get	13
	i32.add 
	local.set	15
	local.get	15
	local.get	11
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB14_1 Depth=1
	local.get	2
	i32.load	12
	local.set	16
	i32.const	1
	local.set	17
	local.get	16
	local.get	17
	i32.add 
	local.set	18
	local.get	2
	local.get	18
	i32.store	12
	br      	0               # 0: up to label31
.LBB14_4:                               # %for.end
	end_loop
	end_block                       # label30:
	return
	end_function
.Lfunc_end14:
	.size	_Z9example12v, .Lfunc_end14-_Z9example12v
                                        # -- End function
	.section	.text._Z9example13PPiS0_S_,"",@
	.globl	_Z9example13PPiS0_S_    # -- Begin function _Z9example13PPiS0_S_
	.type	_Z9example13PPiS0_S_,@function
_Z9example13PPiS0_S_:                   # @_Z9example13PPiS0_S_
	.functype	_Z9example13PPiS0_S_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	i32.const	0
	local.set	6
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store	20
	local.get	5
	local.get	6
	i32.store	16
.LBB15_1:                               # %for.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_3 Depth 2
	block   	
	loop    	                # label33:
	i32.const	32
	local.set	7
	local.get	5
	i32.load	16
	local.set	8
	local.get	8
	local.set	9
	local.get	7
	local.set	10
	local.get	9
	local.get	10
	i32.lt_s
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	local.get	13
	i32.eqz
	br_if   	1               # 1: down to label32
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB15_1 Depth=1
	i32.const	0
	local.set	14
	local.get	5
	local.get	14
	i32.store	8
	local.get	5
	local.get	14
	i32.store	12
.LBB15_3:                               # %for.cond1
                                        #   Parent Loop BB15_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	block   	
	loop    	                # label35:
	i32.const	1024
	local.set	15
	local.get	5
	i32.load	12
	local.set	16
	local.get	16
	local.set	17
	local.get	15
	local.set	18
	local.get	17
	local.get	18
	i32.lt_s
	local.set	19
	i32.const	1
	local.set	20
	local.get	19
	local.get	20
	i32.and 
	local.set	21
	local.get	21
	i32.eqz
	br_if   	1               # 1: down to label34
# %bb.4:                                # %for.body3
                                        #   in Loop: Header=BB15_3 Depth=2
	local.get	5
	i32.load	28
	local.set	22
	local.get	5
	i32.load	16
	local.set	23
	i32.const	2
	local.set	24
	local.get	23
	local.get	24
	i32.shl 
	local.set	25
	local.get	22
	local.get	25
	i32.add 
	local.set	26
	local.get	26
	i32.load	0
	local.set	27
	local.get	5
	i32.load	12
	local.set	28
	i32.const	2
	local.set	29
	local.get	28
	local.get	29
	i32.shl 
	local.set	30
	local.get	27
	local.get	30
	i32.add 
	local.set	31
	local.get	31
	i32.load	0
	local.set	32
	local.get	5
	i32.load	24
	local.set	33
	local.get	5
	i32.load	16
	local.set	34
	i32.const	2
	local.set	35
	local.get	34
	local.get	35
	i32.shl 
	local.set	36
	local.get	33
	local.get	36
	i32.add 
	local.set	37
	local.get	37
	i32.load	0
	local.set	38
	local.get	5
	i32.load	12
	local.set	39
	i32.const	2
	local.set	40
	local.get	39
	local.get	40
	i32.shl 
	local.set	41
	local.get	38
	local.get	41
	i32.add 
	local.set	42
	local.get	42
	i32.load	0
	local.set	43
	local.get	32
	local.get	43
	i32.sub 
	local.set	44
	local.get	5
	i32.load	8
	local.set	45
	local.get	45
	local.get	44
	i32.add 
	local.set	46
	local.get	5
	local.get	46
	i32.store	8
# %bb.5:                                # %for.inc
                                        #   in Loop: Header=BB15_3 Depth=2
	local.get	5
	i32.load	12
	local.set	47
	i32.const	8
	local.set	48
	local.get	47
	local.get	48
	i32.add 
	local.set	49
	local.get	5
	local.get	49
	i32.store	12
	br      	0               # 0: up to label35
.LBB15_6:                               # %for.end
                                        #   in Loop: Header=BB15_1 Depth=1
	end_loop
	end_block                       # label34:
	local.get	5
	i32.load	8
	local.set	50
	local.get	5
	i32.load	20
	local.set	51
	local.get	5
	i32.load	16
	local.set	52
	i32.const	2
	local.set	53
	local.get	52
	local.get	53
	i32.shl 
	local.set	54
	local.get	51
	local.get	54
	i32.add 
	local.set	55
	local.get	55
	local.get	50
	i32.store	0
# %bb.7:                                # %for.inc9
                                        #   in Loop: Header=BB15_1 Depth=1
	local.get	5
	i32.load	16
	local.set	56
	i32.const	1
	local.set	57
	local.get	56
	local.get	57
	i32.add 
	local.set	58
	local.get	5
	local.get	58
	i32.store	16
	br      	0               # 0: up to label33
.LBB15_8:                               # %for.end10
	end_loop
	end_block                       # label32:
	return
	end_function
.Lfunc_end15:
	.size	_Z9example13PPiS0_S_, .Lfunc_end15-_Z9example13PPiS0_S_
                                        # -- End function
	.section	.text._Z9example14PPiS0_S_,"",@
	.globl	_Z9example14PPiS0_S_    # -- Begin function _Z9example14PPiS0_S_
	.type	_Z9example14PPiS0_S_,@function
_Z9example14PPiS0_S_:                   # @_Z9example14PPiS0_S_
	.functype	_Z9example14PPiS0_S_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	i32.const	0
	local.set	6
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store	20
	local.get	5
	local.get	6
	i32.store	8
	local.get	5
	local.get	6
	i32.store	16
.LBB16_1:                               # %for.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_3 Depth 2
                                        #       Child Loop BB16_5 Depth 3
	block   	
	loop    	                # label37:
	i32.const	4
	local.set	7
	local.get	5
	i32.load	16
	local.set	8
	local.get	8
	local.set	9
	local.get	7
	local.set	10
	local.get	9
	local.get	10
	i32.lt_s
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	local.get	13
	i32.eqz
	br_if   	1               # 1: down to label36
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB16_1 Depth=1
	i32.const	0
	local.set	14
	local.get	5
	local.get	14
	i32.store	4
	local.get	5
	local.get	14
	i32.store	12
.LBB16_3:                               # %for.cond1
                                        #   Parent Loop BB16_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_5 Depth 3
	block   	
	loop    	                # label39:
	i32.const	32
	local.set	15
	local.get	5
	i32.load	12
	local.set	16
	local.get	16
	local.set	17
	local.get	15
	local.set	18
	local.get	17
	local.get	18
	i32.lt_s
	local.set	19
	i32.const	1
	local.set	20
	local.get	19
	local.get	20
	i32.and 
	local.set	21
	local.get	21
	i32.eqz
	br_if   	1               # 1: down to label38
# %bb.4:                                # %for.body3
                                        #   in Loop: Header=BB16_3 Depth=2
	i32.const	0
	local.set	22
	local.get	5
	local.get	22
	i32.store	8
.LBB16_5:                               # %for.cond4
                                        #   Parent Loop BB16_1 Depth=1
                                        #     Parent Loop BB16_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	block   	
	loop    	                # label41:
	i32.const	1024
	local.set	23
	local.get	5
	i32.load	8
	local.set	24
	local.get	24
	local.set	25
	local.get	23
	local.set	26
	local.get	25
	local.get	26
	i32.lt_s
	local.set	27
	i32.const	1
	local.set	28
	local.get	27
	local.get	28
	i32.and 
	local.set	29
	local.get	29
	i32.eqz
	br_if   	1               # 1: down to label40
# %bb.6:                                # %for.body6
                                        #   in Loop: Header=BB16_5 Depth=3
	local.get	5
	i32.load	28
	local.set	30
	local.get	5
	i32.load	8
	local.set	31
	local.get	5
	i32.load	16
	local.set	32
	local.get	31
	local.get	32
	i32.add 
	local.set	33
	i32.const	2
	local.set	34
	local.get	33
	local.get	34
	i32.shl 
	local.set	35
	local.get	30
	local.get	35
	i32.add 
	local.set	36
	local.get	36
	i32.load	0
	local.set	37
	local.get	5
	i32.load	12
	local.set	38
	i32.const	2
	local.set	39
	local.get	38
	local.get	39
	i32.shl 
	local.set	40
	local.get	37
	local.get	40
	i32.add 
	local.set	41
	local.get	41
	i32.load	0
	local.set	42
	local.get	5
	i32.load	24
	local.set	43
	local.get	5
	i32.load	8
	local.set	44
	i32.const	2
	local.set	45
	local.get	44
	local.get	45
	i32.shl 
	local.set	46
	local.get	43
	local.get	46
	i32.add 
	local.set	47
	local.get	47
	i32.load	0
	local.set	48
	local.get	5
	i32.load	12
	local.set	49
	i32.const	2
	local.set	50
	local.get	49
	local.get	50
	i32.shl 
	local.set	51
	local.get	48
	local.get	51
	i32.add 
	local.set	52
	local.get	52
	i32.load	0
	local.set	53
	local.get	42
	local.get	53
	i32.mul 
	local.set	54
	local.get	5
	i32.load	4
	local.set	55
	local.get	55
	local.get	54
	i32.add 
	local.set	56
	local.get	5
	local.get	56
	i32.store	4
# %bb.7:                                # %for.inc
                                        #   in Loop: Header=BB16_5 Depth=3
	local.get	5
	i32.load	8
	local.set	57
	i32.const	1
	local.set	58
	local.get	57
	local.get	58
	i32.add 
	local.set	59
	local.get	5
	local.get	59
	i32.store	8
	br      	0               # 0: up to label41
.LBB16_8:                               # %for.end
                                        #   in Loop: Header=BB16_3 Depth=2
	end_loop
	end_block                       # label40:
# %bb.9:                                # %for.inc11
                                        #   in Loop: Header=BB16_3 Depth=2
	local.get	5
	i32.load	12
	local.set	60
	i32.const	1
	local.set	61
	local.get	60
	local.get	61
	i32.add 
	local.set	62
	local.get	5
	local.get	62
	i32.store	12
	br      	0               # 0: up to label39
.LBB16_10:                              # %for.end13
                                        #   in Loop: Header=BB16_1 Depth=1
	end_loop
	end_block                       # label38:
	local.get	5
	i32.load	4
	local.set	63
	local.get	5
	i32.load	20
	local.set	64
	local.get	5
	i32.load	16
	local.set	65
	i32.const	2
	local.set	66
	local.get	65
	local.get	66
	i32.shl 
	local.set	67
	local.get	64
	local.get	67
	i32.add 
	local.set	68
	local.get	68
	local.get	63
	i32.store	0
# %bb.11:                               # %for.inc15
                                        #   in Loop: Header=BB16_1 Depth=1
	local.get	5
	i32.load	16
	local.set	69
	i32.const	1
	local.set	70
	local.get	69
	local.get	70
	i32.add 
	local.set	71
	local.get	5
	local.get	71
	i32.store	16
	br      	0               # 0: up to label37
.LBB16_12:                              # %for.end17
	end_loop
	end_block                       # label36:
	return
	end_function
.Lfunc_end16:
	.size	_Z9example14PPiS0_S_, .Lfunc_end16-_Z9example14PPiS0_S_
                                        # -- End function
	.section	.text._Z9example21Pii,"",@
	.globl	_Z9example21Pii         # -- Begin function _Z9example21Pii
	.type	_Z9example21Pii,@function
_Z9example21Pii:                        # @_Z9example21Pii
	.functype	_Z9example21Pii (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	0
	local.get	4
	i32.load	8
	local.set	6
	i32.const	1
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	local.get	4
	local.get	8
	i32.store	4
.LBB17_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label43:
	i32.const	0
	local.set	9
	local.get	4
	i32.load	4
	local.set	10
	local.get	10
	local.set	11
	local.get	9
	local.set	12
	local.get	11
	local.get	12
	i32.ge_s
	local.set	13
	i32.const	1
	local.set	14
	local.get	13
	local.get	14
	i32.and 
	local.set	15
	local.get	15
	i32.eqz
	br_if   	1               # 1: down to label42
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB17_1 Depth=1
	local.get	4
	i32.load	12
	local.set	16
	local.get	4
	i32.load	4
	local.set	17
	i32.const	2
	local.set	18
	local.get	17
	local.get	18
	i32.shl 
	local.set	19
	local.get	16
	local.get	19
	i32.add 
	local.set	20
	local.get	20
	i32.load	0
	local.set	21
	local.get	4
	i32.load	0
	local.set	22
	local.get	22
	local.get	21
	i32.add 
	local.set	23
	local.get	4
	local.get	23
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB17_1 Depth=1
	local.get	4
	i32.load	4
	local.set	24
	i32.const	-1
	local.set	25
	local.get	24
	local.get	25
	i32.add 
	local.set	26
	local.get	4
	local.get	26
	i32.store	4
	br      	0               # 0: up to label43
.LBB17_4:                               # %for.end
	end_loop
	end_block                       # label42:
	local.get	4
	i32.load	0
	local.set	27
	local.get	4
	i32.load	12
	local.set	28
	local.get	28
	local.get	27
	i32.store	0
	return
	end_function
.Lfunc_end17:
	.size	_Z9example21Pii, .Lfunc_end17-_Z9example21Pii
                                        # -- End function
	.section	.text._Z9example23PtPj,"",@
	.globl	_Z9example23PtPj        # -- Begin function _Z9example23PtPj
	.type	_Z9example23PtPj,@function
_Z9example23PtPj:                       # @_Z9example23PtPj
	.functype	_Z9example23PtPj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	4
.LBB18_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label45:
	i32.const	256
	local.set	6
	local.get	4
	i32.load	4
	local.set	7
	local.get	7
	local.set	8
	local.get	6
	local.set	9
	local.get	8
	local.get	9
	i32.lt_s
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label44
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB18_1 Depth=1
	local.get	4
	i32.load	12
	local.set	13
	i32.const	2
	local.set	14
	local.get	13
	local.get	14
	i32.add 
	local.set	15
	local.get	4
	local.get	15
	i32.store	12
	local.get	13
	i32.load16_u	0
	local.set	16
	i32.const	65535
	local.set	17
	local.get	16
	local.get	17
	i32.and 
	local.set	18
	i32.const	7
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	4
	i32.load	8
	local.set	21
	i32.const	4
	local.set	22
	local.get	21
	local.get	22
	i32.add 
	local.set	23
	local.get	4
	local.get	23
	i32.store	8
	local.get	21
	local.get	20
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB18_1 Depth=1
	local.get	4
	i32.load	4
	local.set	24
	i32.const	1
	local.set	25
	local.get	24
	local.get	25
	i32.add 
	local.set	26
	local.get	4
	local.get	26
	i32.store	4
	br      	0               # 0: up to label45
.LBB18_4:                               # %for.end
	end_loop
	end_block                       # label44:
	return
	end_function
.Lfunc_end18:
	.size	_Z9example23PtPj, .Lfunc_end18-_Z9example23PtPj
                                        # -- End function
	.section	.text._Z9example24ss,"",@
	.globl	_Z9example24ss          # -- Begin function _Z9example24ss
	.type	_Z9example24ss,@function
_Z9example24ss:                         # @_Z9example24ss
	.functype	_Z9example24ss (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, f32, i32, i32, f32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store16	14
	local.get	4
	local.get	1
	i32.store16	12
	local.get	4
	local.get	5
	i32.store	8
.LBB19_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label47:
	i32.const	1024
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	7
	local.set	8
	local.get	6
	local.set	9
	local.get	8
	local.get	9
	i32.lt_s
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label46
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB19_1 Depth=1
	local.get	4
	i32.load	8
	local.set	13
	i32.const	2
	local.set	14
	local.get	13
	local.get	14
	i32.shl 
	local.set	15
	global.get	fa@GOT
	local.set	16
	local.get	16
	local.get	15
	i32.add 
	local.set	17
	local.get	17
	f32.load	0
	local.set	18
	global.get	fb@GOT
	local.set	19
	local.get	19
	local.get	15
	i32.add 
	local.set	20
	local.get	20
	f32.load	0
	local.set	21
	local.get	18
	local.get	21
	f32.lt  
	local.set	22
	i32.const	1
	local.set	23
	local.get	22
	local.get	23
	i32.and 
	local.set	24
	block   	
	block   	
	local.get	24
	i32.eqz
	br_if   	0               # 0: down to label49
# %bb.3:                                # %cond.true
                                        #   in Loop: Header=BB19_1 Depth=1
	local.get	4
	i32.load16_u	14
	local.set	25
	local.get	25
	local.set	26
	br      	1               # 1: down to label48
.LBB19_4:                               # %cond.false
                                        #   in Loop: Header=BB19_1 Depth=1
	end_block                       # label49:
	local.get	4
	i32.load16_u	12
	local.set	27
	local.get	27
	local.set	26
.LBB19_5:                               # %cond.end
                                        #   in Loop: Header=BB19_1 Depth=1
	end_block                       # label48:
	local.get	26
	local.set	28
	i32.const	16
	local.set	29
	local.get	28
	local.get	29
	i32.shl 
	local.set	30
	local.get	30
	local.get	29
	i32.shr_s
	local.set	31
	local.get	4
	i32.load	8
	local.set	32
	i32.const	2
	local.set	33
	local.get	32
	local.get	33
	i32.shl 
	local.set	34
	global.get	ic@GOT
	local.set	35
	local.get	35
	local.get	34
	i32.add 
	local.set	36
	local.get	36
	local.get	31
	i32.store	0
# %bb.6:                                # %for.inc
                                        #   in Loop: Header=BB19_1 Depth=1
	local.get	4
	i32.load	8
	local.set	37
	i32.const	1
	local.set	38
	local.get	37
	local.get	38
	i32.add 
	local.set	39
	local.get	4
	local.get	39
	i32.store	8
	br      	0               # 0: up to label47
.LBB19_7:                               # %for.end
	end_loop
	end_block                       # label46:
	return
	end_function
.Lfunc_end19:
	.size	_Z9example24ss, .Lfunc_end19-_Z9example24ss
                                        # -- End function
	.section	.text._Z9example25v,"",@
	.globl	_Z9example25v           # -- Begin function _Z9example25v
	.type	_Z9example25v,@function
_Z9example25v:                          # @_Z9example25v
	.functype	_Z9example25v () -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, f32, i32, i32, f32, i32, i32, i32, i32, i32, f32, i32, i32, f32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	0
	i32.const	16
	local.set	1
	local.get	0
	local.get	1
	i32.sub 
	local.set	2
	i32.const	0
	local.set	3
	local.get	2
	local.get	3
	i32.store	12
.LBB20_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label51:
	i32.const	1024
	local.set	4
	local.get	2
	i32.load	12
	local.set	5
	local.get	5
	local.set	6
	local.get	4
	local.set	7
	local.get	6
	local.get	7
	i32.lt_s
	local.set	8
	i32.const	1
	local.set	9
	local.get	8
	local.get	9
	i32.and 
	local.set	10
	local.get	10
	i32.eqz
	br_if   	1               # 1: down to label50
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB20_1 Depth=1
	local.get	2
	i32.load	12
	local.set	11
	i32.const	2
	local.set	12
	local.get	11
	local.get	12
	i32.shl 
	local.set	13
	global.get	da@GOT
	local.set	14
	local.get	14
	local.get	13
	i32.add 
	local.set	15
	local.get	15
	f32.load	0
	local.set	16
	global.get	db@GOT
	local.set	17
	local.get	17
	local.get	13
	i32.add 
	local.set	18
	local.get	18
	f32.load	0
	local.set	19
	local.get	16
	local.get	19
	f32.lt  
	local.set	20
	local.get	2
	local.get	20
	i32.store8	11
	local.get	2
	i32.load	12
	local.set	21
	local.get	21
	local.get	12
	i32.shl 
	local.set	22
	global.get	dc@GOT
	local.set	23
	local.get	23
	local.get	22
	i32.add 
	local.set	24
	local.get	24
	f32.load	0
	local.set	25
	global.get	dd@GOT
	local.set	26
	local.get	26
	local.get	22
	i32.add 
	local.set	27
	local.get	27
	f32.load	0
	local.set	28
	local.get	25
	local.get	28
	f32.lt  
	local.set	29
	local.get	2
	local.get	29
	i32.store8	10
	local.get	2
	i32.load8_u	11
	local.set	30
	local.get	2
	i32.load8_u	10
	local.set	31
	local.get	30
	local.get	31
	i32.and 
	local.set	32
	i32.const	24
	local.set	33
	local.get	32
	local.get	33
	i32.shl 
	local.set	34
	local.get	34
	local.get	33
	i32.shr_s
	local.set	35
	local.get	2
	i32.load	12
	local.set	36
	local.get	36
	local.get	12
	i32.shl 
	local.set	37
	global.get	dj@GOT
	local.set	38
	local.get	38
	local.get	37
	i32.add 
	local.set	39
	local.get	39
	local.get	35
	i32.store	0
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB20_1 Depth=1
	local.get	2
	i32.load	12
	local.set	40
	i32.const	1
	local.set	41
	local.get	40
	local.get	41
	i32.add 
	local.set	42
	local.get	2
	local.get	42
	i32.store	12
	br      	0               # 0: up to label51
.LBB20_4:                               # %for.end
	end_loop
	end_block                       # label50:
	return
	end_function
.Lfunc_end20:
	.size	_Z9example25v, .Lfunc_end20-_Z9example25v
                                        # -- End function
	.section	.text._Z11init_memoryPvS_,"",@
	.globl	_Z11init_memoryPvS_     # -- Begin function _Z11init_memoryPvS_
	.type	_Z11init_memoryPvS_,@function
_Z11init_memoryPvS_:                    # @_Z11init_memoryPvS_
	.functype	_Z11init_memoryPvS_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	1
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store8	7
.LBB21_1:                               # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label53:
	local.get	4
	i32.load	12
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.ne  
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label52
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB21_1 Depth=1
	local.get	4
	i32.load8_u	7
	local.set	13
	i32.const	255
	local.set	14
	local.get	13
	local.get	14
	i32.and 
	local.set	15
	i32.const	7
	local.set	16
	local.get	15
	local.get	16
	i32.mul 
	local.set	17
	local.get	4
	local.get	17
	i32.store8	7
	local.get	4
	i32.load8_u	7
	local.set	18
	i32.const	255
	local.set	19
	local.get	18
	local.get	19
	i32.and 
	local.set	20
	i32.const	39
	local.set	21
	local.get	20
	local.get	21
	i32.xor 
	local.set	22
	local.get	4
	local.get	22
	i32.store8	7
	local.get	4
	i32.load8_u	7
	local.set	23
	i32.const	255
	local.set	24
	local.get	23
	local.get	24
	i32.and 
	local.set	25
	i32.const	1
	local.set	26
	local.get	25
	local.get	26
	i32.add 
	local.set	27
	local.get	4
	local.get	27
	i32.store8	7
	local.get	4
	i32.load8_u	7
	local.set	28
	local.get	4
	i32.load	12
	local.set	29
	local.get	29
	local.get	28
	i32.store8	0
	local.get	4
	i32.load	12
	local.set	30
	i32.const	1
	local.set	31
	local.get	30
	local.get	31
	i32.add 
	local.set	32
	local.get	4
	local.get	32
	i32.store	12
	br      	0               # 0: up to label53
.LBB21_3:                               # %while.end
	end_loop
	end_block                       # label52:
	return
	end_function
.Lfunc_end21:
	.size	_Z11init_memoryPvS_, .Lfunc_end21-_Z11init_memoryPvS_
                                        # -- End function
	.section	.text._Z17init_memory_floatPfS_,"",@
	.globl	_Z17init_memory_floatPfS_ # -- Begin function _Z17init_memory_floatPfS_
	.type	_Z17init_memory_floatPfS_,@function
_Z17init_memory_floatPfS_:              # @_Z17init_memory_floatPfS_
	.functype	_Z17init_memory_floatPfS_ (i32, i32) -> ()
	.local  	i32, i32, i32, f32, i32, i32, i32, i32, i32, i32, i32, f32, f64, f64, f64, f32, f32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	f32.const	0x1p0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	f32.store	4
.LBB22_1:                               # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label55:
	local.get	4
	i32.load	12
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.ne  
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label54
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB22_1 Depth=1
	local.get	4
	f32.load	4
	local.set	13
	local.get	13
	f64.promote_f32
	local.set	14
	f64.const	0x1.199999999999ap0
	local.set	15
	local.get	14
	local.get	15
	f64.mul 
	local.set	16
	local.get	16
	f32.demote_f64
	local.set	17
	local.get	4
	local.get	17
	f32.store	4
	local.get	4
	f32.load	4
	local.set	18
	local.get	4
	i32.load	12
	local.set	19
	local.get	19
	local.get	18
	f32.store	0
	local.get	4
	i32.load	12
	local.set	20
	i32.const	4
	local.set	21
	local.get	20
	local.get	21
	i32.add 
	local.set	22
	local.get	4
	local.get	22
	i32.store	12
	br      	0               # 0: up to label55
.LBB22_3:                               # %while.end
	end_loop
	end_block                       # label54:
	return
	end_function
.Lfunc_end22:
	.size	_Z17init_memory_floatPfS_, .Lfunc_end22-_Z17init_memory_floatPfS_
                                        # -- End function
	.section	.text._Z13digest_memoryPvS_,"",@
	.globl	_Z13digest_memoryPvS_   # -- Begin function _Z13digest_memoryPvS_
	.type	_Z13digest_memoryPvS_,@function
_Z13digest_memoryPvS_:                  # @_Z13digest_memoryPvS_
	.functype	_Z13digest_memoryPvS_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	i32.const	1
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	local.get	5
	i32.store	4
.LBB23_1:                               # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label57:
	local.get	4
	i32.load	12
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.ne  
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label56
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB23_1 Depth=1
	local.get	4
	i32.load	4
	local.set	13
	i32.const	3
	local.set	14
	local.get	13
	local.get	14
	i32.mul 
	local.set	15
	local.get	4
	local.get	15
	i32.store	4
	local.get	4
	i32.load	12
	local.set	16
	local.get	16
	i32.load8_u	0
	local.set	17
	i32.const	255
	local.set	18
	local.get	17
	local.get	18
	i32.and 
	local.set	19
	local.get	4
	i32.load	4
	local.set	20
	local.get	20
	local.get	19
	i32.xor 
	local.set	21
	local.get	4
	local.get	21
	i32.store	4
	local.get	4
	i32.load	4
	local.set	22
	i32.const	8
	local.set	23
	local.get	22
	local.get	23
	i32.shr_u
	local.set	24
	local.get	4
	i32.load	4
	local.set	25
	i32.const	8
	local.set	26
	local.get	25
	local.get	26
	i32.shl 
	local.set	27
	local.get	24
	local.get	27
	i32.xor 
	local.set	28
	local.get	4
	local.get	28
	i32.store	4
	local.get	4
	i32.load	12
	local.set	29
	i32.const	1
	local.set	30
	local.get	29
	local.get	30
	i32.add 
	local.set	31
	local.get	4
	local.get	31
	i32.store	12
	br      	0               # 0: up to label57
.LBB23_3:                               # %while.end
	end_loop
	end_block                       # label56:
	local.get	4
	i32.load	4
	local.set	32
	local.get	32
	return
	end_function
.Lfunc_end23:
	.size	_Z13digest_memoryPvS_, .Lfunc_end23-_Z13digest_memoryPvS_
                                        # -- End function
	.section	.text.main,"",@
	.globl	main                    # -- Begin function main
	.type	main,@function
main:                                   # @main
	.functype	main (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	608
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	262144
	local.set	5
	i32.const	0
	local.set	6
	i32.const	576
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	local.set	9
	i32.const	1
	local.set	10
	local.get	4
	local.get	6
	i32.store	604
	local.get	4
	local.get	0
	i32.store	600
	local.get	4
	local.get	1
	i32.store	596
	local.get	4
	local.get	10
	i32.store8	595
	local.get	9
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev
	drop
	local.get	4
	local.get	6
	i32.store	572
	local.get	4
	local.get	5
	i32.store	568
	global.get	ia@GOT
	local.set	11
	i32.const	4096
	local.set	12
	local.get	11
	local.get	12
	i32.add 
	local.set	13
	local.get	11
	local.get	13
	call	_Z11init_memoryPvS_
	global.get	ib@GOT
	local.set	14
	i32.const	4096
	local.set	15
	local.get	14
	local.get	15
	i32.add 
	local.set	16
	local.get	14
	local.get	16
	call	_Z11init_memoryPvS_
	global.get	ic@GOT
	local.set	17
	i32.const	4096
	local.set	18
	local.get	17
	local.get	18
	i32.add 
	local.set	19
	local.get	17
	local.get	19
	call	_Z11init_memoryPvS_
	global.get	sa@GOT
	local.set	20
	i32.const	2048
	local.set	21
	local.get	20
	local.get	21
	i32.add 
	local.set	22
	local.get	20
	local.get	22
	call	_Z11init_memoryPvS_
	global.get	sb@GOT
	local.set	23
	i32.const	2048
	local.set	24
	local.get	23
	local.get	24
	i32.add 
	local.set	25
	local.get	23
	local.get	25
	call	_Z11init_memoryPvS_
	global.get	sc@GOT
	local.set	26
	i32.const	2048
	local.set	27
	local.get	26
	local.get	27
	i32.add 
	local.set	28
	local.get	26
	local.get	28
	call	_Z11init_memoryPvS_
	global.get	a@GOT
	local.set	29
	i32.const	8192
	local.set	30
	local.get	29
	local.get	30
	i32.add 
	local.set	31
	local.get	29
	local.get	31
	call	_Z11init_memoryPvS_
	global.get	b@GOT
	local.set	32
	i32.const	8192
	local.set	33
	local.get	32
	local.get	33
	i32.add 
	local.set	34
	local.get	32
	local.get	34
	call	_Z11init_memoryPvS_
	global.get	c@GOT
	local.set	35
	i32.const	8192
	local.set	36
	local.get	35
	local.get	36
	i32.add 
	local.set	37
	local.get	35
	local.get	37
	call	_Z11init_memoryPvS_
	global.get	ua@GOT
	local.set	38
	i32.const	4096
	local.set	39
	local.get	38
	local.get	39
	i32.add 
	local.set	40
	local.get	38
	local.get	40
	call	_Z11init_memoryPvS_
	global.get	ub@GOT
	local.set	41
	i32.const	4096
	local.set	42
	local.get	41
	local.get	42
	i32.add 
	local.set	43
	local.get	41
	local.get	43
	call	_Z11init_memoryPvS_
	global.get	uc@GOT
	local.set	44
	i32.const	4096
	local.set	45
	local.get	44
	local.get	45
	i32.add 
	local.set	46
	local.get	44
	local.get	46
	call	_Z11init_memoryPvS_
	global.get	G@GOT
	local.set	47
	i32.const	4096
	local.set	48
	local.get	47
	local.get	48
	i32.add 
	local.set	49
	local.get	47
	local.get	49
	call	_Z11init_memoryPvS_
	global.get	fa@GOT
	local.set	50
	i32.const	4096
	local.set	51
	local.get	50
	local.get	51
	i32.add 
	local.set	52
	local.get	50
	local.get	52
	call	_Z17init_memory_floatPfS_
	global.get	fb@GOT
	local.set	53
	i32.const	4096
	local.set	54
	local.get	53
	local.get	54
	i32.add 
	local.set	55
	local.get	53
	local.get	55
	call	_Z17init_memory_floatPfS_
	global.get	da@GOT
	local.set	56
	i32.const	4096
	local.set	57
	local.get	56
	local.get	57
	i32.add 
	local.set	58
	local.get	56
	local.get	58
	call	_Z17init_memory_floatPfS_
	global.get	db@GOT
	local.set	59
	i32.const	4096
	local.set	60
	local.get	59
	local.get	60
	i32.add 
	local.set	61
	local.get	59
	local.get	61
	call	_Z17init_memory_floatPfS_
	global.get	dc@GOT
	local.set	62
	i32.const	4096
	local.set	63
	local.get	62
	local.get	63
	i32.add 
	local.set	64
	local.get	62
	local.get	64
	call	_Z17init_memory_floatPfS_
	global.get	dd@GOT
	local.set	65
	i32.const	4096
	local.set	66
	local.get	65
	local.get	66
	i32.add 
	local.set	67
	local.get	65
	local.get	67
	call	_Z17init_memory_floatPfS_
	call	_Z8example1v
	local.get	4
	i32.load8_u	595
	local.set	68
	i32.const	1
	local.set	69
	local.get	68
	local.get	69
	i32.and 
	local.set	70
	i32.const	.L.str@MBREL
	local.set	71
	global.get	__memory_base
	local.set	72
	local.get	72
	local.get	71
	i32.add 
	local.set	73
	i32.const	544
	local.set	74
	local.get	4
	local.get	74
	i32.add 
	local.set	75
	local.get	75
	local.get	73
	local.get	70
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	76
	i32.const	544
	local.set	77
	local.get	4
	local.get	77
	i32.add 
	local.set	78
	local.get	78
	drop
	local.get	4
	local.get	76
	i32.store	540
.LBB24_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label59:
	i32.const	2621440
	local.set	79
	local.get	4
	i32.load	540
	local.set	80
	local.get	80
	local.set	81
	local.get	79
	local.set	82
	local.get	81
	local.get	82
	i32.lt_s
	local.set	83
	i32.const	1
	local.set	84
	local.get	83
	local.get	84
	i32.and 
	local.set	85
	local.get	85
	i32.eqz
	br_if   	1               # 1: down to label58
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB24_1 Depth=1
	call	_Z8example1v
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB24_1 Depth=1
	local.get	4
	i32.load	540
	local.set	86
	i32.const	1
	local.set	87
	local.get	86
	local.get	87
	i32.add 
	local.set	88
	local.get	4
	local.get	88
	i32.store	540
	br      	0               # 0: up to label59
.LBB24_4:                               # %for.end
	end_loop
	end_block                       # label58:
	global.get	a@GOT
	local.set	89
	i32.const	1024
	local.set	90
	local.get	89
	local.get	90
	i32.add 
	local.set	91
	local.get	89
	local.get	91
	call	_Z13digest_memoryPvS_
	local.set	92
	i32.const	1024
	local.set	93
	i32.const	2
	local.set	94
	i32.const	544
	local.set	95
	local.get	4
	local.get	95
	i32.add 
	local.set	96
	local.get	96
	local.set	97
	i32.const	576
	local.set	98
	local.get	4
	local.get	98
	i32.add 
	local.set	99
	local.get	99
	local.set	100
	i32.const	536
	local.set	101
	local.get	4
	local.get	101
	i32.add 
	local.set	102
	local.get	102
	local.set	103
	local.get	4
	local.get	92
	i32.store	536
	local.get	100
	local.get	103
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	97
	call	_ZN5TimerD1Ev
	drop
	local.get	93
	local.get	94
	call	_Z9example2aii
	local.get	4
	i32.load8_u	595
	local.set	104
	i32.const	1
	local.set	105
	local.get	104
	local.get	105
	i32.and 
	local.set	106
	i32.const	.L.str.1@MBREL
	local.set	107
	global.get	__memory_base
	local.set	108
	local.get	108
	local.get	107
	i32.add 
	local.set	109
	i32.const	512
	local.set	110
	local.get	4
	local.get	110
	i32.add 
	local.set	111
	local.get	111
	local.get	109
	local.get	106
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	112
	i32.const	512
	local.set	113
	local.get	4
	local.get	113
	i32.add 
	local.set	114
	local.get	114
	drop
	local.get	4
	local.get	112
	i32.store	508
.LBB24_5:                               # %for.cond8
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label61:
	i32.const	1048576
	local.set	115
	local.get	4
	i32.load	508
	local.set	116
	local.get	116
	local.set	117
	local.get	115
	local.set	118
	local.get	117
	local.get	118
	i32.lt_s
	local.set	119
	i32.const	1
	local.set	120
	local.get	119
	local.get	120
	i32.and 
	local.set	121
	local.get	121
	i32.eqz
	br_if   	1               # 1: down to label60
# %bb.6:                                # %for.body10
                                        #   in Loop: Header=BB24_5 Depth=1
	i32.const	1024
	local.set	122
	i32.const	2
	local.set	123
	local.get	122
	local.get	123
	call	_Z9example2aii
# %bb.7:                                # %for.inc11
                                        #   in Loop: Header=BB24_5 Depth=1
	local.get	4
	i32.load	508
	local.set	124
	i32.const	1
	local.set	125
	local.get	124
	local.get	125
	i32.add 
	local.set	126
	local.get	4
	local.get	126
	i32.store	508
	br      	0               # 0: up to label61
.LBB24_8:                               # %for.end13
	end_loop
	end_block                       # label60:
	global.get	b@GOT
	local.set	127
	i32.const	4096
	local.set	128
	local.get	127
	local.get	128
	i32.add 
	local.set	129
	local.get	127
	local.get	129
	call	_Z13digest_memoryPvS_
	local.set	130
	i32.const	1024
	local.set	131
	i32.const	2
	local.set	132
	i32.const	512
	local.set	133
	local.get	4
	local.get	133
	i32.add 
	local.set	134
	local.get	134
	local.set	135
	i32.const	576
	local.set	136
	local.get	4
	local.get	136
	i32.add 
	local.set	137
	local.get	137
	local.set	138
	i32.const	504
	local.set	139
	local.get	4
	local.get	139
	i32.add 
	local.set	140
	local.get	140
	local.set	141
	local.get	4
	local.get	130
	i32.store	504
	local.get	138
	local.get	141
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	135
	call	_ZN5TimerD1Ev
	drop
	local.get	131
	local.get	132
	call	_Z9example2bii
	local.get	4
	i32.load8_u	595
	local.set	142
	i32.const	1
	local.set	143
	local.get	142
	local.get	143
	i32.and 
	local.set	144
	i32.const	.L.str.2@MBREL
	local.set	145
	global.get	__memory_base
	local.set	146
	local.get	146
	local.get	145
	i32.add 
	local.set	147
	i32.const	480
	local.set	148
	local.get	4
	local.get	148
	i32.add 
	local.set	149
	local.get	149
	local.get	147
	local.get	144
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	150
	i32.const	480
	local.set	151
	local.get	4
	local.get	151
	i32.add 
	local.set	152
	local.get	152
	drop
	local.get	4
	local.get	150
	i32.store	476
.LBB24_9:                               # %for.cond21
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label63:
	i32.const	524288
	local.set	153
	local.get	4
	i32.load	476
	local.set	154
	local.get	154
	local.set	155
	local.get	153
	local.set	156
	local.get	155
	local.get	156
	i32.lt_s
	local.set	157
	i32.const	1
	local.set	158
	local.get	157
	local.get	158
	i32.and 
	local.set	159
	local.get	159
	i32.eqz
	br_if   	1               # 1: down to label62
# %bb.10:                               # %for.body23
                                        #   in Loop: Header=BB24_9 Depth=1
	i32.const	1024
	local.set	160
	i32.const	2
	local.set	161
	local.get	160
	local.get	161
	call	_Z9example2bii
# %bb.11:                               # %for.inc24
                                        #   in Loop: Header=BB24_9 Depth=1
	local.get	4
	i32.load	476
	local.set	162
	i32.const	1
	local.set	163
	local.get	162
	local.get	163
	i32.add 
	local.set	164
	local.get	4
	local.get	164
	i32.store	476
	br      	0               # 0: up to label63
.LBB24_12:                              # %for.end26
	end_loop
	end_block                       # label62:
	global.get	a@GOT
	local.set	165
	i32.const	4096
	local.set	166
	local.get	165
	local.get	166
	i32.add 
	local.set	167
	local.get	165
	local.get	167
	call	_Z13digest_memoryPvS_
	local.set	168
	i32.const	480
	local.set	169
	local.get	4
	local.get	169
	i32.add 
	local.set	170
	local.get	170
	local.set	171
	i32.const	576
	local.set	172
	local.get	4
	local.get	172
	i32.add 
	local.set	173
	local.get	173
	local.set	174
	i32.const	472
	local.set	175
	local.get	4
	local.get	175
	i32.add 
	local.set	176
	local.get	176
	local.set	177
	local.get	4
	local.get	168
	i32.store	472
	local.get	174
	local.get	177
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	171
	call	_ZN5TimerD1Ev
	drop
	global.get	ib@GOT
	local.set	178
	global.get	ia@GOT
	local.set	179
	i32.const	1024
	local.set	180
	local.get	180
	local.get	179
	local.get	178
	call	_Z8example3iPiS_
	i32.const	1024
	drop
	local.get	4
	i32.load8_u	595
	local.set	181
	i32.const	1
	local.set	182
	local.get	181
	local.get	182
	i32.and 
	local.set	183
	i32.const	.L.str.3@MBREL
	local.set	184
	global.get	__memory_base
	local.set	185
	local.get	185
	local.get	184
	i32.add 
	local.set	186
	i32.const	448
	local.set	187
	local.get	4
	local.get	187
	i32.add 
	local.set	188
	local.get	188
	local.get	186
	local.get	183
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	189
	i32.const	448
	local.set	190
	local.get	4
	local.get	190
	i32.add 
	local.set	191
	local.get	191
	drop
	local.get	4
	local.get	189
	i32.store	444
.LBB24_13:                              # %for.cond34
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label65:
	i32.const	524288
	local.set	192
	local.get	4
	i32.load	444
	local.set	193
	local.get	193
	local.set	194
	local.get	192
	local.set	195
	local.get	194
	local.get	195
	i32.lt_s
	local.set	196
	i32.const	1
	local.set	197
	local.get	196
	local.get	197
	i32.and 
	local.set	198
	local.get	198
	i32.eqz
	br_if   	1               # 1: down to label64
# %bb.14:                               # %for.body36
                                        #   in Loop: Header=BB24_13 Depth=1
	global.get	ib@GOT
	local.set	199
	global.get	ia@GOT
	local.set	200
	i32.const	1024
	local.set	201
	local.get	201
	local.get	200
	local.get	199
	call	_Z8example3iPiS_
	i32.const	1024
	drop
# %bb.15:                               # %for.inc37
                                        #   in Loop: Header=BB24_13 Depth=1
	local.get	4
	i32.load	444
	local.set	202
	i32.const	1
	local.set	203
	local.get	202
	local.get	203
	i32.add 
	local.set	204
	local.get	4
	local.get	204
	i32.store	444
	br      	0               # 0: up to label65
.LBB24_16:                              # %for.end39
	end_loop
	end_block                       # label64:
	global.get	ia@GOT
	local.set	205
	i32.const	4096
	local.set	206
	local.get	205
	local.get	206
	i32.add 
	local.set	207
	local.get	205
	local.get	207
	call	_Z13digest_memoryPvS_
	local.set	208
	i32.const	448
	local.set	209
	local.get	4
	local.get	209
	i32.add 
	local.set	210
	local.get	210
	local.set	211
	i32.const	576
	local.set	212
	local.get	4
	local.get	212
	i32.add 
	local.set	213
	local.get	213
	local.set	214
	i32.const	440
	local.set	215
	local.get	4
	local.get	215
	i32.add 
	local.set	216
	local.get	216
	local.set	217
	local.get	4
	local.get	208
	i32.store	440
	local.get	214
	local.get	217
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	211
	call	_ZN5TimerD1Ev
	drop
	global.get	ib@GOT
	local.set	218
	global.get	ia@GOT
	local.set	219
	i32.const	1024
	local.set	220
	local.get	220
	local.get	219
	local.get	218
	call	_Z9example4aiPiS_
	i32.const	1024
	drop
	local.get	4
	i32.load8_u	595
	local.set	221
	i32.const	1
	local.set	222
	local.get	221
	local.get	222
	i32.and 
	local.set	223
	i32.const	.L.str.4@MBREL
	local.set	224
	global.get	__memory_base
	local.set	225
	local.get	225
	local.get	224
	i32.add 
	local.set	226
	i32.const	416
	local.set	227
	local.get	4
	local.get	227
	i32.add 
	local.set	228
	local.get	228
	local.get	226
	local.get	223
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	229
	i32.const	416
	local.set	230
	local.get	4
	local.get	230
	i32.add 
	local.set	231
	local.get	231
	drop
	local.get	4
	local.get	229
	i32.store	412
.LBB24_17:                              # %for.cond47
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label67:
	i32.const	524288
	local.set	232
	local.get	4
	i32.load	412
	local.set	233
	local.get	233
	local.set	234
	local.get	232
	local.set	235
	local.get	234
	local.get	235
	i32.lt_s
	local.set	236
	i32.const	1
	local.set	237
	local.get	236
	local.get	237
	i32.and 
	local.set	238
	local.get	238
	i32.eqz
	br_if   	1               # 1: down to label66
# %bb.18:                               # %for.body49
                                        #   in Loop: Header=BB24_17 Depth=1
	global.get	ib@GOT
	local.set	239
	global.get	ia@GOT
	local.set	240
	i32.const	1024
	local.set	241
	local.get	241
	local.get	240
	local.get	239
	call	_Z9example4aiPiS_
	i32.const	1024
	drop
# %bb.19:                               # %for.inc50
                                        #   in Loop: Header=BB24_17 Depth=1
	local.get	4
	i32.load	412
	local.set	242
	i32.const	1
	local.set	243
	local.get	242
	local.get	243
	i32.add 
	local.set	244
	local.get	4
	local.get	244
	i32.store	412
	br      	0               # 0: up to label67
.LBB24_20:                              # %for.end52
	end_loop
	end_block                       # label66:
	global.get	ia@GOT
	local.set	245
	i32.const	4096
	local.set	246
	local.get	245
	local.get	246
	i32.add 
	local.set	247
	local.get	245
	local.get	247
	call	_Z13digest_memoryPvS_
	local.set	248
	i32.const	416
	local.set	249
	local.get	4
	local.get	249
	i32.add 
	local.set	250
	local.get	250
	local.set	251
	i32.const	576
	local.set	252
	local.get	4
	local.get	252
	i32.add 
	local.set	253
	local.get	253
	local.set	254
	i32.const	408
	local.set	255
	local.get	4
	local.get	255
	i32.add 
	local.set	256
	local.get	256
	local.set	257
	local.get	4
	local.get	248
	i32.store	408
	local.get	254
	local.get	257
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	251
	call	_ZN5TimerD1Ev
	drop
	global.get	ib@GOT
	local.set	258
	global.get	ia@GOT
	local.set	259
	i32.const	1014
	local.set	260
	local.get	260
	local.get	259
	local.get	258
	call	_Z9example4biPiS_
	i32.const	1014
	drop
	local.get	4
	i32.load8_u	595
	local.set	261
	i32.const	1
	local.set	262
	local.get	261
	local.get	262
	i32.and 
	local.set	263
	i32.const	.L.str.5@MBREL
	local.set	264
	global.get	__memory_base
	local.set	265
	local.get	265
	local.get	264
	i32.add 
	local.set	266
	i32.const	384
	local.set	267
	local.get	4
	local.get	267
	i32.add 
	local.set	268
	local.get	268
	local.get	266
	local.get	263
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	269
	i32.const	384
	local.set	270
	local.get	4
	local.get	270
	i32.add 
	local.set	271
	local.get	271
	drop
	local.get	4
	local.get	269
	i32.store	380
.LBB24_21:                              # %for.cond60
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label69:
	i32.const	524288
	local.set	272
	local.get	4
	i32.load	380
	local.set	273
	local.get	273
	local.set	274
	local.get	272
	local.set	275
	local.get	274
	local.get	275
	i32.lt_s
	local.set	276
	i32.const	1
	local.set	277
	local.get	276
	local.get	277
	i32.and 
	local.set	278
	local.get	278
	i32.eqz
	br_if   	1               # 1: down to label68
# %bb.22:                               # %for.body62
                                        #   in Loop: Header=BB24_21 Depth=1
	global.get	ib@GOT
	local.set	279
	global.get	ia@GOT
	local.set	280
	i32.const	1014
	local.set	281
	local.get	281
	local.get	280
	local.get	279
	call	_Z9example4biPiS_
	i32.const	1014
	drop
# %bb.23:                               # %for.inc63
                                        #   in Loop: Header=BB24_21 Depth=1
	local.get	4
	i32.load	380
	local.set	282
	i32.const	1
	local.set	283
	local.get	282
	local.get	283
	i32.add 
	local.set	284
	local.get	4
	local.get	284
	i32.store	380
	br      	0               # 0: up to label69
.LBB24_24:                              # %for.end65
	end_loop
	end_block                       # label68:
	global.get	ia@GOT
	local.set	285
	i32.const	4096
	local.set	286
	local.get	285
	local.get	286
	i32.add 
	local.set	287
	local.get	285
	local.get	287
	call	_Z13digest_memoryPvS_
	local.set	288
	i32.const	384
	local.set	289
	local.get	4
	local.get	289
	i32.add 
	local.set	290
	local.get	290
	local.set	291
	i32.const	576
	local.set	292
	local.get	4
	local.get	292
	i32.add 
	local.set	293
	local.get	293
	local.set	294
	i32.const	376
	local.set	295
	local.get	4
	local.get	295
	i32.add 
	local.set	296
	local.get	296
	local.set	297
	local.get	4
	local.get	288
	i32.store	376
	local.get	294
	local.get	297
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	291
	call	_ZN5TimerD1Ev
	drop
	global.get	ib@GOT
	local.set	298
	global.get	ia@GOT
	local.set	299
	i32.const	1024
	local.set	300
	local.get	300
	local.get	299
	local.get	298
	call	_Z9example4ciPiS_
	i32.const	1024
	drop
	local.get	4
	i32.load8_u	595
	local.set	301
	i32.const	1
	local.set	302
	local.get	301
	local.get	302
	i32.and 
	local.set	303
	i32.const	.L.str.6@MBREL
	local.set	304
	global.get	__memory_base
	local.set	305
	local.get	305
	local.get	304
	i32.add 
	local.set	306
	i32.const	352
	local.set	307
	local.get	4
	local.get	307
	i32.add 
	local.set	308
	local.get	308
	local.get	306
	local.get	303
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	309
	i32.const	352
	local.set	310
	local.get	4
	local.get	310
	i32.add 
	local.set	311
	local.get	311
	drop
	local.get	4
	local.get	309
	i32.store	348
.LBB24_25:                              # %for.cond73
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label71:
	i32.const	524288
	local.set	312
	local.get	4
	i32.load	348
	local.set	313
	local.get	313
	local.set	314
	local.get	312
	local.set	315
	local.get	314
	local.get	315
	i32.lt_s
	local.set	316
	i32.const	1
	local.set	317
	local.get	316
	local.get	317
	i32.and 
	local.set	318
	local.get	318
	i32.eqz
	br_if   	1               # 1: down to label70
# %bb.26:                               # %for.body75
                                        #   in Loop: Header=BB24_25 Depth=1
	global.get	ib@GOT
	local.set	319
	global.get	ia@GOT
	local.set	320
	i32.const	1024
	local.set	321
	local.get	321
	local.get	320
	local.get	319
	call	_Z9example4ciPiS_
	i32.const	1024
	drop
# %bb.27:                               # %for.inc76
                                        #   in Loop: Header=BB24_25 Depth=1
	local.get	4
	i32.load	348
	local.set	322
	i32.const	1
	local.set	323
	local.get	322
	local.get	323
	i32.add 
	local.set	324
	local.get	4
	local.get	324
	i32.store	348
	br      	0               # 0: up to label71
.LBB24_28:                              # %for.end78
	end_loop
	end_block                       # label70:
	global.get	ib@GOT
	local.set	325
	i32.const	4096
	local.set	326
	local.get	325
	local.get	326
	i32.add 
	local.set	327
	local.get	325
	local.get	327
	call	_Z13digest_memoryPvS_
	local.set	328
	i32.const	4
	local.set	329
	i32.const	352
	local.set	330
	local.get	4
	local.get	330
	i32.add 
	local.set	331
	local.get	331
	local.set	332
	i32.const	576
	local.set	333
	local.get	4
	local.get	333
	i32.add 
	local.set	334
	local.get	334
	local.set	335
	i32.const	344
	local.set	336
	local.get	4
	local.get	336
	i32.add 
	local.set	337
	local.get	337
	local.set	338
	local.get	4
	local.get	328
	i32.store	344
	local.get	335
	local.get	338
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	332
	call	_ZN5TimerD1Ev
	drop
	local.get	329
	call	_Z8example7i
	local.get	4
	i32.load8_u	595
	local.set	339
	i32.const	1
	local.set	340
	local.get	339
	local.get	340
	i32.and 
	local.set	341
	i32.const	.L.str.7@MBREL
	local.set	342
	global.get	__memory_base
	local.set	343
	local.get	343
	local.get	342
	i32.add 
	local.set	344
	i32.const	320
	local.set	345
	local.get	4
	local.get	345
	i32.add 
	local.set	346
	local.get	346
	local.get	344
	local.get	341
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	347
	i32.const	320
	local.set	348
	local.get	4
	local.get	348
	i32.add 
	local.set	349
	local.get	349
	drop
	local.get	4
	local.get	347
	i32.store	316
.LBB24_29:                              # %for.cond86
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label73:
	i32.const	1048576
	local.set	350
	local.get	4
	i32.load	316
	local.set	351
	local.get	351
	local.set	352
	local.get	350
	local.set	353
	local.get	352
	local.get	353
	i32.lt_s
	local.set	354
	i32.const	1
	local.set	355
	local.get	354
	local.get	355
	i32.and 
	local.set	356
	local.get	356
	i32.eqz
	br_if   	1               # 1: down to label72
# %bb.30:                               # %for.body88
                                        #   in Loop: Header=BB24_29 Depth=1
	i32.const	4
	local.set	357
	local.get	357
	call	_Z8example7i
# %bb.31:                               # %for.inc89
                                        #   in Loop: Header=BB24_29 Depth=1
	local.get	4
	i32.load	316
	local.set	358
	i32.const	1
	local.set	359
	local.get	358
	local.get	359
	i32.add 
	local.set	360
	local.get	4
	local.get	360
	i32.store	316
	br      	0               # 0: up to label73
.LBB24_32:                              # %for.end91
	end_loop
	end_block                       # label72:
	global.get	a@GOT
	local.set	361
	i32.const	4096
	local.set	362
	local.get	361
	local.get	362
	i32.add 
	local.set	363
	local.get	361
	local.get	363
	call	_Z13digest_memoryPvS_
	local.set	364
	i32.const	8
	local.set	365
	i32.const	320
	local.set	366
	local.get	4
	local.get	366
	i32.add 
	local.set	367
	local.get	367
	local.set	368
	i32.const	576
	local.set	369
	local.get	4
	local.get	369
	i32.add 
	local.set	370
	local.get	370
	local.set	371
	i32.const	312
	local.set	372
	local.get	4
	local.get	372
	i32.add 
	local.set	373
	local.get	373
	local.set	374
	local.get	4
	local.get	364
	i32.store	312
	local.get	371
	local.get	374
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	368
	call	_ZN5TimerD1Ev
	drop
	local.get	365
	call	_Z8example8i
	local.get	4
	i32.load8_u	595
	local.set	375
	i32.const	1
	local.set	376
	local.get	375
	local.get	376
	i32.and 
	local.set	377
	i32.const	.L.str.8@MBREL
	local.set	378
	global.get	__memory_base
	local.set	379
	local.get	379
	local.get	378
	i32.add 
	local.set	380
	i32.const	288
	local.set	381
	local.get	4
	local.get	381
	i32.add 
	local.set	382
	local.get	382
	local.get	380
	local.get	377
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	383
	i32.const	288
	local.set	384
	local.get	4
	local.get	384
	i32.add 
	local.set	385
	local.get	385
	drop
	local.get	4
	local.get	383
	i32.store	284
.LBB24_33:                              # %for.cond99
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label75:
	i32.const	65536
	local.set	386
	local.get	4
	i32.load	284
	local.set	387
	local.get	387
	local.set	388
	local.get	386
	local.set	389
	local.get	388
	local.get	389
	i32.lt_s
	local.set	390
	i32.const	1
	local.set	391
	local.get	390
	local.get	391
	i32.and 
	local.set	392
	local.get	392
	i32.eqz
	br_if   	1               # 1: down to label74
# %bb.34:                               # %for.body101
                                        #   in Loop: Header=BB24_33 Depth=1
	i32.const	8
	local.set	393
	local.get	393
	call	_Z8example8i
# %bb.35:                               # %for.inc102
                                        #   in Loop: Header=BB24_33 Depth=1
	local.get	4
	i32.load	284
	local.set	394
	i32.const	1
	local.set	395
	local.get	394
	local.get	395
	i32.add 
	local.set	396
	local.get	4
	local.get	396
	i32.store	284
	br      	0               # 0: up to label75
.LBB24_36:                              # %for.end104
	end_loop
	end_block                       # label74:
	global.get	G@GOT
	local.set	397
	i32.const	4096
	local.set	398
	local.get	397
	local.get	398
	i32.add 
	local.set	399
	local.get	397
	local.get	399
	call	_Z13digest_memoryPvS_
	local.set	400
	i32.const	572
	local.set	401
	local.get	4
	local.get	401
	i32.add 
	local.set	402
	local.get	402
	local.set	403
	i32.const	288
	local.set	404
	local.get	4
	local.get	404
	i32.add 
	local.set	405
	local.get	405
	local.set	406
	i32.const	576
	local.set	407
	local.get	4
	local.get	407
	i32.add 
	local.set	408
	local.get	408
	local.set	409
	i32.const	280
	local.set	410
	local.get	4
	local.get	410
	i32.add 
	local.set	411
	local.get	411
	local.set	412
	local.get	4
	local.get	400
	i32.store	280
	local.get	409
	local.get	412
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	406
	call	_ZN5TimerD1Ev
	drop
	local.get	403
	call	_Z8example9Pj
	local.get	4
	i32.load8_u	595
	local.set	413
	i32.const	1
	local.set	414
	local.get	413
	local.get	414
	i32.and 
	local.set	415
	i32.const	.L.str.9@MBREL
	local.set	416
	global.get	__memory_base
	local.set	417
	local.get	417
	local.get	416
	i32.add 
	local.set	418
	i32.const	256
	local.set	419
	local.get	4
	local.get	419
	i32.add 
	local.set	420
	local.get	420
	local.get	418
	local.get	415
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	421
	i32.const	256
	local.set	422
	local.get	4
	local.get	422
	i32.add 
	local.set	423
	local.get	423
	drop
	local.get	4
	local.get	421
	i32.store	252
.LBB24_37:                              # %for.cond112
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label77:
	i32.const	524288
	local.set	424
	local.get	4
	i32.load	252
	local.set	425
	local.get	425
	local.set	426
	local.get	424
	local.set	427
	local.get	426
	local.get	427
	i32.lt_s
	local.set	428
	i32.const	1
	local.set	429
	local.get	428
	local.get	429
	i32.and 
	local.set	430
	local.get	430
	i32.eqz
	br_if   	1               # 1: down to label76
# %bb.38:                               # %for.body114
                                        #   in Loop: Header=BB24_37 Depth=1
	i32.const	572
	local.set	431
	local.get	4
	local.get	431
	i32.add 
	local.set	432
	local.get	432
	local.set	433
	local.get	433
	call	_Z8example9Pj
# %bb.39:                               # %for.inc115
                                        #   in Loop: Header=BB24_37 Depth=1
	local.get	4
	i32.load	252
	local.set	434
	i32.const	1
	local.set	435
	local.get	434
	local.get	435
	i32.add 
	local.set	436
	local.get	4
	local.get	436
	i32.store	252
	br      	0               # 0: up to label77
.LBB24_40:                              # %for.end117
	end_loop
	end_block                       # label76:
	i32.const	256
	local.set	437
	local.get	4
	local.get	437
	i32.add 
	local.set	438
	local.get	438
	local.set	439
	i32.const	576
	local.set	440
	local.get	4
	local.get	440
	i32.add 
	local.set	441
	local.get	441
	local.set	442
	i32.const	248
	local.set	443
	local.get	4
	local.get	443
	i32.add 
	local.set	444
	local.get	444
	local.set	445
	local.get	4
	i32.load	572
	local.set	446
	local.get	4
	local.get	446
	i32.store	248
	local.get	442
	local.get	445
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	439
	call	_ZN5TimerD1Ev
	drop
	global.get	ic@GOT
	local.set	447
	global.get	ib@GOT
	local.set	448
	global.get	ia@GOT
	local.set	449
	global.get	sc@GOT
	local.set	450
	global.get	sb@GOT
	local.set	451
	global.get	sa@GOT
	local.set	452
	local.get	452
	local.get	451
	local.get	450
	local.get	449
	local.get	448
	local.get	447
	call	_Z10example10aPsS_S_PiS0_S0_
	local.get	4
	i32.load8_u	595
	local.set	453
	i32.const	1
	local.set	454
	local.get	453
	local.get	454
	i32.and 
	local.set	455
	i32.const	.L.str.10@MBREL
	local.set	456
	global.get	__memory_base
	local.set	457
	local.get	457
	local.get	456
	i32.add 
	local.set	458
	i32.const	224
	local.set	459
	local.get	4
	local.get	459
	i32.add 
	local.set	460
	local.get	460
	local.get	458
	local.get	455
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	461
	i32.const	224
	local.set	462
	local.get	4
	local.get	462
	i32.add 
	local.set	463
	local.get	463
	drop
	local.get	4
	local.get	461
	i32.store	220
.LBB24_41:                              # %for.cond124
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label79:
	i32.const	524288
	local.set	464
	local.get	4
	i32.load	220
	local.set	465
	local.get	465
	local.set	466
	local.get	464
	local.set	467
	local.get	466
	local.get	467
	i32.lt_s
	local.set	468
	i32.const	1
	local.set	469
	local.get	468
	local.get	469
	i32.and 
	local.set	470
	local.get	470
	i32.eqz
	br_if   	1               # 1: down to label78
# %bb.42:                               # %for.body126
                                        #   in Loop: Header=BB24_41 Depth=1
	global.get	ic@GOT
	local.set	471
	global.get	ib@GOT
	local.set	472
	global.get	ia@GOT
	local.set	473
	global.get	sc@GOT
	local.set	474
	global.get	sb@GOT
	local.set	475
	global.get	sa@GOT
	local.set	476
	local.get	476
	local.get	475
	local.get	474
	local.get	473
	local.get	472
	local.get	471
	call	_Z10example10aPsS_S_PiS0_S0_
# %bb.43:                               # %for.inc127
                                        #   in Loop: Header=BB24_41 Depth=1
	local.get	4
	i32.load	220
	local.set	477
	i32.const	1
	local.set	478
	local.get	477
	local.get	478
	i32.add 
	local.set	479
	local.get	4
	local.get	479
	i32.store	220
	br      	0               # 0: up to label79
.LBB24_44:                              # %for.end129
	end_loop
	end_block                       # label78:
	global.get	ia@GOT
	local.set	480
	i32.const	4096
	local.set	481
	local.get	480
	local.get	481
	i32.add 
	local.set	482
	local.get	480
	local.get	482
	call	_Z13digest_memoryPvS_
	local.set	483
	global.get	sa@GOT
	local.set	484
	i32.const	2048
	local.set	485
	local.get	484
	local.get	485
	i32.add 
	local.set	486
	local.get	484
	local.get	486
	call	_Z13digest_memoryPvS_
	local.set	487
	i32.const	224
	local.set	488
	local.get	4
	local.get	488
	i32.add 
	local.set	489
	local.get	489
	local.set	490
	i32.const	576
	local.set	491
	local.get	4
	local.get	491
	i32.add 
	local.set	492
	local.get	492
	local.set	493
	i32.const	216
	local.set	494
	local.get	4
	local.get	494
	i32.add 
	local.set	495
	local.get	495
	local.set	496
	local.get	483
	local.get	487
	i32.add 
	local.set	497
	local.get	4
	local.get	497
	i32.store	216
	local.get	493
	local.get	496
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	490
	call	_ZN5TimerD1Ev
	drop
	global.get	ic@GOT
	local.set	498
	global.get	ib@GOT
	local.set	499
	global.get	ia@GOT
	local.set	500
	global.get	sc@GOT
	local.set	501
	global.get	sb@GOT
	local.set	502
	global.get	sa@GOT
	local.set	503
	local.get	503
	local.get	502
	local.get	501
	local.get	500
	local.get	499
	local.get	498
	call	_Z10example10bPsS_S_PiS0_S0_
	local.get	4
	i32.load8_u	595
	local.set	504
	i32.const	1
	local.set	505
	local.get	504
	local.get	505
	i32.and 
	local.set	506
	i32.const	.L.str.11@MBREL
	local.set	507
	global.get	__memory_base
	local.set	508
	local.get	508
	local.get	507
	i32.add 
	local.set	509
	i32.const	192
	local.set	510
	local.get	4
	local.get	510
	i32.add 
	local.set	511
	local.get	511
	local.get	509
	local.get	506
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	512
	i32.const	192
	local.set	513
	local.get	4
	local.get	513
	i32.add 
	local.set	514
	local.get	514
	drop
	local.get	4
	local.get	512
	i32.store	188
.LBB24_45:                              # %for.cond138
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label81:
	i32.const	1048576
	local.set	515
	local.get	4
	i32.load	188
	local.set	516
	local.get	516
	local.set	517
	local.get	515
	local.set	518
	local.get	517
	local.get	518
	i32.lt_s
	local.set	519
	i32.const	1
	local.set	520
	local.get	519
	local.get	520
	i32.and 
	local.set	521
	local.get	521
	i32.eqz
	br_if   	1               # 1: down to label80
# %bb.46:                               # %for.body140
                                        #   in Loop: Header=BB24_45 Depth=1
	global.get	ic@GOT
	local.set	522
	global.get	ib@GOT
	local.set	523
	global.get	ia@GOT
	local.set	524
	global.get	sc@GOT
	local.set	525
	global.get	sb@GOT
	local.set	526
	global.get	sa@GOT
	local.set	527
	local.get	527
	local.get	526
	local.get	525
	local.get	524
	local.get	523
	local.get	522
	call	_Z10example10bPsS_S_PiS0_S0_
# %bb.47:                               # %for.inc141
                                        #   in Loop: Header=BB24_45 Depth=1
	local.get	4
	i32.load	188
	local.set	528
	i32.const	1
	local.set	529
	local.get	528
	local.get	529
	i32.add 
	local.set	530
	local.get	4
	local.get	530
	i32.store	188
	br      	0               # 0: up to label81
.LBB24_48:                              # %for.end143
	end_loop
	end_block                       # label80:
	global.get	ia@GOT
	local.set	531
	i32.const	4096
	local.set	532
	local.get	531
	local.get	532
	i32.add 
	local.set	533
	local.get	531
	local.get	533
	call	_Z13digest_memoryPvS_
	local.set	534
	i32.const	192
	local.set	535
	local.get	4
	local.get	535
	i32.add 
	local.set	536
	local.get	536
	local.set	537
	i32.const	576
	local.set	538
	local.get	4
	local.get	538
	i32.add 
	local.set	539
	local.get	539
	local.set	540
	i32.const	184
	local.set	541
	local.get	4
	local.get	541
	i32.add 
	local.set	542
	local.get	542
	local.set	543
	local.get	4
	local.get	534
	i32.store	184
	local.get	540
	local.get	543
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	537
	call	_ZN5TimerD1Ev
	drop
	call	_Z9example11v
	local.get	4
	i32.load8_u	595
	local.set	544
	i32.const	1
	local.set	545
	local.get	544
	local.get	545
	i32.and 
	local.set	546
	i32.const	.L.str.12@MBREL
	local.set	547
	global.get	__memory_base
	local.set	548
	local.get	548
	local.get	547
	i32.add 
	local.set	549
	i32.const	160
	local.set	550
	local.get	4
	local.get	550
	i32.add 
	local.set	551
	local.get	551
	local.get	549
	local.get	546
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	552
	i32.const	160
	local.set	553
	local.get	4
	local.get	553
	i32.add 
	local.set	554
	local.get	554
	drop
	local.get	4
	local.get	552
	i32.store	156
.LBB24_49:                              # %for.cond151
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label83:
	i32.const	524288
	local.set	555
	local.get	4
	i32.load	156
	local.set	556
	local.get	556
	local.set	557
	local.get	555
	local.set	558
	local.get	557
	local.get	558
	i32.lt_s
	local.set	559
	i32.const	1
	local.set	560
	local.get	559
	local.get	560
	i32.and 
	local.set	561
	local.get	561
	i32.eqz
	br_if   	1               # 1: down to label82
# %bb.50:                               # %for.body153
                                        #   in Loop: Header=BB24_49 Depth=1
	call	_Z9example11v
# %bb.51:                               # %for.inc154
                                        #   in Loop: Header=BB24_49 Depth=1
	local.get	4
	i32.load	156
	local.set	562
	i32.const	1
	local.set	563
	local.get	562
	local.get	563
	i32.add 
	local.set	564
	local.get	4
	local.get	564
	i32.store	156
	br      	0               # 0: up to label83
.LBB24_52:                              # %for.end156
	end_loop
	end_block                       # label82:
	global.get	d@GOT
	local.set	565
	i32.const	4096
	local.set	566
	local.get	565
	local.get	566
	i32.add 
	local.set	567
	local.get	565
	local.get	567
	call	_Z13digest_memoryPvS_
	local.set	568
	i32.const	160
	local.set	569
	local.get	4
	local.get	569
	i32.add 
	local.set	570
	local.get	570
	local.set	571
	i32.const	576
	local.set	572
	local.get	4
	local.get	572
	i32.add 
	local.set	573
	local.get	573
	local.set	574
	i32.const	152
	local.set	575
	local.get	4
	local.get	575
	i32.add 
	local.set	576
	local.get	576
	local.set	577
	local.get	4
	local.get	568
	i32.store	152
	local.get	574
	local.get	577
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	571
	call	_ZN5TimerD1Ev
	drop
	call	_Z9example12v
	local.get	4
	i32.load8_u	595
	local.set	578
	i32.const	1
	local.set	579
	local.get	578
	local.get	579
	i32.and 
	local.set	580
	i32.const	.L.str.13@MBREL
	local.set	581
	global.get	__memory_base
	local.set	582
	local.get	582
	local.get	581
	i32.add 
	local.set	583
	i32.const	128
	local.set	584
	local.get	4
	local.get	584
	i32.add 
	local.set	585
	local.get	585
	local.get	583
	local.get	580
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	586
	i32.const	128
	local.set	587
	local.get	4
	local.get	587
	i32.add 
	local.set	588
	local.get	588
	drop
	local.get	4
	local.get	586
	i32.store	124
.LBB24_53:                              # %for.cond164
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label85:
	i32.const	1048576
	local.set	589
	local.get	4
	i32.load	124
	local.set	590
	local.get	590
	local.set	591
	local.get	589
	local.set	592
	local.get	591
	local.get	592
	i32.lt_s
	local.set	593
	i32.const	1
	local.set	594
	local.get	593
	local.get	594
	i32.and 
	local.set	595
	local.get	595
	i32.eqz
	br_if   	1               # 1: down to label84
# %bb.54:                               # %for.body166
                                        #   in Loop: Header=BB24_53 Depth=1
	call	_Z9example12v
# %bb.55:                               # %for.inc167
                                        #   in Loop: Header=BB24_53 Depth=1
	local.get	4
	i32.load	124
	local.set	596
	i32.const	1
	local.set	597
	local.get	596
	local.get	597
	i32.add 
	local.set	598
	local.get	4
	local.get	598
	i32.store	124
	br      	0               # 0: up to label85
.LBB24_56:                              # %for.end169
	end_loop
	end_block                       # label84:
	global.get	a@GOT
	local.set	599
	i32.const	4096
	local.set	600
	local.get	599
	local.get	600
	i32.add 
	local.set	601
	local.get	599
	local.get	601
	call	_Z13digest_memoryPvS_
	local.set	602
	i32.const	128
	local.set	603
	local.get	4
	local.get	603
	i32.add 
	local.set	604
	local.get	604
	local.set	605
	i32.const	576
	local.set	606
	local.get	4
	local.get	606
	i32.add 
	local.set	607
	local.get	607
	local.set	608
	i32.const	120
	local.set	609
	local.get	4
	local.get	609
	i32.add 
	local.set	610
	local.get	610
	local.set	611
	local.get	4
	local.get	602
	i32.store	120
	local.get	608
	local.get	611
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	605
	call	_ZN5TimerD1Ev
	drop
	global.get	ua@GOT
	local.set	612
	global.get	usa@GOT
	local.set	613
	local.get	613
	local.get	612
	call	_Z9example23PtPj
	local.get	4
	i32.load8_u	595
	local.set	614
	i32.const	1
	local.set	615
	local.get	614
	local.get	615
	i32.and 
	local.set	616
	i32.const	.L.str.14@MBREL
	local.set	617
	global.get	__memory_base
	local.set	618
	local.get	618
	local.get	617
	i32.add 
	local.set	619
	i32.const	96
	local.set	620
	local.get	4
	local.get	620
	i32.add 
	local.set	621
	local.get	621
	local.get	619
	local.get	616
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	622
	i32.const	96
	local.set	623
	local.get	4
	local.get	623
	i32.add 
	local.set	624
	local.get	624
	drop
	local.get	4
	local.get	622
	i32.store	92
.LBB24_57:                              # %for.cond177
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label87:
	i32.const	2097152
	local.set	625
	local.get	4
	i32.load	92
	local.set	626
	local.get	626
	local.set	627
	local.get	625
	local.set	628
	local.get	627
	local.get	628
	i32.lt_s
	local.set	629
	i32.const	1
	local.set	630
	local.get	629
	local.get	630
	i32.and 
	local.set	631
	local.get	631
	i32.eqz
	br_if   	1               # 1: down to label86
# %bb.58:                               # %for.body179
                                        #   in Loop: Header=BB24_57 Depth=1
	global.get	ua@GOT
	local.set	632
	global.get	usa@GOT
	local.set	633
	local.get	633
	local.get	632
	call	_Z9example23PtPj
# %bb.59:                               # %for.inc180
                                        #   in Loop: Header=BB24_57 Depth=1
	local.get	4
	i32.load	92
	local.set	634
	i32.const	1
	local.set	635
	local.get	634
	local.get	635
	i32.add 
	local.set	636
	local.get	4
	local.get	636
	i32.store	92
	br      	0               # 0: up to label87
.LBB24_60:                              # %for.end182
	end_loop
	end_block                       # label86:
	global.get	usa@GOT
	local.set	637
	i32.const	512
	local.set	638
	local.get	637
	local.get	638
	i32.add 
	local.set	639
	local.get	637
	local.get	639
	call	_Z13digest_memoryPvS_
	local.set	640
	i32.const	2
	local.set	641
	i32.const	4
	local.set	642
	i32.const	96
	local.set	643
	local.get	4
	local.get	643
	i32.add 
	local.set	644
	local.get	644
	local.set	645
	i32.const	576
	local.set	646
	local.get	4
	local.get	646
	i32.add 
	local.set	647
	local.get	647
	local.set	648
	i32.const	88
	local.set	649
	local.get	4
	local.get	649
	i32.add 
	local.set	650
	local.get	650
	local.set	651
	local.get	4
	local.get	640
	i32.store	88
	local.get	648
	local.get	651
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	645
	call	_ZN5TimerD1Ev
	drop
	i32.const	16
	local.set	652
	local.get	641
	local.get	652
	i32.shl 
	local.set	653
	local.get	653
	local.get	652
	i32.shr_s
	local.set	654
	i32.const	16
	local.set	655
	local.get	642
	local.get	655
	i32.shl 
	local.set	656
	local.get	656
	local.get	655
	i32.shr_s
	local.set	657
	local.get	654
	local.get	657
	call	_Z9example24ss
	local.get	4
	i32.load8_u	595
	local.set	658
	i32.const	1
	local.set	659
	local.get	658
	local.get	659
	i32.and 
	local.set	660
	i32.const	.L.str.15@MBREL
	local.set	661
	global.get	__memory_base
	local.set	662
	local.get	662
	local.get	661
	i32.add 
	local.set	663
	i32.const	64
	local.set	664
	local.get	4
	local.get	664
	i32.add 
	local.set	665
	local.get	665
	local.get	663
	local.get	660
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	666
	i32.const	64
	local.set	667
	local.get	4
	local.get	667
	i32.add 
	local.set	668
	local.get	668
	drop
	local.get	4
	local.get	666
	i32.store	60
.LBB24_61:                              # %for.cond190
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label89:
	i32.const	524288
	local.set	669
	local.get	4
	i32.load	60
	local.set	670
	local.get	670
	local.set	671
	local.get	669
	local.set	672
	local.get	671
	local.get	672
	i32.lt_s
	local.set	673
	i32.const	1
	local.set	674
	local.get	673
	local.get	674
	i32.and 
	local.set	675
	local.get	675
	i32.eqz
	br_if   	1               # 1: down to label88
# %bb.62:                               # %for.body192
                                        #   in Loop: Header=BB24_61 Depth=1
	i32.const	2
	local.set	676
	i32.const	4
	local.set	677
	i32.const	16
	local.set	678
	local.get	676
	local.get	678
	i32.shl 
	local.set	679
	local.get	679
	local.get	678
	i32.shr_s
	local.set	680
	i32.const	16
	local.set	681
	local.get	677
	local.get	681
	i32.shl 
	local.set	682
	local.get	682
	local.get	681
	i32.shr_s
	local.set	683
	local.get	680
	local.get	683
	call	_Z9example24ss
# %bb.63:                               # %for.inc193
                                        #   in Loop: Header=BB24_61 Depth=1
	local.get	4
	i32.load	60
	local.set	684
	i32.const	1
	local.set	685
	local.get	684
	local.get	685
	i32.add 
	local.set	686
	local.get	4
	local.get	686
	i32.store	60
	br      	0               # 0: up to label89
.LBB24_64:                              # %for.end195
	end_loop
	end_block                       # label88:
	i32.const	64
	local.set	687
	local.get	4
	local.get	687
	i32.add 
	local.set	688
	local.get	688
	local.set	689
	i32.const	576
	local.set	690
	local.get	4
	local.get	690
	i32.add 
	local.set	691
	local.get	691
	local.set	692
	i32.const	56
	local.set	693
	local.get	4
	local.get	693
	i32.add 
	local.set	694
	local.get	694
	local.set	695
	i32.const	0
	local.set	696
	local.get	4
	local.get	696
	i32.store	56
	local.get	692
	local.get	695
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	689
	call	_ZN5TimerD1Ev
	drop
	call	_Z9example25v
	local.get	4
	i32.load8_u	595
	local.set	697
	i32.const	1
	local.set	698
	local.get	697
	local.get	698
	i32.and 
	local.set	699
	i32.const	.L.str.16@MBREL
	local.set	700
	global.get	__memory_base
	local.set	701
	local.get	701
	local.get	700
	i32.add 
	local.set	702
	i32.const	32
	local.set	703
	local.get	4
	local.get	703
	i32.add 
	local.set	704
	local.get	704
	local.get	702
	local.get	699
	call	_ZN5TimerC1EPKcb
	drop
	i32.const	0
	local.set	705
	i32.const	32
	local.set	706
	local.get	4
	local.get	706
	i32.add 
	local.set	707
	local.get	707
	drop
	local.get	4
	local.get	705
	i32.store	28
.LBB24_65:                              # %for.cond202
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label91:
	i32.const	524288
	local.set	708
	local.get	4
	i32.load	28
	local.set	709
	local.get	709
	local.set	710
	local.get	708
	local.set	711
	local.get	710
	local.get	711
	i32.lt_s
	local.set	712
	i32.const	1
	local.set	713
	local.get	712
	local.get	713
	i32.and 
	local.set	714
	local.get	714
	i32.eqz
	br_if   	1               # 1: down to label90
# %bb.66:                               # %for.body204
                                        #   in Loop: Header=BB24_65 Depth=1
	call	_Z9example25v
# %bb.67:                               # %for.inc205
                                        #   in Loop: Header=BB24_65 Depth=1
	local.get	4
	i32.load	28
	local.set	715
	i32.const	1
	local.set	716
	local.get	715
	local.get	716
	i32.add 
	local.set	717
	local.get	4
	local.get	717
	i32.store	28
	br      	0               # 0: up to label91
.LBB24_68:                              # %for.end207
	end_loop
	end_block                       # label90:
	global.get	dj@GOT
	local.set	718
	i32.const	4096
	local.set	719
	local.get	718
	local.get	719
	i32.add 
	local.set	720
	local.get	718
	local.get	720
	call	_Z13digest_memoryPvS_
	local.set	721
	i32.const	32
	local.set	722
	local.get	4
	local.get	722
	i32.add 
	local.set	723
	local.get	723
	local.set	724
	i32.const	576
	local.set	725
	local.get	4
	local.get	725
	i32.add 
	local.set	726
	local.get	726
	local.set	727
	i32.const	24
	local.set	728
	local.get	4
	local.get	728
	i32.add 
	local.set	729
	local.get	729
	local.set	730
	local.get	4
	local.get	721
	i32.store	24
	local.get	727
	local.get	730
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	local.get	724
	call	_ZN5TimerD1Ev
	drop
	i32.const	_ZNSt3__23hexERNS_8ios_baseE@TBREL
	local.set	731
	global.get	__table_base
	local.set	732
	local.get	732
	local.get	731
	i32.add 
	local.set	733
	global.get	_ZNSt3__24coutE@GOT
	local.set	734
	local.get	734
	local.get	733
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E
	drop
	i32.const	.L.str.17@MBREL
	local.set	735
	global.get	__memory_base
	local.set	736
	local.get	736
	local.get	735
	i32.add 
	local.set	737
	global.get	_ZNSt3__24coutE@GOT
	local.set	738
	local.get	738
	local.get	737
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	local.set	739
	i32.const	0
	local.set	740
	i32.const	576
	local.set	741
	local.get	4
	local.get	741
	i32.add 
	local.set	742
	local.get	742
	local.set	743
	local.get	743
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv
	local.set	744
	local.get	4
	local.get	744
	i32.store	16
	local.get	743
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv
	local.set	745
	local.get	4
	local.get	745
	i32.store	8
	local.get	4
	i32.load	16
	local.set	746
	local.get	4
	i32.load	8
	local.set	747
	local.get	746
	local.get	747
	local.get	740
	call	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_
	local.set	748
	local.get	739
	local.get	748
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEi
	local.set	749
	i32.const	.L.str.18@MBREL
	local.set	750
	global.get	__memory_base
	local.set	751
	local.get	751
	local.get	750
	i32.add 
	local.set	752
	local.get	749
	local.get	752
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	drop
	i32.const	0
	local.set	753
	local.get	4
	local.get	753
	i32.store	4
.LBB24_69:                              # %for.cond223
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label93:
	i32.const	576
	local.set	754
	local.get	4
	local.get	754
	i32.add 
	local.set	755
	local.get	755
	local.set	756
	local.get	4
	i32.load	4
	local.set	757
	local.get	756
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	local.set	758
	local.get	757
	local.set	759
	local.get	758
	local.set	760
	local.get	759
	local.get	760
	i32.lt_u
	local.set	761
	i32.const	1
	local.set	762
	local.get	761
	local.get	762
	i32.and 
	local.set	763
	local.get	763
	i32.eqz
	br_if   	1               # 1: down to label92
# %bb.70:                               # %for.body226
                                        #   in Loop: Header=BB24_69 Depth=1
	i32.const	.L.str.19@MBREL
	local.set	764
	global.get	__memory_base
	local.set	765
	local.get	765
	local.get	764
	i32.add 
	local.set	766
	global.get	_ZNSt3__24coutE@GOT
	local.set	767
	local.get	767
	local.get	766
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	local.set	768
	i32.const	576
	local.set	769
	local.get	4
	local.get	769
	i32.add 
	local.set	770
	local.get	770
	local.set	771
	local.get	4
	i32.load	4
	local.set	772
	local.get	771
	local.get	772
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm
	local.set	773
	local.get	773
	i32.load	0
	local.set	774
	local.get	768
	local.get	774
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEj
	drop
# %bb.71:                               # %for.inc230
                                        #   in Loop: Header=BB24_69 Depth=1
	local.get	4
	i32.load	4
	local.set	775
	i32.const	1
	local.set	776
	local.get	775
	local.get	776
	i32.add 
	local.set	777
	local.get	4
	local.get	777
	i32.store	4
	br      	0               # 0: up to label93
.LBB24_72:                              # %for.end232
	end_loop
	end_block                       # label92:
	i32.const	.L.str.20@MBREL
	local.set	778
	global.get	__memory_base
	local.set	779
	local.get	779
	local.get	778
	i32.add 
	local.set	780
	global.get	_ZNSt3__24coutE@GOT
	local.set	781
	local.get	781
	local.get	780
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	drop
	i32.const	576
	local.set	782
	local.get	4
	local.get	782
	i32.add 
	local.set	783
	local.get	783
	local.set	784
	i32.const	0
	local.set	785
	local.get	4
	local.get	785
	i32.store	604
	local.get	784
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev
	drop
	local.get	4
	i32.load	604
	local.set	786
	i32.const	608
	local.set	787
	local.get	4
	local.get	787
	i32.add 
	local.set	788
	local.get	788
	global.set	__stack_pointer
	local.get	786
	return
	end_function
.Lfunc_end24:
	.size	main, .Lfunc_end24-main
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end25:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev, .Lfunc_end25-_ZNSt3__26vectorIjNS_9allocatorIjEEEC1Ev
                                        # -- End function
	.section	.text._ZN5TimerC1EPKcb,"",@
	.weak	_ZN5TimerC1EPKcb        # -- Begin function _ZN5TimerC1EPKcb
	.type	_ZN5TimerC1EPKcb,@function
_ZN5TimerC1EPKcb:                       # @_ZN5TimerC1EPKcb
	.functype	_ZN5TimerC1EPKcb (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	2
	local.set	6
	local.get	5
	local.get	6
	i32.store8	7
	local.get	5
	i32.load	12
	local.set	7
	local.get	5
	i32.load	8
	local.set	8
	local.get	5
	i32.load8_u	7
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	7
	local.get	8
	local.get	11
	call	_ZN5TimerC2EPKcb
	drop
	i32.const	16
	local.set	12
	local.get	5
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end26:
	.size	_ZN5TimerC1EPKcb, .Lfunc_end26-_ZN5TimerC1EPKcb
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	i32.load	4
	local.set	6
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	local.set	7
	local.get	7
	i32.load	0
	local.set	8
	local.get	6
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.ne  
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	block   	
	block   	
	local.get	13
	i32.eqz
	br_if   	0               # 0: down to label95
# %bb.1:                                # %if.then
	local.get	4
	i32.load	8
	local.set	14
	local.get	5
	local.get	14
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_
	br      	1               # 1: down to label94
.LBB27_2:                               # %if.else
	end_block                       # label95:
	local.get	4
	i32.load	8
	local.set	15
	local.get	5
	local.get	15
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_
.LBB27_3:                               # %if.end
	end_block                       # label94:
	i32.const	16
	local.set	16
	local.get	4
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end27:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj, .Lfunc_end27-_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj
                                        # -- End function
	.section	.text._ZN5TimerD1Ev,"",@
	.weak	_ZN5TimerD1Ev           # -- Begin function _ZN5TimerD1Ev
	.type	_ZN5TimerD1Ev,@function
_ZN5TimerD1Ev:                          # @_ZN5TimerD1Ev
	.functype	_ZN5TimerD1Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZN5TimerD2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end28:
	.size	_ZN5TimerD1Ev, .Lfunc_end28-_ZN5TimerD1Ev
                                        # -- End function
	.section	.text._ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E,"",@
	.hidden	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E # -- Begin function _ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E
	.weak	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E
	.type	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E,@function
_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E: # @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	i32.load	0
	local.set	7
	i32.const	-12
	local.set	8
	local.get	7
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	i32.load	0
	local.set	10
	local.get	5
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	local.get	6
	call_indirect	(i32) -> (i32)
	drop
	i32.const	16
	local.set	12
	local.get	4
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end29:
	.size	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E, .Lfunc_end29-_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRNS_8ios_baseES5_E
                                        # -- End function
	.section	.text._ZNSt3__23hexERNS_8ios_baseE,"",@
	.hidden	_ZNSt3__23hexERNS_8ios_baseE # -- Begin function _ZNSt3__23hexERNS_8ios_baseE
	.weak	_ZNSt3__23hexERNS_8ios_baseE
	.type	_ZNSt3__23hexERNS_8ios_baseE,@function
_ZNSt3__23hexERNS_8ios_baseE:           # @_ZNSt3__23hexERNS_8ios_baseE
	.functype	_ZNSt3__23hexERNS_8ios_baseE (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	8
	local.set	4
	i32.const	74
	local.set	5
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	6
	local.get	6
	local.get	4
	local.get	5
	call	_ZNSt3__28ios_base4setfEjj
	drop
	local.get	3
	i32.load	12
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end30:
	.size	_ZNSt3__23hexERNS_8ios_baseE, .Lfunc_end30-_ZNSt3__23hexERNS_8ios_baseE
                                        # -- End function
	.section	.text._ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc,"",@
	.weak	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc # -- Begin function _ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.type	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc,@function
_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: # @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.functype	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	7
	call	_ZNSt3__211char_traitsIcE6lengthEPKc
	local.set	8
	local.get	5
	local.get	6
	local.get	8
	call	_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	local.set	9
	i32.const	16
	local.set	10
	local.get	4
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	local.get	9
	return
	end_function
.Lfunc_end31:
	.size	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc, .Lfunc_end31-_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
                                        # -- End function
	.section	.text._ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_,"",@
	.hidden	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_ # -- Begin function _ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_
	.weak	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_
	.type	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_,@function
_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_: # @_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_
	.functype	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	24
	local.get	5
	local.get	1
	i32.store	16
	local.get	5
	local.get	2
	i32.store	12
.LBB32_1:                               # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label97:
	i32.const	24
	local.set	6
	local.get	5
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	local.set	8
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	local.set	11
	local.get	8
	local.get	11
	call	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_
	local.set	12
	i32.const	1
	local.set	13
	local.get	12
	local.get	13
	i32.and 
	local.set	14
	local.get	14
	i32.eqz
	br_if   	1               # 1: down to label96
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB32_1 Depth=1
	i32.const	24
	local.set	15
	local.get	5
	local.get	15
	i32.add 
	local.set	16
	local.get	16
	local.set	17
	local.get	5
	i32.load	12
	local.set	18
	local.get	17
	call	_ZNKSt3__211__wrap_iterIPjEdeEv
	local.set	19
	local.get	19
	i32.load	0
	local.set	20
	local.get	18
	local.get	20
	i32.add 
	local.set	21
	local.get	5
	local.get	21
	i32.store	12
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB32_1 Depth=1
	i32.const	24
	local.set	22
	local.get	5
	local.get	22
	i32.add 
	local.set	23
	local.get	23
	local.set	24
	local.get	24
	call	_ZNSt3__211__wrap_iterIPjEppEv
	drop
	br      	0               # 0: up to label97
.LBB32_4:                               # %for.end
	end_loop
	end_block                       # label96:
	local.get	5
	i32.load	12
	local.set	25
	i32.const	32
	local.set	26
	local.get	5
	local.get	26
	i32.add 
	local.set	27
	local.get	27
	global.set	__stack_pointer
	local.get	25
	return
	end_function
.Lfunc_end32:
	.size	_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_, .Lfunc_end32-_ZNSt3__210accumulateINS_11__wrap_iterIPjEEiEET0_T_S5_S4_
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	4
	local.get	5
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
	local.set	6
	local.get	3
	local.get	6
	i32.store	8
	local.get	3
	i32.load	8
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end33:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv, .Lfunc_end33-_ZNSt3__26vectorIjNS_9allocatorIjEEE5beginEv
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	i32.load	4
	local.set	5
	local.get	4
	local.get	5
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
	local.set	6
	local.get	3
	local.get	6
	i32.store	8
	local.get	3
	i32.load	8
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end34:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv, .Lfunc_end34-_ZNSt3__26vectorIjNS_9allocatorIjEEE3endEv
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	4
	local.set	5
	local.get	4
	i32.load	0
	local.set	6
	local.get	5
	local.get	6
	i32.sub 
	local.set	7
	i32.const	2
	local.set	8
	local.get	7
	local.get	8
	i32.shr_s
	local.set	9
	local.get	9
	return
	end_function
.Lfunc_end35:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv, .Lfunc_end35-_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEEixEm,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEEixEm
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm: # @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	i32.load	0
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	i32.const	2
	local.set	8
	local.get	7
	local.get	8
	i32.shl 
	local.set	9
	local.get	6
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	return
	end_function
.Lfunc_end36:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm, .Lfunc_end36-_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end37:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev, .Lfunc_end37-_ZNSt3__26vectorIjNS_9allocatorIjEEED1Ev
                                        # -- End function
	.section	.text._ZN5TimerC2EPKcb,"",@
	.weak	_ZN5TimerC2EPKcb        # -- Begin function _ZN5TimerC2EPKcb
	.type	_ZN5TimerC2EPKcb,@function
_ZN5TimerC2EPKcb:                       # @_ZN5TimerC2EPKcb
	.functype	_ZN5TimerC2EPKcb (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	i32.const	0
	local.set	6
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	2
	local.set	7
	local.get	5
	local.get	7
	i32.store8	7
	local.get	5
	i32.load	12
	local.set	8
	local.get	5
	i32.load	8
	local.set	9
	local.get	8
	local.get	9
	i32.store	0
	local.get	5
	i32.load8_u	7
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	8
	local.get	12
	i32.store8	4
	i32.const	8
	local.set	13
	local.get	8
	local.get	13
	i32.add 
	local.set	14
	local.get	14
	local.get	6
	call	gettimeofday
	drop
	i32.const	16
	local.set	15
	local.get	5
	local.get	15
	i32.add 
	local.set	16
	local.get	16
	global.set	__stack_pointer
	local.get	8
	return
	end_function
.Lfunc_end38:
	.size	_ZN5TimerC2EPKcb, .Lfunc_end38-_ZN5TimerC2EPKcb
                                        # -- End function
	.section	.text._ZN5TimerD2Ev,"",@
	.weak	_ZN5TimerD2Ev           # -- Begin function _ZN5TimerD2Ev
	.type	_ZN5TimerD2Ev,@function
_ZN5TimerD2Ev:                          # @_ZN5TimerD2Ev
	.functype	_ZN5TimerD2Ev (i32) -> (i32)
	.local  	i32, i32, i32, f64, f64, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, f64, i32, f64, f64, f64, f64, f64, f64, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	32
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	f64.const	0x1p-1
	local.set	4
	f64.const	0x1.f4p9
	local.set	5
	i32.const	0
	local.set	6
	local.get	3
	local.get	0
	i32.store	24
	local.get	3
	i32.load	24
	local.set	7
	local.get	3
	local.get	7
	i32.store	28
	i32.const	16
	local.set	8
	local.get	7
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	local.get	6
	call	gettimeofday
	drop
	local.get	7
	i32.load	16
	local.set	10
	local.get	7
	i32.load	8
	local.set	11
	local.get	10
	local.get	11
	i32.sub 
	local.set	12
	local.get	3
	local.get	12
	i32.store	16
	local.get	7
	i32.load	20
	local.set	13
	local.get	7
	i32.load	12
	local.set	14
	local.get	13
	local.get	14
	i32.sub 
	local.set	15
	local.get	3
	local.get	15
	i32.store	12
	local.get	3
	i32.load	16
	local.set	16
	i32.const	1000
	local.set	17
	local.get	16
	local.get	17
	i32.mul 
	local.set	18
	local.get	18
	f64.convert_i32_s
	local.set	19
	local.get	3
	i32.load	12
	local.set	20
	local.get	20
	f64.convert_i32_s
	local.set	21
	local.get	21
	local.get	5
	f64.div 
	local.set	22
	local.get	19
	local.get	22
	f64.add 
	local.set	23
	local.get	23
	local.get	4
	f64.add 
	local.set	24
	local.get	24
	f64.abs 
	local.set	25
	f64.const	0x1p31
	local.set	26
	local.get	25
	local.get	26
	f64.lt  
	local.set	27
	local.get	27
	i32.eqz
	local.set	28
	block   	
	block   	
	local.get	28
	br_if   	0               # 0: down to label99
# %bb.1:                                # %entry
	local.get	24
	i32.trunc_f64_s
	local.set	29
	local.get	29
	local.set	30
	br      	1               # 1: down to label98
.LBB39_2:                               # %entry
	end_block                       # label99:
	i32.const	-2147483648
	local.set	31
	local.get	31
	local.set	30
.LBB39_3:                               # %entry
	end_block                       # label98:
	local.get	30
	local.set	32
	local.get	3
	local.get	32
	i32.store	20
	local.get	7
	i32.load8_u	4
	local.set	33
	i32.const	1
	local.set	34
	local.get	33
	local.get	34
	i32.and 
	local.set	35
	block   	
	local.get	35
	i32.eqz
	br_if   	0               # 0: down to label100
# %bb.4:                                # %if.then
	local.get	7
	i32.load	0
	local.set	36
	global.get	_ZNSt3__24coutE@GOT
	local.set	37
	local.get	37
	local.get	36
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	local.set	38
	i32.const	.L.str.21@MBREL
	local.set	39
	global.get	__memory_base
	local.set	40
	local.get	40
	local.get	39
	i32.add 
	local.set	41
	local.get	38
	local.get	41
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	local.set	42
	local.get	3
	i32.load	20
	local.set	43
	local.get	42
	local.get	43
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEl
	local.set	44
	i32.const	.L.str.22@MBREL
	local.set	45
	global.get	__memory_base
	local.set	46
	local.get	46
	local.get	45
	i32.add 
	local.set	47
	local.get	44
	local.get	47
	call	_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	drop
.LBB39_5:                               # %if.end
	end_block                       # label100:
	local.get	3
	i32.load	28
	local.set	48
	i32.const	32
	local.set	49
	local.get	3
	local.get	49
	i32.add 
	local.set	50
	local.get	50
	global.set	__stack_pointer
	local.get	48
	return
	end_function
.Lfunc_end39:
	.size	_ZN5TimerD2Ev, .Lfunc_end39-_ZN5TimerD2Ev
                                        # -- End function
	.section	.text._ZNSt3__28ios_base4setfEjj,"",@
	.hidden	_ZNSt3__28ios_base4setfEjj # -- Begin function _ZNSt3__28ios_base4setfEjj
	.weak	_ZNSt3__28ios_base4setfEjj
	.type	_ZNSt3__28ios_base4setfEjj,@function
_ZNSt3__28ios_base4setfEjj:             # @_ZNSt3__28ios_base4setfEjj
	.functype	_ZNSt3__28ios_base4setfEjj (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	6
	i32.load	4
	local.set	7
	local.get	5
	local.get	7
	i32.store	0
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	8
	call	_ZNSt3__28ios_base6unsetfEj
	local.get	5
	i32.load	8
	local.set	9
	local.get	5
	i32.load	4
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	6
	i32.load	4
	local.set	12
	local.get	12
	local.get	11
	i32.or  
	local.set	13
	local.get	6
	local.get	13
	i32.store	4
	local.get	5
	i32.load	0
	local.set	14
	i32.const	16
	local.set	15
	local.get	5
	local.get	15
	i32.add 
	local.set	16
	local.get	16
	global.set	__stack_pointer
	local.get	14
	return
	end_function
.Lfunc_end40:
	.size	_ZNSt3__28ios_base4setfEjj, .Lfunc_end40-_ZNSt3__28ios_base4setfEjj
                                        # -- End function
	.section	.text._ZNSt3__28ios_base6unsetfEj,"",@
	.hidden	_ZNSt3__28ios_base6unsetfEj # -- Begin function _ZNSt3__28ios_base6unsetfEj
	.weak	_ZNSt3__28ios_base6unsetfEj
	.type	_ZNSt3__28ios_base6unsetfEj,@function
_ZNSt3__28ios_base6unsetfEj:            # @_ZNSt3__28ios_base6unsetfEj
	.functype	_ZNSt3__28ios_base6unsetfEj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	i32.const	-1
	local.set	7
	local.get	6
	local.get	7
	i32.xor 
	local.set	8
	local.get	5
	i32.load	4
	local.set	9
	local.get	9
	local.get	8
	i32.and 
	local.set	10
	local.get	5
	local.get	10
	i32.store	4
	return
	end_function
.Lfunc_end41:
	.size	_ZNSt3__28ios_base6unsetfEj, .Lfunc_end41-_ZNSt3__28ios_base6unsetfEj
                                        # -- End function
	.section	.text._ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m,"",@
	.weak	_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m # -- Begin function _ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.type	_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m,@function
_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: # @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.functype	_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	48
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	i32.const	24
	local.set	6
	local.get	5
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	local.set	8
	local.get	5
	local.get	0
	i32.store	44
	local.get	5
	local.get	1
	i32.store	40
	local.get	5
	local.get	2
	i32.store	36
	local.get	5
	i32.load	44
	local.set	9
	local.get	8
	local.get	9
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
	drop
	local.get	8
	call	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	block   	
	local.get	12
	i32.eqz
	br_if   	0               # 0: down to label101
# %bb.1:                                # %if.then
	i32.const	32
	local.set	13
	i32.const	8
	local.set	14
	local.get	5
	local.get	14
	i32.add 
	local.set	15
	local.get	15
	local.set	16
	local.get	5
	i32.load	44
	local.set	17
	local.get	16
	local.get	17
	call	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE
	drop
	local.get	5
	i32.load	40
	local.set	18
	local.get	5
	i32.load	44
	local.set	19
	local.get	19
	i32.load	0
	local.set	20
	i32.const	-12
	local.set	21
	local.get	20
	local.get	21
	i32.add 
	local.set	22
	local.get	22
	i32.load	0
	local.set	23
	local.get	19
	local.get	23
	i32.add 
	local.set	24
	local.get	24
	call	_ZNKSt3__28ios_base5flagsEv
	local.set	25
	i32.const	176
	local.set	26
	local.get	25
	local.get	26
	i32.and 
	local.set	27
	local.get	27
	local.set	28
	local.get	13
	local.set	29
	local.get	28
	local.get	29
	i32.eq  
	local.set	30
	i32.const	1
	local.set	31
	local.get	30
	local.get	31
	i32.and 
	local.set	32
	block   	
	block   	
	local.get	32
	i32.eqz
	br_if   	0               # 0: down to label103
# %bb.2:                                # %cond.true
	local.get	5
	i32.load	40
	local.set	33
	local.get	5
	i32.load	36
	local.set	34
	local.get	33
	local.get	34
	i32.add 
	local.set	35
	local.get	35
	local.set	36
	br      	1               # 1: down to label102
.LBB42_3:                               # %cond.false
	end_block                       # label103:
	local.get	5
	i32.load	40
	local.set	37
	local.get	37
	local.set	36
.LBB42_4:                               # %cond.end
	end_block                       # label102:
	local.get	36
	local.set	38
	i32.const	16
	local.set	39
	local.get	5
	local.get	39
	i32.add 
	local.set	40
	local.get	40
	local.set	41
	local.get	5
	i32.load	40
	local.set	42
	local.get	5
	i32.load	36
	local.set	43
	local.get	42
	local.get	43
	i32.add 
	local.set	44
	local.get	5
	i32.load	44
	local.set	45
	local.get	45
	i32.load	0
	local.set	46
	i32.const	-12
	local.set	47
	local.get	46
	local.get	47
	i32.add 
	local.set	48
	local.get	48
	i32.load	0
	local.set	49
	local.get	45
	local.get	49
	i32.add 
	local.set	50
	local.get	5
	i32.load	44
	local.set	51
	local.get	51
	i32.load	0
	local.set	52
	i32.const	-12
	local.set	53
	local.get	52
	local.get	53
	i32.add 
	local.set	54
	local.get	54
	i32.load	0
	local.set	55
	local.get	51
	local.get	55
	i32.add 
	local.set	56
	local.get	56
	call	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv
	local.set	57
	local.get	5
	i32.load	8
	local.set	58
	i32.const	24
	local.set	59
	local.get	57
	local.get	59
	i32.shl 
	local.set	60
	local.get	60
	local.get	59
	i32.shr_s
	local.set	61
	local.get	58
	local.get	18
	local.get	38
	local.get	44
	local.get	50
	local.get	61
	call	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	local.set	62
	local.get	5
	local.get	62
	i32.store	16
	local.get	41
	call	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv
	local.set	63
	i32.const	1
	local.set	64
	local.get	63
	local.get	64
	i32.and 
	local.set	65
	block   	
	local.get	65
	i32.eqz
	br_if   	0               # 0: down to label104
# %bb.5:                                # %if.then18
	i32.const	5
	local.set	66
	local.get	5
	i32.load	44
	local.set	67
	local.get	67
	i32.load	0
	local.set	68
	i32.const	-12
	local.set	69
	local.get	68
	local.get	69
	i32.add 
	local.set	70
	local.get	70
	i32.load	0
	local.set	71
	local.get	67
	local.get	71
	i32.add 
	local.set	72
	local.get	72
	local.get	66
	call	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj
.LBB42_6:                               # %if.end
	end_block                       # label104:
.LBB42_7:                               # %if.end23
	end_block                       # label101:
	i32.const	24
	local.set	73
	local.get	5
	local.get	73
	i32.add 
	local.set	74
	local.get	74
	local.set	75
	local.get	5
	i32.load	44
	local.set	76
	local.get	75
	call	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	drop
	i32.const	48
	local.set	77
	local.get	5
	local.get	77
	i32.add 
	local.set	78
	local.get	78
	global.set	__stack_pointer
	local.get	76
	return
	end_function
.Lfunc_end42:
	.size	_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m, .Lfunc_end42-_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
                                        # -- End function
	.section	.text._ZNSt3__211char_traitsIcE6lengthEPKc,"",@
	.weak	_ZNSt3__211char_traitsIcE6lengthEPKc # -- Begin function _ZNSt3__211char_traitsIcE6lengthEPKc
	.type	_ZNSt3__211char_traitsIcE6lengthEPKc,@function
_ZNSt3__211char_traitsIcE6lengthEPKc:   # @_ZNSt3__211char_traitsIcE6lengthEPKc
	.functype	_ZNSt3__211char_traitsIcE6lengthEPKc (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	strlen
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end43:
	.size	_ZNSt3__211char_traitsIcE6lengthEPKc, .Lfunc_end43-_ZNSt3__211char_traitsIcE6lengthEPKc
                                        # -- End function
	.section	.text._ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv,"",@
	.hidden	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv # -- Begin function _ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv
	.weak	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv
	.type	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv,@function
_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv: # @_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv
	.functype	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load8_u	0
	local.set	5
	i32.const	1
	local.set	6
	local.get	5
	local.get	6
	i32.and 
	local.set	7
	local.get	7
	return
	end_function
.Lfunc_end44:
	.size	_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv, .Lfunc_end44-_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv
                                        # -- End function
	.section	.text._ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_,"",@
	.hidden	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_ # -- Begin function _ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.type	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_,@function
_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: # @_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.functype	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_ (i32, i32, i32, i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	6
	i32.const	80
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	i32.const	0
	local.set	9
	local.get	8
	local.get	0
	i32.store	64
	local.get	8
	local.get	1
	i32.store	60
	local.get	8
	local.get	2
	i32.store	56
	local.get	8
	local.get	3
	i32.store	52
	local.get	8
	local.get	4
	i32.store	48
	local.get	8
	local.get	5
	i32.store8	47
	local.get	8
	i32.load	64
	local.set	10
	local.get	10
	local.set	11
	local.get	9
	local.set	12
	local.get	11
	local.get	12
	i32.eq  
	local.set	13
	i32.const	1
	local.set	14
	local.get	13
	local.get	14
	i32.and 
	local.set	15
	block   	
	block   	
	local.get	15
	i32.eqz
	br_if   	0               # 0: down to label106
# %bb.1:                                # %if.then
	i32.const	64
	local.set	16
	local.get	8
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	local.set	18
	i32.const	72
	local.set	19
	local.get	8
	local.get	19
	i32.add 
	local.set	20
	local.get	20
	local.set	21
	local.get	18
	i32.load	0
	local.set	22
	local.get	21
	local.get	22
	i32.store	0
	br      	1               # 1: down to label105
.LBB45_2:                               # %if.end
	end_block                       # label106:
	local.get	8
	i32.load	52
	local.set	23
	local.get	8
	i32.load	60
	local.set	24
	local.get	23
	local.get	24
	i32.sub 
	local.set	25
	local.get	8
	local.get	25
	i32.store	40
	local.get	8
	i32.load	48
	local.set	26
	local.get	26
	call	_ZNKSt3__28ios_base5widthEv
	local.set	27
	local.get	8
	local.get	27
	i32.store	36
	local.get	8
	i32.load	36
	local.set	28
	local.get	8
	i32.load	40
	local.set	29
	local.get	28
	local.set	30
	local.get	29
	local.set	31
	local.get	30
	local.get	31
	i32.gt_s
	local.set	32
	i32.const	1
	local.set	33
	local.get	32
	local.get	33
	i32.and 
	local.set	34
	block   	
	block   	
	local.get	34
	i32.eqz
	br_if   	0               # 0: down to label108
# %bb.3:                                # %if.then2
	local.get	8
	i32.load	40
	local.set	35
	local.get	8
	i32.load	36
	local.set	36
	local.get	36
	local.get	35
	i32.sub 
	local.set	37
	local.get	8
	local.get	37
	i32.store	36
	br      	1               # 1: down to label107
.LBB45_4:                               # %if.else
	end_block                       # label108:
	i32.const	0
	local.set	38
	local.get	8
	local.get	38
	i32.store	36
.LBB45_5:                               # %if.end3
	end_block                       # label107:
	i32.const	0
	local.set	39
	local.get	8
	i32.load	56
	local.set	40
	local.get	8
	i32.load	60
	local.set	41
	local.get	40
	local.get	41
	i32.sub 
	local.set	42
	local.get	8
	local.get	42
	i32.store	32
	local.get	8
	i32.load	32
	local.set	43
	local.get	43
	local.set	44
	local.get	39
	local.set	45
	local.get	44
	local.get	45
	i32.gt_s
	local.set	46
	i32.const	1
	local.set	47
	local.get	46
	local.get	47
	i32.and 
	local.set	48
	block   	
	local.get	48
	i32.eqz
	br_if   	0               # 0: down to label109
# %bb.6:                                # %if.then8
	local.get	8
	i32.load	64
	local.set	49
	local.get	8
	i32.load	60
	local.set	50
	local.get	8
	i32.load	32
	local.set	51
	local.get	49
	local.get	50
	local.get	51
	call	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	local.set	52
	local.get	8
	i32.load	32
	local.set	53
	local.get	52
	local.set	54
	local.get	53
	local.set	55
	local.get	54
	local.get	55
	i32.ne  
	local.set	56
	i32.const	1
	local.set	57
	local.get	56
	local.get	57
	i32.and 
	local.set	58
	block   	
	local.get	58
	i32.eqz
	br_if   	0               # 0: down to label110
# %bb.7:                                # %if.then12
	i32.const	64
	local.set	59
	local.get	8
	local.get	59
	i32.add 
	local.set	60
	local.get	60
	local.set	61
	i32.const	72
	local.set	62
	local.get	8
	local.get	62
	i32.add 
	local.set	63
	local.get	63
	local.set	64
	i32.const	0
	local.set	65
	local.get	8
	local.get	65
	i32.store	64
	local.get	61
	i32.load	0
	local.set	66
	local.get	64
	local.get	66
	i32.store	0
	br      	2               # 2: down to label105
.LBB45_8:                               # %if.end14
	end_block                       # label110:
.LBB45_9:                               # %if.end15
	end_block                       # label109:
	i32.const	0
	local.set	67
	local.get	8
	i32.load	36
	local.set	68
	local.get	68
	local.set	69
	local.get	67
	local.set	70
	local.get	69
	local.get	70
	i32.gt_s
	local.set	71
	i32.const	1
	local.set	72
	local.get	71
	local.get	72
	i32.and 
	local.set	73
	block   	
	local.get	73
	i32.eqz
	br_if   	0               # 0: down to label111
# %bb.10:                               # %if.then17
	i32.const	16
	local.set	74
	local.get	8
	local.get	74
	i32.add 
	local.set	75
	local.get	75
	local.set	76
	local.get	8
	i32.load	36
	local.set	77
	local.get	8
	i32.load8_u	47
	local.set	78
	i32.const	24
	local.set	79
	local.get	78
	local.get	79
	i32.shl 
	local.set	80
	local.get	80
	local.get	79
	i32.shr_s
	local.set	81
	local.get	76
	local.get	77
	local.get	81
	call	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc
	drop
	local.get	8
	i32.load	64
	local.set	82
	local.get	76
	call	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv
	local.set	83
	local.get	8
	i32.load	36
	local.set	84
	local.get	82
	local.get	83
	local.get	84
	call	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	local.set	85
	local.get	8
	i32.load	36
	local.set	86
	local.get	85
	local.set	87
	local.get	86
	local.set	88
	local.get	87
	local.get	88
	i32.ne  
	local.set	89
	i32.const	1
	local.set	90
	local.get	89
	local.get	90
	i32.and 
	local.set	91
	block   	
	block   	
	local.get	91
	i32.eqz
	br_if   	0               # 0: down to label113
# %bb.11:                               # %if.then23
	i32.const	1
	local.set	92
	i32.const	64
	local.set	93
	local.get	8
	local.get	93
	i32.add 
	local.set	94
	local.get	94
	local.set	95
	i32.const	72
	local.set	96
	local.get	8
	local.get	96
	i32.add 
	local.set	97
	local.get	97
	local.set	98
	i32.const	0
	local.set	99
	local.get	8
	local.get	99
	i32.store	64
	local.get	95
	i32.load	0
	local.set	100
	local.get	98
	local.get	100
	i32.store	0
	local.get	8
	local.get	92
	i32.store	12
	br      	1               # 1: down to label112
.LBB45_12:                              # %if.end25
	end_block                       # label113:
	i32.const	0
	local.set	101
	local.get	8
	local.get	101
	i32.store	12
.LBB45_13:                              # %cleanup
	end_block                       # label112:
	i32.const	16
	local.set	102
	local.get	8
	local.get	102
	i32.add 
	local.set	103
	local.get	103
	call	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	drop
	local.get	8
	i32.load	12
	local.set	104
	block   	
	local.get	104
	br_table 	{0, 2, 0}       # 2: down to label105
                                        # 0: down to label114
.LBB45_14:                              # %cleanup.cont
	end_block                       # label114:
.LBB45_15:                              # %if.end27
	end_block                       # label111:
	i32.const	0
	local.set	105
	local.get	8
	i32.load	52
	local.set	106
	local.get	8
	i32.load	56
	local.set	107
	local.get	106
	local.get	107
	i32.sub 
	local.set	108
	local.get	8
	local.get	108
	i32.store	32
	local.get	8
	i32.load	32
	local.set	109
	local.get	109
	local.set	110
	local.get	105
	local.set	111
	local.get	110
	local.get	111
	i32.gt_s
	local.set	112
	i32.const	1
	local.set	113
	local.get	112
	local.get	113
	i32.and 
	local.set	114
	block   	
	local.get	114
	i32.eqz
	br_if   	0               # 0: down to label115
# %bb.16:                               # %if.then32
	local.get	8
	i32.load	64
	local.set	115
	local.get	8
	i32.load	56
	local.set	116
	local.get	8
	i32.load	32
	local.set	117
	local.get	115
	local.get	116
	local.get	117
	call	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	local.set	118
	local.get	8
	i32.load	32
	local.set	119
	local.get	118
	local.set	120
	local.get	119
	local.set	121
	local.get	120
	local.get	121
	i32.ne  
	local.set	122
	i32.const	1
	local.set	123
	local.get	122
	local.get	123
	i32.and 
	local.set	124
	block   	
	local.get	124
	i32.eqz
	br_if   	0               # 0: down to label116
# %bb.17:                               # %if.then36
	i32.const	64
	local.set	125
	local.get	8
	local.get	125
	i32.add 
	local.set	126
	local.get	126
	local.set	127
	i32.const	72
	local.set	128
	local.get	8
	local.get	128
	i32.add 
	local.set	129
	local.get	129
	local.set	130
	i32.const	0
	local.set	131
	local.get	8
	local.get	131
	i32.store	64
	local.get	127
	i32.load	0
	local.set	132
	local.get	130
	local.get	132
	i32.store	0
	br      	2               # 2: down to label105
.LBB45_18:                              # %if.end38
	end_block                       # label116:
.LBB45_19:                              # %if.end39
	end_block                       # label115:
	i32.const	64
	local.set	133
	local.get	8
	local.get	133
	i32.add 
	local.set	134
	local.get	134
	local.set	135
	i32.const	72
	local.set	136
	local.get	8
	local.get	136
	i32.add 
	local.set	137
	local.get	137
	local.set	138
	i32.const	0
	local.set	139
	local.get	8
	i32.load	48
	local.set	140
	local.get	140
	local.get	139
	call	_ZNSt3__28ios_base5widthEl
	drop
	local.get	135
	i32.load	0
	local.set	141
	local.get	138
	local.get	141
	i32.store	0
.LBB45_20:                              # %return
	end_block                       # label105:
	local.get	8
	i32.load	72
	local.set	142
	i32.const	80
	local.set	143
	local.get	8
	local.get	143
	i32.add 
	local.set	144
	local.get	144
	global.set	__stack_pointer
	local.get	142
	return
# %bb.21:                               # %unreachable
	unreachable
	end_function
.Lfunc_end45:
	.size	_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_, .Lfunc_end45-_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
                                        # -- End function
	.section	.text._ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE,"",@
	.hidden	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE # -- Begin function _ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE
	.weak	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE
	.type	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE,@function
_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE: # @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE
	.functype	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE
	drop
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end46:
	.size	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE, .Lfunc_end46-_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC1ERNS_13basic_ostreamIcS2_EE
                                        # -- End function
	.section	.text._ZNKSt3__28ios_base5flagsEv,"",@
	.hidden	_ZNKSt3__28ios_base5flagsEv # -- Begin function _ZNKSt3__28ios_base5flagsEv
	.weak	_ZNKSt3__28ios_base5flagsEv
	.type	_ZNKSt3__28ios_base5flagsEv,@function
_ZNKSt3__28ios_base5flagsEv:            # @_ZNKSt3__28ios_base5flagsEv
	.functype	_ZNKSt3__28ios_base5flagsEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	4
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end47:
	.size	_ZNKSt3__28ios_base5flagsEv, .Lfunc_end47-_ZNKSt3__28ios_base5flagsEv
                                        # -- End function
	.section	.text._ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv,"",@
	.hidden	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv # -- Begin function _ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv
	.weak	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv
	.type	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv,@function
_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv: # @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv
	.functype	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	call	_ZNSt3__211char_traitsIcE3eofEv
	local.set	5
	local.get	4
	i32.load	76
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__211char_traitsIcE11eq_int_typeEii
	local.set	7
	i32.const	1
	local.set	8
	local.get	7
	local.get	8
	i32.and 
	local.set	9
	block   	
	local.get	9
	i32.eqz
	br_if   	0               # 0: down to label117
# %bb.1:                                # %if.then
	i32.const	32
	local.set	10
	i32.const	24
	local.set	11
	local.get	10
	local.get	11
	i32.shl 
	local.set	12
	local.get	12
	local.get	11
	i32.shr_s
	local.set	13
	local.get	4
	local.get	13
	call	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc
	local.set	14
	i32.const	24
	local.set	15
	local.get	14
	local.get	15
	i32.shl 
	local.set	16
	local.get	16
	local.get	15
	i32.shr_s
	local.set	17
	local.get	4
	local.get	17
	i32.store	76
.LBB48_2:                               # %if.end
	end_block                       # label117:
	local.get	4
	i32.load	76
	local.set	18
	i32.const	24
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	20
	local.get	19
	i32.shr_s
	local.set	21
	i32.const	16
	local.set	22
	local.get	3
	local.get	22
	i32.add 
	local.set	23
	local.get	23
	global.set	__stack_pointer
	local.get	21
	return
	end_function
.Lfunc_end48:
	.size	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv, .Lfunc_end48-_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv
                                        # -- End function
	.section	.text._ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv,"",@
	.hidden	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv # -- Begin function _ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv
	.weak	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv
	.type	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv,@function
_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv: # @_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv
	.functype	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	5
	local.get	5
	i32.load	0
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.set	8
	local.get	7
	local.get	8
	i32.eq  
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	return
	end_function
.Lfunc_end49:
	.size	_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv, .Lfunc_end49-_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv
                                        # -- End function
	.section	.text._ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj,"",@
	.hidden	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj # -- Begin function _ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj
	.weak	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj
	.type	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj,@function
_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj: # @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj
	.functype	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__28ios_base8setstateEj
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end50:
	.size	_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj, .Lfunc_end50-_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj
                                        # -- End function
	.section	.text._ZNKSt3__28ios_base5widthEv,"",@
	.hidden	_ZNKSt3__28ios_base5widthEv # -- Begin function _ZNKSt3__28ios_base5widthEv
	.weak	_ZNKSt3__28ios_base5widthEv
	.type	_ZNKSt3__28ios_base5widthEv,@function
_ZNKSt3__28ios_base5widthEv:            # @_ZNKSt3__28ios_base5widthEv
	.functype	_ZNKSt3__28ios_base5widthEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end51:
	.size	_ZNKSt3__28ios_base5widthEv, .Lfunc_end51-_ZNKSt3__28ios_base5widthEv
                                        # -- End function
	.section	.text._ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl,"",@
	.hidden	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl # -- Begin function _ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	.weak	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	.type	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl,@function
_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl: # @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
	.functype	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	i32.load	0
	local.set	9
	local.get	9
	i32.load	48
	local.set	10
	local.get	6
	local.get	7
	local.get	8
	local.get	10
	call_indirect	(i32, i32, i32) -> (i32)
	local.set	11
	i32.const	16
	local.set	12
	local.get	5
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	11
	return
	end_function
.Lfunc_end52:
	.size	_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl, .Lfunc_end52-_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl
                                        # -- End function
	.section	.text._ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc,"",@
	.hidden	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc # -- Begin function _ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc
	.weak	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc
	.type	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc,@function
_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc: # @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc
	.functype	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store8	7
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load8_u	7
	local.set	8
	i32.const	24
	local.set	9
	local.get	8
	local.get	9
	i32.shl 
	local.set	10
	local.get	10
	local.get	9
	i32.shr_s
	local.set	11
	local.get	6
	local.get	7
	local.get	11
	call	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc
	drop
	i32.const	16
	local.set	12
	local.get	5
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end53:
	.size	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc, .Lfunc_end53-_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc
                                        # -- End function
	.section	.text._ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv,"",@
	.hidden	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv # -- Begin function _ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv
	.weak	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv
	.type	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv,@function
_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv: # @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv
	.functype	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv
	local.set	5
	local.get	5
	call	_ZNSt3__212__to_addressIKcEEPT_S3_
	local.set	6
	i32.const	16
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end54:
	.size	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv, .Lfunc_end54-_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv
                                        # -- End function
	.section	.text._ZNSt3__28ios_base5widthEl,"",@
	.hidden	_ZNSt3__28ios_base5widthEl # -- Begin function _ZNSt3__28ios_base5widthEl
	.weak	_ZNSt3__28ios_base5widthEl
	.type	_ZNSt3__28ios_base5widthEl,@function
_ZNSt3__28ios_base5widthEl:             # @_ZNSt3__28ios_base5widthEl
	.functype	_ZNSt3__28ios_base5widthEl (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	i32.load	12
	local.set	6
	local.get	4
	local.get	6
	i32.store	4
	local.get	4
	i32.load	8
	local.set	7
	local.get	5
	local.get	7
	i32.store	12
	local.get	4
	i32.load	4
	local.set	8
	local.get	8
	return
	end_function
.Lfunc_end55:
	.size	_ZNSt3__28ios_base5widthEl, .Lfunc_end55-_ZNSt3__28ios_base5widthEl
                                        # -- End function
	.section	.text._ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc,"",@
	.hidden	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc # -- Begin function _ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc
	.weak	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc
	.type	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc,@function
_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc: # @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc
	.functype	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	i32.const	16
	local.set	6
	local.get	5
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	local.set	8
	i32.const	8
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	local.set	11
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store8	23
	local.get	5
	i32.load	28
	local.set	12
	local.get	12
	local.get	8
	local.get	11
	call	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_
	drop
	local.get	5
	i32.load	24
	local.set	13
	local.get	5
	i32.load8_u	23
	local.set	14
	i32.const	24
	local.set	15
	local.get	14
	local.get	15
	i32.shl 
	local.set	16
	local.get	16
	local.get	15
	i32.shr_s
	local.set	17
	local.get	12
	local.get	13
	local.get	17
	call	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	i32.const	32
	local.set	18
	local.get	5
	local.get	18
	i32.add 
	local.set	19
	local.get	19
	global.set	__stack_pointer
	local.get	12
	return
	end_function
.Lfunc_end56:
	.size	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc, .Lfunc_end56-_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_
	.type	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_,@function
_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_: # @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_
	drop
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end57:
	.size	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_, .Lfunc_end57-_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC1INS_18__default_init_tagESA_EEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_
	.type	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_,@function
_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_: # @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store	20
	local.get	5
	i32.load	28
	local.set	6
	local.get	5
	i32.load	24
	local.set	7
	local.get	7
	call	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	drop
	local.get	6
	call	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE
	drop
	local.get	5
	i32.load	20
	local.set	8
	local.get	8
	call	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	drop
	local.get	6
	call	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE
	drop
	i32.const	32
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end58:
	.size	_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_, .Lfunc_end58-_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE,"",@
	.hidden	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE # -- Begin function _ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	.weak	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	.type	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE,@function
_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE: # @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	.functype	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end59:
	.size	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE, .Lfunc_end59-_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE # -- Begin function _ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE
	.weak	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE
	.type	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE,@function
_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE: # @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE
	.functype	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end60:
	.size	_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE, .Lfunc_end60-_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE # -- Begin function _ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE
	.weak	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE
	.type	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE,@function
_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE: # @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE
	.functype	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	call	_ZNSt3__29allocatorIcEC2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end61:
	.size	_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE, .Lfunc_end61-_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIcEC2Ev,"",@
	.hidden	_ZNSt3__29allocatorIcEC2Ev # -- Begin function _ZNSt3__29allocatorIcEC2Ev
	.weak	_ZNSt3__29allocatorIcEC2Ev
	.type	_ZNSt3__29allocatorIcEC2Ev,@function
_ZNSt3__29allocatorIcEC2Ev:             # @_ZNSt3__29allocatorIcEC2Ev
	.functype	_ZNSt3__29allocatorIcEC2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end62:
	.size	_ZNSt3__29allocatorIcEC2Ev, .Lfunc_end62-_ZNSt3__29allocatorIcEC2Ev
                                        # -- End function
	.section	.text._ZNSt3__212__to_addressIKcEEPT_S3_,"",@
	.hidden	_ZNSt3__212__to_addressIKcEEPT_S3_ # -- Begin function _ZNSt3__212__to_addressIKcEEPT_S3_
	.weak	_ZNSt3__212__to_addressIKcEEPT_S3_
	.type	_ZNSt3__212__to_addressIKcEEPT_S3_,@function
_ZNSt3__212__to_addressIKcEEPT_S3_:     # @_ZNSt3__212__to_addressIKcEEPT_S3_
	.functype	_ZNSt3__212__to_addressIKcEEPT_S3_ (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end63:
	.size	_ZNSt3__212__to_addressIKcEEPT_S3_, .Lfunc_end63-_ZNSt3__212__to_addressIKcEEPT_S3_
                                        # -- End function
	.section	.text._ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv,"",@
	.hidden	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv # -- Begin function _ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv
	.weak	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv
	.type	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv,@function
_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv: # @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv
	.functype	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv
	local.set	5
	i32.const	1
	local.set	6
	local.get	5
	local.get	6
	i32.and 
	local.set	7
	block   	
	block   	
	local.get	7
	i32.eqz
	br_if   	0               # 0: down to label119
# %bb.1:                                # %cond.true
	local.get	4
	call	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv
	local.set	8
	local.get	8
	local.set	9
	br      	1               # 1: down to label118
.LBB64_2:                               # %cond.false
	end_block                       # label119:
	local.get	4
	call	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv
	local.set	10
	local.get	10
	local.set	9
.LBB64_3:                               # %cond.end
	end_block                       # label118:
	local.get	9
	local.set	11
	i32.const	16
	local.set	12
	local.get	3
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	11
	return
	end_function
.Lfunc_end64:
	.size	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv, .Lfunc_end64-_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv
                                        # -- End function
	.section	.text._ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv,"",@
	.hidden	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv # -- Begin function _ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv
	.weak	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv
	.type	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv,@function
_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv: # @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv
	.functype	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	5
	local.get	5
	call	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	local.set	6
	local.get	6
	i32.load8_u	11
	local.set	7
	i32.const	255
	local.set	8
	local.get	7
	local.get	8
	i32.and 
	local.set	9
	i32.const	128
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	local.set	12
	local.get	4
	local.set	13
	local.get	12
	local.get	13
	i32.ne  
	local.set	14
	i32.const	1
	local.set	15
	local.get	14
	local.get	15
	i32.and 
	local.set	16
	i32.const	16
	local.set	17
	local.get	3
	local.get	17
	i32.add 
	local.set	18
	local.get	18
	global.set	__stack_pointer
	local.get	16
	return
	end_function
.Lfunc_end65:
	.size	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv, .Lfunc_end65-_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv
                                        # -- End function
	.section	.text._ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv,"",@
	.hidden	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv # -- Begin function _ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv
	.weak	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv
	.type	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv,@function
_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv: # @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv
	.functype	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	local.set	5
	local.get	5
	i32.load	0
	local.set	6
	i32.const	16
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end66:
	.size	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv, .Lfunc_end66-_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv
                                        # -- End function
	.section	.text._ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv,"",@
	.hidden	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv # -- Begin function _ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv
	.weak	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv
	.type	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv,@function
_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv: # @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv
	.functype	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	local.set	5
	local.get	5
	call	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_
	local.set	6
	i32.const	16
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end67:
	.size	_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv, .Lfunc_end67-_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv
                                        # -- End function
	.section	.text._ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv,"",@
	.hidden	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv # -- Begin function _ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	.weak	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	.type	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv,@function
_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv: # @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
	.functype	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end68:
	.size	_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv, .Lfunc_end68-_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv
                                        # -- End function
	.section	.text._ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv,"",@
	.hidden	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv # -- Begin function _ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv
	.weak	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv
	.type	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv,@function
_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv: # @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv
	.functype	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end69:
	.size	_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv, .Lfunc_end69-_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_,"",@
	.hidden	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ # -- Begin function _ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_
	.weak	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_
	.type	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_,@function
_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_: # @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_
	.functype	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__29addressofIKcEEPT_RS2_
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end70:
	.size	_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_, .Lfunc_end70-_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_
                                        # -- End function
	.section	.text._ZNSt3__29addressofIKcEEPT_RS2_,"",@
	.hidden	_ZNSt3__29addressofIKcEEPT_RS2_ # -- Begin function _ZNSt3__29addressofIKcEEPT_RS2_
	.weak	_ZNSt3__29addressofIKcEEPT_RS2_
	.type	_ZNSt3__29addressofIKcEEPT_RS2_,@function
_ZNSt3__29addressofIKcEEPT_RS2_:        # @_ZNSt3__29addressofIKcEEPT_RS2_
	.functype	_ZNSt3__29addressofIKcEEPT_RS2_ (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end71:
	.size	_ZNSt3__29addressofIKcEEPT_RS2_, .Lfunc_end71-_ZNSt3__29addressofIKcEEPT_RS2_
                                        # -- End function
	.section	.text._ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE,"",@
	.hidden	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE # -- Begin function _ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE
	.weak	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE
	.type	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE,@function
_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE: # @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE
	.functype	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	6
	i32.load	0
	local.set	7
	i32.const	-12
	local.set	8
	local.get	7
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	i32.load	0
	local.set	10
	local.get	6
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	call	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv
	local.set	12
	local.get	5
	local.get	12
	i32.store	0
	i32.const	16
	local.set	13
	local.get	4
	local.get	13
	i32.add 
	local.set	14
	local.get	14
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end72:
	.size	_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE, .Lfunc_end72-_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE
                                        # -- End function
	.section	.text._ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv,"",@
	.hidden	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv # -- Begin function _ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv
	.weak	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv
	.type	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv,@function
_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv: # @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv
	.functype	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__28ios_base5rdbufEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end73:
	.size	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv, .Lfunc_end73-_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv
                                        # -- End function
	.section	.text._ZNKSt3__28ios_base5rdbufEv,"",@
	.hidden	_ZNKSt3__28ios_base5rdbufEv # -- Begin function _ZNKSt3__28ios_base5rdbufEv
	.weak	_ZNKSt3__28ios_base5rdbufEv
	.type	_ZNKSt3__28ios_base5rdbufEv,@function
_ZNKSt3__28ios_base5rdbufEv:            # @_ZNKSt3__28ios_base5rdbufEv
	.functype	_ZNKSt3__28ios_base5rdbufEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	24
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end74:
	.size	_ZNKSt3__28ios_base5rdbufEv, .Lfunc_end74-_ZNKSt3__28ios_base5rdbufEv
                                        # -- End function
	.section	.text._ZNSt3__211char_traitsIcE11eq_int_typeEii,"",@
	.weak	_ZNSt3__211char_traitsIcE11eq_int_typeEii # -- Begin function _ZNSt3__211char_traitsIcE11eq_int_typeEii
	.type	_ZNSt3__211char_traitsIcE11eq_int_typeEii,@function
_ZNSt3__211char_traitsIcE11eq_int_typeEii: # @_ZNSt3__211char_traitsIcE11eq_int_typeEii
	.functype	_ZNSt3__211char_traitsIcE11eq_int_typeEii (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.get	8
	i32.eq  
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	local.get	11
	return
	end_function
.Lfunc_end75:
	.size	_ZNSt3__211char_traitsIcE11eq_int_typeEii, .Lfunc_end75-_ZNSt3__211char_traitsIcE11eq_int_typeEii
                                        # -- End function
	.section	.text._ZNSt3__211char_traitsIcE3eofEv,"",@
	.weak	_ZNSt3__211char_traitsIcE3eofEv # -- Begin function _ZNSt3__211char_traitsIcE3eofEv
	.type	_ZNSt3__211char_traitsIcE3eofEv,@function
_ZNSt3__211char_traitsIcE3eofEv:        # @_ZNSt3__211char_traitsIcE3eofEv
	.functype	_ZNSt3__211char_traitsIcE3eofEv () -> (i32)
	.local  	i32
# %bb.0:                                # %entry
	i32.const	4294967295
	local.set	0
	local.get	0
	return
	end_function
.Lfunc_end76:
	.size	_ZNSt3__211char_traitsIcE3eofEv, .Lfunc_end76-_ZNSt3__211char_traitsIcE3eofEv
                                        # -- End function
	.section	.text._ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc,"",@
	.hidden	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc # -- Begin function _ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc
	.weak	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc
	.type	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc,@function
_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc: # @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc
	.functype	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store8	11
	local.get	4
	i32.load	12
	local.set	6
	local.get	5
	local.get	6
	call	_ZNKSt3__28ios_base6getlocEv
	local.get	5
	call	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE
	local.set	7
	local.get	4
	i32.load8_u	11
	local.set	8
	i32.const	24
	local.set	9
	local.get	8
	local.get	9
	i32.shl 
	local.set	10
	local.get	10
	local.get	9
	i32.shr_s
	local.set	11
	local.get	7
	local.get	11
	call	_ZNKSt3__25ctypeIcE5widenEc
	local.set	12
	local.get	5
	call	_ZNSt3__26localeD1Ev
	drop
	i32.const	24
	local.set	13
	local.get	12
	local.get	13
	i32.shl 
	local.set	14
	local.get	14
	local.get	13
	i32.shr_s
	local.set	15
	i32.const	16
	local.set	16
	local.get	4
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	local.get	15
	return
	end_function
.Lfunc_end77:
	.size	_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc, .Lfunc_end77-_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc
                                        # -- End function
	.section	.text._ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE,"",@
	.hidden	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE # -- Begin function _ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE
	.weak	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE
	.type	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE,@function
_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE: # @_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE
	.functype	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	global.get	_ZNSt3__25ctypeIcE2idE@GOT
	local.set	5
	local.get	4
	local.get	5
	call	_ZNKSt3__26locale9use_facetERNS0_2idE
	local.set	6
	i32.const	16
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end78:
	.size	_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE, .Lfunc_end78-_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE
                                        # -- End function
	.section	.text._ZNKSt3__25ctypeIcE5widenEc,"",@
	.hidden	_ZNKSt3__25ctypeIcE5widenEc # -- Begin function _ZNKSt3__25ctypeIcE5widenEc
	.weak	_ZNKSt3__25ctypeIcE5widenEc
	.type	_ZNKSt3__25ctypeIcE5widenEc,@function
_ZNKSt3__25ctypeIcE5widenEc:            # @_ZNKSt3__25ctypeIcE5widenEc
	.functype	_ZNKSt3__25ctypeIcE5widenEc (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store8	11
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load8_u	11
	local.set	6
	local.get	5
	i32.load	0
	local.set	7
	local.get	7
	i32.load	28
	local.set	8
	i32.const	24
	local.set	9
	local.get	6
	local.get	9
	i32.shl 
	local.set	10
	local.get	10
	local.get	9
	i32.shr_s
	local.set	11
	local.get	5
	local.get	11
	local.get	8
	call_indirect	(i32, i32) -> (i32)
	local.set	12
	i32.const	24
	local.set	13
	local.get	12
	local.get	13
	i32.shl 
	local.set	14
	local.get	14
	local.get	13
	i32.shr_s
	local.set	15
	i32.const	16
	local.set	16
	local.get	4
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	local.get	15
	return
	end_function
.Lfunc_end79:
	.size	_ZNKSt3__25ctypeIcE5widenEc, .Lfunc_end79-_ZNKSt3__25ctypeIcE5widenEc
                                        # -- End function
	.section	.text._ZNSt3__28ios_base8setstateEj,"",@
	.hidden	_ZNSt3__28ios_base8setstateEj # -- Begin function _ZNSt3__28ios_base8setstateEj
	.weak	_ZNSt3__28ios_base8setstateEj
	.type	_ZNSt3__28ios_base8setstateEj,@function
_ZNSt3__28ios_base8setstateEj:          # @_ZNSt3__28ios_base8setstateEj
	.functype	_ZNSt3__28ios_base8setstateEj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	i32.load	16
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	6
	local.get	7
	i32.or  
	local.set	8
	local.get	5
	local.get	8
	call	_ZNSt3__28ios_base5clearEj
	i32.const	16
	local.set	9
	local.get	4
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end80:
	.size	_ZNSt3__28ios_base8setstateEj, .Lfunc_end80-_ZNSt3__28ios_base8setstateEj
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end81:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev, .Lfunc_end81-_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev,"",@
	.hidden	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	8
	local.set	4
	local.get	3
	local.get	4
	i32.add 
	local.set	5
	local.get	5
	local.set	6
	local.get	3
	local.set	7
	i32.const	0
	local.set	8
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	9
	local.get	9
	call	_ZNSt3__220__vector_base_commonILb1EEC2Ev
	drop
	local.get	9
	local.get	8
	i32.store	0
	local.get	9
	local.get	8
	i32.store	4
	i32.const	8
	local.set	10
	local.get	9
	local.get	10
	i32.add 
	local.set	11
	local.get	3
	local.get	8
	i32.store	8
	local.get	11
	local.get	6
	local.get	7
	call	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_
	drop
	i32.const	16
	local.set	12
	local.get	3
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	9
	return
	end_function
.Lfunc_end82:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev, .Lfunc_end82-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev
                                        # -- End function
	.section	.text._ZNSt3__220__vector_base_commonILb1EEC2Ev,"",@
	.hidden	_ZNSt3__220__vector_base_commonILb1EEC2Ev # -- Begin function _ZNSt3__220__vector_base_commonILb1EEC2Ev
	.weak	_ZNSt3__220__vector_base_commonILb1EEC2Ev
	.type	_ZNSt3__220__vector_base_commonILb1EEC2Ev,@function
_ZNSt3__220__vector_base_commonILb1EEC2Ev: # @_ZNSt3__220__vector_base_commonILb1EEC2Ev
	.functype	_ZNSt3__220__vector_base_commonILb1EEC2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end83:
	.size	_ZNSt3__220__vector_base_commonILb1EEC2Ev, .Lfunc_end83-_ZNSt3__220__vector_base_commonILb1EEC2Ev
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_
	.type	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_,@function
_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_: # @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_
	drop
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end84:
	.size	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_, .Lfunc_end84-_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC1IDnNS_18__default_init_tagEEEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_
	.type	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_,@function
_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_: # @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	7
	call	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	local.set	8
	local.get	6
	local.get	8
	call	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_
	drop
	local.get	5
	i32.load	4
	local.set	9
	local.get	9
	call	_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE
	drop
	local.get	6
	call	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE
	drop
	i32.const	16
	local.set	10
	local.get	5
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end85:
	.size	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_, .Lfunc_end85-_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE,"",@
	.hidden	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE # -- Begin function _ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	.weak	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	.type	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE,@function
_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE: # @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	.functype	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end86:
	.size	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE, .Lfunc_end86-_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_,"",@
	.weak	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ # -- Begin function _ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_
	.type	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_,@function
_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_: # @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_
	.functype	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	7
	call	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	drop
	local.get	6
	local.get	5
	i32.store	0
	i32.const	16
	local.set	8
	local.get	4
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end87:
	.size	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_, .Lfunc_end87-_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE # -- Begin function _ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE
	.weak	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE
	.type	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE,@function
_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE: # @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE
	.functype	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	call	_ZNSt3__29allocatorIjEC2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end88:
	.size	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE, .Lfunc_end88-_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIjEC2Ev,"",@
	.hidden	_ZNSt3__29allocatorIjEC2Ev # -- Begin function _ZNSt3__29allocatorIjEC2Ev
	.weak	_ZNSt3__29allocatorIjEC2Ev
	.type	_ZNSt3__29allocatorIjEC2Ev,@function
_ZNSt3__29allocatorIjEC2Ev:             # @_ZNSt3__29allocatorIjEC2Ev
	.functype	_ZNSt3__29allocatorIjEC2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end89:
	.size	_ZNSt3__29allocatorIjEC2Ev, .Lfunc_end89-_ZNSt3__29allocatorIjEC2Ev
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
	local.get	4
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end90:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev, .Lfunc_end90-_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	5
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	6
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	local.set	7
	i32.const	2
	local.set	8
	local.get	7
	local.get	8
	i32.shl 
	local.set	9
	local.get	6
	local.get	9
	i32.add 
	local.set	10
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	11
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	local.set	12
	i32.const	2
	local.set	13
	local.get	12
	local.get	13
	i32.shl 
	local.set	14
	local.get	11
	local.get	14
	i32.add 
	local.set	15
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	16
	local.get	4
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	local.set	17
	i32.const	2
	local.set	18
	local.get	17
	local.get	18
	i32.shl 
	local.set	19
	local.get	16
	local.get	19
	i32.add 
	local.set	20
	local.get	4
	local.get	5
	local.get	10
	local.get	15
	local.get	20
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	i32.const	16
	local.set	21
	local.get	3
	local.get	21
	i32.add 
	local.set	22
	local.get	22
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end91:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv, .Lfunc_end91-_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev,"",@
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	8
	local.get	3
	i32.load	8
	local.set	5
	local.get	3
	local.get	5
	i32.store	12
	local.get	5
	i32.load	0
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.set	8
	local.get	7
	local.get	8
	i32.ne  
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	block   	
	local.get	11
	i32.eqz
	br_if   	0               # 0: down to label120
# %bb.1:                                # %if.then
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	12
	local.get	5
	i32.load	0
	local.set	13
	local.get	5
	call	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
	local.set	14
	local.get	12
	local.get	13
	local.get	14
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
.LBB92_2:                               # %if.end
	end_block                       # label120:
	local.get	3
	i32.load	12
	local.set	15
	i32.const	16
	local.set	16
	local.get	3
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	local.get	15
	return
	end_function
.Lfunc_end92:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev, .Lfunc_end92-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ (i32, i32, i32, i32, i32) -> ()
	.local  	i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	5
	i32.const	32
	local.set	6
	local.get	5
	local.get	6
	i32.sub 
	local.set	7
	local.get	7
	local.get	0
	i32.store	28
	local.get	7
	local.get	1
	i32.store	24
	local.get	7
	local.get	2
	i32.store	20
	local.get	7
	local.get	3
	i32.store	16
	local.get	7
	local.get	4
	i32.store	12
	return
	end_function
.Lfunc_end93:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_, .Lfunc_end93-_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	5
	call	_ZNSt3__212__to_addressIjEEPT_S2_
	local.set	6
	i32.const	16
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end94:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv, .Lfunc_end94-_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end95:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv, .Lfunc_end95-_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
                                        # -- End function
	.section	.text._ZNSt3__212__to_addressIjEEPT_S2_,"",@
	.hidden	_ZNSt3__212__to_addressIjEEPT_S2_ # -- Begin function _ZNSt3__212__to_addressIjEEPT_S2_
	.weak	_ZNSt3__212__to_addressIjEEPT_S2_
	.type	_ZNSt3__212__to_addressIjEEPT_S2_,@function
_ZNSt3__212__to_addressIjEEPT_S2_:      # @_ZNSt3__212__to_addressIjEEPT_S2_
	.functype	_ZNSt3__212__to_addressIjEEPT_S2_ (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end96:
	.size	_ZNSt3__212__to_addressIjEEPT_S2_, .Lfunc_end96-_ZNSt3__212__to_addressIjEEPT_S2_
                                        # -- End function
	.section	.text._ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv,"",@
	.hidden	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv # -- Begin function _ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
	.weak	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
	.type	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv,@function
_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv: # @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
	.functype	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	local.set	5
	local.get	5
	i32.load	0
	local.set	6
	local.get	4
	i32.load	0
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	i32.const	2
	local.set	9
	local.get	8
	local.get	9
	i32.shr_s
	local.set	10
	i32.const	16
	local.set	11
	local.get	3
	local.get	11
	i32.add 
	local.set	12
	local.get	12
	global.set	__stack_pointer
	local.get	10
	return
	end_function
.Lfunc_end97:
	.size	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv, .Lfunc_end97-_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv
                                        # -- End function
	.section	.text._ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv,"",@
	.hidden	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv # -- Begin function _ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.weak	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.type	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv,@function
_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv: # @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.functype	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end98:
	.size	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv, .Lfunc_end98-_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
                                        # -- End function
	.section	.text._ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv,"",@
	.hidden	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv # -- Begin function _ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.weak	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.type	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv,@function
_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv: # @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.functype	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end99:
	.size	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv, .Lfunc_end99-_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
                                        # -- End function
	.section	.text._ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv,"",@
	.hidden	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv # -- Begin function _ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.weak	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.type	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv,@function
_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv: # @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.functype	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end100:
	.size	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv, .Lfunc_end100-_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv,"",@
	.hidden	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	4
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end101:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv, .Lfunc_end101-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm,"",@
	.hidden	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__29allocatorIjE10deallocateEPjm
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end102:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm, .Lfunc_end102-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv,"",@
	.hidden	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end103:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv, .Lfunc_end103-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj,"",@
	.hidden	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	i32.load	4
	local.set	6
	local.get	4
	local.get	6
	i32.store	4
.LBB104_1:                              # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label122:
	local.get	4
	i32.load	8
	local.set	7
	local.get	4
	i32.load	4
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.ne  
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	local.get	13
	i32.eqz
	br_if   	1               # 1: down to label121
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB104_1 Depth=1
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	14
	local.get	4
	i32.load	4
	local.set	15
	i32.const	-4
	local.set	16
	local.get	15
	local.get	16
	i32.add 
	local.set	17
	local.get	4
	local.get	17
	i32.store	4
	local.get	17
	call	_ZNSt3__212__to_addressIjEEPT_S2_
	local.set	18
	local.get	14
	local.get	18
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_
	br      	0               # 0: up to label122
.LBB104_3:                              # %while.end
	end_loop
	end_block                       # label121:
	local.get	4
	i32.load	8
	local.set	19
	local.get	5
	local.get	19
	i32.store	4
	i32.const	16
	local.set	20
	local.get	4
	local.get	20
	i32.add 
	local.set	21
	local.get	21
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end104:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj, .Lfunc_end104-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_,"",@
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	32
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	28
	local.get	4
	local.get	1
	i32.store	24
	local.get	4
	i32.load	28
	local.set	5
	local.get	4
	i32.load	24
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_
	i32.const	32
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end105:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_, .Lfunc_end105-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_,"",@
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	4
	local.get	4
	local.get	1
	i32.store	0
	local.get	4
	i32.load	4
	local.set	5
	local.get	4
	i32.load	0
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__29allocatorIjE7destroyEPj
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end106:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_, .Lfunc_end106-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIjE7destroyEPj,"",@
	.hidden	_ZNSt3__29allocatorIjE7destroyEPj # -- Begin function _ZNSt3__29allocatorIjE7destroyEPj
	.weak	_ZNSt3__29allocatorIjE7destroyEPj
	.type	_ZNSt3__29allocatorIjE7destroyEPj,@function
_ZNSt3__29allocatorIjE7destroyEPj:      # @_ZNSt3__29allocatorIjE7destroyEPj
	.functype	_ZNSt3__29allocatorIjE7destroyEPj (i32, i32) -> ()
	.local  	i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	return
	end_function
.Lfunc_end107:
	.size	_ZNSt3__29allocatorIjE7destroyEPj, .Lfunc_end107-_ZNSt3__29allocatorIjE7destroyEPj
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIjE10deallocateEPjm,"",@
	.hidden	_ZNSt3__29allocatorIjE10deallocateEPjm # -- Begin function _ZNSt3__29allocatorIjE10deallocateEPjm
	.weak	_ZNSt3__29allocatorIjE10deallocateEPjm
	.type	_ZNSt3__29allocatorIjE10deallocateEPjm,@function
_ZNSt3__29allocatorIjE10deallocateEPjm: # @_ZNSt3__29allocatorIjE10deallocateEPjm
	.functype	_ZNSt3__29allocatorIjE10deallocateEPjm (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	i32.const	4
	local.set	6
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	i32.const	2
	local.set	9
	local.get	8
	local.get	9
	i32.shl 
	local.set	10
	local.get	7
	local.get	10
	local.get	6
	call	_ZNSt3__219__libcpp_deallocateEPvmm
	i32.const	16
	local.set	11
	local.get	5
	local.get	11
	i32.add 
	local.set	12
	local.get	12
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end108:
	.size	_ZNSt3__29allocatorIjE10deallocateEPjm, .Lfunc_end108-_ZNSt3__29allocatorIjE10deallocateEPjm
                                        # -- End function
	.section	.text._ZNSt3__219__libcpp_deallocateEPvmm,"",@
	.hidden	_ZNSt3__219__libcpp_deallocateEPvmm # -- Begin function _ZNSt3__219__libcpp_deallocateEPvmm
	.weak	_ZNSt3__219__libcpp_deallocateEPvmm
	.type	_ZNSt3__219__libcpp_deallocateEPvmm,@function
_ZNSt3__219__libcpp_deallocateEPvmm:    # @_ZNSt3__219__libcpp_deallocateEPvmm
	.functype	_ZNSt3__219__libcpp_deallocateEPvmm (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end109:
	.size	_ZNSt3__219__libcpp_deallocateEPvmm, .Lfunc_end109-_ZNSt3__219__libcpp_deallocateEPvmm
                                        # -- End function
	.section	.text._ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm,"",@
	.hidden	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm # -- Begin function _ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm
	.weak	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm
	.type	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm,@function
_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm: # @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm
	.functype	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	6
	local.get	7
	call	_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm
	i32.const	16
	local.set	8
	local.get	5
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end110:
	.size	_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm, .Lfunc_end110-_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm
                                        # -- End function
	.section	.text._ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm,"",@
	.weak	_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm # -- Begin function _ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm
	.type	_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm,@function
_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm: # @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm
	.functype	_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	call	_ZNSt3__217_DeallocateCaller9__do_callEPv
	i32.const	16
	local.set	6
	local.get	4
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end111:
	.size	_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm, .Lfunc_end111-_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm
                                        # -- End function
	.section	.text._ZNSt3__217_DeallocateCaller9__do_callEPv,"",@
	.weak	_ZNSt3__217_DeallocateCaller9__do_callEPv # -- Begin function _ZNSt3__217_DeallocateCaller9__do_callEPv
	.type	_ZNSt3__217_DeallocateCaller9__do_callEPv,@function
_ZNSt3__217_DeallocateCaller9__do_callEPv: # @_ZNSt3__217_DeallocateCaller9__do_callEPv
	.functype	_ZNSt3__217_DeallocateCaller9__do_callEPv (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZdlPv
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end112:
	.size	_ZNSt3__217_DeallocateCaller9__do_callEPv, .Lfunc_end112-_ZNSt3__217_DeallocateCaller9__do_callEPv
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv,"",@
	.hidden	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv # -- Begin function _ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.weak	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.type	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv,@function
_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv: # @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.functype	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end113:
	.size	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv, .Lfunc_end113-_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv # -- Begin function _ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.weak	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.type	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv,@function
_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv: # @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.functype	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end114:
	.size	_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv, .Lfunc_end114-_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv,"",@
	.hidden	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv # -- Begin function _ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.weak	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.type	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv,@function
_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv: # @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	.functype	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end115:
	.size	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv, .Lfunc_end115-_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_ # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	32
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	local.set	7
	i32.const	1
	local.set	8
	local.get	4
	local.get	0
	i32.store	28
	local.get	4
	local.get	1
	i32.store	24
	local.get	4
	i32.load	28
	local.set	9
	local.get	7
	local.get	9
	local.get	8
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m
	drop
	local.get	9
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	10
	local.get	4
	i32.load	12
	local.set	11
	local.get	11
	call	_ZNSt3__212__to_addressIjEEPT_S2_
	local.set	12
	local.get	4
	i32.load	24
	local.set	13
	local.get	13
	call	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	local.set	14
	local.get	10
	local.get	12
	local.get	14
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_
	local.get	4
	i32.load	12
	local.set	15
	i32.const	4
	local.set	16
	local.get	15
	local.get	16
	i32.add 
	local.set	17
	local.get	4
	local.get	17
	i32.store	12
	local.get	7
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev
	drop
	i32.const	32
	local.set	18
	local.get	4
	local.get	18
	i32.add 
	local.set	19
	local.get	19
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end116:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_, .Lfunc_end116-_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_ # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	32
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.set	5
	local.get	4
	local.get	0
	i32.store	28
	local.get	4
	local.get	1
	i32.store	24
	local.get	4
	i32.load	28
	local.set	6
	local.get	6
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	7
	local.get	4
	local.get	7
	i32.store	20
	local.get	6
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	local.set	8
	i32.const	1
	local.set	9
	local.get	8
	local.get	9
	i32.add 
	local.set	10
	local.get	6
	local.get	10
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm
	local.set	11
	local.get	6
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	local.set	12
	local.get	4
	i32.load	20
	local.set	13
	local.get	5
	local.get	11
	local.get	12
	local.get	13
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_
	drop
	local.get	4
	i32.load	20
	local.set	14
	local.get	4
	i32.load	8
	local.set	15
	local.get	15
	call	_ZNSt3__212__to_addressIjEEPT_S2_
	local.set	16
	local.get	4
	i32.load	24
	local.set	17
	local.get	17
	call	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	local.set	18
	local.get	14
	local.get	16
	local.get	18
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_
	local.get	4
	i32.load	8
	local.set	19
	i32.const	4
	local.set	20
	local.get	19
	local.get	20
	i32.add 
	local.set	21
	local.get	4
	local.get	21
	i32.store	8
	local.get	6
	local.get	5
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE
	local.get	5
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev
	drop
	i32.const	32
	local.set	22
	local.get	4
	local.get	22
	i32.add 
	local.set	23
	local.get	23
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end117:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_, .Lfunc_end117-_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv,"",@
	.hidden	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv # -- Begin function _ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.weak	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.type	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv,@function
_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv: # @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
	.functype	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end118:
	.size	_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv, .Lfunc_end118-_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv # -- Begin function _ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.weak	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.type	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv,@function
_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv: # @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	.functype	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end119:
	.size	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv, .Lfunc_end119-_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m
	drop
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end120:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m, .Lfunc_end120-_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC1ERS3_m
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_,"",@
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	28
	local.get	5
	local.get	1
	i32.store	24
	local.get	5
	local.get	2
	i32.store	20
	local.get	5
	i32.load	28
	local.set	6
	local.get	5
	i32.load	24
	local.set	7
	local.get	5
	i32.load	20
	local.set	8
	local.get	8
	call	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	local.set	9
	local.get	6
	local.get	7
	local.get	9
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_
	i32.const	32
	local.set	10
	local.get	5
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end121:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_, .Lfunc_end121-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_
                                        # -- End function
	.section	.text._ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE,"",@
	.hidden	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE # -- Begin function _ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	.weak	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	.type	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE,@function
_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE: # @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	.functype	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end122:
	.size	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE, .Lfunc_end122-_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end123:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev, .Lfunc_end123-_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD1Ev
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	6
	local.get	7
	i32.store	0
	local.get	5
	i32.load	8
	local.set	8
	local.get	8
	i32.load	4
	local.set	9
	local.get	6
	local.get	9
	i32.store	4
	local.get	5
	i32.load	8
	local.set	10
	local.get	10
	i32.load	4
	local.set	11
	local.get	5
	i32.load	4
	local.set	12
	i32.const	2
	local.set	13
	local.get	12
	local.get	13
	i32.shl 
	local.set	14
	local.get	11
	local.get	14
	i32.add 
	local.set	15
	local.get	6
	local.get	15
	i32.store	8
	local.get	6
	return
	end_function
.Lfunc_end124:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m, .Lfunc_end124-_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_,"",@
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	32
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	20
	local.get	5
	local.get	1
	i32.store	16
	local.get	5
	local.get	2
	i32.store	12
	local.get	5
	i32.load	20
	local.set	6
	local.get	5
	i32.load	16
	local.set	7
	local.get	5
	i32.load	12
	local.set	8
	local.get	8
	call	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	local.set	9
	local.get	6
	local.get	7
	local.get	9
	call	_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_
	i32.const	32
	local.set	10
	local.get	5
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end125:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_, .Lfunc_end125-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_,"",@
	.weak	_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_ # -- Begin function _ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_
	.type	_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_,@function
_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_: # @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_
	.functype	_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_ (i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	8
	local.set	6
	local.get	5
	i32.load	4
	local.set	7
	local.get	7
	call	_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE
	local.set	8
	local.get	8
	i32.load	0
	local.set	9
	local.get	6
	local.get	9
	i32.store	0
	i32.const	16
	local.set	10
	local.get	5
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end126:
	.size	_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_, .Lfunc_end126-_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	4
	local.set	5
	local.get	4
	i32.load	0
	local.set	6
	local.get	6
	local.get	5
	i32.store	4
	local.get	4
	return
	end_function
.Lfunc_end127:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev, .Lfunc_end127-_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	32
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	24
	local.get	4
	local.get	1
	i32.store	20
	local.get	4
	i32.load	24
	local.set	5
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv
	local.set	6
	local.get	4
	local.get	6
	i32.store	16
	local.get	4
	i32.load	20
	local.set	7
	local.get	4
	i32.load	16
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.gt_u
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	block   	
	local.get	13
	i32.eqz
	br_if   	0               # 0: down to label123
# %bb.1:                                # %if.then
	local.get	5
	call	_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv
	unreachable
.LBB128_2:                              # %if.end
	end_block                       # label123:
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	local.set	14
	local.get	4
	local.get	14
	i32.store	12
	local.get	4
	i32.load	12
	local.set	15
	local.get	4
	i32.load	16
	local.set	16
	i32.const	1
	local.set	17
	local.get	16
	local.get	17
	i32.shr_u
	local.set	18
	local.get	15
	local.set	19
	local.get	18
	local.set	20
	local.get	19
	local.get	20
	i32.ge_u
	local.set	21
	i32.const	1
	local.set	22
	local.get	21
	local.get	22
	i32.and 
	local.set	23
	block   	
	block   	
	local.get	23
	i32.eqz
	br_if   	0               # 0: down to label125
# %bb.3:                                # %if.then4
	local.get	4
	i32.load	16
	local.set	24
	local.get	4
	local.get	24
	i32.store	28
	br      	1               # 1: down to label124
.LBB128_4:                              # %if.end5
	end_block                       # label125:
	i32.const	8
	local.set	25
	local.get	4
	local.get	25
	i32.add 
	local.set	26
	local.get	26
	local.set	27
	i32.const	20
	local.set	28
	local.get	4
	local.get	28
	i32.add 
	local.set	29
	local.get	29
	local.set	30
	local.get	4
	i32.load	12
	local.set	31
	i32.const	1
	local.set	32
	local.get	31
	local.get	32
	i32.shl 
	local.set	33
	local.get	4
	local.get	33
	i32.store	8
	local.get	27
	local.get	30
	call	_ZNSt3__23maxImEERKT_S3_S3_
	local.set	34
	local.get	34
	i32.load	0
	local.set	35
	local.get	4
	local.get	35
	i32.store	28
.LBB128_5:                              # %return
	end_block                       # label124:
	local.get	4
	i32.load	28
	local.set	36
	i32.const	32
	local.set	37
	local.get	4
	local.get	37
	i32.add 
	local.set	38
	local.get	38
	global.set	__stack_pointer
	local.get	36
	return
	end_function
.Lfunc_end128:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm, .Lfunc_end128-_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_,"",@
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_ # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_ (i32, i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	4
	i32.const	16
	local.set	5
	local.get	4
	local.get	5
	i32.sub 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	6
	local.get	0
	i32.store	12
	local.get	6
	local.get	1
	i32.store	8
	local.get	6
	local.get	2
	i32.store	4
	local.get	6
	local.get	3
	i32.store	0
	local.get	6
	i32.load	12
	local.set	7
	local.get	6
	i32.load	8
	local.set	8
	local.get	6
	i32.load	4
	local.set	9
	local.get	6
	i32.load	0
	local.set	10
	local.get	7
	local.get	8
	local.get	9
	local.get	10
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_
	drop
	i32.const	16
	local.set	11
	local.get	6
	local.get	11
	i32.add 
	local.set	12
	local.get	12
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end129:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_, .Lfunc_end129-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC1EmmS3_
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE,"",@
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	6
	local.get	5
	i32.load	0
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	4
	i32.load	8
	local.set	9
	i32.const	4
	local.set	10
	local.get	9
	local.get	10
	i32.add 
	local.set	11
	local.get	6
	local.get	7
	local.get	8
	local.get	11
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_
	local.get	4
	i32.load	8
	local.set	12
	i32.const	4
	local.set	13
	local.get	12
	local.get	13
	i32.add 
	local.set	14
	local.get	5
	local.get	14
	call	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	i32.const	4
	local.set	15
	local.get	5
	local.get	15
	i32.add 
	local.set	16
	local.get	4
	i32.load	8
	local.set	17
	i32.const	8
	local.set	18
	local.get	17
	local.get	18
	i32.add 
	local.set	19
	local.get	16
	local.get	19
	call	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	local.get	5
	call	_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv
	local.set	20
	local.get	4
	i32.load	8
	local.set	21
	local.get	21
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	local.set	22
	local.get	20
	local.get	22
	call	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	local.get	4
	i32.load	8
	local.set	23
	local.get	23
	i32.load	4
	local.set	24
	local.get	4
	i32.load	8
	local.set	25
	local.get	25
	local.get	24
	i32.store	0
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv
	local.set	26
	local.get	5
	local.get	26
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm
	local.get	5
	call	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv
	i32.const	16
	local.set	27
	local.get	4
	local.get	27
	i32.add 
	local.set	28
	local.get	28
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end130:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE, .Lfunc_end130-_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev,"",@
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev
	drop
	i32.const	16
	local.set	5
	local.get	3
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	local.get	4
	return
	end_function
.Lfunc_end131:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev, .Lfunc_end131-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED1Ev
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv,"",@
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	8
	local.set	4
	local.get	3
	local.get	4
	i32.add 
	local.set	5
	local.get	5
	local.set	6
	i32.const	4
	local.set	7
	local.get	3
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	local.set	9
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	10
	local.get	10
	call	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	local.set	11
	local.get	11
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_
	local.set	12
	local.get	3
	local.get	12
	i32.store	8
	call	_ZNSt3__214numeric_limitsIlE3maxEv
	local.set	13
	local.get	3
	local.get	13
	i32.store	4
	local.get	6
	local.get	9
	call	_ZNSt3__23minImEERKT_S3_S3_
	local.set	14
	local.get	14
	i32.load	0
	local.set	15
	i32.const	16
	local.set	16
	local.get	3
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	local.get	15
	return
	end_function
.Lfunc_end132:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv, .Lfunc_end132-_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv
                                        # -- End function
	.section	.text._ZNSt3__23maxImEERKT_S3_S3_,"",@
	.hidden	_ZNSt3__23maxImEERKT_S3_S3_ # -- Begin function _ZNSt3__23maxImEERKT_S3_S3_
	.weak	_ZNSt3__23maxImEERKT_S3_S3_
	.type	_ZNSt3__23maxImEERKT_S3_S3_,@function
_ZNSt3__23maxImEERKT_S3_S3_:            # @_ZNSt3__23maxImEERKT_S3_S3_
	.functype	_ZNSt3__23maxImEERKT_S3_S3_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_
	local.set	7
	i32.const	16
	local.set	8
	local.get	4
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end133:
	.size	_ZNSt3__23maxImEERKT_S3_S3_, .Lfunc_end133-_ZNSt3__23maxImEERKT_S3_S3_
                                        # -- End function
	.section	.text._ZNSt3__23minImEERKT_S3_S3_,"",@
	.hidden	_ZNSt3__23minImEERKT_S3_S3_ # -- Begin function _ZNSt3__23minImEERKT_S3_S3_
	.weak	_ZNSt3__23minImEERKT_S3_S3_
	.type	_ZNSt3__23minImEERKT_S3_S3_,@function
_ZNSt3__23minImEERKT_S3_S3_:            # @_ZNSt3__23minImEERKT_S3_S3_
	.functype	_ZNSt3__23minImEERKT_S3_S3_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_
	local.set	7
	i32.const	16
	local.set	8
	local.get	4
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end134:
	.size	_ZNSt3__23minImEERKT_S3_S3_, .Lfunc_end134-_ZNSt3__23minImEERKT_S3_S3_
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_,"",@
	.hidden	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end135:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_, .Lfunc_end135-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_
                                        # -- End function
	.section	.text._ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv,"",@
	.hidden	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv # -- Begin function _ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.weak	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.type	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv,@function
_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv: # @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
	.functype	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end136:
	.size	_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv, .Lfunc_end136-_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv
                                        # -- End function
	.section	.text._ZNSt3__214numeric_limitsIlE3maxEv,"",@
	.hidden	_ZNSt3__214numeric_limitsIlE3maxEv # -- Begin function _ZNSt3__214numeric_limitsIlE3maxEv
	.weak	_ZNSt3__214numeric_limitsIlE3maxEv
	.type	_ZNSt3__214numeric_limitsIlE3maxEv,@function
_ZNSt3__214numeric_limitsIlE3maxEv:     # @_ZNSt3__214numeric_limitsIlE3maxEv
	.functype	_ZNSt3__214numeric_limitsIlE3maxEv () -> (i32)
	.local  	i32
# %bb.0:                                # %entry
	call	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv
	local.set	0
	local.get	0
	return
	end_function
.Lfunc_end137:
	.size	_ZNSt3__214numeric_limitsIlE3maxEv, .Lfunc_end137-_ZNSt3__214numeric_limitsIlE3maxEv
                                        # -- End function
	.section	.text._ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_,"",@
	.hidden	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ # -- Begin function _ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_
	.weak	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_
	.type	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_,@function
_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_: # @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_
	.functype	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.get	0
	i32.store	4
	local.get	4
	local.get	1
	i32.store	0
	local.get	4
	i32.load	0
	local.set	8
	local.get	4
	i32.load	4
	local.set	9
	local.get	7
	local.get	8
	local.get	9
	call	_ZNKSt3__26__lessImmEclERKmS3_
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	block   	
	block   	
	local.get	12
	i32.eqz
	br_if   	0               # 0: down to label127
# %bb.1:                                # %cond.true
	local.get	4
	i32.load	0
	local.set	13
	local.get	13
	local.set	14
	br      	1               # 1: down to label126
.LBB138_2:                              # %cond.false
	end_block                       # label127:
	local.get	4
	i32.load	4
	local.set	15
	local.get	15
	local.set	14
.LBB138_3:                              # %cond.end
	end_block                       # label126:
	local.get	14
	local.set	16
	i32.const	16
	local.set	17
	local.get	4
	local.get	17
	i32.add 
	local.set	18
	local.get	18
	global.set	__stack_pointer
	local.get	16
	return
	end_function
.Lfunc_end138:
	.size	_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_, .Lfunc_end138-_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_
                                        # -- End function
	.section	.text._ZNKSt3__26__lessImmEclERKmS3_,"",@
	.hidden	_ZNKSt3__26__lessImmEclERKmS3_ # -- Begin function _ZNKSt3__26__lessImmEclERKmS3_
	.weak	_ZNKSt3__26__lessImmEclERKmS3_
	.type	_ZNKSt3__26__lessImmEclERKmS3_,@function
_ZNKSt3__26__lessImmEclERKmS3_:         # @_ZNKSt3__26__lessImmEclERKmS3_
	.functype	_ZNKSt3__26__lessImmEclERKmS3_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	8
	local.set	6
	local.get	6
	i32.load	0
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	8
	i32.load	0
	local.set	9
	local.get	7
	local.set	10
	local.get	9
	local.set	11
	local.get	10
	local.get	11
	i32.lt_u
	local.set	12
	i32.const	1
	local.set	13
	local.get	12
	local.get	13
	i32.and 
	local.set	14
	local.get	14
	return
	end_function
.Lfunc_end139:
	.size	_ZNKSt3__26__lessImmEclERKmS3_, .Lfunc_end139-_ZNKSt3__26__lessImmEclERKmS3_
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_,"",@
	.hidden	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	4
	local.get	3
	i32.load	4
	local.set	4
	local.get	4
	call	_ZNKSt3__29allocatorIjE8max_sizeEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end140:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_, .Lfunc_end140-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_
                                        # -- End function
	.section	.text._ZNKSt3__29allocatorIjE8max_sizeEv,"",@
	.hidden	_ZNKSt3__29allocatorIjE8max_sizeEv # -- Begin function _ZNKSt3__29allocatorIjE8max_sizeEv
	.weak	_ZNKSt3__29allocatorIjE8max_sizeEv
	.type	_ZNKSt3__29allocatorIjE8max_sizeEv,@function
_ZNKSt3__29allocatorIjE8max_sizeEv:     # @_ZNKSt3__29allocatorIjE8max_sizeEv
	.functype	_ZNKSt3__29allocatorIjE8max_sizeEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	i32.const	1073741823
	local.set	4
	local.get	3
	local.get	0
	i32.store	12
	local.get	4
	return
	end_function
.Lfunc_end141:
	.size	_ZNKSt3__29allocatorIjE8max_sizeEv, .Lfunc_end141-_ZNKSt3__29allocatorIjE8max_sizeEv
                                        # -- End function
	.section	.text._ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv,"",@
	.hidden	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv # -- Begin function _ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.weak	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.type	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv,@function
_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv: # @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
	.functype	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end142:
	.size	_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv, .Lfunc_end142-_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv
                                        # -- End function
	.section	.text._ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv,"",@
	.hidden	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv # -- Begin function _ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.weak	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.type	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv,@function
_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv: # @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
	.functype	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end143:
	.size	_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv, .Lfunc_end143-_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv,"",@
	.hidden	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv # -- Begin function _ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv
	.weak	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv
	.type	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv,@function
_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv: # @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv
	.functype	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv () -> (i32)
	.local  	i32
# %bb.0:                                # %entry
	i32.const	2147483647
	local.set	0
	local.get	0
	return
	end_function
.Lfunc_end144:
	.size	_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv, .Lfunc_end144-_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv
                                        # -- End function
	.section	.text._ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_,"",@
	.hidden	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ # -- Begin function _ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_
	.weak	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_
	.type	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_,@function
_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_: # @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_
	.functype	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.get	0
	i32.store	4
	local.get	4
	local.get	1
	i32.store	0
	local.get	4
	i32.load	4
	local.set	8
	local.get	4
	i32.load	0
	local.set	9
	local.get	7
	local.get	8
	local.get	9
	call	_ZNKSt3__26__lessImmEclERKmS3_
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	block   	
	block   	
	local.get	12
	i32.eqz
	br_if   	0               # 0: down to label129
# %bb.1:                                # %cond.true
	local.get	4
	i32.load	0
	local.set	13
	local.get	13
	local.set	14
	br      	1               # 1: down to label128
.LBB145_2:                              # %cond.false
	end_block                       # label129:
	local.get	4
	i32.load	4
	local.set	15
	local.get	15
	local.set	14
.LBB145_3:                              # %cond.end
	end_block                       # label128:
	local.get	14
	local.set	16
	i32.const	16
	local.set	17
	local.get	4
	local.get	17
	i32.add 
	local.set	18
	local.get	18
	global.set	__stack_pointer
	local.get	16
	return
	end_function
.Lfunc_end145:
	.size	_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_, .Lfunc_end145-_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_,"",@
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ (i32, i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	4
	i32.const	32
	local.set	5
	local.get	4
	local.get	5
	i32.sub 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	i32.const	8
	local.set	7
	local.get	6
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	local.set	9
	i32.const	0
	local.set	10
	local.get	6
	local.get	0
	i32.store	24
	local.get	6
	local.get	1
	i32.store	20
	local.get	6
	local.get	2
	i32.store	16
	local.get	6
	local.get	3
	i32.store	12
	local.get	6
	i32.load	24
	local.set	11
	local.get	6
	local.get	11
	i32.store	28
	i32.const	12
	local.set	12
	local.get	11
	local.get	12
	i32.add 
	local.set	13
	local.get	6
	local.get	10
	i32.store	8
	local.get	6
	i32.load	12
	local.set	14
	local.get	13
	local.get	9
	local.get	14
	call	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_
	drop
	local.get	6
	i32.load	20
	local.set	15
	block   	
	block   	
	local.get	15
	i32.eqz
	br_if   	0               # 0: down to label131
# %bb.1:                                # %cond.true
	local.get	11
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	local.set	16
	local.get	6
	i32.load	20
	local.set	17
	local.get	16
	local.get	17
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m
	local.set	18
	local.get	18
	local.set	19
	br      	1               # 1: down to label130
.LBB146_2:                              # %cond.false
	end_block                       # label131:
	i32.const	0
	local.set	20
	local.get	20
	local.set	19
.LBB146_3:                              # %cond.end
	end_block                       # label130:
	local.get	19
	local.set	21
	local.get	11
	local.get	21
	i32.store	0
	local.get	11
	i32.load	0
	local.set	22
	local.get	6
	i32.load	16
	local.set	23
	i32.const	2
	local.set	24
	local.get	23
	local.get	24
	i32.shl 
	local.set	25
	local.get	22
	local.get	25
	i32.add 
	local.set	26
	local.get	11
	local.get	26
	i32.store	8
	local.get	11
	local.get	26
	i32.store	4
	local.get	11
	i32.load	0
	local.set	27
	local.get	6
	i32.load	20
	local.set	28
	i32.const	2
	local.set	29
	local.get	28
	local.get	29
	i32.shl 
	local.set	30
	local.get	27
	local.get	30
	i32.add 
	local.set	31
	local.get	11
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	local.set	32
	local.get	32
	local.get	31
	i32.store	0
	local.get	6
	i32.load	28
	local.set	33
	i32.const	32
	local.set	34
	local.get	6
	local.get	34
	i32.add 
	local.set	35
	local.get	35
	global.set	__stack_pointer
	local.get	33
	return
	end_function
.Lfunc_end146:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_, .Lfunc_end146-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_
	.type	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_,@function
_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_: # @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	5
	i32.load	4
	local.set	8
	local.get	6
	local.get	7
	local.get	8
	call	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_
	drop
	i32.const	16
	local.set	9
	local.get	5
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end147:
	.size	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_, .Lfunc_end147-_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC1IDnS4_EEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m,"",@
	.hidden	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	0
	local.set	5
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	6
	local.get	7
	local.get	5
	call	_ZNSt3__29allocatorIjE8allocateEmPKv
	local.set	8
	i32.const	16
	local.set	9
	local.get	4
	local.get	9
	i32.add 
	local.set	10
	local.get	10
	global.set	__stack_pointer
	local.get	8
	return
	end_function
.Lfunc_end148:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m, .Lfunc_end148-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv,"",@
	.hidden	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	12
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end149:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv, .Lfunc_end149-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv,"",@
	.hidden	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	12
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end150:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv, .Lfunc_end150-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_,"",@
	.weak	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ # -- Begin function _ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_
	.type	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_,@function
_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_: # @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_
	.functype	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	7
	call	_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE
	local.set	8
	local.get	6
	local.get	8
	call	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_
	drop
	i32.const	4
	local.set	9
	local.get	6
	local.get	9
	i32.add 
	local.set	10
	local.get	5
	i32.load	4
	local.set	11
	local.get	11
	call	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
	local.set	12
	local.get	10
	local.get	12
	call	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_
	drop
	i32.const	16
	local.set	13
	local.get	5
	local.get	13
	i32.add 
	local.set	14
	local.get	14
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end151:
	.size	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_, .Lfunc_end151-_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_
                                        # -- End function
	.section	.text._ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE,"",@
	.hidden	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE # -- Begin function _ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
	.weak	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
	.type	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE,@function
_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE: # @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
	.functype	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end152:
	.size	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE, .Lfunc_end152-_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_,"",@
	.weak	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ # -- Begin function _ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_
	.type	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_,@function
_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_: # @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_
	.functype	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	6
	call	_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE
	local.set	7
	local.get	5
	local.get	7
	i32.store	0
	i32.const	16
	local.set	8
	local.get	4
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end153:
	.size	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_, .Lfunc_end153-_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_
                                        # -- End function
	.section	.text._ZNSt3__29allocatorIjE8allocateEmPKv,"",@
	.hidden	_ZNSt3__29allocatorIjE8allocateEmPKv # -- Begin function _ZNSt3__29allocatorIjE8allocateEmPKv
	.weak	_ZNSt3__29allocatorIjE8allocateEmPKv
	.type	_ZNSt3__29allocatorIjE8allocateEmPKv,@function
_ZNSt3__29allocatorIjE8allocateEmPKv:   # @_ZNSt3__29allocatorIjE8allocateEmPKv
	.functype	_ZNSt3__29allocatorIjE8allocateEmPKv (i32, i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	3
	i32.const	16
	local.set	4
	local.get	3
	local.get	4
	i32.sub 
	local.set	5
	local.get	5
	global.set	__stack_pointer
	local.get	5
	local.get	0
	i32.store	12
	local.get	5
	local.get	1
	i32.store	8
	local.get	5
	local.get	2
	i32.store	4
	local.get	5
	i32.load	12
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	6
	call	_ZNKSt3__29allocatorIjE8max_sizeEv
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.gt_u
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	block   	
	local.get	13
	i32.eqz
	br_if   	0               # 0: down to label132
# %bb.1:                                # %if.then
	i32.const	.L.str.23@MBREL
	local.set	14
	global.get	__memory_base
	local.set	15
	local.get	15
	local.get	14
	i32.add 
	local.set	16
	local.get	16
	call	_ZNSt3__220__throw_length_errorEPKc
	unreachable
.LBB154_2:                              # %if.end
	end_block                       # label132:
	i32.const	4
	local.set	17
	local.get	5
	i32.load	8
	local.set	18
	i32.const	2
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	20
	local.get	17
	call	_ZNSt3__217__libcpp_allocateEmm
	local.set	21
	i32.const	16
	local.set	22
	local.get	5
	local.get	22
	i32.add 
	local.set	23
	local.get	23
	global.set	__stack_pointer
	local.get	21
	return
	end_function
.Lfunc_end154:
	.size	_ZNSt3__29allocatorIjE8allocateEmPKv, .Lfunc_end154-_ZNSt3__29allocatorIjE8allocateEmPKv
                                        # -- End function
	.section	.text._ZNSt3__220__throw_length_errorEPKc,"",@
	.hidden	_ZNSt3__220__throw_length_errorEPKc # -- Begin function _ZNSt3__220__throw_length_errorEPKc
	.weak	_ZNSt3__220__throw_length_errorEPKc
	.type	_ZNSt3__220__throw_length_errorEPKc,@function
_ZNSt3__220__throw_length_errorEPKc:    # @_ZNSt3__220__throw_length_errorEPKc
	.functype	_ZNSt3__220__throw_length_errorEPKc (i32) -> ()
	.local  	i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	call	abort
	unreachable
	end_function
.Lfunc_end155:
	.size	_ZNSt3__220__throw_length_errorEPKc, .Lfunc_end155-_ZNSt3__220__throw_length_errorEPKc
                                        # -- End function
	.section	.text._ZNSt3__217__libcpp_allocateEmm,"",@
	.hidden	_ZNSt3__217__libcpp_allocateEmm # -- Begin function _ZNSt3__217__libcpp_allocateEmm
	.weak	_ZNSt3__217__libcpp_allocateEmm
	.type	_ZNSt3__217__libcpp_allocateEmm,@function
_ZNSt3__217__libcpp_allocateEmm:        # @_ZNSt3__217__libcpp_allocateEmm
	.functype	_ZNSt3__217__libcpp_allocateEmm (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	call	_Znwm
	local.set	6
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	6
	return
	end_function
.Lfunc_end156:
	.size	_ZNSt3__217__libcpp_allocateEmm, .Lfunc_end156-_ZNSt3__217__libcpp_allocateEmm
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv,"",@
	.hidden	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv # -- Begin function _ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv
	.weak	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv
	.type	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv,@function
_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv: # @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv
	.functype	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	4
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end157:
	.size	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv, .Lfunc_end157-_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv
                                        # -- End function
	.section	.text._ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv,"",@
	.hidden	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv # -- Begin function _ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv
	.weak	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv
	.type	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv,@function
_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv: # @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv
	.functype	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end158:
	.size	_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv, .Lfunc_end158-_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv
                                        # -- End function
	.section	.text._ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv,"",@
	.hidden	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv # -- Begin function _ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.weak	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.type	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv,@function
_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv: # @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.functype	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end159:
	.size	_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv, .Lfunc_end159-_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
                                        # -- End function
	.section	.text._ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_,"",@
	.weak	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ # -- Begin function _ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_
	.type	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_,@function
_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_: # @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_
	.functype	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ (i32, i32, i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	4
	i32.const	32
	local.set	5
	local.get	4
	local.get	5
	i32.sub 
	local.set	6
	local.get	6
	global.set	__stack_pointer
	i32.const	0
	local.set	7
	local.get	6
	local.get	0
	i32.store	28
	local.get	6
	local.get	1
	i32.store	24
	local.get	6
	local.get	2
	i32.store	20
	local.get	6
	local.get	3
	i32.store	16
	local.get	6
	i32.load	20
	local.set	8
	local.get	6
	i32.load	24
	local.set	9
	local.get	8
	local.get	9
	i32.sub 
	local.set	10
	i32.const	2
	local.set	11
	local.get	10
	local.get	11
	i32.shr_s
	local.set	12
	local.get	6
	local.get	12
	i32.store	12
	local.get	6
	i32.load	12
	local.set	13
	local.get	6
	i32.load	16
	local.set	14
	local.get	14
	i32.load	0
	local.set	15
	local.get	7
	local.get	13
	i32.sub 
	local.set	16
	i32.const	2
	local.set	17
	local.get	16
	local.get	17
	i32.shl 
	local.set	18
	local.get	15
	local.get	18
	i32.add 
	local.set	19
	local.get	14
	local.get	19
	i32.store	0
	local.get	6
	i32.load	12
	local.set	20
	local.get	20
	local.set	21
	local.get	7
	local.set	22
	local.get	21
	local.get	22
	i32.gt_s
	local.set	23
	i32.const	1
	local.set	24
	local.get	23
	local.get	24
	i32.and 
	local.set	25
	block   	
	local.get	25
	i32.eqz
	br_if   	0               # 0: down to label133
# %bb.1:                                # %if.then
	local.get	6
	i32.load	16
	local.set	26
	local.get	26
	i32.load	0
	local.set	27
	local.get	6
	i32.load	24
	local.set	28
	local.get	6
	i32.load	12
	local.set	29
	i32.const	2
	local.set	30
	local.get	29
	local.get	30
	i32.shl 
	local.set	31
	local.get	27
	local.get	28
	local.get	31
	call	memcpy
	drop
.LBB160_2:                              # %if.end
	end_block                       # label133:
	i32.const	32
	local.set	32
	local.get	6
	local.get	32
	i32.add 
	local.set	33
	local.get	33
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end160:
	.size	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_, .Lfunc_end160-_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_
                                        # -- End function
	.section	.text._ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_,"",@
	.hidden	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ # -- Begin function _ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	.weak	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	.type	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_,@function
_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_: # @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
	.functype	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	4
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	8
	local.get	8
	call	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	local.set	9
	local.get	9
	i32.load	0
	local.set	10
	local.get	4
	local.get	10
	i32.store	4
	local.get	4
	i32.load	8
	local.set	11
	local.get	11
	call	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	local.set	12
	local.get	12
	i32.load	0
	local.set	13
	local.get	4
	i32.load	12
	local.set	14
	local.get	14
	local.get	13
	i32.store	0
	local.get	7
	call	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	local.set	15
	local.get	15
	i32.load	0
	local.set	16
	local.get	4
	i32.load	8
	local.set	17
	local.get	17
	local.get	16
	i32.store	0
	i32.const	16
	local.set	18
	local.get	4
	local.get	18
	i32.add 
	local.set	19
	local.get	19
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end161:
	.size	_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_, .Lfunc_end161-_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_
                                        # -- End function
	.section	.text._ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm,"",@
	.hidden	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm # -- Begin function _ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm
	.weak	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm
	.type	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm,@function
_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm: # @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm
	.functype	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	6
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	7
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	local.set	8
	i32.const	2
	local.set	9
	local.get	8
	local.get	9
	i32.shl 
	local.set	10
	local.get	7
	local.get	10
	i32.add 
	local.set	11
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	12
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv
	local.set	13
	i32.const	2
	local.set	14
	local.get	13
	local.get	14
	i32.shl 
	local.set	15
	local.get	12
	local.get	15
	i32.add 
	local.set	16
	local.get	5
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv
	local.set	17
	local.get	4
	i32.load	8
	local.set	18
	i32.const	2
	local.set	19
	local.get	18
	local.get	19
	i32.shl 
	local.set	20
	local.get	17
	local.get	20
	i32.add 
	local.set	21
	local.get	5
	local.get	6
	local.get	11
	local.get	16
	local.get	21
	call	_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	i32.const	16
	local.set	22
	local.get	4
	local.get	22
	i32.add 
	local.set	23
	local.get	23
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end162:
	.size	_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm, .Lfunc_end162-_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv (i32) -> ()
	.local  	i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	return
	end_function
.Lfunc_end163:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv, .Lfunc_end163-_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv
                                        # -- End function
	.section	.text._ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_,"",@
	.hidden	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ # -- Begin function _ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	.weak	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	.type	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_,@function
_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_: # @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
	.functype	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ (i32) -> (i32)
	.local  	i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	return
	end_function
.Lfunc_end164:
	.size	_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_, .Lfunc_end164-_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev,"",@
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	i32.const	0
	local.set	4
	local.get	3
	local.get	0
	i32.store	8
	local.get	3
	i32.load	8
	local.set	5
	local.get	3
	local.get	5
	i32.store	12
	local.get	5
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv
	local.get	5
	i32.load	0
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.set	8
	local.get	7
	local.get	8
	i32.ne  
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	block   	
	local.get	11
	i32.eqz
	br_if   	0               # 0: down to label134
# %bb.1:                                # %if.then
	local.get	5
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	local.set	12
	local.get	5
	i32.load	0
	local.set	13
	local.get	5
	call	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv
	local.set	14
	local.get	12
	local.get	13
	local.get	14
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm
.LBB165_2:                              # %if.end
	end_block                       # label134:
	local.get	3
	i32.load	12
	local.set	15
	i32.const	16
	local.set	16
	local.get	3
	local.get	16
	i32.add 
	local.set	17
	local.get	17
	global.set	__stack_pointer
	local.get	15
	return
	end_function
.Lfunc_end165:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev, .Lfunc_end165-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv,"",@
	.hidden	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv (i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	4
	local.set	5
	local.get	4
	local.get	5
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end166:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv, .Lfunc_end166-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv
                                        # -- End function
	.section	.text._ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv,"",@
	.hidden	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv # -- Begin function _ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv
	.weak	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv
	.type	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv,@function
_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv: # @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv
	.functype	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	local.set	5
	local.get	5
	i32.load	0
	local.set	6
	local.get	4
	i32.load	0
	local.set	7
	local.get	6
	local.get	7
	i32.sub 
	local.set	8
	i32.const	2
	local.set	9
	local.get	8
	local.get	9
	i32.shr_s
	local.set	10
	i32.const	16
	local.set	11
	local.get	3
	local.get	11
	i32.add 
	local.set	12
	local.get	12
	global.set	__stack_pointer
	local.get	10
	return
	end_function
.Lfunc_end167:
	.size	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv, .Lfunc_end167-_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj,"",@
	.hidden	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end168:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj, .Lfunc_end168-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj
                                        # -- End function
	.section	.text._ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE,"",@
	.hidden	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE # -- Begin function _ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE
	.weak	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE
	.type	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE,@function
_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE: # @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE
	.functype	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE (i32, i32) -> ()
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	4
	local.get	4
	local.get	1
	i32.store	0
	local.get	4
	i32.load	4
	local.set	5
.LBB169_1:                              # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	block   	
	loop    	                # label136:
	local.get	4
	i32.load	0
	local.set	6
	local.get	5
	i32.load	8
	local.set	7
	local.get	6
	local.set	8
	local.get	7
	local.set	9
	local.get	8
	local.get	9
	i32.ne  
	local.set	10
	i32.const	1
	local.set	11
	local.get	10
	local.get	11
	i32.and 
	local.set	12
	local.get	12
	i32.eqz
	br_if   	1               # 1: down to label135
# %bb.2:                                # %while.body
                                        #   in Loop: Header=BB169_1 Depth=1
	local.get	5
	call	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv
	local.set	13
	local.get	5
	i32.load	8
	local.set	14
	i32.const	-4
	local.set	15
	local.get	14
	local.get	15
	i32.add 
	local.set	16
	local.get	5
	local.get	16
	i32.store	8
	local.get	16
	call	_ZNSt3__212__to_addressIjEEPT_S2_
	local.set	17
	local.get	13
	local.get	17
	call	_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_
	br      	0               # 0: up to label136
.LBB169_3:                              # %while.end
	end_loop
	end_block                       # label135:
	i32.const	16
	local.set	18
	local.get	4
	local.get	18
	i32.add 
	local.set	19
	local.get	19
	global.set	__stack_pointer
	return
	end_function
.Lfunc_end169:
	.size	_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE, .Lfunc_end169-_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE
                                        # -- End function
	.section	.text._ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv,"",@
	.hidden	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv # -- Begin function _ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.weak	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.type	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv,@function
_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv: # @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
	.functype	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	i32.const	12
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	call	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	local.set	7
	i32.const	16
	local.set	8
	local.get	3
	local.get	8
	i32.add 
	local.set	9
	local.get	9
	global.set	__stack_pointer
	local.get	7
	return
	end_function
.Lfunc_end170:
	.size	_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv, .Lfunc_end170-_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv
                                        # -- End function
	.section	.text._ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv,"",@
	.hidden	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv # -- Begin function _ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.weak	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.type	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv,@function
_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv: # @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
	.functype	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	global.set	__stack_pointer
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	call	_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv
	local.set	5
	i32.const	16
	local.set	6
	local.get	3
	local.get	6
	i32.add 
	local.set	7
	local.get	7
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end171:
	.size	_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv, .Lfunc_end171-_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv
                                        # -- End function
	.section	.text._ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj,"",@
	.hidden	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj # -- Begin function _ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
	.weak	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
	.type	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj,@function
_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj: # @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
	.functype	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	i32.const	8
	local.set	5
	local.get	4
	local.get	5
	i32.add 
	local.set	6
	local.get	6
	local.set	7
	local.get	4
	local.get	0
	i32.store	4
	local.get	4
	local.get	1
	i32.store	0
	local.get	4
	i32.load	0
	local.set	8
	local.get	7
	local.get	8
	call	_ZNSt3__211__wrap_iterIPjEC1ES1_
	drop
	local.get	4
	i32.load	8
	local.set	9
	i32.const	16
	local.set	10
	local.get	4
	local.get	10
	i32.add 
	local.set	11
	local.get	11
	global.set	__stack_pointer
	local.get	9
	return
	end_function
.Lfunc_end172:
	.size	_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj, .Lfunc_end172-_ZNSt3__26vectorIjNS_9allocatorIjEEE11__make_iterEPj
                                        # -- End function
	.section	.text._ZNSt3__211__wrap_iterIPjEC1ES1_,"",@
	.hidden	_ZNSt3__211__wrap_iterIPjEC1ES1_ # -- Begin function _ZNSt3__211__wrap_iterIPjEC1ES1_
	.weak	_ZNSt3__211__wrap_iterIPjEC1ES1_
	.type	_ZNSt3__211__wrap_iterIPjEC1ES1_,@function
_ZNSt3__211__wrap_iterIPjEC1ES1_:       # @_ZNSt3__211__wrap_iterIPjEC1ES1_
	.functype	_ZNSt3__211__wrap_iterIPjEC1ES1_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__211__wrap_iterIPjEC2ES1_
	drop
	i32.const	16
	local.set	7
	local.get	4
	local.get	7
	i32.add 
	local.set	8
	local.get	8
	global.set	__stack_pointer
	local.get	5
	return
	end_function
.Lfunc_end173:
	.size	_ZNSt3__211__wrap_iterIPjEC1ES1_, .Lfunc_end173-_ZNSt3__211__wrap_iterIPjEC1ES1_
                                        # -- End function
	.section	.text._ZNSt3__211__wrap_iterIPjEC2ES1_,"",@
	.hidden	_ZNSt3__211__wrap_iterIPjEC2ES1_ # -- Begin function _ZNSt3__211__wrap_iterIPjEC2ES1_
	.weak	_ZNSt3__211__wrap_iterIPjEC2ES1_
	.type	_ZNSt3__211__wrap_iterIPjEC2ES1_,@function
_ZNSt3__211__wrap_iterIPjEC2ES1_:       # @_ZNSt3__211__wrap_iterIPjEC2ES1_
	.functype	_ZNSt3__211__wrap_iterIPjEC2ES1_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	i32.store	0
	local.get	5
	return
	end_function
.Lfunc_end174:
	.size	_ZNSt3__211__wrap_iterIPjEC2ES1_, .Lfunc_end174-_ZNSt3__211__wrap_iterIPjEC2ES1_
                                        # -- End function
	.section	.text._ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_,"",@
	.hidden	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_ # -- Begin function _ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_
	.weak	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_
	.type	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_,@function
_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_: # @_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_
	.functype	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_ (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	4
	i32.load	8
	local.set	6
	local.get	5
	local.get	6
	call	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE
	local.set	7
	i32.const	-1
	local.set	8
	local.get	7
	local.get	8
	i32.xor 
	local.set	9
	i32.const	1
	local.set	10
	local.get	9
	local.get	10
	i32.and 
	local.set	11
	i32.const	16
	local.set	12
	local.get	4
	local.get	12
	i32.add 
	local.set	13
	local.get	13
	global.set	__stack_pointer
	local.get	11
	return
	end_function
.Lfunc_end175:
	.size	_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_, .Lfunc_end175-_ZNSt3__2neIPjEEbRKNS_11__wrap_iterIT_EES6_
                                        # -- End function
	.section	.text._ZNKSt3__211__wrap_iterIPjEdeEv,"",@
	.hidden	_ZNKSt3__211__wrap_iterIPjEdeEv # -- Begin function _ZNKSt3__211__wrap_iterIPjEdeEv
	.weak	_ZNKSt3__211__wrap_iterIPjEdeEv
	.type	_ZNKSt3__211__wrap_iterIPjEdeEv,@function
_ZNKSt3__211__wrap_iterIPjEdeEv:        # @_ZNKSt3__211__wrap_iterIPjEdeEv
	.functype	_ZNKSt3__211__wrap_iterIPjEdeEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end176:
	.size	_ZNKSt3__211__wrap_iterIPjEdeEv, .Lfunc_end176-_ZNKSt3__211__wrap_iterIPjEdeEv
                                        # -- End function
	.section	.text._ZNSt3__211__wrap_iterIPjEppEv,"",@
	.hidden	_ZNSt3__211__wrap_iterIPjEppEv # -- Begin function _ZNSt3__211__wrap_iterIPjEppEv
	.weak	_ZNSt3__211__wrap_iterIPjEppEv
	.type	_ZNSt3__211__wrap_iterIPjEppEv,@function
_ZNSt3__211__wrap_iterIPjEppEv:         # @_ZNSt3__211__wrap_iterIPjEppEv
	.functype	_ZNSt3__211__wrap_iterIPjEppEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	i32.const	4
	local.set	6
	local.get	5
	local.get	6
	i32.add 
	local.set	7
	local.get	4
	local.get	7
	i32.store	0
	local.get	4
	return
	end_function
.Lfunc_end177:
	.size	_ZNSt3__211__wrap_iterIPjEppEv, .Lfunc_end177-_ZNSt3__211__wrap_iterIPjEppEv
                                        # -- End function
	.section	.text._ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE,"",@
	.hidden	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE # -- Begin function _ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE
	.weak	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE
	.type	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE,@function
_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE: # @_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE
	.functype	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE (i32, i32) -> (i32)
	.local  	i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	2
	i32.const	16
	local.set	3
	local.get	2
	local.get	3
	i32.sub 
	local.set	4
	local.get	4
	global.set	__stack_pointer
	local.get	4
	local.get	0
	i32.store	12
	local.get	4
	local.get	1
	i32.store	8
	local.get	4
	i32.load	12
	local.set	5
	local.get	5
	call	_ZNKSt3__211__wrap_iterIPjE4baseEv
	local.set	6
	local.get	4
	i32.load	8
	local.set	7
	local.get	7
	call	_ZNKSt3__211__wrap_iterIPjE4baseEv
	local.set	8
	local.get	6
	local.set	9
	local.get	8
	local.set	10
	local.get	9
	local.get	10
	i32.eq  
	local.set	11
	i32.const	1
	local.set	12
	local.get	11
	local.get	12
	i32.and 
	local.set	13
	i32.const	16
	local.set	14
	local.get	4
	local.get	14
	i32.add 
	local.set	15
	local.get	15
	global.set	__stack_pointer
	local.get	13
	return
	end_function
.Lfunc_end178:
	.size	_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE, .Lfunc_end178-_ZNSt3__2eqIPjS1_EEbRKNS_11__wrap_iterIT_EERKNS2_IT0_EE
                                        # -- End function
	.section	.text._ZNKSt3__211__wrap_iterIPjE4baseEv,"",@
	.hidden	_ZNKSt3__211__wrap_iterIPjE4baseEv # -- Begin function _ZNKSt3__211__wrap_iterIPjE4baseEv
	.weak	_ZNKSt3__211__wrap_iterIPjE4baseEv
	.type	_ZNKSt3__211__wrap_iterIPjE4baseEv,@function
_ZNKSt3__211__wrap_iterIPjE4baseEv:     # @_ZNKSt3__211__wrap_iterIPjE4baseEv
	.functype	_ZNKSt3__211__wrap_iterIPjE4baseEv (i32) -> (i32)
	.local  	i32, i32, i32, i32, i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	local.set	1
	i32.const	16
	local.set	2
	local.get	1
	local.get	2
	i32.sub 
	local.set	3
	local.get	3
	local.get	0
	i32.store	12
	local.get	3
	i32.load	12
	local.set	4
	local.get	4
	i32.load	0
	local.set	5
	local.get	5
	return
	end_function
.Lfunc_end179:
	.size	_ZNKSt3__211__wrap_iterIPjE4baseEv, .Lfunc_end179-_ZNKSt3__211__wrap_iterIPjE4baseEv
                                        # -- End function
	.type	usa,@object             # @usa
	.section	.bss.usa,"",@
	.globl	usa
	.p2align	4
usa:
	.skip	2048
	.size	usa, 2048

	.type	sa,@object              # @sa
	.section	.bss.sa,"",@
	.globl	sa
	.p2align	4
sa:
	.skip	2048
	.size	sa, 2048

	.type	sb,@object              # @sb
	.section	.bss.sb,"",@
	.globl	sb
	.p2align	4
sb:
	.skip	2048
	.size	sb, 2048

	.type	sc,@object              # @sc
	.section	.bss.sc,"",@
	.globl	sc
	.p2align	4
sc:
	.skip	2048
	.size	sc, 2048

	.type	ua,@object              # @ua
	.section	.bss.ua,"",@
	.globl	ua
	.p2align	4
ua:
	.skip	4096
	.size	ua, 4096

	.type	ia,@object              # @ia
	.section	.bss.ia,"",@
	.globl	ia
	.p2align	4
ia:
	.skip	4096
	.size	ia, 4096

	.type	ib,@object              # @ib
	.section	.bss.ib,"",@
	.globl	ib
	.p2align	4
ib:
	.skip	4096
	.size	ib, 4096

	.type	ic,@object              # @ic
	.section	.bss.ic,"",@
	.globl	ic
	.p2align	4
ic:
	.skip	4096
	.size	ic, 4096

	.type	ub,@object              # @ub
	.section	.bss.ub,"",@
	.globl	ub
	.p2align	4
ub:
	.skip	4096
	.size	ub, 4096

	.type	uc,@object              # @uc
	.section	.bss.uc,"",@
	.globl	uc
	.p2align	4
uc:
	.skip	4096
	.size	uc, 4096

	.type	fa,@object              # @fa
	.section	.bss.fa,"",@
	.globl	fa
	.p2align	4
fa:
	.skip	4096
	.size	fa, 4096

	.type	fb,@object              # @fb
	.section	.bss.fb,"",@
	.globl	fb
	.p2align	4
fb:
	.skip	4096
	.size	fb, 4096

	.type	da,@object              # @da
	.section	.bss.da,"",@
	.globl	da
	.p2align	4
da:
	.skip	4096
	.size	da, 4096

	.type	db,@object              # @db
	.section	.bss.db,"",@
	.globl	db
	.p2align	4
db:
	.skip	4096
	.size	db, 4096

	.type	dc,@object              # @dc
	.section	.bss.dc,"",@
	.globl	dc
	.p2align	4
dc:
	.skip	4096
	.size	dc, 4096

	.type	dd,@object              # @dd
	.section	.bss.dd,"",@
	.globl	dd
	.p2align	4
dd:
	.skip	4096
	.size	dd, 4096

	.type	dj,@object              # @dj
	.section	.bss.dj,"",@
	.globl	dj
	.p2align	4
dj:
	.skip	4096
	.size	dj, 4096

	.type	s,@object               # @s
	.section	.bss.s,"",@
	.globl	s
	.p2align	2
s:
	.skip	4096
	.size	s, 4096

	.type	a,@object               # @a
	.section	.bss.a,"",@
	.globl	a
	.p2align	4
a:
	.skip	8192
	.size	a, 8192

	.type	b,@object               # @b
	.section	.bss.b,"",@
	.globl	b
	.p2align	4
b:
	.skip	8192
	.size	b, 8192

	.type	c,@object               # @c
	.section	.bss.c,"",@
	.globl	c
	.p2align	4
c:
	.skip	8192
	.size	c, 8192

	.type	d,@object               # @d
	.section	.bss.d,"",@
	.globl	d
	.p2align	4
d:
	.skip	8192
	.size	d, 8192

	.type	G,@object               # @G
	.section	.bss.G,"",@
	.globl	G
	.p2align	4
G:
	.skip	131072
	.size	G, 131072

	.type	.L.str,@object          # @.str
	.section	.rodata..L.str,"",@
.L.str:
	.asciz	"Example1"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata..L.str.1,"",@
.L.str.1:
	.asciz	"Example2a"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata..L.str.2,"",@
.L.str.2:
	.asciz	"Example2b"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata..L.str.3,"",@
.L.str.3:
	.asciz	"Example3"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata..L.str.4,"",@
.L.str.4:
	.asciz	"Example4a"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata..L.str.5,"",@
.L.str.5:
	.asciz	"Example4b"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata..L.str.6,"",@
.L.str.6:
	.asciz	"Example4c"
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata..L.str.7,"",@
.L.str.7:
	.asciz	"Example7"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata..L.str.8,"",@
.L.str.8:
	.asciz	"Example8"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata..L.str.9,"",@
.L.str.9:
	.asciz	"Example9"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata..L.str.10,"",@
.L.str.10:
	.asciz	"Example10a"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata..L.str.11,"",@
.L.str.11:
	.asciz	"Example10b"
	.size	.L.str.11, 11

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata..L.str.12,"",@
.L.str.12:
	.asciz	"Example11"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata..L.str.13,"",@
.L.str.13:
	.asciz	"Example12"
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
	.section	.rodata..L.str.14,"",@
.L.str.14:
	.asciz	"Example23"
	.size	.L.str.14, 10

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata..L.str.15,"",@
.L.str.15:
	.asciz	"Example24"
	.size	.L.str.15, 10

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata..L.str.16,"",@
.L.str.16:
	.asciz	"Example25"
	.size	.L.str.16, 10

	.type	.L.str.17,@object       # @.str.17
	.section	.rodata..L.str.17,"",@
.L.str.17:
	.asciz	"Results: ("
	.size	.L.str.17, 11

	.type	.L.str.18,@object       # @.str.18
	.section	.rodata..L.str.18,"",@
.L.str.18:
	.asciz	"):"
	.size	.L.str.18, 3

	.type	.L.str.19,@object       # @.str.19
	.section	.rodata..L.str.19,"",@
.L.str.19:
	.asciz	" "
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
	.section	.rodata..L.str.20,"",@
.L.str.20:
	.asciz	"\n"
	.size	.L.str.20, 2

	.type	.L.str.21,@object       # @.str.21
	.section	.rodata..L.str.21,"",@
.L.str.21:
	.asciz	", "
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
	.section	.rodata..L.str.22,"",@
.L.str.22:
	.asciz	", msec\n"
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata..L.str.23,"",@
.L.str.23:
	.asciz	"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size"
	.size	.L.str.23, 68

	.ident	"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"
	.globaltype	__stack_pointer, i32
	.globaltype	__table_base, i32
	.globaltype	__memory_base, i32
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEi (i32, i32) -> (i32)
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEj (i32, i32) -> (i32)
	.functype	gettimeofday (i32, i32) -> (i32)
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEl (i32, i32) -> (i32)
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_ (i32, i32) -> (i32)
	.functype	_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev (i32) -> (i32)
	.functype	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev (i32) -> (i32)
	.functype	_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc (i32, i32, i32) -> ()
	.functype	_ZNKSt3__28ios_base6getlocEv (i32, i32) -> ()
	.functype	_ZNSt3__26localeD1Ev (i32) -> (i32)
	.functype	_ZNKSt3__26locale9use_facetERNS0_2idE (i32, i32) -> (i32)
	.functype	_ZNSt3__28ios_base5clearEj (i32, i32) -> ()
	.functype	strlen (i32) -> (i32)
	.functype	_ZdlPv (i32) -> ()
	.functype	_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv (i32) -> ()
	.functype	abort () -> ()
	.functype	_Znwm (i32) -> (i32)
	.size	_ZNSt3__24coutE, 84
	.size	_ZNSt3__25ctypeIcE2idE, 8
	.section	.custom_section.producers,"",@
	.int8	1
	.int8	12
	.ascii	"processed-by"
	.int8	1
	.int8	5
	.ascii	"clang"
	.ascii	"\206\001"
	.ascii	"11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"
	.section	.rodata..L.str.23,"",@
