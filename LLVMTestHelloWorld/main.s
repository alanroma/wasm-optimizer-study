	.text
	.file	"test.c"
	.section	.text.__original_main,"",@
	.hidden	__original_main         # -- Begin function __original_main
	.globl	__original_main
	.type	__original_main,@function
__original_main:                        # @__original_main
	.functype	__original_main () -> (i32)
	.local  	i32
# %bb.0:                                # %entry
	global.get	__stack_pointer
	i32.const	16
	i32.sub 
	local.tee	0
	global.set	__stack_pointer
	local.get	0
	i32.const	0
	i32.store	12
	i32.const	.L.str
	i32.const	0
	i32.call	printf
	drop
	local.get	0
	i32.const	16
	i32.add 
	global.set	__stack_pointer
	i32.const	0
                                        # fallthrough-return
	end_function
.Lfunc_end0:
	.size	__original_main, .Lfunc_end0-__original_main
                                        # -- End function
	.section	.text.main,"",@
	.hidden	main                    # -- Begin function main
	.globl	main
	.type	main,@function
main:                                   # @main
	.functype	main (i32, i32) -> (i32)
# %bb.0:                                # %body
	i32.call	__original_main
                                        # fallthrough-return
	end_function
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
                                        # -- End function
	.type	.L.str,@object          # @.str
	.section	.rodata..L.str,"",@
.L.str:
	.asciz	"Hello, World!"
	.size	.L.str, 14

	.ident	"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"
	.globaltype	__stack_pointer, i32
	.functype	printf (i32, i32) -> (i32)
	.section	.custom_section.producers,"",@
	.int8	1
	.int8	12
	.ascii	"processed-by"
	.int8	1
	.int8	5
	.ascii	"clang"
	.ascii	"\206\001"
	.ascii	"11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"
	.section	.rodata..L.str,"",@
