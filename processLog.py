#!/usr/bin/python3
import os
import sys

if len(sys.argv) < 3:
    print('You need to specify an emcc output log to process')
    sys.exit()

input_path = sys.argv[1]
output_path = sys.argv[2]
#Fix output dir
capturedLines = []
with open(input_path, 'r') as reader:
    filelines = reader.readlines()
    for line in filelines:
        if '/home/alan/emsdk/upstream/bin/clang' in line and '--version' not in line:
            line = line.replace('shared:DEBUG: successfully executed ','')
            commandBits = line.split(' ')
            try:
                indexOfOutputFlag = commandBits.index('-o')
                indexOfOutputName = indexOfOutputFlag + 1
                outputName = commandBits[indexOfOutputName]
                newOutputName = os.path.basename(outputName).replace('.o', '.ll')
                newOutputDir = output_path 
                newOutputPath = os.path.join(newOutputDir, newOutputName)
                newFirstBit = f'{commandBits[0]} -Xclang -disable-llvm-passes -Xclang -emit-llvm '
                newCommand = newFirstBit + ' '.join(commandBits[1:indexOfOutputName]) +f' {newOutputPath} ' + ' '.join(commandBits[indexOfOutputName + 1:])

                capturedLines.append(newCommand)
                print(newCommand)
                os.system(newCommand)
            except Exception as err:
                print(line)
