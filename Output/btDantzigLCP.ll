; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/MLCPSolvers/btDantzigLCP.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/MLCPSolvers/btDantzigLCP.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btLCP = type { i32, i32, i32, i32, i32, float**, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btDantzigScratchMemory = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, float**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }

$_Z9btSetZeroIfEvPT_i = comdat any

$_Z10btLargeDotPKfS0_i = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_Z29btEstimateLDLTAddTLTmpbufSizei = comdat any

$_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayIbE6resizeEiRKb = comdat any

$_ZN20btAlignedObjectArrayIbEixEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIPfEixEi = comdat any

$_ZNK5btLCP6getNubEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK5btLCP12AiC_times_qCEiPf = comdat any

$_ZNK5btLCP12AiN_times_qNEiPf = comdat any

$_ZN5btLCP15transfer_i_to_NEi = comdat any

$_ZNK5btLCP3AiiEi = comdat any

$_ZNK5btLCP4numNEv = comdat any

$_ZNK5btLCP6indexNEi = comdat any

$_ZNK5btLCP4numCEv = comdat any

$_ZNK5btLCP6indexCEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZNK20btAlignedObjectArrayIPfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayIPfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK20btAlignedObjectArrayIbE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIbE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIbE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIbE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIbE4copyEiiPb = comdat any

$_ZN20btAlignedObjectArrayIbE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIbE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb = comdat any

$_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb = comdat any

@s_error = hidden global i8 0, align 1
@_ZL14btInfinityMask = internal global i32 2139095040, align 4

@_ZN5btLCPC1EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_ = hidden unnamed_addr alias %struct.btLCP* (%struct.btLCP*, i32, i32, i32, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32*, float**), %struct.btLCP* (%struct.btLCP*, i32, i32, i32, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32*, float**)* @_ZN5btLCPC2EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_

define hidden void @_Z12btFactorLDLTPfS_ii(float* %A, float* %d, i32 %n, i32 %nskip1) #0 {
entry:
  %A.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip1.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  %ell = alloca float*, align 4
  %dee = alloca float*, align 4
  %dd = alloca float, align 4
  %p1 = alloca float, align 4
  %p2 = alloca float, align 4
  %q1 = alloca float, align 4
  %q2 = alloca float, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z22 = alloca float, align 4
  %m22 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store float* %A, float** %A.addr, align 4, !tbaa !2
  store float* %d, float** %d.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %nskip1, i32* %nskip1.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float** %dee to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %dd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = bitcast float* %Z22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc134, %if.end
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %18, 2
  %cmp1 = icmp sle i32 %17, %sub
  br i1 %cmp1, label %for.body, label %for.end136

for.body:                                         ; preds = %for.cond
  %19 = load float*, float** %A.addr, align 4, !tbaa !2
  %20 = load float*, float** %A.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %22 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %21, %22
  %add.ptr = getelementptr inbounds float, float* %20, i32 %mul
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %24 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  call void @_ZL11btSolveL1_2PKfPfii(float* %19, float* %add.ptr, i32 %23, i32 %24)
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z21, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z22, align 4, !tbaa !8
  %25 = load float*, float** %A.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %27 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %26, %27
  %add.ptr3 = getelementptr inbounds float, float* %25, i32 %mul2
  store float* %add.ptr3, float** %ell, align 4, !tbaa !2
  %28 = load float*, float** %d.addr, align 4, !tbaa !2
  store float* %28, float** %dee, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %sub4 = sub nsw i32 %29, 6
  store i32 %sub4, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %30, 0
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond5
  %31 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %31, i32 0
  %32 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %32, float* %p1, align 4, !tbaa !8
  %33 = load float*, float** %ell, align 4, !tbaa !2
  %34 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx8, align 4, !tbaa !8
  store float %35, float* %p2, align 4, !tbaa !8
  %36 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds float, float* %36, i32 0
  %37 = load float, float* %arrayidx9, align 4, !tbaa !8
  store float %37, float* %dd, align 4, !tbaa !8
  %38 = load float, float* %p1, align 4, !tbaa !8
  %39 = load float, float* %dd, align 4, !tbaa !8
  %mul10 = fmul float %38, %39
  store float %mul10, float* %q1, align 4, !tbaa !8
  %40 = load float, float* %p2, align 4, !tbaa !8
  %41 = load float, float* %dd, align 4, !tbaa !8
  %mul11 = fmul float %40, %41
  store float %mul11, float* %q2, align 4, !tbaa !8
  %42 = load float, float* %q1, align 4, !tbaa !8
  %43 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds float, float* %43, i32 0
  store float %42, float* %arrayidx12, align 4, !tbaa !8
  %44 = load float, float* %q2, align 4, !tbaa !8
  %45 = load float*, float** %ell, align 4, !tbaa !2
  %46 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %45, i32 %46
  store float %44, float* %arrayidx13, align 4, !tbaa !8
  %47 = load float, float* %p1, align 4, !tbaa !8
  %48 = load float, float* %q1, align 4, !tbaa !8
  %mul14 = fmul float %47, %48
  store float %mul14, float* %m11, align 4, !tbaa !8
  %49 = load float, float* %p2, align 4, !tbaa !8
  %50 = load float, float* %q1, align 4, !tbaa !8
  %mul15 = fmul float %49, %50
  store float %mul15, float* %m21, align 4, !tbaa !8
  %51 = load float, float* %p2, align 4, !tbaa !8
  %52 = load float, float* %q2, align 4, !tbaa !8
  %mul16 = fmul float %51, %52
  store float %mul16, float* %m22, align 4, !tbaa !8
  %53 = load float, float* %m11, align 4, !tbaa !8
  %54 = load float, float* %Z11, align 4, !tbaa !8
  %add = fadd float %54, %53
  store float %add, float* %Z11, align 4, !tbaa !8
  %55 = load float, float* %m21, align 4, !tbaa !8
  %56 = load float, float* %Z21, align 4, !tbaa !8
  %add17 = fadd float %56, %55
  store float %add17, float* %Z21, align 4, !tbaa !8
  %57 = load float, float* %m22, align 4, !tbaa !8
  %58 = load float, float* %Z22, align 4, !tbaa !8
  %add18 = fadd float %58, %57
  store float %add18, float* %Z22, align 4, !tbaa !8
  %59 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %59, i32 1
  %60 = load float, float* %arrayidx19, align 4, !tbaa !8
  store float %60, float* %p1, align 4, !tbaa !8
  %61 = load float*, float** %ell, align 4, !tbaa !2
  %62 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add20 = add nsw i32 1, %62
  %arrayidx21 = getelementptr inbounds float, float* %61, i32 %add20
  %63 = load float, float* %arrayidx21, align 4, !tbaa !8
  store float %63, float* %p2, align 4, !tbaa !8
  %64 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds float, float* %64, i32 1
  %65 = load float, float* %arrayidx22, align 4, !tbaa !8
  store float %65, float* %dd, align 4, !tbaa !8
  %66 = load float, float* %p1, align 4, !tbaa !8
  %67 = load float, float* %dd, align 4, !tbaa !8
  %mul23 = fmul float %66, %67
  store float %mul23, float* %q1, align 4, !tbaa !8
  %68 = load float, float* %p2, align 4, !tbaa !8
  %69 = load float, float* %dd, align 4, !tbaa !8
  %mul24 = fmul float %68, %69
  store float %mul24, float* %q2, align 4, !tbaa !8
  %70 = load float, float* %q1, align 4, !tbaa !8
  %71 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds float, float* %71, i32 1
  store float %70, float* %arrayidx25, align 4, !tbaa !8
  %72 = load float, float* %q2, align 4, !tbaa !8
  %73 = load float*, float** %ell, align 4, !tbaa !2
  %74 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add26 = add nsw i32 1, %74
  %arrayidx27 = getelementptr inbounds float, float* %73, i32 %add26
  store float %72, float* %arrayidx27, align 4, !tbaa !8
  %75 = load float, float* %p1, align 4, !tbaa !8
  %76 = load float, float* %q1, align 4, !tbaa !8
  %mul28 = fmul float %75, %76
  store float %mul28, float* %m11, align 4, !tbaa !8
  %77 = load float, float* %p2, align 4, !tbaa !8
  %78 = load float, float* %q1, align 4, !tbaa !8
  %mul29 = fmul float %77, %78
  store float %mul29, float* %m21, align 4, !tbaa !8
  %79 = load float, float* %p2, align 4, !tbaa !8
  %80 = load float, float* %q2, align 4, !tbaa !8
  %mul30 = fmul float %79, %80
  store float %mul30, float* %m22, align 4, !tbaa !8
  %81 = load float, float* %m11, align 4, !tbaa !8
  %82 = load float, float* %Z11, align 4, !tbaa !8
  %add31 = fadd float %82, %81
  store float %add31, float* %Z11, align 4, !tbaa !8
  %83 = load float, float* %m21, align 4, !tbaa !8
  %84 = load float, float* %Z21, align 4, !tbaa !8
  %add32 = fadd float %84, %83
  store float %add32, float* %Z21, align 4, !tbaa !8
  %85 = load float, float* %m22, align 4, !tbaa !8
  %86 = load float, float* %Z22, align 4, !tbaa !8
  %add33 = fadd float %86, %85
  store float %add33, float* %Z22, align 4, !tbaa !8
  %87 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds float, float* %87, i32 2
  %88 = load float, float* %arrayidx34, align 4, !tbaa !8
  store float %88, float* %p1, align 4, !tbaa !8
  %89 = load float*, float** %ell, align 4, !tbaa !2
  %90 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add35 = add nsw i32 2, %90
  %arrayidx36 = getelementptr inbounds float, float* %89, i32 %add35
  %91 = load float, float* %arrayidx36, align 4, !tbaa !8
  store float %91, float* %p2, align 4, !tbaa !8
  %92 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds float, float* %92, i32 2
  %93 = load float, float* %arrayidx37, align 4, !tbaa !8
  store float %93, float* %dd, align 4, !tbaa !8
  %94 = load float, float* %p1, align 4, !tbaa !8
  %95 = load float, float* %dd, align 4, !tbaa !8
  %mul38 = fmul float %94, %95
  store float %mul38, float* %q1, align 4, !tbaa !8
  %96 = load float, float* %p2, align 4, !tbaa !8
  %97 = load float, float* %dd, align 4, !tbaa !8
  %mul39 = fmul float %96, %97
  store float %mul39, float* %q2, align 4, !tbaa !8
  %98 = load float, float* %q1, align 4, !tbaa !8
  %99 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds float, float* %99, i32 2
  store float %98, float* %arrayidx40, align 4, !tbaa !8
  %100 = load float, float* %q2, align 4, !tbaa !8
  %101 = load float*, float** %ell, align 4, !tbaa !2
  %102 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add41 = add nsw i32 2, %102
  %arrayidx42 = getelementptr inbounds float, float* %101, i32 %add41
  store float %100, float* %arrayidx42, align 4, !tbaa !8
  %103 = load float, float* %p1, align 4, !tbaa !8
  %104 = load float, float* %q1, align 4, !tbaa !8
  %mul43 = fmul float %103, %104
  store float %mul43, float* %m11, align 4, !tbaa !8
  %105 = load float, float* %p2, align 4, !tbaa !8
  %106 = load float, float* %q1, align 4, !tbaa !8
  %mul44 = fmul float %105, %106
  store float %mul44, float* %m21, align 4, !tbaa !8
  %107 = load float, float* %p2, align 4, !tbaa !8
  %108 = load float, float* %q2, align 4, !tbaa !8
  %mul45 = fmul float %107, %108
  store float %mul45, float* %m22, align 4, !tbaa !8
  %109 = load float, float* %m11, align 4, !tbaa !8
  %110 = load float, float* %Z11, align 4, !tbaa !8
  %add46 = fadd float %110, %109
  store float %add46, float* %Z11, align 4, !tbaa !8
  %111 = load float, float* %m21, align 4, !tbaa !8
  %112 = load float, float* %Z21, align 4, !tbaa !8
  %add47 = fadd float %112, %111
  store float %add47, float* %Z21, align 4, !tbaa !8
  %113 = load float, float* %m22, align 4, !tbaa !8
  %114 = load float, float* %Z22, align 4, !tbaa !8
  %add48 = fadd float %114, %113
  store float %add48, float* %Z22, align 4, !tbaa !8
  %115 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds float, float* %115, i32 3
  %116 = load float, float* %arrayidx49, align 4, !tbaa !8
  store float %116, float* %p1, align 4, !tbaa !8
  %117 = load float*, float** %ell, align 4, !tbaa !2
  %118 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add50 = add nsw i32 3, %118
  %arrayidx51 = getelementptr inbounds float, float* %117, i32 %add50
  %119 = load float, float* %arrayidx51, align 4, !tbaa !8
  store float %119, float* %p2, align 4, !tbaa !8
  %120 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds float, float* %120, i32 3
  %121 = load float, float* %arrayidx52, align 4, !tbaa !8
  store float %121, float* %dd, align 4, !tbaa !8
  %122 = load float, float* %p1, align 4, !tbaa !8
  %123 = load float, float* %dd, align 4, !tbaa !8
  %mul53 = fmul float %122, %123
  store float %mul53, float* %q1, align 4, !tbaa !8
  %124 = load float, float* %p2, align 4, !tbaa !8
  %125 = load float, float* %dd, align 4, !tbaa !8
  %mul54 = fmul float %124, %125
  store float %mul54, float* %q2, align 4, !tbaa !8
  %126 = load float, float* %q1, align 4, !tbaa !8
  %127 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds float, float* %127, i32 3
  store float %126, float* %arrayidx55, align 4, !tbaa !8
  %128 = load float, float* %q2, align 4, !tbaa !8
  %129 = load float*, float** %ell, align 4, !tbaa !2
  %130 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add56 = add nsw i32 3, %130
  %arrayidx57 = getelementptr inbounds float, float* %129, i32 %add56
  store float %128, float* %arrayidx57, align 4, !tbaa !8
  %131 = load float, float* %p1, align 4, !tbaa !8
  %132 = load float, float* %q1, align 4, !tbaa !8
  %mul58 = fmul float %131, %132
  store float %mul58, float* %m11, align 4, !tbaa !8
  %133 = load float, float* %p2, align 4, !tbaa !8
  %134 = load float, float* %q1, align 4, !tbaa !8
  %mul59 = fmul float %133, %134
  store float %mul59, float* %m21, align 4, !tbaa !8
  %135 = load float, float* %p2, align 4, !tbaa !8
  %136 = load float, float* %q2, align 4, !tbaa !8
  %mul60 = fmul float %135, %136
  store float %mul60, float* %m22, align 4, !tbaa !8
  %137 = load float, float* %m11, align 4, !tbaa !8
  %138 = load float, float* %Z11, align 4, !tbaa !8
  %add61 = fadd float %138, %137
  store float %add61, float* %Z11, align 4, !tbaa !8
  %139 = load float, float* %m21, align 4, !tbaa !8
  %140 = load float, float* %Z21, align 4, !tbaa !8
  %add62 = fadd float %140, %139
  store float %add62, float* %Z21, align 4, !tbaa !8
  %141 = load float, float* %m22, align 4, !tbaa !8
  %142 = load float, float* %Z22, align 4, !tbaa !8
  %add63 = fadd float %142, %141
  store float %add63, float* %Z22, align 4, !tbaa !8
  %143 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds float, float* %143, i32 4
  %144 = load float, float* %arrayidx64, align 4, !tbaa !8
  store float %144, float* %p1, align 4, !tbaa !8
  %145 = load float*, float** %ell, align 4, !tbaa !2
  %146 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add65 = add nsw i32 4, %146
  %arrayidx66 = getelementptr inbounds float, float* %145, i32 %add65
  %147 = load float, float* %arrayidx66, align 4, !tbaa !8
  store float %147, float* %p2, align 4, !tbaa !8
  %148 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds float, float* %148, i32 4
  %149 = load float, float* %arrayidx67, align 4, !tbaa !8
  store float %149, float* %dd, align 4, !tbaa !8
  %150 = load float, float* %p1, align 4, !tbaa !8
  %151 = load float, float* %dd, align 4, !tbaa !8
  %mul68 = fmul float %150, %151
  store float %mul68, float* %q1, align 4, !tbaa !8
  %152 = load float, float* %p2, align 4, !tbaa !8
  %153 = load float, float* %dd, align 4, !tbaa !8
  %mul69 = fmul float %152, %153
  store float %mul69, float* %q2, align 4, !tbaa !8
  %154 = load float, float* %q1, align 4, !tbaa !8
  %155 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds float, float* %155, i32 4
  store float %154, float* %arrayidx70, align 4, !tbaa !8
  %156 = load float, float* %q2, align 4, !tbaa !8
  %157 = load float*, float** %ell, align 4, !tbaa !2
  %158 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add71 = add nsw i32 4, %158
  %arrayidx72 = getelementptr inbounds float, float* %157, i32 %add71
  store float %156, float* %arrayidx72, align 4, !tbaa !8
  %159 = load float, float* %p1, align 4, !tbaa !8
  %160 = load float, float* %q1, align 4, !tbaa !8
  %mul73 = fmul float %159, %160
  store float %mul73, float* %m11, align 4, !tbaa !8
  %161 = load float, float* %p2, align 4, !tbaa !8
  %162 = load float, float* %q1, align 4, !tbaa !8
  %mul74 = fmul float %161, %162
  store float %mul74, float* %m21, align 4, !tbaa !8
  %163 = load float, float* %p2, align 4, !tbaa !8
  %164 = load float, float* %q2, align 4, !tbaa !8
  %mul75 = fmul float %163, %164
  store float %mul75, float* %m22, align 4, !tbaa !8
  %165 = load float, float* %m11, align 4, !tbaa !8
  %166 = load float, float* %Z11, align 4, !tbaa !8
  %add76 = fadd float %166, %165
  store float %add76, float* %Z11, align 4, !tbaa !8
  %167 = load float, float* %m21, align 4, !tbaa !8
  %168 = load float, float* %Z21, align 4, !tbaa !8
  %add77 = fadd float %168, %167
  store float %add77, float* %Z21, align 4, !tbaa !8
  %169 = load float, float* %m22, align 4, !tbaa !8
  %170 = load float, float* %Z22, align 4, !tbaa !8
  %add78 = fadd float %170, %169
  store float %add78, float* %Z22, align 4, !tbaa !8
  %171 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds float, float* %171, i32 5
  %172 = load float, float* %arrayidx79, align 4, !tbaa !8
  store float %172, float* %p1, align 4, !tbaa !8
  %173 = load float*, float** %ell, align 4, !tbaa !2
  %174 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add80 = add nsw i32 5, %174
  %arrayidx81 = getelementptr inbounds float, float* %173, i32 %add80
  %175 = load float, float* %arrayidx81, align 4, !tbaa !8
  store float %175, float* %p2, align 4, !tbaa !8
  %176 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds float, float* %176, i32 5
  %177 = load float, float* %arrayidx82, align 4, !tbaa !8
  store float %177, float* %dd, align 4, !tbaa !8
  %178 = load float, float* %p1, align 4, !tbaa !8
  %179 = load float, float* %dd, align 4, !tbaa !8
  %mul83 = fmul float %178, %179
  store float %mul83, float* %q1, align 4, !tbaa !8
  %180 = load float, float* %p2, align 4, !tbaa !8
  %181 = load float, float* %dd, align 4, !tbaa !8
  %mul84 = fmul float %180, %181
  store float %mul84, float* %q2, align 4, !tbaa !8
  %182 = load float, float* %q1, align 4, !tbaa !8
  %183 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds float, float* %183, i32 5
  store float %182, float* %arrayidx85, align 4, !tbaa !8
  %184 = load float, float* %q2, align 4, !tbaa !8
  %185 = load float*, float** %ell, align 4, !tbaa !2
  %186 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add86 = add nsw i32 5, %186
  %arrayidx87 = getelementptr inbounds float, float* %185, i32 %add86
  store float %184, float* %arrayidx87, align 4, !tbaa !8
  %187 = load float, float* %p1, align 4, !tbaa !8
  %188 = load float, float* %q1, align 4, !tbaa !8
  %mul88 = fmul float %187, %188
  store float %mul88, float* %m11, align 4, !tbaa !8
  %189 = load float, float* %p2, align 4, !tbaa !8
  %190 = load float, float* %q1, align 4, !tbaa !8
  %mul89 = fmul float %189, %190
  store float %mul89, float* %m21, align 4, !tbaa !8
  %191 = load float, float* %p2, align 4, !tbaa !8
  %192 = load float, float* %q2, align 4, !tbaa !8
  %mul90 = fmul float %191, %192
  store float %mul90, float* %m22, align 4, !tbaa !8
  %193 = load float, float* %m11, align 4, !tbaa !8
  %194 = load float, float* %Z11, align 4, !tbaa !8
  %add91 = fadd float %194, %193
  store float %add91, float* %Z11, align 4, !tbaa !8
  %195 = load float, float* %m21, align 4, !tbaa !8
  %196 = load float, float* %Z21, align 4, !tbaa !8
  %add92 = fadd float %196, %195
  store float %add92, float* %Z21, align 4, !tbaa !8
  %197 = load float, float* %m22, align 4, !tbaa !8
  %198 = load float, float* %Z22, align 4, !tbaa !8
  %add93 = fadd float %198, %197
  store float %add93, float* %Z22, align 4, !tbaa !8
  %199 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr94 = getelementptr inbounds float, float* %199, i32 6
  store float* %add.ptr94, float** %ell, align 4, !tbaa !2
  %200 = load float*, float** %dee, align 4, !tbaa !2
  %add.ptr95 = getelementptr inbounds float, float* %200, i32 6
  store float* %add.ptr95, float** %dee, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %201 = load i32, i32* %j, align 4, !tbaa !6
  %sub96 = sub nsw i32 %201, 6
  store i32 %sub96, i32* %j, align 4, !tbaa !6
  br label %for.cond5

for.end:                                          ; preds = %for.cond5
  %202 = load i32, i32* %j, align 4, !tbaa !6
  %add97 = add nsw i32 %202, 6
  store i32 %add97, i32* %j, align 4, !tbaa !6
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc115, %for.end
  %203 = load i32, i32* %j, align 4, !tbaa !6
  %cmp99 = icmp sgt i32 %203, 0
  br i1 %cmp99, label %for.body100, label %for.end116

for.body100:                                      ; preds = %for.cond98
  %204 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds float, float* %204, i32 0
  %205 = load float, float* %arrayidx101, align 4, !tbaa !8
  store float %205, float* %p1, align 4, !tbaa !8
  %206 = load float*, float** %ell, align 4, !tbaa !2
  %207 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx102 = getelementptr inbounds float, float* %206, i32 %207
  %208 = load float, float* %arrayidx102, align 4, !tbaa !8
  store float %208, float* %p2, align 4, !tbaa !8
  %209 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds float, float* %209, i32 0
  %210 = load float, float* %arrayidx103, align 4, !tbaa !8
  store float %210, float* %dd, align 4, !tbaa !8
  %211 = load float, float* %p1, align 4, !tbaa !8
  %212 = load float, float* %dd, align 4, !tbaa !8
  %mul104 = fmul float %211, %212
  store float %mul104, float* %q1, align 4, !tbaa !8
  %213 = load float, float* %p2, align 4, !tbaa !8
  %214 = load float, float* %dd, align 4, !tbaa !8
  %mul105 = fmul float %213, %214
  store float %mul105, float* %q2, align 4, !tbaa !8
  %215 = load float, float* %q1, align 4, !tbaa !8
  %216 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds float, float* %216, i32 0
  store float %215, float* %arrayidx106, align 4, !tbaa !8
  %217 = load float, float* %q2, align 4, !tbaa !8
  %218 = load float*, float** %ell, align 4, !tbaa !2
  %219 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds float, float* %218, i32 %219
  store float %217, float* %arrayidx107, align 4, !tbaa !8
  %220 = load float, float* %p1, align 4, !tbaa !8
  %221 = load float, float* %q1, align 4, !tbaa !8
  %mul108 = fmul float %220, %221
  store float %mul108, float* %m11, align 4, !tbaa !8
  %222 = load float, float* %p2, align 4, !tbaa !8
  %223 = load float, float* %q1, align 4, !tbaa !8
  %mul109 = fmul float %222, %223
  store float %mul109, float* %m21, align 4, !tbaa !8
  %224 = load float, float* %p2, align 4, !tbaa !8
  %225 = load float, float* %q2, align 4, !tbaa !8
  %mul110 = fmul float %224, %225
  store float %mul110, float* %m22, align 4, !tbaa !8
  %226 = load float, float* %m11, align 4, !tbaa !8
  %227 = load float, float* %Z11, align 4, !tbaa !8
  %add111 = fadd float %227, %226
  store float %add111, float* %Z11, align 4, !tbaa !8
  %228 = load float, float* %m21, align 4, !tbaa !8
  %229 = load float, float* %Z21, align 4, !tbaa !8
  %add112 = fadd float %229, %228
  store float %add112, float* %Z21, align 4, !tbaa !8
  %230 = load float, float* %m22, align 4, !tbaa !8
  %231 = load float, float* %Z22, align 4, !tbaa !8
  %add113 = fadd float %231, %230
  store float %add113, float* %Z22, align 4, !tbaa !8
  %232 = load float*, float** %ell, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds float, float* %232, i32 1
  store float* %incdec.ptr, float** %ell, align 4, !tbaa !2
  %233 = load float*, float** %dee, align 4, !tbaa !2
  %incdec.ptr114 = getelementptr inbounds float, float* %233, i32 1
  store float* %incdec.ptr114, float** %dee, align 4, !tbaa !2
  br label %for.inc115

for.inc115:                                       ; preds = %for.body100
  %234 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %234, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond98

for.end116:                                       ; preds = %for.cond98
  %235 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds float, float* %235, i32 0
  %236 = load float, float* %arrayidx117, align 4, !tbaa !8
  %237 = load float, float* %Z11, align 4, !tbaa !8
  %sub118 = fsub float %236, %237
  store float %sub118, float* %Z11, align 4, !tbaa !8
  %238 = load float*, float** %ell, align 4, !tbaa !2
  %239 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx119 = getelementptr inbounds float, float* %238, i32 %239
  %240 = load float, float* %arrayidx119, align 4, !tbaa !8
  %241 = load float, float* %Z21, align 4, !tbaa !8
  %sub120 = fsub float %240, %241
  store float %sub120, float* %Z21, align 4, !tbaa !8
  %242 = load float*, float** %ell, align 4, !tbaa !2
  %243 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %add121 = add nsw i32 1, %243
  %arrayidx122 = getelementptr inbounds float, float* %242, i32 %add121
  %244 = load float, float* %arrayidx122, align 4, !tbaa !8
  %245 = load float, float* %Z22, align 4, !tbaa !8
  %sub123 = fsub float %244, %245
  store float %sub123, float* %Z22, align 4, !tbaa !8
  %246 = load float*, float** %d.addr, align 4, !tbaa !2
  %247 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr124 = getelementptr inbounds float, float* %246, i32 %247
  store float* %add.ptr124, float** %dee, align 4, !tbaa !2
  %248 = load float, float* %Z11, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %248
  %249 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds float, float* %249, i32 0
  store float %div, float* %arrayidx125, align 4, !tbaa !8
  store float 0.000000e+00, float* %sum, align 4, !tbaa !8
  %250 = load float, float* %Z21, align 4, !tbaa !8
  store float %250, float* %q1, align 4, !tbaa !8
  %251 = load float, float* %q1, align 4, !tbaa !8
  %252 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds float, float* %252, i32 0
  %253 = load float, float* %arrayidx126, align 4, !tbaa !8
  %mul127 = fmul float %251, %253
  store float %mul127, float* %q2, align 4, !tbaa !8
  %254 = load float, float* %q2, align 4, !tbaa !8
  store float %254, float* %Z21, align 4, !tbaa !8
  %255 = load float, float* %q1, align 4, !tbaa !8
  %256 = load float, float* %q2, align 4, !tbaa !8
  %mul128 = fmul float %255, %256
  %257 = load float, float* %sum, align 4, !tbaa !8
  %add129 = fadd float %257, %mul128
  store float %add129, float* %sum, align 4, !tbaa !8
  %258 = load float, float* %Z22, align 4, !tbaa !8
  %259 = load float, float* %sum, align 4, !tbaa !8
  %sub130 = fsub float %258, %259
  %div131 = fdiv float 1.000000e+00, %sub130
  %260 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds float, float* %260, i32 1
  store float %div131, float* %arrayidx132, align 4, !tbaa !8
  %261 = load float, float* %Z21, align 4, !tbaa !8
  %262 = load float*, float** %ell, align 4, !tbaa !2
  %263 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %arrayidx133 = getelementptr inbounds float, float* %262, i32 %263
  store float %261, float* %arrayidx133, align 4, !tbaa !8
  br label %for.inc134

for.inc134:                                       ; preds = %for.end116
  %264 = load i32, i32* %i, align 4, !tbaa !6
  %add135 = add nsw i32 %264, 2
  store i32 %add135, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end136:                                       ; preds = %for.cond
  %265 = load i32, i32* %n.addr, align 4, !tbaa !6
  %266 = load i32, i32* %i, align 4, !tbaa !6
  %sub137 = sub nsw i32 %265, %266
  switch i32 %sub137, label %sw.epilog [
    i32 0, label %sw.epilog
    i32 1, label %sw.bb
  ]

sw.bb:                                            ; preds = %for.end136
  %267 = load float*, float** %A.addr, align 4, !tbaa !2
  %268 = load float*, float** %A.addr, align 4, !tbaa !2
  %269 = load i32, i32* %i, align 4, !tbaa !6
  %270 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %mul138 = mul nsw i32 %269, %270
  %add.ptr139 = getelementptr inbounds float, float* %268, i32 %mul138
  %271 = load i32, i32* %i, align 4, !tbaa !6
  %272 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  call void @_ZL11btSolveL1_1PKfPfii(float* %267, float* %add.ptr139, i32 %271, i32 %272)
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  %273 = load float*, float** %A.addr, align 4, !tbaa !2
  %274 = load i32, i32* %i, align 4, !tbaa !6
  %275 = load i32, i32* %nskip1.addr, align 4, !tbaa !6
  %mul140 = mul nsw i32 %274, %275
  %add.ptr141 = getelementptr inbounds float, float* %273, i32 %mul140
  store float* %add.ptr141, float** %ell, align 4, !tbaa !2
  %276 = load float*, float** %d.addr, align 4, !tbaa !2
  store float* %276, float** %dee, align 4, !tbaa !2
  %277 = load i32, i32* %i, align 4, !tbaa !6
  %sub142 = sub nsw i32 %277, 6
  store i32 %sub142, i32* %j, align 4, !tbaa !6
  br label %for.cond143

for.cond143:                                      ; preds = %for.inc184, %sw.bb
  %278 = load i32, i32* %j, align 4, !tbaa !6
  %cmp144 = icmp sge i32 %278, 0
  br i1 %cmp144, label %for.body145, label %for.end186

for.body145:                                      ; preds = %for.cond143
  %279 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds float, float* %279, i32 0
  %280 = load float, float* %arrayidx146, align 4, !tbaa !8
  store float %280, float* %p1, align 4, !tbaa !8
  %281 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds float, float* %281, i32 0
  %282 = load float, float* %arrayidx147, align 4, !tbaa !8
  store float %282, float* %dd, align 4, !tbaa !8
  %283 = load float, float* %p1, align 4, !tbaa !8
  %284 = load float, float* %dd, align 4, !tbaa !8
  %mul148 = fmul float %283, %284
  store float %mul148, float* %q1, align 4, !tbaa !8
  %285 = load float, float* %q1, align 4, !tbaa !8
  %286 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds float, float* %286, i32 0
  store float %285, float* %arrayidx149, align 4, !tbaa !8
  %287 = load float, float* %p1, align 4, !tbaa !8
  %288 = load float, float* %q1, align 4, !tbaa !8
  %mul150 = fmul float %287, %288
  store float %mul150, float* %m11, align 4, !tbaa !8
  %289 = load float, float* %m11, align 4, !tbaa !8
  %290 = load float, float* %Z11, align 4, !tbaa !8
  %add151 = fadd float %290, %289
  store float %add151, float* %Z11, align 4, !tbaa !8
  %291 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds float, float* %291, i32 1
  %292 = load float, float* %arrayidx152, align 4, !tbaa !8
  store float %292, float* %p1, align 4, !tbaa !8
  %293 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds float, float* %293, i32 1
  %294 = load float, float* %arrayidx153, align 4, !tbaa !8
  store float %294, float* %dd, align 4, !tbaa !8
  %295 = load float, float* %p1, align 4, !tbaa !8
  %296 = load float, float* %dd, align 4, !tbaa !8
  %mul154 = fmul float %295, %296
  store float %mul154, float* %q1, align 4, !tbaa !8
  %297 = load float, float* %q1, align 4, !tbaa !8
  %298 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds float, float* %298, i32 1
  store float %297, float* %arrayidx155, align 4, !tbaa !8
  %299 = load float, float* %p1, align 4, !tbaa !8
  %300 = load float, float* %q1, align 4, !tbaa !8
  %mul156 = fmul float %299, %300
  store float %mul156, float* %m11, align 4, !tbaa !8
  %301 = load float, float* %m11, align 4, !tbaa !8
  %302 = load float, float* %Z11, align 4, !tbaa !8
  %add157 = fadd float %302, %301
  store float %add157, float* %Z11, align 4, !tbaa !8
  %303 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds float, float* %303, i32 2
  %304 = load float, float* %arrayidx158, align 4, !tbaa !8
  store float %304, float* %p1, align 4, !tbaa !8
  %305 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds float, float* %305, i32 2
  %306 = load float, float* %arrayidx159, align 4, !tbaa !8
  store float %306, float* %dd, align 4, !tbaa !8
  %307 = load float, float* %p1, align 4, !tbaa !8
  %308 = load float, float* %dd, align 4, !tbaa !8
  %mul160 = fmul float %307, %308
  store float %mul160, float* %q1, align 4, !tbaa !8
  %309 = load float, float* %q1, align 4, !tbaa !8
  %310 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds float, float* %310, i32 2
  store float %309, float* %arrayidx161, align 4, !tbaa !8
  %311 = load float, float* %p1, align 4, !tbaa !8
  %312 = load float, float* %q1, align 4, !tbaa !8
  %mul162 = fmul float %311, %312
  store float %mul162, float* %m11, align 4, !tbaa !8
  %313 = load float, float* %m11, align 4, !tbaa !8
  %314 = load float, float* %Z11, align 4, !tbaa !8
  %add163 = fadd float %314, %313
  store float %add163, float* %Z11, align 4, !tbaa !8
  %315 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds float, float* %315, i32 3
  %316 = load float, float* %arrayidx164, align 4, !tbaa !8
  store float %316, float* %p1, align 4, !tbaa !8
  %317 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds float, float* %317, i32 3
  %318 = load float, float* %arrayidx165, align 4, !tbaa !8
  store float %318, float* %dd, align 4, !tbaa !8
  %319 = load float, float* %p1, align 4, !tbaa !8
  %320 = load float, float* %dd, align 4, !tbaa !8
  %mul166 = fmul float %319, %320
  store float %mul166, float* %q1, align 4, !tbaa !8
  %321 = load float, float* %q1, align 4, !tbaa !8
  %322 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds float, float* %322, i32 3
  store float %321, float* %arrayidx167, align 4, !tbaa !8
  %323 = load float, float* %p1, align 4, !tbaa !8
  %324 = load float, float* %q1, align 4, !tbaa !8
  %mul168 = fmul float %323, %324
  store float %mul168, float* %m11, align 4, !tbaa !8
  %325 = load float, float* %m11, align 4, !tbaa !8
  %326 = load float, float* %Z11, align 4, !tbaa !8
  %add169 = fadd float %326, %325
  store float %add169, float* %Z11, align 4, !tbaa !8
  %327 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds float, float* %327, i32 4
  %328 = load float, float* %arrayidx170, align 4, !tbaa !8
  store float %328, float* %p1, align 4, !tbaa !8
  %329 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds float, float* %329, i32 4
  %330 = load float, float* %arrayidx171, align 4, !tbaa !8
  store float %330, float* %dd, align 4, !tbaa !8
  %331 = load float, float* %p1, align 4, !tbaa !8
  %332 = load float, float* %dd, align 4, !tbaa !8
  %mul172 = fmul float %331, %332
  store float %mul172, float* %q1, align 4, !tbaa !8
  %333 = load float, float* %q1, align 4, !tbaa !8
  %334 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds float, float* %334, i32 4
  store float %333, float* %arrayidx173, align 4, !tbaa !8
  %335 = load float, float* %p1, align 4, !tbaa !8
  %336 = load float, float* %q1, align 4, !tbaa !8
  %mul174 = fmul float %335, %336
  store float %mul174, float* %m11, align 4, !tbaa !8
  %337 = load float, float* %m11, align 4, !tbaa !8
  %338 = load float, float* %Z11, align 4, !tbaa !8
  %add175 = fadd float %338, %337
  store float %add175, float* %Z11, align 4, !tbaa !8
  %339 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds float, float* %339, i32 5
  %340 = load float, float* %arrayidx176, align 4, !tbaa !8
  store float %340, float* %p1, align 4, !tbaa !8
  %341 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds float, float* %341, i32 5
  %342 = load float, float* %arrayidx177, align 4, !tbaa !8
  store float %342, float* %dd, align 4, !tbaa !8
  %343 = load float, float* %p1, align 4, !tbaa !8
  %344 = load float, float* %dd, align 4, !tbaa !8
  %mul178 = fmul float %343, %344
  store float %mul178, float* %q1, align 4, !tbaa !8
  %345 = load float, float* %q1, align 4, !tbaa !8
  %346 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds float, float* %346, i32 5
  store float %345, float* %arrayidx179, align 4, !tbaa !8
  %347 = load float, float* %p1, align 4, !tbaa !8
  %348 = load float, float* %q1, align 4, !tbaa !8
  %mul180 = fmul float %347, %348
  store float %mul180, float* %m11, align 4, !tbaa !8
  %349 = load float, float* %m11, align 4, !tbaa !8
  %350 = load float, float* %Z11, align 4, !tbaa !8
  %add181 = fadd float %350, %349
  store float %add181, float* %Z11, align 4, !tbaa !8
  %351 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr182 = getelementptr inbounds float, float* %351, i32 6
  store float* %add.ptr182, float** %ell, align 4, !tbaa !2
  %352 = load float*, float** %dee, align 4, !tbaa !2
  %add.ptr183 = getelementptr inbounds float, float* %352, i32 6
  store float* %add.ptr183, float** %dee, align 4, !tbaa !2
  br label %for.inc184

for.inc184:                                       ; preds = %for.body145
  %353 = load i32, i32* %j, align 4, !tbaa !6
  %sub185 = sub nsw i32 %353, 6
  store i32 %sub185, i32* %j, align 4, !tbaa !6
  br label %for.cond143

for.end186:                                       ; preds = %for.cond143
  %354 = load i32, i32* %j, align 4, !tbaa !6
  %add187 = add nsw i32 %354, 6
  store i32 %add187, i32* %j, align 4, !tbaa !6
  br label %for.cond188

for.cond188:                                      ; preds = %for.inc199, %for.end186
  %355 = load i32, i32* %j, align 4, !tbaa !6
  %cmp189 = icmp sgt i32 %355, 0
  br i1 %cmp189, label %for.body190, label %for.end201

for.body190:                                      ; preds = %for.cond188
  %356 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds float, float* %356, i32 0
  %357 = load float, float* %arrayidx191, align 4, !tbaa !8
  store float %357, float* %p1, align 4, !tbaa !8
  %358 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds float, float* %358, i32 0
  %359 = load float, float* %arrayidx192, align 4, !tbaa !8
  store float %359, float* %dd, align 4, !tbaa !8
  %360 = load float, float* %p1, align 4, !tbaa !8
  %361 = load float, float* %dd, align 4, !tbaa !8
  %mul193 = fmul float %360, %361
  store float %mul193, float* %q1, align 4, !tbaa !8
  %362 = load float, float* %q1, align 4, !tbaa !8
  %363 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds float, float* %363, i32 0
  store float %362, float* %arrayidx194, align 4, !tbaa !8
  %364 = load float, float* %p1, align 4, !tbaa !8
  %365 = load float, float* %q1, align 4, !tbaa !8
  %mul195 = fmul float %364, %365
  store float %mul195, float* %m11, align 4, !tbaa !8
  %366 = load float, float* %m11, align 4, !tbaa !8
  %367 = load float, float* %Z11, align 4, !tbaa !8
  %add196 = fadd float %367, %366
  store float %add196, float* %Z11, align 4, !tbaa !8
  %368 = load float*, float** %ell, align 4, !tbaa !2
  %incdec.ptr197 = getelementptr inbounds float, float* %368, i32 1
  store float* %incdec.ptr197, float** %ell, align 4, !tbaa !2
  %369 = load float*, float** %dee, align 4, !tbaa !2
  %incdec.ptr198 = getelementptr inbounds float, float* %369, i32 1
  store float* %incdec.ptr198, float** %dee, align 4, !tbaa !2
  br label %for.inc199

for.inc199:                                       ; preds = %for.body190
  %370 = load i32, i32* %j, align 4, !tbaa !6
  %dec200 = add nsw i32 %370, -1
  store i32 %dec200, i32* %j, align 4, !tbaa !6
  br label %for.cond188

for.end201:                                       ; preds = %for.cond188
  %371 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds float, float* %371, i32 0
  %372 = load float, float* %arrayidx202, align 4, !tbaa !8
  %373 = load float, float* %Z11, align 4, !tbaa !8
  %sub203 = fsub float %372, %373
  store float %sub203, float* %Z11, align 4, !tbaa !8
  %374 = load float*, float** %d.addr, align 4, !tbaa !2
  %375 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr204 = getelementptr inbounds float, float* %374, i32 %375
  store float* %add.ptr204, float** %dee, align 4, !tbaa !2
  %376 = load float, float* %Z11, align 4, !tbaa !8
  %div205 = fdiv float 1.000000e+00, %376
  %377 = load float*, float** %dee, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds float, float* %377, i32 0
  store float %div205, float* %arrayidx206, align 4, !tbaa !8
  br label %sw.epilog

sw.epilog:                                        ; preds = %for.end136, %for.end201, %for.end136
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %if.then
  %378 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %378) #7
  %379 = bitcast float* %Z22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #7
  %380 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %380) #7
  %381 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %381) #7
  %382 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %382) #7
  %383 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %383) #7
  %384 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #7
  %385 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #7
  %386 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #7
  %387 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #7
  %388 = bitcast float* %dd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #7
  %389 = bitcast float** %dee to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %389) #7
  %390 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %390) #7
  %391 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #7
  %392 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #7
  %393 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @_ZL11btSolveL1_2PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #2 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z12 = alloca float, align 4
  %m12 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z22 = alloca float, align 4
  %m22 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %q2 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %lskip1, i32* %lskip1.addr, align 4, !tbaa !6
  %0 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %Z12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %m12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %Z22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast float** %ex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc70, %entry
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %16, %17
  br i1 %cmp, label %for.body, label %for.end72

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z12, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z21, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z22, align 4, !tbaa !8
  %18 = load float*, float** %L.addr, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %19, %20
  %add.ptr = getelementptr inbounds float, float* %18, i32 %mul
  store float* %add.ptr, float** %ell, align 4, !tbaa !2
  %21 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %21, float** %ex, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %sub = sub nsw i32 %22, 2
  store i32 %sub, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %23, 0
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %24 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %24, i32 0
  %25 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %25, float* %p1, align 4, !tbaa !8
  %26 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %26, i32 0
  %27 = load float, float* %arrayidx4, align 4, !tbaa !8
  store float %27, float* %q1, align 4, !tbaa !8
  %28 = load float, float* %p1, align 4, !tbaa !8
  %29 = load float, float* %q1, align 4, !tbaa !8
  %mul5 = fmul float %28, %29
  store float %mul5, float* %m11, align 4, !tbaa !8
  %30 = load float*, float** %ex, align 4, !tbaa !2
  %31 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %30, i32 %31
  %32 = load float, float* %arrayidx6, align 4, !tbaa !8
  store float %32, float* %q2, align 4, !tbaa !8
  %33 = load float, float* %p1, align 4, !tbaa !8
  %34 = load float, float* %q2, align 4, !tbaa !8
  %mul7 = fmul float %33, %34
  store float %mul7, float* %m12, align 4, !tbaa !8
  %35 = load float*, float** %ell, align 4, !tbaa !2
  %36 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %35, i32 %36
  %37 = load float, float* %arrayidx8, align 4, !tbaa !8
  store float %37, float* %p2, align 4, !tbaa !8
  %38 = load float, float* %p2, align 4, !tbaa !8
  %39 = load float, float* %q1, align 4, !tbaa !8
  %mul9 = fmul float %38, %39
  store float %mul9, float* %m21, align 4, !tbaa !8
  %40 = load float, float* %p2, align 4, !tbaa !8
  %41 = load float, float* %q2, align 4, !tbaa !8
  %mul10 = fmul float %40, %41
  store float %mul10, float* %m22, align 4, !tbaa !8
  %42 = load float, float* %m11, align 4, !tbaa !8
  %43 = load float, float* %Z11, align 4, !tbaa !8
  %add = fadd float %43, %42
  store float %add, float* %Z11, align 4, !tbaa !8
  %44 = load float, float* %m12, align 4, !tbaa !8
  %45 = load float, float* %Z12, align 4, !tbaa !8
  %add11 = fadd float %45, %44
  store float %add11, float* %Z12, align 4, !tbaa !8
  %46 = load float, float* %m21, align 4, !tbaa !8
  %47 = load float, float* %Z21, align 4, !tbaa !8
  %add12 = fadd float %47, %46
  store float %add12, float* %Z21, align 4, !tbaa !8
  %48 = load float, float* %m22, align 4, !tbaa !8
  %49 = load float, float* %Z22, align 4, !tbaa !8
  %add13 = fadd float %49, %48
  store float %add13, float* %Z22, align 4, !tbaa !8
  %50 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %50, i32 1
  %51 = load float, float* %arrayidx14, align 4, !tbaa !8
  store float %51, float* %p1, align 4, !tbaa !8
  %52 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %52, i32 1
  %53 = load float, float* %arrayidx15, align 4, !tbaa !8
  store float %53, float* %q1, align 4, !tbaa !8
  %54 = load float, float* %p1, align 4, !tbaa !8
  %55 = load float, float* %q1, align 4, !tbaa !8
  %mul16 = fmul float %54, %55
  store float %mul16, float* %m11, align 4, !tbaa !8
  %56 = load float*, float** %ex, align 4, !tbaa !2
  %57 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add17 = add nsw i32 1, %57
  %arrayidx18 = getelementptr inbounds float, float* %56, i32 %add17
  %58 = load float, float* %arrayidx18, align 4, !tbaa !8
  store float %58, float* %q2, align 4, !tbaa !8
  %59 = load float, float* %p1, align 4, !tbaa !8
  %60 = load float, float* %q2, align 4, !tbaa !8
  %mul19 = fmul float %59, %60
  store float %mul19, float* %m12, align 4, !tbaa !8
  %61 = load float*, float** %ell, align 4, !tbaa !2
  %62 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add20 = add nsw i32 1, %62
  %arrayidx21 = getelementptr inbounds float, float* %61, i32 %add20
  %63 = load float, float* %arrayidx21, align 4, !tbaa !8
  store float %63, float* %p2, align 4, !tbaa !8
  %64 = load float, float* %p2, align 4, !tbaa !8
  %65 = load float, float* %q1, align 4, !tbaa !8
  %mul22 = fmul float %64, %65
  store float %mul22, float* %m21, align 4, !tbaa !8
  %66 = load float, float* %p2, align 4, !tbaa !8
  %67 = load float, float* %q2, align 4, !tbaa !8
  %mul23 = fmul float %66, %67
  store float %mul23, float* %m22, align 4, !tbaa !8
  %68 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds float, float* %68, i32 2
  store float* %add.ptr24, float** %ell, align 4, !tbaa !2
  %69 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds float, float* %69, i32 2
  store float* %add.ptr25, float** %ex, align 4, !tbaa !2
  %70 = load float, float* %m11, align 4, !tbaa !8
  %71 = load float, float* %Z11, align 4, !tbaa !8
  %add26 = fadd float %71, %70
  store float %add26, float* %Z11, align 4, !tbaa !8
  %72 = load float, float* %m12, align 4, !tbaa !8
  %73 = load float, float* %Z12, align 4, !tbaa !8
  %add27 = fadd float %73, %72
  store float %add27, float* %Z12, align 4, !tbaa !8
  %74 = load float, float* %m21, align 4, !tbaa !8
  %75 = load float, float* %Z21, align 4, !tbaa !8
  %add28 = fadd float %75, %74
  store float %add28, float* %Z21, align 4, !tbaa !8
  %76 = load float, float* %m22, align 4, !tbaa !8
  %77 = load float, float* %Z22, align 4, !tbaa !8
  %add29 = fadd float %77, %76
  store float %add29, float* %Z22, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %78 = load i32, i32* %j, align 4, !tbaa !6
  %sub30 = sub nsw i32 %78, 2
  store i32 %sub30, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %79 = load i32, i32* %j, align 4, !tbaa !6
  %add31 = add nsw i32 %79, 2
  store i32 %add31, i32* %j, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc49, %for.end
  %80 = load i32, i32* %j, align 4, !tbaa !6
  %cmp33 = icmp sgt i32 %80, 0
  br i1 %cmp33, label %for.body34, label %for.end50

for.body34:                                       ; preds = %for.cond32
  %81 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds float, float* %81, i32 0
  %82 = load float, float* %arrayidx35, align 4, !tbaa !8
  store float %82, float* %p1, align 4, !tbaa !8
  %83 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds float, float* %83, i32 0
  %84 = load float, float* %arrayidx36, align 4, !tbaa !8
  store float %84, float* %q1, align 4, !tbaa !8
  %85 = load float, float* %p1, align 4, !tbaa !8
  %86 = load float, float* %q1, align 4, !tbaa !8
  %mul37 = fmul float %85, %86
  store float %mul37, float* %m11, align 4, !tbaa !8
  %87 = load float*, float** %ex, align 4, !tbaa !2
  %88 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds float, float* %87, i32 %88
  %89 = load float, float* %arrayidx38, align 4, !tbaa !8
  store float %89, float* %q2, align 4, !tbaa !8
  %90 = load float, float* %p1, align 4, !tbaa !8
  %91 = load float, float* %q2, align 4, !tbaa !8
  %mul39 = fmul float %90, %91
  store float %mul39, float* %m12, align 4, !tbaa !8
  %92 = load float*, float** %ell, align 4, !tbaa !2
  %93 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds float, float* %92, i32 %93
  %94 = load float, float* %arrayidx40, align 4, !tbaa !8
  store float %94, float* %p2, align 4, !tbaa !8
  %95 = load float, float* %p2, align 4, !tbaa !8
  %96 = load float, float* %q1, align 4, !tbaa !8
  %mul41 = fmul float %95, %96
  store float %mul41, float* %m21, align 4, !tbaa !8
  %97 = load float, float* %p2, align 4, !tbaa !8
  %98 = load float, float* %q2, align 4, !tbaa !8
  %mul42 = fmul float %97, %98
  store float %mul42, float* %m22, align 4, !tbaa !8
  %99 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds float, float* %99, i32 1
  store float* %add.ptr43, float** %ell, align 4, !tbaa !2
  %100 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds float, float* %100, i32 1
  store float* %add.ptr44, float** %ex, align 4, !tbaa !2
  %101 = load float, float* %m11, align 4, !tbaa !8
  %102 = load float, float* %Z11, align 4, !tbaa !8
  %add45 = fadd float %102, %101
  store float %add45, float* %Z11, align 4, !tbaa !8
  %103 = load float, float* %m12, align 4, !tbaa !8
  %104 = load float, float* %Z12, align 4, !tbaa !8
  %add46 = fadd float %104, %103
  store float %add46, float* %Z12, align 4, !tbaa !8
  %105 = load float, float* %m21, align 4, !tbaa !8
  %106 = load float, float* %Z21, align 4, !tbaa !8
  %add47 = fadd float %106, %105
  store float %add47, float* %Z21, align 4, !tbaa !8
  %107 = load float, float* %m22, align 4, !tbaa !8
  %108 = load float, float* %Z22, align 4, !tbaa !8
  %add48 = fadd float %108, %107
  store float %add48, float* %Z22, align 4, !tbaa !8
  br label %for.inc49

for.inc49:                                        ; preds = %for.body34
  %109 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %109, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond32

for.end50:                                        ; preds = %for.cond32
  %110 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds float, float* %110, i32 0
  %111 = load float, float* %arrayidx51, align 4, !tbaa !8
  %112 = load float, float* %Z11, align 4, !tbaa !8
  %sub52 = fsub float %111, %112
  store float %sub52, float* %Z11, align 4, !tbaa !8
  %113 = load float, float* %Z11, align 4, !tbaa !8
  %114 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds float, float* %114, i32 0
  store float %113, float* %arrayidx53, align 4, !tbaa !8
  %115 = load float*, float** %ex, align 4, !tbaa !2
  %116 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds float, float* %115, i32 %116
  %117 = load float, float* %arrayidx54, align 4, !tbaa !8
  %118 = load float, float* %Z12, align 4, !tbaa !8
  %sub55 = fsub float %117, %118
  store float %sub55, float* %Z12, align 4, !tbaa !8
  %119 = load float, float* %Z12, align 4, !tbaa !8
  %120 = load float*, float** %ex, align 4, !tbaa !2
  %121 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds float, float* %120, i32 %121
  store float %119, float* %arrayidx56, align 4, !tbaa !8
  %122 = load float*, float** %ell, align 4, !tbaa !2
  %123 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds float, float* %122, i32 %123
  %124 = load float, float* %arrayidx57, align 4, !tbaa !8
  store float %124, float* %p1, align 4, !tbaa !8
  %125 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds float, float* %125, i32 1
  %126 = load float, float* %arrayidx58, align 4, !tbaa !8
  %127 = load float, float* %Z21, align 4, !tbaa !8
  %sub59 = fsub float %126, %127
  %128 = load float, float* %p1, align 4, !tbaa !8
  %129 = load float, float* %Z11, align 4, !tbaa !8
  %mul60 = fmul float %128, %129
  %sub61 = fsub float %sub59, %mul60
  store float %sub61, float* %Z21, align 4, !tbaa !8
  %130 = load float, float* %Z21, align 4, !tbaa !8
  %131 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds float, float* %131, i32 1
  store float %130, float* %arrayidx62, align 4, !tbaa !8
  %132 = load float*, float** %ex, align 4, !tbaa !2
  %133 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add63 = add nsw i32 1, %133
  %arrayidx64 = getelementptr inbounds float, float* %132, i32 %add63
  %134 = load float, float* %arrayidx64, align 4, !tbaa !8
  %135 = load float, float* %Z22, align 4, !tbaa !8
  %sub65 = fsub float %134, %135
  %136 = load float, float* %p1, align 4, !tbaa !8
  %137 = load float, float* %Z12, align 4, !tbaa !8
  %mul66 = fmul float %136, %137
  %sub67 = fsub float %sub65, %mul66
  store float %sub67, float* %Z22, align 4, !tbaa !8
  %138 = load float, float* %Z22, align 4, !tbaa !8
  %139 = load float*, float** %ex, align 4, !tbaa !2
  %140 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add68 = add nsw i32 1, %140
  %arrayidx69 = getelementptr inbounds float, float* %139, i32 %add68
  store float %138, float* %arrayidx69, align 4, !tbaa !8
  br label %for.inc70

for.inc70:                                        ; preds = %for.end50
  %141 = load i32, i32* %i, align 4, !tbaa !6
  %add71 = add nsw i32 %141, 2
  store i32 %add71, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end72:                                        ; preds = %for.cond
  %142 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #7
  %143 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  %144 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  %145 = bitcast float** %ex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #7
  %146 = bitcast float* %q2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #7
  %147 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #7
  %148 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #7
  %149 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #7
  %150 = bitcast float* %m22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #7
  %151 = bitcast float* %Z22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #7
  %152 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #7
  %153 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #7
  %154 = bitcast float* %m12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #7
  %155 = bitcast float* %Z12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #7
  %156 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #7
  %157 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #7
  ret void
}

; Function Attrs: nounwind
define internal void @_ZL11btSolveL1_1PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #2 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %lskip1, i32* %lskip1.addr, align 4, !tbaa !6
  %0 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float** %ex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc44, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.end46

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z21, align 4, !tbaa !8
  %13 = load float*, float** %L.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %14, %15
  %add.ptr = getelementptr inbounds float, float* %13, i32 %mul
  store float* %add.ptr, float** %ell, align 4, !tbaa !2
  %16 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %16, float** %ex, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %sub = sub nsw i32 %17, 2
  store i32 %sub, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %18, 0
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %19 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %19, i32 0
  %20 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %20, float* %p1, align 4, !tbaa !8
  %21 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds float, float* %21, i32 0
  %22 = load float, float* %arrayidx4, align 4, !tbaa !8
  store float %22, float* %q1, align 4, !tbaa !8
  %23 = load float, float* %p1, align 4, !tbaa !8
  %24 = load float, float* %q1, align 4, !tbaa !8
  %mul5 = fmul float %23, %24
  store float %mul5, float* %m11, align 4, !tbaa !8
  %25 = load float*, float** %ell, align 4, !tbaa !2
  %26 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx6, align 4, !tbaa !8
  store float %27, float* %p2, align 4, !tbaa !8
  %28 = load float, float* %p2, align 4, !tbaa !8
  %29 = load float, float* %q1, align 4, !tbaa !8
  %mul7 = fmul float %28, %29
  store float %mul7, float* %m21, align 4, !tbaa !8
  %30 = load float, float* %m11, align 4, !tbaa !8
  %31 = load float, float* %Z11, align 4, !tbaa !8
  %add = fadd float %31, %30
  store float %add, float* %Z11, align 4, !tbaa !8
  %32 = load float, float* %m21, align 4, !tbaa !8
  %33 = load float, float* %Z21, align 4, !tbaa !8
  %add8 = fadd float %33, %32
  store float %add8, float* %Z21, align 4, !tbaa !8
  %34 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds float, float* %34, i32 1
  %35 = load float, float* %arrayidx9, align 4, !tbaa !8
  store float %35, float* %p1, align 4, !tbaa !8
  %36 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %36, i32 1
  %37 = load float, float* %arrayidx10, align 4, !tbaa !8
  store float %37, float* %q1, align 4, !tbaa !8
  %38 = load float, float* %p1, align 4, !tbaa !8
  %39 = load float, float* %q1, align 4, !tbaa !8
  %mul11 = fmul float %38, %39
  store float %mul11, float* %m11, align 4, !tbaa !8
  %40 = load float*, float** %ell, align 4, !tbaa !2
  %41 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add12 = add nsw i32 1, %41
  %arrayidx13 = getelementptr inbounds float, float* %40, i32 %add12
  %42 = load float, float* %arrayidx13, align 4, !tbaa !8
  store float %42, float* %p2, align 4, !tbaa !8
  %43 = load float, float* %p2, align 4, !tbaa !8
  %44 = load float, float* %q1, align 4, !tbaa !8
  %mul14 = fmul float %43, %44
  store float %mul14, float* %m21, align 4, !tbaa !8
  %45 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr15 = getelementptr inbounds float, float* %45, i32 2
  store float* %add.ptr15, float** %ell, align 4, !tbaa !2
  %46 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds float, float* %46, i32 2
  store float* %add.ptr16, float** %ex, align 4, !tbaa !2
  %47 = load float, float* %m11, align 4, !tbaa !8
  %48 = load float, float* %Z11, align 4, !tbaa !8
  %add17 = fadd float %48, %47
  store float %add17, float* %Z11, align 4, !tbaa !8
  %49 = load float, float* %m21, align 4, !tbaa !8
  %50 = load float, float* %Z21, align 4, !tbaa !8
  %add18 = fadd float %50, %49
  store float %add18, float* %Z21, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %sub19 = sub nsw i32 %51, 2
  store i32 %sub19, i32* %j, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %52 = load i32, i32* %j, align 4, !tbaa !6
  %add20 = add nsw i32 %52, 2
  store i32 %add20, i32* %j, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc33, %for.end
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %cmp22 = icmp sgt i32 %53, 0
  br i1 %cmp22, label %for.body23, label %for.end34

for.body23:                                       ; preds = %for.cond21
  %54 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds float, float* %54, i32 0
  %55 = load float, float* %arrayidx24, align 4, !tbaa !8
  store float %55, float* %p1, align 4, !tbaa !8
  %56 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds float, float* %56, i32 0
  %57 = load float, float* %arrayidx25, align 4, !tbaa !8
  store float %57, float* %q1, align 4, !tbaa !8
  %58 = load float, float* %p1, align 4, !tbaa !8
  %59 = load float, float* %q1, align 4, !tbaa !8
  %mul26 = fmul float %58, %59
  store float %mul26, float* %m11, align 4, !tbaa !8
  %60 = load float*, float** %ell, align 4, !tbaa !2
  %61 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds float, float* %60, i32 %61
  %62 = load float, float* %arrayidx27, align 4, !tbaa !8
  store float %62, float* %p2, align 4, !tbaa !8
  %63 = load float, float* %p2, align 4, !tbaa !8
  %64 = load float, float* %q1, align 4, !tbaa !8
  %mul28 = fmul float %63, %64
  store float %mul28, float* %m21, align 4, !tbaa !8
  %65 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds float, float* %65, i32 1
  store float* %add.ptr29, float** %ell, align 4, !tbaa !2
  %66 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr30 = getelementptr inbounds float, float* %66, i32 1
  store float* %add.ptr30, float** %ex, align 4, !tbaa !2
  %67 = load float, float* %m11, align 4, !tbaa !8
  %68 = load float, float* %Z11, align 4, !tbaa !8
  %add31 = fadd float %68, %67
  store float %add31, float* %Z11, align 4, !tbaa !8
  %69 = load float, float* %m21, align 4, !tbaa !8
  %70 = load float, float* %Z21, align 4, !tbaa !8
  %add32 = fadd float %70, %69
  store float %add32, float* %Z21, align 4, !tbaa !8
  br label %for.inc33

for.inc33:                                        ; preds = %for.body23
  %71 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %71, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond21

for.end34:                                        ; preds = %for.cond21
  %72 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds float, float* %72, i32 0
  %73 = load float, float* %arrayidx35, align 4, !tbaa !8
  %74 = load float, float* %Z11, align 4, !tbaa !8
  %sub36 = fsub float %73, %74
  store float %sub36, float* %Z11, align 4, !tbaa !8
  %75 = load float, float* %Z11, align 4, !tbaa !8
  %76 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds float, float* %76, i32 0
  store float %75, float* %arrayidx37, align 4, !tbaa !8
  %77 = load float*, float** %ell, align 4, !tbaa !2
  %78 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds float, float* %77, i32 %78
  %79 = load float, float* %arrayidx38, align 4, !tbaa !8
  store float %79, float* %p1, align 4, !tbaa !8
  %80 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds float, float* %80, i32 1
  %81 = load float, float* %arrayidx39, align 4, !tbaa !8
  %82 = load float, float* %Z21, align 4, !tbaa !8
  %sub40 = fsub float %81, %82
  %83 = load float, float* %p1, align 4, !tbaa !8
  %84 = load float, float* %Z11, align 4, !tbaa !8
  %mul41 = fmul float %83, %84
  %sub42 = fsub float %sub40, %mul41
  store float %sub42, float* %Z21, align 4, !tbaa !8
  %85 = load float, float* %Z21, align 4, !tbaa !8
  %86 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds float, float* %86, i32 1
  store float %85, float* %arrayidx43, align 4, !tbaa !8
  br label %for.inc44

for.inc44:                                        ; preds = %for.end34
  %87 = load i32, i32* %i, align 4, !tbaa !6
  %add45 = add nsw i32 %87, 2
  store i32 %add45, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end46:                                        ; preds = %for.cond
  %88 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  %89 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  %91 = bitcast float** %ex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #7
  %92 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %94 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  %95 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  %97 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  %98 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @_Z9btSolveL1PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #2 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %Z31 = alloca float, align 4
  %Z41 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %p3 = alloca float, align 4
  %p4 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %lskip2 = alloca i32, align 4
  %lskip3 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %lskip1, i32* %lskip1.addr, align 4, !tbaa !6
  %0 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %Z31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %Z41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %p4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast float** %ex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = bitcast i32* %lskip2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast i32* %lskip3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %15
  store i32 %mul, i32* %lskip2, align 4, !tbaa !6
  %16 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 3, %16
  store i32 %mul1, i32* %lskip3, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc251, %entry
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %18, 4
  %cmp = icmp sle i32 %17, %sub
  br i1 %cmp, label %for.body, label %for.end253

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z21, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z31, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z41, align 4, !tbaa !8
  %19 = load float*, float** %L.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul2 = mul nsw i32 %20, %21
  %add.ptr = getelementptr inbounds float, float* %19, i32 %mul2
  store float* %add.ptr, float** %ell, align 4, !tbaa !2
  %22 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %22, float** %ex, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %sub3 = sub nsw i32 %23, 12
  store i32 %sub3, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %24, 0
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %25 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %25, i32 0
  %26 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %26, float* %p1, align 4, !tbaa !8
  %27 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %27, i32 0
  %28 = load float, float* %arrayidx7, align 4, !tbaa !8
  store float %28, float* %q1, align 4, !tbaa !8
  %29 = load float*, float** %ell, align 4, !tbaa !2
  %30 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx8, align 4, !tbaa !8
  store float %31, float* %p2, align 4, !tbaa !8
  %32 = load float*, float** %ell, align 4, !tbaa !2
  %33 = load i32, i32* %lskip2, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx9, align 4, !tbaa !8
  store float %34, float* %p3, align 4, !tbaa !8
  %35 = load float*, float** %ell, align 4, !tbaa !2
  %36 = load i32, i32* %lskip3, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %35, i32 %36
  %37 = load float, float* %arrayidx10, align 4, !tbaa !8
  store float %37, float* %p4, align 4, !tbaa !8
  %38 = load float, float* %p1, align 4, !tbaa !8
  %39 = load float, float* %q1, align 4, !tbaa !8
  %mul11 = fmul float %38, %39
  %40 = load float, float* %Z11, align 4, !tbaa !8
  %add = fadd float %40, %mul11
  store float %add, float* %Z11, align 4, !tbaa !8
  %41 = load float, float* %p2, align 4, !tbaa !8
  %42 = load float, float* %q1, align 4, !tbaa !8
  %mul12 = fmul float %41, %42
  %43 = load float, float* %Z21, align 4, !tbaa !8
  %add13 = fadd float %43, %mul12
  store float %add13, float* %Z21, align 4, !tbaa !8
  %44 = load float, float* %p3, align 4, !tbaa !8
  %45 = load float, float* %q1, align 4, !tbaa !8
  %mul14 = fmul float %44, %45
  %46 = load float, float* %Z31, align 4, !tbaa !8
  %add15 = fadd float %46, %mul14
  store float %add15, float* %Z31, align 4, !tbaa !8
  %47 = load float, float* %p4, align 4, !tbaa !8
  %48 = load float, float* %q1, align 4, !tbaa !8
  %mul16 = fmul float %47, %48
  %49 = load float, float* %Z41, align 4, !tbaa !8
  %add17 = fadd float %49, %mul16
  store float %add17, float* %Z41, align 4, !tbaa !8
  %50 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %50, i32 1
  %51 = load float, float* %arrayidx18, align 4, !tbaa !8
  store float %51, float* %p1, align 4, !tbaa !8
  %52 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %52, i32 1
  %53 = load float, float* %arrayidx19, align 4, !tbaa !8
  store float %53, float* %q1, align 4, !tbaa !8
  %54 = load float*, float** %ell, align 4, !tbaa !2
  %55 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add20 = add nsw i32 1, %55
  %arrayidx21 = getelementptr inbounds float, float* %54, i32 %add20
  %56 = load float, float* %arrayidx21, align 4, !tbaa !8
  store float %56, float* %p2, align 4, !tbaa !8
  %57 = load float*, float** %ell, align 4, !tbaa !2
  %58 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add22 = add nsw i32 1, %58
  %arrayidx23 = getelementptr inbounds float, float* %57, i32 %add22
  %59 = load float, float* %arrayidx23, align 4, !tbaa !8
  store float %59, float* %p3, align 4, !tbaa !8
  %60 = load float*, float** %ell, align 4, !tbaa !2
  %61 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add24 = add nsw i32 1, %61
  %arrayidx25 = getelementptr inbounds float, float* %60, i32 %add24
  %62 = load float, float* %arrayidx25, align 4, !tbaa !8
  store float %62, float* %p4, align 4, !tbaa !8
  %63 = load float, float* %p1, align 4, !tbaa !8
  %64 = load float, float* %q1, align 4, !tbaa !8
  %mul26 = fmul float %63, %64
  %65 = load float, float* %Z11, align 4, !tbaa !8
  %add27 = fadd float %65, %mul26
  store float %add27, float* %Z11, align 4, !tbaa !8
  %66 = load float, float* %p2, align 4, !tbaa !8
  %67 = load float, float* %q1, align 4, !tbaa !8
  %mul28 = fmul float %66, %67
  %68 = load float, float* %Z21, align 4, !tbaa !8
  %add29 = fadd float %68, %mul28
  store float %add29, float* %Z21, align 4, !tbaa !8
  %69 = load float, float* %p3, align 4, !tbaa !8
  %70 = load float, float* %q1, align 4, !tbaa !8
  %mul30 = fmul float %69, %70
  %71 = load float, float* %Z31, align 4, !tbaa !8
  %add31 = fadd float %71, %mul30
  store float %add31, float* %Z31, align 4, !tbaa !8
  %72 = load float, float* %p4, align 4, !tbaa !8
  %73 = load float, float* %q1, align 4, !tbaa !8
  %mul32 = fmul float %72, %73
  %74 = load float, float* %Z41, align 4, !tbaa !8
  %add33 = fadd float %74, %mul32
  store float %add33, float* %Z41, align 4, !tbaa !8
  %75 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds float, float* %75, i32 2
  %76 = load float, float* %arrayidx34, align 4, !tbaa !8
  store float %76, float* %p1, align 4, !tbaa !8
  %77 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds float, float* %77, i32 2
  %78 = load float, float* %arrayidx35, align 4, !tbaa !8
  store float %78, float* %q1, align 4, !tbaa !8
  %79 = load float*, float** %ell, align 4, !tbaa !2
  %80 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add36 = add nsw i32 2, %80
  %arrayidx37 = getelementptr inbounds float, float* %79, i32 %add36
  %81 = load float, float* %arrayidx37, align 4, !tbaa !8
  store float %81, float* %p2, align 4, !tbaa !8
  %82 = load float*, float** %ell, align 4, !tbaa !2
  %83 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add38 = add nsw i32 2, %83
  %arrayidx39 = getelementptr inbounds float, float* %82, i32 %add38
  %84 = load float, float* %arrayidx39, align 4, !tbaa !8
  store float %84, float* %p3, align 4, !tbaa !8
  %85 = load float*, float** %ell, align 4, !tbaa !2
  %86 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add40 = add nsw i32 2, %86
  %arrayidx41 = getelementptr inbounds float, float* %85, i32 %add40
  %87 = load float, float* %arrayidx41, align 4, !tbaa !8
  store float %87, float* %p4, align 4, !tbaa !8
  %88 = load float, float* %p1, align 4, !tbaa !8
  %89 = load float, float* %q1, align 4, !tbaa !8
  %mul42 = fmul float %88, %89
  %90 = load float, float* %Z11, align 4, !tbaa !8
  %add43 = fadd float %90, %mul42
  store float %add43, float* %Z11, align 4, !tbaa !8
  %91 = load float, float* %p2, align 4, !tbaa !8
  %92 = load float, float* %q1, align 4, !tbaa !8
  %mul44 = fmul float %91, %92
  %93 = load float, float* %Z21, align 4, !tbaa !8
  %add45 = fadd float %93, %mul44
  store float %add45, float* %Z21, align 4, !tbaa !8
  %94 = load float, float* %p3, align 4, !tbaa !8
  %95 = load float, float* %q1, align 4, !tbaa !8
  %mul46 = fmul float %94, %95
  %96 = load float, float* %Z31, align 4, !tbaa !8
  %add47 = fadd float %96, %mul46
  store float %add47, float* %Z31, align 4, !tbaa !8
  %97 = load float, float* %p4, align 4, !tbaa !8
  %98 = load float, float* %q1, align 4, !tbaa !8
  %mul48 = fmul float %97, %98
  %99 = load float, float* %Z41, align 4, !tbaa !8
  %add49 = fadd float %99, %mul48
  store float %add49, float* %Z41, align 4, !tbaa !8
  %100 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds float, float* %100, i32 3
  %101 = load float, float* %arrayidx50, align 4, !tbaa !8
  store float %101, float* %p1, align 4, !tbaa !8
  %102 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds float, float* %102, i32 3
  %103 = load float, float* %arrayidx51, align 4, !tbaa !8
  store float %103, float* %q1, align 4, !tbaa !8
  %104 = load float*, float** %ell, align 4, !tbaa !2
  %105 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add52 = add nsw i32 3, %105
  %arrayidx53 = getelementptr inbounds float, float* %104, i32 %add52
  %106 = load float, float* %arrayidx53, align 4, !tbaa !8
  store float %106, float* %p2, align 4, !tbaa !8
  %107 = load float*, float** %ell, align 4, !tbaa !2
  %108 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add54 = add nsw i32 3, %108
  %arrayidx55 = getelementptr inbounds float, float* %107, i32 %add54
  %109 = load float, float* %arrayidx55, align 4, !tbaa !8
  store float %109, float* %p3, align 4, !tbaa !8
  %110 = load float*, float** %ell, align 4, !tbaa !2
  %111 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add56 = add nsw i32 3, %111
  %arrayidx57 = getelementptr inbounds float, float* %110, i32 %add56
  %112 = load float, float* %arrayidx57, align 4, !tbaa !8
  store float %112, float* %p4, align 4, !tbaa !8
  %113 = load float, float* %p1, align 4, !tbaa !8
  %114 = load float, float* %q1, align 4, !tbaa !8
  %mul58 = fmul float %113, %114
  %115 = load float, float* %Z11, align 4, !tbaa !8
  %add59 = fadd float %115, %mul58
  store float %add59, float* %Z11, align 4, !tbaa !8
  %116 = load float, float* %p2, align 4, !tbaa !8
  %117 = load float, float* %q1, align 4, !tbaa !8
  %mul60 = fmul float %116, %117
  %118 = load float, float* %Z21, align 4, !tbaa !8
  %add61 = fadd float %118, %mul60
  store float %add61, float* %Z21, align 4, !tbaa !8
  %119 = load float, float* %p3, align 4, !tbaa !8
  %120 = load float, float* %q1, align 4, !tbaa !8
  %mul62 = fmul float %119, %120
  %121 = load float, float* %Z31, align 4, !tbaa !8
  %add63 = fadd float %121, %mul62
  store float %add63, float* %Z31, align 4, !tbaa !8
  %122 = load float, float* %p4, align 4, !tbaa !8
  %123 = load float, float* %q1, align 4, !tbaa !8
  %mul64 = fmul float %122, %123
  %124 = load float, float* %Z41, align 4, !tbaa !8
  %add65 = fadd float %124, %mul64
  store float %add65, float* %Z41, align 4, !tbaa !8
  %125 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds float, float* %125, i32 4
  %126 = load float, float* %arrayidx66, align 4, !tbaa !8
  store float %126, float* %p1, align 4, !tbaa !8
  %127 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds float, float* %127, i32 4
  %128 = load float, float* %arrayidx67, align 4, !tbaa !8
  store float %128, float* %q1, align 4, !tbaa !8
  %129 = load float*, float** %ell, align 4, !tbaa !2
  %130 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add68 = add nsw i32 4, %130
  %arrayidx69 = getelementptr inbounds float, float* %129, i32 %add68
  %131 = load float, float* %arrayidx69, align 4, !tbaa !8
  store float %131, float* %p2, align 4, !tbaa !8
  %132 = load float*, float** %ell, align 4, !tbaa !2
  %133 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add70 = add nsw i32 4, %133
  %arrayidx71 = getelementptr inbounds float, float* %132, i32 %add70
  %134 = load float, float* %arrayidx71, align 4, !tbaa !8
  store float %134, float* %p3, align 4, !tbaa !8
  %135 = load float*, float** %ell, align 4, !tbaa !2
  %136 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add72 = add nsw i32 4, %136
  %arrayidx73 = getelementptr inbounds float, float* %135, i32 %add72
  %137 = load float, float* %arrayidx73, align 4, !tbaa !8
  store float %137, float* %p4, align 4, !tbaa !8
  %138 = load float, float* %p1, align 4, !tbaa !8
  %139 = load float, float* %q1, align 4, !tbaa !8
  %mul74 = fmul float %138, %139
  %140 = load float, float* %Z11, align 4, !tbaa !8
  %add75 = fadd float %140, %mul74
  store float %add75, float* %Z11, align 4, !tbaa !8
  %141 = load float, float* %p2, align 4, !tbaa !8
  %142 = load float, float* %q1, align 4, !tbaa !8
  %mul76 = fmul float %141, %142
  %143 = load float, float* %Z21, align 4, !tbaa !8
  %add77 = fadd float %143, %mul76
  store float %add77, float* %Z21, align 4, !tbaa !8
  %144 = load float, float* %p3, align 4, !tbaa !8
  %145 = load float, float* %q1, align 4, !tbaa !8
  %mul78 = fmul float %144, %145
  %146 = load float, float* %Z31, align 4, !tbaa !8
  %add79 = fadd float %146, %mul78
  store float %add79, float* %Z31, align 4, !tbaa !8
  %147 = load float, float* %p4, align 4, !tbaa !8
  %148 = load float, float* %q1, align 4, !tbaa !8
  %mul80 = fmul float %147, %148
  %149 = load float, float* %Z41, align 4, !tbaa !8
  %add81 = fadd float %149, %mul80
  store float %add81, float* %Z41, align 4, !tbaa !8
  %150 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds float, float* %150, i32 5
  %151 = load float, float* %arrayidx82, align 4, !tbaa !8
  store float %151, float* %p1, align 4, !tbaa !8
  %152 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds float, float* %152, i32 5
  %153 = load float, float* %arrayidx83, align 4, !tbaa !8
  store float %153, float* %q1, align 4, !tbaa !8
  %154 = load float*, float** %ell, align 4, !tbaa !2
  %155 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add84 = add nsw i32 5, %155
  %arrayidx85 = getelementptr inbounds float, float* %154, i32 %add84
  %156 = load float, float* %arrayidx85, align 4, !tbaa !8
  store float %156, float* %p2, align 4, !tbaa !8
  %157 = load float*, float** %ell, align 4, !tbaa !2
  %158 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add86 = add nsw i32 5, %158
  %arrayidx87 = getelementptr inbounds float, float* %157, i32 %add86
  %159 = load float, float* %arrayidx87, align 4, !tbaa !8
  store float %159, float* %p3, align 4, !tbaa !8
  %160 = load float*, float** %ell, align 4, !tbaa !2
  %161 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add88 = add nsw i32 5, %161
  %arrayidx89 = getelementptr inbounds float, float* %160, i32 %add88
  %162 = load float, float* %arrayidx89, align 4, !tbaa !8
  store float %162, float* %p4, align 4, !tbaa !8
  %163 = load float, float* %p1, align 4, !tbaa !8
  %164 = load float, float* %q1, align 4, !tbaa !8
  %mul90 = fmul float %163, %164
  %165 = load float, float* %Z11, align 4, !tbaa !8
  %add91 = fadd float %165, %mul90
  store float %add91, float* %Z11, align 4, !tbaa !8
  %166 = load float, float* %p2, align 4, !tbaa !8
  %167 = load float, float* %q1, align 4, !tbaa !8
  %mul92 = fmul float %166, %167
  %168 = load float, float* %Z21, align 4, !tbaa !8
  %add93 = fadd float %168, %mul92
  store float %add93, float* %Z21, align 4, !tbaa !8
  %169 = load float, float* %p3, align 4, !tbaa !8
  %170 = load float, float* %q1, align 4, !tbaa !8
  %mul94 = fmul float %169, %170
  %171 = load float, float* %Z31, align 4, !tbaa !8
  %add95 = fadd float %171, %mul94
  store float %add95, float* %Z31, align 4, !tbaa !8
  %172 = load float, float* %p4, align 4, !tbaa !8
  %173 = load float, float* %q1, align 4, !tbaa !8
  %mul96 = fmul float %172, %173
  %174 = load float, float* %Z41, align 4, !tbaa !8
  %add97 = fadd float %174, %mul96
  store float %add97, float* %Z41, align 4, !tbaa !8
  %175 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds float, float* %175, i32 6
  %176 = load float, float* %arrayidx98, align 4, !tbaa !8
  store float %176, float* %p1, align 4, !tbaa !8
  %177 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds float, float* %177, i32 6
  %178 = load float, float* %arrayidx99, align 4, !tbaa !8
  store float %178, float* %q1, align 4, !tbaa !8
  %179 = load float*, float** %ell, align 4, !tbaa !2
  %180 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add100 = add nsw i32 6, %180
  %arrayidx101 = getelementptr inbounds float, float* %179, i32 %add100
  %181 = load float, float* %arrayidx101, align 4, !tbaa !8
  store float %181, float* %p2, align 4, !tbaa !8
  %182 = load float*, float** %ell, align 4, !tbaa !2
  %183 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add102 = add nsw i32 6, %183
  %arrayidx103 = getelementptr inbounds float, float* %182, i32 %add102
  %184 = load float, float* %arrayidx103, align 4, !tbaa !8
  store float %184, float* %p3, align 4, !tbaa !8
  %185 = load float*, float** %ell, align 4, !tbaa !2
  %186 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add104 = add nsw i32 6, %186
  %arrayidx105 = getelementptr inbounds float, float* %185, i32 %add104
  %187 = load float, float* %arrayidx105, align 4, !tbaa !8
  store float %187, float* %p4, align 4, !tbaa !8
  %188 = load float, float* %p1, align 4, !tbaa !8
  %189 = load float, float* %q1, align 4, !tbaa !8
  %mul106 = fmul float %188, %189
  %190 = load float, float* %Z11, align 4, !tbaa !8
  %add107 = fadd float %190, %mul106
  store float %add107, float* %Z11, align 4, !tbaa !8
  %191 = load float, float* %p2, align 4, !tbaa !8
  %192 = load float, float* %q1, align 4, !tbaa !8
  %mul108 = fmul float %191, %192
  %193 = load float, float* %Z21, align 4, !tbaa !8
  %add109 = fadd float %193, %mul108
  store float %add109, float* %Z21, align 4, !tbaa !8
  %194 = load float, float* %p3, align 4, !tbaa !8
  %195 = load float, float* %q1, align 4, !tbaa !8
  %mul110 = fmul float %194, %195
  %196 = load float, float* %Z31, align 4, !tbaa !8
  %add111 = fadd float %196, %mul110
  store float %add111, float* %Z31, align 4, !tbaa !8
  %197 = load float, float* %p4, align 4, !tbaa !8
  %198 = load float, float* %q1, align 4, !tbaa !8
  %mul112 = fmul float %197, %198
  %199 = load float, float* %Z41, align 4, !tbaa !8
  %add113 = fadd float %199, %mul112
  store float %add113, float* %Z41, align 4, !tbaa !8
  %200 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds float, float* %200, i32 7
  %201 = load float, float* %arrayidx114, align 4, !tbaa !8
  store float %201, float* %p1, align 4, !tbaa !8
  %202 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds float, float* %202, i32 7
  %203 = load float, float* %arrayidx115, align 4, !tbaa !8
  store float %203, float* %q1, align 4, !tbaa !8
  %204 = load float*, float** %ell, align 4, !tbaa !2
  %205 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add116 = add nsw i32 7, %205
  %arrayidx117 = getelementptr inbounds float, float* %204, i32 %add116
  %206 = load float, float* %arrayidx117, align 4, !tbaa !8
  store float %206, float* %p2, align 4, !tbaa !8
  %207 = load float*, float** %ell, align 4, !tbaa !2
  %208 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add118 = add nsw i32 7, %208
  %arrayidx119 = getelementptr inbounds float, float* %207, i32 %add118
  %209 = load float, float* %arrayidx119, align 4, !tbaa !8
  store float %209, float* %p3, align 4, !tbaa !8
  %210 = load float*, float** %ell, align 4, !tbaa !2
  %211 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add120 = add nsw i32 7, %211
  %arrayidx121 = getelementptr inbounds float, float* %210, i32 %add120
  %212 = load float, float* %arrayidx121, align 4, !tbaa !8
  store float %212, float* %p4, align 4, !tbaa !8
  %213 = load float, float* %p1, align 4, !tbaa !8
  %214 = load float, float* %q1, align 4, !tbaa !8
  %mul122 = fmul float %213, %214
  %215 = load float, float* %Z11, align 4, !tbaa !8
  %add123 = fadd float %215, %mul122
  store float %add123, float* %Z11, align 4, !tbaa !8
  %216 = load float, float* %p2, align 4, !tbaa !8
  %217 = load float, float* %q1, align 4, !tbaa !8
  %mul124 = fmul float %216, %217
  %218 = load float, float* %Z21, align 4, !tbaa !8
  %add125 = fadd float %218, %mul124
  store float %add125, float* %Z21, align 4, !tbaa !8
  %219 = load float, float* %p3, align 4, !tbaa !8
  %220 = load float, float* %q1, align 4, !tbaa !8
  %mul126 = fmul float %219, %220
  %221 = load float, float* %Z31, align 4, !tbaa !8
  %add127 = fadd float %221, %mul126
  store float %add127, float* %Z31, align 4, !tbaa !8
  %222 = load float, float* %p4, align 4, !tbaa !8
  %223 = load float, float* %q1, align 4, !tbaa !8
  %mul128 = fmul float %222, %223
  %224 = load float, float* %Z41, align 4, !tbaa !8
  %add129 = fadd float %224, %mul128
  store float %add129, float* %Z41, align 4, !tbaa !8
  %225 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds float, float* %225, i32 8
  %226 = load float, float* %arrayidx130, align 4, !tbaa !8
  store float %226, float* %p1, align 4, !tbaa !8
  %227 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds float, float* %227, i32 8
  %228 = load float, float* %arrayidx131, align 4, !tbaa !8
  store float %228, float* %q1, align 4, !tbaa !8
  %229 = load float*, float** %ell, align 4, !tbaa !2
  %230 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add132 = add nsw i32 8, %230
  %arrayidx133 = getelementptr inbounds float, float* %229, i32 %add132
  %231 = load float, float* %arrayidx133, align 4, !tbaa !8
  store float %231, float* %p2, align 4, !tbaa !8
  %232 = load float*, float** %ell, align 4, !tbaa !2
  %233 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add134 = add nsw i32 8, %233
  %arrayidx135 = getelementptr inbounds float, float* %232, i32 %add134
  %234 = load float, float* %arrayidx135, align 4, !tbaa !8
  store float %234, float* %p3, align 4, !tbaa !8
  %235 = load float*, float** %ell, align 4, !tbaa !2
  %236 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add136 = add nsw i32 8, %236
  %arrayidx137 = getelementptr inbounds float, float* %235, i32 %add136
  %237 = load float, float* %arrayidx137, align 4, !tbaa !8
  store float %237, float* %p4, align 4, !tbaa !8
  %238 = load float, float* %p1, align 4, !tbaa !8
  %239 = load float, float* %q1, align 4, !tbaa !8
  %mul138 = fmul float %238, %239
  %240 = load float, float* %Z11, align 4, !tbaa !8
  %add139 = fadd float %240, %mul138
  store float %add139, float* %Z11, align 4, !tbaa !8
  %241 = load float, float* %p2, align 4, !tbaa !8
  %242 = load float, float* %q1, align 4, !tbaa !8
  %mul140 = fmul float %241, %242
  %243 = load float, float* %Z21, align 4, !tbaa !8
  %add141 = fadd float %243, %mul140
  store float %add141, float* %Z21, align 4, !tbaa !8
  %244 = load float, float* %p3, align 4, !tbaa !8
  %245 = load float, float* %q1, align 4, !tbaa !8
  %mul142 = fmul float %244, %245
  %246 = load float, float* %Z31, align 4, !tbaa !8
  %add143 = fadd float %246, %mul142
  store float %add143, float* %Z31, align 4, !tbaa !8
  %247 = load float, float* %p4, align 4, !tbaa !8
  %248 = load float, float* %q1, align 4, !tbaa !8
  %mul144 = fmul float %247, %248
  %249 = load float, float* %Z41, align 4, !tbaa !8
  %add145 = fadd float %249, %mul144
  store float %add145, float* %Z41, align 4, !tbaa !8
  %250 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds float, float* %250, i32 9
  %251 = load float, float* %arrayidx146, align 4, !tbaa !8
  store float %251, float* %p1, align 4, !tbaa !8
  %252 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds float, float* %252, i32 9
  %253 = load float, float* %arrayidx147, align 4, !tbaa !8
  store float %253, float* %q1, align 4, !tbaa !8
  %254 = load float*, float** %ell, align 4, !tbaa !2
  %255 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add148 = add nsw i32 9, %255
  %arrayidx149 = getelementptr inbounds float, float* %254, i32 %add148
  %256 = load float, float* %arrayidx149, align 4, !tbaa !8
  store float %256, float* %p2, align 4, !tbaa !8
  %257 = load float*, float** %ell, align 4, !tbaa !2
  %258 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add150 = add nsw i32 9, %258
  %arrayidx151 = getelementptr inbounds float, float* %257, i32 %add150
  %259 = load float, float* %arrayidx151, align 4, !tbaa !8
  store float %259, float* %p3, align 4, !tbaa !8
  %260 = load float*, float** %ell, align 4, !tbaa !2
  %261 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add152 = add nsw i32 9, %261
  %arrayidx153 = getelementptr inbounds float, float* %260, i32 %add152
  %262 = load float, float* %arrayidx153, align 4, !tbaa !8
  store float %262, float* %p4, align 4, !tbaa !8
  %263 = load float, float* %p1, align 4, !tbaa !8
  %264 = load float, float* %q1, align 4, !tbaa !8
  %mul154 = fmul float %263, %264
  %265 = load float, float* %Z11, align 4, !tbaa !8
  %add155 = fadd float %265, %mul154
  store float %add155, float* %Z11, align 4, !tbaa !8
  %266 = load float, float* %p2, align 4, !tbaa !8
  %267 = load float, float* %q1, align 4, !tbaa !8
  %mul156 = fmul float %266, %267
  %268 = load float, float* %Z21, align 4, !tbaa !8
  %add157 = fadd float %268, %mul156
  store float %add157, float* %Z21, align 4, !tbaa !8
  %269 = load float, float* %p3, align 4, !tbaa !8
  %270 = load float, float* %q1, align 4, !tbaa !8
  %mul158 = fmul float %269, %270
  %271 = load float, float* %Z31, align 4, !tbaa !8
  %add159 = fadd float %271, %mul158
  store float %add159, float* %Z31, align 4, !tbaa !8
  %272 = load float, float* %p4, align 4, !tbaa !8
  %273 = load float, float* %q1, align 4, !tbaa !8
  %mul160 = fmul float %272, %273
  %274 = load float, float* %Z41, align 4, !tbaa !8
  %add161 = fadd float %274, %mul160
  store float %add161, float* %Z41, align 4, !tbaa !8
  %275 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds float, float* %275, i32 10
  %276 = load float, float* %arrayidx162, align 4, !tbaa !8
  store float %276, float* %p1, align 4, !tbaa !8
  %277 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds float, float* %277, i32 10
  %278 = load float, float* %arrayidx163, align 4, !tbaa !8
  store float %278, float* %q1, align 4, !tbaa !8
  %279 = load float*, float** %ell, align 4, !tbaa !2
  %280 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add164 = add nsw i32 10, %280
  %arrayidx165 = getelementptr inbounds float, float* %279, i32 %add164
  %281 = load float, float* %arrayidx165, align 4, !tbaa !8
  store float %281, float* %p2, align 4, !tbaa !8
  %282 = load float*, float** %ell, align 4, !tbaa !2
  %283 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add166 = add nsw i32 10, %283
  %arrayidx167 = getelementptr inbounds float, float* %282, i32 %add166
  %284 = load float, float* %arrayidx167, align 4, !tbaa !8
  store float %284, float* %p3, align 4, !tbaa !8
  %285 = load float*, float** %ell, align 4, !tbaa !2
  %286 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add168 = add nsw i32 10, %286
  %arrayidx169 = getelementptr inbounds float, float* %285, i32 %add168
  %287 = load float, float* %arrayidx169, align 4, !tbaa !8
  store float %287, float* %p4, align 4, !tbaa !8
  %288 = load float, float* %p1, align 4, !tbaa !8
  %289 = load float, float* %q1, align 4, !tbaa !8
  %mul170 = fmul float %288, %289
  %290 = load float, float* %Z11, align 4, !tbaa !8
  %add171 = fadd float %290, %mul170
  store float %add171, float* %Z11, align 4, !tbaa !8
  %291 = load float, float* %p2, align 4, !tbaa !8
  %292 = load float, float* %q1, align 4, !tbaa !8
  %mul172 = fmul float %291, %292
  %293 = load float, float* %Z21, align 4, !tbaa !8
  %add173 = fadd float %293, %mul172
  store float %add173, float* %Z21, align 4, !tbaa !8
  %294 = load float, float* %p3, align 4, !tbaa !8
  %295 = load float, float* %q1, align 4, !tbaa !8
  %mul174 = fmul float %294, %295
  %296 = load float, float* %Z31, align 4, !tbaa !8
  %add175 = fadd float %296, %mul174
  store float %add175, float* %Z31, align 4, !tbaa !8
  %297 = load float, float* %p4, align 4, !tbaa !8
  %298 = load float, float* %q1, align 4, !tbaa !8
  %mul176 = fmul float %297, %298
  %299 = load float, float* %Z41, align 4, !tbaa !8
  %add177 = fadd float %299, %mul176
  store float %add177, float* %Z41, align 4, !tbaa !8
  %300 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx178 = getelementptr inbounds float, float* %300, i32 11
  %301 = load float, float* %arrayidx178, align 4, !tbaa !8
  store float %301, float* %p1, align 4, !tbaa !8
  %302 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds float, float* %302, i32 11
  %303 = load float, float* %arrayidx179, align 4, !tbaa !8
  store float %303, float* %q1, align 4, !tbaa !8
  %304 = load float*, float** %ell, align 4, !tbaa !2
  %305 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add180 = add nsw i32 11, %305
  %arrayidx181 = getelementptr inbounds float, float* %304, i32 %add180
  %306 = load float, float* %arrayidx181, align 4, !tbaa !8
  store float %306, float* %p2, align 4, !tbaa !8
  %307 = load float*, float** %ell, align 4, !tbaa !2
  %308 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add182 = add nsw i32 11, %308
  %arrayidx183 = getelementptr inbounds float, float* %307, i32 %add182
  %309 = load float, float* %arrayidx183, align 4, !tbaa !8
  store float %309, float* %p3, align 4, !tbaa !8
  %310 = load float*, float** %ell, align 4, !tbaa !2
  %311 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add184 = add nsw i32 11, %311
  %arrayidx185 = getelementptr inbounds float, float* %310, i32 %add184
  %312 = load float, float* %arrayidx185, align 4, !tbaa !8
  store float %312, float* %p4, align 4, !tbaa !8
  %313 = load float, float* %p1, align 4, !tbaa !8
  %314 = load float, float* %q1, align 4, !tbaa !8
  %mul186 = fmul float %313, %314
  %315 = load float, float* %Z11, align 4, !tbaa !8
  %add187 = fadd float %315, %mul186
  store float %add187, float* %Z11, align 4, !tbaa !8
  %316 = load float, float* %p2, align 4, !tbaa !8
  %317 = load float, float* %q1, align 4, !tbaa !8
  %mul188 = fmul float %316, %317
  %318 = load float, float* %Z21, align 4, !tbaa !8
  %add189 = fadd float %318, %mul188
  store float %add189, float* %Z21, align 4, !tbaa !8
  %319 = load float, float* %p3, align 4, !tbaa !8
  %320 = load float, float* %q1, align 4, !tbaa !8
  %mul190 = fmul float %319, %320
  %321 = load float, float* %Z31, align 4, !tbaa !8
  %add191 = fadd float %321, %mul190
  store float %add191, float* %Z31, align 4, !tbaa !8
  %322 = load float, float* %p4, align 4, !tbaa !8
  %323 = load float, float* %q1, align 4, !tbaa !8
  %mul192 = fmul float %322, %323
  %324 = load float, float* %Z41, align 4, !tbaa !8
  %add193 = fadd float %324, %mul192
  store float %add193, float* %Z41, align 4, !tbaa !8
  %325 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr194 = getelementptr inbounds float, float* %325, i32 12
  store float* %add.ptr194, float** %ell, align 4, !tbaa !2
  %326 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr195 = getelementptr inbounds float, float* %326, i32 12
  store float* %add.ptr195, float** %ex, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %327 = load i32, i32* %j, align 4, !tbaa !6
  %sub196 = sub nsw i32 %327, 12
  store i32 %sub196, i32* %j, align 4, !tbaa !6
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %328 = load i32, i32* %j, align 4, !tbaa !6
  %add197 = add nsw i32 %328, 12
  store i32 %add197, i32* %j, align 4, !tbaa !6
  br label %for.cond198

for.cond198:                                      ; preds = %for.inc216, %for.end
  %329 = load i32, i32* %j, align 4, !tbaa !6
  %cmp199 = icmp sgt i32 %329, 0
  br i1 %cmp199, label %for.body200, label %for.end217

for.body200:                                      ; preds = %for.cond198
  %330 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds float, float* %330, i32 0
  %331 = load float, float* %arrayidx201, align 4, !tbaa !8
  store float %331, float* %p1, align 4, !tbaa !8
  %332 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds float, float* %332, i32 0
  %333 = load float, float* %arrayidx202, align 4, !tbaa !8
  store float %333, float* %q1, align 4, !tbaa !8
  %334 = load float*, float** %ell, align 4, !tbaa !2
  %335 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx203 = getelementptr inbounds float, float* %334, i32 %335
  %336 = load float, float* %arrayidx203, align 4, !tbaa !8
  store float %336, float* %p2, align 4, !tbaa !8
  %337 = load float*, float** %ell, align 4, !tbaa !2
  %338 = load i32, i32* %lskip2, align 4, !tbaa !6
  %arrayidx204 = getelementptr inbounds float, float* %337, i32 %338
  %339 = load float, float* %arrayidx204, align 4, !tbaa !8
  store float %339, float* %p3, align 4, !tbaa !8
  %340 = load float*, float** %ell, align 4, !tbaa !2
  %341 = load i32, i32* %lskip3, align 4, !tbaa !6
  %arrayidx205 = getelementptr inbounds float, float* %340, i32 %341
  %342 = load float, float* %arrayidx205, align 4, !tbaa !8
  store float %342, float* %p4, align 4, !tbaa !8
  %343 = load float, float* %p1, align 4, !tbaa !8
  %344 = load float, float* %q1, align 4, !tbaa !8
  %mul206 = fmul float %343, %344
  %345 = load float, float* %Z11, align 4, !tbaa !8
  %add207 = fadd float %345, %mul206
  store float %add207, float* %Z11, align 4, !tbaa !8
  %346 = load float, float* %p2, align 4, !tbaa !8
  %347 = load float, float* %q1, align 4, !tbaa !8
  %mul208 = fmul float %346, %347
  %348 = load float, float* %Z21, align 4, !tbaa !8
  %add209 = fadd float %348, %mul208
  store float %add209, float* %Z21, align 4, !tbaa !8
  %349 = load float, float* %p3, align 4, !tbaa !8
  %350 = load float, float* %q1, align 4, !tbaa !8
  %mul210 = fmul float %349, %350
  %351 = load float, float* %Z31, align 4, !tbaa !8
  %add211 = fadd float %351, %mul210
  store float %add211, float* %Z31, align 4, !tbaa !8
  %352 = load float, float* %p4, align 4, !tbaa !8
  %353 = load float, float* %q1, align 4, !tbaa !8
  %mul212 = fmul float %352, %353
  %354 = load float, float* %Z41, align 4, !tbaa !8
  %add213 = fadd float %354, %mul212
  store float %add213, float* %Z41, align 4, !tbaa !8
  %355 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr214 = getelementptr inbounds float, float* %355, i32 1
  store float* %add.ptr214, float** %ell, align 4, !tbaa !2
  %356 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr215 = getelementptr inbounds float, float* %356, i32 1
  store float* %add.ptr215, float** %ex, align 4, !tbaa !2
  br label %for.inc216

for.inc216:                                       ; preds = %for.body200
  %357 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %357, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond198

for.end217:                                       ; preds = %for.cond198
  %358 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds float, float* %358, i32 0
  %359 = load float, float* %arrayidx218, align 4, !tbaa !8
  %360 = load float, float* %Z11, align 4, !tbaa !8
  %sub219 = fsub float %359, %360
  store float %sub219, float* %Z11, align 4, !tbaa !8
  %361 = load float, float* %Z11, align 4, !tbaa !8
  %362 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds float, float* %362, i32 0
  store float %361, float* %arrayidx220, align 4, !tbaa !8
  %363 = load float*, float** %ell, align 4, !tbaa !2
  %364 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %arrayidx221 = getelementptr inbounds float, float* %363, i32 %364
  %365 = load float, float* %arrayidx221, align 4, !tbaa !8
  store float %365, float* %p1, align 4, !tbaa !8
  %366 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds float, float* %366, i32 1
  %367 = load float, float* %arrayidx222, align 4, !tbaa !8
  %368 = load float, float* %Z21, align 4, !tbaa !8
  %sub223 = fsub float %367, %368
  %369 = load float, float* %p1, align 4, !tbaa !8
  %370 = load float, float* %Z11, align 4, !tbaa !8
  %mul224 = fmul float %369, %370
  %sub225 = fsub float %sub223, %mul224
  store float %sub225, float* %Z21, align 4, !tbaa !8
  %371 = load float, float* %Z21, align 4, !tbaa !8
  %372 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx226 = getelementptr inbounds float, float* %372, i32 1
  store float %371, float* %arrayidx226, align 4, !tbaa !8
  %373 = load float*, float** %ell, align 4, !tbaa !2
  %374 = load i32, i32* %lskip2, align 4, !tbaa !6
  %arrayidx227 = getelementptr inbounds float, float* %373, i32 %374
  %375 = load float, float* %arrayidx227, align 4, !tbaa !8
  store float %375, float* %p1, align 4, !tbaa !8
  %376 = load float*, float** %ell, align 4, !tbaa !2
  %377 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add228 = add nsw i32 1, %377
  %arrayidx229 = getelementptr inbounds float, float* %376, i32 %add228
  %378 = load float, float* %arrayidx229, align 4, !tbaa !8
  store float %378, float* %p2, align 4, !tbaa !8
  %379 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds float, float* %379, i32 2
  %380 = load float, float* %arrayidx230, align 4, !tbaa !8
  %381 = load float, float* %Z31, align 4, !tbaa !8
  %sub231 = fsub float %380, %381
  %382 = load float, float* %p1, align 4, !tbaa !8
  %383 = load float, float* %Z11, align 4, !tbaa !8
  %mul232 = fmul float %382, %383
  %sub233 = fsub float %sub231, %mul232
  %384 = load float, float* %p2, align 4, !tbaa !8
  %385 = load float, float* %Z21, align 4, !tbaa !8
  %mul234 = fmul float %384, %385
  %sub235 = fsub float %sub233, %mul234
  store float %sub235, float* %Z31, align 4, !tbaa !8
  %386 = load float, float* %Z31, align 4, !tbaa !8
  %387 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds float, float* %387, i32 2
  store float %386, float* %arrayidx236, align 4, !tbaa !8
  %388 = load float*, float** %ell, align 4, !tbaa !2
  %389 = load i32, i32* %lskip3, align 4, !tbaa !6
  %arrayidx237 = getelementptr inbounds float, float* %388, i32 %389
  %390 = load float, float* %arrayidx237, align 4, !tbaa !8
  store float %390, float* %p1, align 4, !tbaa !8
  %391 = load float*, float** %ell, align 4, !tbaa !2
  %392 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add238 = add nsw i32 1, %392
  %arrayidx239 = getelementptr inbounds float, float* %391, i32 %add238
  %393 = load float, float* %arrayidx239, align 4, !tbaa !8
  store float %393, float* %p2, align 4, !tbaa !8
  %394 = load float*, float** %ell, align 4, !tbaa !2
  %395 = load i32, i32* %lskip3, align 4, !tbaa !6
  %add240 = add nsw i32 2, %395
  %arrayidx241 = getelementptr inbounds float, float* %394, i32 %add240
  %396 = load float, float* %arrayidx241, align 4, !tbaa !8
  store float %396, float* %p3, align 4, !tbaa !8
  %397 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds float, float* %397, i32 3
  %398 = load float, float* %arrayidx242, align 4, !tbaa !8
  %399 = load float, float* %Z41, align 4, !tbaa !8
  %sub243 = fsub float %398, %399
  %400 = load float, float* %p1, align 4, !tbaa !8
  %401 = load float, float* %Z11, align 4, !tbaa !8
  %mul244 = fmul float %400, %401
  %sub245 = fsub float %sub243, %mul244
  %402 = load float, float* %p2, align 4, !tbaa !8
  %403 = load float, float* %Z21, align 4, !tbaa !8
  %mul246 = fmul float %402, %403
  %sub247 = fsub float %sub245, %mul246
  %404 = load float, float* %p3, align 4, !tbaa !8
  %405 = load float, float* %Z31, align 4, !tbaa !8
  %mul248 = fmul float %404, %405
  %sub249 = fsub float %sub247, %mul248
  store float %sub249, float* %Z41, align 4, !tbaa !8
  %406 = load float, float* %Z41, align 4, !tbaa !8
  %407 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds float, float* %407, i32 3
  store float %406, float* %arrayidx250, align 4, !tbaa !8
  br label %for.inc251

for.inc251:                                       ; preds = %for.end217
  %408 = load i32, i32* %i, align 4, !tbaa !6
  %add252 = add nsw i32 %408, 4
  store i32 %add252, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end253:                                       ; preds = %for.cond
  br label %for.cond254

for.cond254:                                      ; preds = %for.inc332, %for.end253
  %409 = load i32, i32* %i, align 4, !tbaa !6
  %410 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp255 = icmp slt i32 %409, %410
  br i1 %cmp255, label %for.body256, label %for.end333

for.body256:                                      ; preds = %for.cond254
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  %411 = load float*, float** %L.addr, align 4, !tbaa !2
  %412 = load i32, i32* %i, align 4, !tbaa !6
  %413 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul257 = mul nsw i32 %412, %413
  %add.ptr258 = getelementptr inbounds float, float* %411, i32 %mul257
  store float* %add.ptr258, float** %ell, align 4, !tbaa !2
  %414 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %414, float** %ex, align 4, !tbaa !2
  %415 = load i32, i32* %i, align 4, !tbaa !6
  %sub259 = sub nsw i32 %415, 12
  store i32 %sub259, i32* %j, align 4, !tbaa !6
  br label %for.cond260

for.cond260:                                      ; preds = %for.inc313, %for.body256
  %416 = load i32, i32* %j, align 4, !tbaa !6
  %cmp261 = icmp sge i32 %416, 0
  br i1 %cmp261, label %for.body262, label %for.end315

for.body262:                                      ; preds = %for.cond260
  %417 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx263 = getelementptr inbounds float, float* %417, i32 0
  %418 = load float, float* %arrayidx263, align 4, !tbaa !8
  store float %418, float* %p1, align 4, !tbaa !8
  %419 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds float, float* %419, i32 0
  %420 = load float, float* %arrayidx264, align 4, !tbaa !8
  store float %420, float* %q1, align 4, !tbaa !8
  %421 = load float, float* %p1, align 4, !tbaa !8
  %422 = load float, float* %q1, align 4, !tbaa !8
  %mul265 = fmul float %421, %422
  %423 = load float, float* %Z11, align 4, !tbaa !8
  %add266 = fadd float %423, %mul265
  store float %add266, float* %Z11, align 4, !tbaa !8
  %424 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx267 = getelementptr inbounds float, float* %424, i32 1
  %425 = load float, float* %arrayidx267, align 4, !tbaa !8
  store float %425, float* %p1, align 4, !tbaa !8
  %426 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx268 = getelementptr inbounds float, float* %426, i32 1
  %427 = load float, float* %arrayidx268, align 4, !tbaa !8
  store float %427, float* %q1, align 4, !tbaa !8
  %428 = load float, float* %p1, align 4, !tbaa !8
  %429 = load float, float* %q1, align 4, !tbaa !8
  %mul269 = fmul float %428, %429
  %430 = load float, float* %Z11, align 4, !tbaa !8
  %add270 = fadd float %430, %mul269
  store float %add270, float* %Z11, align 4, !tbaa !8
  %431 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds float, float* %431, i32 2
  %432 = load float, float* %arrayidx271, align 4, !tbaa !8
  store float %432, float* %p1, align 4, !tbaa !8
  %433 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx272 = getelementptr inbounds float, float* %433, i32 2
  %434 = load float, float* %arrayidx272, align 4, !tbaa !8
  store float %434, float* %q1, align 4, !tbaa !8
  %435 = load float, float* %p1, align 4, !tbaa !8
  %436 = load float, float* %q1, align 4, !tbaa !8
  %mul273 = fmul float %435, %436
  %437 = load float, float* %Z11, align 4, !tbaa !8
  %add274 = fadd float %437, %mul273
  store float %add274, float* %Z11, align 4, !tbaa !8
  %438 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx275 = getelementptr inbounds float, float* %438, i32 3
  %439 = load float, float* %arrayidx275, align 4, !tbaa !8
  store float %439, float* %p1, align 4, !tbaa !8
  %440 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx276 = getelementptr inbounds float, float* %440, i32 3
  %441 = load float, float* %arrayidx276, align 4, !tbaa !8
  store float %441, float* %q1, align 4, !tbaa !8
  %442 = load float, float* %p1, align 4, !tbaa !8
  %443 = load float, float* %q1, align 4, !tbaa !8
  %mul277 = fmul float %442, %443
  %444 = load float, float* %Z11, align 4, !tbaa !8
  %add278 = fadd float %444, %mul277
  store float %add278, float* %Z11, align 4, !tbaa !8
  %445 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds float, float* %445, i32 4
  %446 = load float, float* %arrayidx279, align 4, !tbaa !8
  store float %446, float* %p1, align 4, !tbaa !8
  %447 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds float, float* %447, i32 4
  %448 = load float, float* %arrayidx280, align 4, !tbaa !8
  store float %448, float* %q1, align 4, !tbaa !8
  %449 = load float, float* %p1, align 4, !tbaa !8
  %450 = load float, float* %q1, align 4, !tbaa !8
  %mul281 = fmul float %449, %450
  %451 = load float, float* %Z11, align 4, !tbaa !8
  %add282 = fadd float %451, %mul281
  store float %add282, float* %Z11, align 4, !tbaa !8
  %452 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx283 = getelementptr inbounds float, float* %452, i32 5
  %453 = load float, float* %arrayidx283, align 4, !tbaa !8
  store float %453, float* %p1, align 4, !tbaa !8
  %454 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds float, float* %454, i32 5
  %455 = load float, float* %arrayidx284, align 4, !tbaa !8
  store float %455, float* %q1, align 4, !tbaa !8
  %456 = load float, float* %p1, align 4, !tbaa !8
  %457 = load float, float* %q1, align 4, !tbaa !8
  %mul285 = fmul float %456, %457
  %458 = load float, float* %Z11, align 4, !tbaa !8
  %add286 = fadd float %458, %mul285
  store float %add286, float* %Z11, align 4, !tbaa !8
  %459 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx287 = getelementptr inbounds float, float* %459, i32 6
  %460 = load float, float* %arrayidx287, align 4, !tbaa !8
  store float %460, float* %p1, align 4, !tbaa !8
  %461 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx288 = getelementptr inbounds float, float* %461, i32 6
  %462 = load float, float* %arrayidx288, align 4, !tbaa !8
  store float %462, float* %q1, align 4, !tbaa !8
  %463 = load float, float* %p1, align 4, !tbaa !8
  %464 = load float, float* %q1, align 4, !tbaa !8
  %mul289 = fmul float %463, %464
  %465 = load float, float* %Z11, align 4, !tbaa !8
  %add290 = fadd float %465, %mul289
  store float %add290, float* %Z11, align 4, !tbaa !8
  %466 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds float, float* %466, i32 7
  %467 = load float, float* %arrayidx291, align 4, !tbaa !8
  store float %467, float* %p1, align 4, !tbaa !8
  %468 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx292 = getelementptr inbounds float, float* %468, i32 7
  %469 = load float, float* %arrayidx292, align 4, !tbaa !8
  store float %469, float* %q1, align 4, !tbaa !8
  %470 = load float, float* %p1, align 4, !tbaa !8
  %471 = load float, float* %q1, align 4, !tbaa !8
  %mul293 = fmul float %470, %471
  %472 = load float, float* %Z11, align 4, !tbaa !8
  %add294 = fadd float %472, %mul293
  store float %add294, float* %Z11, align 4, !tbaa !8
  %473 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx295 = getelementptr inbounds float, float* %473, i32 8
  %474 = load float, float* %arrayidx295, align 4, !tbaa !8
  store float %474, float* %p1, align 4, !tbaa !8
  %475 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds float, float* %475, i32 8
  %476 = load float, float* %arrayidx296, align 4, !tbaa !8
  store float %476, float* %q1, align 4, !tbaa !8
  %477 = load float, float* %p1, align 4, !tbaa !8
  %478 = load float, float* %q1, align 4, !tbaa !8
  %mul297 = fmul float %477, %478
  %479 = load float, float* %Z11, align 4, !tbaa !8
  %add298 = fadd float %479, %mul297
  store float %add298, float* %Z11, align 4, !tbaa !8
  %480 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx299 = getelementptr inbounds float, float* %480, i32 9
  %481 = load float, float* %arrayidx299, align 4, !tbaa !8
  store float %481, float* %p1, align 4, !tbaa !8
  %482 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx300 = getelementptr inbounds float, float* %482, i32 9
  %483 = load float, float* %arrayidx300, align 4, !tbaa !8
  store float %483, float* %q1, align 4, !tbaa !8
  %484 = load float, float* %p1, align 4, !tbaa !8
  %485 = load float, float* %q1, align 4, !tbaa !8
  %mul301 = fmul float %484, %485
  %486 = load float, float* %Z11, align 4, !tbaa !8
  %add302 = fadd float %486, %mul301
  store float %add302, float* %Z11, align 4, !tbaa !8
  %487 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx303 = getelementptr inbounds float, float* %487, i32 10
  %488 = load float, float* %arrayidx303, align 4, !tbaa !8
  store float %488, float* %p1, align 4, !tbaa !8
  %489 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx304 = getelementptr inbounds float, float* %489, i32 10
  %490 = load float, float* %arrayidx304, align 4, !tbaa !8
  store float %490, float* %q1, align 4, !tbaa !8
  %491 = load float, float* %p1, align 4, !tbaa !8
  %492 = load float, float* %q1, align 4, !tbaa !8
  %mul305 = fmul float %491, %492
  %493 = load float, float* %Z11, align 4, !tbaa !8
  %add306 = fadd float %493, %mul305
  store float %add306, float* %Z11, align 4, !tbaa !8
  %494 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx307 = getelementptr inbounds float, float* %494, i32 11
  %495 = load float, float* %arrayidx307, align 4, !tbaa !8
  store float %495, float* %p1, align 4, !tbaa !8
  %496 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx308 = getelementptr inbounds float, float* %496, i32 11
  %497 = load float, float* %arrayidx308, align 4, !tbaa !8
  store float %497, float* %q1, align 4, !tbaa !8
  %498 = load float, float* %p1, align 4, !tbaa !8
  %499 = load float, float* %q1, align 4, !tbaa !8
  %mul309 = fmul float %498, %499
  %500 = load float, float* %Z11, align 4, !tbaa !8
  %add310 = fadd float %500, %mul309
  store float %add310, float* %Z11, align 4, !tbaa !8
  %501 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr311 = getelementptr inbounds float, float* %501, i32 12
  store float* %add.ptr311, float** %ell, align 4, !tbaa !2
  %502 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr312 = getelementptr inbounds float, float* %502, i32 12
  store float* %add.ptr312, float** %ex, align 4, !tbaa !2
  br label %for.inc313

for.inc313:                                       ; preds = %for.body262
  %503 = load i32, i32* %j, align 4, !tbaa !6
  %sub314 = sub nsw i32 %503, 12
  store i32 %sub314, i32* %j, align 4, !tbaa !6
  br label %for.cond260

for.end315:                                       ; preds = %for.cond260
  %504 = load i32, i32* %j, align 4, !tbaa !6
  %add316 = add nsw i32 %504, 12
  store i32 %add316, i32* %j, align 4, !tbaa !6
  br label %for.cond317

for.cond317:                                      ; preds = %for.inc326, %for.end315
  %505 = load i32, i32* %j, align 4, !tbaa !6
  %cmp318 = icmp sgt i32 %505, 0
  br i1 %cmp318, label %for.body319, label %for.end328

for.body319:                                      ; preds = %for.cond317
  %506 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx320 = getelementptr inbounds float, float* %506, i32 0
  %507 = load float, float* %arrayidx320, align 4, !tbaa !8
  store float %507, float* %p1, align 4, !tbaa !8
  %508 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds float, float* %508, i32 0
  %509 = load float, float* %arrayidx321, align 4, !tbaa !8
  store float %509, float* %q1, align 4, !tbaa !8
  %510 = load float, float* %p1, align 4, !tbaa !8
  %511 = load float, float* %q1, align 4, !tbaa !8
  %mul322 = fmul float %510, %511
  %512 = load float, float* %Z11, align 4, !tbaa !8
  %add323 = fadd float %512, %mul322
  store float %add323, float* %Z11, align 4, !tbaa !8
  %513 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr324 = getelementptr inbounds float, float* %513, i32 1
  store float* %add.ptr324, float** %ell, align 4, !tbaa !2
  %514 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr325 = getelementptr inbounds float, float* %514, i32 1
  store float* %add.ptr325, float** %ex, align 4, !tbaa !2
  br label %for.inc326

for.inc326:                                       ; preds = %for.body319
  %515 = load i32, i32* %j, align 4, !tbaa !6
  %dec327 = add nsw i32 %515, -1
  store i32 %dec327, i32* %j, align 4, !tbaa !6
  br label %for.cond317

for.end328:                                       ; preds = %for.cond317
  %516 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds float, float* %516, i32 0
  %517 = load float, float* %arrayidx329, align 4, !tbaa !8
  %518 = load float, float* %Z11, align 4, !tbaa !8
  %sub330 = fsub float %517, %518
  store float %sub330, float* %Z11, align 4, !tbaa !8
  %519 = load float, float* %Z11, align 4, !tbaa !8
  %520 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds float, float* %520, i32 0
  store float %519, float* %arrayidx331, align 4, !tbaa !8
  br label %for.inc332

for.inc332:                                       ; preds = %for.end328
  %521 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %521, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond254

for.end333:                                       ; preds = %for.cond254
  %522 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %522) #7
  %523 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %523) #7
  %524 = bitcast i32* %lskip3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %524) #7
  %525 = bitcast i32* %lskip2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %525) #7
  %526 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %526) #7
  %527 = bitcast float** %ex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %527) #7
  %528 = bitcast float* %p4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %528) #7
  %529 = bitcast float* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %529) #7
  %530 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %530) #7
  %531 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %531) #7
  %532 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %532) #7
  %533 = bitcast float* %Z41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %533) #7
  %534 = bitcast float* %Z31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %534) #7
  %535 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %535) #7
  %536 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %536) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z10btSolveL1TPKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #2 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z31 = alloca float, align 4
  %m31 = alloca float, align 4
  %Z41 = alloca float, align 4
  %m41 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %p3 = alloca float, align 4
  %p4 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %lskip2 = alloca i32, align 4
  %lskip3 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %lskip1, i32* %lskip1.addr, align 4, !tbaa !6
  %0 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %Z31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %m31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %Z41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = bitcast float* %m41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = bitcast float* %p3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast float* %p4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = bitcast float** %ex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = bitcast i32* %lskip2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = bitcast i32* %lskip3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load float*, float** %L.addr, align 4, !tbaa !2
  %20 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %20, 1
  %21 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add = add nsw i32 %21, 1
  %mul = mul nsw i32 %sub, %add
  %add.ptr = getelementptr inbounds float, float* %19, i32 %mul
  store float* %add.ptr, float** %L.addr, align 4, !tbaa !2
  %22 = load float*, float** %B.addr, align 4, !tbaa !2
  %23 = load i32, i32* %n.addr, align 4, !tbaa !6
  %add.ptr1 = getelementptr inbounds float, float* %22, i32 %23
  %add.ptr2 = getelementptr inbounds float, float* %add.ptr1, i32 -1
  store float* %add.ptr2, float** %B.addr, align 4, !tbaa !2
  %24 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %sub3 = sub nsw i32 0, %24
  store i32 %sub3, i32* %lskip1.addr, align 4, !tbaa !6
  %25 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul4 = mul nsw i32 2, %25
  store i32 %mul4, i32* %lskip2, align 4, !tbaa !6
  %26 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %mul5 = mul nsw i32 3, %26
  store i32 %mul5, i32* %lskip3, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc123, %entry
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %28 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub6 = sub nsw i32 %28, 4
  %cmp = icmp sle i32 %27, %sub6
  br i1 %cmp, label %for.body, label %for.end125

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z21, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z31, align 4, !tbaa !8
  store float 0.000000e+00, float* %Z41, align 4, !tbaa !8
  %29 = load float*, float** %L.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %idx.neg = sub i32 0, %30
  %add.ptr7 = getelementptr inbounds float, float* %29, i32 %idx.neg
  store float* %add.ptr7, float** %ell, align 4, !tbaa !2
  %31 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %31, float** %ex, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %sub8 = sub nsw i32 %32, 4
  store i32 %sub8, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %33 = load i32, i32* %j, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %33, 0
  br i1 %cmp10, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond9
  %34 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %34, i32 0
  %35 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %35, float* %p1, align 4, !tbaa !8
  %36 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds float, float* %36, i32 0
  %37 = load float, float* %arrayidx12, align 4, !tbaa !8
  store float %37, float* %q1, align 4, !tbaa !8
  %38 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %38, i32 -1
  %39 = load float, float* %arrayidx13, align 4, !tbaa !8
  store float %39, float* %p2, align 4, !tbaa !8
  %40 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %40, i32 -2
  %41 = load float, float* %arrayidx14, align 4, !tbaa !8
  store float %41, float* %p3, align 4, !tbaa !8
  %42 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %42, i32 -3
  %43 = load float, float* %arrayidx15, align 4, !tbaa !8
  store float %43, float* %p4, align 4, !tbaa !8
  %44 = load float, float* %p1, align 4, !tbaa !8
  %45 = load float, float* %q1, align 4, !tbaa !8
  %mul16 = fmul float %44, %45
  store float %mul16, float* %m11, align 4, !tbaa !8
  %46 = load float, float* %p2, align 4, !tbaa !8
  %47 = load float, float* %q1, align 4, !tbaa !8
  %mul17 = fmul float %46, %47
  store float %mul17, float* %m21, align 4, !tbaa !8
  %48 = load float, float* %p3, align 4, !tbaa !8
  %49 = load float, float* %q1, align 4, !tbaa !8
  %mul18 = fmul float %48, %49
  store float %mul18, float* %m31, align 4, !tbaa !8
  %50 = load float, float* %p4, align 4, !tbaa !8
  %51 = load float, float* %q1, align 4, !tbaa !8
  %mul19 = fmul float %50, %51
  store float %mul19, float* %m41, align 4, !tbaa !8
  %52 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %53 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds float, float* %53, i32 %52
  store float* %add.ptr20, float** %ell, align 4, !tbaa !2
  %54 = load float, float* %m11, align 4, !tbaa !8
  %55 = load float, float* %Z11, align 4, !tbaa !8
  %add21 = fadd float %55, %54
  store float %add21, float* %Z11, align 4, !tbaa !8
  %56 = load float, float* %m21, align 4, !tbaa !8
  %57 = load float, float* %Z21, align 4, !tbaa !8
  %add22 = fadd float %57, %56
  store float %add22, float* %Z21, align 4, !tbaa !8
  %58 = load float, float* %m31, align 4, !tbaa !8
  %59 = load float, float* %Z31, align 4, !tbaa !8
  %add23 = fadd float %59, %58
  store float %add23, float* %Z31, align 4, !tbaa !8
  %60 = load float, float* %m41, align 4, !tbaa !8
  %61 = load float, float* %Z41, align 4, !tbaa !8
  %add24 = fadd float %61, %60
  store float %add24, float* %Z41, align 4, !tbaa !8
  %62 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds float, float* %62, i32 0
  %63 = load float, float* %arrayidx25, align 4, !tbaa !8
  store float %63, float* %p1, align 4, !tbaa !8
  %64 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds float, float* %64, i32 -1
  %65 = load float, float* %arrayidx26, align 4, !tbaa !8
  store float %65, float* %q1, align 4, !tbaa !8
  %66 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds float, float* %66, i32 -1
  %67 = load float, float* %arrayidx27, align 4, !tbaa !8
  store float %67, float* %p2, align 4, !tbaa !8
  %68 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds float, float* %68, i32 -2
  %69 = load float, float* %arrayidx28, align 4, !tbaa !8
  store float %69, float* %p3, align 4, !tbaa !8
  %70 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds float, float* %70, i32 -3
  %71 = load float, float* %arrayidx29, align 4, !tbaa !8
  store float %71, float* %p4, align 4, !tbaa !8
  %72 = load float, float* %p1, align 4, !tbaa !8
  %73 = load float, float* %q1, align 4, !tbaa !8
  %mul30 = fmul float %72, %73
  store float %mul30, float* %m11, align 4, !tbaa !8
  %74 = load float, float* %p2, align 4, !tbaa !8
  %75 = load float, float* %q1, align 4, !tbaa !8
  %mul31 = fmul float %74, %75
  store float %mul31, float* %m21, align 4, !tbaa !8
  %76 = load float, float* %p3, align 4, !tbaa !8
  %77 = load float, float* %q1, align 4, !tbaa !8
  %mul32 = fmul float %76, %77
  store float %mul32, float* %m31, align 4, !tbaa !8
  %78 = load float, float* %p4, align 4, !tbaa !8
  %79 = load float, float* %q1, align 4, !tbaa !8
  %mul33 = fmul float %78, %79
  store float %mul33, float* %m41, align 4, !tbaa !8
  %80 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %81 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr34 = getelementptr inbounds float, float* %81, i32 %80
  store float* %add.ptr34, float** %ell, align 4, !tbaa !2
  %82 = load float, float* %m11, align 4, !tbaa !8
  %83 = load float, float* %Z11, align 4, !tbaa !8
  %add35 = fadd float %83, %82
  store float %add35, float* %Z11, align 4, !tbaa !8
  %84 = load float, float* %m21, align 4, !tbaa !8
  %85 = load float, float* %Z21, align 4, !tbaa !8
  %add36 = fadd float %85, %84
  store float %add36, float* %Z21, align 4, !tbaa !8
  %86 = load float, float* %m31, align 4, !tbaa !8
  %87 = load float, float* %Z31, align 4, !tbaa !8
  %add37 = fadd float %87, %86
  store float %add37, float* %Z31, align 4, !tbaa !8
  %88 = load float, float* %m41, align 4, !tbaa !8
  %89 = load float, float* %Z41, align 4, !tbaa !8
  %add38 = fadd float %89, %88
  store float %add38, float* %Z41, align 4, !tbaa !8
  %90 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds float, float* %90, i32 0
  %91 = load float, float* %arrayidx39, align 4, !tbaa !8
  store float %91, float* %p1, align 4, !tbaa !8
  %92 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds float, float* %92, i32 -2
  %93 = load float, float* %arrayidx40, align 4, !tbaa !8
  store float %93, float* %q1, align 4, !tbaa !8
  %94 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds float, float* %94, i32 -1
  %95 = load float, float* %arrayidx41, align 4, !tbaa !8
  store float %95, float* %p2, align 4, !tbaa !8
  %96 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds float, float* %96, i32 -2
  %97 = load float, float* %arrayidx42, align 4, !tbaa !8
  store float %97, float* %p3, align 4, !tbaa !8
  %98 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds float, float* %98, i32 -3
  %99 = load float, float* %arrayidx43, align 4, !tbaa !8
  store float %99, float* %p4, align 4, !tbaa !8
  %100 = load float, float* %p1, align 4, !tbaa !8
  %101 = load float, float* %q1, align 4, !tbaa !8
  %mul44 = fmul float %100, %101
  store float %mul44, float* %m11, align 4, !tbaa !8
  %102 = load float, float* %p2, align 4, !tbaa !8
  %103 = load float, float* %q1, align 4, !tbaa !8
  %mul45 = fmul float %102, %103
  store float %mul45, float* %m21, align 4, !tbaa !8
  %104 = load float, float* %p3, align 4, !tbaa !8
  %105 = load float, float* %q1, align 4, !tbaa !8
  %mul46 = fmul float %104, %105
  store float %mul46, float* %m31, align 4, !tbaa !8
  %106 = load float, float* %p4, align 4, !tbaa !8
  %107 = load float, float* %q1, align 4, !tbaa !8
  %mul47 = fmul float %106, %107
  store float %mul47, float* %m41, align 4, !tbaa !8
  %108 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %109 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds float, float* %109, i32 %108
  store float* %add.ptr48, float** %ell, align 4, !tbaa !2
  %110 = load float, float* %m11, align 4, !tbaa !8
  %111 = load float, float* %Z11, align 4, !tbaa !8
  %add49 = fadd float %111, %110
  store float %add49, float* %Z11, align 4, !tbaa !8
  %112 = load float, float* %m21, align 4, !tbaa !8
  %113 = load float, float* %Z21, align 4, !tbaa !8
  %add50 = fadd float %113, %112
  store float %add50, float* %Z21, align 4, !tbaa !8
  %114 = load float, float* %m31, align 4, !tbaa !8
  %115 = load float, float* %Z31, align 4, !tbaa !8
  %add51 = fadd float %115, %114
  store float %add51, float* %Z31, align 4, !tbaa !8
  %116 = load float, float* %m41, align 4, !tbaa !8
  %117 = load float, float* %Z41, align 4, !tbaa !8
  %add52 = fadd float %117, %116
  store float %add52, float* %Z41, align 4, !tbaa !8
  %118 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds float, float* %118, i32 0
  %119 = load float, float* %arrayidx53, align 4, !tbaa !8
  store float %119, float* %p1, align 4, !tbaa !8
  %120 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds float, float* %120, i32 -3
  %121 = load float, float* %arrayidx54, align 4, !tbaa !8
  store float %121, float* %q1, align 4, !tbaa !8
  %122 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds float, float* %122, i32 -1
  %123 = load float, float* %arrayidx55, align 4, !tbaa !8
  store float %123, float* %p2, align 4, !tbaa !8
  %124 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds float, float* %124, i32 -2
  %125 = load float, float* %arrayidx56, align 4, !tbaa !8
  store float %125, float* %p3, align 4, !tbaa !8
  %126 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds float, float* %126, i32 -3
  %127 = load float, float* %arrayidx57, align 4, !tbaa !8
  store float %127, float* %p4, align 4, !tbaa !8
  %128 = load float, float* %p1, align 4, !tbaa !8
  %129 = load float, float* %q1, align 4, !tbaa !8
  %mul58 = fmul float %128, %129
  store float %mul58, float* %m11, align 4, !tbaa !8
  %130 = load float, float* %p2, align 4, !tbaa !8
  %131 = load float, float* %q1, align 4, !tbaa !8
  %mul59 = fmul float %130, %131
  store float %mul59, float* %m21, align 4, !tbaa !8
  %132 = load float, float* %p3, align 4, !tbaa !8
  %133 = load float, float* %q1, align 4, !tbaa !8
  %mul60 = fmul float %132, %133
  store float %mul60, float* %m31, align 4, !tbaa !8
  %134 = load float, float* %p4, align 4, !tbaa !8
  %135 = load float, float* %q1, align 4, !tbaa !8
  %mul61 = fmul float %134, %135
  store float %mul61, float* %m41, align 4, !tbaa !8
  %136 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %137 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr62 = getelementptr inbounds float, float* %137, i32 %136
  store float* %add.ptr62, float** %ell, align 4, !tbaa !2
  %138 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr63 = getelementptr inbounds float, float* %138, i32 -4
  store float* %add.ptr63, float** %ex, align 4, !tbaa !2
  %139 = load float, float* %m11, align 4, !tbaa !8
  %140 = load float, float* %Z11, align 4, !tbaa !8
  %add64 = fadd float %140, %139
  store float %add64, float* %Z11, align 4, !tbaa !8
  %141 = load float, float* %m21, align 4, !tbaa !8
  %142 = load float, float* %Z21, align 4, !tbaa !8
  %add65 = fadd float %142, %141
  store float %add65, float* %Z21, align 4, !tbaa !8
  %143 = load float, float* %m31, align 4, !tbaa !8
  %144 = load float, float* %Z31, align 4, !tbaa !8
  %add66 = fadd float %144, %143
  store float %add66, float* %Z31, align 4, !tbaa !8
  %145 = load float, float* %m41, align 4, !tbaa !8
  %146 = load float, float* %Z41, align 4, !tbaa !8
  %add67 = fadd float %146, %145
  store float %add67, float* %Z41, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %147 = load i32, i32* %j, align 4, !tbaa !6
  %sub68 = sub nsw i32 %147, 4
  store i32 %sub68, i32* %j, align 4, !tbaa !6
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %148 = load i32, i32* %j, align 4, !tbaa !6
  %add69 = add nsw i32 %148, 4
  store i32 %add69, i32* %j, align 4, !tbaa !6
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc88, %for.end
  %149 = load i32, i32* %j, align 4, !tbaa !6
  %cmp71 = icmp sgt i32 %149, 0
  br i1 %cmp71, label %for.body72, label %for.end89

for.body72:                                       ; preds = %for.cond70
  %150 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds float, float* %150, i32 0
  %151 = load float, float* %arrayidx73, align 4, !tbaa !8
  store float %151, float* %p1, align 4, !tbaa !8
  %152 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds float, float* %152, i32 0
  %153 = load float, float* %arrayidx74, align 4, !tbaa !8
  store float %153, float* %q1, align 4, !tbaa !8
  %154 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds float, float* %154, i32 -1
  %155 = load float, float* %arrayidx75, align 4, !tbaa !8
  store float %155, float* %p2, align 4, !tbaa !8
  %156 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds float, float* %156, i32 -2
  %157 = load float, float* %arrayidx76, align 4, !tbaa !8
  store float %157, float* %p3, align 4, !tbaa !8
  %158 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds float, float* %158, i32 -3
  %159 = load float, float* %arrayidx77, align 4, !tbaa !8
  store float %159, float* %p4, align 4, !tbaa !8
  %160 = load float, float* %p1, align 4, !tbaa !8
  %161 = load float, float* %q1, align 4, !tbaa !8
  %mul78 = fmul float %160, %161
  store float %mul78, float* %m11, align 4, !tbaa !8
  %162 = load float, float* %p2, align 4, !tbaa !8
  %163 = load float, float* %q1, align 4, !tbaa !8
  %mul79 = fmul float %162, %163
  store float %mul79, float* %m21, align 4, !tbaa !8
  %164 = load float, float* %p3, align 4, !tbaa !8
  %165 = load float, float* %q1, align 4, !tbaa !8
  %mul80 = fmul float %164, %165
  store float %mul80, float* %m31, align 4, !tbaa !8
  %166 = load float, float* %p4, align 4, !tbaa !8
  %167 = load float, float* %q1, align 4, !tbaa !8
  %mul81 = fmul float %166, %167
  store float %mul81, float* %m41, align 4, !tbaa !8
  %168 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %169 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr82 = getelementptr inbounds float, float* %169, i32 %168
  store float* %add.ptr82, float** %ell, align 4, !tbaa !2
  %170 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr83 = getelementptr inbounds float, float* %170, i32 -1
  store float* %add.ptr83, float** %ex, align 4, !tbaa !2
  %171 = load float, float* %m11, align 4, !tbaa !8
  %172 = load float, float* %Z11, align 4, !tbaa !8
  %add84 = fadd float %172, %171
  store float %add84, float* %Z11, align 4, !tbaa !8
  %173 = load float, float* %m21, align 4, !tbaa !8
  %174 = load float, float* %Z21, align 4, !tbaa !8
  %add85 = fadd float %174, %173
  store float %add85, float* %Z21, align 4, !tbaa !8
  %175 = load float, float* %m31, align 4, !tbaa !8
  %176 = load float, float* %Z31, align 4, !tbaa !8
  %add86 = fadd float %176, %175
  store float %add86, float* %Z31, align 4, !tbaa !8
  %177 = load float, float* %m41, align 4, !tbaa !8
  %178 = load float, float* %Z41, align 4, !tbaa !8
  %add87 = fadd float %178, %177
  store float %add87, float* %Z41, align 4, !tbaa !8
  br label %for.inc88

for.inc88:                                        ; preds = %for.body72
  %179 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %179, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond70

for.end89:                                        ; preds = %for.cond70
  %180 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds float, float* %180, i32 0
  %181 = load float, float* %arrayidx90, align 4, !tbaa !8
  %182 = load float, float* %Z11, align 4, !tbaa !8
  %sub91 = fsub float %181, %182
  store float %sub91, float* %Z11, align 4, !tbaa !8
  %183 = load float, float* %Z11, align 4, !tbaa !8
  %184 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds float, float* %184, i32 0
  store float %183, float* %arrayidx92, align 4, !tbaa !8
  %185 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds float, float* %185, i32 -1
  %186 = load float, float* %arrayidx93, align 4, !tbaa !8
  store float %186, float* %p1, align 4, !tbaa !8
  %187 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds float, float* %187, i32 -1
  %188 = load float, float* %arrayidx94, align 4, !tbaa !8
  %189 = load float, float* %Z21, align 4, !tbaa !8
  %sub95 = fsub float %188, %189
  %190 = load float, float* %p1, align 4, !tbaa !8
  %191 = load float, float* %Z11, align 4, !tbaa !8
  %mul96 = fmul float %190, %191
  %sub97 = fsub float %sub95, %mul96
  store float %sub97, float* %Z21, align 4, !tbaa !8
  %192 = load float, float* %Z21, align 4, !tbaa !8
  %193 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds float, float* %193, i32 -1
  store float %192, float* %arrayidx98, align 4, !tbaa !8
  %194 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds float, float* %194, i32 -2
  %195 = load float, float* %arrayidx99, align 4, !tbaa !8
  store float %195, float* %p1, align 4, !tbaa !8
  %196 = load float*, float** %ell, align 4, !tbaa !2
  %197 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add100 = add nsw i32 -2, %197
  %arrayidx101 = getelementptr inbounds float, float* %196, i32 %add100
  %198 = load float, float* %arrayidx101, align 4, !tbaa !8
  store float %198, float* %p2, align 4, !tbaa !8
  %199 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds float, float* %199, i32 -2
  %200 = load float, float* %arrayidx102, align 4, !tbaa !8
  %201 = load float, float* %Z31, align 4, !tbaa !8
  %sub103 = fsub float %200, %201
  %202 = load float, float* %p1, align 4, !tbaa !8
  %203 = load float, float* %Z11, align 4, !tbaa !8
  %mul104 = fmul float %202, %203
  %sub105 = fsub float %sub103, %mul104
  %204 = load float, float* %p2, align 4, !tbaa !8
  %205 = load float, float* %Z21, align 4, !tbaa !8
  %mul106 = fmul float %204, %205
  %sub107 = fsub float %sub105, %mul106
  store float %sub107, float* %Z31, align 4, !tbaa !8
  %206 = load float, float* %Z31, align 4, !tbaa !8
  %207 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds float, float* %207, i32 -2
  store float %206, float* %arrayidx108, align 4, !tbaa !8
  %208 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds float, float* %208, i32 -3
  %209 = load float, float* %arrayidx109, align 4, !tbaa !8
  store float %209, float* %p1, align 4, !tbaa !8
  %210 = load float*, float** %ell, align 4, !tbaa !2
  %211 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %add110 = add nsw i32 -3, %211
  %arrayidx111 = getelementptr inbounds float, float* %210, i32 %add110
  %212 = load float, float* %arrayidx111, align 4, !tbaa !8
  store float %212, float* %p2, align 4, !tbaa !8
  %213 = load float*, float** %ell, align 4, !tbaa !2
  %214 = load i32, i32* %lskip2, align 4, !tbaa !6
  %add112 = add nsw i32 -3, %214
  %arrayidx113 = getelementptr inbounds float, float* %213, i32 %add112
  %215 = load float, float* %arrayidx113, align 4, !tbaa !8
  store float %215, float* %p3, align 4, !tbaa !8
  %216 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds float, float* %216, i32 -3
  %217 = load float, float* %arrayidx114, align 4, !tbaa !8
  %218 = load float, float* %Z41, align 4, !tbaa !8
  %sub115 = fsub float %217, %218
  %219 = load float, float* %p1, align 4, !tbaa !8
  %220 = load float, float* %Z11, align 4, !tbaa !8
  %mul116 = fmul float %219, %220
  %sub117 = fsub float %sub115, %mul116
  %221 = load float, float* %p2, align 4, !tbaa !8
  %222 = load float, float* %Z21, align 4, !tbaa !8
  %mul118 = fmul float %221, %222
  %sub119 = fsub float %sub117, %mul118
  %223 = load float, float* %p3, align 4, !tbaa !8
  %224 = load float, float* %Z31, align 4, !tbaa !8
  %mul120 = fmul float %223, %224
  %sub121 = fsub float %sub119, %mul120
  store float %sub121, float* %Z41, align 4, !tbaa !8
  %225 = load float, float* %Z41, align 4, !tbaa !8
  %226 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds float, float* %226, i32 -3
  store float %225, float* %arrayidx122, align 4, !tbaa !8
  br label %for.inc123

for.inc123:                                       ; preds = %for.end89
  %227 = load i32, i32* %i, align 4, !tbaa !6
  %add124 = add nsw i32 %227, 4
  store i32 %add124, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end125:                                       ; preds = %for.cond
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc175, %for.end125
  %228 = load i32, i32* %i, align 4, !tbaa !6
  %229 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp127 = icmp slt i32 %228, %229
  br i1 %cmp127, label %for.body128, label %for.end176

for.body128:                                      ; preds = %for.cond126
  store float 0.000000e+00, float* %Z11, align 4, !tbaa !8
  %230 = load float*, float** %L.addr, align 4, !tbaa !2
  %231 = load i32, i32* %i, align 4, !tbaa !6
  %idx.neg129 = sub i32 0, %231
  %add.ptr130 = getelementptr inbounds float, float* %230, i32 %idx.neg129
  store float* %add.ptr130, float** %ell, align 4, !tbaa !2
  %232 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %232, float** %ex, align 4, !tbaa !2
  %233 = load i32, i32* %i, align 4, !tbaa !6
  %sub131 = sub nsw i32 %233, 4
  store i32 %sub131, i32* %j, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc156, %for.body128
  %234 = load i32, i32* %j, align 4, !tbaa !6
  %cmp133 = icmp sge i32 %234, 0
  br i1 %cmp133, label %for.body134, label %for.end158

for.body134:                                      ; preds = %for.cond132
  %235 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds float, float* %235, i32 0
  %236 = load float, float* %arrayidx135, align 4, !tbaa !8
  store float %236, float* %p1, align 4, !tbaa !8
  %237 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds float, float* %237, i32 0
  %238 = load float, float* %arrayidx136, align 4, !tbaa !8
  store float %238, float* %q1, align 4, !tbaa !8
  %239 = load float, float* %p1, align 4, !tbaa !8
  %240 = load float, float* %q1, align 4, !tbaa !8
  %mul137 = fmul float %239, %240
  store float %mul137, float* %m11, align 4, !tbaa !8
  %241 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %242 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr138 = getelementptr inbounds float, float* %242, i32 %241
  store float* %add.ptr138, float** %ell, align 4, !tbaa !2
  %243 = load float, float* %m11, align 4, !tbaa !8
  %244 = load float, float* %Z11, align 4, !tbaa !8
  %add139 = fadd float %244, %243
  store float %add139, float* %Z11, align 4, !tbaa !8
  %245 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds float, float* %245, i32 0
  %246 = load float, float* %arrayidx140, align 4, !tbaa !8
  store float %246, float* %p1, align 4, !tbaa !8
  %247 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds float, float* %247, i32 -1
  %248 = load float, float* %arrayidx141, align 4, !tbaa !8
  store float %248, float* %q1, align 4, !tbaa !8
  %249 = load float, float* %p1, align 4, !tbaa !8
  %250 = load float, float* %q1, align 4, !tbaa !8
  %mul142 = fmul float %249, %250
  store float %mul142, float* %m11, align 4, !tbaa !8
  %251 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %252 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr143 = getelementptr inbounds float, float* %252, i32 %251
  store float* %add.ptr143, float** %ell, align 4, !tbaa !2
  %253 = load float, float* %m11, align 4, !tbaa !8
  %254 = load float, float* %Z11, align 4, !tbaa !8
  %add144 = fadd float %254, %253
  store float %add144, float* %Z11, align 4, !tbaa !8
  %255 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds float, float* %255, i32 0
  %256 = load float, float* %arrayidx145, align 4, !tbaa !8
  store float %256, float* %p1, align 4, !tbaa !8
  %257 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds float, float* %257, i32 -2
  %258 = load float, float* %arrayidx146, align 4, !tbaa !8
  store float %258, float* %q1, align 4, !tbaa !8
  %259 = load float, float* %p1, align 4, !tbaa !8
  %260 = load float, float* %q1, align 4, !tbaa !8
  %mul147 = fmul float %259, %260
  store float %mul147, float* %m11, align 4, !tbaa !8
  %261 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %262 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr148 = getelementptr inbounds float, float* %262, i32 %261
  store float* %add.ptr148, float** %ell, align 4, !tbaa !2
  %263 = load float, float* %m11, align 4, !tbaa !8
  %264 = load float, float* %Z11, align 4, !tbaa !8
  %add149 = fadd float %264, %263
  store float %add149, float* %Z11, align 4, !tbaa !8
  %265 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds float, float* %265, i32 0
  %266 = load float, float* %arrayidx150, align 4, !tbaa !8
  store float %266, float* %p1, align 4, !tbaa !8
  %267 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds float, float* %267, i32 -3
  %268 = load float, float* %arrayidx151, align 4, !tbaa !8
  store float %268, float* %q1, align 4, !tbaa !8
  %269 = load float, float* %p1, align 4, !tbaa !8
  %270 = load float, float* %q1, align 4, !tbaa !8
  %mul152 = fmul float %269, %270
  store float %mul152, float* %m11, align 4, !tbaa !8
  %271 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %272 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr153 = getelementptr inbounds float, float* %272, i32 %271
  store float* %add.ptr153, float** %ell, align 4, !tbaa !2
  %273 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr154 = getelementptr inbounds float, float* %273, i32 -4
  store float* %add.ptr154, float** %ex, align 4, !tbaa !2
  %274 = load float, float* %m11, align 4, !tbaa !8
  %275 = load float, float* %Z11, align 4, !tbaa !8
  %add155 = fadd float %275, %274
  store float %add155, float* %Z11, align 4, !tbaa !8
  br label %for.inc156

for.inc156:                                       ; preds = %for.body134
  %276 = load i32, i32* %j, align 4, !tbaa !6
  %sub157 = sub nsw i32 %276, 4
  store i32 %sub157, i32* %j, align 4, !tbaa !6
  br label %for.cond132

for.end158:                                       ; preds = %for.cond132
  %277 = load i32, i32* %j, align 4, !tbaa !6
  %add159 = add nsw i32 %277, 4
  store i32 %add159, i32* %j, align 4, !tbaa !6
  br label %for.cond160

for.cond160:                                      ; preds = %for.inc169, %for.end158
  %278 = load i32, i32* %j, align 4, !tbaa !6
  %cmp161 = icmp sgt i32 %278, 0
  br i1 %cmp161, label %for.body162, label %for.end171

for.body162:                                      ; preds = %for.cond160
  %279 = load float*, float** %ell, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds float, float* %279, i32 0
  %280 = load float, float* %arrayidx163, align 4, !tbaa !8
  store float %280, float* %p1, align 4, !tbaa !8
  %281 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds float, float* %281, i32 0
  %282 = load float, float* %arrayidx164, align 4, !tbaa !8
  store float %282, float* %q1, align 4, !tbaa !8
  %283 = load float, float* %p1, align 4, !tbaa !8
  %284 = load float, float* %q1, align 4, !tbaa !8
  %mul165 = fmul float %283, %284
  store float %mul165, float* %m11, align 4, !tbaa !8
  %285 = load i32, i32* %lskip1.addr, align 4, !tbaa !6
  %286 = load float*, float** %ell, align 4, !tbaa !2
  %add.ptr166 = getelementptr inbounds float, float* %286, i32 %285
  store float* %add.ptr166, float** %ell, align 4, !tbaa !2
  %287 = load float*, float** %ex, align 4, !tbaa !2
  %add.ptr167 = getelementptr inbounds float, float* %287, i32 -1
  store float* %add.ptr167, float** %ex, align 4, !tbaa !2
  %288 = load float, float* %m11, align 4, !tbaa !8
  %289 = load float, float* %Z11, align 4, !tbaa !8
  %add168 = fadd float %289, %288
  store float %add168, float* %Z11, align 4, !tbaa !8
  br label %for.inc169

for.inc169:                                       ; preds = %for.body162
  %290 = load i32, i32* %j, align 4, !tbaa !6
  %dec170 = add nsw i32 %290, -1
  store i32 %dec170, i32* %j, align 4, !tbaa !6
  br label %for.cond160

for.end171:                                       ; preds = %for.cond160
  %291 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds float, float* %291, i32 0
  %292 = load float, float* %arrayidx172, align 4, !tbaa !8
  %293 = load float, float* %Z11, align 4, !tbaa !8
  %sub173 = fsub float %292, %293
  store float %sub173, float* %Z11, align 4, !tbaa !8
  %294 = load float, float* %Z11, align 4, !tbaa !8
  %295 = load float*, float** %ex, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds float, float* %295, i32 0
  store float %294, float* %arrayidx174, align 4, !tbaa !8
  br label %for.inc175

for.inc175:                                       ; preds = %for.end171
  %296 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %296, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond126

for.end176:                                       ; preds = %for.cond126
  %297 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #7
  %298 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #7
  %299 = bitcast i32* %lskip3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #7
  %300 = bitcast i32* %lskip2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #7
  %301 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #7
  %302 = bitcast float** %ex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #7
  %303 = bitcast float* %p4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #7
  %304 = bitcast float* %p3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #7
  %305 = bitcast float* %p2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #7
  %306 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #7
  %307 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %307) #7
  %308 = bitcast float* %m41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #7
  %309 = bitcast float* %Z41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #7
  %310 = bitcast float* %m31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #7
  %311 = bitcast float* %Z31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #7
  %312 = bitcast float* %m21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #7
  %313 = bitcast float* %Z21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #7
  %314 = bitcast float* %m11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #7
  %315 = bitcast float* %Z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z13btVectorScalePfPKfi(float* %a, float* %d, i32 %n) #2 {
entry:
  %a.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %d, float** %d.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %d.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4, !tbaa !8
  %7 = load float*, float** %a.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx1, align 4, !tbaa !8
  %mul = fmul float %9, %6
  store float %mul, float* %arrayidx1, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z11btSolveLDLTPKfS0_Pfii(float* %L, float* %d, float* %b, i32 %n, i32 %nskip) #2 {
entry:
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %d, float** %d.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  %0 = load float*, float** %L.addr, align 4, !tbaa !2
  %1 = load float*, float** %b.addr, align 4, !tbaa !2
  %2 = load i32, i32* %n.addr, align 4, !tbaa !6
  %3 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  call void @_Z9btSolveL1PKfPfii(float* %0, float* %1, i32 %2, i32 %3)
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float*, float** %d.addr, align 4, !tbaa !2
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void @_Z13btVectorScalePfPKfi(float* %4, float* %5, i32 %6)
  %7 = load float*, float** %L.addr, align 4, !tbaa !2
  %8 = load float*, float** %b.addr, align 4, !tbaa !2
  %9 = load i32, i32* %n.addr, align 4, !tbaa !6
  %10 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  call void @_Z10btSolveL1TPKfPfii(float* %7, float* %8, i32 %9, i32 %10)
  ret void
}

define hidden %struct.btLCP* @_ZN5btLCPC2EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_(%struct.btLCP* returned %this, i32 %_n, i32 %_nskip, i32 %_nub, float* %_Adata, float* %_x, float* %_b, float* %_w, float* %_lo, float* %_hi, float* %_L, float* %_d, float* %_Dell, float* %_ell, float* %_tmp, i8* %_state, i32* %_findex, i32* %_p, i32* %_C, float** %Arows) unnamed_addr #0 {
entry:
  %retval = alloca %struct.btLCP*, align 4
  %this.addr = alloca %struct.btLCP*, align 4
  %_n.addr = alloca i32, align 4
  %_nskip.addr = alloca i32, align 4
  %_nub.addr = alloca i32, align 4
  %_Adata.addr = alloca float*, align 4
  %_x.addr = alloca float*, align 4
  %_b.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  %_lo.addr = alloca float*, align 4
  %_hi.addr = alloca float*, align 4
  %_L.addr = alloca float*, align 4
  %_d.addr = alloca float*, align 4
  %_Dell.addr = alloca float*, align 4
  %_ell.addr = alloca float*, align 4
  %_tmp.addr = alloca float*, align 4
  %_state.addr = alloca i8*, align 4
  %_findex.addr = alloca i32*, align 4
  %_p.addr = alloca i32*, align 4
  %_C.addr = alloca i32*, align 4
  %Arows.addr = alloca float**, align 4
  %aptr = alloca float*, align 4
  %A = alloca float**, align 4
  %n = alloca i32, align 4
  %nskip = alloca i32, align 4
  %k = alloca i32, align 4
  %p = alloca i32*, align 4
  %n8 = alloca i32, align 4
  %k10 = alloca i32, align 4
  %findex = alloca i32*, align 4
  %lo = alloca float*, align 4
  %hi = alloca float*, align 4
  %n22 = alloca i32, align 4
  %k24 = alloca i32, align 4
  %nub = alloca i32, align 4
  %Lrow = alloca float*, align 4
  %nskip57 = alloca i32, align 4
  %j = alloca i32, align 4
  %C = alloca i32*, align 4
  %k81 = alloca i32, align 4
  %nub95 = alloca i32, align 4
  %findex97 = alloca i32*, align 4
  %num_at_end = alloca i32, align 4
  %k99 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %_n, i32* %_n.addr, align 4, !tbaa !6
  store i32 %_nskip, i32* %_nskip.addr, align 4, !tbaa !6
  store i32 %_nub, i32* %_nub.addr, align 4, !tbaa !6
  store float* %_Adata, float** %_Adata.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_b, float** %_b.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  store float* %_lo, float** %_lo.addr, align 4, !tbaa !2
  store float* %_hi, float** %_hi.addr, align 4, !tbaa !2
  store float* %_L, float** %_L.addr, align 4, !tbaa !2
  store float* %_d, float** %_d.addr, align 4, !tbaa !2
  store float* %_Dell, float** %_Dell.addr, align 4, !tbaa !2
  store float* %_ell, float** %_ell.addr, align 4, !tbaa !2
  store float* %_tmp, float** %_tmp.addr, align 4, !tbaa !2
  store i8* %_state, i8** %_state.addr, align 4, !tbaa !2
  store i32* %_findex, i32** %_findex.addr, align 4, !tbaa !2
  store i32* %_p, i32** %_p.addr, align 4, !tbaa !2
  store i32* %_C, i32** %_C.addr, align 4, !tbaa !2
  store float** %Arows, float*** %Arows.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  store %struct.btLCP* %this1, %struct.btLCP** %retval, align 4
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %0 = load i32, i32* %_n.addr, align 4, !tbaa !6
  store i32 %0, i32* %m_n, align 4, !tbaa !10
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %1 = load i32, i32* %_nskip.addr, align 4, !tbaa !6
  store i32 %1, i32* %m_nskip, align 4, !tbaa !12
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %2 = load i32, i32* %_nub.addr, align 4, !tbaa !6
  store i32 %2, i32* %m_nub, align 4, !tbaa !13
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 0, i32* %m_nC, align 4, !tbaa !14
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  store i32 0, i32* %m_nN, align 4, !tbaa !15
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %3 = load float**, float*** %Arows.addr, align 4, !tbaa !2
  store float** %3, float*** %m_A, align 4, !tbaa !16
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %4 = load float*, float** %_x.addr, align 4, !tbaa !2
  store float* %4, float** %m_x, align 4, !tbaa !17
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %5 = load float*, float** %_b.addr, align 4, !tbaa !2
  store float* %5, float** %m_b, align 4, !tbaa !18
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  store float* %6, float** %m_w, align 4, !tbaa !19
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %7 = load float*, float** %_lo.addr, align 4, !tbaa !2
  store float* %7, float** %m_lo, align 4, !tbaa !20
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %8 = load float*, float** %_hi.addr, align 4, !tbaa !2
  store float* %8, float** %m_hi, align 4, !tbaa !21
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %9 = load float*, float** %_L.addr, align 4, !tbaa !2
  store float* %9, float** %m_L, align 4, !tbaa !22
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %10 = load float*, float** %_d.addr, align 4, !tbaa !2
  store float* %10, float** %m_d, align 4, !tbaa !23
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %11 = load float*, float** %_Dell.addr, align 4, !tbaa !2
  store float* %11, float** %m_Dell, align 4, !tbaa !24
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %12 = load float*, float** %_ell.addr, align 4, !tbaa !2
  store float* %12, float** %m_ell, align 4, !tbaa !25
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %13 = load float*, float** %_tmp.addr, align 4, !tbaa !2
  store float* %13, float** %m_tmp, align 4, !tbaa !26
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %14 = load i8*, i8** %_state.addr, align 4, !tbaa !2
  store i8* %14, i8** %m_state, align 4, !tbaa !27
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %15 = load i32*, i32** %_findex.addr, align 4, !tbaa !2
  store i32* %15, i32** %m_findex, align 4, !tbaa !28
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %16 = load i32*, i32** %_p.addr, align 4, !tbaa !2
  store i32* %16, i32** %m_p, align 4, !tbaa !29
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %17 = load i32*, i32** %_C.addr, align 4, !tbaa !2
  store i32* %17, i32** %m_C, align 4, !tbaa !30
  %m_x2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %18 = load float*, float** %m_x2, align 4, !tbaa !17
  %m_n3 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %19 = load i32, i32* %m_n3, align 4, !tbaa !10
  call void @_Z9btSetZeroIfEvPT_i(float* %18, i32 %19)
  %20 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %21 = load float*, float** %_Adata.addr, align 4, !tbaa !2
  store float* %21, float** %aptr, align 4, !tbaa !2
  %22 = bitcast float*** %A to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %m_A4 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %23 = load float**, float*** %m_A4, align 4, !tbaa !16
  store float** %23, float*** %A, align 4, !tbaa !2
  %24 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %m_n5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %25 = load i32, i32* %m_n5, align 4, !tbaa !10
  store i32 %25, i32* %n, align 4, !tbaa !6
  %26 = bitcast i32* %nskip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %m_nskip6 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %27 = load i32, i32* %m_nskip6, align 4, !tbaa !12
  store i32 %27, i32* %nskip, align 4, !tbaa !6
  %28 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %29 = load i32, i32* %k, align 4, !tbaa !6
  %30 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %29, %30
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %31 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load float*, float** %aptr, align 4, !tbaa !2
  %33 = load float**, float*** %A, align 4, !tbaa !2
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %33, i32 %34
  store float* %32, float** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %nskip, align 4, !tbaa !6
  %36 = load float*, float** %aptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %36, i32 %35
  store float* %add.ptr, float** %aptr, align 4, !tbaa !2
  %37 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = bitcast i32* %nskip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float*** %A to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast i32** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %m_p7 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %43 = load i32*, i32** %m_p7, align 4, !tbaa !29
  store i32* %43, i32** %p, align 4, !tbaa !2
  %44 = bitcast i32* %n8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %m_n9 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %45 = load i32, i32* %m_n9, align 4, !tbaa !10
  store i32 %45, i32* %n8, align 4, !tbaa !6
  %46 = bitcast i32* %k10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  store i32 0, i32* %k10, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc16, %for.end
  %47 = load i32, i32* %k10, align 4, !tbaa !6
  %48 = load i32, i32* %n8, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %47, %48
  br i1 %cmp12, label %for.body14, label %for.cond.cleanup13

for.cond.cleanup13:                               ; preds = %for.cond11
  %49 = bitcast i32* %k10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %for.end18

for.body14:                                       ; preds = %for.cond11
  %50 = load i32, i32* %k10, align 4, !tbaa !6
  %51 = load i32*, i32** %p, align 4, !tbaa !2
  %52 = load i32, i32* %k10, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds i32, i32* %51, i32 %52
  store i32 %50, i32* %arrayidx15, align 4, !tbaa !6
  br label %for.inc16

for.inc16:                                        ; preds = %for.body14
  %53 = load i32, i32* %k10, align 4, !tbaa !6
  %inc17 = add nsw i32 %53, 1
  store i32 %inc17, i32* %k10, align 4, !tbaa !6
  br label %for.cond11

for.end18:                                        ; preds = %for.cond.cleanup13
  %54 = bitcast i32* %n8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  %55 = bitcast i32** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %56 = bitcast i32** %findex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %m_findex19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %57 = load i32*, i32** %m_findex19, align 4, !tbaa !28
  store i32* %57, i32** %findex, align 4, !tbaa !2
  %58 = bitcast float** %lo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %m_lo20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %59 = load float*, float** %m_lo20, align 4, !tbaa !20
  store float* %59, float** %lo, align 4, !tbaa !2
  %60 = bitcast float** %hi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %m_hi21 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %61 = load float*, float** %m_hi21, align 4, !tbaa !21
  store float* %61, float** %hi, align 4, !tbaa !2
  %62 = bitcast i32* %n22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #7
  %m_n23 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %63 = load i32, i32* %m_n23, align 4, !tbaa !10
  store i32 %63, i32* %n22, align 4, !tbaa !6
  %64 = bitcast i32* %k24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  %m_nub25 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %65 = load i32, i32* %m_nub25, align 4, !tbaa !13
  store i32 %65, i32* %k24, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc49, %for.end18
  %66 = load i32, i32* %k24, align 4, !tbaa !6
  %67 = load i32, i32* %n22, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %66, %67
  br i1 %cmp27, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond26
  %68 = bitcast i32* %k24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #7
  br label %for.end51

for.body29:                                       ; preds = %for.cond26
  %69 = load i32*, i32** %findex, align 4, !tbaa !2
  %tobool = icmp ne i32* %69, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body29
  %70 = load i32*, i32** %findex, align 4, !tbaa !2
  %71 = load i32, i32* %k24, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i32, i32* %70, i32 %71
  %72 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %cmp31 = icmp sge i32 %72, 0
  br i1 %cmp31, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %for.inc49

if.end:                                           ; preds = %land.lhs.true, %for.body29
  %73 = load float*, float** %lo, align 4, !tbaa !2
  %74 = load i32, i32* %k24, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds float, float* %73, i32 %74
  %75 = load float, float* %arrayidx32, align 4, !tbaa !8
  %76 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %fneg = fneg float %76
  %cmp33 = fcmp oeq float %75, %fneg
  br i1 %cmp33, label %land.lhs.true34, label %if.end48

land.lhs.true34:                                  ; preds = %if.end
  %77 = load float*, float** %hi, align 4, !tbaa !2
  %78 = load i32, i32* %k24, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds float, float* %77, i32 %78
  %79 = load float, float* %arrayidx35, align 4, !tbaa !8
  %80 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %cmp36 = fcmp oeq float %79, %80
  br i1 %cmp36, label %if.then37, label %if.end48

if.then37:                                        ; preds = %land.lhs.true34
  %m_A38 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %81 = load float**, float*** %m_A38, align 4, !tbaa !16
  %m_x39 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %82 = load float*, float** %m_x39, align 4, !tbaa !17
  %m_b40 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %83 = load float*, float** %m_b40, align 4, !tbaa !18
  %m_w41 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %84 = load float*, float** %m_w41, align 4, !tbaa !19
  %85 = load float*, float** %lo, align 4, !tbaa !2
  %86 = load float*, float** %hi, align 4, !tbaa !2
  %m_p42 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %87 = load i32*, i32** %m_p42, align 4, !tbaa !29
  %m_state43 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %88 = load i8*, i8** %m_state43, align 4, !tbaa !27
  %89 = load i32*, i32** %findex, align 4, !tbaa !2
  %90 = load i32, i32* %n22, align 4, !tbaa !6
  %m_nub44 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %91 = load i32, i32* %m_nub44, align 4, !tbaa !13
  %92 = load i32, i32* %k24, align 4, !tbaa !6
  %m_nskip45 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %93 = load i32, i32* %m_nskip45, align 4, !tbaa !12
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %81, float* %82, float* %83, float* %84, float* %85, float* %86, i32* %87, i8* %88, i32* %89, i32 %90, i32 %91, i32 %92, i32 %93, i32 0)
  %m_nub46 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %94 = load i32, i32* %m_nub46, align 4, !tbaa !13
  %inc47 = add nsw i32 %94, 1
  store i32 %inc47, i32* %m_nub46, align 4, !tbaa !13
  br label %if.end48

if.end48:                                         ; preds = %if.then37, %land.lhs.true34, %if.end
  br label %for.inc49

for.inc49:                                        ; preds = %if.end48, %if.then
  %95 = load i32, i32* %k24, align 4, !tbaa !6
  %inc50 = add nsw i32 %95, 1
  store i32 %inc50, i32* %k24, align 4, !tbaa !6
  br label %for.cond26

for.end51:                                        ; preds = %for.cond.cleanup28
  %96 = bitcast i32* %n22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  %97 = bitcast float** %hi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  %98 = bitcast float** %lo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast i32** %findex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %m_nub52 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %100 = load i32, i32* %m_nub52, align 4, !tbaa !13
  %cmp53 = icmp sgt i32 %100, 0
  br i1 %cmp53, label %if.then54, label %if.end91

if.then54:                                        ; preds = %for.end51
  %101 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #7
  %m_nub55 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %102 = load i32, i32* %m_nub55, align 4, !tbaa !13
  store i32 %102, i32* %nub, align 4, !tbaa !6
  %103 = bitcast float** %Lrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #7
  %m_L56 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %104 = load float*, float** %m_L56, align 4, !tbaa !22
  store float* %104, float** %Lrow, align 4, !tbaa !2
  %105 = bitcast i32* %nskip57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  %m_nskip58 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %106 = load i32, i32* %m_nskip58, align 4, !tbaa !12
  store i32 %106, i32* %nskip57, align 4, !tbaa !6
  %107 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc65, %if.then54
  %108 = load i32, i32* %j, align 4, !tbaa !6
  %109 = load i32, i32* %nub, align 4, !tbaa !6
  %cmp60 = icmp slt i32 %108, %109
  br i1 %cmp60, label %for.body62, label %for.cond.cleanup61

for.cond.cleanup61:                               ; preds = %for.cond59
  %110 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  br label %for.end68

for.body62:                                       ; preds = %for.cond59
  %111 = load float*, float** %Lrow, align 4, !tbaa !2
  %112 = bitcast float* %111 to i8*
  %m_A63 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %113 = load float**, float*** %m_A63, align 4, !tbaa !16
  %114 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds float*, float** %113, i32 %114
  %115 = load float*, float** %arrayidx64, align 4, !tbaa !2
  %116 = bitcast float* %115 to i8*
  %117 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %117, 1
  %mul = mul i32 %add, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %116, i32 %mul, i1 false)
  br label %for.inc65

for.inc65:                                        ; preds = %for.body62
  %118 = load i32, i32* %nskip57, align 4, !tbaa !6
  %119 = load float*, float** %Lrow, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds float, float* %119, i32 %118
  store float* %add.ptr66, float** %Lrow, align 4, !tbaa !2
  %120 = load i32, i32* %j, align 4, !tbaa !6
  %inc67 = add nsw i32 %120, 1
  store i32 %inc67, i32* %j, align 4, !tbaa !6
  br label %for.cond59

for.end68:                                        ; preds = %for.cond.cleanup61
  %121 = bitcast i32* %nskip57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #7
  %122 = bitcast float** %Lrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #7
  %m_L69 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %123 = load float*, float** %m_L69, align 4, !tbaa !22
  %m_d70 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %124 = load float*, float** %m_d70, align 4, !tbaa !23
  %125 = load i32, i32* %nub, align 4, !tbaa !6
  %m_nskip71 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %126 = load i32, i32* %m_nskip71, align 4, !tbaa !12
  call void @_Z12btFactorLDLTPfS_ii(float* %123, float* %124, i32 %125, i32 %126)
  %m_x72 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %127 = load float*, float** %m_x72, align 4, !tbaa !17
  %128 = bitcast float* %127 to i8*
  %m_b73 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %129 = load float*, float** %m_b73, align 4, !tbaa !18
  %130 = bitcast float* %129 to i8*
  %131 = load i32, i32* %nub, align 4, !tbaa !6
  %mul74 = mul i32 %131, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %128, i8* align 4 %130, i32 %mul74, i1 false)
  %m_L75 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %132 = load float*, float** %m_L75, align 4, !tbaa !22
  %m_d76 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %133 = load float*, float** %m_d76, align 4, !tbaa !23
  %m_x77 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %134 = load float*, float** %m_x77, align 4, !tbaa !17
  %135 = load i32, i32* %nub, align 4, !tbaa !6
  %m_nskip78 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %136 = load i32, i32* %m_nskip78, align 4, !tbaa !12
  call void @_Z11btSolveLDLTPKfS0_Pfii(float* %132, float* %133, float* %134, i32 %135, i32 %136)
  %m_w79 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %137 = load float*, float** %m_w79, align 4, !tbaa !19
  %138 = load i32, i32* %nub, align 4, !tbaa !6
  call void @_Z9btSetZeroIfEvPT_i(float* %137, i32 %138)
  %139 = bitcast i32** %C to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #7
  %m_C80 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %140 = load i32*, i32** %m_C80, align 4, !tbaa !30
  store i32* %140, i32** %C, align 4, !tbaa !2
  %141 = bitcast i32* %k81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #7
  store i32 0, i32* %k81, align 4, !tbaa !6
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc87, %for.end68
  %142 = load i32, i32* %k81, align 4, !tbaa !6
  %143 = load i32, i32* %nub, align 4, !tbaa !6
  %cmp83 = icmp slt i32 %142, %143
  br i1 %cmp83, label %for.body85, label %for.cond.cleanup84

for.cond.cleanup84:                               ; preds = %for.cond82
  %144 = bitcast i32* %k81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  br label %for.end89

for.body85:                                       ; preds = %for.cond82
  %145 = load i32, i32* %k81, align 4, !tbaa !6
  %146 = load i32*, i32** %C, align 4, !tbaa !2
  %147 = load i32, i32* %k81, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i32, i32* %146, i32 %147
  store i32 %145, i32* %arrayidx86, align 4, !tbaa !6
  br label %for.inc87

for.inc87:                                        ; preds = %for.body85
  %148 = load i32, i32* %k81, align 4, !tbaa !6
  %inc88 = add nsw i32 %148, 1
  store i32 %inc88, i32* %k81, align 4, !tbaa !6
  br label %for.cond82

for.end89:                                        ; preds = %for.cond.cleanup84
  %149 = bitcast i32** %C to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #7
  %150 = load i32, i32* %nub, align 4, !tbaa !6
  %m_nC90 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %150, i32* %m_nC90, align 4, !tbaa !14
  %151 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #7
  br label %if.end91

if.end91:                                         ; preds = %for.end89, %for.end51
  %m_findex92 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %152 = load i32*, i32** %m_findex92, align 4, !tbaa !28
  %tobool93 = icmp ne i32* %152, null
  br i1 %tobool93, label %if.then94, label %if.end125

if.then94:                                        ; preds = %if.end91
  %153 = bitcast i32* %nub95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #7
  %m_nub96 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %154 = load i32, i32* %m_nub96, align 4, !tbaa !13
  store i32 %154, i32* %nub95, align 4, !tbaa !6
  %155 = bitcast i32** %findex97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #7
  %m_findex98 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %156 = load i32*, i32** %m_findex98, align 4, !tbaa !28
  store i32* %156, i32** %findex97, align 4, !tbaa !2
  %157 = bitcast i32* %num_at_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #7
  store i32 0, i32* %num_at_end, align 4, !tbaa !6
  %158 = bitcast i32* %k99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #7
  %m_n100 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %159 = load i32, i32* %m_n100, align 4, !tbaa !10
  %sub = sub nsw i32 %159, 1
  store i32 %sub, i32* %k99, align 4, !tbaa !6
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc123, %if.then94
  %160 = load i32, i32* %k99, align 4, !tbaa !6
  %161 = load i32, i32* %nub95, align 4, !tbaa !6
  %cmp102 = icmp sge i32 %160, %161
  br i1 %cmp102, label %for.body104, label %for.cond.cleanup103

for.cond.cleanup103:                              ; preds = %for.cond101
  %162 = bitcast i32* %k99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #7
  br label %for.end124

for.body104:                                      ; preds = %for.cond101
  %163 = load i32*, i32** %findex97, align 4, !tbaa !2
  %164 = load i32, i32* %k99, align 4, !tbaa !6
  %arrayidx105 = getelementptr inbounds i32, i32* %163, i32 %164
  %165 = load i32, i32* %arrayidx105, align 4, !tbaa !6
  %cmp106 = icmp sge i32 %165, 0
  br i1 %cmp106, label %if.then107, label %if.end122

if.then107:                                       ; preds = %for.body104
  %m_A108 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %166 = load float**, float*** %m_A108, align 4, !tbaa !16
  %m_x109 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %167 = load float*, float** %m_x109, align 4, !tbaa !17
  %m_b110 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %168 = load float*, float** %m_b110, align 4, !tbaa !18
  %m_w111 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %169 = load float*, float** %m_w111, align 4, !tbaa !19
  %m_lo112 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %170 = load float*, float** %m_lo112, align 4, !tbaa !20
  %m_hi113 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %171 = load float*, float** %m_hi113, align 4, !tbaa !21
  %m_p114 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %172 = load i32*, i32** %m_p114, align 4, !tbaa !29
  %m_state115 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %173 = load i8*, i8** %m_state115, align 4, !tbaa !27
  %174 = load i32*, i32** %findex97, align 4, !tbaa !2
  %m_n116 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %175 = load i32, i32* %m_n116, align 4, !tbaa !10
  %176 = load i32, i32* %k99, align 4, !tbaa !6
  %m_n117 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %177 = load i32, i32* %m_n117, align 4, !tbaa !10
  %sub118 = sub nsw i32 %177, 1
  %178 = load i32, i32* %num_at_end, align 4, !tbaa !6
  %sub119 = sub nsw i32 %sub118, %178
  %m_nskip120 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %179 = load i32, i32* %m_nskip120, align 4, !tbaa !12
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %166, float* %167, float* %168, float* %169, float* %170, float* %171, i32* %172, i8* %173, i32* %174, i32 %175, i32 %176, i32 %sub119, i32 %179, i32 1)
  %180 = load i32, i32* %num_at_end, align 4, !tbaa !6
  %inc121 = add nsw i32 %180, 1
  store i32 %inc121, i32* %num_at_end, align 4, !tbaa !6
  br label %if.end122

if.end122:                                        ; preds = %if.then107, %for.body104
  br label %for.inc123

for.inc123:                                       ; preds = %if.end122
  %181 = load i32, i32* %k99, align 4, !tbaa !6
  %dec = add nsw i32 %181, -1
  store i32 %dec, i32* %k99, align 4, !tbaa !6
  br label %for.cond101

for.end124:                                       ; preds = %for.cond.cleanup103
  %182 = bitcast i32* %num_at_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #7
  %183 = bitcast i32** %findex97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #7
  %184 = bitcast i32* %nub95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #7
  br label %if.end125

if.end125:                                        ; preds = %for.end124, %if.end91
  %185 = load %struct.btLCP*, %struct.btLCP** %retval, align 4
  ret %struct.btLCP* %185
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z9btSetZeroIfEvPT_i(float* %a, i32 %n) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %acurr = alloca float*, align 4
  %ncurr = alloca i32, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast float** %acurr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float*, float** %a.addr, align 4, !tbaa !2
  store float* %1, float** %acurr, align 4, !tbaa !2
  %2 = bitcast i32* %ncurr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %n.addr, align 4, !tbaa !6
  store i32 %3, i32* %ncurr, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %ncurr, align 4, !tbaa !31
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load float*, float** %acurr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds float, float* %5, i32 1
  store float* %incdec.ptr, float** %acurr, align 4, !tbaa !2
  store float 0.000000e+00, float* %5, align 4, !tbaa !8
  %6 = load i32, i32* %ncurr, align 4, !tbaa !31
  %dec = add i32 %6, -1
  store i32 %dec, i32* %ncurr, align 4, !tbaa !31
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = bitcast i32* %ncurr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  %8 = bitcast float** %acurr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

define internal void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %A, float* %x, float* %b, float* %w, float* %lo, float* %hi, i32* %p, i8* %state, i32* %findex, i32 %n, i32 %i1, i32 %i2, i32 %nskip, i32 %do_fast_row_swaps) #0 {
entry:
  %A.addr = alloca float**, align 4
  %x.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %w.addr = alloca float*, align 4
  %lo.addr = alloca float*, align 4
  %hi.addr = alloca float*, align 4
  %p.addr = alloca i32*, align 4
  %state.addr = alloca i8*, align 4
  %findex.addr = alloca i32*, align 4
  %n.addr = alloca i32, align 4
  %i1.addr = alloca i32, align 4
  %i2.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %do_fast_row_swaps.addr = alloca i32, align 4
  %tmpr = alloca float, align 4
  %tmpi = alloca i32, align 4
  %tmpb = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store float** %A, float*** %A.addr, align 4, !tbaa !2
  store float* %x, float** %x.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  store float* %w, float** %w.addr, align 4, !tbaa !2
  store float* %lo, float** %lo.addr, align 4, !tbaa !2
  store float* %hi, float** %hi.addr, align 4, !tbaa !2
  store i32* %p, i32** %p.addr, align 4, !tbaa !2
  store i8* %state, i8** %state.addr, align 4, !tbaa !2
  store i32* %findex, i32** %findex.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %i1, i32* %i1.addr, align 4, !tbaa !6
  store i32 %i2, i32* %i2.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  store i32 %do_fast_row_swaps, i32* %do_fast_row_swaps.addr, align 4, !tbaa !6
  %0 = bitcast float* %tmpr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i32* %tmpi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tmpb) #7
  %2 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %3 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load float**, float*** %A.addr, align 4, !tbaa !2
  %5 = load i32, i32* %n.addr, align 4, !tbaa !6
  %6 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %7 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %8 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %9 = load i32, i32* %do_fast_row_swaps.addr, align 4, !tbaa !6
  call void @_ZL17btSwapRowsAndColsPPfiiiii(float** %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9)
  %10 = load float*, float** %x.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %10, i32 %11
  %12 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %12, float* %tmpr, align 4, !tbaa !8
  %13 = load float*, float** %x.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds float, float* %13, i32 %14
  %15 = load float, float* %arrayidx1, align 4, !tbaa !8
  %16 = load float*, float** %x.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %16, i32 %17
  store float %15, float* %arrayidx2, align 4, !tbaa !8
  %18 = load float, float* %tmpr, align 4, !tbaa !8
  %19 = load float*, float** %x.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %19, i32 %20
  store float %18, float* %arrayidx3, align 4, !tbaa !8
  %21 = load float*, float** %b.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %21, i32 %22
  %23 = load float, float* %arrayidx4, align 4, !tbaa !8
  store float %23, float* %tmpr, align 4, !tbaa !8
  %24 = load float*, float** %b.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx5, align 4, !tbaa !8
  %27 = load float*, float** %b.addr, align 4, !tbaa !2
  %28 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %27, i32 %28
  store float %26, float* %arrayidx6, align 4, !tbaa !8
  %29 = load float, float* %tmpr, align 4, !tbaa !8
  %30 = load float*, float** %b.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds float, float* %30, i32 %31
  store float %29, float* %arrayidx7, align 4, !tbaa !8
  %32 = load float*, float** %w.addr, align 4, !tbaa !2
  %33 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx8, align 4, !tbaa !8
  store float %34, float* %tmpr, align 4, !tbaa !8
  %35 = load float*, float** %w.addr, align 4, !tbaa !2
  %36 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds float, float* %35, i32 %36
  %37 = load float, float* %arrayidx9, align 4, !tbaa !8
  %38 = load float*, float** %w.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %38, i32 %39
  store float %37, float* %arrayidx10, align 4, !tbaa !8
  %40 = load float, float* %tmpr, align 4, !tbaa !8
  %41 = load float*, float** %w.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds float, float* %41, i32 %42
  store float %40, float* %arrayidx11, align 4, !tbaa !8
  %43 = load float*, float** %lo.addr, align 4, !tbaa !2
  %44 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %43, i32 %44
  %45 = load float, float* %arrayidx12, align 4, !tbaa !8
  store float %45, float* %tmpr, align 4, !tbaa !8
  %46 = load float*, float** %lo.addr, align 4, !tbaa !2
  %47 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %46, i32 %47
  %48 = load float, float* %arrayidx13, align 4, !tbaa !8
  %49 = load float*, float** %lo.addr, align 4, !tbaa !2
  %50 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds float, float* %49, i32 %50
  store float %48, float* %arrayidx14, align 4, !tbaa !8
  %51 = load float, float* %tmpr, align 4, !tbaa !8
  %52 = load float*, float** %lo.addr, align 4, !tbaa !2
  %53 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds float, float* %52, i32 %53
  store float %51, float* %arrayidx15, align 4, !tbaa !8
  %54 = load float*, float** %hi.addr, align 4, !tbaa !2
  %55 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx16, align 4, !tbaa !8
  store float %56, float* %tmpr, align 4, !tbaa !8
  %57 = load float*, float** %hi.addr, align 4, !tbaa !2
  %58 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds float, float* %57, i32 %58
  %59 = load float, float* %arrayidx17, align 4, !tbaa !8
  %60 = load float*, float** %hi.addr, align 4, !tbaa !2
  %61 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds float, float* %60, i32 %61
  store float %59, float* %arrayidx18, align 4, !tbaa !8
  %62 = load float, float* %tmpr, align 4, !tbaa !8
  %63 = load float*, float** %hi.addr, align 4, !tbaa !2
  %64 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds float, float* %63, i32 %64
  store float %62, float* %arrayidx19, align 4, !tbaa !8
  %65 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %66 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  store i32 %67, i32* %tmpi, align 4, !tbaa !6
  %68 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %69 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %71 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %72 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i32, i32* %71, i32 %72
  store i32 %70, i32* %arrayidx22, align 4, !tbaa !6
  %73 = load i32, i32* %tmpi, align 4, !tbaa !6
  %74 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %75 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i32, i32* %74, i32 %75
  store i32 %73, i32* %arrayidx23, align 4, !tbaa !6
  %76 = load i8*, i8** %state.addr, align 4, !tbaa !2
  %77 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds i8, i8* %76, i32 %77
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !33, !range !35
  %tobool = trunc i8 %78 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %tmpb, align 1, !tbaa !33
  %79 = load i8*, i8** %state.addr, align 4, !tbaa !2
  %80 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 %80
  %81 = load i8, i8* %arrayidx25, align 1, !tbaa !33, !range !35
  %tobool26 = trunc i8 %81 to i1
  %82 = load i8*, i8** %state.addr, align 4, !tbaa !2
  %83 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 %83
  %frombool28 = zext i1 %tobool26 to i8
  store i8 %frombool28, i8* %arrayidx27, align 1, !tbaa !33
  %84 = load i8, i8* %tmpb, align 1, !tbaa !33, !range !35
  %tobool29 = trunc i8 %84 to i1
  %85 = load i8*, i8** %state.addr, align 4, !tbaa !2
  %86 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i8, i8* %85, i32 %86
  %frombool31 = zext i1 %tobool29 to i8
  store i8 %frombool31, i8* %arrayidx30, align 1, !tbaa !33
  %87 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %tobool32 = icmp ne i32* %87, null
  br i1 %tobool32, label %if.then33, label %if.end38

if.then33:                                        ; preds = %if.end
  %88 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %89 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds i32, i32* %88, i32 %89
  %90 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  store i32 %90, i32* %tmpi, align 4, !tbaa !6
  %91 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %92 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i32, i32* %91, i32 %92
  %93 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %94 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %95 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds i32, i32* %94, i32 %95
  store i32 %93, i32* %arrayidx36, align 4, !tbaa !6
  %96 = load i32, i32* %tmpi, align 4, !tbaa !6
  %97 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %98 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds i32, i32* %97, i32 %98
  store i32 %96, i32* %arrayidx37, align 4, !tbaa !6
  br label %if.end38

if.end38:                                         ; preds = %if.then33, %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end38, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tmpb) #7
  %99 = bitcast i32* %tmpi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  %100 = bitcast float* %tmpr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

define hidden void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %nC = alloca i32, align 4
  %Ltgt = alloca float*, align 4
  %ell = alloca float*, align 4
  %j = alloca i32, align 4
  %nC5 = alloca i32, align 4
  %nC20 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4, !tbaa !14
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %m_nC2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_nC2, align 4, !tbaa !14
  store i32 %2, i32* %nC, align 4, !tbaa !6
  %3 = bitcast float** %Ltgt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %4 = load float*, float** %m_L, align 4, !tbaa !22
  %5 = load i32, i32* %nC, align 4, !tbaa !6
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_nskip, align 4, !tbaa !12
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds float, float* %4, i32 %mul
  store float* %add.ptr, float** %Ltgt, align 4, !tbaa !2
  %7 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %8 = load float*, float** %m_ell, align 4, !tbaa !25
  store float* %8, float** %ell, align 4, !tbaa !2
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %11 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %10, %11
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load float*, float** %ell, align 4, !tbaa !2
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %13, i32 %14
  %15 = load float, float* %arrayidx, align 4, !tbaa !8
  %16 = load float*, float** %Ltgt, align 4, !tbaa !2
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %16, i32 %17
  store float %15, float* %arrayidx4, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %19 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast float** %Ltgt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast i32* %nC5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %m_nC6 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %23 = load i32, i32* %m_nC6, align 4, !tbaa !14
  store i32 %23, i32* %nC5, align 4, !tbaa !6
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %24 = load float**, float*** %m_A, align 4, !tbaa !16
  %25 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds float*, float** %24, i32 %25
  %26 = load float*, float** %arrayidx7, align 4, !tbaa !2
  %27 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %26, i32 %27
  %28 = load float, float* %arrayidx8, align 4, !tbaa !8
  %m_ell9 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %29 = load float*, float** %m_ell9, align 4, !tbaa !25
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %30 = load float*, float** %m_Dell, align 4, !tbaa !24
  %31 = load i32, i32* %nC5, align 4, !tbaa !6
  %call = call float @_Z10btLargeDotPKfS0_i(float* %29, float* %30, i32 %31)
  %sub = fsub float %28, %call
  %div = fdiv float 1.000000e+00, %sub
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %32 = load float*, float** %m_d, align 4, !tbaa !23
  %33 = load i32, i32* %nC5, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %32, i32 %33
  store float %div, float* %arrayidx10, align 4, !tbaa !8
  %34 = bitcast i32* %nC5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %m_A11 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %35 = load float**, float*** %m_A11, align 4, !tbaa !16
  %36 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float*, float** %35, i32 %36
  %37 = load float*, float** %arrayidx12, align 4, !tbaa !2
  %38 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %37, i32 %38
  %39 = load float, float* %arrayidx13, align 4, !tbaa !8
  %div14 = fdiv float 1.000000e+00, %39
  %m_d15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %40 = load float*, float** %m_d15, align 4, !tbaa !23
  %arrayidx16 = getelementptr inbounds float, float* %40, i32 0
  store float %div14, float* %arrayidx16, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  %m_A17 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %41 = load float**, float*** %m_A17, align 4, !tbaa !16
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %42 = load float*, float** %m_x, align 4, !tbaa !17
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %43 = load float*, float** %m_b, align 4, !tbaa !18
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %44 = load float*, float** %m_w, align 4, !tbaa !19
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %45 = load float*, float** %m_lo, align 4, !tbaa !20
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %46 = load float*, float** %m_hi, align 4, !tbaa !21
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %47 = load i32*, i32** %m_p, align 4, !tbaa !29
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %48 = load i8*, i8** %m_state, align 4, !tbaa !27
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %49 = load i32*, i32** %m_findex, align 4, !tbaa !28
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %50 = load i32, i32* %m_n, align 4, !tbaa !10
  %m_nC18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %51 = load i32, i32* %m_nC18, align 4, !tbaa !14
  %52 = load i32, i32* %i.addr, align 4, !tbaa !6
  %m_nskip19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %53 = load i32, i32* %m_nskip19, align 4, !tbaa !12
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %41, float* %42, float* %43, float* %44, float* %45, float* %46, i32* %47, i8* %48, i32* %49, i32 %50, i32 %51, i32 %52, i32 %53, i32 1)
  %54 = bitcast i32* %nC20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %m_nC21 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %55 = load i32, i32* %m_nC21, align 4, !tbaa !14
  store i32 %55, i32* %nC20, align 4, !tbaa !6
  %56 = load i32, i32* %nC20, align 4, !tbaa !6
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %57 = load i32*, i32** %m_C, align 4, !tbaa !30
  %58 = load i32, i32* %nC20, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i32, i32* %57, i32 %58
  store i32 %56, i32* %arrayidx22, align 4, !tbaa !6
  %59 = load i32, i32* %nC20, align 4, !tbaa !6
  %add = add nsw i32 %59, 1
  %m_nC23 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %add, i32* %m_nC23, align 4, !tbaa !14
  %60 = bitcast i32* %nC20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z10btLargeDotPKfS0_i(float* %a, float* %b, i32 %n) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %p0 = alloca float, align 4
  %q0 = alloca float, align 4
  %m0 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %m1 = alloca float, align 4
  %sum = alloca float, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = bitcast float* %p0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast float* %q0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = bitcast float* %m0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float* %m1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  store float 0.000000e+00, float* %sum, align 4, !tbaa !8
  %7 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %7, 2
  store i32 %sub, i32* %n.addr, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %8, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %9, i32 0
  %10 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %10, float* %p0, align 4, !tbaa !8
  %11 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %11, i32 0
  %12 = load float, float* %arrayidx1, align 4, !tbaa !8
  store float %12, float* %q0, align 4, !tbaa !8
  %13 = load float, float* %p0, align 4, !tbaa !8
  %14 = load float, float* %q0, align 4, !tbaa !8
  %mul = fmul float %13, %14
  store float %mul, float* %m0, align 4, !tbaa !8
  %15 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds float, float* %15, i32 1
  %16 = load float, float* %arrayidx2, align 4, !tbaa !8
  store float %16, float* %p1, align 4, !tbaa !8
  %17 = load float*, float** %b.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds float, float* %17, i32 1
  %18 = load float, float* %arrayidx3, align 4, !tbaa !8
  store float %18, float* %q1, align 4, !tbaa !8
  %19 = load float, float* %p1, align 4, !tbaa !8
  %20 = load float, float* %q1, align 4, !tbaa !8
  %mul4 = fmul float %19, %20
  store float %mul4, float* %m1, align 4, !tbaa !8
  %21 = load float, float* %m0, align 4, !tbaa !8
  %22 = load float, float* %sum, align 4, !tbaa !8
  %add = fadd float %22, %21
  store float %add, float* %sum, align 4, !tbaa !8
  %23 = load float, float* %m1, align 4, !tbaa !8
  %24 = load float, float* %sum, align 4, !tbaa !8
  %add5 = fadd float %24, %23
  store float %add5, float* %sum, align 4, !tbaa !8
  %25 = load float*, float** %a.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %25, i32 2
  store float* %add.ptr, float** %a.addr, align 4, !tbaa !2
  %26 = load float*, float** %b.addr, align 4, !tbaa !2
  %add.ptr6 = getelementptr inbounds float, float* %26, i32 2
  store float* %add.ptr6, float** %b.addr, align 4, !tbaa !2
  %27 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub7 = sub nsw i32 %27, 2
  store i32 %sub7, i32* %n.addr, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i32, i32* %n.addr, align 4, !tbaa !6
  %add8 = add nsw i32 %28, 2
  store i32 %add8, i32* %n.addr, align 4, !tbaa !6
  br label %while.cond9

while.cond9:                                      ; preds = %while.body11, %while.end
  %29 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp10 = icmp sgt i32 %29, 0
  br i1 %cmp10, label %while.body11, label %while.end15

while.body11:                                     ; preds = %while.cond9
  %30 = load float*, float** %a.addr, align 4, !tbaa !2
  %31 = load float, float* %30, align 4, !tbaa !8
  %32 = load float*, float** %b.addr, align 4, !tbaa !2
  %33 = load float, float* %32, align 4, !tbaa !8
  %mul12 = fmul float %31, %33
  %34 = load float, float* %sum, align 4, !tbaa !8
  %add13 = fadd float %34, %mul12
  store float %add13, float* %sum, align 4, !tbaa !8
  %35 = load float*, float** %a.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds float, float* %35, i32 1
  store float* %incdec.ptr, float** %a.addr, align 4, !tbaa !2
  %36 = load float*, float** %b.addr, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds float, float* %36, i32 1
  store float* %incdec.ptr14, float** %b.addr, align 4, !tbaa !2
  %37 = load i32, i32* %n.addr, align 4, !tbaa !6
  %dec = add nsw i32 %37, -1
  store i32 %dec, i32* %n.addr, align 4, !tbaa !6
  br label %while.cond9

while.end15:                                      ; preds = %while.cond9
  %38 = load float, float* %sum, align 4, !tbaa !8
  %39 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float* %m1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast float* %q1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = bitcast float* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = bitcast float* %m0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast float* %q0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = bitcast float* %p0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  ret float %38
}

define hidden void @_ZN5btLCP22transfer_i_from_N_to_CEi(%struct.btLCP* %this, i32 %i) #0 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %aptr = alloca float*, align 4
  %Dell = alloca float*, align 4
  %C = alloca i32*, align 4
  %nub = alloca i32, align 4
  %j = alloca i32, align 4
  %nC = alloca i32, align 4
  %nC17 = alloca i32, align 4
  %Ltgt = alloca float*, align 4
  %ell = alloca float*, align 4
  %Dell21 = alloca float*, align 4
  %d = alloca float*, align 4
  %j23 = alloca i32, align 4
  %nC35 = alloca i32, align 4
  %nC53 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4, !tbaa !14
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %2 = load float**, float*** %m_A, align 4, !tbaa !16
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %2, i32 %3
  %4 = load float*, float** %arrayidx, align 4, !tbaa !2
  store float* %4, float** %aptr, align 4, !tbaa !2
  %5 = bitcast float** %Dell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %6 = load float*, float** %m_Dell, align 4, !tbaa !24
  store float* %6, float** %Dell, align 4, !tbaa !2
  %7 = bitcast i32** %C to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %8 = load i32*, i32** %m_C, align 4, !tbaa !30
  store i32* %8, i32** %C, align 4, !tbaa !2
  %9 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %10 = load i32, i32* %m_nub, align 4, !tbaa !13
  store i32 %10, i32* %nub, align 4, !tbaa !6
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %13 = load i32, i32* %nub, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %12, %13
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load float*, float** %aptr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx3, align 4, !tbaa !8
  %17 = load float*, float** %Dell, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %17, i32 %18
  store float %16, float* %arrayidx4, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %m_nC5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %21 = load i32, i32* %m_nC5, align 4, !tbaa !14
  store i32 %21, i32* %nC, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %for.end
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %23 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %22, %23
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %24 = load float*, float** %aptr, align 4, !tbaa !2
  %25 = load i32*, i32** %C, align 4, !tbaa !2
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i32, i32* %25, i32 %26
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %24, i32 %27
  %28 = load float, float* %arrayidx10, align 4, !tbaa !8
  %29 = load float*, float** %Dell, align 4, !tbaa !2
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds float, float* %29, i32 %30
  store float %28, float* %arrayidx11, align 4, !tbaa !8
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %inc13 = add nsw i32 %31, 1
  store i32 %inc13, i32* %j, align 4, !tbaa !6
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  %32 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i32** %C to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast float** %Dell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %38 = load float*, float** %m_L, align 4, !tbaa !22
  %m_Dell15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %39 = load float*, float** %m_Dell15, align 4, !tbaa !24
  %m_nC16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %40 = load i32, i32* %m_nC16, align 4, !tbaa !14
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %41 = load i32, i32* %m_nskip, align 4, !tbaa !12
  call void @_Z9btSolveL1PKfPfii(float* %38, float* %39, i32 %40, i32 %41)
  %42 = bitcast i32* %nC17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %m_nC18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %43 = load i32, i32* %m_nC18, align 4, !tbaa !14
  store i32 %43, i32* %nC17, align 4, !tbaa !6
  %44 = bitcast float** %Ltgt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %m_L19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %45 = load float*, float** %m_L19, align 4, !tbaa !22
  %46 = load i32, i32* %nC17, align 4, !tbaa !6
  %m_nskip20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %47 = load i32, i32* %m_nskip20, align 4, !tbaa !12
  %mul = mul nsw i32 %46, %47
  %add.ptr = getelementptr inbounds float, float* %45, i32 %mul
  store float* %add.ptr, float** %Ltgt, align 4, !tbaa !2
  %48 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %49 = load float*, float** %m_ell, align 4, !tbaa !25
  store float* %49, float** %ell, align 4, !tbaa !2
  %50 = bitcast float** %Dell21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %m_Dell22 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %51 = load float*, float** %m_Dell22, align 4, !tbaa !24
  store float* %51, float** %Dell21, align 4, !tbaa !2
  %52 = bitcast float** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #7
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %53 = load float*, float** %m_d, align 4, !tbaa !23
  store float* %53, float** %d, align 4, !tbaa !2
  %54 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  store i32 0, i32* %j23, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc32, %for.end14
  %55 = load i32, i32* %j23, align 4, !tbaa !6
  %56 = load i32, i32* %nC17, align 4, !tbaa !6
  %cmp25 = icmp slt i32 %55, %56
  br i1 %cmp25, label %for.body26, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond24
  %57 = bitcast i32* %j23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  br label %for.end34

for.body26:                                       ; preds = %for.cond24
  %58 = load float*, float** %Dell21, align 4, !tbaa !2
  %59 = load i32, i32* %j23, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx27, align 4, !tbaa !8
  %61 = load float*, float** %d, align 4, !tbaa !2
  %62 = load i32, i32* %j23, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds float, float* %61, i32 %62
  %63 = load float, float* %arrayidx28, align 4, !tbaa !8
  %mul29 = fmul float %60, %63
  %64 = load float*, float** %ell, align 4, !tbaa !2
  %65 = load i32, i32* %j23, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds float, float* %64, i32 %65
  store float %mul29, float* %arrayidx30, align 4, !tbaa !8
  %66 = load float*, float** %Ltgt, align 4, !tbaa !2
  %67 = load i32, i32* %j23, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds float, float* %66, i32 %67
  store float %mul29, float* %arrayidx31, align 4, !tbaa !8
  br label %for.inc32

for.inc32:                                        ; preds = %for.body26
  %68 = load i32, i32* %j23, align 4, !tbaa !6
  %inc33 = add nsw i32 %68, 1
  store i32 %inc33, i32* %j23, align 4, !tbaa !6
  br label %for.cond24

for.end34:                                        ; preds = %for.cond.cleanup
  %69 = bitcast float** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %70 = bitcast float** %Dell21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast float** %Ltgt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  %73 = bitcast i32* %nC17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast i32* %nC35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #7
  %m_nC36 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %75 = load i32, i32* %m_nC36, align 4, !tbaa !14
  store i32 %75, i32* %nC35, align 4, !tbaa !6
  %m_A37 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %76 = load float**, float*** %m_A37, align 4, !tbaa !16
  %77 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds float*, float** %76, i32 %77
  %78 = load float*, float** %arrayidx38, align 4, !tbaa !2
  %79 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx39, align 4, !tbaa !8
  %m_ell40 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %81 = load float*, float** %m_ell40, align 4, !tbaa !25
  %m_Dell41 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %82 = load float*, float** %m_Dell41, align 4, !tbaa !24
  %83 = load i32, i32* %nC35, align 4, !tbaa !6
  %call = call float @_Z10btLargeDotPKfS0_i(float* %81, float* %82, i32 %83)
  %sub = fsub float %80, %call
  %div = fdiv float 1.000000e+00, %sub
  %m_d42 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %84 = load float*, float** %m_d42, align 4, !tbaa !23
  %85 = load i32, i32* %nC35, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds float, float* %84, i32 %85
  store float %div, float* %arrayidx43, align 4, !tbaa !8
  %86 = bitcast i32* %nC35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %m_A44 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %87 = load float**, float*** %m_A44, align 4, !tbaa !16
  %88 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds float*, float** %87, i32 %88
  %89 = load float*, float** %arrayidx45, align 4, !tbaa !2
  %90 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds float, float* %89, i32 %90
  %91 = load float, float* %arrayidx46, align 4, !tbaa !8
  %div47 = fdiv float 1.000000e+00, %91
  %m_d48 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %92 = load float*, float** %m_d48, align 4, !tbaa !23
  %arrayidx49 = getelementptr inbounds float, float* %92, i32 0
  store float %div47, float* %arrayidx49, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end34
  %m_A50 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %93 = load float**, float*** %m_A50, align 4, !tbaa !16
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %94 = load float*, float** %m_x, align 4, !tbaa !17
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %95 = load float*, float** %m_b, align 4, !tbaa !18
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %96 = load float*, float** %m_w, align 4, !tbaa !19
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %97 = load float*, float** %m_lo, align 4, !tbaa !20
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %98 = load float*, float** %m_hi, align 4, !tbaa !21
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %99 = load i32*, i32** %m_p, align 4, !tbaa !29
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %100 = load i8*, i8** %m_state, align 4, !tbaa !27
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %101 = load i32*, i32** %m_findex, align 4, !tbaa !28
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %102 = load i32, i32* %m_n, align 4, !tbaa !10
  %m_nC51 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %103 = load i32, i32* %m_nC51, align 4, !tbaa !14
  %104 = load i32, i32* %i.addr, align 4, !tbaa !6
  %m_nskip52 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %105 = load i32, i32* %m_nskip52, align 4, !tbaa !12
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %93, float* %94, float* %95, float* %96, float* %97, float* %98, i32* %99, i8* %100, i32* %101, i32 %102, i32 %103, i32 %104, i32 %105, i32 1)
  %106 = bitcast i32* %nC53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #7
  %m_nC54 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %107 = load i32, i32* %m_nC54, align 4, !tbaa !14
  store i32 %107, i32* %nC53, align 4, !tbaa !6
  %108 = load i32, i32* %nC53, align 4, !tbaa !6
  %m_C55 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %109 = load i32*, i32** %m_C55, align 4, !tbaa !30
  %110 = load i32, i32* %nC53, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds i32, i32* %109, i32 %110
  store i32 %108, i32* %arrayidx56, align 4, !tbaa !6
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %111 = load i32, i32* %m_nN, align 4, !tbaa !15
  %dec = add nsw i32 %111, -1
  store i32 %dec, i32* %m_nN, align 4, !tbaa !15
  %112 = load i32, i32* %nC53, align 4, !tbaa !6
  %add = add nsw i32 %112, 1
  %m_nC57 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %add, i32* %m_nC57, align 4, !tbaa !14
  %113 = bitcast i32* %nC53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z14btRemoveRowColPfiii(float* %A, i32 %n, i32 %nskip, i32 %r) #2 {
entry:
  %A.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %move_size = alloca i32, align 4
  %Adst = alloca float*, align 4
  %i = alloca i32, align 4
  %Asrc = alloca float*, align 4
  %cpy_size = alloca i32, align 4
  %Adst9 = alloca float*, align 4
  %i12 = alloca i32, align 4
  %Asrc18 = alloca float*, align 4
  %cpy_size24 = alloca i32, align 4
  %Adst28 = alloca float*, align 4
  %i31 = alloca i32, align 4
  %Asrc37 = alloca float*, align 4
  store float* %A, float** %A.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  %0 = load i32, i32* %r.addr, align 4, !tbaa !6
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, 1
  %cmp = icmp sge i32 %0, %sub
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end23

if.then2:                                         ; preds = %if.end
  %3 = bitcast i32* %move_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %n.addr, align 4, !tbaa !6
  %5 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub3 = sub nsw i32 %4, %5
  %sub4 = sub nsw i32 %sub3, 1
  %mul = mul i32 %sub4, 4
  store i32 %mul, i32* %move_size, align 4, !tbaa !31
  %6 = bitcast float** %Adst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load float*, float** %A.addr, align 4, !tbaa !2
  %8 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %7, i32 %8
  store float* %add.ptr, float** %Adst, align 4, !tbaa !2
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %10, %11
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast float** %Asrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load float*, float** %Adst, align 4, !tbaa !2
  %add.ptr6 = getelementptr inbounds float, float* %14, i32 1
  store float* %add.ptr6, float** %Asrc, align 4, !tbaa !2
  %15 = load float*, float** %Adst, align 4, !tbaa !2
  %16 = bitcast float* %15 to i8*
  %17 = load float*, float** %Asrc, align 4, !tbaa !2
  %18 = bitcast float* %17 to i8*
  %19 = load i32, i32* %move_size, align 4, !tbaa !31
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %18, i32 %19, i1 false)
  %20 = bitcast float** %Asrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %22 = load float*, float** %Adst, align 4, !tbaa !2
  %add.ptr7 = getelementptr inbounds float, float* %22, i32 %21
  store float* %add.ptr7, float** %Adst, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast float** %Adst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast i32* %move_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast i32* %cpy_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load i32, i32* %r.addr, align 4, !tbaa !6
  %mul8 = mul i32 %27, 4
  store i32 %mul8, i32* %cpy_size, align 4, !tbaa !31
  %28 = bitcast float** %Adst9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = load float*, float** %A.addr, align 4, !tbaa !2
  %30 = load i32, i32* %r.addr, align 4, !tbaa !6
  %31 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul10 = mul nsw i32 %30, %31
  %add.ptr11 = getelementptr inbounds float, float* %29, i32 %mul10
  store float* %add.ptr11, float** %Adst9, align 4, !tbaa !2
  %32 = bitcast i32* %i12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load i32, i32* %r.addr, align 4, !tbaa !6
  store i32 %33, i32* %i12, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc20, %for.end
  %34 = load i32, i32* %i12, align 4, !tbaa !6
  %35 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub14 = sub nsw i32 %35, 1
  %cmp15 = icmp slt i32 %34, %sub14
  br i1 %cmp15, label %for.body17, label %for.cond.cleanup16

for.cond.cleanup16:                               ; preds = %for.cond13
  %36 = bitcast i32* %i12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  br label %for.end22

for.body17:                                       ; preds = %for.cond13
  %37 = bitcast float** %Asrc18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #7
  %38 = load float*, float** %Adst9, align 4, !tbaa !2
  %39 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add.ptr19 = getelementptr inbounds float, float* %38, i32 %39
  store float* %add.ptr19, float** %Asrc18, align 4, !tbaa !2
  %40 = load float*, float** %Adst9, align 4, !tbaa !2
  %41 = bitcast float* %40 to i8*
  %42 = load float*, float** %Asrc18, align 4, !tbaa !2
  %43 = bitcast float* %42 to i8*
  %44 = load i32, i32* %cpy_size, align 4, !tbaa !31
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %43, i32 %44, i1 false)
  %45 = load float*, float** %Asrc18, align 4, !tbaa !2
  store float* %45, float** %Adst9, align 4, !tbaa !2
  %46 = bitcast float** %Asrc18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  br label %for.inc20

for.inc20:                                        ; preds = %for.body17
  %47 = load i32, i32* %i12, align 4, !tbaa !6
  %inc21 = add nsw i32 %47, 1
  store i32 %inc21, i32* %i12, align 4, !tbaa !6
  br label %for.cond13

for.end22:                                        ; preds = %for.cond.cleanup16
  %48 = bitcast float** %Adst9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = bitcast i32* %cpy_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #7
  br label %if.end23

if.end23:                                         ; preds = %for.end22, %if.end
  %50 = bitcast i32* %cpy_size24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  %51 = load i32, i32* %n.addr, align 4, !tbaa !6
  %52 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub25 = sub nsw i32 %51, %52
  %sub26 = sub nsw i32 %sub25, 1
  %mul27 = mul i32 %sub26, 4
  store i32 %mul27, i32* %cpy_size24, align 4, !tbaa !31
  %53 = bitcast float** %Adst28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #7
  %54 = load float*, float** %A.addr, align 4, !tbaa !2
  %55 = load i32, i32* %r.addr, align 4, !tbaa !6
  %56 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add = add nsw i32 %56, 1
  %mul29 = mul nsw i32 %55, %add
  %add.ptr30 = getelementptr inbounds float, float* %54, i32 %mul29
  store float* %add.ptr30, float** %Adst28, align 4, !tbaa !2
  %57 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #7
  %58 = load i32, i32* %r.addr, align 4, !tbaa !6
  store i32 %58, i32* %i31, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc41, %if.end23
  %59 = load i32, i32* %i31, align 4, !tbaa !6
  %60 = load i32, i32* %n.addr, align 4, !tbaa !6
  %sub33 = sub nsw i32 %60, 1
  %cmp34 = icmp slt i32 %59, %sub33
  br i1 %cmp34, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond32
  %61 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  br label %for.end43

for.body36:                                       ; preds = %for.cond32
  %62 = bitcast float** %Asrc37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #7
  %63 = load float*, float** %Adst28, align 4, !tbaa !2
  %64 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add38 = add nsw i32 %64, 1
  %add.ptr39 = getelementptr inbounds float, float* %63, i32 %add38
  store float* %add.ptr39, float** %Asrc37, align 4, !tbaa !2
  %65 = load float*, float** %Adst28, align 4, !tbaa !2
  %66 = bitcast float* %65 to i8*
  %67 = load float*, float** %Asrc37, align 4, !tbaa !2
  %68 = bitcast float* %67 to i8*
  %69 = load i32, i32* %cpy_size24, align 4, !tbaa !31
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %66, i8* align 4 %68, i32 %69, i1 false)
  %70 = load float*, float** %Asrc37, align 4, !tbaa !2
  %add.ptr40 = getelementptr inbounds float, float* %70, i32 -1
  store float* %add.ptr40, float** %Adst28, align 4, !tbaa !2
  %71 = bitcast float** %Asrc37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  br label %for.inc41

for.inc41:                                        ; preds = %for.body36
  %72 = load i32, i32* %i31, align 4, !tbaa !6
  %inc42 = add nsw i32 %72, 1
  store i32 %inc42, i32* %i31, align 4, !tbaa !6
  br label %for.cond32

for.end43:                                        ; preds = %for.cond.cleanup35
  %73 = bitcast float** %Adst28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #7
  %74 = bitcast i32* %cpy_size24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #7
  br label %return

return:                                           ; preds = %for.end43, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

define hidden void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %L, float* %d, float* %a, i32 %n, i32 %nskip, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #0 {
entry:
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %ref.tmp = alloca float, align 4
  %W1 = alloca float*, align 4
  %W2 = alloca float*, align 4
  %j = alloca i32, align 4
  %W11 = alloca float, align 4
  %W21 = alloca float, align 4
  %alpha1 = alloca float, align 4
  %alpha2 = alloca float, align 4
  %dee = alloca float, align 4
  %alphanew = alloca float, align 4
  %gamma1 = alloca float, align 4
  %k1 = alloca float, align 4
  %k2 = alloca float, align 4
  %ll = alloca float*, align 4
  %p = alloca i32, align 4
  %Wp = alloca float, align 4
  %ell = alloca float, align 4
  %ll45 = alloca float*, align 4
  %j48 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %k153 = alloca float, align 4
  %k255 = alloca float, align 4
  %dee57 = alloca float, align 4
  %alphanew59 = alloca float, align 4
  %gamma164 = alloca float, align 4
  %gamma2 = alloca float, align 4
  %l = alloca float*, align 4
  %p75 = alloca i32, align 4
  %ell81 = alloca float, align 4
  %Wp82 = alloca float, align 4
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %d, float** %d.addr, align 4, !tbaa !2
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %2 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul = mul nsw i32 2, %2
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %1, i32 %mul, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  %5 = bitcast float** %W1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %6, i32 0)
  store float* %call, float** %W1, align 4, !tbaa !2
  %7 = bitcast float** %W2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load float*, float** %W1, align 4, !tbaa !2
  %9 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %8, i32 %9
  store float* %add.ptr, float** %W2, align 4, !tbaa !2
  %10 = load float*, float** %W1, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %10, i32 0
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %11 = load float*, float** %W2, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds float, float* %11, i32 0
  store float 0.000000e+00, float* %arrayidx1, align 4, !tbaa !8
  %12 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  store i32 1, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %13 = load i32, i32* %j, align 4, !tbaa !6
  %14 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load float*, float** %a.addr, align 4, !tbaa !2
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %16, i32 %17
  %18 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %18, 0x3FE6A09E60000000
  %19 = load float*, float** %W2, align 4, !tbaa !2
  %20 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds float, float* %19, i32 %20
  store float %mul4, float* %arrayidx5, align 4, !tbaa !8
  %21 = load float*, float** %W1, align 4, !tbaa !2
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %21, i32 %22
  store float %mul4, float* %arrayidx6, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast float* %W11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %25, i32 0
  %26 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float 5.000000e-01, %26
  %add = fadd float %mul8, 1.000000e+00
  %mul9 = fmul float %add, 0x3FE6A09E60000000
  store float %mul9, float* %W11, align 4, !tbaa !8
  %27 = bitcast float* %W21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = load float*, float** %a.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %28, i32 0
  %29 = load float, float* %arrayidx10, align 4, !tbaa !8
  %mul11 = fmul float 5.000000e-01, %29
  %sub = fsub float %mul11, 1.000000e+00
  %mul12 = fmul float %sub, 0x3FE6A09E60000000
  store float %mul12, float* %W21, align 4, !tbaa !8
  %30 = bitcast float* %alpha1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  store float 1.000000e+00, float* %alpha1, align 4, !tbaa !8
  %31 = bitcast float* %alpha2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  store float 1.000000e+00, float* %alpha2, align 4, !tbaa !8
  %32 = bitcast float* %dee to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load float*, float** %d.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %33, i32 0
  %34 = load float, float* %arrayidx13, align 4, !tbaa !8
  store float %34, float* %dee, align 4, !tbaa !8
  %35 = bitcast float* %alphanew to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load float, float* %alpha1, align 4, !tbaa !8
  %37 = load float, float* %W11, align 4, !tbaa !8
  %38 = load float, float* %W11, align 4, !tbaa !8
  %mul14 = fmul float %37, %38
  %39 = load float, float* %dee, align 4, !tbaa !8
  %mul15 = fmul float %mul14, %39
  %add16 = fadd float %36, %mul15
  store float %add16, float* %alphanew, align 4, !tbaa !8
  %40 = load float, float* %alphanew, align 4, !tbaa !8
  %41 = load float, float* %dee, align 4, !tbaa !8
  %div = fdiv float %41, %40
  store float %div, float* %dee, align 4, !tbaa !8
  %42 = bitcast float* %gamma1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %43 = load float, float* %W11, align 4, !tbaa !8
  %44 = load float, float* %dee, align 4, !tbaa !8
  %mul17 = fmul float %43, %44
  store float %mul17, float* %gamma1, align 4, !tbaa !8
  %45 = load float, float* %alpha1, align 4, !tbaa !8
  %46 = load float, float* %dee, align 4, !tbaa !8
  %mul18 = fmul float %46, %45
  store float %mul18, float* %dee, align 4, !tbaa !8
  %47 = load float, float* %alphanew, align 4, !tbaa !8
  store float %47, float* %alpha1, align 4, !tbaa !8
  %48 = load float, float* %alpha2, align 4, !tbaa !8
  %49 = load float, float* %W21, align 4, !tbaa !8
  %50 = load float, float* %W21, align 4, !tbaa !8
  %mul19 = fmul float %49, %50
  %51 = load float, float* %dee, align 4, !tbaa !8
  %mul20 = fmul float %mul19, %51
  %sub21 = fsub float %48, %mul20
  store float %sub21, float* %alphanew, align 4, !tbaa !8
  %52 = load float, float* %alphanew, align 4, !tbaa !8
  %53 = load float, float* %dee, align 4, !tbaa !8
  %div22 = fdiv float %53, %52
  store float %div22, float* %dee, align 4, !tbaa !8
  %54 = load float, float* %alphanew, align 4, !tbaa !8
  store float %54, float* %alpha2, align 4, !tbaa !8
  %55 = bitcast float* %k1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  %56 = load float, float* %W21, align 4, !tbaa !8
  %57 = load float, float* %gamma1, align 4, !tbaa !8
  %mul23 = fmul float %56, %57
  %sub24 = fsub float 1.000000e+00, %mul23
  store float %sub24, float* %k1, align 4, !tbaa !8
  %58 = bitcast float* %k2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #7
  %59 = load float, float* %W21, align 4, !tbaa !8
  %60 = load float, float* %gamma1, align 4, !tbaa !8
  %mul25 = fmul float %59, %60
  %61 = load float, float* %W11, align 4, !tbaa !8
  %mul26 = fmul float %mul25, %61
  %62 = load float, float* %W21, align 4, !tbaa !8
  %sub27 = fsub float %mul26, %62
  store float %sub27, float* %k2, align 4, !tbaa !8
  %63 = bitcast float** %ll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #7
  %64 = load float*, float** %L.addr, align 4, !tbaa !2
  %65 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add.ptr28 = getelementptr inbounds float, float* %64, i32 %65
  store float* %add.ptr28, float** %ll, align 4, !tbaa !2
  %66 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  store i32 1, i32* %p, align 4, !tbaa !6
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc41, %for.end
  %67 = load i32, i32* %p, align 4, !tbaa !6
  %68 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp30 = icmp slt i32 %67, %68
  br i1 %cmp30, label %for.body32, label %for.cond.cleanup31

for.cond.cleanup31:                               ; preds = %for.cond29
  %69 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  br label %for.end44

for.body32:                                       ; preds = %for.cond29
  %70 = bitcast float* %Wp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #7
  %71 = load float*, float** %W1, align 4, !tbaa !2
  %72 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds float, float* %71, i32 %72
  %73 = load float, float* %arrayidx33, align 4, !tbaa !8
  store float %73, float* %Wp, align 4, !tbaa !8
  %74 = bitcast float* %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #7
  %75 = load float*, float** %ll, align 4, !tbaa !2
  %76 = load float, float* %75, align 4, !tbaa !8
  store float %76, float* %ell, align 4, !tbaa !8
  %77 = load float, float* %Wp, align 4, !tbaa !8
  %78 = load float, float* %W11, align 4, !tbaa !8
  %79 = load float, float* %ell, align 4, !tbaa !8
  %mul34 = fmul float %78, %79
  %sub35 = fsub float %77, %mul34
  %80 = load float*, float** %W1, align 4, !tbaa !2
  %81 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds float, float* %80, i32 %81
  store float %sub35, float* %arrayidx36, align 4, !tbaa !8
  %82 = load float, float* %k1, align 4, !tbaa !8
  %83 = load float, float* %Wp, align 4, !tbaa !8
  %mul37 = fmul float %82, %83
  %84 = load float, float* %k2, align 4, !tbaa !8
  %85 = load float, float* %ell, align 4, !tbaa !8
  %mul38 = fmul float %84, %85
  %add39 = fadd float %mul37, %mul38
  %86 = load float*, float** %W2, align 4, !tbaa !2
  %87 = load i32, i32* %p, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds float, float* %86, i32 %87
  store float %add39, float* %arrayidx40, align 4, !tbaa !8
  %88 = bitcast float* %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #7
  %89 = bitcast float* %Wp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  br label %for.inc41

for.inc41:                                        ; preds = %for.body32
  %90 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %91 = load float*, float** %ll, align 4, !tbaa !2
  %add.ptr42 = getelementptr inbounds float, float* %91, i32 %90
  store float* %add.ptr42, float** %ll, align 4, !tbaa !2
  %92 = load i32, i32* %p, align 4, !tbaa !6
  %inc43 = add nsw i32 %92, 1
  store i32 %inc43, i32* %p, align 4, !tbaa !6
  br label %for.cond29

for.end44:                                        ; preds = %for.cond.cleanup31
  %93 = bitcast float** %ll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  %94 = bitcast float* %k2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #7
  %95 = bitcast float* %k1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #7
  %96 = bitcast float* %gamma1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  %97 = bitcast float* %alphanew to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  %98 = bitcast float* %dee to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #7
  %99 = bitcast float** %ll45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #7
  %100 = load float*, float** %L.addr, align 4, !tbaa !2
  %101 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add46 = add nsw i32 %101, 1
  %add.ptr47 = getelementptr inbounds float, float* %100, i32 %add46
  store float* %add.ptr47, float** %ll45, align 4, !tbaa !2
  %102 = bitcast i32* %j48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #7
  store i32 1, i32* %j48, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc99, %for.end44
  %103 = load i32, i32* %j48, align 4, !tbaa !6
  %104 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %103, %104
  br i1 %cmp50, label %for.body52, label %for.cond.cleanup51

for.cond.cleanup51:                               ; preds = %for.cond49
  store i32 8, i32* %cleanup.dest.slot, align 4
  %105 = bitcast i32* %j48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #7
  br label %for.end103

for.body52:                                       ; preds = %for.cond49
  %106 = bitcast float* %k153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #7
  %107 = load float*, float** %W1, align 4, !tbaa !2
  %108 = load i32, i32* %j48, align 4, !tbaa !6
  %arrayidx54 = getelementptr inbounds float, float* %107, i32 %108
  %109 = load float, float* %arrayidx54, align 4, !tbaa !8
  store float %109, float* %k153, align 4, !tbaa !8
  %110 = bitcast float* %k255 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #7
  %111 = load float*, float** %W2, align 4, !tbaa !2
  %112 = load i32, i32* %j48, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds float, float* %111, i32 %112
  %113 = load float, float* %arrayidx56, align 4, !tbaa !8
  store float %113, float* %k255, align 4, !tbaa !8
  %114 = bitcast float* %dee57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #7
  %115 = load float*, float** %d.addr, align 4, !tbaa !2
  %116 = load i32, i32* %j48, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds float, float* %115, i32 %116
  %117 = load float, float* %arrayidx58, align 4, !tbaa !8
  store float %117, float* %dee57, align 4, !tbaa !8
  %118 = bitcast float* %alphanew59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #7
  %119 = load float, float* %alpha1, align 4, !tbaa !8
  %120 = load float, float* %k153, align 4, !tbaa !8
  %121 = load float, float* %k153, align 4, !tbaa !8
  %mul60 = fmul float %120, %121
  %122 = load float, float* %dee57, align 4, !tbaa !8
  %mul61 = fmul float %mul60, %122
  %add62 = fadd float %119, %mul61
  store float %add62, float* %alphanew59, align 4, !tbaa !8
  %123 = load float, float* %alphanew59, align 4, !tbaa !8
  %124 = load float, float* %dee57, align 4, !tbaa !8
  %div63 = fdiv float %124, %123
  store float %div63, float* %dee57, align 4, !tbaa !8
  %125 = bitcast float* %gamma164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #7
  %126 = load float, float* %k153, align 4, !tbaa !8
  %127 = load float, float* %dee57, align 4, !tbaa !8
  %mul65 = fmul float %126, %127
  store float %mul65, float* %gamma164, align 4, !tbaa !8
  %128 = load float, float* %alpha1, align 4, !tbaa !8
  %129 = load float, float* %dee57, align 4, !tbaa !8
  %mul66 = fmul float %129, %128
  store float %mul66, float* %dee57, align 4, !tbaa !8
  %130 = load float, float* %alphanew59, align 4, !tbaa !8
  store float %130, float* %alpha1, align 4, !tbaa !8
  %131 = load float, float* %alpha2, align 4, !tbaa !8
  %132 = load float, float* %k255, align 4, !tbaa !8
  %133 = load float, float* %k255, align 4, !tbaa !8
  %mul67 = fmul float %132, %133
  %134 = load float, float* %dee57, align 4, !tbaa !8
  %mul68 = fmul float %mul67, %134
  %sub69 = fsub float %131, %mul68
  store float %sub69, float* %alphanew59, align 4, !tbaa !8
  %135 = load float, float* %alphanew59, align 4, !tbaa !8
  %136 = load float, float* %dee57, align 4, !tbaa !8
  %div70 = fdiv float %136, %135
  store float %div70, float* %dee57, align 4, !tbaa !8
  %137 = bitcast float* %gamma2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  %138 = load float, float* %k255, align 4, !tbaa !8
  %139 = load float, float* %dee57, align 4, !tbaa !8
  %mul71 = fmul float %138, %139
  store float %mul71, float* %gamma2, align 4, !tbaa !8
  %140 = load float, float* %alpha2, align 4, !tbaa !8
  %141 = load float, float* %dee57, align 4, !tbaa !8
  %mul72 = fmul float %141, %140
  store float %mul72, float* %dee57, align 4, !tbaa !8
  %142 = load float, float* %dee57, align 4, !tbaa !8
  %143 = load float*, float** %d.addr, align 4, !tbaa !2
  %144 = load i32, i32* %j48, align 4, !tbaa !6
  %arrayidx73 = getelementptr inbounds float, float* %143, i32 %144
  store float %142, float* %arrayidx73, align 4, !tbaa !8
  %145 = load float, float* %alphanew59, align 4, !tbaa !8
  store float %145, float* %alpha2, align 4, !tbaa !8
  %146 = bitcast float** %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #7
  %147 = load float*, float** %ll45, align 4, !tbaa !2
  %148 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add.ptr74 = getelementptr inbounds float, float* %147, i32 %148
  store float* %add.ptr74, float** %l, align 4, !tbaa !2
  %149 = bitcast i32* %p75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #7
  %150 = load i32, i32* %j48, align 4, !tbaa !6
  %add76 = add nsw i32 %150, 1
  store i32 %add76, i32* %p75, align 4, !tbaa !6
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc95, %for.body52
  %151 = load i32, i32* %p75, align 4, !tbaa !6
  %152 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp78 = icmp slt i32 %151, %152
  br i1 %cmp78, label %for.body80, label %for.cond.cleanup79

for.cond.cleanup79:                               ; preds = %for.cond77
  store i32 11, i32* %cleanup.dest.slot, align 4
  %153 = bitcast i32* %p75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #7
  br label %for.end98

for.body80:                                       ; preds = %for.cond77
  %154 = bitcast float* %ell81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #7
  %155 = load float*, float** %l, align 4, !tbaa !2
  %156 = load float, float* %155, align 4, !tbaa !8
  store float %156, float* %ell81, align 4, !tbaa !8
  %157 = bitcast float* %Wp82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #7
  %158 = load float*, float** %W1, align 4, !tbaa !2
  %159 = load i32, i32* %p75, align 4, !tbaa !6
  %arrayidx83 = getelementptr inbounds float, float* %158, i32 %159
  %160 = load float, float* %arrayidx83, align 4, !tbaa !8
  %161 = load float, float* %k153, align 4, !tbaa !8
  %162 = load float, float* %ell81, align 4, !tbaa !8
  %mul84 = fmul float %161, %162
  %sub85 = fsub float %160, %mul84
  store float %sub85, float* %Wp82, align 4, !tbaa !8
  %163 = load float, float* %gamma164, align 4, !tbaa !8
  %164 = load float, float* %Wp82, align 4, !tbaa !8
  %mul86 = fmul float %163, %164
  %165 = load float, float* %ell81, align 4, !tbaa !8
  %add87 = fadd float %165, %mul86
  store float %add87, float* %ell81, align 4, !tbaa !8
  %166 = load float, float* %Wp82, align 4, !tbaa !8
  %167 = load float*, float** %W1, align 4, !tbaa !2
  %168 = load i32, i32* %p75, align 4, !tbaa !6
  %arrayidx88 = getelementptr inbounds float, float* %167, i32 %168
  store float %166, float* %arrayidx88, align 4, !tbaa !8
  %169 = load float*, float** %W2, align 4, !tbaa !2
  %170 = load i32, i32* %p75, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds float, float* %169, i32 %170
  %171 = load float, float* %arrayidx89, align 4, !tbaa !8
  %172 = load float, float* %k255, align 4, !tbaa !8
  %173 = load float, float* %ell81, align 4, !tbaa !8
  %mul90 = fmul float %172, %173
  %sub91 = fsub float %171, %mul90
  store float %sub91, float* %Wp82, align 4, !tbaa !8
  %174 = load float, float* %gamma2, align 4, !tbaa !8
  %175 = load float, float* %Wp82, align 4, !tbaa !8
  %mul92 = fmul float %174, %175
  %176 = load float, float* %ell81, align 4, !tbaa !8
  %sub93 = fsub float %176, %mul92
  store float %sub93, float* %ell81, align 4, !tbaa !8
  %177 = load float, float* %Wp82, align 4, !tbaa !8
  %178 = load float*, float** %W2, align 4, !tbaa !2
  %179 = load i32, i32* %p75, align 4, !tbaa !6
  %arrayidx94 = getelementptr inbounds float, float* %178, i32 %179
  store float %177, float* %arrayidx94, align 4, !tbaa !8
  %180 = load float, float* %ell81, align 4, !tbaa !8
  %181 = load float*, float** %l, align 4, !tbaa !2
  store float %180, float* %181, align 4, !tbaa !8
  %182 = bitcast float* %Wp82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #7
  %183 = bitcast float* %ell81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #7
  br label %for.inc95

for.inc95:                                        ; preds = %for.body80
  %184 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %185 = load float*, float** %l, align 4, !tbaa !2
  %add.ptr96 = getelementptr inbounds float, float* %185, i32 %184
  store float* %add.ptr96, float** %l, align 4, !tbaa !2
  %186 = load i32, i32* %p75, align 4, !tbaa !6
  %inc97 = add nsw i32 %186, 1
  store i32 %inc97, i32* %p75, align 4, !tbaa !6
  br label %for.cond77

for.end98:                                        ; preds = %for.cond.cleanup79
  %187 = bitcast float** %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #7
  %188 = bitcast float* %gamma2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #7
  %189 = bitcast float* %gamma164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #7
  %190 = bitcast float* %alphanew59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #7
  %191 = bitcast float* %dee57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #7
  %192 = bitcast float* %k255 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #7
  %193 = bitcast float* %k153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #7
  br label %for.inc99

for.inc99:                                        ; preds = %for.end98
  %194 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %add100 = add nsw i32 %194, 1
  %195 = load float*, float** %ll45, align 4, !tbaa !2
  %add.ptr101 = getelementptr inbounds float, float* %195, i32 %add100
  store float* %add.ptr101, float** %ll45, align 4, !tbaa !2
  %196 = load i32, i32* %j48, align 4, !tbaa !6
  %inc102 = add nsw i32 %196, 1
  store i32 %inc102, i32* %j48, align 4, !tbaa !6
  br label %for.cond49

for.end103:                                       ; preds = %for.cond.cleanup51
  %197 = bitcast float** %ll45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #7
  %198 = bitcast float* %alpha2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #7
  %199 = bitcast float* %alpha1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #7
  %200 = bitcast float* %W21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #7
  %201 = bitcast float* %W11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #7
  %202 = bitcast float** %W2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #7
  %203 = bitcast float** %W1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #7
  br label %return

return:                                           ; preds = %for.end103, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !36
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !8
  store float %23, float* %21, align 4, !tbaa !8
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !39
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

define hidden void @_Z12btLDLTRemovePPfPKiS_S_iiiiR20btAlignedObjectArrayIfE(float** %A, i32* %p, float* %L, float* %d, i32 %n1, i32 %n2, i32 %r, i32 %nskip, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #0 {
entry:
  %A.addr = alloca float**, align 4
  %p.addr = alloca i32*, align 4
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n1.addr = alloca i32, align 4
  %n2.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %LDLTAddTL_size = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %tmp = alloca float*, align 4
  %a = alloca float*, align 4
  %p_0 = alloca i32, align 4
  %i = alloca i32, align 4
  %t = alloca float*, align 4
  %Lcurr = alloca float*, align 4
  %i20 = alloca i32, align 4
  %a30 = alloca float*, align 4
  %Lcurr32 = alloca float*, align 4
  %pp_r = alloca i32*, align 4
  %p_r = alloca i32, align 4
  %n2_minus_r = alloca i32, align 4
  %i37 = alloca i32, align 4
  store float** %A, float*** %A.addr, align 4, !tbaa !2
  store i32* %p, i32** %p.addr, align 4, !tbaa !2
  store float* %L, float** %L.addr, align 4, !tbaa !2
  store float* %d, float** %d.addr, align 4, !tbaa !2
  store i32 %n1, i32* %n1.addr, align 4, !tbaa !6
  store i32 %n2, i32* %n2.addr, align 4, !tbaa !6
  store i32 %r, i32* %r.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %0 = load i32, i32* %r.addr, align 4, !tbaa !6
  %1 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %1, 1
  %cmp = icmp eq i32 %0, %sub
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end78

if.else:                                          ; preds = %entry
  %2 = bitcast i32* %LDLTAddTL_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %call = call i32 @_Z29btEstimateLDLTAddTLTmpbufSizei(i32 %3)
  store i32 %call, i32* %LDLTAddTL_size, align 4, !tbaa !31
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %5 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %5, 2
  %6 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %6
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %4, i32 %add, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  %9 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %10, i32 0)
  store float* %call1, float** %tmp, align 4, !tbaa !2
  %11 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %11, 0
  br i1 %cmp2, label %if.then3, label %if.else16

if.then3:                                         ; preds = %if.else
  %12 = bitcast float** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load float*, float** %tmp, align 4, !tbaa !2
  %14 = bitcast float* %13 to i8*
  %15 = load i32, i32* %LDLTAddTL_size, align 4, !tbaa !31
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = bitcast i8* %add.ptr to float*
  store float* %16, float** %a, align 4, !tbaa !2
  %17 = bitcast i32* %p_0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 0
  %19 = load i32, i32* %arrayidx, align 4, !tbaa !6
  store i32 %19, i32* %p_0, align 4, !tbaa !6
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %22 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %21, %22
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i32, i32* %24, i32 %25
  %26 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %27 = load i32, i32* %p_0, align 4, !tbaa !6
  %cmp6 = icmp sgt i32 %26, %27
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %28 = load float**, float*** %A.addr, align 4, !tbaa !2
  %29 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i32, i32* %29, i32 %30
  %31 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float*, float** %28, i32 %31
  %32 = load float*, float** %arrayidx8, align 4, !tbaa !2
  %33 = load i32, i32* %p_0, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx9, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %35 = load float**, float*** %A.addr, align 4, !tbaa !2
  %36 = load i32, i32* %p_0, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float*, float** %35, i32 %36
  %37 = load float*, float** %arrayidx10, align 4, !tbaa !2
  %38 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i32, i32* %38, i32 %39
  %40 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float, float* %37, i32 %40
  %41 = load float, float* %arrayidx12, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %34, %cond.true ], [ %41, %cond.false ]
  %fneg = fneg float %cond
  %42 = load float*, float** %a, align 4, !tbaa !2
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %42, i32 %43
  store float %fneg, float* %arrayidx13, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %45 = load float*, float** %a, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %45, i32 0
  %46 = load float, float* %arrayidx14, align 4, !tbaa !8
  %add15 = fadd float %46, 1.000000e+00
  store float %add15, float* %arrayidx14, align 4, !tbaa !8
  %47 = load float*, float** %L.addr, align 4, !tbaa !2
  %48 = load float*, float** %d.addr, align 4, !tbaa !2
  %49 = load float*, float** %a, align 4, !tbaa !2
  %50 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %51 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %52 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  call void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %47, float* %48, float* %49, i32 %50, i32 %51, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %52)
  %53 = bitcast i32* %p_0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  %54 = bitcast float** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  br label %if.end

if.else16:                                        ; preds = %if.else
  %55 = bitcast float** %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  %56 = load float*, float** %tmp, align 4, !tbaa !2
  %57 = bitcast float* %56 to i8*
  %58 = load i32, i32* %LDLTAddTL_size, align 4, !tbaa !31
  %add.ptr17 = getelementptr inbounds i8, i8* %57, i32 %58
  %59 = bitcast i8* %add.ptr17 to float*
  store float* %59, float** %t, align 4, !tbaa !2
  %60 = bitcast float** %Lcurr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #7
  %61 = load float*, float** %L.addr, align 4, !tbaa !2
  %62 = load i32, i32* %r.addr, align 4, !tbaa !6
  %63 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul18 = mul nsw i32 %62, %63
  %add.ptr19 = getelementptr inbounds float, float* %61, i32 %mul18
  store float* %add.ptr19, float** %Lcurr, align 4, !tbaa !2
  %64 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #7
  store i32 0, i32* %i20, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc27, %if.else16
  %65 = load i32, i32* %i20, align 4, !tbaa !6
  %66 = load i32, i32* %r.addr, align 4, !tbaa !6
  %cmp22 = icmp slt i32 %65, %66
  br i1 %cmp22, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond21
  %67 = bitcast i32* %i20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #7
  br label %for.end29

for.body24:                                       ; preds = %for.cond21
  %68 = load float*, float** %Lcurr, align 4, !tbaa !2
  %69 = load float, float* %68, align 4, !tbaa !8
  %70 = load float*, float** %d.addr, align 4, !tbaa !2
  %71 = load i32, i32* %i20, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds float, float* %70, i32 %71
  %72 = load float, float* %arrayidx25, align 4, !tbaa !8
  %div = fdiv float %69, %72
  %73 = load float*, float** %t, align 4, !tbaa !2
  %74 = load i32, i32* %i20, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds float, float* %73, i32 %74
  store float %div, float* %arrayidx26, align 4, !tbaa !8
  br label %for.inc27

for.inc27:                                        ; preds = %for.body24
  %75 = load float*, float** %Lcurr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds float, float* %75, i32 1
  store float* %incdec.ptr, float** %Lcurr, align 4, !tbaa !2
  %76 = load i32, i32* %i20, align 4, !tbaa !6
  %inc28 = add nsw i32 %76, 1
  store i32 %inc28, i32* %i20, align 4, !tbaa !6
  br label %for.cond21

for.end29:                                        ; preds = %for.cond.cleanup23
  %77 = bitcast float** %Lcurr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast float** %a30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #7
  %79 = load float*, float** %t, align 4, !tbaa !2
  %80 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr31 = getelementptr inbounds float, float* %79, i32 %80
  store float* %add.ptr31, float** %a30, align 4, !tbaa !2
  %81 = bitcast float** %Lcurr32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #7
  %82 = load float*, float** %L.addr, align 4, !tbaa !2
  %83 = load i32, i32* %r.addr, align 4, !tbaa !6
  %84 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul33 = mul nsw i32 %83, %84
  %add.ptr34 = getelementptr inbounds float, float* %82, i32 %mul33
  store float* %add.ptr34, float** %Lcurr32, align 4, !tbaa !2
  %85 = bitcast i32** %pp_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #7
  %86 = load i32*, i32** %p.addr, align 4, !tbaa !2
  %87 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr35 = getelementptr inbounds i32, i32* %86, i32 %87
  store i32* %add.ptr35, i32** %pp_r, align 4, !tbaa !2
  %88 = bitcast i32* %p_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #7
  %89 = load i32*, i32** %pp_r, align 4, !tbaa !2
  %90 = load i32, i32* %89, align 4, !tbaa !6
  store i32 %90, i32* %p_r, align 4, !tbaa !6
  %91 = bitcast i32* %n2_minus_r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #7
  %92 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %93 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub36 = sub nsw i32 %92, %93
  store i32 %sub36, i32* %n2_minus_r, align 4, !tbaa !6
  %94 = bitcast i32* %i37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #7
  store i32 0, i32* %i37, align 4, !tbaa !6
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc57, %for.end29
  %95 = load i32, i32* %i37, align 4, !tbaa !6
  %96 = load i32, i32* %n2_minus_r, align 4, !tbaa !6
  %cmp39 = icmp slt i32 %95, %96
  br i1 %cmp39, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond38
  %97 = bitcast i32* %i37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #7
  br label %for.end60

for.body41:                                       ; preds = %for.cond38
  %98 = load float*, float** %Lcurr32, align 4, !tbaa !2
  %99 = load float*, float** %t, align 4, !tbaa !2
  %100 = load i32, i32* %r.addr, align 4, !tbaa !6
  %call42 = call float @_Z10btLargeDotPKfS0_i(float* %98, float* %99, i32 %100)
  %101 = load i32*, i32** %pp_r, align 4, !tbaa !2
  %102 = load i32, i32* %i37, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds i32, i32* %101, i32 %102
  %103 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %104 = load i32, i32* %p_r, align 4, !tbaa !6
  %cmp44 = icmp sgt i32 %103, %104
  br i1 %cmp44, label %cond.true45, label %cond.false49

cond.true45:                                      ; preds = %for.body41
  %105 = load float**, float*** %A.addr, align 4, !tbaa !2
  %106 = load i32*, i32** %pp_r, align 4, !tbaa !2
  %107 = load i32, i32* %i37, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx46, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds float*, float** %105, i32 %108
  %109 = load float*, float** %arrayidx47, align 4, !tbaa !2
  %110 = load i32, i32* %p_r, align 4, !tbaa !6
  %arrayidx48 = getelementptr inbounds float, float* %109, i32 %110
  %111 = load float, float* %arrayidx48, align 4, !tbaa !8
  br label %cond.end53

cond.false49:                                     ; preds = %for.body41
  %112 = load float**, float*** %A.addr, align 4, !tbaa !2
  %113 = load i32, i32* %p_r, align 4, !tbaa !6
  %arrayidx50 = getelementptr inbounds float*, float** %112, i32 %113
  %114 = load float*, float** %arrayidx50, align 4, !tbaa !2
  %115 = load i32*, i32** %pp_r, align 4, !tbaa !2
  %116 = load i32, i32* %i37, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds i32, i32* %115, i32 %116
  %117 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds float, float* %114, i32 %117
  %118 = load float, float* %arrayidx52, align 4, !tbaa !8
  br label %cond.end53

cond.end53:                                       ; preds = %cond.false49, %cond.true45
  %cond54 = phi float [ %111, %cond.true45 ], [ %118, %cond.false49 ]
  %sub55 = fsub float %call42, %cond54
  %119 = load float*, float** %a30, align 4, !tbaa !2
  %120 = load i32, i32* %i37, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds float, float* %119, i32 %120
  store float %sub55, float* %arrayidx56, align 4, !tbaa !8
  br label %for.inc57

for.inc57:                                        ; preds = %cond.end53
  %121 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %122 = load float*, float** %Lcurr32, align 4, !tbaa !2
  %add.ptr58 = getelementptr inbounds float, float* %122, i32 %121
  store float* %add.ptr58, float** %Lcurr32, align 4, !tbaa !2
  %123 = load i32, i32* %i37, align 4, !tbaa !6
  %inc59 = add nsw i32 %123, 1
  store i32 %inc59, i32* %i37, align 4, !tbaa !6
  br label %for.cond38

for.end60:                                        ; preds = %for.cond.cleanup40
  %124 = bitcast i32* %n2_minus_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #7
  %125 = bitcast i32* %p_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #7
  %126 = bitcast i32** %pp_r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #7
  %127 = bitcast float** %Lcurr32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #7
  %128 = load float*, float** %a30, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds float, float* %128, i32 0
  %129 = load float, float* %arrayidx61, align 4, !tbaa !8
  %add62 = fadd float %129, 1.000000e+00
  store float %add62, float* %arrayidx61, align 4, !tbaa !8
  %130 = load float*, float** %L.addr, align 4, !tbaa !2
  %131 = load i32, i32* %r.addr, align 4, !tbaa !6
  %132 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul63 = mul nsw i32 %131, %132
  %add.ptr64 = getelementptr inbounds float, float* %130, i32 %mul63
  %133 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr65 = getelementptr inbounds float, float* %add.ptr64, i32 %133
  %134 = load float*, float** %d.addr, align 4, !tbaa !2
  %135 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr66 = getelementptr inbounds float, float* %134, i32 %135
  %136 = load float*, float** %a30, align 4, !tbaa !2
  %137 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %138 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub67 = sub nsw i32 %137, %138
  %139 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %140 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  call void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %add.ptr65, float* %add.ptr66, float* %136, i32 %sub67, i32 %139, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %140)
  %141 = bitcast float** %a30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #7
  %142 = bitcast float** %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #7
  br label %if.end

if.end:                                           ; preds = %for.end60, %for.end
  %143 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #7
  %144 = bitcast i32* %LDLTAddTL_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #7
  br label %if.end68

if.end68:                                         ; preds = %if.end
  %145 = load float*, float** %L.addr, align 4, !tbaa !2
  %146 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %147 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %148 = load i32, i32* %r.addr, align 4, !tbaa !6
  call void @_Z14btRemoveRowColPfiii(float* %145, i32 %146, i32 %147, i32 %148)
  %149 = load i32, i32* %r.addr, align 4, !tbaa !6
  %150 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %sub69 = sub nsw i32 %150, 1
  %cmp70 = icmp slt i32 %149, %sub69
  br i1 %cmp70, label %if.then71, label %if.end78

if.then71:                                        ; preds = %if.end68
  %151 = load float*, float** %d.addr, align 4, !tbaa !2
  %152 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr72 = getelementptr inbounds float, float* %151, i32 %152
  %153 = bitcast float* %add.ptr72 to i8*
  %154 = load float*, float** %d.addr, align 4, !tbaa !2
  %155 = load i32, i32* %r.addr, align 4, !tbaa !6
  %add.ptr73 = getelementptr inbounds float, float* %154, i32 %155
  %add.ptr74 = getelementptr inbounds float, float* %add.ptr73, i32 1
  %156 = bitcast float* %add.ptr74 to i8*
  %157 = load i32, i32* %n2.addr, align 4, !tbaa !6
  %158 = load i32, i32* %r.addr, align 4, !tbaa !6
  %sub75 = sub nsw i32 %157, %158
  %sub76 = sub nsw i32 %sub75, 1
  %mul77 = mul i32 %sub76, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %153, i8* align 4 %156, i32 %mul77, i1 false)
  br label %if.end78

if.end78:                                         ; preds = %if.then, %if.then71, %if.end68
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z29btEstimateLDLTAddTLTmpbufSizei(i32 %nskip) #3 comdat {
entry:
  %nskip.addr = alloca i32, align 4
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  %0 = load i32, i32* %nskip.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %0, 2
  %mul1 = mul i32 %mul, 4
  ret i32 %mul1
}

define hidden void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %this, i32 %i, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #0 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %C = alloca i32*, align 4
  %last_idx = alloca i32, align 4
  %nC = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = bitcast i32** %C to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %1 = load i32*, i32** %m_C, align 4, !tbaa !30
  store i32* %1, i32** %C, align 4, !tbaa !2
  %2 = bitcast i32* %last_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 -1, i32* %last_idx, align 4, !tbaa !6
  %3 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_nC, align 4, !tbaa !14
  store i32 %4, i32* %nC, align 4, !tbaa !6
  %5 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %8 = load i32*, i32** %C, align 4, !tbaa !2
  %9 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %11 = load i32, i32* %nC, align 4, !tbaa !6
  %sub = sub nsw i32 %11, 1
  %cmp2 = icmp eq i32 %10, %sub
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %12 = load i32, i32* %j, align 4, !tbaa !6
  store i32 %12, i32* %last_idx, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %13 = load i32*, i32** %C, align 4, !tbaa !2
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds i32, i32* %13, i32 %14
  %15 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %16 = load i32, i32* %i.addr, align 4, !tbaa !6
  %cmp4 = icmp eq i32 %15, %16
  br i1 %cmp4, label %if.then5, label %if.end27

if.then5:                                         ; preds = %if.end
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %17 = load float**, float*** %m_A, align 4, !tbaa !16
  %18 = load i32*, i32** %C, align 4, !tbaa !2
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %19 = load float*, float** %m_L, align 4, !tbaa !22
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %20 = load float*, float** %m_d, align 4, !tbaa !23
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %21 = load i32, i32* %m_n, align 4, !tbaa !10
  %22 = load i32, i32* %nC, align 4, !tbaa !6
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %24 = load i32, i32* %m_nskip, align 4, !tbaa !12
  %25 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4, !tbaa !2
  call void @_Z12btLDLTRemovePPfPKiS_S_iiiiR20btAlignedObjectArrayIfE(float** %17, i32* %18, float* %19, float* %20, i32 %21, i32 %22, i32 %23, i32 %24, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %25)
  %26 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load i32, i32* %last_idx, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %27, -1
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %add = add nsw i32 %28, 1
  store i32 %add, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then7
  %29 = load i32, i32* %k, align 4, !tbaa !6
  %30 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp9 = icmp slt i32 %29, %30
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %31 = load i32*, i32** %C, align 4, !tbaa !2
  %32 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i32, i32* %31, i32 %32
  %33 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %34 = load i32, i32* %nC, align 4, !tbaa !6
  %sub12 = sub nsw i32 %34, 1
  %cmp13 = icmp eq i32 %33, %sub12
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body10
  br label %for.end

if.end15:                                         ; preds = %for.body10
  br label %for.inc

for.inc:                                          ; preds = %if.end15
  %35 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.end:                                          ; preds = %if.then14, %for.cond8
  br label %if.end16

if.else:                                          ; preds = %if.then5
  %36 = load i32, i32* %last_idx, align 4, !tbaa !6
  store i32 %36, i32* %k, align 4, !tbaa !6
  br label %if.end16

if.end16:                                         ; preds = %if.else, %for.end
  %37 = load i32*, i32** %C, align 4, !tbaa !2
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds i32, i32* %37, i32 %38
  %39 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %40 = load i32*, i32** %C, align 4, !tbaa !2
  %41 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i32, i32* %40, i32 %41
  store i32 %39, i32* %arrayidx18, align 4, !tbaa !6
  %42 = load i32, i32* %j, align 4, !tbaa !6
  %43 = load i32, i32* %nC, align 4, !tbaa !6
  %sub19 = sub nsw i32 %43, 1
  %cmp20 = icmp slt i32 %42, %sub19
  br i1 %cmp20, label %if.then21, label %if.end26

if.then21:                                        ; preds = %if.end16
  %44 = load i32*, i32** %C, align 4, !tbaa !2
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i32, i32* %44, i32 %45
  %46 = bitcast i32* %add.ptr to i8*
  %47 = load i32*, i32** %C, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %add.ptr22 = getelementptr inbounds i32, i32* %47, i32 %48
  %add.ptr23 = getelementptr inbounds i32, i32* %add.ptr22, i32 1
  %49 = bitcast i32* %add.ptr23 to i8*
  %50 = load i32, i32* %nC, align 4, !tbaa !6
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %sub24 = sub nsw i32 %50, %51
  %sub25 = sub nsw i32 %sub24, 1
  %mul = mul i32 %sub25, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %49, i32 %mul, i1 false)
  br label %if.end26

if.end26:                                         ; preds = %if.then21, %if.end16
  %52 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  br label %for.end30

if.end27:                                         ; preds = %if.end
  br label %for.inc28

for.inc28:                                        ; preds = %if.end27
  %53 = load i32, i32* %j, align 4, !tbaa !6
  %inc29 = add nsw i32 %53, 1
  store i32 %inc29, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end30:                                        ; preds = %if.end26, %for.cond
  %m_A31 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %54 = load float**, float*** %m_A31, align 4, !tbaa !16
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %55 = load float*, float** %m_x, align 4, !tbaa !17
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %56 = load float*, float** %m_b, align 4, !tbaa !18
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %57 = load float*, float** %m_w, align 4, !tbaa !19
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %58 = load float*, float** %m_lo, align 4, !tbaa !20
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %59 = load float*, float** %m_hi, align 4, !tbaa !21
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %60 = load i32*, i32** %m_p, align 4, !tbaa !29
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %61 = load i8*, i8** %m_state, align 4, !tbaa !27
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %62 = load i32*, i32** %m_findex, align 4, !tbaa !28
  %m_n32 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %63 = load i32, i32* %m_n32, align 4, !tbaa !10
  %64 = load i32, i32* %i.addr, align 4, !tbaa !6
  %65 = load i32, i32* %nC, align 4, !tbaa !6
  %sub33 = sub nsw i32 %65, 1
  %m_nskip34 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %66 = load i32, i32* %m_nskip34, align 4, !tbaa !12
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %54, float* %55, float* %56, float* %57, float* %58, float* %59, i32* %60, i8* %61, i32* %62, i32 %63, i32 %64, i32 %sub33, i32 %66, i32 1)
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %67 = load i32, i32* %m_nN, align 4, !tbaa !15
  %inc35 = add nsw i32 %67, 1
  store i32 %inc35, i32* %m_nN, align 4, !tbaa !15
  %68 = load i32, i32* %nC, align 4, !tbaa !6
  %sub36 = sub nsw i32 %68, 1
  %m_nC37 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %sub36, i32* %m_nC37, align 4, !tbaa !14
  %69 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #7
  %70 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  %71 = bitcast i32* %last_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = bitcast i32** %C to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  ret void
}

define hidden void @_ZN5btLCP22pN_equals_ANC_times_qCEPfS0_(%struct.btLCP* %this, float* %p, float* %q) #0 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %ptgt = alloca float*, align 4
  %nN = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store float* %q, float** %q.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4, !tbaa !14
  store i32 %1, i32* %nC, align 4, !tbaa !6
  %2 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load float*, float** %p.addr, align 4, !tbaa !2
  %4 = load i32, i32* %nC, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %3, i32 %4
  store float* %add.ptr, float** %ptgt, align 4, !tbaa !2
  %5 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %6 = load i32, i32* %m_nN, align 4, !tbaa !15
  store i32 %6, i32* %nN, align 4, !tbaa !6
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load i32, i32* %nN, align 4, !tbaa !6
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %11 = load float**, float*** %m_A, align 4, !tbaa !16
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = load i32, i32* %nC, align 4, !tbaa !6
  %add = add nsw i32 %12, %13
  %arrayidx = getelementptr inbounds float*, float** %11, i32 %add
  %14 = load float*, float** %arrayidx, align 4, !tbaa !2
  %15 = load float*, float** %q.addr, align 4, !tbaa !2
  %16 = load i32, i32* %nC, align 4, !tbaa !6
  %call = call float @_Z10btLargeDotPKfS0_i(float* %14, float* %15, i32 %16)
  %17 = load float*, float** %ptgt, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %17, i32 %18
  store float %call, float* %arrayidx2, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  %21 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN5btLCP17pN_plusequals_ANiEPfii(%struct.btLCP* %this, float* %p, i32 %i, i32 %sign) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %i.addr = alloca i32, align 4
  %sign.addr = alloca i32, align 4
  %nC = alloca i32, align 4
  %aptr = alloca float*, align 4
  %ptgt = alloca float*, align 4
  %nN = alloca i32, align 4
  %j = alloca i32, align 4
  %nN6 = alloca i32, align 4
  %j8 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store i32 %sign, i32* %sign.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4, !tbaa !14
  store i32 %1, i32* %nC, align 4, !tbaa !6
  %2 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %3 = load float**, float*** %m_A, align 4, !tbaa !16
  %4 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %3, i32 %4
  %5 = load float*, float** %arrayidx, align 4, !tbaa !2
  %6 = load i32, i32* %nC, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %5, i32 %6
  store float* %add.ptr, float** %aptr, align 4, !tbaa !2
  %7 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load float*, float** %p.addr, align 4, !tbaa !2
  %9 = load i32, i32* %nC, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds float, float* %8, i32 %9
  store float* %add.ptr2, float** %ptgt, align 4, !tbaa !2
  %10 = load i32, i32* %sign.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %10, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %11 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %12 = load i32, i32* %m_nN, align 4, !tbaa !15
  store i32 %12, i32* %nN, align 4, !tbaa !6
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %15 = load i32, i32* %nN, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load float*, float** %aptr, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx4, align 4, !tbaa !8
  %20 = load float*, float** %ptgt, align 4, !tbaa !2
  %21 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds float, float* %20, i32 %21
  %22 = load float, float* %arrayidx5, align 4, !tbaa !8
  %add = fadd float %22, %19
  store float %add, float* %arrayidx5, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  br label %if.end

if.else:                                          ; preds = %entry
  %25 = bitcast i32* %nN6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %m_nN7 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %26 = load i32, i32* %m_nN7, align 4, !tbaa !15
  store i32 %26, i32* %nN6, align 4, !tbaa !6
  %27 = bitcast i32* %j8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  store i32 0, i32* %j8, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc15, %if.else
  %28 = load i32, i32* %j8, align 4, !tbaa !6
  %29 = load i32, i32* %nN6, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %28, %29
  br i1 %cmp10, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond9
  %30 = bitcast i32* %j8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  br label %for.end17

for.body12:                                       ; preds = %for.cond9
  %31 = load float*, float** %aptr, align 4, !tbaa !2
  %32 = load i32, i32* %j8, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx13, align 4, !tbaa !8
  %34 = load float*, float** %ptgt, align 4, !tbaa !2
  %35 = load i32, i32* %j8, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds float, float* %34, i32 %35
  %36 = load float, float* %arrayidx14, align 4, !tbaa !8
  %sub = fsub float %36, %33
  store float %sub, float* %arrayidx14, align 4, !tbaa !8
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %37 = load i32, i32* %j8, align 4, !tbaa !6
  %inc16 = add nsw i32 %37, 1
  store i32 %inc16, i32* %j8, align 4, !tbaa !6
  br label %for.cond9

for.end17:                                        ; preds = %for.cond.cleanup11
  %38 = bitcast i32* %nN6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  br label %if.end

if.end:                                           ; preds = %for.end17, %for.end
  %39 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  %40 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN5btLCP24pC_plusequals_s_times_qCEPffS0_(%struct.btLCP* %this, float* %p, float %s, float* %q) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %s.addr = alloca float, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store float %s, float* %s.addr, align 4, !tbaa !8
  store float* %q, float** %q.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4, !tbaa !14
  store i32 %1, i32* %nC, align 4, !tbaa !6
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load float, float* %s.addr, align 4, !tbaa !8
  %7 = load float*, float** %q.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %6, %9
  %10 = load float*, float** %p.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %10, i32 %11
  %12 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %12, %mul
  store float %add, float* %arrayidx2, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %14 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN5btLCP24pN_plusequals_s_times_qNEPffS0_(%struct.btLCP* %this, float* %p, float %s, float* %q) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %s.addr = alloca float, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %ptgt = alloca float*, align 4
  %qsrc = alloca float*, align 4
  %nN = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store float* %p, float** %p.addr, align 4, !tbaa !2
  store float %s, float* %s.addr, align 4, !tbaa !8
  store float* %q, float** %q.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4, !tbaa !14
  store i32 %1, i32* %nC, align 4, !tbaa !6
  %2 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load float*, float** %p.addr, align 4, !tbaa !2
  %4 = load i32, i32* %nC, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %3, i32 %4
  store float* %add.ptr, float** %ptgt, align 4, !tbaa !2
  %5 = bitcast float** %qsrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load float*, float** %q.addr, align 4, !tbaa !2
  %7 = load i32, i32* %nC, align 4, !tbaa !6
  %add.ptr2 = getelementptr inbounds float, float* %6, i32 %7
  store float* %add.ptr2, float** %qsrc, align 4, !tbaa !2
  %8 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %9 = load i32, i32* %m_nN, align 4, !tbaa !15
  store i32 %9, i32* %nN, align 4, !tbaa !6
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %nN, align 4, !tbaa !6
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load float, float* %s.addr, align 4, !tbaa !8
  %15 = load float*, float** %qsrc, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %14, %17
  %18 = load float*, float** %ptgt, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %18, i32 %19
  %20 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %20, %mul
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %22 = bitcast i32* %nN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast float** %qsrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %24 = bitcast float** %ptgt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %this, float* %a, i32 %i, i32 %dir, i32 %only_transfer) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %a.addr = alloca float*, align 4
  %i.addr = alloca i32, align 4
  %dir.addr = alloca i32, align 4
  %only_transfer.addr = alloca i32, align 4
  %Dell = alloca float*, align 4
  %C = alloca i32*, align 4
  %aptr = alloca float*, align 4
  %nub = alloca i32, align 4
  %j = alloca i32, align 4
  %nC = alloca i32, align 4
  %ell = alloca float*, align 4
  %Dell17 = alloca float*, align 4
  %d = alloca float*, align 4
  %nC19 = alloca i32, align 4
  %j21 = alloca i32, align 4
  %tmp = alloca float*, align 4
  %ell32 = alloca float*, align 4
  %nC34 = alloca i32, align 4
  %j36 = alloca i32, align 4
  %C51 = alloca i32*, align 4
  %tmp53 = alloca float*, align 4
  %nC55 = alloca i32, align 4
  %j57 = alloca i32, align 4
  %C68 = alloca i32*, align 4
  %tmp70 = alloca float*, align 4
  %nC72 = alloca i32, align 4
  %j74 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store i32 %only_transfer, i32* %only_transfer.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4, !tbaa !14
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end86

if.then:                                          ; preds = %entry
  %1 = bitcast float** %Dell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %2 = load float*, float** %m_Dell, align 4, !tbaa !24
  store float* %2, float** %Dell, align 4, !tbaa !2
  %3 = bitcast i32** %C to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %4 = load i32*, i32** %m_C, align 4, !tbaa !30
  store i32* %4, i32** %C, align 4, !tbaa !2
  %5 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %6 = load float**, float*** %m_A, align 4, !tbaa !16
  %7 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %6, i32 %7
  %8 = load float*, float** %arrayidx, align 4, !tbaa !2
  store float* %8, float** %aptr, align 4, !tbaa !2
  %9 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %10 = load i32, i32* %m_nub, align 4, !tbaa !13
  store i32 %10, i32* %nub, align 4, !tbaa !6
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %12 = load i32, i32* %j, align 4, !tbaa !6
  %13 = load i32, i32* %nub, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %12, %13
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load float*, float** %aptr, align 4, !tbaa !2
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx3, align 4, !tbaa !8
  %17 = load float*, float** %Dell, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %17, i32 %18
  store float %16, float* %arrayidx4, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %m_nC5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %21 = load i32, i32* %m_nC5, align 4, !tbaa !14
  store i32 %21, i32* %nC, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %for.end
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %23 = load i32, i32* %nC, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %22, %23
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %24 = load float*, float** %aptr, align 4, !tbaa !2
  %25 = load i32*, i32** %C, align 4, !tbaa !2
  %26 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i32, i32* %25, i32 %26
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %24, i32 %27
  %28 = load float, float* %arrayidx10, align 4, !tbaa !8
  %29 = load float*, float** %Dell, align 4, !tbaa !2
  %30 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds float, float* %29, i32 %30
  store float %28, float* %arrayidx11, align 4, !tbaa !8
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %inc13 = add nsw i32 %31, 1
  store i32 %inc13, i32* %j, align 4, !tbaa !6
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  %32 = bitcast i32* %nC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast float** %aptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast i32** %C to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast float** %Dell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %38 = load float*, float** %m_L, align 4, !tbaa !22
  %m_Dell15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %39 = load float*, float** %m_Dell15, align 4, !tbaa !24
  %m_nC16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %40 = load i32, i32* %m_nC16, align 4, !tbaa !14
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %41 = load i32, i32* %m_nskip, align 4, !tbaa !12
  call void @_Z9btSolveL1PKfPfii(float* %38, float* %39, i32 %40, i32 %41)
  %42 = bitcast float** %ell to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %43 = load float*, float** %m_ell, align 4, !tbaa !25
  store float* %43, float** %ell, align 4, !tbaa !2
  %44 = bitcast float** %Dell17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %m_Dell18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %45 = load float*, float** %m_Dell18, align 4, !tbaa !24
  store float* %45, float** %Dell17, align 4, !tbaa !2
  %46 = bitcast float** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #7
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %47 = load float*, float** %m_d, align 4, !tbaa !23
  store float* %47, float** %d, align 4, !tbaa !2
  %48 = bitcast i32* %nC19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #7
  %m_nC20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %49 = load i32, i32* %m_nC20, align 4, !tbaa !14
  store i32 %49, i32* %nC19, align 4, !tbaa !6
  %50 = bitcast i32* %j21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  store i32 0, i32* %j21, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc28, %for.end14
  %51 = load i32, i32* %j21, align 4, !tbaa !6
  %52 = load i32, i32* %nC19, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %51, %52
  br i1 %cmp23, label %for.body24, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond22
  %53 = bitcast i32* %j21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  br label %for.end30

for.body24:                                       ; preds = %for.cond22
  %54 = load float*, float** %Dell17, align 4, !tbaa !2
  %55 = load i32, i32* %j21, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx25, align 4, !tbaa !8
  %57 = load float*, float** %d, align 4, !tbaa !2
  %58 = load i32, i32* %j21, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds float, float* %57, i32 %58
  %59 = load float, float* %arrayidx26, align 4, !tbaa !8
  %mul = fmul float %56, %59
  %60 = load float*, float** %ell, align 4, !tbaa !2
  %61 = load i32, i32* %j21, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds float, float* %60, i32 %61
  store float %mul, float* %arrayidx27, align 4, !tbaa !8
  br label %for.inc28

for.inc28:                                        ; preds = %for.body24
  %62 = load i32, i32* %j21, align 4, !tbaa !6
  %inc29 = add nsw i32 %62, 1
  store i32 %inc29, i32* %j21, align 4, !tbaa !6
  br label %for.cond22

for.end30:                                        ; preds = %for.cond.cleanup
  %63 = bitcast i32* %nC19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = bitcast float** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #7
  %65 = bitcast float** %Dell17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #7
  %66 = bitcast float** %ell to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = load i32, i32* %only_transfer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %67, 0
  br i1 %tobool, label %if.end85, label %if.then31

if.then31:                                        ; preds = %for.end30
  %68 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #7
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %69 = load float*, float** %m_tmp, align 4, !tbaa !26
  store float* %69, float** %tmp, align 4, !tbaa !2
  %70 = bitcast float** %ell32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #7
  %m_ell33 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %71 = load float*, float** %m_ell33, align 4, !tbaa !25
  store float* %71, float** %ell32, align 4, !tbaa !2
  %72 = bitcast i32* %nC34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #7
  %m_nC35 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %73 = load i32, i32* %m_nC35, align 4, !tbaa !14
  store i32 %73, i32* %nC34, align 4, !tbaa !6
  %74 = bitcast i32* %j36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #7
  store i32 0, i32* %j36, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc43, %if.then31
  %75 = load i32, i32* %j36, align 4, !tbaa !6
  %76 = load i32, i32* %nC34, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %75, %76
  br i1 %cmp38, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond37
  %77 = bitcast i32* %j36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  br label %for.end45

for.body40:                                       ; preds = %for.cond37
  %78 = load float*, float** %ell32, align 4, !tbaa !2
  %79 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx41, align 4, !tbaa !8
  %81 = load float*, float** %tmp, align 4, !tbaa !2
  %82 = load i32, i32* %j36, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds float, float* %81, i32 %82
  store float %80, float* %arrayidx42, align 4, !tbaa !8
  br label %for.inc43

for.inc43:                                        ; preds = %for.body40
  %83 = load i32, i32* %j36, align 4, !tbaa !6
  %inc44 = add nsw i32 %83, 1
  store i32 %inc44, i32* %j36, align 4, !tbaa !6
  br label %for.cond37

for.end45:                                        ; preds = %for.cond.cleanup39
  %84 = bitcast i32* %nC34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #7
  %m_L46 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %85 = load float*, float** %m_L46, align 4, !tbaa !22
  %86 = load float*, float** %tmp, align 4, !tbaa !2
  %m_nC47 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %87 = load i32, i32* %m_nC47, align 4, !tbaa !14
  %m_nskip48 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %88 = load i32, i32* %m_nskip48, align 4, !tbaa !12
  call void @_Z10btSolveL1TPKfPfii(float* %85, float* %86, i32 %87, i32 %88)
  %89 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %cmp49 = icmp sgt i32 %89, 0
  br i1 %cmp49, label %if.then50, label %if.else

if.then50:                                        ; preds = %for.end45
  %90 = bitcast i32** %C51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #7
  %m_C52 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %91 = load i32*, i32** %m_C52, align 4, !tbaa !30
  store i32* %91, i32** %C51, align 4, !tbaa !2
  %92 = bitcast float** %tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #7
  %m_tmp54 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %93 = load float*, float** %m_tmp54, align 4, !tbaa !26
  store float* %93, float** %tmp53, align 4, !tbaa !2
  %94 = bitcast i32* %nC55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #7
  %m_nC56 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %95 = load i32, i32* %m_nC56, align 4, !tbaa !14
  store i32 %95, i32* %nC55, align 4, !tbaa !6
  %96 = bitcast i32* %j57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #7
  store i32 0, i32* %j57, align 4, !tbaa !6
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc65, %if.then50
  %97 = load i32, i32* %j57, align 4, !tbaa !6
  %98 = load i32, i32* %nC55, align 4, !tbaa !6
  %cmp59 = icmp slt i32 %97, %98
  br i1 %cmp59, label %for.body61, label %for.cond.cleanup60

for.cond.cleanup60:                               ; preds = %for.cond58
  %99 = bitcast i32* %j57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #7
  br label %for.end67

for.body61:                                       ; preds = %for.cond58
  %100 = load float*, float** %tmp53, align 4, !tbaa !2
  %101 = load i32, i32* %j57, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds float, float* %100, i32 %101
  %102 = load float, float* %arrayidx62, align 4, !tbaa !8
  %fneg = fneg float %102
  %103 = load float*, float** %a.addr, align 4, !tbaa !2
  %104 = load i32*, i32** %C51, align 4, !tbaa !2
  %105 = load i32, i32* %j57, align 4, !tbaa !6
  %arrayidx63 = getelementptr inbounds i32, i32* %104, i32 %105
  %106 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds float, float* %103, i32 %106
  store float %fneg, float* %arrayidx64, align 4, !tbaa !8
  br label %for.inc65

for.inc65:                                        ; preds = %for.body61
  %107 = load i32, i32* %j57, align 4, !tbaa !6
  %inc66 = add nsw i32 %107, 1
  store i32 %inc66, i32* %j57, align 4, !tbaa !6
  br label %for.cond58

for.end67:                                        ; preds = %for.cond.cleanup60
  %108 = bitcast i32* %nC55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  %109 = bitcast float** %tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #7
  %110 = bitcast i32** %C51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  br label %if.end

if.else:                                          ; preds = %for.end45
  %111 = bitcast i32** %C68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #7
  %m_C69 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %112 = load i32*, i32** %m_C69, align 4, !tbaa !30
  store i32* %112, i32** %C68, align 4, !tbaa !2
  %113 = bitcast float** %tmp70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #7
  %m_tmp71 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %114 = load float*, float** %m_tmp71, align 4, !tbaa !26
  store float* %114, float** %tmp70, align 4, !tbaa !2
  %115 = bitcast i32* %nC72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #7
  %m_nC73 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %116 = load i32, i32* %m_nC73, align 4, !tbaa !14
  store i32 %116, i32* %nC72, align 4, !tbaa !6
  %117 = bitcast i32* %j74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #7
  store i32 0, i32* %j74, align 4, !tbaa !6
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc82, %if.else
  %118 = load i32, i32* %j74, align 4, !tbaa !6
  %119 = load i32, i32* %nC72, align 4, !tbaa !6
  %cmp76 = icmp slt i32 %118, %119
  br i1 %cmp76, label %for.body78, label %for.cond.cleanup77

for.cond.cleanup77:                               ; preds = %for.cond75
  %120 = bitcast i32* %j74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #7
  br label %for.end84

for.body78:                                       ; preds = %for.cond75
  %121 = load float*, float** %tmp70, align 4, !tbaa !2
  %122 = load i32, i32* %j74, align 4, !tbaa !6
  %arrayidx79 = getelementptr inbounds float, float* %121, i32 %122
  %123 = load float, float* %arrayidx79, align 4, !tbaa !8
  %124 = load float*, float** %a.addr, align 4, !tbaa !2
  %125 = load i32*, i32** %C68, align 4, !tbaa !2
  %126 = load i32, i32* %j74, align 4, !tbaa !6
  %arrayidx80 = getelementptr inbounds i32, i32* %125, i32 %126
  %127 = load i32, i32* %arrayidx80, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds float, float* %124, i32 %127
  store float %123, float* %arrayidx81, align 4, !tbaa !8
  br label %for.inc82

for.inc82:                                        ; preds = %for.body78
  %128 = load i32, i32* %j74, align 4, !tbaa !6
  %inc83 = add nsw i32 %128, 1
  store i32 %inc83, i32* %j74, align 4, !tbaa !6
  br label %for.cond75

for.end84:                                        ; preds = %for.cond.cleanup77
  %129 = bitcast i32* %nC72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #7
  %130 = bitcast float** %tmp70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #7
  %131 = bitcast i32** %C68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #7
  br label %if.end

if.end:                                           ; preds = %for.end84, %for.end67
  %132 = bitcast float** %ell32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #7
  %133 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #7
  br label %if.end85

if.end85:                                         ; preds = %if.end, %for.end30
  br label %if.end86

if.end86:                                         ; preds = %if.end85, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN5btLCP9unpermuteEv(%struct.btLCP* %this) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %x = alloca float*, align 4
  %tmp = alloca float*, align 4
  %p = alloca i32*, align 4
  %n = alloca i32, align 4
  %j = alloca i32, align 4
  %w = alloca float*, align 4
  %tmp11 = alloca float*, align 4
  %p13 = alloca i32*, align 4
  %n15 = alloca i32, align 4
  %j17 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %0 = load float*, float** %m_tmp, align 4, !tbaa !26
  %1 = bitcast float* %0 to i8*
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %2 = load float*, float** %m_x, align 4, !tbaa !17
  %3 = bitcast float* %2 to i8*
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %4 = load i32, i32* %m_n, align 4, !tbaa !10
  %mul = mul i32 %4, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 %mul, i1 false)
  %5 = bitcast float** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %m_x2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %6 = load float*, float** %m_x2, align 4, !tbaa !17
  store float* %6, float** %x, align 4, !tbaa !2
  %7 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %m_tmp3 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %8 = load float*, float** %m_tmp3, align 4, !tbaa !26
  store float* %8, float** %tmp, align 4, !tbaa !2
  %9 = bitcast i32** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %10 = load i32*, i32** %m_p, align 4, !tbaa !29
  store i32* %10, i32** %p, align 4, !tbaa !2
  %11 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %m_n4 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %12 = load i32, i32* %m_n4, align 4, !tbaa !10
  store i32 %12, i32* %n, align 4, !tbaa !6
  %13 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %j, align 4, !tbaa !6
  %15 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %16 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load float*, float** %tmp, align 4, !tbaa !2
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx, align 4, !tbaa !8
  %20 = load float*, float** %x, align 4, !tbaa !2
  %21 = load i32*, i32** %p, align 4, !tbaa !2
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 %22
  %23 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %20, i32 %23
  store float %19, float* %arrayidx6, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %25 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  %26 = bitcast i32** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = bitcast float** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  %28 = bitcast float** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #7
  %m_tmp7 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %29 = load float*, float** %m_tmp7, align 4, !tbaa !26
  %30 = bitcast float* %29 to i8*
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %31 = load float*, float** %m_w, align 4, !tbaa !19
  %32 = bitcast float* %31 to i8*
  %m_n8 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %33 = load i32, i32* %m_n8, align 4, !tbaa !10
  %mul9 = mul i32 %33, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %32, i32 %mul9, i1 false)
  %34 = bitcast float** %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %m_w10 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %35 = load float*, float** %m_w10, align 4, !tbaa !19
  store float* %35, float** %w, align 4, !tbaa !2
  %36 = bitcast float** %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #7
  %m_tmp12 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %37 = load float*, float** %m_tmp12, align 4, !tbaa !26
  store float* %37, float** %tmp11, align 4, !tbaa !2
  %38 = bitcast i32** %p13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %m_p14 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %39 = load i32*, i32** %m_p14, align 4, !tbaa !29
  store i32* %39, i32** %p13, align 4, !tbaa !2
  %40 = bitcast i32* %n15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #7
  %m_n16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %41 = load i32, i32* %m_n16, align 4, !tbaa !10
  store i32 %41, i32* %n15, align 4, !tbaa !6
  %42 = bitcast i32* %j17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  store i32 0, i32* %j17, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc25, %for.end
  %43 = load i32, i32* %j17, align 4, !tbaa !6
  %44 = load i32, i32* %n15, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %43, %44
  br i1 %cmp19, label %for.body21, label %for.cond.cleanup20

for.cond.cleanup20:                               ; preds = %for.cond18
  %45 = bitcast i32* %j17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #7
  br label %for.end27

for.body21:                                       ; preds = %for.cond18
  %46 = load float*, float** %tmp11, align 4, !tbaa !2
  %47 = load i32, i32* %j17, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds float, float* %46, i32 %47
  %48 = load float, float* %arrayidx22, align 4, !tbaa !8
  %49 = load float*, float** %w, align 4, !tbaa !2
  %50 = load i32*, i32** %p13, align 4, !tbaa !2
  %51 = load i32, i32* %j17, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds i32, i32* %50, i32 %51
  %52 = load i32, i32* %arrayidx23, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds float, float* %49, i32 %52
  store float %48, float* %arrayidx24, align 4, !tbaa !8
  br label %for.inc25

for.inc25:                                        ; preds = %for.body21
  %53 = load i32, i32* %j17, align 4, !tbaa !6
  %inc26 = add nsw i32 %53, 1
  store i32 %inc26, i32* %j17, align 4, !tbaa !6
  br label %for.cond18

for.end27:                                        ; preds = %for.cond.cleanup20
  %54 = bitcast i32* %n15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #7
  %55 = bitcast i32** %p13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  %56 = bitcast float** %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = bitcast float** %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  ret void
}

define hidden zeroext i1 @_Z17btSolveDantzigLCPiPfS_S_S_iS_S_PiR22btDantzigScratchMemory(i32 %n, float* %A, float* %x, float* %b, float* %outer_w, i32 %nub, float* %lo, float* %hi, i32* %findex, %struct.btDantzigScratchMemory* nonnull align 4 dereferenceable(220) %scratchMem) #0 {
entry:
  %retval = alloca i1, align 1
  %n.addr = alloca i32, align 4
  %A.addr = alloca float*, align 4
  %x.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %outer_w.addr = alloca float*, align 4
  %nub.addr = alloca i32, align 4
  %lo.addr = alloca float*, align 4
  %hi.addr = alloca float*, align 4
  %findex.addr = alloca i32*, align 4
  %scratchMem.addr = alloca %struct.btDantzigScratchMemory*, align 4
  %nskip = alloca i32, align 4
  %nskip1 = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %w = alloca float*, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float*, align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp10 = alloca i32, align 4
  %ref.tmp11 = alloca i8, align 1
  %lcp = alloca %struct.btLCP, align 4
  %adj_nub = alloca i32, align 4
  %hit_first_friction_index = alloca i8, align 1
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %wfk = alloca float, align 4
  %dir = alloca i32, align 4
  %dirf = alloca float, align 4
  %cmd = alloca i32, align 4
  %si = alloca i32, align 4
  %s = alloca float, align 4
  %s2 = alloca float, align 4
  %s2140 = alloca float, align 4
  %numN = alloca i32, align 4
  %k151 = alloca i32, align 4
  %indexN_k = alloca i32, align 4
  %s2174 = alloca float, align 4
  %numC = alloca i32, align 4
  %k189 = alloca i32, align 4
  %indexC_k = alloca i32, align 4
  %s2203 = alloca float, align 4
  %s2221 = alloca float, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float* %A, float** %A.addr, align 4, !tbaa !2
  store float* %x, float** %x.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  store float* %outer_w, float** %outer_w.addr, align 4, !tbaa !2
  store i32 %nub, i32* %nub.addr, align 4, !tbaa !6
  store float* %lo, float** %lo.addr, align 4, !tbaa !2
  store float* %hi, float** %hi.addr, align 4, !tbaa !2
  store i32* %findex, i32** %findex.addr, align 4, !tbaa !2
  store %struct.btDantzigScratchMemory* %scratchMem, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  store i8 0, i8* @s_error, align 1, !tbaa !33
  %0 = load i32, i32* %nub.addr, align 4, !tbaa !6
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %nskip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %n.addr, align 4, !tbaa !6
  store i32 %3, i32* %nskip, align 4, !tbaa !6
  %4 = load float*, float** %A.addr, align 4, !tbaa !2
  %5 = load float*, float** %outer_w.addr, align 4, !tbaa !2
  %6 = load i32, i32* %n.addr, align 4, !tbaa !6
  %7 = load i32, i32* %nskip, align 4, !tbaa !6
  call void @_Z12btFactorLDLTPfS_ii(float* %4, float* %5, i32 %6, i32 %7)
  %8 = load float*, float** %A.addr, align 4, !tbaa !2
  %9 = load float*, float** %outer_w.addr, align 4, !tbaa !2
  %10 = load float*, float** %b.addr, align 4, !tbaa !2
  %11 = load i32, i32* %n.addr, align 4, !tbaa !6
  %12 = load i32, i32* %nskip, align 4, !tbaa !6
  call void @_Z11btSolveLDLTPKfS0_Pfii(float* %8, float* %9, float* %10, i32 %11, i32 %12)
  %13 = load float*, float** %x.addr, align 4, !tbaa !2
  %14 = bitcast float* %13 to i8*
  %15 = load float*, float** %b.addr, align 4, !tbaa !2
  %16 = bitcast float* %15 to i8*
  %17 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 %17, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %16, i32 %mul, i1 false)
  %18 = load i8, i8* @s_error, align 1, !tbaa !33, !range !35
  %tobool = trunc i8 %18 to i1
  %lnot = xor i1 %tobool, true
  store i1 %lnot, i1* %retval, align 1
  %19 = bitcast i32* %nskip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  br label %return

if.end:                                           ; preds = %entry
  %20 = bitcast i32* %nskip1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %21 = load i32, i32* %n.addr, align 4, !tbaa !6
  store i32 %21, i32* %nskip1, align 4, !tbaa !6
  %22 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %L = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %22, i32 0, i32 1
  %23 = load i32, i32* %n.addr, align 4, !tbaa !6
  %24 = load i32, i32* %nskip1, align 4, !tbaa !6
  %mul2 = mul nsw i32 %23, %24
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %L, i32 %mul2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %26 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  %27 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %d = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %27, i32 0, i32 2
  %28 = load i32, i32* %n.addr, align 4, !tbaa !6
  %29 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %d, i32 %28, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %30 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #7
  %31 = bitcast float** %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = load float*, float** %outer_w.addr, align 4, !tbaa !2
  store float* %32, float** %w, align 4, !tbaa !2
  %33 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %33, i32 0, i32 3
  %34 = load i32, i32* %n.addr, align 4, !tbaa !6
  %35 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %delta_w, i32 %34, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %36 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %37, i32 0, i32 4
  %38 = load i32, i32* %n.addr, align 4, !tbaa !6
  %39 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %delta_x, i32 %38, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %40 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %Dell = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %41, i32 0, i32 5
  %42 = load i32, i32* %n.addr, align 4, !tbaa !6
  %43 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #7
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %Dell, i32 %42, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %44 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %ell = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %45, i32 0, i32 6
  %46 = load i32, i32* %n.addr, align 4, !tbaa !6
  %47 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %ell, i32 %46, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %48 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #7
  %49 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %Arows = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %49, i32 0, i32 7
  %50 = load i32, i32* %n.addr, align 4, !tbaa !6
  %51 = bitcast float** %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  store float* null, float** %ref.tmp8, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %Arows, i32 %50, float** nonnull align 4 dereferenceable(4) %ref.tmp8)
  %52 = bitcast float** %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #7
  %53 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %p = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %53, i32 0, i32 8
  %54 = load i32, i32* %n.addr, align 4, !tbaa !6
  %55 = bitcast i32* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #7
  store i32 0, i32* %ref.tmp9, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %p, i32 %54, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %56 = bitcast i32* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #7
  %57 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %C = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %57, i32 0, i32 9
  %58 = load i32, i32* %n.addr, align 4, !tbaa !6
  %59 = bitcast i32* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  store i32 0, i32* %ref.tmp10, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %C, i32 %58, i32* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %60 = bitcast i32* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #7
  %61 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %61, i32 0, i32 10
  %62 = load i32, i32* %n.addr, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ref.tmp11) #7
  store i8 0, i8* %ref.tmp11, align 1, !tbaa !33
  call void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.8* %state, i32 %62, i8* nonnull align 1 dereferenceable(1) %ref.tmp11)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ref.tmp11) #7
  %63 = bitcast %struct.btLCP* %lcp to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %63) #7
  %64 = load i32, i32* %n.addr, align 4, !tbaa !6
  %65 = load i32, i32* %nskip1, align 4, !tbaa !6
  %66 = load i32, i32* %nub.addr, align 4, !tbaa !6
  %67 = load float*, float** %A.addr, align 4, !tbaa !2
  %68 = load float*, float** %x.addr, align 4, !tbaa !2
  %69 = load float*, float** %b.addr, align 4, !tbaa !2
  %70 = load float*, float** %w, align 4, !tbaa !2
  %71 = load float*, float** %lo.addr, align 4, !tbaa !2
  %72 = load float*, float** %hi.addr, align 4, !tbaa !2
  %73 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %L12 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %73, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %L12, i32 0)
  %74 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %d13 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %74, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %d13, i32 0)
  %75 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %Dell15 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %75, i32 0, i32 5
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %Dell15, i32 0)
  %76 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %ell17 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %76, i32 0, i32 6
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %ell17, i32 0)
  %77 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w19 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %77, i32 0, i32 3
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w19, i32 0)
  %78 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state21 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %78, i32 0, i32 10
  %call22 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state21, i32 0)
  %79 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %80 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %p23 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %80, i32 0, i32 8
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %p23, i32 0)
  %81 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %C25 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %81, i32 0, i32 9
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %C25, i32 0)
  %82 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %Arows27 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %82, i32 0, i32 7
  %call28 = call nonnull align 4 dereferenceable(4) float** @_ZN20btAlignedObjectArrayIPfEixEi(%class.btAlignedObjectArray.0* %Arows27, i32 0)
  %call29 = call %struct.btLCP* @_ZN5btLCPC1EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_(%struct.btLCP* %lcp, i32 %64, i32 %65, i32 %66, float* %67, float* %68, float* %69, float* %70, float* %71, float* %72, float* %call, float* %call14, float* %call16, float* %call18, float* %call20, i8* %call22, i32* %79, i32* %call24, i32* %call26, float** %call28)
  %83 = bitcast i32* %adj_nub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #7
  %call30 = call i32 @_ZNK5btLCP6getNubEv(%struct.btLCP* %lcp)
  store i32 %call30, i32* %adj_nub, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hit_first_friction_index) #7
  store i8 0, i8* %hit_first_friction_index, align 1, !tbaa !33
  %84 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #7
  %85 = load i32, i32* %adj_nub, align 4, !tbaa !6
  store i32 %85, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc298, %if.end
  %86 = load i32, i32* %i, align 4, !tbaa !6
  %87 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp31 = icmp slt i32 %86, %87
  br i1 %cmp31, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup300

for.body:                                         ; preds = %for.cond
  store i8 0, i8* @s_error, align 1, !tbaa !33
  %88 = load i8, i8* %hit_first_friction_index, align 1, !tbaa !33, !range !35
  %tobool32 = trunc i8 %88 to i1
  br i1 %tobool32, label %if.end67, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %89 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %tobool33 = icmp ne i32* %89, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end67

land.lhs.true34:                                  ; preds = %land.lhs.true
  %90 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %90, i32 %91
  %92 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %cmp35 = icmp sge i32 %92, 0
  br i1 %cmp35, label %if.then36, label %if.end67

if.then36:                                        ; preds = %land.lhs.true34
  %93 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #7
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then36
  %94 = load i32, i32* %j, align 4, !tbaa !6
  %95 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %94, %95
  br i1 %cmp38, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond37
  store i32 5, i32* %cleanup.dest.slot, align 4
  %96 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #7
  br label %for.end

for.body40:                                       ; preds = %for.cond37
  %97 = load float*, float** %x.addr, align 4, !tbaa !2
  %98 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds float, float* %97, i32 %98
  %99 = load float, float* %arrayidx41, align 4, !tbaa !8
  %100 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w42 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %100, i32 0, i32 3
  %101 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %p43 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %101, i32 0, i32 8
  %102 = load i32, i32* %j, align 4, !tbaa !6
  %call44 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %p43, i32 %102)
  %103 = load i32, i32* %call44, align 4, !tbaa !6
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w42, i32 %103)
  store float %99, float* %call45, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body40
  %104 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %104, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond37

for.end:                                          ; preds = %for.cond.cleanup39
  %105 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  %106 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %106, i32* %k, align 4, !tbaa !6
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc64, %for.end
  %107 = load i32, i32* %k, align 4, !tbaa !6
  %108 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp47 = icmp slt i32 %107, %108
  br i1 %cmp47, label %for.body49, label %for.cond.cleanup48

for.cond.cleanup48:                               ; preds = %for.cond46
  store i32 8, i32* %cleanup.dest.slot, align 4
  %109 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #7
  br label %for.end66

for.body49:                                       ; preds = %for.cond46
  %110 = bitcast float* %wfk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #7
  %111 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w50 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %111, i32 0, i32 3
  %112 = load i32*, i32** %findex.addr, align 4, !tbaa !2
  %113 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w50, i32 %114)
  %115 = load float, float* %call52, align 4, !tbaa !8
  store float %115, float* %wfk, align 4, !tbaa !8
  %116 = load float, float* %wfk, align 4, !tbaa !8
  %cmp53 = fcmp oeq float %116, 0.000000e+00
  br i1 %cmp53, label %if.then54, label %if.else

if.then54:                                        ; preds = %for.body49
  %117 = load float*, float** %hi.addr, align 4, !tbaa !2
  %118 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx55 = getelementptr inbounds float, float* %117, i32 %118
  store float 0.000000e+00, float* %arrayidx55, align 4, !tbaa !8
  %119 = load float*, float** %lo.addr, align 4, !tbaa !2
  %120 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds float, float* %119, i32 %120
  store float 0.000000e+00, float* %arrayidx56, align 4, !tbaa !8
  br label %if.end63

if.else:                                          ; preds = %for.body49
  %121 = load float*, float** %hi.addr, align 4, !tbaa !2
  %122 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds float, float* %121, i32 %122
  %123 = load float, float* %arrayidx57, align 4, !tbaa !8
  %124 = load float, float* %wfk, align 4, !tbaa !8
  %mul58 = fmul float %123, %124
  %call59 = call float @_Z6btFabsf(float %mul58)
  %125 = load float*, float** %hi.addr, align 4, !tbaa !2
  %126 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds float, float* %125, i32 %126
  store float %call59, float* %arrayidx60, align 4, !tbaa !8
  %127 = load float*, float** %hi.addr, align 4, !tbaa !2
  %128 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds float, float* %127, i32 %128
  %129 = load float, float* %arrayidx61, align 4, !tbaa !8
  %fneg = fneg float %129
  %130 = load float*, float** %lo.addr, align 4, !tbaa !2
  %131 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds float, float* %130, i32 %131
  store float %fneg, float* %arrayidx62, align 4, !tbaa !8
  br label %if.end63

if.end63:                                         ; preds = %if.else, %if.then54
  %132 = bitcast float* %wfk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #7
  br label %for.inc64

for.inc64:                                        ; preds = %if.end63
  %133 = load i32, i32* %k, align 4, !tbaa !6
  %inc65 = add nsw i32 %133, 1
  store i32 %inc65, i32* %k, align 4, !tbaa !6
  br label %for.cond46

for.end66:                                        ; preds = %for.cond.cleanup48
  store i8 1, i8* %hit_first_friction_index, align 1, !tbaa !33
  br label %if.end67

if.end67:                                         ; preds = %for.end66, %land.lhs.true34, %land.lhs.true, %for.body
  %134 = load i32, i32* %i, align 4, !tbaa !6
  %135 = load float*, float** %x.addr, align 4, !tbaa !2
  %call68 = call float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %lcp, i32 %134, float* %135)
  %136 = load i32, i32* %i, align 4, !tbaa !6
  %137 = load float*, float** %x.addr, align 4, !tbaa !2
  %call69 = call float @_ZNK5btLCP12AiN_times_qNEiPf(%struct.btLCP* %lcp, i32 %136, float* %137)
  %add = fadd float %call68, %call69
  %138 = load float*, float** %b.addr, align 4, !tbaa !2
  %139 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds float, float* %138, i32 %139
  %140 = load float, float* %arrayidx70, align 4, !tbaa !8
  %sub = fsub float %add, %140
  %141 = load float*, float** %w, align 4, !tbaa !2
  %142 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds float, float* %141, i32 %142
  store float %sub, float* %arrayidx71, align 4, !tbaa !8
  %143 = load float*, float** %lo.addr, align 4, !tbaa !2
  %144 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds float, float* %143, i32 %144
  %145 = load float, float* %arrayidx72, align 4, !tbaa !8
  %cmp73 = fcmp oeq float %145, 0.000000e+00
  br i1 %cmp73, label %land.lhs.true74, label %if.else80

land.lhs.true74:                                  ; preds = %if.end67
  %146 = load float*, float** %w, align 4, !tbaa !2
  %147 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx75 = getelementptr inbounds float, float* %146, i32 %147
  %148 = load float, float* %arrayidx75, align 4, !tbaa !8
  %cmp76 = fcmp oge float %148, 0.000000e+00
  br i1 %cmp76, label %if.then77, label %if.else80

if.then77:                                        ; preds = %land.lhs.true74
  %149 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %149)
  %150 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state78 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %150, i32 0, i32 10
  %151 = load i32, i32* %i, align 4, !tbaa !6
  %call79 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state78, i32 %151)
  store i8 0, i8* %call79, align 1, !tbaa !33
  br label %if.end294

if.else80:                                        ; preds = %land.lhs.true74, %if.end67
  %152 = load float*, float** %hi.addr, align 4, !tbaa !2
  %153 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx81 = getelementptr inbounds float, float* %152, i32 %153
  %154 = load float, float* %arrayidx81, align 4, !tbaa !8
  %cmp82 = fcmp oeq float %154, 0.000000e+00
  br i1 %cmp82, label %land.lhs.true83, label %if.else89

land.lhs.true83:                                  ; preds = %if.else80
  %155 = load float*, float** %w, align 4, !tbaa !2
  %156 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx84 = getelementptr inbounds float, float* %155, i32 %156
  %157 = load float, float* %arrayidx84, align 4, !tbaa !8
  %cmp85 = fcmp ole float %157, 0.000000e+00
  br i1 %cmp85, label %if.then86, label %if.else89

if.then86:                                        ; preds = %land.lhs.true83
  %158 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %158)
  %159 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state87 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %159, i32 0, i32 10
  %160 = load i32, i32* %i, align 4, !tbaa !6
  %call88 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state87, i32 %160)
  store i8 1, i8* %call88, align 1, !tbaa !33
  br label %if.end293

if.else89:                                        ; preds = %land.lhs.true83, %if.else80
  %161 = load float*, float** %w, align 4, !tbaa !2
  %162 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx90 = getelementptr inbounds float, float* %161, i32 %162
  %163 = load float, float* %arrayidx90, align 4, !tbaa !8
  %cmp91 = fcmp oeq float %163, 0.000000e+00
  br i1 %cmp91, label %if.then92, label %if.else95

if.then92:                                        ; preds = %if.else89
  %164 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x93 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %164, i32 0, i32 4
  %call94 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x93, i32 0)
  %165 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %lcp, float* %call94, i32 %165, i32 0, i32 1)
  %166 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %lcp, i32 %166)
  br label %if.end292

if.else95:                                        ; preds = %if.else89
  br label %for.cond96

for.cond96:                                       ; preds = %cleanup.cont290, %if.else95
  %167 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #7
  %168 = bitcast float* %dirf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #7
  %169 = load float*, float** %w, align 4, !tbaa !2
  %170 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx97 = getelementptr inbounds float, float* %169, i32 %170
  %171 = load float, float* %arrayidx97, align 4, !tbaa !8
  %cmp98 = fcmp ole float %171, 0.000000e+00
  br i1 %cmp98, label %if.then99, label %if.else100

if.then99:                                        ; preds = %for.cond96
  store i32 1, i32* %dir, align 4, !tbaa !6
  store float 1.000000e+00, float* %dirf, align 4, !tbaa !8
  br label %if.end101

if.else100:                                       ; preds = %for.cond96
  store i32 -1, i32* %dir, align 4, !tbaa !6
  store float -1.000000e+00, float* %dirf, align 4, !tbaa !8
  br label %if.end101

if.end101:                                        ; preds = %if.else100, %if.then99
  %172 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x102 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %172, i32 0, i32 4
  %call103 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x102, i32 0)
  %173 = load i32, i32* %i, align 4, !tbaa !6
  %174 = load i32, i32* %dir, align 4, !tbaa !6
  call void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %lcp, float* %call103, i32 %173, i32 %174, i32 0)
  %175 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w104 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %175, i32 0, i32 3
  %call105 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w104, i32 0)
  %176 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x106 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %176, i32 0, i32 4
  %call107 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x106, i32 0)
  call void @_ZN5btLCP22pN_equals_ANC_times_qCEPfS0_(%struct.btLCP* %lcp, float* %call105, float* %call107)
  %177 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w108 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %177, i32 0, i32 3
  %call109 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w108, i32 0)
  %178 = load i32, i32* %i, align 4, !tbaa !6
  %179 = load i32, i32* %dir, align 4, !tbaa !6
  call void @_ZN5btLCP17pN_plusequals_ANiEPfii(%struct.btLCP* %lcp, float* %call109, i32 %178, i32 %179)
  %180 = load i32, i32* %i, align 4, !tbaa !6
  %181 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x110 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %181, i32 0, i32 4
  %call111 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x110, i32 0)
  %call112 = call float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %lcp, i32 %180, float* %call111)
  %182 = load i32, i32* %i, align 4, !tbaa !6
  %call113 = call float @_ZNK5btLCP3AiiEi(%struct.btLCP* %lcp, i32 %182)
  %183 = load float, float* %dirf, align 4, !tbaa !8
  %mul114 = fmul float %call113, %183
  %add115 = fadd float %call112, %mul114
  %184 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w116 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %184, i32 0, i32 3
  %185 = load i32, i32* %i, align 4, !tbaa !6
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w116, i32 %185)
  store float %add115, float* %call117, align 4, !tbaa !8
  %186 = bitcast i32* %cmd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #7
  store i32 1, i32* %cmd, align 4, !tbaa !6
  %187 = bitcast i32* %si to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #7
  store i32 0, i32* %si, align 4, !tbaa !6
  %188 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %188) #7
  %189 = load float*, float** %w, align 4, !tbaa !2
  %190 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx118 = getelementptr inbounds float, float* %189, i32 %190
  %191 = load float, float* %arrayidx118, align 4, !tbaa !8
  %fneg119 = fneg float %191
  %192 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w120 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %192, i32 0, i32 3
  %193 = load i32, i32* %i, align 4, !tbaa !6
  %call121 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w120, i32 %193)
  %194 = load float, float* %call121, align 4, !tbaa !8
  %div = fdiv float %fneg119, %194
  store float %div, float* %s, align 4, !tbaa !8
  %195 = load i32, i32* %dir, align 4, !tbaa !6
  %cmp122 = icmp sgt i32 %195, 0
  br i1 %cmp122, label %if.then123, label %if.else135

if.then123:                                       ; preds = %if.end101
  %196 = load float*, float** %hi.addr, align 4, !tbaa !2
  %197 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx124 = getelementptr inbounds float, float* %196, i32 %197
  %198 = load float, float* %arrayidx124, align 4, !tbaa !8
  %199 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %cmp125 = fcmp olt float %198, %199
  br i1 %cmp125, label %if.then126, label %if.end134

if.then126:                                       ; preds = %if.then123
  %200 = bitcast float* %s2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #7
  %201 = load float*, float** %hi.addr, align 4, !tbaa !2
  %202 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx127 = getelementptr inbounds float, float* %201, i32 %202
  %203 = load float, float* %arrayidx127, align 4, !tbaa !8
  %204 = load float*, float** %x.addr, align 4, !tbaa !2
  %205 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx128 = getelementptr inbounds float, float* %204, i32 %205
  %206 = load float, float* %arrayidx128, align 4, !tbaa !8
  %sub129 = fsub float %203, %206
  %207 = load float, float* %dirf, align 4, !tbaa !8
  %mul130 = fmul float %sub129, %207
  store float %mul130, float* %s2, align 4, !tbaa !8
  %208 = load float, float* %s2, align 4, !tbaa !8
  %209 = load float, float* %s, align 4, !tbaa !8
  %cmp131 = fcmp olt float %208, %209
  br i1 %cmp131, label %if.then132, label %if.end133

if.then132:                                       ; preds = %if.then126
  %210 = load float, float* %s2, align 4, !tbaa !8
  store float %210, float* %s, align 4, !tbaa !8
  store i32 3, i32* %cmd, align 4, !tbaa !6
  br label %if.end133

if.end133:                                        ; preds = %if.then132, %if.then126
  %211 = bitcast float* %s2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #7
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %if.then123
  br label %if.end149

if.else135:                                       ; preds = %if.end101
  %212 = load float*, float** %lo.addr, align 4, !tbaa !2
  %213 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx136 = getelementptr inbounds float, float* %212, i32 %213
  %214 = load float, float* %arrayidx136, align 4, !tbaa !8
  %215 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %fneg137 = fneg float %215
  %cmp138 = fcmp ogt float %214, %fneg137
  br i1 %cmp138, label %if.then139, label %if.end148

if.then139:                                       ; preds = %if.else135
  %216 = bitcast float* %s2140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #7
  %217 = load float*, float** %lo.addr, align 4, !tbaa !2
  %218 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx141 = getelementptr inbounds float, float* %217, i32 %218
  %219 = load float, float* %arrayidx141, align 4, !tbaa !8
  %220 = load float*, float** %x.addr, align 4, !tbaa !2
  %221 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx142 = getelementptr inbounds float, float* %220, i32 %221
  %222 = load float, float* %arrayidx142, align 4, !tbaa !8
  %sub143 = fsub float %219, %222
  %223 = load float, float* %dirf, align 4, !tbaa !8
  %mul144 = fmul float %sub143, %223
  store float %mul144, float* %s2140, align 4, !tbaa !8
  %224 = load float, float* %s2140, align 4, !tbaa !8
  %225 = load float, float* %s, align 4, !tbaa !8
  %cmp145 = fcmp olt float %224, %225
  br i1 %cmp145, label %if.then146, label %if.end147

if.then146:                                       ; preds = %if.then139
  %226 = load float, float* %s2140, align 4, !tbaa !8
  store float %226, float* %s, align 4, !tbaa !8
  store i32 2, i32* %cmd, align 4, !tbaa !6
  br label %if.end147

if.end147:                                        ; preds = %if.then146, %if.then139
  %227 = bitcast float* %s2140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #7
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.else135
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.end134
  %228 = bitcast i32* %numN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #7
  %call150 = call i32 @_ZNK5btLCP4numNEv(%struct.btLCP* %lcp)
  store i32 %call150, i32* %numN, align 4, !tbaa !6
  %229 = bitcast i32* %k151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #7
  store i32 0, i32* %k151, align 4, !tbaa !6
  br label %for.cond152

for.cond152:                                      ; preds = %for.inc184, %if.end149
  %230 = load i32, i32* %k151, align 4, !tbaa !6
  %231 = load i32, i32* %numN, align 4, !tbaa !6
  %cmp153 = icmp slt i32 %230, %231
  br i1 %cmp153, label %for.body155, label %for.cond.cleanup154

for.cond.cleanup154:                              ; preds = %for.cond152
  store i32 13, i32* %cleanup.dest.slot, align 4
  %232 = bitcast i32* %k151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #7
  br label %for.end187

for.body155:                                      ; preds = %for.cond152
  %233 = bitcast i32* %indexN_k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %233) #7
  %234 = load i32, i32* %k151, align 4, !tbaa !6
  %call156 = call i32 @_ZNK5btLCP6indexNEi(%struct.btLCP* %lcp, i32 %234)
  store i32 %call156, i32* %indexN_k, align 4, !tbaa !6
  %235 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state157 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %235, i32 0, i32 10
  %236 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %call158 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state157, i32 %236)
  %237 = load i8, i8* %call158, align 1, !tbaa !33, !range !35
  %tobool159 = trunc i8 %237 to i1
  br i1 %tobool159, label %cond.false, label %cond.true

cond.true:                                        ; preds = %for.body155
  %238 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w160 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %238, i32 0, i32 3
  %239 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w160, i32 %239)
  %240 = load float, float* %call161, align 4, !tbaa !8
  %cmp162 = fcmp olt float %240, 0.000000e+00
  br i1 %cmp162, label %if.then166, label %if.end183

cond.false:                                       ; preds = %for.body155
  %241 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w163 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %241, i32 0, i32 3
  %242 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %call164 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w163, i32 %242)
  %243 = load float, float* %call164, align 4, !tbaa !8
  %cmp165 = fcmp ogt float %243, 0.000000e+00
  br i1 %cmp165, label %if.then166, label %if.end183

if.then166:                                       ; preds = %cond.false, %cond.true
  %244 = load float*, float** %lo.addr, align 4, !tbaa !2
  %245 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %arrayidx167 = getelementptr inbounds float, float* %244, i32 %245
  %246 = load float, float* %arrayidx167, align 4, !tbaa !8
  %cmp168 = fcmp oeq float %246, 0.000000e+00
  br i1 %cmp168, label %land.lhs.true169, label %if.end173

land.lhs.true169:                                 ; preds = %if.then166
  %247 = load float*, float** %hi.addr, align 4, !tbaa !2
  %248 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %arrayidx170 = getelementptr inbounds float, float* %247, i32 %248
  %249 = load float, float* %arrayidx170, align 4, !tbaa !8
  %cmp171 = fcmp oeq float %249, 0.000000e+00
  br i1 %cmp171, label %if.then172, label %if.end173

if.then172:                                       ; preds = %land.lhs.true169
  store i32 15, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end173:                                        ; preds = %land.lhs.true169, %if.then166
  %250 = bitcast float* %s2174 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %250) #7
  %251 = load float*, float** %w, align 4, !tbaa !2
  %252 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %arrayidx175 = getelementptr inbounds float, float* %251, i32 %252
  %253 = load float, float* %arrayidx175, align 4, !tbaa !8
  %fneg176 = fneg float %253
  %254 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w177 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %254, i32 0, i32 3
  %255 = load i32, i32* %indexN_k, align 4, !tbaa !6
  %call178 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w177, i32 %255)
  %256 = load float, float* %call178, align 4, !tbaa !8
  %div179 = fdiv float %fneg176, %256
  store float %div179, float* %s2174, align 4, !tbaa !8
  %257 = load float, float* %s2174, align 4, !tbaa !8
  %258 = load float, float* %s, align 4, !tbaa !8
  %cmp180 = fcmp olt float %257, %258
  br i1 %cmp180, label %if.then181, label %if.end182

if.then181:                                       ; preds = %if.end173
  %259 = load float, float* %s2174, align 4, !tbaa !8
  store float %259, float* %s, align 4, !tbaa !8
  store i32 4, i32* %cmd, align 4, !tbaa !6
  %260 = load i32, i32* %indexN_k, align 4, !tbaa !6
  store i32 %260, i32* %si, align 4, !tbaa !6
  br label %if.end182

if.end182:                                        ; preds = %if.then181, %if.end173
  %261 = bitcast float* %s2174 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #7
  br label %if.end183

if.end183:                                        ; preds = %if.end182, %cond.false, %cond.true
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end183, %if.then172
  %262 = bitcast i32* %indexN_k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 15, label %for.inc184
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc184

for.inc184:                                       ; preds = %cleanup.cont, %cleanup
  %263 = load i32, i32* %k151, align 4, !tbaa !6
  %inc185 = add nsw i32 %263, 1
  store i32 %inc185, i32* %k151, align 4, !tbaa !6
  br label %for.cond152

for.end187:                                       ; preds = %for.cond.cleanup154
  %264 = bitcast i32* %numN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #7
  %265 = bitcast i32* %numC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %265) #7
  %call188 = call i32 @_ZNK5btLCP4numCEv(%struct.btLCP* %lcp)
  store i32 %call188, i32* %numC, align 4, !tbaa !6
  %266 = bitcast i32* %k189 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %266) #7
  %267 = load i32, i32* %adj_nub, align 4, !tbaa !6
  store i32 %267, i32* %k189, align 4, !tbaa !6
  br label %for.cond190

for.cond190:                                      ; preds = %for.inc232, %for.end187
  %268 = load i32, i32* %k189, align 4, !tbaa !6
  %269 = load i32, i32* %numC, align 4, !tbaa !6
  %cmp191 = icmp slt i32 %268, %269
  br i1 %cmp191, label %for.body193, label %for.cond.cleanup192

for.cond.cleanup192:                              ; preds = %for.cond190
  store i32 16, i32* %cleanup.dest.slot, align 4
  %270 = bitcast i32* %k189 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #7
  br label %for.end235

for.body193:                                      ; preds = %for.cond190
  %271 = bitcast i32* %indexC_k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %271) #7
  %272 = load i32, i32* %k189, align 4, !tbaa !6
  %call194 = call i32 @_ZNK5btLCP6indexCEi(%struct.btLCP* %lcp, i32 %272)
  store i32 %call194, i32* %indexC_k, align 4, !tbaa !6
  %273 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x195 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %273, i32 0, i32 4
  %274 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %call196 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x195, i32 %274)
  %275 = load float, float* %call196, align 4, !tbaa !8
  %cmp197 = fcmp olt float %275, 0.000000e+00
  br i1 %cmp197, label %land.lhs.true198, label %if.end213

land.lhs.true198:                                 ; preds = %for.body193
  %276 = load float*, float** %lo.addr, align 4, !tbaa !2
  %277 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx199 = getelementptr inbounds float, float* %276, i32 %277
  %278 = load float, float* %arrayidx199, align 4, !tbaa !8
  %279 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %fneg200 = fneg float %279
  %cmp201 = fcmp ogt float %278, %fneg200
  br i1 %cmp201, label %if.then202, label %if.end213

if.then202:                                       ; preds = %land.lhs.true198
  %280 = bitcast float* %s2203 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %280) #7
  %281 = load float*, float** %lo.addr, align 4, !tbaa !2
  %282 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx204 = getelementptr inbounds float, float* %281, i32 %282
  %283 = load float, float* %arrayidx204, align 4, !tbaa !8
  %284 = load float*, float** %x.addr, align 4, !tbaa !2
  %285 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx205 = getelementptr inbounds float, float* %284, i32 %285
  %286 = load float, float* %arrayidx205, align 4, !tbaa !8
  %sub206 = fsub float %283, %286
  %287 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x207 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %287, i32 0, i32 4
  %288 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %call208 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x207, i32 %288)
  %289 = load float, float* %call208, align 4, !tbaa !8
  %div209 = fdiv float %sub206, %289
  store float %div209, float* %s2203, align 4, !tbaa !8
  %290 = load float, float* %s2203, align 4, !tbaa !8
  %291 = load float, float* %s, align 4, !tbaa !8
  %cmp210 = fcmp olt float %290, %291
  br i1 %cmp210, label %if.then211, label %if.end212

if.then211:                                       ; preds = %if.then202
  %292 = load float, float* %s2203, align 4, !tbaa !8
  store float %292, float* %s, align 4, !tbaa !8
  store i32 5, i32* %cmd, align 4, !tbaa !6
  %293 = load i32, i32* %indexC_k, align 4, !tbaa !6
  store i32 %293, i32* %si, align 4, !tbaa !6
  br label %if.end212

if.end212:                                        ; preds = %if.then211, %if.then202
  %294 = bitcast float* %s2203 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #7
  br label %if.end213

if.end213:                                        ; preds = %if.end212, %land.lhs.true198, %for.body193
  %295 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x214 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %295, i32 0, i32 4
  %296 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %call215 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x214, i32 %296)
  %297 = load float, float* %call215, align 4, !tbaa !8
  %cmp216 = fcmp ogt float %297, 0.000000e+00
  br i1 %cmp216, label %land.lhs.true217, label %if.end231

land.lhs.true217:                                 ; preds = %if.end213
  %298 = load float*, float** %hi.addr, align 4, !tbaa !2
  %299 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx218 = getelementptr inbounds float, float* %298, i32 %299
  %300 = load float, float* %arrayidx218, align 4, !tbaa !8
  %301 = load float, float* bitcast (i32* @_ZL14btInfinityMask to float*), align 4, !tbaa !8
  %cmp219 = fcmp olt float %300, %301
  br i1 %cmp219, label %if.then220, label %if.end231

if.then220:                                       ; preds = %land.lhs.true217
  %302 = bitcast float* %s2221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #7
  %303 = load float*, float** %hi.addr, align 4, !tbaa !2
  %304 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx222 = getelementptr inbounds float, float* %303, i32 %304
  %305 = load float, float* %arrayidx222, align 4, !tbaa !8
  %306 = load float*, float** %x.addr, align 4, !tbaa !2
  %307 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %arrayidx223 = getelementptr inbounds float, float* %306, i32 %307
  %308 = load float, float* %arrayidx223, align 4, !tbaa !8
  %sub224 = fsub float %305, %308
  %309 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x225 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %309, i32 0, i32 4
  %310 = load i32, i32* %indexC_k, align 4, !tbaa !6
  %call226 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x225, i32 %310)
  %311 = load float, float* %call226, align 4, !tbaa !8
  %div227 = fdiv float %sub224, %311
  store float %div227, float* %s2221, align 4, !tbaa !8
  %312 = load float, float* %s2221, align 4, !tbaa !8
  %313 = load float, float* %s, align 4, !tbaa !8
  %cmp228 = fcmp olt float %312, %313
  br i1 %cmp228, label %if.then229, label %if.end230

if.then229:                                       ; preds = %if.then220
  %314 = load float, float* %s2221, align 4, !tbaa !8
  store float %314, float* %s, align 4, !tbaa !8
  store i32 6, i32* %cmd, align 4, !tbaa !6
  %315 = load i32, i32* %indexC_k, align 4, !tbaa !6
  store i32 %315, i32* %si, align 4, !tbaa !6
  br label %if.end230

if.end230:                                        ; preds = %if.then229, %if.then220
  %316 = bitcast float* %s2221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #7
  br label %if.end231

if.end231:                                        ; preds = %if.end230, %land.lhs.true217, %if.end213
  %317 = bitcast i32* %indexC_k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #7
  br label %for.inc232

for.inc232:                                       ; preds = %if.end231
  %318 = load i32, i32* %k189, align 4, !tbaa !6
  %inc233 = add nsw i32 %318, 1
  store i32 %inc233, i32* %k189, align 4, !tbaa !6
  br label %for.cond190

for.end235:                                       ; preds = %for.cond.cleanup192
  %319 = bitcast i32* %numC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #7
  %320 = load float, float* %s, align 4, !tbaa !8
  %cmp236 = fcmp ole float %320, 0.000000e+00
  br i1 %cmp236, label %if.then237, label %if.end244

if.then237:                                       ; preds = %for.end235
  %321 = load i32, i32* %i, align 4, !tbaa !6
  %322 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp238 = icmp slt i32 %321, %322
  br i1 %cmp238, label %if.then239, label %if.end243

if.then239:                                       ; preds = %if.then237
  %323 = load float*, float** %x.addr, align 4, !tbaa !2
  %324 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %323, i32 %324
  %325 = load i32, i32* %n.addr, align 4, !tbaa !6
  %326 = load i32, i32* %i, align 4, !tbaa !6
  %sub240 = sub nsw i32 %325, %326
  call void @_Z9btSetZeroIfEvPT_i(float* %add.ptr, i32 %sub240)
  %327 = load float*, float** %w, align 4, !tbaa !2
  %328 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr241 = getelementptr inbounds float, float* %327, i32 %328
  %329 = load i32, i32* %n.addr, align 4, !tbaa !6
  %330 = load i32, i32* %i, align 4, !tbaa !6
  %sub242 = sub nsw i32 %329, %330
  call void @_Z9btSetZeroIfEvPT_i(float* %add.ptr241, i32 %sub242)
  br label %if.end243

if.end243:                                        ; preds = %if.then239, %if.then237
  store i8 1, i8* @s_error, align 1, !tbaa !33
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup284

if.end244:                                        ; preds = %for.end235
  %331 = load float*, float** %x.addr, align 4, !tbaa !2
  %332 = load float, float* %s, align 4, !tbaa !8
  %333 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_x245 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %333, i32 0, i32 4
  %call246 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x245, i32 0)
  call void @_ZN5btLCP24pC_plusequals_s_times_qCEPffS0_(%struct.btLCP* %lcp, float* %331, float %332, float* %call246)
  %334 = load float, float* %s, align 4, !tbaa !8
  %335 = load float, float* %dirf, align 4, !tbaa !8
  %mul247 = fmul float %334, %335
  %336 = load float*, float** %x.addr, align 4, !tbaa !2
  %337 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx248 = getelementptr inbounds float, float* %336, i32 %337
  %338 = load float, float* %arrayidx248, align 4, !tbaa !8
  %add249 = fadd float %338, %mul247
  store float %add249, float* %arrayidx248, align 4, !tbaa !8
  %339 = load float*, float** %w, align 4, !tbaa !2
  %340 = load float, float* %s, align 4, !tbaa !8
  %341 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w250 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %341, i32 0, i32 3
  %call251 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w250, i32 0)
  call void @_ZN5btLCP24pN_plusequals_s_times_qNEPffS0_(%struct.btLCP* %lcp, float* %339, float %340, float* %call251)
  %342 = load float, float* %s, align 4, !tbaa !8
  %343 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %delta_w252 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %343, i32 0, i32 3
  %344 = load i32, i32* %i, align 4, !tbaa !6
  %call253 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w252, i32 %344)
  %345 = load float, float* %call253, align 4, !tbaa !8
  %mul254 = fmul float %342, %345
  %346 = load float*, float** %w, align 4, !tbaa !2
  %347 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx255 = getelementptr inbounds float, float* %346, i32 %347
  %348 = load float, float* %arrayidx255, align 4, !tbaa !8
  %add256 = fadd float %348, %mul254
  store float %add256, float* %arrayidx255, align 4, !tbaa !8
  %349 = load i32, i32* %cmd, align 4, !tbaa !6
  switch i32 %349, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb258
    i32 3, label %sw.bb263
    i32 4, label %sw.bb268
    i32 5, label %sw.bb270
    i32 6, label %sw.bb275
  ]

sw.bb:                                            ; preds = %if.end244
  %350 = load float*, float** %w, align 4, !tbaa !2
  %351 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx257 = getelementptr inbounds float, float* %350, i32 %351
  store float 0.000000e+00, float* %arrayidx257, align 4, !tbaa !8
  %352 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %lcp, i32 %352)
  br label %sw.epilog

sw.bb258:                                         ; preds = %if.end244
  %353 = load float*, float** %lo.addr, align 4, !tbaa !2
  %354 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx259 = getelementptr inbounds float, float* %353, i32 %354
  %355 = load float, float* %arrayidx259, align 4, !tbaa !8
  %356 = load float*, float** %x.addr, align 4, !tbaa !2
  %357 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx260 = getelementptr inbounds float, float* %356, i32 %357
  store float %355, float* %arrayidx260, align 4, !tbaa !8
  %358 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state261 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %358, i32 0, i32 10
  %359 = load i32, i32* %i, align 4, !tbaa !6
  %call262 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state261, i32 %359)
  store i8 0, i8* %call262, align 1, !tbaa !33
  %360 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %360)
  br label %sw.epilog

sw.bb263:                                         ; preds = %if.end244
  %361 = load float*, float** %hi.addr, align 4, !tbaa !2
  %362 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx264 = getelementptr inbounds float, float* %361, i32 %362
  %363 = load float, float* %arrayidx264, align 4, !tbaa !8
  %364 = load float*, float** %x.addr, align 4, !tbaa !2
  %365 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx265 = getelementptr inbounds float, float* %364, i32 %365
  store float %363, float* %arrayidx265, align 4, !tbaa !8
  %366 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state266 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %366, i32 0, i32 10
  %367 = load i32, i32* %i, align 4, !tbaa !6
  %call267 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state266, i32 %367)
  store i8 1, i8* %call267, align 1, !tbaa !33
  %368 = load i32, i32* %i, align 4, !tbaa !6
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %368)
  br label %sw.epilog

sw.bb268:                                         ; preds = %if.end244
  %369 = load float*, float** %w, align 4, !tbaa !2
  %370 = load i32, i32* %si, align 4, !tbaa !6
  %arrayidx269 = getelementptr inbounds float, float* %369, i32 %370
  store float 0.000000e+00, float* %arrayidx269, align 4, !tbaa !8
  %371 = load i32, i32* %si, align 4, !tbaa !6
  call void @_ZN5btLCP22transfer_i_from_N_to_CEi(%struct.btLCP* %lcp, i32 %371)
  br label %sw.epilog

sw.bb270:                                         ; preds = %if.end244
  %372 = load float*, float** %lo.addr, align 4, !tbaa !2
  %373 = load i32, i32* %si, align 4, !tbaa !6
  %arrayidx271 = getelementptr inbounds float, float* %372, i32 %373
  %374 = load float, float* %arrayidx271, align 4, !tbaa !8
  %375 = load float*, float** %x.addr, align 4, !tbaa !2
  %376 = load i32, i32* %si, align 4, !tbaa !6
  %arrayidx272 = getelementptr inbounds float, float* %375, i32 %376
  store float %374, float* %arrayidx272, align 4, !tbaa !8
  %377 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state273 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %377, i32 0, i32 10
  %378 = load i32, i32* %si, align 4, !tbaa !6
  %call274 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state273, i32 %378)
  store i8 0, i8* %call274, align 1, !tbaa !33
  %379 = load i32, i32* %si, align 4, !tbaa !6
  %380 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %m_scratch = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %380, i32 0, i32 0
  call void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %lcp, i32 %379, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_scratch)
  br label %sw.epilog

sw.bb275:                                         ; preds = %if.end244
  %381 = load float*, float** %hi.addr, align 4, !tbaa !2
  %382 = load i32, i32* %si, align 4, !tbaa !6
  %arrayidx276 = getelementptr inbounds float, float* %381, i32 %382
  %383 = load float, float* %arrayidx276, align 4, !tbaa !8
  %384 = load float*, float** %x.addr, align 4, !tbaa !2
  %385 = load i32, i32* %si, align 4, !tbaa !6
  %arrayidx277 = getelementptr inbounds float, float* %384, i32 %385
  store float %383, float* %arrayidx277, align 4, !tbaa !8
  %386 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %state278 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %386, i32 0, i32 10
  %387 = load i32, i32* %si, align 4, !tbaa !6
  %call279 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state278, i32 %387)
  store i8 1, i8* %call279, align 1, !tbaa !33
  %388 = load i32, i32* %si, align 4, !tbaa !6
  %389 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4, !tbaa !2
  %m_scratch280 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %389, i32 0, i32 0
  call void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %lcp, i32 %388, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_scratch280)
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end244, %sw.bb275, %sw.bb270, %sw.bb268, %sw.bb263, %sw.bb258, %sw.bb
  %390 = load i32, i32* %cmd, align 4, !tbaa !6
  %cmp281 = icmp sle i32 %390, 3
  br i1 %cmp281, label %if.then282, label %if.end283

if.then282:                                       ; preds = %sw.epilog
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup284

if.end283:                                        ; preds = %sw.epilog
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup284

cleanup284:                                       ; preds = %if.end283, %if.then282, %if.end243
  %391 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #7
  %392 = bitcast i32* %si to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #7
  %393 = bitcast i32* %cmd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #7
  %394 = bitcast float* %dirf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #7
  %395 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %395) #7
  %cleanup.dest289 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest289, label %unreachable [
    i32 0, label %cleanup.cont290
    i32 11, label %for.end291
  ]

cleanup.cont290:                                  ; preds = %cleanup284
  br label %for.cond96

for.end291:                                       ; preds = %cleanup284
  br label %if.end292

if.end292:                                        ; preds = %for.end291, %if.then92
  br label %if.end293

if.end293:                                        ; preds = %if.end292, %if.then86
  br label %if.end294

if.end294:                                        ; preds = %if.end293, %if.then77
  %396 = load i8, i8* @s_error, align 1, !tbaa !33, !range !35
  %tobool295 = trunc i8 %396 to i1
  br i1 %tobool295, label %if.then296, label %if.end297

if.then296:                                       ; preds = %if.end294
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup300

if.end297:                                        ; preds = %if.end294
  br label %for.inc298

for.inc298:                                       ; preds = %if.end297
  %397 = load i32, i32* %i, align 4, !tbaa !6
  %inc299 = add nsw i32 %397, 1
  store i32 %inc299, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup300:                                       ; preds = %if.then296, %for.cond.cleanup
  %398 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %398) #7
  br label %for.end301

for.end301:                                       ; preds = %cleanup300
  call void @_ZN5btLCP9unpermuteEv(%struct.btLCP* %lcp)
  %399 = load i8, i8* @s_error, align 1, !tbaa !33, !range !35
  %tobool302 = trunc i8 %399 to i1
  %lnot303 = xor i1 %tobool302, true
  store i1 %lnot303, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hit_first_friction_index) #7
  %400 = bitcast i32* %adj_nub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %400) #7
  %401 = bitcast %struct.btLCP* %lcp to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %401) #7
  %402 = bitcast float** %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #7
  %403 = bitcast i32* %nskip1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %403) #7
  br label %return

return:                                           ; preds = %for.end301, %if.then
  %404 = load i1, i1* %retval, align 1
  ret i1 %404

unreachable:                                      ; preds = %cleanup284, %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, float** nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store float** %fillData, float*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load float**, float*** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIPfE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load float**, float*** %m_data11, align 4, !tbaa !40
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float*, float** %18, i32 %19
  %20 = bitcast float** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float**
  %22 = load float**, float*** %fillData.addr, align 4, !tbaa !2
  %23 = load float*, float** %22, align 4, !tbaa !2
  store float* %23, float** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !43
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !44
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !44
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !6
  store i32 %23, i32* %21, align 4, !tbaa !6
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !47
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.8* %this, i32 %newsize, i8* nonnull align 1 dereferenceable(1) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i8*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store i8* %fillData, i8** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load i8*, i8** %m_data, align 4, !tbaa !48
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %18 = load i8*, i8** %m_data11, align 4, !tbaa !48
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i8*, i8** %fillData.addr, align 4, !tbaa !2
  %21 = load i8, i8* %20, align 1, !tbaa !33, !range !35
  %tobool = trunc i8 %21 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx12, align 1, !tbaa !33
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %22 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %22, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %23 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %23, i32* %m_size, align 4, !tbaa !51
  %24 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4, !tbaa !48
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %1
  ret i8* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !44
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZN20btAlignedObjectArrayIPfEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load float**, float*** %m_data, align 4, !tbaa !40
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  ret float** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK5btLCP6getNubEv(%struct.btLCP* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_nub, align 4, !tbaa !13
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %this, i32 %i, float* %q) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %q.addr = alloca float*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float* %q, float** %q.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4, !tbaa !16
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4, !tbaa !2
  %3 = load float*, float** %q.addr, align 4, !tbaa !2
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_nC, align 4, !tbaa !14
  %call = call float @_Z10btLargeDotPKfS0_i(float* %2, float* %3, i32 %4)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK5btLCP12AiN_times_qNEiPf(%struct.btLCP* %this, i32 %i, float* %q) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %q.addr = alloca float*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store float* %q, float** %q.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4, !tbaa !16
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4, !tbaa !2
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %3 = load i32, i32* %m_nC, align 4, !tbaa !14
  %add.ptr = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float*, float** %q.addr, align 4, !tbaa !2
  %m_nC2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %5 = load i32, i32* %m_nC2, align 4, !tbaa !14
  %add.ptr3 = getelementptr inbounds float, float* %4, i32 %5
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %6 = load i32, i32* %m_nN, align 4, !tbaa !15
  %call = call float @_Z10btLargeDotPKfS0_i(float* %add.ptr, float* %add.ptr3, i32 %6)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_nN, align 4, !tbaa !15
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_nN, align 4, !tbaa !15
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK5btLCP3AiiEi(%struct.btLCP* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4, !tbaa !16
  %1 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4, !tbaa !2
  %3 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  ret float %4
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK5btLCP4numNEv(%struct.btLCP* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_nN, align 4, !tbaa !15
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK5btLCP6indexNEi(%struct.btLCP* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4, !tbaa !14
  %add = add nsw i32 %0, %1
  ret i32 %add
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK5btLCP4numCEv(%struct.btLCP* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4, !tbaa !14
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK5btLCP6indexCEi(%struct.btLCP* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  ret i32 %0
}

; Function Attrs: nounwind
define internal void @_ZL17btSwapRowsAndColsPPfiiiii(float** %A, i32 %n, i32 %i1, i32 %i2, i32 %nskip, i32 %do_fast_row_swaps) #2 {
entry:
  %A.addr = alloca float**, align 4
  %n.addr = alloca i32, align 4
  %i1.addr = alloca i32, align 4
  %i2.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %do_fast_row_swaps.addr = alloca i32, align 4
  %A_i1 = alloca float*, align 4
  %A_i2 = alloca float*, align 4
  %i = alloca i32, align 4
  %A_i_i1 = alloca float*, align 4
  %k = alloca i32, align 4
  %tmp = alloca float, align 4
  %j = alloca i32, align 4
  %A_j = alloca float*, align 4
  %tmp30 = alloca float, align 4
  store float** %A, float*** %A.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32 %i1, i32* %i1.addr, align 4, !tbaa !6
  store i32 %i2, i32* %i2.addr, align 4, !tbaa !6
  store i32 %nskip, i32* %nskip.addr, align 4, !tbaa !6
  store i32 %do_fast_row_swaps, i32* %do_fast_row_swaps.addr, align 4, !tbaa !6
  %0 = bitcast float** %A_i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load float**, float*** %A.addr, align 4, !tbaa !2
  %2 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %1, i32 %2
  %3 = load float*, float** %arrayidx, align 4, !tbaa !2
  store float* %3, float** %A_i1, align 4, !tbaa !2
  %4 = bitcast float** %A_i2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load float**, float*** %A.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds float*, float** %5, i32 %6
  %7 = load float*, float** %arrayidx1, align 4, !tbaa !2
  store float* %7, float** %A_i2, align 4, !tbaa !2
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %add = add nsw i32 %9, 1
  store i32 %add, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast float** %A_i_i1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load float**, float*** %A.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float*, float** %14, i32 %15
  %16 = load float*, float** %arrayidx2, align 4, !tbaa !2
  %17 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds float, float* %16, i32 %17
  store float* %add.ptr, float** %A_i_i1, align 4, !tbaa !2
  %18 = load float*, float** %A_i_i1, align 4, !tbaa !2
  %19 = load float, float* %18, align 4, !tbaa !8
  %20 = load float*, float** %A_i1, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds float, float* %20, i32 %21
  store float %19, float* %arrayidx3, align 4, !tbaa !8
  %22 = load float*, float** %A_i2, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx4, align 4, !tbaa !8
  %25 = load float*, float** %A_i_i1, align 4, !tbaa !2
  store float %24, float* %25, align 4, !tbaa !8
  %26 = bitcast float** %A_i_i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = load float*, float** %A_i1, align 4, !tbaa !2
  %29 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx5, align 4, !tbaa !8
  %31 = load float*, float** %A_i1, align 4, !tbaa !2
  %32 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds float, float* %31, i32 %32
  store float %30, float* %arrayidx6, align 4, !tbaa !8
  %33 = load float*, float** %A_i2, align 4, !tbaa !2
  %34 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx7, align 4, !tbaa !8
  %36 = load float*, float** %A_i1, align 4, !tbaa !2
  %37 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds float, float* %36, i32 %37
  store float %35, float* %arrayidx8, align 4, !tbaa !8
  %38 = load float*, float** %A_i2, align 4, !tbaa !2
  %39 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds float, float* %38, i32 %39
  %40 = load float, float* %arrayidx9, align 4, !tbaa !8
  %41 = load float*, float** %A_i2, align 4, !tbaa !2
  %42 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds float, float* %41, i32 %42
  store float %40, float* %arrayidx10, align 4, !tbaa !8
  %43 = load i32, i32* %do_fast_row_swaps.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %43, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.end
  %44 = load float*, float** %A_i2, align 4, !tbaa !2
  %45 = load float**, float*** %A.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds float*, float** %45, i32 %46
  store float* %44, float** %arrayidx11, align 4, !tbaa !2
  %47 = load float*, float** %A_i1, align 4, !tbaa !2
  %48 = load float**, float*** %A.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds float*, float** %48, i32 %49
  store float* %47, float** %arrayidx12, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.end
  %50 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #7
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc21, %if.else
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %52 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %cmp14 = icmp sle i32 %51, %52
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond13
  %53 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #7
  br label %for.end23

for.body16:                                       ; preds = %for.cond13
  %54 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #7
  %55 = load float*, float** %A_i1, align 4, !tbaa !2
  %56 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx17, align 4, !tbaa !8
  store float %57, float* %tmp, align 4, !tbaa !8
  %58 = load float*, float** %A_i2, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx18, align 4, !tbaa !8
  %61 = load float*, float** %A_i1, align 4, !tbaa !2
  %62 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds float, float* %61, i32 %62
  store float %60, float* %arrayidx19, align 4, !tbaa !8
  %63 = load float, float* %tmp, align 4, !tbaa !8
  %64 = load float*, float** %A_i2, align 4, !tbaa !2
  %65 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds float, float* %64, i32 %65
  store float %63, float* %arrayidx20, align 4, !tbaa !8
  %66 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  br label %for.inc21

for.inc21:                                        ; preds = %for.body16
  %67 = load i32, i32* %k, align 4, !tbaa !6
  %inc22 = add nsw i32 %67, 1
  store i32 %inc22, i32* %k, align 4, !tbaa !6
  br label %for.cond13

for.end23:                                        ; preds = %for.cond.cleanup15
  br label %if.end

if.end:                                           ; preds = %for.end23, %if.then
  %68 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #7
  %69 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %add24 = add nsw i32 %69, 1
  store i32 %add24, i32* %j, align 4, !tbaa !6
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc35, %if.end
  %70 = load i32, i32* %j, align 4, !tbaa !6
  %71 = load i32, i32* %n.addr, align 4, !tbaa !6
  %cmp26 = icmp slt i32 %70, %71
  br i1 %cmp26, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond25
  %72 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  br label %for.end37

for.body28:                                       ; preds = %for.cond25
  %73 = bitcast float** %A_j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #7
  %74 = load float**, float*** %A.addr, align 4, !tbaa !2
  %75 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds float*, float** %74, i32 %75
  %76 = load float*, float** %arrayidx29, align 4, !tbaa !2
  store float* %76, float** %A_j, align 4, !tbaa !2
  %77 = bitcast float* %tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #7
  %78 = load float*, float** %A_j, align 4, !tbaa !2
  %79 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx31, align 4, !tbaa !8
  store float %80, float* %tmp30, align 4, !tbaa !8
  %81 = load float*, float** %A_j, align 4, !tbaa !2
  %82 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds float, float* %81, i32 %82
  %83 = load float, float* %arrayidx32, align 4, !tbaa !8
  %84 = load float*, float** %A_j, align 4, !tbaa !2
  %85 = load i32, i32* %i1.addr, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds float, float* %84, i32 %85
  store float %83, float* %arrayidx33, align 4, !tbaa !8
  %86 = load float, float* %tmp30, align 4, !tbaa !8
  %87 = load float*, float** %A_j, align 4, !tbaa !2
  %88 = load i32, i32* %i2.addr, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds float, float* %87, i32 %88
  store float %86, float* %arrayidx34, align 4, !tbaa !8
  %89 = bitcast float* %tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #7
  %90 = bitcast float** %A_j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #7
  br label %for.inc35

for.inc35:                                        ; preds = %for.body28
  %91 = load i32, i32* %j, align 4, !tbaa !6
  %inc36 = add nsw i32 %91, 1
  store i32 %inc36, i32* %j, align 4, !tbaa !6
  br label %for.cond25

for.end37:                                        ; preds = %for.cond.cleanup27
  %92 = bitcast float** %A_i2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #7
  %93 = bitcast float** %A_i1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #7
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !52
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !36
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !53
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, float* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !8
  store float %10, float* %7, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !36
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !36
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !52, !range !35
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !36
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !36
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !43
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPfE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPfE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float**
  store float** %3, float*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load float**, float*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, float** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIPfE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPfE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !54
  %5 = load float**, float*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store float** %5, float*** %m_data, align 4, !tbaa !40
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !55
  %7 = bitcast float*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPfE8capacityEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPfE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call float** @_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, float*** null)
  %2 = bitcast float** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, float** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store float** %dest, float*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float**, float*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %4, i32 %5
  %6 = bitcast float** %arrayidx to i8*
  %7 = bitcast i8* %6 to float**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load float**, float*** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds float*, float** %8, i32 %9
  %10 = load float*, float** %arrayidx2, align 4, !tbaa !2
  store float* %10, float** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load float**, float*** %m_data, align 4, !tbaa !40
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float*, float** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE10deallocateEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load float**, float*** %m_data, align 4, !tbaa !40
  %tobool = icmp ne float** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !54, !range !35
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load float**, float*** %m_data4, align 4, !tbaa !40
  call void @_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, float** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store float** null, float*** %m_data5, align 4, !tbaa !40
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden float** @_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, float*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store float*** %hint, float**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float**
  ret float** %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, float** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca float**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store float** %ptr, float*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load float**, float*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !47
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !56
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !44
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !57
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !57
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, i32* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !44
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %10, i32* %7, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !44
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !44
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !56, !range !35
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !44
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !44
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !51
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i8** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  store i8* %call2, i8** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load i8*, i8** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, i8* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !58
  %4 = load i8*, i8** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i8* %4, i8** %m_data, align 4, !tbaa !48
  %5 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4, !tbaa !59
  %6 = bitcast i8** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !59
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.9* %m_allocator, i32 %1, i8** null)
  store i8* %call, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i8*, i8** %retval, align 4
  ret i8* %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, i8* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %6 = load i8*, i8** %m_data, align 4, !tbaa !48
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx2, align 1, !tbaa !33, !range !35
  %tobool = trunc i8 %8 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx, align 1, !tbaa !33
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_data, align 4, !tbaa !48
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4, !tbaa !48
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !58, !range !35
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_data4, align 4, !tbaa !48
  call void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.9* %m_allocator, i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i8* null, i8** %m_data5, align 4, !tbaa !48
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.9* %this, i32 %n, i8** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i8**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i8** %hint, i8*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 1, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  ret i8* %call
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.9* %this, i8* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !7, i64 0}
!11 = !{!"_ZTS5btLCP", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60, !3, i64 64, !3, i64 68, !3, i64 72, !3, i64 76}
!12 = !{!11, !7, i64 4}
!13 = !{!11, !7, i64 8}
!14 = !{!11, !7, i64 12}
!15 = !{!11, !7, i64 16}
!16 = !{!11, !3, i64 20}
!17 = !{!11, !3, i64 24}
!18 = !{!11, !3, i64 28}
!19 = !{!11, !3, i64 32}
!20 = !{!11, !3, i64 36}
!21 = !{!11, !3, i64 40}
!22 = !{!11, !3, i64 44}
!23 = !{!11, !3, i64 48}
!24 = !{!11, !3, i64 52}
!25 = !{!11, !3, i64 56}
!26 = !{!11, !3, i64 60}
!27 = !{!11, !3, i64 64}
!28 = !{!11, !3, i64 68}
!29 = !{!11, !3, i64 72}
!30 = !{!11, !3, i64 76}
!31 = !{!32, !32, i64 0}
!32 = !{!"long", !4, i64 0}
!33 = !{!34, !34, i64 0}
!34 = !{!"bool", !4, i64 0}
!35 = !{i8 0, i8 2}
!36 = !{!37, !3, i64 12}
!37 = !{!"_ZTS20btAlignedObjectArrayIfE", !38, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !34, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!39 = !{!37, !7, i64 4}
!40 = !{!41, !3, i64 12}
!41 = !{!"_ZTS20btAlignedObjectArrayIPfE", !42, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !34, i64 16}
!42 = !{!"_ZTS18btAlignedAllocatorIPfLj16EE"}
!43 = !{!41, !7, i64 4}
!44 = !{!45, !3, i64 12}
!45 = !{!"_ZTS20btAlignedObjectArrayIiE", !46, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !34, i64 16}
!46 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!47 = !{!45, !7, i64 4}
!48 = !{!49, !3, i64 12}
!49 = !{!"_ZTS20btAlignedObjectArrayIbE", !50, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !34, i64 16}
!50 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!51 = !{!49, !7, i64 4}
!52 = !{!37, !34, i64 16}
!53 = !{!37, !7, i64 8}
!54 = !{!41, !34, i64 16}
!55 = !{!41, !7, i64 8}
!56 = !{!45, !34, i64 16}
!57 = !{!45, !7, i64 8}
!58 = !{!49, !34, i64 16}
!59 = !{!49, !7, i64 8}
