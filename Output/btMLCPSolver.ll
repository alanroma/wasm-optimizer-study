; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/MLCPSolvers/btMLCPSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/MLCPSolvers/btMLCPSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btMatrixX = type { i32, i32, i32, i32, i32, %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22 }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, %class.btAlignedObjectArray.10*, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btMLCPSolver = type { %class.btSequentialImpulseConstraintSolver, %struct.btMatrixX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.5, %class.btMLCPSolverInterface*, i32 }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.14, i32, i32, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.3, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.3 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%struct.btVectorX = type { %class.btAlignedObjectArray.18 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.8, i32, i32, i32, i32 }
%union.anon.8 = type { i8* }
%class.btMLCPSolverInterface = type { i32 (...)** }
%class.btPersistentManifold = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%class.btIDebugDraw = type opaque
%class.CProfileSample = type { i8 }
%class.btAlignedObjectArray.26 = type <{ %class.btAlignedAllocator.27, [3 x i8], i32, i32, %struct.btJointNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.27 = type { i8 }
%struct.btJointNode = type { i32, i32, i32, i32 }
%class.btDispatcher = type opaque

$_ZN9btMatrixXIfEC2Ev = comdat any

$_ZN9btVectorXIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN9btVectorXIfED2Ev = comdat any

$_ZN9btMatrixXIfED2Ev = comdat any

$_ZN35btSequentialImpulseConstraintSolverdlEPv = comdat any

$_ZN14CProfileSampleC2EPKc = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE6resizeEiRKS0_ = comdat any

$_ZN18btSolverConstraintC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN9btMatrixXIfE6resizeEii = comdat any

$_ZN9btVectorXIfE6resizeEi = comdat any

$_ZN14CProfileSampleD2Ev = comdat any

$_ZNK9btMatrixXIfE4rowsEv = comdat any

$_ZN9btMatrixXIfEC2ERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2ERKS0_ = comdat any

$_ZN9btVectorXIfEixEi = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi = comdat any

$_ZN9btMatrixXIfE7setZeroEv = comdat any

$_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeEixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZN9btMatrixXIfE7setElemEiif = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btMatrixXIfE16getBufferPointerEv = comdat any

$_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii = comdat any

$_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii = comdat any

$_ZNK9btMatrixXIfEclEii = comdat any

$_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv = comdat any

$_ZN9btVectorXIfE7setZeroEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeED2Ev = comdat any

$_Z7setElemR9btMatrixXIfEiif = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btMatrixXIfE9transposeEv = comdat any

$_ZN9btMatrixXIfEaSEOS0_ = comdat any

$_ZN9btMatrixXIfEmlERKS0_ = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK12btMLCPSolver13getSolverTypeEv = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayIS_IiEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE4initEv = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2ERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_ = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE8allocateEi = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN20btAlignedObjectArrayIfEaSERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEC2Ev = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi = comdat any

$_ZN18btSolverConstraintnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi = comdat any

$_ZN9btMatrixXIfE15clearSparseInfoEv = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_ = comdat any

$_Z9btSetZeroIfEvPT_i = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi = comdat any

$_ZN9btMatrixXIfE7addElemEiif = comdat any

$_ZN9btMatrixXIfEC2Eii = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK9btMatrixXIfE4colsEv = comdat any

@_ZTV12btMLCPSolver = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btMLCPSolver to i8*), i8* bitcast (%class.btMLCPSolver* (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD1Ev to i8*), i8* bitcast (void (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btMLCPSolver*)* @_ZNK12btMLCPSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMLCPSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN12btMLCPSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMLCPSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN12btMLCPSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver10createMLCPERK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfo to i8*), i8* bitcast (i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver9solveMLCPERK19btContactSolverInfo to i8*)] }, align 4
@gUseMatrixMultiply = hidden global i8 0, align 1
@interleaveContactAndFriction = hidden global i8 0, align 1
@.str = private unnamed_addr constant [23 x i8] c"gather constraint data\00", align 1
@.str.1 = private unnamed_addr constant [11 x i8] c"createMLCP\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"createMLCPFast\00", align 1
@.str.3 = private unnamed_addr constant [13 x i8] c"init b (rhs)\00", align 1
@.str.4 = private unnamed_addr constant [11 x i8] c"init lo/ho\00", align 1
@.str.5 = private unnamed_addr constant [26 x i8] c"bodyJointNodeArray.resize\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"jointNodeArray.reserve\00", align 1
@_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3 = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3 = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@.str.7 = private unnamed_addr constant [10 x i8] c"J3.resize\00", align 1
@_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3 = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3 = internal global i32 0, align 4
@.str.9 = private unnamed_addr constant [22 x i8] c"JinvM3.resize/setZero\00", align 1
@_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs = internal global %class.btAlignedObjectArray.10 zeroinitializer, align 4
@_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs = internal global i32 0, align 4
@.str.11 = private unnamed_addr constant [11 x i8] c"ofs resize\00", align 1
@.str.12 = private unnamed_addr constant [20 x i8] c"Compute J and JinvM\00", align 1
@.str.13 = private unnamed_addr constant [11 x i8] c"m_A.resize\00", align 1
@.str.14 = private unnamed_addr constant [12 x i8] c"m_A.setZero\00", align 1
@.str.15 = private unnamed_addr constant [10 x i8] c"Compute A\00", align 1
@.str.16 = private unnamed_addr constant [17 x i8] c"compute diagonal\00", align 1
@.str.17 = private unnamed_addr constant [25 x i8] c"fill the upper triangle \00", align 1
@.str.18 = private unnamed_addr constant [14 x i8] c"resize/init x\00", align 1
@_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv = internal global i32 0, align 4
@_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J = internal global i32 0, align 4
@_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose = internal global i32 0, align 4
@_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp = internal global %struct.btMatrixX zeroinitializer, align 4
@_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp = internal global i32 0, align 4
@.str.23 = private unnamed_addr constant [7 x i8] c"J*Minv\00", align 1
@.str.24 = private unnamed_addr constant [6 x i8] c"J*tmp\00", align 1
@.str.25 = private unnamed_addr constant [10 x i8] c"solveMLCP\00", align 1
@.str.26 = private unnamed_addr constant [21 x i8] c"process MLCP results\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS12btMLCPSolver = hidden constant [15 x i8] c"12btMLCPSolver\00", align 1
@_ZTI35btSequentialImpulseConstraintSolver = external constant i8*
@_ZTI12btMLCPSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btMLCPSolver, i32 0, i32 0), i8* bitcast (i8** @_ZTI35btSequentialImpulseConstraintSolver to i8*) }, align 4
@.str.27 = private unnamed_addr constant [17 x i8] c"m_storage.resize\00", align 1
@.str.28 = private unnamed_addr constant [18 x i8] c"clearSparseInfo=0\00", align 1
@.str.29 = private unnamed_addr constant [10 x i8] c"storage=0\00", align 1

@_ZN12btMLCPSolverC1EP21btMLCPSolverInterface = hidden unnamed_addr alias %class.btMLCPSolver* (%class.btMLCPSolver*, %class.btMLCPSolverInterface*), %class.btMLCPSolver* (%class.btMLCPSolver*, %class.btMLCPSolverInterface*)* @_ZN12btMLCPSolverC2EP21btMLCPSolverInterface
@_ZN12btMLCPSolverD1Ev = hidden unnamed_addr alias %class.btMLCPSolver* (%class.btMLCPSolver*), %class.btMLCPSolver* (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD2Ev

define hidden %class.btMLCPSolver* @_ZN12btMLCPSolverC2EP21btMLCPSolverInterface(%class.btMLCPSolver* returned %this, %class.btMLCPSolverInterface* %solver) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %solver.addr = alloca %class.btMLCPSolverInterface*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %class.btMLCPSolverInterface* %solver, %class.btMLCPSolverInterface** %solver.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC2Ev(%class.btSequentialImpulseConstraintSolver* %0)
  %1 = bitcast %class.btMLCPSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btMLCPSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call2 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_A)
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %call3 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_b)
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %call4 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_x)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %call5 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_lo)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %call6 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_hi)
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %call7 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_bSplit)
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %call8 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_xSplit)
  %m_bSplit1 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 8
  %call9 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_bSplit1)
  %m_xSplit2 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 9
  %call10 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_xSplit2)
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call11 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %m_limitDependencies)
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call12 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.5* %m_allConstraintArray)
  %m_solver = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %2 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %solver.addr, align 4, !tbaa !2
  store %class.btMLCPSolverInterface* %2, %class.btMLCPSolverInterface** %m_solver, align 4, !tbaa !8
  %m_fallback = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 13
  store i32 0, i32* %m_fallback, align 4, !tbaa !22
  ret %class.btMLCPSolver* %this1
}

declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #1

define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  store i32 0, i32* %m_rows, align 4, !tbaa !23
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  store i32 0, i32* %m_cols, align 4, !tbaa !24
  %m_operations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 2
  store i32 0, i32* %m_operations, align 4, !tbaa !25
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  store i32 0, i32* %m_resizeOperations, align 4, !tbaa !26
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  store i32 0, i32* %m_setElemOperations, align 4, !tbaa !27
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.18* %m_storage)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1)
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %call3 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.22* %m_colNonZeroElements)
  ret %struct.btMatrixX* %this1
}

define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.18* %m_storage)
  ret %struct.btVectorX* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.6* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: nounwind
define hidden %class.btMLCPSolver* @_ZN12btMLCPSolverD2Ev(%class.btMLCPSolver* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btMLCPSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.5* %m_allConstraintArray) #8
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call2 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %m_limitDependencies) #8
  %m_xSplit2 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 9
  %call3 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_xSplit2) #8
  %m_bSplit1 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 8
  %call4 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_bSplit1) #8
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %call5 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_xSplit) #8
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %call6 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_bSplit) #8
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %call7 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_hi) #8
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %call8 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_lo) #8
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %call9 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_x) #8
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %call10 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_b) #8
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call11 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_A) #8
  %1 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call12 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* %1) #8
  ret %class.btMLCPSolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.18* %m_storage) #8
  ret %struct.btVectorX* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.22* %m_colNonZeroElements) #8
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1) #8
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call3 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.18* %m_storage) #8
  ret %struct.btMatrixX* %this1
}

; Function Attrs: nounwind
declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #4

; Function Attrs: nounwind
define hidden void @_ZN12btMLCPSolverD0Ev(%class.btMLCPSolver* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %call = call %class.btMLCPSolver* @_ZN12btMLCPSolverD1Ev(%class.btMLCPSolver* %this1) #8
  %0 = bitcast %class.btMLCPSolver* %this1 to i8*
  call void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden float @_ZN12btMLCPSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMLCPSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodiesUnUsed, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodiesUnUsed.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %numFrictionPerContact = alloca i32, align 4
  %numBodies = alloca i32, align 4
  %ref.tmp = alloca %struct.btSolverConstraint, align 4
  %ref.tmp13 = alloca i32, align 4
  %dindex = alloca i32, align 4
  %i = alloca i32, align 4
  %firstContactConstraintOffset = alloca i32, align 4
  %i23 = alloca i32, align 4
  %findex = alloca i32, align 4
  %i62 = alloca i32, align 4
  %i78 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %__profile112 = alloca %class.CProfileSample, align 1
  %__profile116 = alloca %class.CProfileSample, align 1
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodiesUnUsed, i32* %numBodiesUnUsed.addr, align 4, !tbaa !28
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !28
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !28
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %2 = load i32, i32* %numBodiesUnUsed.addr, align 4, !tbaa !28
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  %4 = load i32, i32* %numManifolds.addr, align 4, !tbaa !28
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numConstraints.addr, align 4, !tbaa !28
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %call = call float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %7, %class.btIDebugDraw* %8)
  %9 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %9) #8
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0))
  %10 = bitcast i32* %numFrictionPerContact to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %11, i32 0, i32 2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool)
  %12 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %12, i32 0, i32 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool)
  %cmp = icmp eq i32 %call3, %call4
  %13 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 2
  store i32 %cond, i32* %numFrictionPerContact, align 4, !tbaa !28
  %14 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %15, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call5, i32* %numBodies, align 4, !tbaa !28
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %16 = bitcast %struct.btSolverConstraint* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 152, i8* %16) #8
  %17 = bitcast %struct.btSolverConstraint* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %17, i8 0, i32 152, i1 false)
  %call6 = call %struct.btSolverConstraint* @_ZN18btSolverConstraintC2Ev(%struct.btSolverConstraint* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray, i32 0, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %ref.tmp)
  %18 = bitcast %struct.btSolverConstraint* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 152, i8* %18) #8
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %19 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %19, i32 0, i32 3
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool)
  %20 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool8 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %20, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool8)
  %add = add nsw i32 %call7, %call9
  %21 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %21, i32 0, i32 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool10)
  %add12 = add nsw i32 %add, %call11
  %22 = bitcast i32* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  store i32 0, i32* %ref.tmp13, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %m_limitDependencies, i32 %add12, i32* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %23 = bitcast i32* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast i32* %dindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  store i32 0, i32* %dindex, align 4, !tbaa !28
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %26 = load i32, i32* %i, align 4, !tbaa !28
  %27 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool14 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %27, i32 0, i32 3
  %call15 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool14)
  %cmp16 = icmp slt i32 %26, %call15
  br i1 %cmp16, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintArray17 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %29 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool18 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %29, i32 0, i32 3
  %30 = load i32, i32* %i, align 4, !tbaa !28
  %call19 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool18, i32 %30)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray17, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call19)
  %m_limitDependencies20 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %31 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %dindex, align 4, !tbaa !28
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies20, i32 %31)
  store i32 -1, i32* %call21, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %32 = load i32, i32* %i, align 4, !tbaa !28
  %inc22 = add nsw i32 %32, 1
  store i32 %inc22, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast i32* %firstContactConstraintOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = load i32, i32* %dindex, align 4, !tbaa !28
  store i32 %34, i32* %firstContactConstraintOffset, align 4, !tbaa !28
  %35 = load i8, i8* @interleaveContactAndFriction, align 1, !tbaa !29, !range !30
  %tobool = trunc i8 %35 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.end
  %36 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  store i32 0, i32* %i23, align 4, !tbaa !28
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc59, %if.then
  %37 = load i32, i32* %i23, align 4, !tbaa !28
  %38 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool25 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %38, i32 0, i32 2
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool25)
  %cmp27 = icmp slt i32 %37, %call26
  br i1 %cmp27, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond24
  %39 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  br label %for.end61

for.body29:                                       ; preds = %for.cond24
  %m_allConstraintArray30 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %40 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool31 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %40, i32 0, i32 2
  %41 = load i32, i32* %i23, align 4, !tbaa !28
  %call32 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool31, i32 %41)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray30, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call32)
  %m_limitDependencies33 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %42 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc34 = add nsw i32 %42, 1
  store i32 %inc34, i32* %dindex, align 4, !tbaa !28
  %call35 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies33, i32 %42)
  store i32 -1, i32* %call35, align 4, !tbaa !28
  %m_allConstraintArray36 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %43 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool37 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %43, i32 0, i32 4
  %44 = load i32, i32* %i23, align 4, !tbaa !28
  %45 = load i32, i32* %numFrictionPerContact, align 4, !tbaa !28
  %mul = mul nsw i32 %44, %45
  %call38 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool37, i32 %mul)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray36, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call38)
  %46 = bitcast i32* %findex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool39 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %47, i32 0, i32 4
  %48 = load i32, i32* %i23, align 4, !tbaa !28
  %49 = load i32, i32* %numFrictionPerContact, align 4, !tbaa !28
  %mul40 = mul nsw i32 %48, %49
  %call41 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool39, i32 %mul40)
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call41, i32 0, i32 17
  %50 = load i32, i32* %m_frictionIndex, align 4, !tbaa !31
  %51 = load i32, i32* %numFrictionPerContact, align 4, !tbaa !28
  %add42 = add nsw i32 1, %51
  %mul43 = mul nsw i32 %50, %add42
  store i32 %mul43, i32* %findex, align 4, !tbaa !28
  %52 = load i32, i32* %findex, align 4, !tbaa !28
  %53 = load i32, i32* %firstContactConstraintOffset, align 4, !tbaa !28
  %add44 = add nsw i32 %52, %53
  %m_limitDependencies45 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %54 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc46 = add nsw i32 %54, 1
  store i32 %inc46, i32* %dindex, align 4, !tbaa !28
  %call47 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies45, i32 %54)
  store i32 %add44, i32* %call47, align 4, !tbaa !28
  %55 = load i32, i32* %numFrictionPerContact, align 4, !tbaa !28
  %cmp48 = icmp eq i32 %55, 2
  br i1 %cmp48, label %if.then49, label %if.end

if.then49:                                        ; preds = %for.body29
  %m_allConstraintArray50 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %56 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool51 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %56, i32 0, i32 4
  %57 = load i32, i32* %i23, align 4, !tbaa !28
  %58 = load i32, i32* %numFrictionPerContact, align 4, !tbaa !28
  %mul52 = mul nsw i32 %57, %58
  %add53 = add nsw i32 %mul52, 1
  %call54 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool51, i32 %add53)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray50, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call54)
  %59 = load i32, i32* %findex, align 4, !tbaa !28
  %60 = load i32, i32* %firstContactConstraintOffset, align 4, !tbaa !28
  %add55 = add nsw i32 %59, %60
  %m_limitDependencies56 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %61 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc57 = add nsw i32 %61, 1
  store i32 %inc57, i32* %dindex, align 4, !tbaa !28
  %call58 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies56, i32 %61)
  store i32 %add55, i32* %call58, align 4, !tbaa !28
  br label %if.end

if.end:                                           ; preds = %if.then49, %for.body29
  %62 = bitcast i32* %findex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  br label %for.inc59

for.inc59:                                        ; preds = %if.end
  %63 = load i32, i32* %i23, align 4, !tbaa !28
  %inc60 = add nsw i32 %63, 1
  store i32 %inc60, i32* %i23, align 4, !tbaa !28
  br label %for.cond24

for.end61:                                        ; preds = %for.cond.cleanup28
  br label %if.end98

if.else:                                          ; preds = %for.end
  %64 = bitcast i32* %i62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  store i32 0, i32* %i62, align 4, !tbaa !28
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc75, %if.else
  %65 = load i32, i32* %i62, align 4, !tbaa !28
  %66 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool64 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %66, i32 0, i32 2
  %call65 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool64)
  %cmp66 = icmp slt i32 %65, %call65
  br i1 %cmp66, label %for.body68, label %for.cond.cleanup67

for.cond.cleanup67:                               ; preds = %for.cond63
  %67 = bitcast i32* %i62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #8
  br label %for.end77

for.body68:                                       ; preds = %for.cond63
  %m_allConstraintArray69 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %68 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool70 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %68, i32 0, i32 2
  %69 = load i32, i32* %i62, align 4, !tbaa !28
  %call71 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactConstraintPool70, i32 %69)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray69, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call71)
  %m_limitDependencies72 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %70 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc73 = add nsw i32 %70, 1
  store i32 %inc73, i32* %dindex, align 4, !tbaa !28
  %call74 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies72, i32 %70)
  store i32 -1, i32* %call74, align 4, !tbaa !28
  br label %for.inc75

for.inc75:                                        ; preds = %for.body68
  %71 = load i32, i32* %i62, align 4, !tbaa !28
  %inc76 = add nsw i32 %71, 1
  store i32 %inc76, i32* %i62, align 4, !tbaa !28
  br label %for.cond63

for.end77:                                        ; preds = %for.cond.cleanup67
  %72 = bitcast i32* %i78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #8
  store i32 0, i32* %i78, align 4, !tbaa !28
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc95, %for.end77
  %73 = load i32, i32* %i78, align 4, !tbaa !28
  %74 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool80 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %74, i32 0, i32 4
  %call81 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool80)
  %cmp82 = icmp slt i32 %73, %call81
  br i1 %cmp82, label %for.body84, label %for.cond.cleanup83

for.cond.cleanup83:                               ; preds = %for.cond79
  %75 = bitcast i32* %i78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  br label %for.end97

for.body84:                                       ; preds = %for.cond79
  %m_allConstraintArray85 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %76 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool86 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %76, i32 0, i32 4
  %77 = load i32, i32* %i78, align 4, !tbaa !28
  %call87 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool86, i32 %77)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %m_allConstraintArray85, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %call87)
  %78 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool88 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %78, i32 0, i32 4
  %79 = load i32, i32* %i78, align 4, !tbaa !28
  %call89 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_tmpSolverContactFrictionConstraintPool88, i32 %79)
  %m_frictionIndex90 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call89, i32 0, i32 17
  %80 = load i32, i32* %m_frictionIndex90, align 4, !tbaa !31
  %81 = load i32, i32* %firstContactConstraintOffset, align 4, !tbaa !28
  %add91 = add nsw i32 %80, %81
  %m_limitDependencies92 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %82 = load i32, i32* %dindex, align 4, !tbaa !28
  %inc93 = add nsw i32 %82, 1
  store i32 %inc93, i32* %dindex, align 4, !tbaa !28
  %call94 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %m_limitDependencies92, i32 %82)
  store i32 %add91, i32* %call94, align 4, !tbaa !28
  br label %for.inc95

for.inc95:                                        ; preds = %for.body84
  %83 = load i32, i32* %i78, align 4, !tbaa !28
  %inc96 = add nsw i32 %83, 1
  store i32 %inc96, i32* %i78, align 4, !tbaa !28
  br label %for.cond79

for.end97:                                        ; preds = %for.cond.cleanup83
  br label %if.end98

if.end98:                                         ; preds = %for.end97, %for.end61
  %m_allConstraintArray99 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call100 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray99)
  %tobool101 = icmp ne i32 %call100, 0
  br i1 %tobool101, label %if.end103, label %if.then102

if.then102:                                       ; preds = %if.end98
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %m_A, i32 0, i32 0)
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 0)
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 0)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 0)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 0)
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end103:                                        ; preds = %if.end98
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end103, %if.then102
  %84 = bitcast i32* %firstContactConstraintOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  %85 = bitcast i32* %dindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast i32* %numFrictionPerContact to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %call108 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %88 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %88) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  %89 = load i8, i8* @gUseMatrixMultiply, align 1, !tbaa !29, !range !30
  %tobool110 = trunc i8 %89 to i1
  br i1 %tobool110, label %if.then111, label %if.else115

if.then111:                                       ; preds = %cleanup.cont
  %90 = bitcast %class.CProfileSample* %__profile112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %90) #8
  %call113 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile112, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0))
  %91 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %92 = bitcast %class.btMLCPSolver* %this1 to void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %92, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable, i64 13
  %93 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn, align 4
  call void %93(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %91)
  %call114 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile112) #8
  %94 = bitcast %class.CProfileSample* %__profile112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %94) #8
  br label %if.end121

if.else115:                                       ; preds = %cleanup.cont
  %95 = bitcast %class.CProfileSample* %__profile116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %95) #8
  %call117 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile116, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0))
  %96 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %97 = bitcast %class.btMLCPSolver* %this1 to void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable118 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %97, align 4, !tbaa !6
  %vfn119 = getelementptr inbounds void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable118, i64 14
  %98 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn119, align 4
  call void %98(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %96)
  %call120 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile116) #8
  %99 = bitcast %class.CProfileSample* %__profile116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %99) #8
  br label %if.end121

if.end121:                                        ; preds = %if.else115, %if.then111
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end121, %cleanup
  %100 = load float, float* %retval, align 4
  ret float %100

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  store i8* %name, i8** %name.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4, !tbaa !2
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !35
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !36
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %this, i32 %newsize, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btSolverConstraint*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !28
  store %struct.btSolverConstraint* %fillData, %struct.btSolverConstraint** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !28
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %2 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  store i32 %4, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %6 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !28
  store i32 %14, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !28
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %18 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data11, align 4, !tbaa !39
  %19 = load i32, i32* %i6, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %18, i32 %19
  %20 = bitcast %struct.btSolverConstraint* %arrayidx12 to i8*
  %call13 = call i8* @_ZN18btSolverConstraintnwEmPv(i32 152, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btSolverConstraint*
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btSolverConstraint* %21 to i8*
  %24 = bitcast %struct.btSolverConstraint* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 152, i1 false), !tbaa.struct !40
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !28
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !35
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSolverConstraint* @_ZN18btSolverConstraintC2Ev(%struct.btSolverConstraint* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverConstraint*, align 4
  store %struct.btSolverConstraint* %this, %struct.btSolverConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %this.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos1CrossNormal)
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal1)
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos2CrossNormal)
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal2)
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentA)
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %this1, i32 0, i32 5
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentB)
  ret %struct.btSolverConstraint* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !28
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !28
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %2 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  store i32 %4, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %6 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !43
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !28
  store i32 %14, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !28
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !43
  %19 = load i32, i32* %i6, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !28
  store i32 %23, i32* %21, align 4, !tbaa !28
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !28
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !44
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE9push_backERKS0_(%class.btAlignedObjectArray.5* %this, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Val.addr = alloca %struct.btSolverConstraint*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store %struct.btSolverConstraint* %_Val, %struct.btSolverConstraint** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !28
  %1 = load i32, i32* %sz, align 4, !tbaa !28
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.5* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !35
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %2, i32 %3
  %4 = bitcast %struct.btSolverConstraint* %arrayidx to i8*
  %call5 = call i8* @_ZN18btSolverConstraintnwEmPv(i32 152, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btSolverConstraint*
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btSolverConstraint* %5 to i8*
  %8 = bitcast %struct.btSolverConstraint* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 152, i1 false), !tbaa.struct !40
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !35
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !35
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 %1
  ret %struct.btSolverConstraint* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !43
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this, i32 %rows, i32 %cols) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !28
  store i32 %cols, i32* %cols.addr, align 4, !tbaa !28
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_resizeOperations, align 4, !tbaa !26
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_resizeOperations, align 4, !tbaa !26
  %1 = load i32, i32* %rows.addr, align 4, !tbaa !28
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  store i32 %1, i32* %m_rows, align 4, !tbaa !23
  %2 = load i32, i32* %cols.addr, align 4, !tbaa !28
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  store i32 %2, i32* %m_cols, align 4, !tbaa !24
  %3 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %3) #8
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.27, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %4 = load i32, i32* %rows.addr, align 4, !tbaa !28
  %5 = load i32, i32* %cols.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %4, %5
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !42
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.18* %m_storage, i32 %mul, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %8 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %8) #8
  call void @_ZN9btMatrixXIfE15clearSparseInfoEv(%struct.btMatrixX* %this1)
  ret void
}

define linkonce_odr hidden void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %this, i32 %rows) #0 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %rows.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !28
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %rows.addr, align 4, !tbaa !28
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !42
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.18* %m_storage, i32 %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret %class.CProfileSample* %this1
}

define hidden zeroext i1 @_ZN12btMLCPSolver9solveMLCPERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %result = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %Acopy = alloca %struct.btMatrixX, align 4
  %limitDependenciesCopy = alloca %class.btAlignedObjectArray.10, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %result) #8
  store i8 1, i8* %result, align 1, !tbaa !29
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btContactSolverInfo* %0 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 11
  %2 = load i32, i32* %m_splitImpulse, align 4, !tbaa !45
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %3 = bitcast %struct.btMatrixX* %Acopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %3) #8
  %m_A3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call4 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2ERKS0_(%struct.btMatrixX* %Acopy, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %m_A3)
  %4 = bitcast %class.btAlignedObjectArray.10* %limitDependenciesCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %4) #8
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call5 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.10* %limitDependenciesCopy, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %m_limitDependencies)
  %m_solver = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %5 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver, align 4, !tbaa !8
  %m_A6 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %m_limitDependencies7 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %6 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btContactSolverInfo* %6 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %7, i32 0, i32 5
  %8 = load i32, i32* %m_numIterations, align 4, !tbaa !47
  %9 = bitcast %class.btMLCPSolverInterface* %5 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)***
  %vtable = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*** %9, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vtable, i64 2
  %10 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vfn, align 4
  %call8 = call zeroext i1 %10(%class.btMLCPSolverInterface* %5, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %m_A6, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_b, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_x, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %m_limitDependencies7, i32 %8, i1 zeroext true)
  %frombool = zext i1 %call8 to i8
  store i8 %frombool, i8* %result, align 1, !tbaa !29
  %11 = load i8, i8* %result, align 1, !tbaa !29, !range !30
  %tobool9 = trunc i8 %11 to i1
  br i1 %tobool9, label %if.then10, label %if.end19

if.then10:                                        ; preds = %if.then2
  %m_solver11 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %12 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver11, align 4, !tbaa !8
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %m_lo12 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi13 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %14 = bitcast %struct.btContactSolverInfo* %13 to %struct.btContactSolverInfoData*
  %m_numIterations14 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 5
  %15 = load i32, i32* %m_numIterations14, align 4, !tbaa !47
  %16 = bitcast %class.btMLCPSolverInterface* %12 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)***
  %vtable15 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*** %16, align 4, !tbaa !6
  %vfn16 = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vtable15, i64 2
  %17 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vfn16, align 4
  %call17 = call zeroext i1 %17(%class.btMLCPSolverInterface* %12, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %Acopy, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_bSplit, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_xSplit, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo12, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi13, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %limitDependenciesCopy, i32 %15, i1 zeroext true)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %result, align 1, !tbaa !29
  br label %if.end19

if.end19:                                         ; preds = %if.then10, %if.then2
  %call20 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %limitDependenciesCopy) #8
  %18 = bitcast %class.btAlignedObjectArray.10* %limitDependenciesCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %18) #8
  %call21 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %Acopy) #8
  %19 = bitcast %struct.btMatrixX* %Acopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %19) #8
  br label %if.end34

if.else:                                          ; preds = %if.end
  %m_solver22 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %20 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver22, align 4, !tbaa !8
  %m_A23 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %m_b24 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %m_x25 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %m_lo26 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi27 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %m_limitDependencies28 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %21 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %22 = bitcast %struct.btContactSolverInfo* %21 to %struct.btContactSolverInfoData*
  %m_numIterations29 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %22, i32 0, i32 5
  %23 = load i32, i32* %m_numIterations29, align 4, !tbaa !47
  %24 = bitcast %class.btMLCPSolverInterface* %20 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)***
  %vtable30 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*** %24, align 4, !tbaa !6
  %vfn31 = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vtable30, i64 2
  %25 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.10*, i32, i1)** %vfn31, align 4
  %call32 = call zeroext i1 %25(%class.btMLCPSolverInterface* %20, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %m_A23, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_b24, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_x25, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo26, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi27, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %m_limitDependencies28, i32 %23, i1 zeroext true)
  %frombool33 = zext i1 %call32 to i8
  store i8 %frombool33, i8* %result, align 1, !tbaa !29
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end19
  %26 = load i8, i8* %result, align 1, !tbaa !29, !range !30
  %tobool35 = trunc i8 %26 to i1
  store i1 %tobool35, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end34, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %result) #8
  %27 = load i1, i1* %retval, align 1
  ret i1 %27
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_rows, align 4, !tbaa !23
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2ERKS0_(%struct.btMatrixX* returned %this, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %0) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store %struct.btMatrixX* %0, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rows2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %1, i32 0, i32 0
  %2 = bitcast i32* %m_rows to i8*
  %3 = bitcast i32* %m_rows2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %2, i8* align 4 %3, i64 20, i1 false)
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %4 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %4, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray.18* %m_storage, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %m_storage3)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %5 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_rowNonZeroElements14 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %5, i32 0, i32 6
  %call5 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_rowNonZeroElements14)
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %6 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_colNonZeroElements6 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %6, i32 0, i32 7
  %call7 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_(%class.btAlignedObjectArray.22* %m_colNonZeroElements, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_colNonZeroElements6)
  ret %struct.btMatrixX* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.10* returned %this, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.10* %otherArray, %class.btAlignedObjectArray.10** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.10* %this1)
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %otherArray.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %1)
  store i32 %call2, i32* %otherSize, align 4, !tbaa !28
  %2 = load i32, i32* %otherSize, align 4, !tbaa !28
  %3 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %ref.tmp, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %this1, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4, !tbaa !43
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.10* %5, i32 0, i32 %6, i32* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret %class.btAlignedObjectArray.10* %this1
}

define hidden void @_ZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numContactRows = alloca i32, align 4
  %numConstraintRows = alloca i32, align 4
  %n = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %w = alloca float*, align 4
  %nub = alloca i32, align 4
  %__profile22 = alloca %class.CProfileSample, align 1
  %i24 = alloca i32, align 4
  %m = alloca i32, align 4
  %numBodies = alloca i32, align 4
  %bodyJointNodeArray = alloca %class.btAlignedObjectArray.10, align 4
  %__profile45 = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca i32, align 4
  %jointNodeArray = alloca %class.btAlignedObjectArray.26, align 4
  %__profile49 = alloca %class.CProfileSample, align 1
  %__profile56 = alloca %class.CProfileSample, align 1
  %__profile66 = alloca %class.CProfileSample, align 1
  %cur = alloca i32, align 4
  %rowOffset = alloca i32, align 4
  %__profile76 = alloca %class.CProfileSample, align 1
  %ref.tmp78 = alloca i32, align 4
  %__profile82 = alloca %class.CProfileSample, align 1
  %c = alloca i32, align 4
  %numRows = alloca i32, align 4
  %i84 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sbA = alloca i32, align 4
  %sbB = alloca i32, align 4
  %orgBodyA = alloca %class.btRigidBody*, align 4
  %orgBodyB = alloca %class.btRigidBody*, align 4
  %slotA = alloca i32, align 4
  %ref.tmp108 = alloca %struct.btJointNode, align 4
  %prevSlot = alloca i32, align 4
  %row = alloca i32, align 4
  %normalInvMass = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca float, align 4
  %relPosCrossNormalInvInertia = alloca %class.btVector3, align 4
  %r = alloca i32, align 4
  %slotB = alloca i32, align 4
  %ref.tmp166 = alloca %struct.btJointNode, align 4
  %prevSlot168 = alloca i32, align 4
  %row184 = alloca i32, align 4
  %normalInvMassB = alloca %class.btVector3, align 4
  %ref.tmp192 = alloca float, align 4
  %relPosInvInertiaB = alloca %class.btVector3, align 4
  %r198 = alloca i32, align 4
  %JinvM = alloca float*, align 4
  %Jptr = alloca float*, align 4
  %__profile239 = alloca %class.CProfileSample, align 1
  %__profile242 = alloca %class.CProfileSample, align 1
  %c246 = alloca i32, align 4
  %numRows247 = alloca i32, align 4
  %__profile248 = alloca %class.CProfileSample, align 1
  %i250 = alloca i32, align 4
  %row__ = alloca i32, align 4
  %sbA258 = alloca i32, align 4
  %sbB262 = alloca i32, align 4
  %orgBodyA266 = alloca %class.btRigidBody*, align 4
  %orgBodyB270 = alloca %class.btRigidBody*, align 4
  %JinvMrow = alloca float*, align 4
  %startJointNodeA = alloca i32, align 4
  %j0 = alloca i32, align 4
  %cr0 = alloca i32, align 4
  %numRowsOther = alloca i32, align 4
  %ofsother = alloca i32, align 4
  %startJointNodeB = alloca i32, align 4
  %j1 = alloca i32, align 4
  %cj1 = alloca i32, align 4
  %numRowsOther331 = alloca i32, align 4
  %ofsother342 = alloca i32, align 4
  %__profile368 = alloca %class.CProfileSample, align 1
  %row__370 = alloca i32, align 4
  %numJointRows = alloca i32, align 4
  %jj = alloca i32, align 4
  %sbA376 = alloca i32, align 4
  %sbB380 = alloca i32, align 4
  %orgBodyA384 = alloca %class.btRigidBody*, align 4
  %orgBodyB388 = alloca %class.btRigidBody*, align 4
  %infom = alloca i32, align 4
  %JinvMrow402 = alloca float*, align 4
  %Jrow = alloca float*, align 4
  %i421 = alloca i32, align 4
  %cfm = alloca float, align 4
  %__profile436 = alloca %class.CProfileSample, align 1
  %__profile440 = alloca %class.CProfileSample, align 1
  %i444 = alloca i32, align 4
  %c451 = alloca %struct.btSolverConstraint*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast i32* %numContactRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8, i8* @interleaveContactAndFriction, align 1, !tbaa !29, !range !30
  %tobool = trunc i8 %1 to i1
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 3, i32 1
  store i32 %cond, i32* %numContactRows, align 4, !tbaa !28
  %3 = bitcast i32* %numConstraintRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray)
  store i32 %call, i32* %numConstraintRows, align 4, !tbaa !28
  %4 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  store i32 %5, i32* %n, align 4, !tbaa !28
  %6 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %6) #8
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0))
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %7 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 %7)
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %8 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_bSplit, i32 %8)
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %11 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintArray3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %13 = load i32, i32* %i, align 4, !tbaa !28
  %call4 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray3, i32 %13)
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call4, i32 0, i32 9
  %14 = load float, float* %m_jacDiagABInv, align 4, !tbaa !48
  %tobool5 = fcmp une float %14, 0.000000e+00
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_allConstraintArray6 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %15 = load i32, i32* %i, align 4, !tbaa !28
  %call7 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray6, i32 %15)
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call7, i32 0, i32 10
  %16 = load float, float* %m_rhs, align 4, !tbaa !49
  %m_allConstraintArray8 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %17 = load i32, i32* %i, align 4, !tbaa !28
  %call9 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray8, i32 %17)
  %m_jacDiagABInv10 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call9, i32 0, i32 9
  %18 = load float, float* %m_jacDiagABInv10, align 4, !tbaa !48
  %div = fdiv float %16, %18
  %m_b11 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %19 = load i32, i32* %i, align 4, !tbaa !28
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_b11, i32 %19)
  store float %div, float* %call12, align 4, !tbaa !42
  %m_allConstraintArray13 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %20 = load i32, i32* %i, align 4, !tbaa !28
  %call14 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray13, i32 %20)
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call14, i32 0, i32 14
  %21 = load float, float* %m_rhsPenetration, align 4, !tbaa !50
  %m_allConstraintArray15 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %22 = load i32, i32* %i, align 4, !tbaa !28
  %call16 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray15, i32 %22)
  %m_jacDiagABInv17 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call16, i32 0, i32 9
  %23 = load float, float* %m_jacDiagABInv17, align 4, !tbaa !48
  %div18 = fdiv float %21, %23
  %m_bSplit19 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %24 = load i32, i32* %i, align 4, !tbaa !28
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_bSplit19, i32 %24)
  store float %div18, float* %call20, align 4, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %25 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %call21 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %26 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %26) #8
  %27 = bitcast float** %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  store float* null, float** %w, align 4, !tbaa !2
  %28 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store i32 0, i32* %nub, align 4, !tbaa !28
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %29 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 %29)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %30 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 %30)
  %31 = bitcast %class.CProfileSample* %__profile22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %31) #8
  %call23 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile22, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i32 0, i32 0))
  %32 = bitcast i32* %i24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  store i32 0, i32* %i24, align 4, !tbaa !28
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc37, %for.end
  %33 = load i32, i32* %i24, align 4, !tbaa !28
  %34 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  %cmp26 = icmp slt i32 %33, %34
  br i1 %cmp26, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond25
  %35 = bitcast i32* %i24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %for.end39

for.body28:                                       ; preds = %for.cond25
  %m_allConstraintArray29 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %36 = load i32, i32* %i24, align 4, !tbaa !28
  %call30 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray29, i32 %36)
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call30, i32 0, i32 12
  %37 = load float, float* %m_lowerLimit, align 4, !tbaa !51
  %m_lo31 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %38 = load i32, i32* %i24, align 4, !tbaa !28
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_lo31, i32 %38)
  store float %37, float* %call32, align 4, !tbaa !42
  %m_allConstraintArray33 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %39 = load i32, i32* %i24, align 4, !tbaa !28
  %call34 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray33, i32 %39)
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call34, i32 0, i32 13
  %40 = load float, float* %m_upperLimit, align 4, !tbaa !52
  %m_hi35 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %41 = load i32, i32* %i24, align 4, !tbaa !28
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_hi35, i32 %41)
  store float %40, float* %call36, align 4, !tbaa !42
  br label %for.inc37

for.inc37:                                        ; preds = %for.body28
  %42 = load i32, i32* %i24, align 4, !tbaa !28
  %inc38 = add nsw i32 %42, 1
  store i32 %inc38, i32* %i24, align 4, !tbaa !28
  br label %for.cond25

for.end39:                                        ; preds = %for.cond.cleanup27
  %call40 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile22) #8
  %43 = bitcast %class.CProfileSample* %__profile22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %43) #8
  %44 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #8
  %m_allConstraintArray41 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call42 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray41)
  store i32 %call42, i32* %m, align 4, !tbaa !28
  %45 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  %46 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %46, i32 0, i32 1
  %call43 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call43, i32* %numBodies, align 4, !tbaa !28
  %47 = bitcast %class.btAlignedObjectArray.10* %bodyJointNodeArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %47) #8
  %call44 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %bodyJointNodeArray)
  %48 = bitcast %class.CProfileSample* %__profile45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %48) #8
  %call46 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile45, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.5, i32 0, i32 0))
  %49 = load i32, i32* %numBodies, align 4, !tbaa !28
  %50 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  store i32 -1, i32* %ref.tmp, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %49, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %51 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %call47 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile45) #8
  %52 = bitcast %class.CProfileSample* %__profile45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %52) #8
  %53 = bitcast %class.btAlignedObjectArray.26* %jointNodeArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %53) #8
  %call48 = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev(%class.btAlignedObjectArray.26* %jointNodeArray)
  %54 = bitcast %class.CProfileSample* %__profile49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %54) #8
  %call50 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile49, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i32 0, i32 0))
  %m_allConstraintArray51 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call52 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray51)
  %mul = mul nsw i32 2, %call52
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %mul)
  %call53 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile49) #8
  %55 = bitcast %class.CProfileSample* %__profile49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %55) #8
  %56 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3 to i8*) acquire, align 4
  %57 = and i8 %56, 1
  %guard.uninitialized = icmp eq i8 %57, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !53

init.check:                                       ; preds = %for.end39
  %58 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3) #8
  %tobool54 = icmp ne i32 %58, 0
  br i1 %tobool54, label %init, label %init.end

init:                                             ; preds = %init.check
  %call55 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3)
  %59 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3) #8
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %for.end39
  %60 = bitcast %class.CProfileSample* %__profile56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %60) #8
  %call57 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile56, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.7, i32 0, i32 0))
  %61 = load i32, i32* %m, align 4, !tbaa !28
  %mul58 = mul nsw i32 2, %61
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %mul58, i32 8)
  %call59 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile56) #8
  %62 = bitcast %class.CProfileSample* %__profile56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %62) #8
  %63 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3 to i8*) acquire, align 4
  %64 = and i8 %63, 1
  %guard.uninitialized60 = icmp eq i8 %64, 0
  br i1 %guard.uninitialized60, label %init.check61, label %init.end65, !prof !53

init.check61:                                     ; preds = %init.end
  %65 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3) #8
  %tobool62 = icmp ne i32 %65, 0
  br i1 %tobool62, label %init63, label %init.end65

init63:                                           ; preds = %init.check61
  %call64 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3)
  %66 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.8, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3) #8
  br label %init.end65

init.end65:                                       ; preds = %init63, %init.check61, %init.end
  %67 = bitcast %class.CProfileSample* %__profile66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %67) #8
  %call67 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile66, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.9, i32 0, i32 0))
  %68 = load i32, i32* %m, align 4, !tbaa !28
  %mul68 = mul nsw i32 2, %68
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %mul68, i32 8)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3)
  %call69 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile66) #8
  %69 = bitcast %class.CProfileSample* %__profile66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %69) #8
  %70 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #8
  store i32 0, i32* %cur, align 4, !tbaa !28
  %71 = bitcast i32* %rowOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  store i32 0, i32* %rowOffset, align 4, !tbaa !28
  %72 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs to i8*) acquire, align 4
  %73 = and i8 %72, 1
  %guard.uninitialized70 = icmp eq i8 %73, 0
  br i1 %guard.uninitialized70, label %init.check71, label %init.end75, !prof !53

init.check71:                                     ; preds = %init.end65
  %74 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs) #8
  %tobool72 = icmp ne i32 %74, 0
  br i1 %tobool72, label %init73, label %init.end75

init73:                                           ; preds = %init.check71
  %call74 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs)
  %75 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.10, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs) #8
  br label %init.end75

init.end75:                                       ; preds = %init73, %init.check71, %init.end65
  %76 = bitcast %class.CProfileSample* %__profile76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %76) #8
  %call77 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile76, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.11, i32 0, i32 0))
  %77 = bitcast i32* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #8
  store i32 0, i32* %ref.tmp78, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp78)
  %78 = bitcast i32* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %m_allConstraintArray79 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call80 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray79)
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %call80)
  %call81 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile76) #8
  %79 = bitcast %class.CProfileSample* %__profile76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %79) #8
  %80 = bitcast %class.CProfileSample* %__profile82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %80) #8
  %call83 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile82, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.12, i32 0, i32 0))
  %81 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #8
  store i32 0, i32* %c, align 4, !tbaa !28
  %82 = bitcast i32* %numRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #8
  store i32 0, i32* %numRows, align 4, !tbaa !28
  %83 = bitcast i32* %i84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  store i32 0, i32* %i84, align 4, !tbaa !28
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc232, %init.end75
  %84 = load i32, i32* %i84, align 4, !tbaa !28
  %m_allConstraintArray86 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call87 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray86)
  %cmp88 = icmp slt i32 %84, %call87
  br i1 %cmp88, label %for.body90, label %for.cond.cleanup89

for.cond.cleanup89:                               ; preds = %for.cond85
  store i32 8, i32* %cleanup.dest.slot, align 4
  %85 = bitcast i32* %i84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  br label %for.end235

for.body90:                                       ; preds = %for.cond85
  %86 = load i32, i32* %rowOffset, align 4, !tbaa !28
  %87 = load i32, i32* %c, align 4, !tbaa !28
  %call91 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %87)
  store i32 %86, i32* %call91, align 4, !tbaa !28
  %88 = bitcast i32* %sbA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  %m_allConstraintArray92 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %89 = load i32, i32* %i84, align 4, !tbaa !28
  %call93 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray92, i32 %89)
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call93, i32 0, i32 18
  %90 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !54
  store i32 %90, i32* %sbA, align 4, !tbaa !28
  %91 = bitcast i32* %sbB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #8
  %m_allConstraintArray94 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %92 = load i32, i32* %i84, align 4, !tbaa !28
  %call95 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray94, i32 %92)
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call95, i32 0, i32 19
  %93 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !55
  store i32 %93, i32* %sbB, align 4, !tbaa !28
  %94 = bitcast %class.btRigidBody** %orgBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #8
  %95 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool96 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %95, i32 0, i32 1
  %96 = load i32, i32* %sbA, align 4, !tbaa !28
  %call97 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool96, i32 %96)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call97, i32 0, i32 12
  %97 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !56
  store %class.btRigidBody* %97, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %98 = bitcast %class.btRigidBody** %orgBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #8
  %99 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool98 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %99, i32 0, i32 1
  %100 = load i32, i32* %sbB, align 4, !tbaa !28
  %call99 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool98, i32 %100)
  %m_originalBody100 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call99, i32 0, i32 12
  %101 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody100, align 4, !tbaa !56
  store %class.btRigidBody* %101, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %102 = load i32, i32* %i84, align 4, !tbaa !28
  %103 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %103, i32 0, i32 3
  %call101 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool)
  %cmp102 = icmp slt i32 %102, %call101
  br i1 %cmp102, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body90
  %104 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %104, i32 0, i32 9
  %105 = load i32, i32* %c, align 4, !tbaa !28
  %call103 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %m_tmpConstraintSizesPool, i32 %105)
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call103, i32 0, i32 0
  %106 = load i32, i32* %m_numConstraintRows, align 4, !tbaa !60
  br label %cond.end

cond.false:                                       ; preds = %for.body90
  %107 = load i32, i32* %numContactRows, align 4, !tbaa !28
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond104 = phi i32 [ %106, %cond.true ], [ %107, %cond.false ]
  store i32 %cond104, i32* %numRows, align 4, !tbaa !28
  %108 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %tobool105 = icmp ne %class.btRigidBody* %108, null
  br i1 %tobool105, label %if.then106, label %if.else

if.then106:                                       ; preds = %cond.end
  %109 = bitcast i32* %slotA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #8
  store i32 -1, i32* %slotA, align 4, !tbaa !28
  %call107 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %jointNodeArray)
  store i32 %call107, i32* %slotA, align 4, !tbaa !28
  %110 = bitcast %struct.btJointNode* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #8
  %111 = bitcast %struct.btJointNode* %ref.tmp108 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %111, i8 0, i32 16, i1 false)
  %call109 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.26* %jointNodeArray, %struct.btJointNode* nonnull align 4 dereferenceable(16) %ref.tmp108)
  %112 = bitcast %struct.btJointNode* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %112) #8
  %113 = bitcast i32* %prevSlot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #8
  %114 = load i32, i32* %sbA, align 4, !tbaa !28
  %call110 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %114)
  %115 = load i32, i32* %call110, align 4, !tbaa !28
  store i32 %115, i32* %prevSlot, align 4, !tbaa !28
  %116 = load i32, i32* %slotA, align 4, !tbaa !28
  %117 = load i32, i32* %sbA, align 4, !tbaa !28
  %call111 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %117)
  store i32 %116, i32* %call111, align 4, !tbaa !28
  %118 = load i32, i32* %prevSlot, align 4, !tbaa !28
  %119 = load i32, i32* %slotA, align 4, !tbaa !28
  %call112 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %119)
  %nextJointNodeIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call112, i32 0, i32 2
  store i32 %118, i32* %nextJointNodeIndex, align 4, !tbaa !62
  %120 = load i32, i32* %c, align 4, !tbaa !28
  %121 = load i32, i32* %slotA, align 4, !tbaa !28
  %call113 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %121)
  %jointIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call113, i32 0, i32 0
  store i32 %120, i32* %jointIndex, align 4, !tbaa !64
  %122 = load i32, i32* %i84, align 4, !tbaa !28
  %123 = load i32, i32* %slotA, align 4, !tbaa !28
  %call114 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %123)
  %constraintRowIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call114, i32 0, i32 3
  store i32 %122, i32* %constraintRowIndex, align 4, !tbaa !65
  %124 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %tobool115 = icmp ne %class.btRigidBody* %124, null
  br i1 %tobool115, label %cond.true116, label %cond.false117

cond.true116:                                     ; preds = %if.then106
  %125 = load i32, i32* %sbB, align 4, !tbaa !28
  br label %cond.end118

cond.false117:                                    ; preds = %if.then106
  br label %cond.end118

cond.end118:                                      ; preds = %cond.false117, %cond.true116
  %cond119 = phi i32 [ %125, %cond.true116 ], [ -1, %cond.false117 ]
  %126 = load i32, i32* %slotA, align 4, !tbaa !28
  %call120 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %126)
  %otherBodyIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call120, i32 0, i32 1
  store i32 %cond119, i32* %otherBodyIndex, align 4, !tbaa !66
  %127 = bitcast i32* %prevSlot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #8
  %128 = bitcast i32* %slotA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  %129 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #8
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc157, %cond.end118
  %130 = load i32, i32* %row, align 4, !tbaa !28
  %131 = load i32, i32* %numRows, align 4, !tbaa !28
  %cmp122 = icmp slt i32 %130, %131
  br i1 %cmp122, label %for.body124, label %for.cond.cleanup123

for.cond.cleanup123:                              ; preds = %for.cond121
  store i32 11, i32* %cleanup.dest.slot, align 4
  %132 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  br label %for.end160

for.body124:                                      ; preds = %for.cond121
  %133 = bitcast %class.btVector3* %normalInvMass to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #8
  %m_allConstraintArray125 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %134 = load i32, i32* %i84, align 4, !tbaa !28
  %135 = load i32, i32* %row, align 4, !tbaa !28
  %add = add nsw i32 %134, %135
  %call126 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray125, i32 %add)
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call126, i32 0, i32 1
  %136 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #8
  %137 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %call128 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %137)
  store float %call128, float* %ref.tmp127, align 4, !tbaa !42
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %normalInvMass, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %ref.tmp127)
  %138 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #8
  %139 = bitcast %class.btVector3* %relPosCrossNormalInvInertia to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #8
  %m_allConstraintArray129 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %140 = load i32, i32* %i84, align 4, !tbaa !28
  %141 = load i32, i32* %row, align 4, !tbaa !28
  %add130 = add nsw i32 %140, %141
  %call131 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray129, i32 %add130)
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call131, i32 0, i32 0
  %142 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %call132 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %142)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %relPosCrossNormalInvInertia, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call132)
  %143 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #8
  store i32 0, i32* %r, align 4, !tbaa !28
  br label %for.cond133

for.cond133:                                      ; preds = %for.inc154, %for.body124
  %144 = load i32, i32* %r, align 4, !tbaa !28
  %cmp134 = icmp slt i32 %144, 3
  br i1 %cmp134, label %for.body136, label %for.cond.cleanup135

for.cond.cleanup135:                              ; preds = %for.cond133
  store i32 14, i32* %cleanup.dest.slot, align 4
  %145 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #8
  br label %for.end156

for.body136:                                      ; preds = %for.cond133
  %146 = load i32, i32* %cur, align 4, !tbaa !28
  %147 = load i32, i32* %r, align 4, !tbaa !28
  %m_allConstraintArray137 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %148 = load i32, i32* %i84, align 4, !tbaa !28
  %149 = load i32, i32* %row, align 4, !tbaa !28
  %add138 = add nsw i32 %148, %149
  %call139 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray137, i32 %add138)
  %m_contactNormal1140 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call139, i32 0, i32 1
  %call141 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1140)
  %150 = load i32, i32* %r, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %call141, i32 %150
  %151 = load float, float* %arrayidx, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %146, i32 %147, float %151)
  %152 = load i32, i32* %cur, align 4, !tbaa !28
  %153 = load i32, i32* %r, align 4, !tbaa !28
  %add142 = add nsw i32 %153, 4
  %m_allConstraintArray143 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %154 = load i32, i32* %i84, align 4, !tbaa !28
  %155 = load i32, i32* %row, align 4, !tbaa !28
  %add144 = add nsw i32 %154, %155
  %call145 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray143, i32 %add144)
  %m_relpos1CrossNormal146 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call145, i32 0, i32 0
  %call147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal146)
  %156 = load i32, i32* %r, align 4, !tbaa !28
  %arrayidx148 = getelementptr inbounds float, float* %call147, i32 %156
  %157 = load float, float* %arrayidx148, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %152, i32 %add142, float %157)
  %158 = load i32, i32* %cur, align 4, !tbaa !28
  %159 = load i32, i32* %r, align 4, !tbaa !28
  %call149 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalInvMass)
  %160 = load i32, i32* %r, align 4, !tbaa !28
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 %160
  %161 = load float, float* %arrayidx150, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %158, i32 %159, float %161)
  %162 = load i32, i32* %cur, align 4, !tbaa !28
  %163 = load i32, i32* %r, align 4, !tbaa !28
  %add151 = add nsw i32 %163, 4
  %call152 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %relPosCrossNormalInvInertia)
  %164 = load i32, i32* %r, align 4, !tbaa !28
  %arrayidx153 = getelementptr inbounds float, float* %call152, i32 %164
  %165 = load float, float* %arrayidx153, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %162, i32 %add151, float %165)
  br label %for.inc154

for.inc154:                                       ; preds = %for.body136
  %166 = load i32, i32* %r, align 4, !tbaa !28
  %inc155 = add nsw i32 %166, 1
  store i32 %inc155, i32* %r, align 4, !tbaa !28
  br label %for.cond133

for.end156:                                       ; preds = %for.cond.cleanup135
  %167 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %167, i32 3, float 0.000000e+00)
  %168 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %168, i32 3, float 0.000000e+00)
  %169 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %169, i32 7, float 0.000000e+00)
  %170 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %170, i32 7, float 0.000000e+00)
  %171 = bitcast %class.btVector3* %relPosCrossNormalInvInertia to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %171) #8
  %172 = bitcast %class.btVector3* %normalInvMass to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #8
  br label %for.inc157

for.inc157:                                       ; preds = %for.end156
  %173 = load i32, i32* %row, align 4, !tbaa !28
  %inc158 = add nsw i32 %173, 1
  store i32 %inc158, i32* %row, align 4, !tbaa !28
  %174 = load i32, i32* %cur, align 4, !tbaa !28
  %inc159 = add nsw i32 %174, 1
  store i32 %inc159, i32* %cur, align 4, !tbaa !28
  br label %for.cond121

for.end160:                                       ; preds = %for.cond.cleanup123
  br label %if.end162

if.else:                                          ; preds = %cond.end
  %175 = load i32, i32* %numRows, align 4, !tbaa !28
  %176 = load i32, i32* %cur, align 4, !tbaa !28
  %add161 = add nsw i32 %176, %175
  store i32 %add161, i32* %cur, align 4, !tbaa !28
  br label %if.end162

if.end162:                                        ; preds = %if.else, %for.end160
  %177 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %tobool163 = icmp ne %class.btRigidBody* %177, null
  br i1 %tobool163, label %if.then164, label %if.else228

if.then164:                                       ; preds = %if.end162
  %178 = bitcast i32* %slotB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #8
  store i32 -1, i32* %slotB, align 4, !tbaa !28
  %call165 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %jointNodeArray)
  store i32 %call165, i32* %slotB, align 4, !tbaa !28
  %179 = bitcast %struct.btJointNode* %ref.tmp166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %179) #8
  %180 = bitcast %struct.btJointNode* %ref.tmp166 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %180, i8 0, i32 16, i1 false)
  %call167 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.26* %jointNodeArray, %struct.btJointNode* nonnull align 4 dereferenceable(16) %ref.tmp166)
  %181 = bitcast %struct.btJointNode* %ref.tmp166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %181) #8
  %182 = bitcast i32* %prevSlot168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %182) #8
  %183 = load i32, i32* %sbB, align 4, !tbaa !28
  %call169 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %183)
  %184 = load i32, i32* %call169, align 4, !tbaa !28
  store i32 %184, i32* %prevSlot168, align 4, !tbaa !28
  %185 = load i32, i32* %slotB, align 4, !tbaa !28
  %186 = load i32, i32* %sbB, align 4, !tbaa !28
  %call170 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %186)
  store i32 %185, i32* %call170, align 4, !tbaa !28
  %187 = load i32, i32* %prevSlot168, align 4, !tbaa !28
  %188 = load i32, i32* %slotB, align 4, !tbaa !28
  %call171 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %188)
  %nextJointNodeIndex172 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call171, i32 0, i32 2
  store i32 %187, i32* %nextJointNodeIndex172, align 4, !tbaa !62
  %189 = load i32, i32* %c, align 4, !tbaa !28
  %190 = load i32, i32* %slotB, align 4, !tbaa !28
  %call173 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %190)
  %jointIndex174 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call173, i32 0, i32 0
  store i32 %189, i32* %jointIndex174, align 4, !tbaa !64
  %191 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %tobool175 = icmp ne %class.btRigidBody* %191, null
  br i1 %tobool175, label %cond.true176, label %cond.false177

cond.true176:                                     ; preds = %if.then164
  %192 = load i32, i32* %sbA, align 4, !tbaa !28
  br label %cond.end178

cond.false177:                                    ; preds = %if.then164
  br label %cond.end178

cond.end178:                                      ; preds = %cond.false177, %cond.true176
  %cond179 = phi i32 [ %192, %cond.true176 ], [ -1, %cond.false177 ]
  %193 = load i32, i32* %slotB, align 4, !tbaa !28
  %call180 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %193)
  %otherBodyIndex181 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call180, i32 0, i32 1
  store i32 %cond179, i32* %otherBodyIndex181, align 4, !tbaa !66
  %194 = load i32, i32* %i84, align 4, !tbaa !28
  %195 = load i32, i32* %slotB, align 4, !tbaa !28
  %call182 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %195)
  %constraintRowIndex183 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call182, i32 0, i32 3
  store i32 %194, i32* %constraintRowIndex183, align 4, !tbaa !65
  %196 = bitcast i32* %prevSlot168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #8
  %197 = bitcast i32* %slotB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #8
  %198 = bitcast i32* %row184 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #8
  store i32 0, i32* %row184, align 4, !tbaa !28
  br label %for.cond185

for.cond185:                                      ; preds = %for.inc224, %cond.end178
  %199 = load i32, i32* %row184, align 4, !tbaa !28
  %200 = load i32, i32* %numRows, align 4, !tbaa !28
  %cmp186 = icmp slt i32 %199, %200
  br i1 %cmp186, label %for.body188, label %for.cond.cleanup187

for.cond.cleanup187:                              ; preds = %for.cond185
  store i32 17, i32* %cleanup.dest.slot, align 4
  %201 = bitcast i32* %row184 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #8
  br label %for.end227

for.body188:                                      ; preds = %for.cond185
  %202 = bitcast %class.btVector3* %normalInvMassB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %202) #8
  %m_allConstraintArray189 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %203 = load i32, i32* %i84, align 4, !tbaa !28
  %204 = load i32, i32* %row184, align 4, !tbaa !28
  %add190 = add nsw i32 %203, %204
  %call191 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray189, i32 %add190)
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call191, i32 0, i32 3
  %205 = bitcast float* %ref.tmp192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %205) #8
  %206 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %call193 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %206)
  store float %call193, float* %ref.tmp192, align 4, !tbaa !42
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %normalInvMassB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, float* nonnull align 4 dereferenceable(4) %ref.tmp192)
  %207 = bitcast float* %ref.tmp192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #8
  %208 = bitcast %class.btVector3* %relPosInvInertiaB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %208) #8
  %m_allConstraintArray194 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %209 = load i32, i32* %i84, align 4, !tbaa !28
  %210 = load i32, i32* %row184, align 4, !tbaa !28
  %add195 = add nsw i32 %209, %210
  %call196 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray194, i32 %add195)
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call196, i32 0, i32 2
  %211 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %call197 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %211)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %relPosInvInertiaB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call197)
  %212 = bitcast i32* %r198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %212) #8
  store i32 0, i32* %r198, align 4, !tbaa !28
  br label %for.cond199

for.cond199:                                      ; preds = %for.inc221, %for.body188
  %213 = load i32, i32* %r198, align 4, !tbaa !28
  %cmp200 = icmp slt i32 %213, 3
  br i1 %cmp200, label %for.body202, label %for.cond.cleanup201

for.cond.cleanup201:                              ; preds = %for.cond199
  store i32 20, i32* %cleanup.dest.slot, align 4
  %214 = bitcast i32* %r198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #8
  br label %for.end223

for.body202:                                      ; preds = %for.cond199
  %215 = load i32, i32* %cur, align 4, !tbaa !28
  %216 = load i32, i32* %r198, align 4, !tbaa !28
  %m_allConstraintArray203 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %217 = load i32, i32* %i84, align 4, !tbaa !28
  %218 = load i32, i32* %row184, align 4, !tbaa !28
  %add204 = add nsw i32 %217, %218
  %call205 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray203, i32 %add204)
  %m_contactNormal2206 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call205, i32 0, i32 3
  %call207 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2206)
  %219 = load i32, i32* %r198, align 4, !tbaa !28
  %arrayidx208 = getelementptr inbounds float, float* %call207, i32 %219
  %220 = load float, float* %arrayidx208, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %215, i32 %216, float %220)
  %221 = load i32, i32* %cur, align 4, !tbaa !28
  %222 = load i32, i32* %r198, align 4, !tbaa !28
  %add209 = add nsw i32 %222, 4
  %m_allConstraintArray210 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %223 = load i32, i32* %i84, align 4, !tbaa !28
  %224 = load i32, i32* %row184, align 4, !tbaa !28
  %add211 = add nsw i32 %223, %224
  %call212 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray210, i32 %add211)
  %m_relpos2CrossNormal213 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call212, i32 0, i32 2
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal213)
  %225 = load i32, i32* %r198, align 4, !tbaa !28
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 %225
  %226 = load float, float* %arrayidx215, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %221, i32 %add209, float %226)
  %227 = load i32, i32* %cur, align 4, !tbaa !28
  %228 = load i32, i32* %r198, align 4, !tbaa !28
  %call216 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalInvMassB)
  %229 = load i32, i32* %r198, align 4, !tbaa !28
  %arrayidx217 = getelementptr inbounds float, float* %call216, i32 %229
  %230 = load float, float* %arrayidx217, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %227, i32 %228, float %230)
  %231 = load i32, i32* %cur, align 4, !tbaa !28
  %232 = load i32, i32* %r198, align 4, !tbaa !28
  %add218 = add nsw i32 %232, 4
  %call219 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %relPosInvInertiaB)
  %233 = load i32, i32* %r198, align 4, !tbaa !28
  %arrayidx220 = getelementptr inbounds float, float* %call219, i32 %233
  %234 = load float, float* %arrayidx220, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %231, i32 %add218, float %234)
  br label %for.inc221

for.inc221:                                       ; preds = %for.body202
  %235 = load i32, i32* %r198, align 4, !tbaa !28
  %inc222 = add nsw i32 %235, 1
  store i32 %inc222, i32* %r198, align 4, !tbaa !28
  br label %for.cond199

for.end223:                                       ; preds = %for.cond.cleanup201
  %236 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %236, i32 3, float 0.000000e+00)
  %237 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %237, i32 3, float 0.000000e+00)
  %238 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3, i32 %238, i32 7, float 0.000000e+00)
  %239 = load i32, i32* %cur, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3, i32 %239, i32 7, float 0.000000e+00)
  %240 = bitcast %class.btVector3* %relPosInvInertiaB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %240) #8
  %241 = bitcast %class.btVector3* %normalInvMassB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %241) #8
  br label %for.inc224

for.inc224:                                       ; preds = %for.end223
  %242 = load i32, i32* %row184, align 4, !tbaa !28
  %inc225 = add nsw i32 %242, 1
  store i32 %inc225, i32* %row184, align 4, !tbaa !28
  %243 = load i32, i32* %cur, align 4, !tbaa !28
  %inc226 = add nsw i32 %243, 1
  store i32 %inc226, i32* %cur, align 4, !tbaa !28
  br label %for.cond185

for.end227:                                       ; preds = %for.cond.cleanup187
  br label %if.end230

if.else228:                                       ; preds = %if.end162
  %244 = load i32, i32* %numRows, align 4, !tbaa !28
  %245 = load i32, i32* %cur, align 4, !tbaa !28
  %add229 = add nsw i32 %245, %244
  store i32 %add229, i32* %cur, align 4, !tbaa !28
  br label %if.end230

if.end230:                                        ; preds = %if.else228, %for.end227
  %246 = load i32, i32* %numRows, align 4, !tbaa !28
  %247 = load i32, i32* %rowOffset, align 4, !tbaa !28
  %add231 = add nsw i32 %247, %246
  store i32 %add231, i32* %rowOffset, align 4, !tbaa !28
  %248 = bitcast %class.btRigidBody** %orgBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #8
  %249 = bitcast %class.btRigidBody** %orgBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #8
  %250 = bitcast i32* %sbB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #8
  %251 = bitcast i32* %sbA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #8
  br label %for.inc232

for.inc232:                                       ; preds = %if.end230
  %252 = load i32, i32* %numRows, align 4, !tbaa !28
  %253 = load i32, i32* %i84, align 4, !tbaa !28
  %add233 = add nsw i32 %253, %252
  store i32 %add233, i32* %i84, align 4, !tbaa !28
  %254 = load i32, i32* %c, align 4, !tbaa !28
  %inc234 = add nsw i32 %254, 1
  store i32 %inc234, i32* %c, align 4, !tbaa !28
  br label %for.cond85

for.end235:                                       ; preds = %for.cond.cleanup89
  %255 = bitcast i32* %numRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #8
  %256 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #8
  %call236 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile82) #8
  %257 = bitcast %class.CProfileSample* %__profile82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %257) #8
  %258 = bitcast float** %JinvM to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %258) #8
  %call237 = call float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3)
  store float* %call237, float** %JinvM, align 4, !tbaa !2
  %259 = bitcast float** %Jptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %259) #8
  %call238 = call float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3)
  store float* %call238, float** %Jptr, align 4, !tbaa !2
  %260 = bitcast %class.CProfileSample* %__profile239 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %260) #8
  %call240 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile239, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.13, i32 0, i32 0))
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %261 = load i32, i32* %n, align 4, !tbaa !28
  %262 = load i32, i32* %n, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %m_A, i32 %261, i32 %262)
  %call241 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile239) #8
  %263 = bitcast %class.CProfileSample* %__profile239 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %263) #8
  %264 = bitcast %class.CProfileSample* %__profile242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %264) #8
  %call243 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile242, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.14, i32 0, i32 0))
  %m_A244 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %m_A244)
  %call245 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile242) #8
  %265 = bitcast %class.CProfileSample* %__profile242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %265) #8
  %266 = bitcast i32* %c246 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %266) #8
  store i32 0, i32* %c246, align 4, !tbaa !28
  %267 = bitcast i32* %numRows247 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %267) #8
  store i32 0, i32* %numRows247, align 4, !tbaa !28
  %268 = bitcast %class.CProfileSample* %__profile248 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %268) #8
  %call249 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile248, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.15, i32 0, i32 0))
  %269 = bitcast i32* %i250 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %269) #8
  store i32 0, i32* %i250, align 4, !tbaa !28
  br label %for.cond251

for.cond251:                                      ; preds = %for.inc364, %for.end235
  %270 = load i32, i32* %i250, align 4, !tbaa !28
  %m_allConstraintArray252 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call253 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray252)
  %cmp254 = icmp slt i32 %270, %call253
  br i1 %cmp254, label %for.body256, label %for.cond.cleanup255

for.cond.cleanup255:                              ; preds = %for.cond251
  store i32 23, i32* %cleanup.dest.slot, align 4
  %271 = bitcast i32* %i250 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #8
  br label %for.end367

for.body256:                                      ; preds = %for.cond251
  %272 = bitcast i32* %row__ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %272) #8
  %273 = load i32, i32* %c246, align 4, !tbaa !28
  %call257 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %273)
  %274 = load i32, i32* %call257, align 4, !tbaa !28
  store i32 %274, i32* %row__, align 4, !tbaa !28
  %275 = bitcast i32* %sbA258 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %275) #8
  %m_allConstraintArray259 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %276 = load i32, i32* %i250, align 4, !tbaa !28
  %call260 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray259, i32 %276)
  %m_solverBodyIdA261 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call260, i32 0, i32 18
  %277 = load i32, i32* %m_solverBodyIdA261, align 4, !tbaa !54
  store i32 %277, i32* %sbA258, align 4, !tbaa !28
  %278 = bitcast i32* %sbB262 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %278) #8
  %m_allConstraintArray263 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %279 = load i32, i32* %i250, align 4, !tbaa !28
  %call264 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray263, i32 %279)
  %m_solverBodyIdB265 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call264, i32 0, i32 19
  %280 = load i32, i32* %m_solverBodyIdB265, align 4, !tbaa !55
  store i32 %280, i32* %sbB262, align 4, !tbaa !28
  %281 = bitcast %class.btRigidBody** %orgBodyA266 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %281) #8
  %282 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool267 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %282, i32 0, i32 1
  %283 = load i32, i32* %sbA258, align 4, !tbaa !28
  %call268 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool267, i32 %283)
  %m_originalBody269 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call268, i32 0, i32 12
  %284 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody269, align 4, !tbaa !56
  store %class.btRigidBody* %284, %class.btRigidBody** %orgBodyA266, align 4, !tbaa !2
  %285 = bitcast %class.btRigidBody** %orgBodyB270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %285) #8
  %286 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool271 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %286, i32 0, i32 1
  %287 = load i32, i32* %sbB262, align 4, !tbaa !28
  %call272 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool271, i32 %287)
  %m_originalBody273 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call272, i32 0, i32 12
  %288 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody273, align 4, !tbaa !56
  store %class.btRigidBody* %288, %class.btRigidBody** %orgBodyB270, align 4, !tbaa !2
  %289 = load i32, i32* %i250, align 4, !tbaa !28
  %290 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool274 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %290, i32 0, i32 3
  %call275 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool274)
  %cmp276 = icmp slt i32 %289, %call275
  br i1 %cmp276, label %cond.true277, label %cond.false281

cond.true277:                                     ; preds = %for.body256
  %291 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool278 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %291, i32 0, i32 9
  %292 = load i32, i32* %c246, align 4, !tbaa !28
  %call279 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %m_tmpConstraintSizesPool278, i32 %292)
  %m_numConstraintRows280 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call279, i32 0, i32 0
  %293 = load i32, i32* %m_numConstraintRows280, align 4, !tbaa !60
  br label %cond.end282

cond.false281:                                    ; preds = %for.body256
  %294 = load i32, i32* %numContactRows, align 4, !tbaa !28
  br label %cond.end282

cond.end282:                                      ; preds = %cond.false281, %cond.true277
  %cond283 = phi i32 [ %293, %cond.true277 ], [ %294, %cond.false281 ]
  store i32 %cond283, i32* %numRows247, align 4, !tbaa !28
  %295 = bitcast float** %JinvMrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %295) #8
  %296 = load float*, float** %JinvM, align 4, !tbaa !2
  %297 = load i32, i32* %row__, align 4, !tbaa !28
  %mul284 = mul i32 16, %297
  %add.ptr = getelementptr inbounds float, float* %296, i32 %mul284
  store float* %add.ptr, float** %JinvMrow, align 4, !tbaa !2
  %298 = bitcast i32* %startJointNodeA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %298) #8
  %299 = load i32, i32* %sbA258, align 4, !tbaa !28
  %call285 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %299)
  %300 = load i32, i32* %call285, align 4, !tbaa !28
  store i32 %300, i32* %startJointNodeA, align 4, !tbaa !28
  br label %while.cond

while.cond:                                       ; preds = %if.end318, %cond.end282
  %301 = load i32, i32* %startJointNodeA, align 4, !tbaa !28
  %cmp286 = icmp sge i32 %301, 0
  br i1 %cmp286, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %302 = bitcast i32* %j0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #8
  %303 = load i32, i32* %startJointNodeA, align 4, !tbaa !28
  %call287 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %303)
  %jointIndex288 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call287, i32 0, i32 0
  %304 = load i32, i32* %jointIndex288, align 4, !tbaa !64
  store i32 %304, i32* %j0, align 4, !tbaa !28
  %305 = bitcast i32* %cr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #8
  %306 = load i32, i32* %startJointNodeA, align 4, !tbaa !28
  %call289 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %306)
  %constraintRowIndex290 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call289, i32 0, i32 3
  %307 = load i32, i32* %constraintRowIndex290, align 4, !tbaa !65
  store i32 %307, i32* %cr0, align 4, !tbaa !28
  %308 = load i32, i32* %j0, align 4, !tbaa !28
  %309 = load i32, i32* %c246, align 4, !tbaa !28
  %cmp291 = icmp slt i32 %308, %309
  br i1 %cmp291, label %if.then292, label %if.end318

if.then292:                                       ; preds = %while.body
  %310 = bitcast i32* %numRowsOther to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %310) #8
  %311 = load i32, i32* %cr0, align 4, !tbaa !28
  %312 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool293 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %312, i32 0, i32 3
  %call294 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool293)
  %cmp295 = icmp slt i32 %311, %call294
  br i1 %cmp295, label %cond.true296, label %cond.false300

cond.true296:                                     ; preds = %if.then292
  %313 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool297 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %313, i32 0, i32 9
  %314 = load i32, i32* %j0, align 4, !tbaa !28
  %call298 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %m_tmpConstraintSizesPool297, i32 %314)
  %m_numConstraintRows299 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call298, i32 0, i32 0
  %315 = load i32, i32* %m_numConstraintRows299, align 4, !tbaa !60
  br label %cond.end301

cond.false300:                                    ; preds = %if.then292
  %316 = load i32, i32* %numContactRows, align 4, !tbaa !28
  br label %cond.end301

cond.end301:                                      ; preds = %cond.false300, %cond.true296
  %cond302 = phi i32 [ %315, %cond.true296 ], [ %316, %cond.false300 ]
  store i32 %cond302, i32* %numRowsOther, align 4, !tbaa !28
  %317 = bitcast i32* %ofsother to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %317) #8
  %m_allConstraintArray303 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %318 = load i32, i32* %cr0, align 4, !tbaa !28
  %call304 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray303, i32 %318)
  %m_solverBodyIdB305 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call304, i32 0, i32 19
  %319 = load i32, i32* %m_solverBodyIdB305, align 4, !tbaa !55
  %320 = load i32, i32* %sbA258, align 4, !tbaa !28
  %cmp306 = icmp eq i32 %319, %320
  br i1 %cmp306, label %cond.true307, label %cond.false309

cond.true307:                                     ; preds = %cond.end301
  %321 = load i32, i32* %numRowsOther, align 4, !tbaa !28
  %mul308 = mul nsw i32 8, %321
  br label %cond.end310

cond.false309:                                    ; preds = %cond.end301
  br label %cond.end310

cond.end310:                                      ; preds = %cond.false309, %cond.true307
  %cond311 = phi i32 [ %mul308, %cond.true307 ], [ 0, %cond.false309 ]
  store i32 %cond311, i32* %ofsother, align 4, !tbaa !67
  %m_A312 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %322 = load float*, float** %JinvMrow, align 4, !tbaa !2
  %323 = load float*, float** %Jptr, align 4, !tbaa !2
  %324 = load i32, i32* %j0, align 4, !tbaa !28
  %call313 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %324)
  %325 = load i32, i32* %call313, align 4, !tbaa !28
  %mul314 = mul i32 16, %325
  %add.ptr315 = getelementptr inbounds float, float* %323, i32 %mul314
  %326 = load i32, i32* %ofsother, align 4, !tbaa !67
  %add.ptr316 = getelementptr inbounds float, float* %add.ptr315, i32 %326
  %327 = load i32, i32* %numRows247, align 4, !tbaa !28
  %328 = load i32, i32* %numRowsOther, align 4, !tbaa !28
  %329 = load i32, i32* %row__, align 4, !tbaa !28
  %330 = load i32, i32* %j0, align 4, !tbaa !28
  %call317 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %330)
  %331 = load i32, i32* %call317, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A312, float* %322, float* %add.ptr316, i32 %327, i32 %328, i32 %329, i32 %331)
  %332 = bitcast i32* %ofsother to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #8
  %333 = bitcast i32* %numRowsOther to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #8
  br label %if.end318

if.end318:                                        ; preds = %cond.end310, %while.body
  %334 = load i32, i32* %startJointNodeA, align 4, !tbaa !28
  %call319 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %334)
  %nextJointNodeIndex320 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call319, i32 0, i32 2
  %335 = load i32, i32* %nextJointNodeIndex320, align 4, !tbaa !62
  store i32 %335, i32* %startJointNodeA, align 4, !tbaa !28
  %336 = bitcast i32* %cr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #8
  %337 = bitcast i32* %j0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %338 = bitcast i32* %startJointNodeA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #8
  %339 = bitcast i32* %startJointNodeB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %339) #8
  %340 = load i32, i32* %sbB262, align 4, !tbaa !28
  %call321 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %bodyJointNodeArray, i32 %340)
  %341 = load i32, i32* %call321, align 4, !tbaa !28
  store i32 %341, i32* %startJointNodeB, align 4, !tbaa !28
  br label %while.cond322

while.cond322:                                    ; preds = %if.end360, %while.end
  %342 = load i32, i32* %startJointNodeB, align 4, !tbaa !28
  %cmp323 = icmp sge i32 %342, 0
  br i1 %cmp323, label %while.body324, label %while.end363

while.body324:                                    ; preds = %while.cond322
  %343 = bitcast i32* %j1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %343) #8
  %344 = load i32, i32* %startJointNodeB, align 4, !tbaa !28
  %call325 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %344)
  %jointIndex326 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call325, i32 0, i32 0
  %345 = load i32, i32* %jointIndex326, align 4, !tbaa !64
  store i32 %345, i32* %j1, align 4, !tbaa !28
  %346 = bitcast i32* %cj1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %346) #8
  %347 = load i32, i32* %startJointNodeB, align 4, !tbaa !28
  %call327 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %347)
  %constraintRowIndex328 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call327, i32 0, i32 3
  %348 = load i32, i32* %constraintRowIndex328, align 4, !tbaa !65
  store i32 %348, i32* %cj1, align 4, !tbaa !28
  %349 = load i32, i32* %j1, align 4, !tbaa !28
  %350 = load i32, i32* %c246, align 4, !tbaa !28
  %cmp329 = icmp slt i32 %349, %350
  br i1 %cmp329, label %if.then330, label %if.end360

if.then330:                                       ; preds = %while.body324
  %351 = bitcast i32* %numRowsOther331 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %351) #8
  %352 = load i32, i32* %cj1, align 4, !tbaa !28
  %353 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool332 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %353, i32 0, i32 3
  %call333 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool332)
  %cmp334 = icmp slt i32 %352, %call333
  br i1 %cmp334, label %cond.true335, label %cond.false339

cond.true335:                                     ; preds = %if.then330
  %354 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool336 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %354, i32 0, i32 9
  %355 = load i32, i32* %j1, align 4, !tbaa !28
  %call337 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %m_tmpConstraintSizesPool336, i32 %355)
  %m_numConstraintRows338 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call337, i32 0, i32 0
  %356 = load i32, i32* %m_numConstraintRows338, align 4, !tbaa !60
  br label %cond.end340

cond.false339:                                    ; preds = %if.then330
  %357 = load i32, i32* %numContactRows, align 4, !tbaa !28
  br label %cond.end340

cond.end340:                                      ; preds = %cond.false339, %cond.true335
  %cond341 = phi i32 [ %356, %cond.true335 ], [ %357, %cond.false339 ]
  store i32 %cond341, i32* %numRowsOther331, align 4, !tbaa !28
  %358 = bitcast i32* %ofsother342 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %358) #8
  %m_allConstraintArray343 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %359 = load i32, i32* %cj1, align 4, !tbaa !28
  %call344 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray343, i32 %359)
  %m_solverBodyIdB345 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call344, i32 0, i32 19
  %360 = load i32, i32* %m_solverBodyIdB345, align 4, !tbaa !55
  %361 = load i32, i32* %sbB262, align 4, !tbaa !28
  %cmp346 = icmp eq i32 %360, %361
  br i1 %cmp346, label %cond.true347, label %cond.false349

cond.true347:                                     ; preds = %cond.end340
  %362 = load i32, i32* %numRowsOther331, align 4, !tbaa !28
  %mul348 = mul nsw i32 8, %362
  br label %cond.end350

cond.false349:                                    ; preds = %cond.end340
  br label %cond.end350

cond.end350:                                      ; preds = %cond.false349, %cond.true347
  %cond351 = phi i32 [ %mul348, %cond.true347 ], [ 0, %cond.false349 ]
  store i32 %cond351, i32* %ofsother342, align 4, !tbaa !67
  %m_A352 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %363 = load float*, float** %JinvMrow, align 4, !tbaa !2
  %364 = load i32, i32* %numRows247, align 4, !tbaa !28
  %mul353 = mul i32 8, %364
  %add.ptr354 = getelementptr inbounds float, float* %363, i32 %mul353
  %365 = load float*, float** %Jptr, align 4, !tbaa !2
  %366 = load i32, i32* %j1, align 4, !tbaa !28
  %call355 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %366)
  %367 = load i32, i32* %call355, align 4, !tbaa !28
  %mul356 = mul i32 16, %367
  %add.ptr357 = getelementptr inbounds float, float* %365, i32 %mul356
  %368 = load i32, i32* %ofsother342, align 4, !tbaa !67
  %add.ptr358 = getelementptr inbounds float, float* %add.ptr357, i32 %368
  %369 = load i32, i32* %numRows247, align 4, !tbaa !28
  %370 = load i32, i32* %numRowsOther331, align 4, !tbaa !28
  %371 = load i32, i32* %row__, align 4, !tbaa !28
  %372 = load i32, i32* %j1, align 4, !tbaa !28
  %call359 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs, i32 %372)
  %373 = load i32, i32* %call359, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A352, float* %add.ptr354, float* %add.ptr358, i32 %369, i32 %370, i32 %371, i32 %373)
  %374 = bitcast i32* %ofsother342 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #8
  %375 = bitcast i32* %numRowsOther331 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #8
  br label %if.end360

if.end360:                                        ; preds = %cond.end350, %while.body324
  %376 = load i32, i32* %startJointNodeB, align 4, !tbaa !28
  %call361 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %jointNodeArray, i32 %376)
  %nextJointNodeIndex362 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call361, i32 0, i32 2
  %377 = load i32, i32* %nextJointNodeIndex362, align 4, !tbaa !62
  store i32 %377, i32* %startJointNodeB, align 4, !tbaa !28
  %378 = bitcast i32* %cj1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %378) #8
  %379 = bitcast i32* %j1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #8
  br label %while.cond322

while.end363:                                     ; preds = %while.cond322
  %380 = bitcast i32* %startJointNodeB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %380) #8
  %381 = bitcast float** %JinvMrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %381) #8
  %382 = bitcast %class.btRigidBody** %orgBodyB270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %382) #8
  %383 = bitcast %class.btRigidBody** %orgBodyA266 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %383) #8
  %384 = bitcast i32* %sbB262 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #8
  %385 = bitcast i32* %sbA258 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #8
  %386 = bitcast i32* %row__ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #8
  br label %for.inc364

for.inc364:                                       ; preds = %while.end363
  %387 = load i32, i32* %numRows247, align 4, !tbaa !28
  %388 = load i32, i32* %i250, align 4, !tbaa !28
  %add365 = add nsw i32 %388, %387
  store i32 %add365, i32* %i250, align 4, !tbaa !28
  %389 = load i32, i32* %c246, align 4, !tbaa !28
  %inc366 = add nsw i32 %389, 1
  store i32 %inc366, i32* %c246, align 4, !tbaa !28
  br label %for.cond251

for.end367:                                       ; preds = %for.cond.cleanup255
  %390 = bitcast %class.CProfileSample* %__profile368 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %390) #8
  %call369 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile368, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.16, i32 0, i32 0))
  %391 = bitcast i32* %row__370 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %391) #8
  store i32 0, i32* %row__370, align 4, !tbaa !28
  %392 = bitcast i32* %numJointRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %392) #8
  %m_allConstraintArray371 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call372 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray371)
  store i32 %call372, i32* %numJointRows, align 4, !tbaa !28
  %393 = bitcast i32* %jj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %393) #8
  store i32 0, i32* %jj, align 4, !tbaa !28
  br label %for.cond373

for.cond373:                                      ; preds = %if.end415, %for.end367
  %394 = load i32, i32* %row__370, align 4, !tbaa !28
  %395 = load i32, i32* %numJointRows, align 4, !tbaa !28
  %cmp374 = icmp slt i32 %394, %395
  br i1 %cmp374, label %for.body375, label %for.end418

for.body375:                                      ; preds = %for.cond373
  %396 = bitcast i32* %sbA376 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %396) #8
  %m_allConstraintArray377 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %397 = load i32, i32* %row__370, align 4, !tbaa !28
  %call378 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray377, i32 %397)
  %m_solverBodyIdA379 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call378, i32 0, i32 18
  %398 = load i32, i32* %m_solverBodyIdA379, align 4, !tbaa !54
  store i32 %398, i32* %sbA376, align 4, !tbaa !28
  %399 = bitcast i32* %sbB380 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %399) #8
  %m_allConstraintArray381 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %400 = load i32, i32* %row__370, align 4, !tbaa !28
  %call382 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray381, i32 %400)
  %m_solverBodyIdB383 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call382, i32 0, i32 19
  %401 = load i32, i32* %m_solverBodyIdB383, align 4, !tbaa !55
  store i32 %401, i32* %sbB380, align 4, !tbaa !28
  %402 = bitcast %class.btRigidBody** %orgBodyA384 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %402) #8
  %403 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool385 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %403, i32 0, i32 1
  %404 = load i32, i32* %sbA376, align 4, !tbaa !28
  %call386 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool385, i32 %404)
  %m_originalBody387 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call386, i32 0, i32 12
  %405 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody387, align 4, !tbaa !56
  store %class.btRigidBody* %405, %class.btRigidBody** %orgBodyA384, align 4, !tbaa !2
  %406 = bitcast %class.btRigidBody** %orgBodyB388 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %406) #8
  %407 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool389 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %407, i32 0, i32 1
  %408 = load i32, i32* %sbB380, align 4, !tbaa !28
  %call390 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool389, i32 %408)
  %m_originalBody391 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call390, i32 0, i32 12
  %409 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody391, align 4, !tbaa !56
  store %class.btRigidBody* %409, %class.btRigidBody** %orgBodyB388, align 4, !tbaa !2
  %410 = bitcast i32* %infom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %410) #8
  %411 = load i32, i32* %row__370, align 4, !tbaa !28
  %412 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool392 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %412, i32 0, i32 3
  %call393 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_tmpSolverNonContactConstraintPool392)
  %cmp394 = icmp slt i32 %411, %call393
  br i1 %cmp394, label %cond.true395, label %cond.false399

cond.true395:                                     ; preds = %for.body375
  %413 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool396 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %413, i32 0, i32 9
  %414 = load i32, i32* %jj, align 4, !tbaa !28
  %call397 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %m_tmpConstraintSizesPool396, i32 %414)
  %m_numConstraintRows398 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call397, i32 0, i32 0
  %415 = load i32, i32* %m_numConstraintRows398, align 4, !tbaa !60
  br label %cond.end400

cond.false399:                                    ; preds = %for.body375
  %416 = load i32, i32* %numContactRows, align 4, !tbaa !28
  br label %cond.end400

cond.end400:                                      ; preds = %cond.false399, %cond.true395
  %cond401 = phi i32 [ %415, %cond.true395 ], [ %416, %cond.false399 ]
  store i32 %cond401, i32* %infom, align 4, !tbaa !28
  %417 = bitcast float** %JinvMrow402 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %417) #8
  %418 = load float*, float** %JinvM, align 4, !tbaa !2
  %419 = load i32, i32* %row__370, align 4, !tbaa !28
  %mul403 = mul i32 16, %419
  %add.ptr404 = getelementptr inbounds float, float* %418, i32 %mul403
  store float* %add.ptr404, float** %JinvMrow402, align 4, !tbaa !2
  %420 = bitcast float** %Jrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %420) #8
  %421 = load float*, float** %Jptr, align 4, !tbaa !2
  %422 = load i32, i32* %row__370, align 4, !tbaa !28
  %mul405 = mul i32 16, %422
  %add.ptr406 = getelementptr inbounds float, float* %421, i32 %mul405
  store float* %add.ptr406, float** %Jrow, align 4, !tbaa !2
  %m_A407 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %423 = load float*, float** %JinvMrow402, align 4, !tbaa !2
  %424 = load float*, float** %Jrow, align 4, !tbaa !2
  %425 = load i32, i32* %infom, align 4, !tbaa !28
  %426 = load i32, i32* %infom, align 4, !tbaa !28
  %427 = load i32, i32* %row__370, align 4, !tbaa !28
  %428 = load i32, i32* %row__370, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A407, float* %423, float* %424, i32 %425, i32 %426, i32 %427, i32 %428)
  %429 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB388, align 4, !tbaa !2
  %tobool408 = icmp ne %class.btRigidBody* %429, null
  br i1 %tobool408, label %if.then409, label %if.end415

if.then409:                                       ; preds = %cond.end400
  %m_A410 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %430 = load float*, float** %JinvMrow402, align 4, !tbaa !2
  %431 = load i32, i32* %infom, align 4, !tbaa !28
  %mul411 = mul i32 8, %431
  %add.ptr412 = getelementptr inbounds float, float* %430, i32 %mul411
  %432 = load float*, float** %Jrow, align 4, !tbaa !2
  %433 = load i32, i32* %infom, align 4, !tbaa !28
  %mul413 = mul i32 8, %433
  %add.ptr414 = getelementptr inbounds float, float* %432, i32 %mul413
  %434 = load i32, i32* %infom, align 4, !tbaa !28
  %435 = load i32, i32* %infom, align 4, !tbaa !28
  %436 = load i32, i32* %row__370, align 4, !tbaa !28
  %437 = load i32, i32* %row__370, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A410, float* %add.ptr412, float* %add.ptr414, i32 %434, i32 %435, i32 %436, i32 %437)
  br label %if.end415

if.end415:                                        ; preds = %if.then409, %cond.end400
  %438 = load i32, i32* %infom, align 4, !tbaa !28
  %439 = load i32, i32* %row__370, align 4, !tbaa !28
  %add416 = add i32 %439, %438
  store i32 %add416, i32* %row__370, align 4, !tbaa !28
  %440 = load i32, i32* %jj, align 4, !tbaa !28
  %inc417 = add nsw i32 %440, 1
  store i32 %inc417, i32* %jj, align 4, !tbaa !28
  %441 = bitcast float** %Jrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %441) #8
  %442 = bitcast float** %JinvMrow402 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %442) #8
  %443 = bitcast i32* %infom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #8
  %444 = bitcast %class.btRigidBody** %orgBodyB388 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #8
  %445 = bitcast %class.btRigidBody** %orgBodyA384 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #8
  %446 = bitcast i32* %sbB380 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %446) #8
  %447 = bitcast i32* %sbA376 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %447) #8
  br label %for.cond373

for.end418:                                       ; preds = %for.cond373
  %448 = bitcast i32* %jj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %448) #8
  %449 = bitcast i32* %numJointRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %449) #8
  %450 = bitcast i32* %row__370 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %450) #8
  %call419 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile368) #8
  %451 = bitcast %class.CProfileSample* %__profile368 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %451) #8
  %call420 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile248) #8
  %452 = bitcast %class.CProfileSample* %__profile248 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %452) #8
  %453 = bitcast i32* %numRows247 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #8
  %454 = bitcast i32* %i421 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %454) #8
  store i32 0, i32* %i421, align 4, !tbaa !28
  br label %for.cond422

for.cond422:                                      ; preds = %for.inc433, %for.end418
  %455 = load i32, i32* %i421, align 4, !tbaa !28
  %m_A423 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call424 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A423)
  %cmp425 = icmp slt i32 %455, %call424
  br i1 %cmp425, label %for.body427, label %for.cond.cleanup426

for.cond.cleanup426:                              ; preds = %for.cond422
  store i32 32, i32* %cleanup.dest.slot, align 4
  %456 = bitcast i32* %i421 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %456) #8
  br label %for.end435

for.body427:                                      ; preds = %for.cond422
  %457 = bitcast float* %cfm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %457) #8
  store float 0x3EE4F8B580000000, float* %cfm, align 4, !tbaa !42
  %m_A428 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %458 = load i32, i32* %i421, align 4, !tbaa !28
  %459 = load i32, i32* %i421, align 4, !tbaa !28
  %m_A429 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %460 = load i32, i32* %i421, align 4, !tbaa !28
  %461 = load i32, i32* %i421, align 4, !tbaa !28
  %call430 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %m_A429, i32 %460, i32 %461)
  %462 = load float, float* %call430, align 4, !tbaa !42
  %463 = load float, float* %cfm, align 4, !tbaa !42
  %464 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %465 = bitcast %struct.btContactSolverInfo* %464 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %465, i32 0, i32 3
  %466 = load float, float* %m_timeStep, align 4, !tbaa !69
  %div431 = fdiv float %463, %466
  %add432 = fadd float %462, %div431
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %m_A428, i32 %458, i32 %459, float %add432)
  %467 = bitcast float* %cfm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %467) #8
  br label %for.inc433

for.inc433:                                       ; preds = %for.body427
  %468 = load i32, i32* %i421, align 4, !tbaa !28
  %inc434 = add nsw i32 %468, 1
  store i32 %inc434, i32* %i421, align 4, !tbaa !28
  br label %for.cond422

for.end435:                                       ; preds = %for.cond.cleanup426
  %469 = bitcast %class.CProfileSample* %__profile436 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %469) #8
  %call437 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile436, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.17, i32 0, i32 0))
  %m_A438 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv(%struct.btMatrixX* %m_A438)
  %call439 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile436) #8
  %470 = bitcast %class.CProfileSample* %__profile436 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %470) #8
  %471 = bitcast %class.CProfileSample* %__profile440 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %471) #8
  %call441 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile440, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.18, i32 0, i32 0))
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %472 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 %472)
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %473 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_xSplit, i32 %473)
  %474 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %475 = bitcast %struct.btContactSolverInfo* %474 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %475, i32 0, i32 16
  %476 = load i32, i32* %m_solverMode, align 4, !tbaa !70
  %and = and i32 %476, 4
  %tobool442 = icmp ne i32 %and, 0
  br i1 %tobool442, label %if.then443, label %if.else461

if.then443:                                       ; preds = %for.end435
  %477 = bitcast i32* %i444 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %477) #8
  store i32 0, i32* %i444, align 4, !tbaa !28
  br label %for.cond445

for.cond445:                                      ; preds = %for.inc458, %if.then443
  %478 = load i32, i32* %i444, align 4, !tbaa !28
  %m_allConstraintArray446 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call447 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray446)
  %cmp448 = icmp slt i32 %478, %call447
  br i1 %cmp448, label %for.body450, label %for.cond.cleanup449

for.cond.cleanup449:                              ; preds = %for.cond445
  store i32 35, i32* %cleanup.dest.slot, align 4
  %479 = bitcast i32* %i444 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %479) #8
  br label %for.end460

for.body450:                                      ; preds = %for.cond445
  %480 = bitcast %struct.btSolverConstraint** %c451 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %480) #8
  %m_allConstraintArray452 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %481 = load i32, i32* %i444, align 4, !tbaa !28
  %call453 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray452, i32 %481)
  store %struct.btSolverConstraint* %call453, %struct.btSolverConstraint** %c451, align 4, !tbaa !2
  %482 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c451, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %482, i32 0, i32 7
  %483 = load float, float* %m_appliedImpulse, align 4, !tbaa !71
  %m_x454 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %484 = load i32, i32* %i444, align 4, !tbaa !28
  %call455 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x454, i32 %484)
  store float %483, float* %call455, align 4, !tbaa !42
  %485 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c451, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %485, i32 0, i32 6
  %486 = load float, float* %m_appliedPushImpulse, align 4, !tbaa !72
  %m_xSplit456 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %487 = load i32, i32* %i444, align 4, !tbaa !28
  %call457 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit456, i32 %487)
  store float %486, float* %call457, align 4, !tbaa !42
  %488 = bitcast %struct.btSolverConstraint** %c451 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #8
  br label %for.inc458

for.inc458:                                       ; preds = %for.body450
  %489 = load i32, i32* %i444, align 4, !tbaa !28
  %inc459 = add nsw i32 %489, 1
  store i32 %inc459, i32* %i444, align 4, !tbaa !28
  br label %for.cond445

for.end460:                                       ; preds = %for.cond.cleanup449
  br label %if.end464

if.else461:                                       ; preds = %for.end435
  %m_x462 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_x462)
  %m_xSplit463 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_xSplit463)
  br label %if.end464

if.end464:                                        ; preds = %if.else461, %for.end460
  %call465 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile440) #8
  %490 = bitcast %class.CProfileSample* %__profile440 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %490) #8
  %491 = bitcast i32* %c246 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %491) #8
  %492 = bitcast float** %Jptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %492) #8
  %493 = bitcast float** %JinvM to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %493) #8
  %494 = bitcast i32* %rowOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %494) #8
  %495 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %495) #8
  %call466 = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayI11btJointNodeED2Ev(%class.btAlignedObjectArray.26* %jointNodeArray) #8
  %496 = bitcast %class.btAlignedObjectArray.26* %jointNodeArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %496) #8
  %call467 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %bodyJointNodeArray) #8
  %497 = bitcast %class.btAlignedObjectArray.10* %bodyJointNodeArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %497) #8
  %498 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %498) #8
  %499 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %499) #8
  %500 = bitcast i32* %nub to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %500) #8
  %501 = bitcast float** %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %501) #8
  %502 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %502) #8
  %503 = bitcast i32* %numConstraintRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %503) #8
  %504 = bitcast i32* %numContactRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %504) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %index.addr = alloca i32, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !28
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4, !tbaa !28
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 %0)
  ret float* %call
}

define linkonce_odr hidden %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev(%class.btAlignedObjectArray.26* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.27* @_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev(%class.btAlignedAllocator.27* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.26* %this1)
  ret %class.btAlignedObjectArray.26* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.26* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btJointNode*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.26* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btJointNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi(%class.btAlignedObjectArray.26* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btJointNode*
  store %struct.btJointNode* %3, %struct.btJointNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  %4 = load %struct.btJointNode*, %struct.btJointNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call3, %struct.btJointNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.26* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !73
  %5 = load %struct.btJointNode*, %struct.btJointNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %struct.btJointNode* %5, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !77
  %7 = bitcast %struct.btJointNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #8

define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE2J3) #8
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #8

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #8

define internal void @__cxx_global_array_dtor.8(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE6JinvM3) #8
  ret void
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %__profile6 = alloca %class.CProfileSample, align 1
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #8
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.29, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 0)
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %m_storage3)
  call void @_Z9btSetZeroIfEvPT_i(float* %call2, i32 %call4)
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %1 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %1) #8
  %2 = bitcast %class.CProfileSample* %__profile6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %2) #8
  %call7 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile6, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.28, i32 0, i32 0))
  call void @_ZN9btMatrixXIfE15clearSparseInfoEv(%struct.btMatrixX* %this1)
  %call8 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile6) #8
  %3 = bitcast %class.CProfileSample* %__profile6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %3) #8
  ret void
}

define internal void @__cxx_global_array_dtor.10(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* @_ZZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfoE3ofs) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.10* %this, i32 %newsize) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %curSize = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !28
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %2 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end5

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %cmp3 = icmp sgt i32 %3, %call2
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %4)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %5 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %5, i32* %m_size, align 4, !tbaa !44
  %6 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4, !tbaa !78
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.14* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4, !tbaa !79
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 %1
  ret %"struct.btTypedConstraint::btConstraintInfo1"* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !82
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.26* %this, %struct.btJointNode* nonnull align 4 dereferenceable(16) %fillValue) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %fillValue.addr = alloca %struct.btJointNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store %struct.btJointNode* %fillValue, %struct.btJointNode** %fillValue.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !28
  %1 = load i32, i32* %sz, align 4, !tbaa !28
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.26* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi(%class.btAlignedObjectArray.26* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.26* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !82
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !82
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %3 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %4 = load i32, i32* %sz, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %3, i32 %4
  %5 = bitcast %struct.btJointNode* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btJointNode*
  %7 = load %struct.btJointNode*, %struct.btJointNode** %fillValue.addr, align 4, !tbaa !2
  %8 = bitcast %struct.btJointNode* %6 to i8*
  %9 = bitcast %struct.btJointNode* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !83
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %10 = load %struct.btJointNode*, %struct.btJointNode** %m_data5, align 4, !tbaa !76
  %11 = load i32, i32* %sz, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %10, i32 %11
  %12 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret %struct.btJointNode* %arrayidx6
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.26* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %0, i32 %1
  ret %struct.btJointNode* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !42
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !42
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !42
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !42
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !42
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !42
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !42
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !42
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !42
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !84
  ret float %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !42
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !42
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !42
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  store float %val, float* %val.addr, align 4, !tbaa !42
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_setElemOperations, align 4, !tbaa !27
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_setElemOperations, align 4, !tbaa !27
  %1 = load float, float* %val.addr, align 4, !tbaa !42
  %tobool = fcmp une float %1, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end10

if.then:                                          ; preds = %entry
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %2 = load i32, i32* %col.addr, align 4, !tbaa !28
  %3 = load i32, i32* %row.addr, align 4, !tbaa !28
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_cols, align 4, !tbaa !24
  %mul = mul nsw i32 %3, %4
  %add = add nsw i32 %2, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 %add)
  %5 = load float, float* %call, align 4, !tbaa !42
  %cmp = fcmp oeq float %5, 0.000000e+00
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %6 = load i32, i32* %row.addr, align 4, !tbaa !28
  %call3 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1, i32 %6)
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.10* %call3, i32* nonnull align 4 dereferenceable(4) %col.addr)
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %7 = load i32, i32* %col.addr, align 4, !tbaa !28
  %call4 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements, i32 %7)
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.10* %call4, i32* nonnull align 4 dereferenceable(4) %row.addr)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %8 = load float, float* %val.addr, align 4, !tbaa !42
  %m_storage5 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %9 = load i32, i32* %row.addr, align 4, !tbaa !28
  %m_cols6 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %10 = load i32, i32* %m_cols6, align 4, !tbaa !24
  %mul7 = mul nsw i32 %9, %10
  %11 = load i32, i32* %col.addr, align 4, !tbaa !28
  %add8 = add nsw i32 %mul7, %11
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage5, i32 %add8)
  store float %8, float* %call9, align 4, !tbaa !42
  br label %if.end10

if.end10:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %m_storage)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_storage2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %call3, %cond.true ], [ null, %cond.false ]
  ret float* %cond
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %this, float* %B, float* %C, i32 %numRows, i32 %numRowsOther, i32 %row, i32 %col) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %B.addr = alloca float*, align 4
  %C.addr = alloca float*, align 4
  %numRows.addr = alloca i32, align 4
  %numRowsOther.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %bb = alloca float*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cc = alloca float*, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store float* %C, float** %C.addr, align 4, !tbaa !2
  store i32 %numRows, i32* %numRows.addr, align 4, !tbaa !28
  store i32 %numRowsOther, i32* %numRowsOther.addr, align 4, !tbaa !28
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = bitcast float** %bb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %1, float** %bb, align 4, !tbaa !2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !28
  %4 = load i32, i32* %numRows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  br label %for.end31

for.body:                                         ; preds = %for.cond
  %6 = bitcast float** %cc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float*, float** %C.addr, align 4, !tbaa !2
  store float* %7, float** %cc, align 4, !tbaa !2
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %j, align 4, !tbaa !28
  %10 = load i32, i32* %numRowsOther.addr, align 4, !tbaa !28
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end

for.body5:                                        ; preds = %for.cond2
  %12 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %13, i32 0
  %14 = load float, float* %arrayidx, align 4, !tbaa !42
  %15 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %15, i32 0
  %16 = load float, float* %arrayidx6, align 4, !tbaa !42
  %mul = fmul float %14, %16
  store float %mul, float* %sum, align 4, !tbaa !42
  %17 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %17, i32 1
  %18 = load float, float* %arrayidx7, align 4, !tbaa !42
  %19 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %19, i32 1
  %20 = load float, float* %arrayidx8, align 4, !tbaa !42
  %mul9 = fmul float %18, %20
  %21 = load float, float* %sum, align 4, !tbaa !42
  %add = fadd float %21, %mul9
  store float %add, float* %sum, align 4, !tbaa !42
  %22 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %22, i32 2
  %23 = load float, float* %arrayidx10, align 4, !tbaa !42
  %24 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds float, float* %24, i32 2
  %25 = load float, float* %arrayidx11, align 4, !tbaa !42
  %mul12 = fmul float %23, %25
  %26 = load float, float* %sum, align 4, !tbaa !42
  %add13 = fadd float %26, %mul12
  store float %add13, float* %sum, align 4, !tbaa !42
  %27 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %27, i32 4
  %28 = load float, float* %arrayidx14, align 4, !tbaa !42
  %29 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %29, i32 4
  %30 = load float, float* %arrayidx15, align 4, !tbaa !42
  %mul16 = fmul float %28, %30
  %31 = load float, float* %sum, align 4, !tbaa !42
  %add17 = fadd float %31, %mul16
  store float %add17, float* %sum, align 4, !tbaa !42
  %32 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %32, i32 5
  %33 = load float, float* %arrayidx18, align 4, !tbaa !42
  %34 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %34, i32 5
  %35 = load float, float* %arrayidx19, align 4, !tbaa !42
  %mul20 = fmul float %33, %35
  %36 = load float, float* %sum, align 4, !tbaa !42
  %add21 = fadd float %36, %mul20
  store float %add21, float* %sum, align 4, !tbaa !42
  %37 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds float, float* %37, i32 6
  %38 = load float, float* %arrayidx22, align 4, !tbaa !42
  %39 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %39, i32 6
  %40 = load float, float* %arrayidx23, align 4, !tbaa !42
  %mul24 = fmul float %38, %40
  %41 = load float, float* %sum, align 4, !tbaa !42
  %add25 = fadd float %41, %mul24
  store float %add25, float* %sum, align 4, !tbaa !42
  %42 = load i32, i32* %row.addr, align 4, !tbaa !28
  %43 = load i32, i32* %i, align 4, !tbaa !28
  %add26 = add nsw i32 %42, %43
  %44 = load i32, i32* %col.addr, align 4, !tbaa !28
  %45 = load i32, i32* %j, align 4, !tbaa !28
  %add27 = add nsw i32 %44, %45
  %46 = load float, float* %sum, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7addElemEiif(%struct.btMatrixX* %this1, i32 %add26, i32 %add27, float %46)
  %47 = load float*, float** %cc, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %47, i32 8
  store float* %add.ptr, float** %cc, align 4, !tbaa !2
  %48 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %49 = load i32, i32* %j, align 4, !tbaa !28
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup4
  %50 = load float*, float** %bb, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds float, float* %50, i32 8
  store float* %add.ptr28, float** %bb, align 4, !tbaa !2
  %51 = bitcast float** %cc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %52 = load i32, i32* %i, align 4, !tbaa !28
  %inc30 = add nsw i32 %52, 1
  store i32 %inc30, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end31:                                        ; preds = %for.cond.cleanup
  %53 = bitcast float** %bb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  ret void
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii(%struct.btMatrixX* %this, float* %B, float* %C, i32 %numRows, i32 %numRowsOther, i32 %row, i32 %col) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %B.addr = alloca float*, align 4
  %C.addr = alloca float*, align 4
  %numRows.addr = alloca i32, align 4
  %numRowsOther.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %bb = alloca float*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %cc = alloca float*, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store float* %B, float** %B.addr, align 4, !tbaa !2
  store float* %C, float** %C.addr, align 4, !tbaa !2
  store i32 %numRows, i32* %numRows.addr, align 4, !tbaa !28
  store i32 %numRowsOther, i32* %numRowsOther.addr, align 4, !tbaa !28
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = bitcast float** %bb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %B.addr, align 4, !tbaa !2
  store float* %1, float** %bb, align 4, !tbaa !2
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !28
  %4 = load i32, i32* %numRows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  br label %for.end31

for.body:                                         ; preds = %for.cond
  %6 = bitcast float** %cc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load float*, float** %C.addr, align 4, !tbaa !2
  store float* %7, float** %cc, align 4, !tbaa !2
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %j, align 4, !tbaa !28
  %10 = load i32, i32* %numRowsOther.addr, align 4, !tbaa !28
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end

for.body5:                                        ; preds = %for.cond2
  %12 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %13, i32 0
  %14 = load float, float* %arrayidx, align 4, !tbaa !42
  %15 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds float, float* %15, i32 0
  %16 = load float, float* %arrayidx6, align 4, !tbaa !42
  %mul = fmul float %14, %16
  store float %mul, float* %sum, align 4, !tbaa !42
  %17 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds float, float* %17, i32 1
  %18 = load float, float* %arrayidx7, align 4, !tbaa !42
  %19 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds float, float* %19, i32 1
  %20 = load float, float* %arrayidx8, align 4, !tbaa !42
  %mul9 = fmul float %18, %20
  %21 = load float, float* %sum, align 4, !tbaa !42
  %add = fadd float %21, %mul9
  store float %add, float* %sum, align 4, !tbaa !42
  %22 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %22, i32 2
  %23 = load float, float* %arrayidx10, align 4, !tbaa !42
  %24 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds float, float* %24, i32 2
  %25 = load float, float* %arrayidx11, align 4, !tbaa !42
  %mul12 = fmul float %23, %25
  %26 = load float, float* %sum, align 4, !tbaa !42
  %add13 = fadd float %26, %mul12
  store float %add13, float* %sum, align 4, !tbaa !42
  %27 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %27, i32 4
  %28 = load float, float* %arrayidx14, align 4, !tbaa !42
  %29 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds float, float* %29, i32 4
  %30 = load float, float* %arrayidx15, align 4, !tbaa !42
  %mul16 = fmul float %28, %30
  %31 = load float, float* %sum, align 4, !tbaa !42
  %add17 = fadd float %31, %mul16
  store float %add17, float* %sum, align 4, !tbaa !42
  %32 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %32, i32 5
  %33 = load float, float* %arrayidx18, align 4, !tbaa !42
  %34 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds float, float* %34, i32 5
  %35 = load float, float* %arrayidx19, align 4, !tbaa !42
  %mul20 = fmul float %33, %35
  %36 = load float, float* %sum, align 4, !tbaa !42
  %add21 = fadd float %36, %mul20
  store float %add21, float* %sum, align 4, !tbaa !42
  %37 = load float*, float** %bb, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds float, float* %37, i32 6
  %38 = load float, float* %arrayidx22, align 4, !tbaa !42
  %39 = load float*, float** %cc, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds float, float* %39, i32 6
  %40 = load float, float* %arrayidx23, align 4, !tbaa !42
  %mul24 = fmul float %38, %40
  %41 = load float, float* %sum, align 4, !tbaa !42
  %add25 = fadd float %41, %mul24
  store float %add25, float* %sum, align 4, !tbaa !42
  %42 = load i32, i32* %row.addr, align 4, !tbaa !28
  %43 = load i32, i32* %i, align 4, !tbaa !28
  %add26 = add nsw i32 %42, %43
  %44 = load i32, i32* %col.addr, align 4, !tbaa !28
  %45 = load i32, i32* %j, align 4, !tbaa !28
  %add27 = add nsw i32 %44, %45
  %46 = load float, float* %sum, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %add26, i32 %add27, float %46)
  %47 = load float*, float** %cc, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds float, float* %47, i32 8
  store float* %add.ptr, float** %cc, align 4, !tbaa !2
  %48 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %49 = load i32, i32* %j, align 4, !tbaa !28
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup4
  %50 = load float*, float** %bb, align 4, !tbaa !2
  %add.ptr28 = getelementptr inbounds float, float* %50, i32 8
  store float* %add.ptr28, float** %bb, align 4, !tbaa !2
  %51 = bitcast float** %cc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %52 = load i32, i32* %i, align 4, !tbaa !28
  %inc30 = add nsw i32 %52, 1
  store i32 %inc30, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end31:                                        ; preds = %for.cond.cleanup
  %53 = bitcast float** %bb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this, i32 %row, i32 %col) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %0 = load i32, i32* %col.addr, align 4, !tbaa !28
  %1 = load i32, i32* %row.addr, align 4, !tbaa !28
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_cols, align 4, !tbaa !24
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 %add)
  ret float* %call
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv(%struct.btMatrixX* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %count = alloca i32, align 4
  %row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %count, align 4, !tbaa !28
  %1 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %2 = load i32, i32* %row, align 4, !tbaa !28
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1)
  %cmp = icmp slt i32 %2, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end16

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4, !tbaa !28
  %m_rowNonZeroElements13 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %6 = load i32, i32* %row, align 4, !tbaa !28
  %call4 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements13, i32 %6)
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call4)
  %cmp6 = icmp slt i32 %5, %call5
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body8:                                        ; preds = %for.cond2
  %8 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_rowNonZeroElements19 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %9 = load i32, i32* %row, align 4, !tbaa !28
  %call10 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements19, i32 %9)
  %10 = load i32, i32* %j, align 4, !tbaa !28
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call10, i32 %10)
  %11 = load i32, i32* %call11, align 4, !tbaa !28
  store i32 %11, i32* %col, align 4, !tbaa !28
  %12 = load i32, i32* %col, align 4, !tbaa !28
  %13 = load i32, i32* %row, align 4, !tbaa !28
  %14 = load i32, i32* %row, align 4, !tbaa !28
  %15 = load i32, i32* %col, align 4, !tbaa !28
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %14, i32 %15)
  %16 = load float, float* %call12, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %12, i32 %13, float %16)
  %17 = load i32, i32* %count, align 4, !tbaa !28
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %count, align 4, !tbaa !28
  %18 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %19 = load i32, i32* %j, align 4, !tbaa !28
  %inc13 = add nsw i32 %19, 1
  store i32 %inc13, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup7
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %20 = load i32, i32* %row, align 4, !tbaa !28
  %inc15 = add nsw i32 %20, 1
  store i32 %inc15, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end16:                                        ; preds = %for.cond.cleanup
  %21 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 0)
  %m_storage2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %m_storage2)
  call void @_Z9btSetZeroIfEvPT_i(float* %call, i32 %call3)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayI11btJointNodeED2Ev(%class.btAlignedObjectArray.26* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv(%class.btAlignedObjectArray.26* %this1)
  ret %class.btAlignedObjectArray.26* %this1
}

define hidden void @_ZN12btMLCPSolver10createMLCPERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numBodies = alloca i32, align 4
  %numConstraintRows = alloca i32, align 4
  %i = alloca i32, align 4
  %i30 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %rb = alloca %struct.btSolverBody*, align 4
  %invMass = alloca %class.btVector3*, align 4
  %orgBody = alloca %class.btRigidBody*, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %i90 = alloca i32, align 4
  %bodyIndex0 = alloca i32, align 4
  %bodyIndex1 = alloca i32, align 4
  %ref.tmp = alloca %struct.btMatrixX, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp217 = alloca %struct.btMatrixX, align 4
  %__profile221 = alloca %class.CProfileSample, align 1
  %ref.tmp223 = alloca %struct.btMatrixX, align 4
  %i227 = alloca i32, align 4
  %cfm = alloca float, align 4
  %i246 = alloca i32, align 4
  %c253 = alloca %struct.btSolverConstraint*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call, i32* %numBodies, align 4, !tbaa !28
  %2 = bitcast i32* %numConstraintRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray)
  store i32 %call2, i32* %numConstraintRows, align 4, !tbaa !28
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %3 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 %3)
  %4 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btContactSolverInfo* %4 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %5, i32 0, i32 11
  %6 = load i32, i32* %m_splitImpulse, align 4, !tbaa !45
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %7 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_bSplit, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %10 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintArray3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %12 = load i32, i32* %i, align 4, !tbaa !28
  %call4 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray3, i32 %12)
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call4, i32 0, i32 9
  %13 = load float, float* %m_jacDiagABInv, align 4, !tbaa !48
  %tobool5 = fcmp une float %13, 0.000000e+00
  br i1 %tobool5, label %if.then6, label %if.end26

if.then6:                                         ; preds = %for.body
  %m_allConstraintArray7 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %14 = load i32, i32* %i, align 4, !tbaa !28
  %call8 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray7, i32 %14)
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call8, i32 0, i32 10
  %15 = load float, float* %m_rhs, align 4, !tbaa !49
  %m_allConstraintArray9 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %16 = load i32, i32* %i, align 4, !tbaa !28
  %call10 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray9, i32 %16)
  %m_jacDiagABInv11 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call10, i32 0, i32 9
  %17 = load float, float* %m_jacDiagABInv11, align 4, !tbaa !48
  %div = fdiv float %15, %17
  %m_b12 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %18 = load i32, i32* %i, align 4, !tbaa !28
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_b12, i32 %18)
  store float %div, float* %call13, align 4, !tbaa !42
  %19 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %20 = bitcast %struct.btContactSolverInfo* %19 to %struct.btContactSolverInfoData*
  %m_splitImpulse14 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %20, i32 0, i32 11
  %21 = load i32, i32* %m_splitImpulse14, align 4, !tbaa !45
  %tobool15 = icmp ne i32 %21, 0
  br i1 %tobool15, label %if.then16, label %if.end25

if.then16:                                        ; preds = %if.then6
  %m_allConstraintArray17 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %22 = load i32, i32* %i, align 4, !tbaa !28
  %call18 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray17, i32 %22)
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call18, i32 0, i32 14
  %23 = load float, float* %m_rhsPenetration, align 4, !tbaa !50
  %m_allConstraintArray19 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %24 = load i32, i32* %i, align 4, !tbaa !28
  %call20 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray19, i32 %24)
  %m_jacDiagABInv21 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call20, i32 0, i32 9
  %25 = load float, float* %m_jacDiagABInv21, align 4, !tbaa !48
  %div22 = fdiv float %23, %25
  %m_bSplit23 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %26 = load i32, i32* %i, align 4, !tbaa !28
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_bSplit23, i32 %26)
  store float %div22, float* %call24, align 4, !tbaa !42
  br label %if.end25

if.end25:                                         ; preds = %if.then16, %if.then6
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end26
  %27 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv to i8*) acquire, align 4
  %29 = and i8 %28, 1
  %guard.uninitialized = icmp eq i8 %29, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !53

init.check:                                       ; preds = %for.end
  %30 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv) #8
  %tobool27 = icmp ne i32 %30, 0
  br i1 %tobool27, label %init, label %init.end

init:                                             ; preds = %init.check
  %call28 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv)
  %31 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.19, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv) #8
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %for.end
  %32 = load i32, i32* %numBodies, align 4, !tbaa !28
  %mul = mul nsw i32 6, %32
  %33 = load i32, i32* %numBodies, align 4, !tbaa !28
  %mul29 = mul nsw i32 6, %33
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv, i32 %mul, i32 %mul29)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv)
  %34 = bitcast i32* %i30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  store i32 0, i32* %i30, align 4, !tbaa !28
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc80, %init.end
  %35 = load i32, i32* %i30, align 4, !tbaa !28
  %36 = load i32, i32* %numBodies, align 4, !tbaa !28
  %cmp32 = icmp slt i32 %35, %36
  br i1 %cmp32, label %for.body34, label %for.cond.cleanup33

for.cond.cleanup33:                               ; preds = %for.cond31
  store i32 5, i32* %cleanup.dest.slot, align 4
  %37 = bitcast i32* %i30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %for.end82

for.body34:                                       ; preds = %for.cond31
  %38 = bitcast %struct.btSolverBody** %rb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool35 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %39, i32 0, i32 1
  %40 = load i32, i32* %i30, align 4, !tbaa !28
  %call36 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool35, i32 %40)
  store %struct.btSolverBody* %call36, %struct.btSolverBody** %rb, align 4, !tbaa !2
  %41 = bitcast %class.btVector3** %invMass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %42 = load %struct.btSolverBody*, %struct.btSolverBody** %rb, align 4, !tbaa !2
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %42, i32 0, i32 5
  store %class.btVector3* %m_invMass, %class.btVector3** %invMass, align 4, !tbaa !2
  %43 = load i32, i32* %i30, align 4, !tbaa !28
  %mul37 = mul nsw i32 %43, 6
  %add = add nsw i32 %mul37, 0
  %44 = load i32, i32* %i30, align 4, !tbaa !28
  %mul38 = mul nsw i32 %44, 6
  %add39 = add nsw i32 %mul38, 0
  %45 = load %class.btVector3*, %class.btVector3** %invMass, align 4, !tbaa !2
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %45)
  %arrayidx = getelementptr inbounds float, float* %call40, i32 0
  %46 = load float, float* %arrayidx, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv, i32 %add, i32 %add39, float %46)
  %47 = load i32, i32* %i30, align 4, !tbaa !28
  %mul41 = mul nsw i32 %47, 6
  %add42 = add nsw i32 %mul41, 1
  %48 = load i32, i32* %i30, align 4, !tbaa !28
  %mul43 = mul nsw i32 %48, 6
  %add44 = add nsw i32 %mul43, 1
  %49 = load %class.btVector3*, %class.btVector3** %invMass, align 4, !tbaa !2
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %49)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 1
  %50 = load float, float* %arrayidx46, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv, i32 %add42, i32 %add44, float %50)
  %51 = load i32, i32* %i30, align 4, !tbaa !28
  %mul47 = mul nsw i32 %51, 6
  %add48 = add nsw i32 %mul47, 2
  %52 = load i32, i32* %i30, align 4, !tbaa !28
  %mul49 = mul nsw i32 %52, 6
  %add50 = add nsw i32 %mul49, 2
  %53 = load %class.btVector3*, %class.btVector3** %invMass, align 4, !tbaa !2
  %call51 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %53)
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 2
  %54 = load float, float* %arrayidx52, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv, i32 %add48, i32 %add50, float %54)
  %55 = bitcast %class.btRigidBody** %orgBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  %56 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool53 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %56, i32 0, i32 1
  %57 = load i32, i32* %i30, align 4, !tbaa !28
  %call54 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool53, i32 %57)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call54, i32 0, i32 12
  %58 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !56
  store %class.btRigidBody* %58, %class.btRigidBody** %orgBody, align 4, !tbaa !2
  %59 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  store i32 0, i32* %r, align 4, !tbaa !28
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc77, %for.body34
  %60 = load i32, i32* %r, align 4, !tbaa !28
  %cmp56 = icmp slt i32 %60, 3
  br i1 %cmp56, label %for.body58, label %for.cond.cleanup57

for.cond.cleanup57:                               ; preds = %for.cond55
  store i32 8, i32* %cleanup.dest.slot, align 4
  %61 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  br label %for.end79

for.body58:                                       ; preds = %for.cond55
  %62 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  store i32 0, i32* %c, align 4, !tbaa !28
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc74, %for.body58
  %63 = load i32, i32* %c, align 4, !tbaa !28
  %cmp60 = icmp slt i32 %63, 3
  br i1 %cmp60, label %for.body62, label %for.cond.cleanup61

for.cond.cleanup61:                               ; preds = %for.cond59
  store i32 11, i32* %cleanup.dest.slot, align 4
  %64 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #8
  br label %for.end76

for.body62:                                       ; preds = %for.cond59
  %65 = load i32, i32* %i30, align 4, !tbaa !28
  %mul63 = mul nsw i32 %65, 6
  %add64 = add nsw i32 %mul63, 3
  %66 = load i32, i32* %r, align 4, !tbaa !28
  %add65 = add nsw i32 %add64, %66
  %67 = load i32, i32* %i30, align 4, !tbaa !28
  %mul66 = mul nsw i32 %67, 6
  %add67 = add nsw i32 %mul66, 3
  %68 = load i32, i32* %c, align 4, !tbaa !28
  %add68 = add nsw i32 %add67, %68
  %69 = load %class.btRigidBody*, %class.btRigidBody** %orgBody, align 4, !tbaa !2
  %tobool69 = icmp ne %class.btRigidBody* %69, null
  br i1 %tobool69, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body62
  %70 = load %class.btRigidBody*, %class.btRigidBody** %orgBody, align 4, !tbaa !2
  %call70 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %70)
  %71 = load i32, i32* %r, align 4, !tbaa !28
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call70, i32 %71)
  %call72 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call71)
  %72 = load i32, i32* %c, align 4, !tbaa !28
  %arrayidx73 = getelementptr inbounds float, float* %call72, i32 %72
  %73 = load float, float* %arrayidx73, align 4, !tbaa !42
  br label %cond.end

cond.false:                                       ; preds = %for.body62
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %73, %cond.true ], [ 0.000000e+00, %cond.false ]
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv, i32 %add65, i32 %add68, float %cond)
  br label %for.inc74

for.inc74:                                        ; preds = %cond.end
  %74 = load i32, i32* %c, align 4, !tbaa !28
  %inc75 = add nsw i32 %74, 1
  store i32 %inc75, i32* %c, align 4, !tbaa !28
  br label %for.cond59

for.end76:                                        ; preds = %for.cond.cleanup61
  br label %for.inc77

for.inc77:                                        ; preds = %for.end76
  %75 = load i32, i32* %r, align 4, !tbaa !28
  %inc78 = add nsw i32 %75, 1
  store i32 %inc78, i32* %r, align 4, !tbaa !28
  br label %for.cond55

for.end79:                                        ; preds = %for.cond.cleanup57
  %76 = bitcast %class.btRigidBody** %orgBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast %class.btVector3** %invMass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast %struct.btSolverBody** %rb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  br label %for.inc80

for.inc80:                                        ; preds = %for.end79
  %79 = load i32, i32* %i30, align 4, !tbaa !28
  %inc81 = add nsw i32 %79, 1
  store i32 %inc81, i32* %i30, align 4, !tbaa !28
  br label %for.cond31

for.end82:                                        ; preds = %for.cond.cleanup33
  %80 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J to i8*) acquire, align 4
  %81 = and i8 %80, 1
  %guard.uninitialized83 = icmp eq i8 %81, 0
  br i1 %guard.uninitialized83, label %init.check84, label %init.end88, !prof !53

init.check84:                                     ; preds = %for.end82
  %82 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J) #8
  %tobool85 = icmp ne i32 %82, 0
  br i1 %tobool85, label %init86, label %init.end88

init86:                                           ; preds = %init.check84
  %call87 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J)
  %83 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.20, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J) #8
  br label %init.end88

init.end88:                                       ; preds = %init86, %init.check84, %for.end82
  %84 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  %85 = load i32, i32* %numBodies, align 4, !tbaa !28
  %mul89 = mul nsw i32 6, %85
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %84, i32 %mul89)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %86 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 %86)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %87 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 %87)
  %88 = bitcast i32* %i90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  store i32 0, i32* %i90, align 4, !tbaa !28
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc199, %init.end88
  %89 = load i32, i32* %i90, align 4, !tbaa !28
  %90 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  %cmp92 = icmp slt i32 %89, %90
  br i1 %cmp92, label %for.body94, label %for.cond.cleanup93

for.cond.cleanup93:                               ; preds = %for.cond91
  store i32 14, i32* %cleanup.dest.slot, align 4
  %91 = bitcast i32* %i90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  br label %for.end201

for.body94:                                       ; preds = %for.cond91
  %m_allConstraintArray95 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %92 = load i32, i32* %i90, align 4, !tbaa !28
  %call96 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray95, i32 %92)
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call96, i32 0, i32 12
  %93 = load float, float* %m_lowerLimit, align 4, !tbaa !51
  %m_lo97 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %94 = load i32, i32* %i90, align 4, !tbaa !28
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_lo97, i32 %94)
  store float %93, float* %call98, align 4, !tbaa !42
  %m_allConstraintArray99 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %95 = load i32, i32* %i90, align 4, !tbaa !28
  %call100 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray99, i32 %95)
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call100, i32 0, i32 13
  %96 = load float, float* %m_upperLimit, align 4, !tbaa !52
  %m_hi101 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %97 = load i32, i32* %i90, align 4, !tbaa !28
  %call102 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_hi101, i32 %97)
  store float %96, float* %call102, align 4, !tbaa !42
  %98 = bitcast i32* %bodyIndex0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #8
  %m_allConstraintArray103 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %99 = load i32, i32* %i90, align 4, !tbaa !28
  %call104 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray103, i32 %99)
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call104, i32 0, i32 18
  %100 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !54
  store i32 %100, i32* %bodyIndex0, align 4, !tbaa !28
  %101 = bitcast i32* %bodyIndex1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #8
  %m_allConstraintArray105 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %102 = load i32, i32* %i90, align 4, !tbaa !28
  %call106 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray105, i32 %102)
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call106, i32 0, i32 19
  %103 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !55
  store i32 %103, i32* %bodyIndex1, align 4, !tbaa !28
  %104 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool107 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %104, i32 0, i32 1
  %105 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %call108 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool107, i32 %105)
  %m_originalBody109 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call108, i32 0, i32 12
  %106 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody109, align 4, !tbaa !56
  %tobool110 = icmp ne %class.btRigidBody* %106, null
  br i1 %tobool110, label %if.then111, label %if.end152

if.then111:                                       ; preds = %for.body94
  %107 = load i32, i32* %i90, align 4, !tbaa !28
  %108 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul112 = mul nsw i32 6, %108
  %add113 = add nsw i32 %mul112, 0
  %m_allConstraintArray114 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %109 = load i32, i32* %i90, align 4, !tbaa !28
  %call115 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray114, i32 %109)
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call115, i32 0, i32 1
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 0
  %110 = load float, float* %arrayidx117, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %107, i32 %add113, float %110)
  %111 = load i32, i32* %i90, align 4, !tbaa !28
  %112 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul118 = mul nsw i32 6, %112
  %add119 = add nsw i32 %mul118, 1
  %m_allConstraintArray120 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %113 = load i32, i32* %i90, align 4, !tbaa !28
  %call121 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray120, i32 %113)
  %m_contactNormal1122 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call121, i32 0, i32 1
  %call123 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1122)
  %arrayidx124 = getelementptr inbounds float, float* %call123, i32 1
  %114 = load float, float* %arrayidx124, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %111, i32 %add119, float %114)
  %115 = load i32, i32* %i90, align 4, !tbaa !28
  %116 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul125 = mul nsw i32 6, %116
  %add126 = add nsw i32 %mul125, 2
  %m_allConstraintArray127 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %117 = load i32, i32* %i90, align 4, !tbaa !28
  %call128 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray127, i32 %117)
  %m_contactNormal1129 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call128, i32 0, i32 1
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1129)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 2
  %118 = load float, float* %arrayidx131, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %115, i32 %add126, float %118)
  %119 = load i32, i32* %i90, align 4, !tbaa !28
  %120 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul132 = mul nsw i32 6, %120
  %add133 = add nsw i32 %mul132, 3
  %m_allConstraintArray134 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %121 = load i32, i32* %i90, align 4, !tbaa !28
  %call135 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray134, i32 %121)
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call135, i32 0, i32 0
  %call136 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal)
  %arrayidx137 = getelementptr inbounds float, float* %call136, i32 0
  %122 = load float, float* %arrayidx137, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %119, i32 %add133, float %122)
  %123 = load i32, i32* %i90, align 4, !tbaa !28
  %124 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul138 = mul nsw i32 6, %124
  %add139 = add nsw i32 %mul138, 4
  %m_allConstraintArray140 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %125 = load i32, i32* %i90, align 4, !tbaa !28
  %call141 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray140, i32 %125)
  %m_relpos1CrossNormal142 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call141, i32 0, i32 0
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal142)
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 1
  %126 = load float, float* %arrayidx144, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %123, i32 %add139, float %126)
  %127 = load i32, i32* %i90, align 4, !tbaa !28
  %128 = load i32, i32* %bodyIndex0, align 4, !tbaa !28
  %mul145 = mul nsw i32 6, %128
  %add146 = add nsw i32 %mul145, 5
  %m_allConstraintArray147 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %129 = load i32, i32* %i90, align 4, !tbaa !28
  %call148 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray147, i32 %129)
  %m_relpos1CrossNormal149 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call148, i32 0, i32 0
  %call150 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal149)
  %arrayidx151 = getelementptr inbounds float, float* %call150, i32 2
  %130 = load float, float* %arrayidx151, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %127, i32 %add146, float %130)
  br label %if.end152

if.end152:                                        ; preds = %if.then111, %for.body94
  %131 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool153 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %131, i32 0, i32 1
  %132 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %call154 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool153, i32 %132)
  %m_originalBody155 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call154, i32 0, i32 12
  %133 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody155, align 4, !tbaa !56
  %tobool156 = icmp ne %class.btRigidBody* %133, null
  br i1 %tobool156, label %if.then157, label %if.end198

if.then157:                                       ; preds = %if.end152
  %134 = load i32, i32* %i90, align 4, !tbaa !28
  %135 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul158 = mul nsw i32 6, %135
  %add159 = add nsw i32 %mul158, 0
  %m_allConstraintArray160 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %136 = load i32, i32* %i90, align 4, !tbaa !28
  %call161 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray160, i32 %136)
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call161, i32 0, i32 3
  %call162 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2)
  %arrayidx163 = getelementptr inbounds float, float* %call162, i32 0
  %137 = load float, float* %arrayidx163, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %134, i32 %add159, float %137)
  %138 = load i32, i32* %i90, align 4, !tbaa !28
  %139 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul164 = mul nsw i32 6, %139
  %add165 = add nsw i32 %mul164, 1
  %m_allConstraintArray166 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %140 = load i32, i32* %i90, align 4, !tbaa !28
  %call167 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray166, i32 %140)
  %m_contactNormal2168 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call167, i32 0, i32 3
  %call169 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2168)
  %arrayidx170 = getelementptr inbounds float, float* %call169, i32 1
  %141 = load float, float* %arrayidx170, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %138, i32 %add165, float %141)
  %142 = load i32, i32* %i90, align 4, !tbaa !28
  %143 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul171 = mul nsw i32 6, %143
  %add172 = add nsw i32 %mul171, 2
  %m_allConstraintArray173 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %144 = load i32, i32* %i90, align 4, !tbaa !28
  %call174 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray173, i32 %144)
  %m_contactNormal2175 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call174, i32 0, i32 3
  %call176 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2175)
  %arrayidx177 = getelementptr inbounds float, float* %call176, i32 2
  %145 = load float, float* %arrayidx177, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %142, i32 %add172, float %145)
  %146 = load i32, i32* %i90, align 4, !tbaa !28
  %147 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul178 = mul nsw i32 6, %147
  %add179 = add nsw i32 %mul178, 3
  %m_allConstraintArray180 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %148 = load i32, i32* %i90, align 4, !tbaa !28
  %call181 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray180, i32 %148)
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call181, i32 0, i32 2
  %call182 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal)
  %arrayidx183 = getelementptr inbounds float, float* %call182, i32 0
  %149 = load float, float* %arrayidx183, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %146, i32 %add179, float %149)
  %150 = load i32, i32* %i90, align 4, !tbaa !28
  %151 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul184 = mul nsw i32 6, %151
  %add185 = add nsw i32 %mul184, 4
  %m_allConstraintArray186 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %152 = load i32, i32* %i90, align 4, !tbaa !28
  %call187 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray186, i32 %152)
  %m_relpos2CrossNormal188 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call187, i32 0, i32 2
  %call189 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal188)
  %arrayidx190 = getelementptr inbounds float, float* %call189, i32 1
  %153 = load float, float* %arrayidx190, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %150, i32 %add185, float %153)
  %154 = load i32, i32* %i90, align 4, !tbaa !28
  %155 = load i32, i32* %bodyIndex1, align 4, !tbaa !28
  %mul191 = mul nsw i32 6, %155
  %add192 = add nsw i32 %mul191, 5
  %m_allConstraintArray193 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %156 = load i32, i32* %i90, align 4, !tbaa !28
  %call194 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray193, i32 %156)
  %m_relpos2CrossNormal195 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call194, i32 0, i32 2
  %call196 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal195)
  %arrayidx197 = getelementptr inbounds float, float* %call196, i32 2
  %157 = load float, float* %arrayidx197, align 4, !tbaa !42
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, i32 %154, i32 %add192, float %157)
  br label %if.end198

if.end198:                                        ; preds = %if.then157, %if.end152
  %158 = bitcast i32* %bodyIndex1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #8
  %159 = bitcast i32* %bodyIndex0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #8
  br label %for.inc199

for.inc199:                                       ; preds = %if.end198
  %160 = load i32, i32* %i90, align 4, !tbaa !28
  %inc200 = add nsw i32 %160, 1
  store i32 %inc200, i32* %i90, align 4, !tbaa !28
  br label %for.cond91

for.end201:                                       ; preds = %for.cond.cleanup93
  %161 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose to i8*) acquire, align 4
  %162 = and i8 %161, 1
  %guard.uninitialized202 = icmp eq i8 %162, 0
  br i1 %guard.uninitialized202, label %init.check203, label %init.end207, !prof !53

init.check203:                                    ; preds = %for.end201
  %163 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose) #8
  %tobool204 = icmp ne i32 %163, 0
  br i1 %tobool204, label %init205, label %init.end207

init205:                                          ; preds = %init.check203
  %call206 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose)
  %164 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.21, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose) #8
  br label %init.end207

init.end207:                                      ; preds = %init205, %init.check203, %for.end201
  %165 = bitcast %struct.btMatrixX* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %165) #8
  call void @_ZNK9btMatrixXIfE9transposeEv(%struct.btMatrixX* sret align 4 %ref.tmp, %struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J)
  %call208 = call nonnull align 4 dereferenceable(80) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %ref.tmp)
  %call209 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp) #8
  %166 = bitcast %struct.btMatrixX* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %166) #8
  %167 = load atomic i8, i8* bitcast (i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp to i8*) acquire, align 4
  %168 = and i8 %167, 1
  %guard.uninitialized210 = icmp eq i8 %168, 0
  br i1 %guard.uninitialized210, label %init.check211, label %init.end215, !prof !53

init.check211:                                    ; preds = %init.end207
  %169 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp) #8
  %tobool212 = icmp ne i32 %169, 0
  br i1 %tobool212, label %init213, label %init.end215

init213:                                          ; preds = %init.check211
  %call214 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp)
  %170 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.22, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp) #8
  br label %init.end215

init.end215:                                      ; preds = %init213, %init.check211, %init.end207
  %171 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %171) #8
  %call216 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.23, i32 0, i32 0))
  %172 = bitcast %struct.btMatrixX* %ref.tmp217 to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %172) #8
  call void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* sret align 4 %ref.tmp217, %struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J, %struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv)
  %call218 = call nonnull align 4 dereferenceable(80) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %ref.tmp217)
  %call219 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp217) #8
  %173 = bitcast %struct.btMatrixX* %ref.tmp217 to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %173) #8
  %call220 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %174 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %174) #8
  %175 = bitcast %class.CProfileSample* %__profile221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %175) #8
  %call222 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile221, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.24, i32 0, i32 0))
  %176 = bitcast %struct.btMatrixX* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 80, i8* %176) #8
  call void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* sret align 4 %ref.tmp223, %struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp, %struct.btMatrixX* nonnull align 4 dereferenceable(80) @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose)
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call224 = call nonnull align 4 dereferenceable(80) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %m_A, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %ref.tmp223)
  %call225 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp223) #8
  %177 = bitcast %struct.btMatrixX* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 80, i8* %177) #8
  %call226 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile221) #8
  %178 = bitcast %class.CProfileSample* %__profile221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %178) #8
  %179 = bitcast i32* %i227 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #8
  store i32 0, i32* %i227, align 4, !tbaa !28
  br label %for.cond228

for.cond228:                                      ; preds = %for.inc239, %init.end215
  %180 = load i32, i32* %i227, align 4, !tbaa !28
  %m_A229 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call230 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A229)
  %cmp231 = icmp slt i32 %180, %call230
  br i1 %cmp231, label %for.body233, label %for.cond.cleanup232

for.cond.cleanup232:                              ; preds = %for.cond228
  store i32 17, i32* %cleanup.dest.slot, align 4
  %181 = bitcast i32* %i227 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #8
  br label %for.end241

for.body233:                                      ; preds = %for.cond228
  %182 = bitcast float* %cfm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %182) #8
  store float 0x3F1A36E2E0000000, float* %cfm, align 4, !tbaa !42
  %m_A234 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %183 = load i32, i32* %i227, align 4, !tbaa !28
  %184 = load i32, i32* %i227, align 4, !tbaa !28
  %m_A235 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %185 = load i32, i32* %i227, align 4, !tbaa !28
  %186 = load i32, i32* %i227, align 4, !tbaa !28
  %call236 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %m_A235, i32 %185, i32 %186)
  %187 = load float, float* %call236, align 4, !tbaa !42
  %188 = load float, float* %cfm, align 4, !tbaa !42
  %189 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %190 = bitcast %struct.btContactSolverInfo* %189 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %190, i32 0, i32 3
  %191 = load float, float* %m_timeStep, align 4, !tbaa !69
  %div237 = fdiv float %188, %191
  %add238 = fadd float %187, %div237
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %m_A234, i32 %183, i32 %184, float %add238)
  %192 = bitcast float* %cfm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #8
  br label %for.inc239

for.inc239:                                       ; preds = %for.body233
  %193 = load i32, i32* %i227, align 4, !tbaa !28
  %inc240 = add nsw i32 %193, 1
  store i32 %inc240, i32* %i227, align 4, !tbaa !28
  br label %for.cond228

for.end241:                                       ; preds = %for.cond.cleanup232
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %194 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 %194)
  %195 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %196 = bitcast %struct.btContactSolverInfo* %195 to %struct.btContactSolverInfoData*
  %m_splitImpulse242 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %196, i32 0, i32 11
  %197 = load i32, i32* %m_splitImpulse242, align 4, !tbaa !45
  %tobool243 = icmp ne i32 %197, 0
  br i1 %tobool243, label %if.then244, label %if.end245

if.then244:                                       ; preds = %for.end241
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %198 = load i32, i32* %numConstraintRows, align 4, !tbaa !28
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_xSplit, i32 %198)
  br label %if.end245

if.end245:                                        ; preds = %if.then244, %for.end241
  %199 = bitcast i32* %i246 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #8
  store i32 0, i32* %i246, align 4, !tbaa !28
  br label %for.cond247

for.cond247:                                      ; preds = %for.inc264, %if.end245
  %200 = load i32, i32* %i246, align 4, !tbaa !28
  %m_allConstraintArray248 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call249 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray248)
  %cmp250 = icmp slt i32 %200, %call249
  br i1 %cmp250, label %for.body252, label %for.cond.cleanup251

for.cond.cleanup251:                              ; preds = %for.cond247
  store i32 20, i32* %cleanup.dest.slot, align 4
  %201 = bitcast i32* %i246 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #8
  br label %for.end266

for.body252:                                      ; preds = %for.cond247
  %202 = bitcast %struct.btSolverConstraint** %c253 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #8
  %m_allConstraintArray254 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %203 = load i32, i32* %i246, align 4, !tbaa !28
  %call255 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray254, i32 %203)
  store %struct.btSolverConstraint* %call255, %struct.btSolverConstraint** %c253, align 4, !tbaa !2
  %204 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c253, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %204, i32 0, i32 7
  %205 = load float, float* %m_appliedImpulse, align 4, !tbaa !71
  %m_x256 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %206 = load i32, i32* %i246, align 4, !tbaa !28
  %call257 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x256, i32 %206)
  store float %205, float* %call257, align 4, !tbaa !42
  %207 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %208 = bitcast %struct.btContactSolverInfo* %207 to %struct.btContactSolverInfoData*
  %m_splitImpulse258 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %208, i32 0, i32 11
  %209 = load i32, i32* %m_splitImpulse258, align 4, !tbaa !45
  %tobool259 = icmp ne i32 %209, 0
  br i1 %tobool259, label %if.then260, label %if.end263

if.then260:                                       ; preds = %for.body252
  %210 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c253, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %210, i32 0, i32 6
  %211 = load float, float* %m_appliedPushImpulse, align 4, !tbaa !72
  %m_xSplit261 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %212 = load i32, i32* %i246, align 4, !tbaa !28
  %call262 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit261, i32 %212)
  store float %211, float* %call262, align 4, !tbaa !42
  br label %if.end263

if.end263:                                        ; preds = %if.then260, %for.body252
  %213 = bitcast %struct.btSolverConstraint** %c253 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #8
  br label %for.inc264

for.inc264:                                       ; preds = %if.end263
  %214 = load i32, i32* %i246, align 4, !tbaa !28
  %inc265 = add nsw i32 %214, 1
  store i32 %inc265, i32* %i246, align 4, !tbaa !28
  br label %for.cond247

for.end266:                                       ; preds = %for.cond.cleanup251
  %215 = bitcast i32* %numConstraintRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #8
  %216 = bitcast i32* %numBodies to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #8
  ret void
}

define internal void @__cxx_global_array_dtor.19(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE4Minv) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(80) %mat, i32 %row, i32 %col, float %val) #6 comdat {
entry:
  %mat.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %mat, %struct.btMatrixX** %mat.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  store float %val, float* %val.addr, align 4, !tbaa !42
  %0 = load %struct.btMatrixX*, %struct.btMatrixX** %mat.addr, align 4, !tbaa !2
  %1 = load i32, i32* %row.addr, align 4, !tbaa !28
  %2 = load i32, i32* %col.addr, align 4, !tbaa !28
  %3 = load float, float* %val.addr, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %0, i32 %1, i32 %2, float %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !28
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define internal void @__cxx_global_array_dtor.20(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE1J) #8
  ret void
}

define internal void @__cxx_global_array_dtor.21(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE11J_transpose) #8
  ret void
}

define linkonce_odr hidden void @_ZNK9btMatrixXIfE9transposeEv(%struct.btMatrixX* noalias sret align 4 %agg.result, %struct.btMatrixX* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %struct.btMatrixX*, align 4
  %nrvo = alloca i1, align 1
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %h = alloca i32, align 4
  %j = alloca i32, align 4
  %v = alloca float, align 4
  %0 = bitcast %struct.btMatrixX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i1 false, i1* %nrvo, align 1
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_cols, align 4, !tbaa !24
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_rows, align 4, !tbaa !23
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %agg.result, i32 %1, i32 %2)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %agg.result)
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !28
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %m_colNonZeroElements)
  %cmp = icmp slt i32 %4, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  br label %for.end16

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 0, i32* %h, align 4, !tbaa !28
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %h, align 4, !tbaa !28
  %m_colNonZeroElements4 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements4, i32 %8)
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call5)
  %cmp7 = icmp slt i32 %7, %call6
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %for.end

for.body9:                                        ; preds = %for.cond3
  %10 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %m_colNonZeroElements10 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %call11 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements10, i32 %11)
  %12 = load i32, i32* %h, align 4, !tbaa !28
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call11, i32 %12)
  %13 = load i32, i32* %call12, align 4, !tbaa !28
  store i32 %13, i32* %j, align 4, !tbaa !28
  %14 = bitcast float* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load i32, i32* %j, align 4, !tbaa !28
  %16 = load i32, i32* %i, align 4, !tbaa !28
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %15, i32 %16)
  %17 = load float, float* %call13, align 4, !tbaa !42
  store float %17, float* %v, align 4, !tbaa !42
  %18 = load i32, i32* %i, align 4, !tbaa !28
  %19 = load i32, i32* %j, align 4, !tbaa !28
  %20 = load float, float* %v, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %agg.result, i32 %18, i32 %19, float %20)
  %21 = bitcast float* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %23 = load i32, i32* %h, align 4, !tbaa !28
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %h, align 4, !tbaa !28
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup8
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %24 = load i32, i32* %i, align 4, !tbaa !28
  %inc15 = add nsw i32 %24, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end16:                                        ; preds = %for.cond.cleanup
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end16
  %call17 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end16
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %0) #6 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store %struct.btMatrixX* %0, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rows2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %1, i32 0, i32 0
  %2 = bitcast i32* %m_rows to i8*
  %3 = bitcast i32* %m_rows2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %2, i8* align 4 %3, i64 20, i1 false)
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %4 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %4, i32 0, i32 5
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEaSERKS0_(%class.btAlignedObjectArray.18* %m_storage, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %m_storage3)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %5 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_rowNonZeroElements14 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %5, i32 0, i32 6
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_rowNonZeroElements14)
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %6 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4, !tbaa !2
  %m_colNonZeroElements6 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %6, i32 0, i32 7
  %call7 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_(%class.btAlignedObjectArray.22* %m_colNonZeroElements, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_colNonZeroElements6)
  ret %struct.btMatrixX* %this1
}

define internal void @__cxx_global_array_dtor.22(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* @_ZZN12btMLCPSolver10createMLCPERK19btContactSolverInfoE3tmp) #8
  ret void
}

define linkonce_odr hidden void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* noalias sret align 4 %agg.result, %struct.btMatrixX* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(80) %other) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %struct.btMatrixX*, align 4
  %other.addr = alloca %struct.btMatrixX*, align 4
  %nrvo = alloca i1, align 1
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %dotProd = alloca float, align 4
  %dotProd2 = alloca float, align 4
  %waste = alloca i32, align 4
  %waste2 = alloca i32, align 4
  %doubleWalk = alloca i8, align 1
  %numRows = alloca i32, align 4
  %numOtherCols = alloca i32, align 4
  %ii = alloca i32, align 4
  %vThis = alloca i32, align 4
  %ii21 = alloca i32, align 4
  %vOther = alloca i32, align 4
  %indexRow = alloca i32, align 4
  %indexOtherCol = alloca i32, align 4
  %vThis34 = alloca i32, align 4
  %vOther38 = alloca i32, align 4
  %useOtherCol = alloca i8, align 1
  %q = alloca i32, align 4
  %v = alloca i32, align 4
  %w = alloca float, align 4
  %q84 = alloca i32, align 4
  %v92 = alloca i32, align 4
  %w96 = alloca float, align 4
  %0 = bitcast %struct.btMatrixX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store %struct.btMatrixX* %other, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i1 false, i1* %nrvo, align 1
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %1)
  %call3 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %agg.result, i32 %call, i32 %call2)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %agg.result)
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc116, %entry
  %3 = load i32, i32* %j, align 4, !tbaa !28
  %call4 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %agg.result)
  %cmp = icmp slt i32 %3, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  br label %for.end118

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc113, %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %call6 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %agg.result)
  %cmp7 = icmp slt i32 %6, %call6
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8

for.cond.cleanup8:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end115

for.body9:                                        ; preds = %for.cond5
  %8 = bitcast float* %dotProd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %dotProd, align 4, !tbaa !42
  %9 = bitcast float* %dotProd2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %dotProd2, align 4, !tbaa !42
  %10 = bitcast i32* %waste to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store i32 0, i32* %waste, align 4, !tbaa !28
  %11 = bitcast i32* %waste2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store i32 0, i32* %waste2, align 4, !tbaa !28
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %doubleWalk) #8
  store i8 0, i8* %doubleWalk, align 1, !tbaa !29
  %12 = load i8, i8* %doubleWalk, align 1, !tbaa !29, !range !30
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %if.then, label %if.else51

if.then:                                          ; preds = %for.body9
  %13 = bitcast i32* %numRows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %14 = load i32, i32* %i, align 4, !tbaa !28
  %call10 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1, i32 %14)
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call10)
  store i32 %call11, i32* %numRows, align 4, !tbaa !28
  %15 = bitcast i32* %numOtherCols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %16, i32 0, i32 7
  %17 = load i32, i32* %j, align 4, !tbaa !28
  %call12 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements, i32 %17)
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call12)
  store i32 %call13, i32* %numOtherCols, align 4, !tbaa !28
  %18 = bitcast i32* %ii to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 0, i32* %ii, align 4, !tbaa !28
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %if.then
  %19 = load i32, i32* %ii, align 4, !tbaa !28
  %20 = load i32, i32* %numRows, align 4, !tbaa !28
  %cmp15 = icmp slt i32 %19, %20
  br i1 %cmp15, label %for.body17, label %for.cond.cleanup16

for.cond.cleanup16:                               ; preds = %for.cond14
  store i32 8, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %ii to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.end

for.body17:                                       ; preds = %for.cond14
  %22 = bitcast i32* %vThis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %m_rowNonZeroElements118 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %23 = load i32, i32* %i, align 4, !tbaa !28
  %call19 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements118, i32 %23)
  %24 = load i32, i32* %ii, align 4, !tbaa !28
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call19, i32 %24)
  %25 = load i32, i32* %call20, align 4, !tbaa !28
  store i32 %25, i32* %vThis, align 4, !tbaa !28
  %26 = bitcast i32* %vThis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body17
  %27 = load i32, i32* %ii, align 4, !tbaa !28
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %ii, align 4, !tbaa !28
  br label %for.cond14

for.end:                                          ; preds = %for.cond.cleanup16
  %28 = bitcast i32* %ii21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store i32 0, i32* %ii21, align 4, !tbaa !28
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc29, %for.end
  %29 = load i32, i32* %ii21, align 4, !tbaa !28
  %30 = load i32, i32* %numOtherCols, align 4, !tbaa !28
  %cmp23 = icmp slt i32 %29, %30
  br i1 %cmp23, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond22
  store i32 11, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %ii21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  br label %for.end31

for.body25:                                       ; preds = %for.cond22
  %32 = bitcast i32* %vOther to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements26 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %33, i32 0, i32 7
  %34 = load i32, i32* %j, align 4, !tbaa !28
  %call27 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements26, i32 %34)
  %35 = load i32, i32* %ii21, align 4, !tbaa !28
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call27, i32 %35)
  %36 = load i32, i32* %call28, align 4, !tbaa !28
  store i32 %36, i32* %vOther, align 4, !tbaa !28
  %37 = bitcast i32* %vOther to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %for.inc29

for.inc29:                                        ; preds = %for.body25
  %38 = load i32, i32* %ii21, align 4, !tbaa !28
  %inc30 = add nsw i32 %38, 1
  store i32 %inc30, i32* %ii21, align 4, !tbaa !28
  br label %for.cond22

for.end31:                                        ; preds = %for.cond.cleanup24
  %39 = bitcast i32* %indexRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store i32 0, i32* %indexRow, align 4, !tbaa !28
  %40 = bitcast i32* %indexOtherCol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  store i32 0, i32* %indexOtherCol, align 4, !tbaa !28
  br label %while.cond

while.cond:                                       ; preds = %if.end50, %for.end31
  %41 = load i32, i32* %indexRow, align 4, !tbaa !28
  %42 = load i32, i32* %numRows, align 4, !tbaa !28
  %cmp32 = icmp slt i32 %41, %42
  br i1 %cmp32, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %43 = load i32, i32* %indexOtherCol, align 4, !tbaa !28
  %44 = load i32, i32* %numOtherCols, align 4, !tbaa !28
  %cmp33 = icmp slt i32 %43, %44
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %45 = phi i1 [ false, %while.cond ], [ %cmp33, %land.rhs ]
  br i1 %45, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %46 = bitcast i32* %vThis34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %m_rowNonZeroElements135 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %47 = load i32, i32* %i, align 4, !tbaa !28
  %call36 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements135, i32 %47)
  %48 = load i32, i32* %indexRow, align 4, !tbaa !28
  %call37 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call36, i32 %48)
  %49 = load i32, i32* %call37, align 4, !tbaa !28
  store i32 %49, i32* %vThis34, align 4, !tbaa !28
  %50 = bitcast i32* %vOther38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements39 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %51, i32 0, i32 7
  %52 = load i32, i32* %j, align 4, !tbaa !28
  %call40 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements39, i32 %52)
  %53 = load i32, i32* %indexOtherCol, align 4, !tbaa !28
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call40, i32 %53)
  %54 = load i32, i32* %call41, align 4, !tbaa !28
  store i32 %54, i32* %vOther38, align 4, !tbaa !28
  %55 = load i32, i32* %vOther38, align 4, !tbaa !28
  %56 = load i32, i32* %vThis34, align 4, !tbaa !28
  %cmp42 = icmp eq i32 %55, %56
  br i1 %cmp42, label %if.then43, label %if.end

if.then43:                                        ; preds = %while.body
  %57 = load i32, i32* %i, align 4, !tbaa !28
  %58 = load i32, i32* %vThis34, align 4, !tbaa !28
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %57, i32 %58)
  %59 = load float, float* %call44, align 4, !tbaa !42
  %60 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %61 = load i32, i32* %vThis34, align 4, !tbaa !28
  %62 = load i32, i32* %j, align 4, !tbaa !28
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %60, i32 %61, i32 %62)
  %63 = load float, float* %call45, align 4, !tbaa !42
  %mul = fmul float %59, %63
  %64 = load float, float* %dotProd, align 4, !tbaa !42
  %add = fadd float %64, %mul
  store float %add, float* %dotProd, align 4, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.then43, %while.body
  %65 = load i32, i32* %vThis34, align 4, !tbaa !28
  %66 = load i32, i32* %vOther38, align 4, !tbaa !28
  %cmp46 = icmp slt i32 %65, %66
  br i1 %cmp46, label %if.then47, label %if.else

if.then47:                                        ; preds = %if.end
  %67 = load i32, i32* %indexRow, align 4, !tbaa !28
  %inc48 = add nsw i32 %67, 1
  store i32 %inc48, i32* %indexRow, align 4, !tbaa !28
  br label %if.end50

if.else:                                          ; preds = %if.end
  %68 = load i32, i32* %indexOtherCol, align 4, !tbaa !28
  %inc49 = add nsw i32 %68, 1
  store i32 %inc49, i32* %indexOtherCol, align 4, !tbaa !28
  br label %if.end50

if.end50:                                         ; preds = %if.else, %if.then47
  %69 = bitcast i32* %vOther38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  %70 = bitcast i32* %vThis34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  br label %while.cond

while.end:                                        ; preds = %land.end
  %71 = bitcast i32* %indexOtherCol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast i32* %indexRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast i32* %numOtherCols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast i32* %numRows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %if.end109

if.else51:                                        ; preds = %for.body9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %useOtherCol) #8
  store i8 1, i8* %useOtherCol, align 1, !tbaa !29
  %75 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements52 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %75, i32 0, i32 7
  %76 = load i32, i32* %j, align 4, !tbaa !28
  %call53 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements52, i32 %76)
  %call54 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call53)
  %m_rowNonZeroElements155 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %77 = load i32, i32* %i, align 4, !tbaa !28
  %call56 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements155, i32 %77)
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call56)
  %cmp58 = icmp slt i32 %call54, %call57
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %if.else51
  store i8 1, i8* %useOtherCol, align 1, !tbaa !29
  br label %if.end60

if.end60:                                         ; preds = %if.then59, %if.else51
  %78 = load i8, i8* %useOtherCol, align 1, !tbaa !29, !range !30
  %tobool61 = trunc i8 %78 to i1
  br i1 %tobool61, label %if.else83, label %if.then62

if.then62:                                        ; preds = %if.end60
  %79 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #8
  store i32 0, i32* %q, align 4, !tbaa !28
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc80, %if.then62
  %80 = load i32, i32* %q, align 4, !tbaa !28
  %81 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements64 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %81, i32 0, i32 7
  %82 = load i32, i32* %j, align 4, !tbaa !28
  %call65 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements64, i32 %82)
  %call66 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call65)
  %cmp67 = icmp slt i32 %80, %call66
  br i1 %cmp67, label %for.body69, label %for.cond.cleanup68

for.cond.cleanup68:                               ; preds = %for.cond63
  store i32 16, i32* %cleanup.dest.slot, align 4
  %83 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  br label %for.end82

for.body69:                                       ; preds = %for.cond63
  %84 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #8
  %85 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %m_colNonZeroElements70 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %85, i32 0, i32 7
  %86 = load i32, i32* %j, align 4, !tbaa !28
  %call71 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements70, i32 %86)
  %87 = load i32, i32* %q, align 4, !tbaa !28
  %call72 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call71, i32 %87)
  %88 = load i32, i32* %call72, align 4, !tbaa !28
  store i32 %88, i32* %v, align 4, !tbaa !28
  %89 = bitcast float* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #8
  %90 = load i32, i32* %i, align 4, !tbaa !28
  %91 = load i32, i32* %v, align 4, !tbaa !28
  %call73 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %90, i32 %91)
  %92 = load float, float* %call73, align 4, !tbaa !42
  store float %92, float* %w, align 4, !tbaa !42
  %93 = load float, float* %w, align 4, !tbaa !42
  %cmp74 = fcmp une float %93, 0.000000e+00
  br i1 %cmp74, label %if.then75, label %if.end79

if.then75:                                        ; preds = %for.body69
  %94 = load float, float* %w, align 4, !tbaa !42
  %95 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %96 = load i32, i32* %v, align 4, !tbaa !28
  %97 = load i32, i32* %j, align 4, !tbaa !28
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %95, i32 %96, i32 %97)
  %98 = load float, float* %call76, align 4, !tbaa !42
  %mul77 = fmul float %94, %98
  %99 = load float, float* %dotProd, align 4, !tbaa !42
  %add78 = fadd float %99, %mul77
  store float %add78, float* %dotProd, align 4, !tbaa !42
  br label %if.end79

if.end79:                                         ; preds = %if.then75, %for.body69
  %100 = bitcast float* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %102 = load i32, i32* %q, align 4, !tbaa !28
  %inc81 = add nsw i32 %102, 1
  store i32 %inc81, i32* %q, align 4, !tbaa !28
  br label %for.cond63

for.end82:                                        ; preds = %for.cond.cleanup68
  br label %if.end108

if.else83:                                        ; preds = %if.end60
  %103 = bitcast i32* %q84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #8
  store i32 0, i32* %q84, align 4, !tbaa !28
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc105, %if.else83
  %104 = load i32, i32* %q84, align 4, !tbaa !28
  %m_rowNonZeroElements186 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %105 = load i32, i32* %i, align 4, !tbaa !28
  %call87 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements186, i32 %105)
  %call88 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %call87)
  %cmp89 = icmp slt i32 %104, %call88
  br i1 %cmp89, label %for.body91, label %for.cond.cleanup90

for.cond.cleanup90:                               ; preds = %for.cond85
  store i32 19, i32* %cleanup.dest.slot, align 4
  %106 = bitcast i32* %q84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #8
  br label %for.end107

for.body91:                                       ; preds = %for.cond85
  %107 = bitcast i32* %v92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #8
  %m_rowNonZeroElements193 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %108 = load i32, i32* %i, align 4, !tbaa !28
  %call94 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements193, i32 %108)
  %109 = load i32, i32* %q84, align 4, !tbaa !28
  %call95 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %call94, i32 %109)
  %110 = load i32, i32* %call95, align 4, !tbaa !28
  store i32 %110, i32* %v92, align 4, !tbaa !28
  %111 = bitcast float* %w96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #8
  %112 = load i32, i32* %i, align 4, !tbaa !28
  %113 = load i32, i32* %v92, align 4, !tbaa !28
  %call97 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %112, i32 %113)
  %114 = load float, float* %call97, align 4, !tbaa !42
  store float %114, float* %w96, align 4, !tbaa !42
  %115 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %116 = load i32, i32* %v92, align 4, !tbaa !28
  %117 = load i32, i32* %j, align 4, !tbaa !28
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %115, i32 %116, i32 %117)
  %118 = load float, float* %call98, align 4, !tbaa !42
  %cmp99 = fcmp une float %118, 0.000000e+00
  br i1 %cmp99, label %if.then100, label %if.end104

if.then100:                                       ; preds = %for.body91
  %119 = load float, float* %w96, align 4, !tbaa !42
  %120 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4, !tbaa !2
  %121 = load i32, i32* %v92, align 4, !tbaa !28
  %122 = load i32, i32* %j, align 4, !tbaa !28
  %call101 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %120, i32 %121, i32 %122)
  %123 = load float, float* %call101, align 4, !tbaa !42
  %mul102 = fmul float %119, %123
  %124 = load float, float* %dotProd, align 4, !tbaa !42
  %add103 = fadd float %124, %mul102
  store float %add103, float* %dotProd, align 4, !tbaa !42
  br label %if.end104

if.end104:                                        ; preds = %if.then100, %for.body91
  %125 = bitcast float* %w96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #8
  %126 = bitcast i32* %v92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %127 = load i32, i32* %q84, align 4, !tbaa !28
  %inc106 = add nsw i32 %127, 1
  store i32 %inc106, i32* %q84, align 4, !tbaa !28
  br label %for.cond85

for.end107:                                       ; preds = %for.cond.cleanup90
  br label %if.end108

if.end108:                                        ; preds = %for.end107, %for.end82
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %useOtherCol) #8
  br label %if.end109

if.end109:                                        ; preds = %if.end108, %while.end
  %128 = load float, float* %dotProd, align 4, !tbaa !42
  %tobool110 = fcmp une float %128, 0.000000e+00
  br i1 %tobool110, label %if.then111, label %if.end112

if.then111:                                       ; preds = %if.end109
  %129 = load i32, i32* %i, align 4, !tbaa !28
  %130 = load i32, i32* %j, align 4, !tbaa !28
  %131 = load float, float* %dotProd, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %agg.result, i32 %129, i32 %130, float %131)
  br label %if.end112

if.end112:                                        ; preds = %if.then111, %if.end109
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %doubleWalk) #8
  %132 = bitcast i32* %waste2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #8
  %133 = bitcast i32* %waste to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #8
  %134 = bitcast float* %dotProd2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #8
  %135 = bitcast float* %dotProd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #8
  br label %for.inc113

for.inc113:                                       ; preds = %if.end112
  %136 = load i32, i32* %i, align 4, !tbaa !28
  %inc114 = add nsw i32 %136, 1
  store i32 %inc114, i32* %i, align 4, !tbaa !28
  br label %for.cond5

for.end115:                                       ; preds = %for.cond.cleanup8
  br label %for.inc116

for.inc116:                                       ; preds = %for.end115
  %137 = load i32, i32* %j, align 4, !tbaa !28
  %inc117 = add nsw i32 %137, 1
  store i32 %inc117, i32* %j, align 4, !tbaa !28
  br label %for.cond

for.end118:                                       ; preds = %for.cond.cleanup
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end118
  %call119 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end118
  ret void
}

define hidden float @_ZN12btMLCPSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMLCPSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %result = alloca i8, align 1
  %__profile = alloca %class.CProfileSample, align 1
  %__profile4 = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %c = alloca %struct.btSolverConstraint*, align 4
  %sbA = alloca i32, align 4
  %sbB = alloca i32, align 4
  %orgBodyA = alloca %class.btRigidBody*, align 4
  %orgBodyB = alloca %class.btRigidBody*, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  store i32 %numBodies, i32* %numBodies.addr, align 4, !tbaa !28
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  store i32 %numManifolds, i32* %numManifolds.addr, align 4, !tbaa !28
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  store i32 %numConstraints, i32* %numConstraints.addr, align 4, !tbaa !28
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %result) #8
  store i8 1, i8* %result, align 1, !tbaa !29
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #8
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.25, i32 0, i32 0))
  %1 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %2 = bitcast %class.btMLCPSolver* %this1 to i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable = load i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable, i64 15
  %3 = load i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn, align 4
  %call2 = call zeroext i1 %3(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %1)
  %frombool = zext i1 %call2 to i8
  store i8 %frombool, i8* %result, align 1, !tbaa !29
  %call3 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %4 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %4) #8
  %5 = load i8, i8* %result, align 1, !tbaa !29, !range !30
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = bitcast %class.CProfileSample* %__profile4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %6) #8
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile4, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.26, i32 0, i32 0))
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %m_allConstraintArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %m_allConstraintArray)
  %cmp = icmp slt i32 %8, %call6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %struct.btSolverConstraint** %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %m_allConstraintArray7 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %call8 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.5* %m_allConstraintArray7, i32 %11)
  store %struct.btSolverConstraint* %call8, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %12 = bitcast i32* %sbA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %13, i32 0, i32 18
  %14 = load i32, i32* %m_solverBodyIdA, align 4, !tbaa !54
  store i32 %14, i32* %sbA, align 4, !tbaa !28
  %15 = bitcast i32* %sbB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %16, i32 0, i32 19
  %17 = load i32, i32* %m_solverBodyIdB, align 4, !tbaa !55
  store i32 %17, i32* %sbB, align 4, !tbaa !28
  %18 = bitcast %class.btRigidBody** %orgBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %19, i32 0, i32 1
  %20 = load i32, i32* %sbA, align 4, !tbaa !28
  %call9 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %20)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call9, i32 0, i32 12
  %21 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !56
  store %class.btRigidBody* %21, %class.btRigidBody** %orgBodyA, align 4, !tbaa !2
  %22 = bitcast %class.btRigidBody** %orgBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %23, i32 0, i32 1
  %24 = load i32, i32* %sbB, align 4, !tbaa !28
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %24)
  %m_originalBody12 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call11, i32 0, i32 12
  %25 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody12, align 4, !tbaa !56
  store %class.btRigidBody* %25, %class.btRigidBody** %orgBodyB, align 4, !tbaa !2
  %26 = bitcast %struct.btSolverBody** %solverBodyA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool13 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %27, i32 0, i32 1
  %28 = load i32, i32* %sbA, align 4, !tbaa !28
  %call14 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool13, i32 %28)
  store %struct.btSolverBody* %call14, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %29 = bitcast %struct.btSolverBody** %solverBodyB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool15 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %30, i32 0, i32 1
  %31 = load i32, i32* %sbB, align 4, !tbaa !28
  %call16 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool15, i32 %31)
  store %struct.btSolverBody* %call16, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %32 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %33 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #8
  %34 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %34, i32 0, i32 1
  %35 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %35)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call17)
  %36 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %36, i32 0, i32 4
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %37 = load i32, i32* %i, align 4, !tbaa !28
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x, i32 %37)
  %38 = load float, float* %call18, align 4, !tbaa !42
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %38)
  %39 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #8
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %41 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  %42 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %42, i32 0, i32 3
  %43 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %43)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call20)
  %44 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %44, i32 0, i32 5
  %m_x21 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %45 = load i32, i32* %i, align 4, !tbaa !28
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x21, i32 %45)
  %46 = load float, float* %call22, align 4, !tbaa !42
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %46)
  %47 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %49 = bitcast %struct.btContactSolverInfo* %48 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %49, i32 0, i32 11
  %50 = load i32, i32* %m_splitImpulse, align 4, !tbaa !45
  %tobool23 = icmp ne i32 %50, 0
  br i1 %tobool23, label %if.then24, label %if.end

if.then24:                                        ; preds = %for.body
  %51 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %52 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  %53 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_contactNormal126 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %53, i32 0, i32 1
  %54 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %54)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal126, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %55 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_angularComponentA28 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %55, i32 0, i32 4
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %56 = load i32, i32* %i, align 4, !tbaa !28
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit, i32 %56)
  %57 = load float, float* %call29, align 4, !tbaa !42
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA28, float %57)
  %58 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #8
  %59 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %60 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #8
  %61 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_contactNormal231 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %61, i32 0, i32 3
  %62 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %62)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal231, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %63 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_angularComponentB33 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %63, i32 0, i32 5
  %m_xSplit34 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %64 = load i32, i32* %i, align 4, !tbaa !28
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit34, i32 %64)
  %65 = load float, float* %call35, align 4, !tbaa !42
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %59, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB33, float %65)
  %66 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  %m_xSplit36 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %67 = load i32, i32* %i, align 4, !tbaa !28
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit36, i32 %67)
  %68 = load float, float* %call37, align 4, !tbaa !42
  %69 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %69, i32 0, i32 6
  store float %68, float* %m_appliedPushImpulse, align 4, !tbaa !72
  br label %if.end

if.end:                                           ; preds = %if.then24, %for.body
  %m_x38 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %70 = load i32, i32* %i, align 4, !tbaa !28
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x38, i32 %70)
  %71 = load float, float* %call39, align 4, !tbaa !42
  %72 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4, !tbaa !2
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %72, i32 0, i32 7
  store float %71, float* %m_appliedImpulse, align 4, !tbaa !71
  %73 = bitcast %struct.btSolverBody** %solverBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  %74 = bitcast %struct.btSolverBody** %solverBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast %class.btRigidBody** %orgBodyB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast %class.btRigidBody** %orgBodyA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %77 = bitcast i32* %sbB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast i32* %sbA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = bitcast %struct.btSolverConstraint** %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %80 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %call40 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile4) #8
  %81 = bitcast %class.CProfileSample* %__profile4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %81) #8
  br label %if.end43

if.else:                                          ; preds = %entry
  %m_fallback = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 13
  %82 = load i32, i32* %m_fallback, align 4, !tbaa !22
  %inc41 = add nsw i32 %82, 1
  store i32 %inc41, i32* %m_fallback, align 4, !tbaa !22
  %83 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %84 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4, !tbaa !2
  %85 = load i32, i32* %numBodies.addr, align 4, !tbaa !28
  %86 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4, !tbaa !2
  %87 = load i32, i32* %numManifolds.addr, align 4, !tbaa !28
  %88 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4, !tbaa !2
  %89 = load i32, i32* %numConstraints.addr, align 4, !tbaa !28
  %90 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %91 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %call42 = call float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %83, %class.btCollisionObject** %84, i32 %85, %class.btPersistentManifold** %86, i32 %87, %class.btTypedConstraint** %88, i32 %89, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %90, %class.btIDebugDraw* %91)
  br label %if.end43

if.end43:                                         ; preds = %if.else, %for.end
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %result) #8
  ret float 0.000000e+00
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4, !tbaa !42
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !56
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !42
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !42
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !42
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !42
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !42
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !42
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !42
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !42
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !42
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #6 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4, !tbaa !42
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4, !tbaa !56
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_pushVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4, !tbaa !2
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_turnVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !28
  store i32 %1, i32* %.addr1, align 4, !tbaa !28
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*, %class.btDispatcher*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %0, %class.btIDebugDraw* %1) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK12btMLCPSolver13getSolverTypeEv(%class.btMLCPSolver* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  ret i32 2
}

declare void @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84)) unnamed_addr #1

declare void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #1

declare float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84)) unnamed_addr #1

declare float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), %class.btIDebugDraw*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.18* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.18* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !28
  store i32 %last, i32* %last.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %last.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !88
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !89
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.18* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !88
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !90, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !88
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.19* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !88
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !90
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !88
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !89
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !91
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.19* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.22* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.22* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !28
  store i32 %last, i32* %last.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %last.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %4 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %4, i32 %5
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !93
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.22* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %tobool = icmp ne %class.btAlignedObjectArray.10* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !94, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data4, align 4, !tbaa !92
  call void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.23* %m_allocator, %class.btAlignedObjectArray.10* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.10* null, %class.btAlignedObjectArray.10** %m_data5, align 4, !tbaa !92
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.22* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !94
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.10* null, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !93
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !95
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.23* %this, %class.btAlignedObjectArray.10* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %ptr.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.10* %ptr, %class.btAlignedObjectArray.10** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btAlignedObjectArray.10* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_ZN15CProfileManager13Start_ProfileEPKc(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

declare void @_ZN15CProfileManager12Stop_ProfileEv() #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #5

define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray.18* returned %this, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.18* %otherArray, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.19* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.18* %this1)
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %1)
  store i32 %call2, i32* %otherSize, align 4, !tbaa !28
  %2 = load i32, i32* %otherSize, align 4, !tbaa !28
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !42
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.18* %this1, i32 %2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4, !tbaa !88
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.18* %5, i32 0, i32 %6, float* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret %class.btAlignedObjectArray.18* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_(%class.btAlignedObjectArray.22* returned %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btAlignedObjectArray.10, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.22* %otherArray, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.23* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.22* %this1)
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %1)
  store i32 %call2, i32* %otherSize, align 4, !tbaa !28
  %2 = load i32, i32* %otherSize, align 4, !tbaa !28
  %3 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %3) #8
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %this1, i32 %2, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call4 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %ref.tmp) #8
  %4 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.22* %5, i32 0, i32 %6, %class.btAlignedObjectArray.10* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.19* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  ret %class.btAlignedAllocator.19* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.18* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !28
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !28
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %2 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  store i32 %4, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %6 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !88
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.18* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !28
  store i32 %14, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !28
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !88
  %19 = load i32, i32* %i6, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !2
  %23 = load float, float* %22, align 4, !tbaa !42
  store float %23, float* %21, align 4, !tbaa !42
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !28
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !28
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !89
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.18* %this, i32 %start, i32 %end, float* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !28
  store i32 %end, i32* %end.addr, align 4, !tbaa !28
  store float* %dest, float** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %end.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !88
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !42
  store float %10, float* %7, align 4, !tbaa !42
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.18* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.18* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.18* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !90
  %5 = load float*, float** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !88
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !91
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !91
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.18* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.19* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.19* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  store float** %hint, float*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !28
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.23* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  ret %class.btAlignedAllocator.23* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %this, i32 %newsize, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i7 = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !28
  store %class.btAlignedObjectArray.10* %fillData, %class.btAlignedObjectArray.10** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !28
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %2 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  store i32 %4, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %6 = load i32, i32* %curSize, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %8 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %8, i32 %9
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end18

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %cmp5 = icmp sgt i32 %11, %call4
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIS_IiEE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else
  %13 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !28
  store i32 %14, i32* %i7, align 4, !tbaa !28
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc15, %if.end
  %15 = load i32, i32* %i7, align 4, !tbaa !28
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %cmp9 = icmp slt i32 %15, %16
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond8
  %17 = bitcast i32* %i7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end17

for.body11:                                       ; preds = %for.cond8
  %m_data12 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %18 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data12, align 4, !tbaa !92
  %19 = load i32, i32* %i7, align 4, !tbaa !28
  %arrayidx13 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %18, i32 %19
  %20 = bitcast %class.btAlignedObjectArray.10* %arrayidx13 to i8*
  %21 = bitcast i8* %20 to %class.btAlignedObjectArray.10*
  %22 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %fillData.addr, align 4, !tbaa !2
  %call14 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.10* %21, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %22)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body11
  %23 = load i32, i32* %i7, align 4, !tbaa !28
  %inc16 = add nsw i32 %23, 1
  store i32 %inc16, i32* %i7, align 4, !tbaa !28
  br label %for.cond8

for.end17:                                        ; preds = %for.cond.cleanup10
  br label %if.end18

if.end18:                                         ; preds = %for.end17, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !28
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !93
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.22* %this, i32 %start, i32 %end, %class.btAlignedObjectArray.10* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !28
  store i32 %end, i32* %end.addr, align 4, !tbaa !28
  store %class.btAlignedObjectArray.10* %dest, %class.btAlignedObjectArray.10** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %end.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %4, i32 %5
  %6 = bitcast %class.btAlignedObjectArray.10* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btAlignedObjectArray.10*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %8 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %8, i32 %9
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.10* %7, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE7reserveEi(%class.btAlignedObjectArray.22* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btAlignedObjectArray.10** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %call2 = call i8* @_ZN20btAlignedObjectArrayIS_IiEE8allocateEi(%class.btAlignedObjectArray.22* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btAlignedObjectArray.10*
  store %class.btAlignedObjectArray.10* %3, %class.btAlignedObjectArray.10** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %4 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call3, %class.btAlignedObjectArray.10* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !94
  %5 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.10* %5, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !95
  %7 = bitcast %class.btAlignedObjectArray.10** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv(%class.btAlignedObjectArray.22* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !95
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIS_IiEE8allocateEi(%class.btAlignedObjectArray.22* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %call = call %class.btAlignedObjectArray.10* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.23* %m_allocator, i32 %1, %class.btAlignedObjectArray.10** null)
  %2 = bitcast %class.btAlignedObjectArray.10* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.23* %this, i32 %n, %class.btAlignedObjectArray.10** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btAlignedObjectArray.10**, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  store %class.btAlignedObjectArray.10** %hint, %class.btAlignedObjectArray.10*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !28
  %mul = mul i32 20, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btAlignedObjectArray.10*
  ret %class.btAlignedObjectArray.10* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !42
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !42
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !42
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !42
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !42
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !42
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !42
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !42
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !42
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !42
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !42
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !42
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !42
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !42
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !42
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !42
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !42
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !42
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !42
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !42
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !42
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !42
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !42
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !42
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !42
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEaSERKS0_(%class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %other.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.18* %other, %class.btAlignedObjectArray.18** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %other.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_(%class.btAlignedObjectArray.18* %this1, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %0)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_(%class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %other.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.22* %other, %class.btAlignedObjectArray.22** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %other.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_(%class.btAlignedObjectArray.22* %this1, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %0)
  ret %class.btAlignedObjectArray.22* %this1
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_(%class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %otherArray) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.18* %otherArray, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.18* %1)
  store i32 %call, i32* %otherSize, align 4, !tbaa !28
  %2 = load i32, i32* %otherSize, align 4, !tbaa !28
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !42
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.18* %this1, i32 %2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4, !tbaa !88
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.18* %5, i32 0, i32 %6, float* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_(%class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %otherArray) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btAlignedObjectArray.10, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.22* %otherArray, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.22* %1)
  store i32 %call, i32* %otherSize, align 4, !tbaa !28
  %2 = load i32, i32* %otherSize, align 4, !tbaa !28
  %3 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %3) #8
  %call2 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %this1, i32 %2, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %ref.tmp) #8
  %4 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %4) #8
  %5 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.22* %5, i32 0, i32 %6, %class.btAlignedObjectArray.10* %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !42
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !42
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !42
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !42
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !42
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !42
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !42
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !42
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !42
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.11* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  ret %class.btAlignedAllocator.11* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !96
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !43
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !44
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !97
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.10* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.10* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !28
  store i32 %last, i32* %last.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %last.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !43
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !44
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.10* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !43
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !96, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !43
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.11* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !43
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.11* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !88
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %0, i32 %1
  ret %class.btAlignedObjectArray.10* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.10* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32* %_Val, i32** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !28
  %1 = load i32, i32* %sz, align 4, !tbaa !28
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.10* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data, align 4, !tbaa !43
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !44
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = bitcast i32* %arrayidx to i8*
  %5 = bitcast i8* %4 to i32*
  %6 = load i32*, i32** %_Val.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !28
  store i32 %7, i32* %5, align 4, !tbaa !28
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !44
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !44
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !97
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.10* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.10* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !96
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !43
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !97
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.10* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.10* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.11* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.10* %this, i32 %start, i32 %end, i32* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !28
  store i32 %end, i32* %end.addr, align 4, !tbaa !28
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %end.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !43
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !28
  store i32 %10, i32* %7, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.11* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !28
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !88
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.19* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.23* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.6* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  ret %class.btAlignedAllocator.6* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !98
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* null, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !35
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !99
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.5* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.5* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !28
  store i32 %last, i32* %last.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %last.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.5* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %tobool = icmp ne %struct.btSolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !98, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data4, align 4, !tbaa !39
  call void @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %m_allocator, %struct.btSolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* null, %struct.btSolverConstraint** %m_data5, align 4, !tbaa !39
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %this, %struct.btSolverConstraint* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %ptr.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  store %struct.btSolverConstraint* %ptr, %struct.btSolverConstraint** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btSolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.5* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSolverConstraint*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btSolverConstraint** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.5* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btSolverConstraint*
  store %struct.btSolverConstraint* %3, %struct.btSolverConstraint** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call3, %struct.btSolverConstraint* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !98
  %5 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* %5, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !28
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !99
  %7 = bitcast %struct.btSolverConstraint** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btSolverConstraintnwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !67
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !99
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.5* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %call = call %struct.btSolverConstraint* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %m_allocator, i32 %1, %struct.btSolverConstraint** null)
  %2 = bitcast %struct.btSolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.5* %this, i32 %start, i32 %end, %struct.btSolverConstraint* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !28
  store i32 %end, i32* %end.addr, align 4, !tbaa !28
  store %struct.btSolverConstraint* %dest, %struct.btSolverConstraint** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %end.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %4, i32 %5
  %6 = bitcast %struct.btSolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN18btSolverConstraintnwEmPv(i32 152, i8* %6)
  %7 = bitcast i8* %call to %struct.btSolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4, !tbaa !39
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %8, i32 %9
  %10 = bitcast %struct.btSolverConstraint* %7 to i8*
  %11 = bitcast %struct.btSolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 152, i1 false), !tbaa.struct !40
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %struct.btSolverConstraint* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %this, i32 %n, %struct.btSolverConstraint** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSolverConstraint**, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  store %struct.btSolverConstraint** %hint, %struct.btSolverConstraint*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !28
  %mul = mul i32 152, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSolverConstraint*
  ret %struct.btSolverConstraint* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.5* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE15clearSparseInfoEv(%struct.btMatrixX* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca %class.btAlignedObjectArray.10, align 4
  %ref.tmp4 = alloca %class.btAlignedObjectArray.10, align 4
  %i = alloca i32, align 4
  %ref.tmp10 = alloca i32, align 4
  %j = alloca i32, align 4
  %ref.tmp18 = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #8
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.28, i32 0, i32 0))
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_rows, align 4, !tbaa !23
  %2 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %2) #8
  %call2 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1, i32 %1, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %ref.tmp) #8
  %3 = bitcast %class.btAlignedObjectArray.10* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %3) #8
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_cols, align 4, !tbaa !24
  %5 = bitcast %class.btAlignedObjectArray.10* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %5) #8
  %call5 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.10* %ref.tmp4)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %m_colNonZeroElements, i32 %4, %class.btAlignedObjectArray.10* nonnull align 4 dereferenceable(17) %ref.tmp4)
  %call6 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.10* %ref.tmp4) #8
  %6 = bitcast %class.btAlignedObjectArray.10* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %6) #8
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %m_rows7 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %9 = load i32, i32* %m_rows7, align 4, !tbaa !23
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_rowNonZeroElements18 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %11 = load i32, i32* %i, align 4, !tbaa !28
  %call9 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_rowNonZeroElements18, i32 %11)
  %12 = bitcast i32* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store i32 0, i32* %ref.tmp10, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %call9, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %13 = bitcast i32* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %15 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc19, %for.end
  %16 = load i32, i32* %j, align 4, !tbaa !28
  %m_cols12 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %17 = load i32, i32* %m_cols12, align 4, !tbaa !24
  %cmp13 = icmp slt i32 %16, %17
  br i1 %cmp13, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond11
  %18 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %for.end21

for.body15:                                       ; preds = %for.cond11
  %m_colNonZeroElements16 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %19 = load i32, i32* %j, align 4, !tbaa !28
  %call17 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %m_colNonZeroElements16, i32 %19)
  %20 = bitcast i32* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store i32 0, i32* %ref.tmp18, align 4, !tbaa !28
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.10* %call17, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %21 = bitcast i32* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.inc19

for.inc19:                                        ; preds = %for.body15
  %22 = load i32, i32* %j, align 4, !tbaa !28
  %inc20 = add nsw i32 %22, 1
  store i32 %inc20, i32* %j, align 4, !tbaa !28
  br label %for.cond11

for.end21:                                        ; preds = %for.cond.cleanup14
  %call22 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #8
  %23 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %23) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.27* @_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev(%class.btAlignedAllocator.27* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  ret %class.btAlignedAllocator.27* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.26* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !73
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %struct.btJointNode* null, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !82
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !77
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv(%class.btAlignedObjectArray.26* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.26* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.26* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !28
  store i32 %last, i32* %last.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %last.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %4 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.26* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %tobool = icmp ne %struct.btJointNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !73, !range !30
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %2 = load %struct.btJointNode*, %struct.btJointNode** %m_data4, align 4, !tbaa !76
  call void @_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.27* %m_allocator, %struct.btJointNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %struct.btJointNode* null, %struct.btJointNode** %m_data5, align 4, !tbaa !76
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.27* %this, %struct.btJointNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  %ptr.addr = alloca %struct.btJointNode*, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4, !tbaa !2
  store %struct.btJointNode* %ptr, %struct.btJointNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btJointNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.26* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !77
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi(%class.btAlignedObjectArray.26* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %call = call %struct.btJointNode* @_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.27* %m_allocator, i32 %1, %struct.btJointNode** null)
  %2 = bitcast %struct.btJointNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_(%class.btAlignedObjectArray.26* %this, i32 %start, i32 %end, %struct.btJointNode* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btJointNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !28
  store i32 %end, i32* %end.addr, align 4, !tbaa !28
  store %struct.btJointNode* %dest, %struct.btJointNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !28
  store i32 %1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !28
  %3 = load i32, i32* %end.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btJointNode*, %struct.btJointNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %4, i32 %5
  %6 = bitcast %struct.btJointNode* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btJointNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %8 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4, !tbaa !76
  %9 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx2 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %8, i32 %9
  %10 = bitcast %struct.btJointNode* %7 to i8*
  %11 = bitcast %struct.btJointNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !83
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %struct.btJointNode* @_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.27* %this, i32 %n, %struct.btJointNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btJointNode**, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  store %struct.btJointNode** %hint, %struct.btJointNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !28
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btJointNode*
  ret %struct.btJointNode* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z9btSetZeroIfEvPT_i(float* %a, i32 %n) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %acurr = alloca float*, align 4
  %ncurr = alloca i32, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %0 = bitcast float** %acurr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %a.addr, align 4, !tbaa !2
  store float* %1, float** %acurr, align 4, !tbaa !2
  %2 = bitcast i32* %ncurr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %n.addr, align 4, !tbaa !28
  store i32 %3, i32* %ncurr, align 4, !tbaa !67
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %ncurr, align 4, !tbaa !67
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load float*, float** %acurr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds float, float* %5, i32 1
  store float* %incdec.ptr, float** %acurr, align 4, !tbaa !2
  store float 0.000000e+00, float* %5, align 4, !tbaa !42
  %6 = load i32, i32* %ncurr, align 4, !tbaa !67
  %dec = add i32 %6, -1
  store i32 %dec, i32* %ncurr, align 4, !tbaa !67
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = bitcast i32* %ncurr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float** %acurr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi(%class.btAlignedObjectArray.26* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !28
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

define linkonce_odr hidden void @_ZN9btMatrixXIfE7addElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !28
  store i32 %col, i32* %col.addr, align 4, !tbaa !28
  store float %val, float* %val.addr, align 4, !tbaa !42
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = load float, float* %val.addr, align 4, !tbaa !42
  %tobool = fcmp une float %0, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %1 = load i32, i32* %col.addr, align 4, !tbaa !28
  %2 = load i32, i32* %row.addr, align 4, !tbaa !28
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_cols, align 4, !tbaa !24
  %mul = mul nsw i32 %2, %3
  %add = add nsw i32 %1, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage, i32 %add)
  %4 = load float, float* %call, align 4, !tbaa !42
  %cmp = fcmp oeq float %4, 0.000000e+00
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %5 = load i32, i32* %row.addr, align 4, !tbaa !28
  %6 = load i32, i32* %col.addr, align 4, !tbaa !28
  %7 = load float, float* %val.addr, align 4, !tbaa !42
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %5, i32 %6, float %7)
  br label %if.end

if.else:                                          ; preds = %if.then
  %8 = load float, float* %val.addr, align 4, !tbaa !42
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %9 = load i32, i32* %row.addr, align 4, !tbaa !28
  %m_cols4 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %10 = load i32, i32* %m_cols4, align 4, !tbaa !24
  %mul5 = mul nsw i32 %9, %10
  %11 = load i32, i32* %col.addr, align 4, !tbaa !28
  %add6 = add nsw i32 %mul5, %11
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.18* %m_storage3, i32 %add6)
  %12 = load float, float* %call7, align 4, !tbaa !42
  %add8 = fadd float %12, %8
  store float %add8, float* %call7, align 4, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* returned %this, i32 %rows, i32 %cols) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !28
  store i32 %cols, i32* %cols.addr, align 4, !tbaa !28
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %rows.addr, align 4, !tbaa !28
  store i32 %0, i32* %m_rows, align 4, !tbaa !23
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %1 = load i32, i32* %cols.addr, align 4, !tbaa !28
  store i32 %1, i32* %m_cols, align 4, !tbaa !24
  %m_operations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 2
  store i32 0, i32* %m_operations, align 4, !tbaa !25
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  store i32 0, i32* %m_resizeOperations, align 4, !tbaa !26
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  store i32 0, i32* %m_setElemOperations, align 4, !tbaa !27
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.18* %m_storage)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.22* %m_rowNonZeroElements1)
  %m_colNonZeroElements = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 7
  %call3 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.22* %m_colNonZeroElements)
  %2 = load i32, i32* %rows.addr, align 4, !tbaa !28
  %3 = load i32, i32* %cols.addr, align 4, !tbaa !28
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this1, i32 %2, i32 %3)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZNK20btAlignedObjectArrayIS_IiEEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %m_data, align 4, !tbaa !92
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %0, i32 %1
  ret %class.btAlignedObjectArray.10* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !28
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !43
  %1 = load i32, i32* %n.addr, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_cols, align 4, !tbaa !24
  ret i32 %0
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 476}
!9 = !{!"_ZTS12btMLCPSolver", !10, i64 196, !17, i64 276, !17, i64 296, !17, i64 316, !17, i64 336, !17, i64 356, !17, i64 376, !17, i64 396, !17, i64 416, !18, i64 436, !20, i64 456, !3, i64 476, !11, i64 480}
!10 = !{!"_ZTS9btMatrixXIfE", !11, i64 0, !11, i64 4, !11, i64 8, !11, i64 12, !11, i64 16, !12, i64 20, !15, i64 40, !15, i64 60}
!11 = !{!"int", !4, i64 0}
!12 = !{!"_ZTS20btAlignedObjectArrayIfE", !13, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!13 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!14 = !{!"bool", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayIS_IiEE", !16, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE"}
!17 = !{!"_ZTS9btVectorXIfE", !12, i64 0}
!18 = !{!"_ZTS20btAlignedObjectArrayIiE", !19, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!20 = !{!"_ZTS20btAlignedObjectArrayI18btSolverConstraintE", !21, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorI18btSolverConstraintLj16EE"}
!22 = !{!9, !11, i64 480}
!23 = !{!10, !11, i64 0}
!24 = !{!10, !11, i64 4}
!25 = !{!10, !11, i64 8}
!26 = !{!10, !11, i64 12}
!27 = !{!10, !11, i64 16}
!28 = !{!11, !11, i64 0}
!29 = !{!14, !14, i64 0}
!30 = !{i8 0, i8 2}
!31 = !{!32, !11, i64 140}
!32 = !{!"_ZTS18btSolverConstraint", !33, i64 0, !33, i64 16, !33, i64 32, !33, i64 48, !33, i64 64, !33, i64 80, !34, i64 96, !34, i64 100, !34, i64 104, !34, i64 108, !34, i64 112, !34, i64 116, !34, i64 120, !34, i64 124, !34, i64 128, !4, i64 132, !11, i64 136, !11, i64 140, !11, i64 144, !11, i64 148}
!33 = !{!"_ZTS9btVector3", !4, i64 0}
!34 = !{!"float", !4, i64 0}
!35 = !{!20, !11, i64 4}
!36 = !{!37, !11, i64 4}
!37 = !{!"_ZTS20btAlignedObjectArrayI12btSolverBodyE", !38, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorI12btSolverBodyLj16EE"}
!39 = !{!20, !3, i64 12}
!40 = !{i64 0, i64 16, !41, i64 16, i64 16, !41, i64 32, i64 16, !41, i64 48, i64 16, !41, i64 64, i64 16, !41, i64 80, i64 16, !41, i64 96, i64 4, !42, i64 100, i64 4, !42, i64 104, i64 4, !42, i64 108, i64 4, !42, i64 112, i64 4, !42, i64 116, i64 4, !42, i64 120, i64 4, !42, i64 124, i64 4, !42, i64 128, i64 4, !42, i64 132, i64 4, !2, i64 132, i64 4, !42, i64 132, i64 4, !28, i64 136, i64 4, !28, i64 140, i64 4, !28, i64 144, i64 4, !28, i64 148, i64 4, !28}
!41 = !{!4, !4, i64 0}
!42 = !{!34, !34, i64 0}
!43 = !{!18, !3, i64 12}
!44 = !{!18, !11, i64 4}
!45 = !{!46, !11, i64 44}
!46 = !{!"_ZTS23btContactSolverInfoData", !34, i64 0, !34, i64 4, !34, i64 8, !34, i64 12, !34, i64 16, !11, i64 20, !34, i64 24, !34, i64 28, !34, i64 32, !34, i64 36, !34, i64 40, !11, i64 44, !34, i64 48, !34, i64 52, !34, i64 56, !34, i64 60, !11, i64 64, !11, i64 68, !11, i64 72, !34, i64 76, !34, i64 80}
!47 = !{!46, !11, i64 20}
!48 = !{!32, !34, i64 108}
!49 = !{!32, !34, i64 112}
!50 = !{!32, !34, i64 128}
!51 = !{!32, !34, i64 120}
!52 = !{!32, !34, i64 124}
!53 = !{!"branch_weights", i32 1, i32 1048575}
!54 = !{!32, !11, i64 144}
!55 = !{!32, !11, i64 148}
!56 = !{!57, !3, i64 240}
!57 = !{!"_ZTS12btSolverBody", !58, i64 0, !33, i64 64, !33, i64 80, !33, i64 96, !33, i64 112, !33, i64 128, !33, i64 144, !33, i64 160, !33, i64 176, !33, i64 192, !33, i64 208, !33, i64 224, !3, i64 240}
!58 = !{!"_ZTS11btTransform", !59, i64 0, !33, i64 48}
!59 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!60 = !{!61, !11, i64 0}
!61 = !{!"_ZTSN17btTypedConstraint17btConstraintInfo1E", !11, i64 0, !11, i64 4}
!62 = !{!63, !11, i64 8}
!63 = !{!"_ZTS11btJointNode", !11, i64 0, !11, i64 4, !11, i64 8, !11, i64 12}
!64 = !{!63, !11, i64 0}
!65 = !{!63, !11, i64 12}
!66 = !{!63, !11, i64 4}
!67 = !{!68, !68, i64 0}
!68 = !{!"long", !4, i64 0}
!69 = !{!46, !34, i64 12}
!70 = !{!46, !11, i64 64}
!71 = !{!32, !34, i64 100}
!72 = !{!32, !34, i64 96}
!73 = !{!74, !14, i64 16}
!74 = !{!"_ZTS20btAlignedObjectArrayI11btJointNodeE", !75, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!75 = !{!"_ZTS18btAlignedAllocatorI11btJointNodeLj16EE"}
!76 = !{!74, !3, i64 12}
!77 = !{!74, !11, i64 8}
!78 = !{!37, !3, i64 12}
!79 = !{!80, !3, i64 12}
!80 = !{!"_ZTS20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE", !81, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!81 = !{!"_ZTS18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE"}
!82 = !{!74, !11, i64 4}
!83 = !{i64 0, i64 4, !28, i64 4, i64 4, !28, i64 8, i64 4, !28, i64 12, i64 4, !28}
!84 = !{!85, !34, i64 344}
!85 = !{!"_ZTS11btRigidBody", !59, i64 264, !33, i64 312, !33, i64 328, !34, i64 344, !33, i64 348, !33, i64 364, !33, i64 380, !33, i64 396, !33, i64 412, !33, i64 428, !34, i64 444, !34, i64 448, !14, i64 452, !34, i64 456, !34, i64 460, !34, i64 464, !34, i64 468, !34, i64 472, !34, i64 476, !3, i64 480, !86, i64 484, !11, i64 504, !11, i64 508, !33, i64 512, !33, i64 528, !33, i64 544, !33, i64 560, !33, i64 576, !33, i64 592, !11, i64 608, !11, i64 612}
!86 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !87, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !14, i64 16}
!87 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!88 = !{!12, !3, i64 12}
!89 = !{!12, !11, i64 4}
!90 = !{!12, !14, i64 16}
!91 = !{!12, !11, i64 8}
!92 = !{!15, !3, i64 12}
!93 = !{!15, !11, i64 4}
!94 = !{!15, !14, i64 16}
!95 = !{!15, !11, i64 8}
!96 = !{!18, !14, i64 16}
!97 = !{!18, !11, i64 8}
!98 = !{!20, !14, i64 16}
!99 = !{!20, !11, i64 8}
