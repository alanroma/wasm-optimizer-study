const ProgramDetails = require('./ProgramDetails');
const { argv } = require('yargs')
// .demandCommand(2)
.option('tab-separated', {
    alias: '-t',
    type: 'boolean',
    description: 'Pass to only print out the information in a tab-separated line (perfect for pasting into CSV files).',
    default: false
})
.option('dir', {
    alias: '-d',
    type: 'string',
    description: 'Directory of Optimization Pass and Flag Outputs'
});
const wabtRequire = require("wabt");
const fs = require('fs');
const { promisify } = require('util');
const { resolve } = require('path');
const readDirAsync = promisify(fs.readdir);
const stat = promisify(fs.stat);

async function readPassDirectories(){
    const subdirectories = (await readDirAsync(argv.dir)).filter(name => !name.endsWith('.wasm'));
    const passMap = new Map();
    for(const subdirName of subdirectories){
        const subdir = resolve(argv.dir, subdirName); 
        const filesInSubdir = await readDirAsync(subdir);
        const baselineFile = filesInSubdir.filter(file => (file.includes('baseline') || file.includes('original')) && file.endsWith('.wat'))[0];
        const optimizedFile = filesInSubdir.filter(file => (file.includes('c') || file.includes('O')) && file.endsWith('.wat'))[0];
        if(!baselineFile || !optimizedFile){
            console.log(baselineFile,optimizedFile,filesInSubdir)
        }
        const filesToUse = {
            baseline: resolve(subdir,baselineFile),
            optimized:resolve(subdir,optimizedFile)
        }
        passMap.set(subdirName, filesToUse)
    }
    return passMap;
}

async function getFileDifferences(firstFile, secondFile,wabtModule, nameToPrint = ''){
    const printAsCSVLine = argv["tab-separated"];
    const firstFileDetails = new ProgramDetails(firstFile,wabtModule);
    const secondFileDetails = new ProgramDetails(secondFile,wabtModule);
    try {
        const firstFileFeatures = await firstFileDetails.main();
        const secondFileFeatures = await secondFileDetails.main();
        const binaryFileSizeChange = secondFileFeatures.WasmFileSize - firstFileFeatures.WasmFileSize;
        const locChange = secondFileFeatures.TotalLinesOfCode - firstFileFeatures.TotalLinesOfCode;
        const numberOfFunctionChange = secondFileFeatures.NumberOfFunctions - firstFileFeatures.NumberOfFunctions;
        const numberOfImportChange = secondFileFeatures.NumberOfImports - firstFileFeatures.NumberOfImports;
        const numberOfExportChange = secondFileFeatures.NumberOfExports - firstFileFeatures.NumberOfExports;
        const instructionCountChange = secondFileFeatures.TotalInstructionCount - firstFileFeatures.TotalInstructionCount;
        const typeChanges = firstFileDetails.getDifferentTypes(secondFileDetails);
        const globalChanges = firstFileDetails.getDifferentGlobals(secondFileDetails);
        const typeCountChange = typeChanges.Added.length - typeChanges.Removed.length;
        const globalCountChange = globalChanges.Added.length + globalChanges.Changed.length - globalChanges.Removed.length;
        if(printAsCSVLine){
            const __SEP__ = ',' 
            console.log(`${instructionCountChange}${__SEP__}${binaryFileSizeChange}${__SEP__}${locChange}${__SEP__}${typeCountChange}${__SEP__}${numberOfImportChange}/${numberOfFunctionChange}/${numberOfExportChange}${__SEP__}${globalCountChange}`)
            if(nameToPrint.includes(55)){ //print placeholder for now, eventually handle doing optimization level differences (-ol & -s)
                console.log('')
            }
        } else {
            console.log(`${nameToPrint}`)
            console.log(`Instructions Count Change: ${instructionCountChange}`);
            console.log(`Binary Size Change: ${binaryFileSizeChange}`);
            console.log(`Lines of Code Count Change: ${locChange}`);
            console.log(`Type Changes: (${typeCountChange})`, typeChanges)
            console.log(`Number of Imports Count Change: ${numberOfImportChange}`);
            console.log(`Number of Functions Count Change: ${numberOfFunctionChange}`);
            console.log(`Number of Exports Count Change: ${numberOfExportChange}`);
            console.log(`Global Changes: (${globalCountChange})`, globalChanges);
        }
    } catch (err) {
        console.log(err);
    }
}

async function main() {
    const wabtModule = await wabtRequire().then(wabtModule => {return wabtModule});
    const filesToRead = argv._;

    if(argv.dir != null){
        const targetFilesOfPasses = await readPassDirectories();
        for(const [passName, targetFiles] of targetFilesOfPasses.entries()){
            const {baseline, optimized} = targetFiles;
            try{
                await getFileDifferences(baseline, optimized, wabtModule, passName);
            }
            catch(error){
                console.error(passName, error);
                continue;
            }
        }
    } else {
        const firstFile = filesToRead[0];
        const secondFile =  filesToRead[1];
        await getFileDifferences(firstFile, secondFile, wabtModule)
    }
}
main();