node runner.js -t ../15PickLoadSigns/baseline14.wat ../15PickLoadSigns/c15.wat
node runner.js -t ../16Precompute/baseline15.wat ../16Precompute/c16.wat
node runner.js -t ../17PrecomputePropagate/baseline15.wat ../17PrecomputePropagate/c17.wat
node runner.js -t ../18OptimizeAddedConstants/baseline16.wat ../18OptimizeAddedConstants/c18.wat
node runner.js -t ../19OptimizeAddedConstantsPropagate/baseline17.wat ../19OptimizeAddedConstantsPropagate/c19.wat
node runner.js -t ../20CodePushing/baseline18.wat ../20CodePushing/c20.wat
node runner.js -t ../25MergeLocals/baseline24.wat ../25MergeLocals/c25.wat
node runner.js -t ../33CodeFolding/baseline32.wat ../33CodeFolding/c33.wat
node runner.js -t ../38Precompute/baseline37.wat ../38Precompute/c38.wat
node runner.js -t ../39PrecomputePropagate/baseline37.wat ../39PrecomputePropagate/c39.wat
node runner.js -t ../41Rse/baseline40.wat ../41Rse/c41.wat
node runner.js -t ../43DaeOptimizing/baseline42.wat ../43DaeOptimizing/c43.wat
node runner.js -t ../44InliningOptimizing/baseline43.wat ../44InliningOptimizing/c44.wat
node runner.js -t ../44InliningOptimizing/baseline43.wat ../44InliningOptimizing/c44-with-args.wat
node runner.js -t ../47SimplifyGlobals/baseline46.wat ../47SimplifyGlobals/c47.wat
node runner.js -t ../48SimplifyGlobalsOptimizing/baseline46.wat ../48SimplifyGlobalsOptimizing/c48.wat
node runner ../O0/original.wasm ../O0/O0.wasm -t
node runner ../O1/original.wasm ../O1/O1.wasm -t
node runner ../O2/original.wasm ../O2/O2.wasm -t
node runner ../O3/original.wasm ../O3/O3.wasm -t
node runner ../Os/original.wasm ../Os/Os.wasm -t
node runner ../Oz/original.wasm ../Oz/Oz.wasm -t