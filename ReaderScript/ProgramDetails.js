const readline = require('readline');
const fs = require('fs');
const path = require('path');
const uuid = require('uuid/v4');
const {
    promisify
} = require('util');
const exec = promisify(require('child_process').exec);
const readFile = promisify(fs.readFile)
const {
    makeFileHash
} = require('./CommonUtilities');
const TOKENS = require('./token_constants');
const { parse } = require('path');

class FunctionDetails {
    constructor(functionName) {
        this.Name = functionName;
        this.Params = [];
        this.Calls = [];
        this.NumberOfLoops = 0;
        this.NumberOfIfs = 0;
        this.Loops = [];
        this.Ifs = [];
        this.LoopLines = [];
        this.HasResult = false;
        this.ResultType = null;
        this.FunctionType = null;
        this.Locals = [];
        this.InstructionSequence = [];
        // this.LinesOfCode = 0;
        this.StartingLineNumber = 0;
        this.EndingLineNumber = 0;
    }
}

class DataSection {
    constructor(startOffset, payload) {
        this.Start = startOffset;
        this.Payload = payload;
    }
}

class ImportDetails {
    constructor(name, type = TOKENS.FUNCTION_TOKEN) {
        this.Name = name;
        this.Type = type;
        this.FunctionType = null;
        this.ImportedName = null;
    }
}

class ExportDetails {
    constructor(name, type = TOKENS.FUNCTION_TOKEN) {
        this.Name = name;
        this.Type = type;
        this.ExportSource = null;

    }
}

class GlobalDetails {
    constructor(name,index, type, isMutable = false, value= null, valueType = null){
        this.Name = name;
        this.Index = index;
        this.Type = type;
        this.IsMutable = isMutable;
        this.ValueType = valueType;
        this.Value = value;
    }

    equals(otherGlobal){
        const preliminaryChecks = this.Type === otherGlobal.Type
                                    && this.IsMutable === otherGlobal.IsMutable
                                    // && this.Name == otherTypeDetails.Name;
        if(!preliminaryChecks) return false;

        return this.ValueType === otherGlobal.ValueType && this.Value === otherGlobal.Value;

    }
}

class TypeDetails {
    constructor(name, index) {
        this.Name = name;
        this.Index = index;
        this.Params = [];
        this.Result = null;
    }

    equals(otherTypeDetails){
        const preliminaryChecks = this.Result === otherTypeDetails.Result
                                    && this.Params.length === otherTypeDetails.Params.length
                                    // && this.Name == otherTypeDetails.Name;
        if(!preliminaryChecks) return false;

        for(let i = 0; i < this.Params.length; i++){
            const currentParam = this.Params[i];
            const otherParam = otherTypeDetails.Params[i];
            if(currentParam !== otherParam){
                return false;
            }
        }
        return true;
    }
    
}


class ProgramDetailsModel {
    constructor(filename, wabtModule) {
        this.wabt = wabtModule;
        const fullPath = filename;
        const wasmFilename = path.basename(fullPath);
        this.Filename = wasmFilename;
        this.File = filename;
        this.FileHash = null;
        this.OriginalFilename = filename;
        this.WasmFileSize = null;
        this.WatFileSize = null;
        this.WebAssemblyType = null;
        this.NumberOfFunctions = 0;
        this.Functions = {};
        this.Types = {};
        this.Globals = [];
        this.TableFunctions = [];
        this.Imports = [];
        this.Exports = [];
        this.DataSections = [];
        this.TotalInstructionCount = 0;
        this.currentFunction = null;
        this.contextBreakStack = [];
        this.currentGlobalIndex = 0;
        this.currentTypeIndex = 0;
        this.lineNumber = 0;
    }


    onLineReadForDetails(line) {
        let tokens = line.split(/\s+/).map(this.cleanToken).filter(token => token != '');
        // Clean comments
        let markCommentIndex = -1;
        for (let k = 0; k < tokens.length; k++) {
            if (tokens[k].includes(';;')) {
                // this and every token after is a comment
                markCommentIndex = k;
                break;
            }
        }

        if (markCommentIndex !== -1) {
            tokens = tokens.slice(0, markCommentIndex);
        }

        if (this.currentFunction != null) {
            this.Functions[this.currentFunction].EndingLineNumber += 1;
        }
        
        if(TOKENS.all_instructions.includes(tokens[0])){
            this.Functions[this.currentFunction].InstructionSequence.push(line);
            this.TotalInstructionCount += 1;
            return;
        }

        let currentTokenIndex = 0;
        while (currentTokenIndex < tokens.length) {
            const token = tokens[currentTokenIndex];

            if (token == '') {
                currentTokenIndex++;
                continue;
            }
            if (token == TOKENS.DATA_TOKEN && tokens[currentTokenIndex + 2] == TOKENS.i32_const) {
                const startOffset = tokens[currentTokenIndex + 2];
                const payload = tokens.slice(4).join('');
                const dataSection = new DataSection(startOffset, payload);
                this.DataSections.push(dataSection);
            }

            if(currentTokenIndex == 0 && token == TOKENS.GLOBAL_TOKEN){
                const globalIndex = this.currentGlobalIndex;
                this.currentGlobalIndex += 1;
                const globalName = tokens[currentTokenIndex + 1];
                const globalIsMutable = tokens[currentTokenIndex + 2] == 'mut'; 
                const mutableOffset = globalIsMutable ? 0 : 1;
                const globalType = tokens[currentTokenIndex + 3 - mutableOffset];
                const globalValueType = tokens[currentTokenIndex + 4 - mutableOffset];
                let globalValue;
                if(globalValueType == TOKENS.get_global_alt || globalValueType == TOKENS.get_global){
                    globalValue = `${globalValueType} ${tokens[currentTokenIndex + 5 - mutableOffset]}`
                } else {
                    globalValue = tokens[currentTokenIndex + 5 - mutableOffset];
                    
                }

                const detectedGlobal = new GlobalDetails(globalName,globalIndex, globalType, globalIsMutable, globalValue, globalValueType);
                this.Globals.push(detectedGlobal);
            
            }

            if (token == TOKENS.IMPORT_TOKEN && tokens.includes(TOKENS.FUNCTION_TOKEN)) {
                let funcTokenIndex = tokens.indexOf(TOKENS.FUNCTION_TOKEN);
                let typeTokenIndex = tokens.indexOf(TOKENS.FUNCTION_TYPE_TOKEN);
                const importName = tokens.slice(currentTokenIndex + 1, funcTokenIndex).join(' ');
                const importType = TOKENS.FUNCTION_TOKEN;
                const importFunctionType = this.Types[tokens.slice(typeTokenIndex + 1).join('')];
                const importedName = tokens[funcTokenIndex + 1];
                const importDetails = new ImportDetails(importName, importType);
                importDetails.FunctionType = importFunctionType;
                importDetails.ImportedName = importedName;

                this.Imports.push(importDetails);
            }

            if (token == TOKENS.IMPORT_TOKEN && tokens.includes(TOKENS.GLOBAL_TOKEN)) {
                let globalTokenIndex = tokens.indexOf(TOKENS.GLOBAL_TOKEN);
                const globalName = tokens.slice(currentTokenIndex + 1, globalTokenIndex).join(' ');
                const globalIndex = this.currentGlobalIndex;
                this.currentGlobalIndex += 1;
                let globalType = tokens[globalTokenIndex + 2];

                const detectedGlobal = new GlobalDetails(globalName, globalIndex, globalType);
                this.Globals.push(detectedGlobal);
            }

            if (token == TOKENS.EXPORT_TOKEN && tokens.includes(TOKENS.FUNCTION_TOKEN)) {
                let funcTokenIndex = tokens.indexOf(TOKENS.FUNCTION_TOKEN);
                const exportName = tokens.slice(currentTokenIndex + 1, funcTokenIndex).join(' ');
                const exportType = TOKENS.FUNCTION_TOKEN;
                const exportedName = tokens.slice(funcTokenIndex + 1).join('');
                const exportDetails = new ExportDetails(exportName, exportType);
                exportDetails.ExportSource = exportedName;

                this.Exports.push(exportDetails);
            }

            if (token == TOKENS.FUNCTION_TOKEN && tokens[currentTokenIndex + 2] == TOKENS.FUNCTION_TYPE_TOKEN && !tokens.includes(TOKENS.IMPORT_TOKEN)) {
                const functionName = tokens[currentTokenIndex + 1];
                const functionType = tokens[currentTokenIndex + 3];

                // if (this.currentFunction != null && this.Functions[this.currentFunction] != null) {
                //     //Update the line numbers
                //     const prevFunction = this.Functions[this.currentFunction];
                //     prevFunction.EndingLineNumber = this.lineNumber - 1;
                //     prevFunction.LinesOfCode = prevFunction.EndingLineNumber - prevFunction.StartingLineNumber;
                // }

                // Update current function context
                this.currentFunction = functionName;
                // Get the function details
                this.NumberOfFunctions += 1;
                const functionDetails = new FunctionDetails(functionName, this);
                functionDetails.StartingLineNumber = this.lineNumber;
                functionDetails.EndingLineNumber = this.lineNumber;
                functionDetails.FunctionType = functionType;
                for (let i = 3; i < tokens.length; i++) {
                    // Get function parameter details
                    if (tokens[i] == TOKENS.FUNCTION_PARAM_TOKEN) {
                        functionDetails.Params.push({
                            Name: tokens[i + 1],
                            Type: tokens[i + 2]
                        }); // get rid of trailing parenthesis
                    }

                    if (tokens[i] == TOKENS.FUNCTION_RESULT_TOKEN) {
                        functionDetails.HasResult = true;
                        functionDetails.ResultType = tokens[i + 1];
                    }
                }
                if (this.Functions[functionName] == null) {
                    this.Functions[functionName] = functionDetails;
                } else {
                    this.Functions[functionName].Params = functionDetails.Params;
                    this.Functions[functionName].NumberOfLoops = functionDetails.NumberOfLoops;
                    this.Functions[functionName].NumberOfIfs = functionDetails.NumberOfIfs;
                    this.Functions[functionName].Loops = functionDetails.Loops;
                    this.Functions[functionName].LoopLines = functionDetails.LoopLines;
                    this.Functions[functionName].Ifs = functionDetails.Ifs;
                    this.Functions[functionName].HasResult = functionDetails.HasResult;
                    this.Functions[functionName].ResultType = functionDetails.ResultType;
                }

                return;
            }

            if (token == TOKENS.LOCAL_TOKEN && currentTokenIndex == 0) {
                const collectedLocals = []
                for(let localIndex = currentTokenIndex + 1; localIndex < tokens.length ; localIndex++){
                    const currentLocal = tokens[localIndex];
                    collectedLocals.push(currentLocal);
                }

                this.Functions[this.currentFunction].Locals = collectedLocals;
            }

            if (token == TOKENS.FUNCTION_TOKEN && tokens[currentTokenIndex + 1] == TOKENS.EXPORT_TOKEN) {
                const functionName = tokens[currentTokenIndex + 1];
                // Update current function context
                this.currentFunction = functionName;
                // Get the function details
                this.NumberOfFunctions += 1;
                const functionDetails = new FunctionDetails(functionName, this);

                if (this.Functions[functionName] == null) {
                    this.Functions[functionName] = functionDetails;
                } else {
                    this.Functions[functionName].Params = functionDetails.Params;
                    this.Functions[functionName].NumberOfLoops = functionDetails.NumberOfLoops;
                    this.Functions[functionName].NumberOfIfs = functionDetails.NumberOfIfs;
                    this.Functions[functionName].Loops = functionDetails.Loops;
                    this.Functions[functionName].LoopLines = functionDetails.LoopLines;
                    this.Functions[functionName].Ifs = functionDetails.Ifs;
                }

                return;
            }

            if (token == TOKENS.FUNCTION_TYPE_TOKEN && currentTokenIndex == 0) {
                const typeName = tokens[currentTokenIndex + 1];
                const typeIndex = this.currentTypeIndex;
                this.currentTypeIndex += 1;
                const typeDetails = new TypeDetails(typeName,typeIndex);

                let seenParamKeyword = false;
                let seenResultKeyword = false;
                for (let i = currentTokenIndex + 2; i < tokens.length; i++) {
                    if (tokens[i] == TOKENS.FUNCTION_PARAM_TOKEN) {
                        seenParamKeyword = true;
                        continue;
                    }

                    if (tokens[i] == TOKENS.FUNCTION_RESULT_TOKEN) {
                        seenParamKeyword = false;
                        seenResultKeyword = true;
                        continue;
                    }

                    if (seenParamKeyword) {
                        typeDetails.Params.push(tokens[i]);
                    }

                    if (seenResultKeyword) {
                        typeDetails.Result = tokens[i];

                    }
                }

                this.Types[typeName] = typeDetails;
            }

            if (token == TOKENS.elemToken) {
                let globalInstructionIndex = tokens.indexOf(TOKENS.get_global);
                if (globalInstructionIndex == -1) {
                    globalInstructionIndex = tokens.indexOf(TOKENS.get_global_alt);
                } else {
                    globalInstructionIndex += 2;
                }
                if (globalInstructionIndex == -1) {
                    globalInstructionIndex = currentTokenIndex + 3;
                } else {
                    globalInstructionIndex += 2;
                }

                for (let i = globalInstructionIndex; i < tokens.length; i++) {
                    this.TableFunctions.push(tokens[i]);
                }
            }

            if(token == TOKENS.call){
                const functionCallName = tokens[++currentTokenIndex]; //this.cleanToken(tokens[++currentTokenIndex]);
                if (this.Functions[functionCallName] == null) {
                    // this.Functions[functionCallName] = new FunctionDetails(functionCallName);
                    break;
                }
                this.Functions[this.currentFunction].Calls.push({
                    Function: this.Functions[functionCallName],
                    Line: this.lineNumber,
                    Args: [],
                });
            }

            if (token == TOKENS.CALL_INDIRECT_TOKEN) {
                this.Functions[this.currentFunction].Calls.push({
                    Function: {
                        Name: '<call_indirect>',
                        Calls: []
                    },
                    Line: this.lineNumber,
                    Args: [],
                });
            }

            // Look for the start of loops
            if (token == TOKENS.LOOP_TOKEN) {
                this.contextBreakStack.push({
                    type: TOKENS.LOOP_TOKEN,
                    line: this.lineNumber
                });
                this.Functions[this.currentFunction].LoopLines.push(this.lineNumber);
            }

            // Look for the start of if statements
            if (token == TOKENS.IF_TOKEN) {
                this.contextBreakStack.push({
                    type: TOKENS.IF_TOKEN,
                    line: this.lineNumber
                });
            }

            // Look for the end of a loop or if statement
            if (token == TOKENS.END_TOKEN) {
                if (this.contextBreakStack.length > 0) {
                    const lastToken = this.contextBreakStack.pop();

                    if (lastToken.type == TOKENS.LOOP_TOKEN) {
                        this.Functions[this.currentFunction].NumberOfLoops += 1;
                    } else if (lastToken.type == TOKENS.IF_TOKEN) {
                        this.Functions[this.currentFunction].NumberOfIfs += 1;
                        this.Functions[this.currentFunction].Ifs.push({
                            type: TOKENS.IF_TOKEN,
                            start: lastToken.line,
                            end: this.lineNumber,
                        });
                    }
                }


            }


            currentTokenIndex += 1;
        }

    }


    async getFileHash() {
        let filepath = this.File;
        const filehash = await makeFileHash(filepath);
        this.FileHash = filehash;
        return filehash;
    }


    readWatForDetails() {
        if(this.isWasm()){
            return this.convertWasmToWat()
            .then(watString => this.readWatAsString(watString, this.onLineReadForDetails.bind(this)))
        } else {
            this.getBinarySize();
            return this.readWatFile(this.File, this.onLineReadForDetails.bind(this));
        }
    }

    readFileSize(filePath){
        return fs.statSync(filePath).size
    }

    async convertWatToWasm(){
        this.WatFileSize = this.readFileSize(this.File)
        const watFileBuffer = await readFile(this.File);
        const wasmModule = this.wabt.parseWat(this.File, watFileBuffer);
        const wasmBufferResult = wasmModule.toBinary({log: false});
        const wasmBuffer = wasmBufferResult.buffer;
        this.WasmFileSize = wasmBuffer.length;
        return wasmBuffer;
    }

    byteLengthOfString(str) {
        //https://stackoverflow.com/questions/5515869/string-length-in-bytes-in-javascript
        // returns the byte length of an utf8 string
        var s = str.length;
        for (var i=str.length-1; i>=0; i--) {
            var code = str.charCodeAt(i);
            if (code > 0x7f && code <= 0x7ff) s++;
            else if (code > 0x7ff && code <= 0xffff) s+=2;
            if (code >= 0xDC00 && code <= 0xDFFF) i--; //trail surrogate
        }
        return s;
    }
    async convertWasmToWat(){
        this.WasmFileSize = this.readFileSize(this.File)
        const wasmFileBuffer = await readFile(this.File);
        const wasmModule = this.wabt.readWasm(wasmFileBuffer, { readDebugNames: true });
        const watString = wasmModule.toText({ foldExprs: false, inlineExport: false });
        // fs.writeFileSync('./temp.wat', watString);
        wasmModule.destroy();
        this.WatFileSize = this.byteLengthOfString(watString);
        return watString;
    }

    readWatAsString(watString, callback){
        this.lineNumber = 0;
        this.currentFunction = null;
        const watLines = watString.split(/\r?\n/);
        for(const line of watLines){
            this.lineNumber += 1;
            callback(line);
        }
    }

    async getBinarySize(){
        const wasmVersionOfFile = await this.convertWatToWasm();
        const fileSizeInBytes = wasmVersionOfFile.length;
        return fileSizeInBytes; // file size in bytes;
    }

    readWatFile(watFilename, callback) {
        this.lineNumber = 0;
        this.currentFunction = null;
        return new Promise((resolve, reject) => {
            const fileStats = fs.statSync(watFilename);
            this.WatFileSize = fileStats.size;

            const readInterface = readline.createInterface({
                input: fs.createReadStream(watFilename),
            });

            readInterface.on('line', (line) => {
                this.lineNumber += 1;
                callback(line);

            });
            readInterface.on('error', (error) => {
                console.error(error);
                reject(error)
            });

            readInterface.on('close', () => {
                resolve()

            });

        })
    }

    isWasm() {
        const filename = this.Filename;
        if (filename.endsWith('.wasm')) {
            return true;
        }
        return false;
    }

    getFeatures() {
        const fileHash = this.FileHash;

        const cleanImportName = (name) => name.replace(/"/g, '').replace(' ', '.');
        const importFunctions = this.Imports.map(imp => cleanImportName(imp.Name)).join(',')
        const exportFunctions = this.Exports.map(exp => cleanImportName(exp.Name)).join(',')
        const cleanFunctionName = functionName => functionName.replace('$', '')
        const functions = Object.keys(this.Functions).map(cleanFunctionName)
        // .filter(functionName => !importFunctions.includes(functionName))
        const wasmFileSize = this.WasmFileSize ; //bytes
        const watFileSize = this.WatFileSize ;
        const expansionFactor = watFileSize / wasmFileSize;
        const isAsm = this.WebAssemblyType == 'asm';
        const totalLinesOfCode = this.lineNumber;
        let minFunctionLinesOfCode = Number.MAX_SAFE_INTEGER;
        let maxFuntionLinesOfCode = Number.MIN_SAFE_INTEGER;
        let avgFuntionLinesOfCode = 0;
        const numberOfImports = this.Imports.length;
        const numberOfExports = this.Exports.length;
        const numberOfDataSections = this.DataSections.length;
        const numberOfTableEntries = this.TableFunctions.length;
        const internalFunctions = Object.keys(this.Functions).map(cleanFunctionName)
                                    .filter(functionName => !importFunctions.includes(functionName) && !exportFunctions.includes(functionName))
                                    .join(',');


        const originalFunctionNames = Object.keys(this.Functions);
        for (let functionName of originalFunctionNames) {
            let currentFunctionDetails = this.Functions[functionName];
            let numLinesOfCode = currentFunctionDetails.EndingLineNumber - currentFunctionDetails.StartingLineNumber;
            if (numLinesOfCode < minFunctionLinesOfCode) minFunctionLinesOfCode = numLinesOfCode;
            if (numLinesOfCode > maxFuntionLinesOfCode) maxFuntionLinesOfCode = numLinesOfCode;
            avgFuntionLinesOfCode += numLinesOfCode;
        }
        if (originalFunctionNames.length == 0) {
            avgFuntionLinesOfCode = 0;
            minFunctionLinesOfCode = 0;
            maxFuntionLinesOfCode = 0;
        } else {
            avgFuntionLinesOfCode = avgFuntionLinesOfCode / originalFunctionNames.length;

        }

        const numberOfTypes = Object.keys(this.Types).length;
        const Types = this.Types;
        const Globals = this.Globals;
        const TotalInstructionCount = this.TotalInstructionCount;
        
        const FeatureDetails = {
            FileHash: fileHash,
            ImportFunctions: importFunctions,
            ExportFunctions: exportFunctions,
            NumberOfFunctions: functions.length,
            WasmFileSize: wasmFileSize,
            WatFileSize: watFileSize,
            ExpansionFactor: expansionFactor,
            IsAsm: isAsm,
            TotalLinesOfCode: totalLinesOfCode,
            MinFunctionLinesOfCode: minFunctionLinesOfCode,
            MaxFunctionLinesOfCode: maxFuntionLinesOfCode,
            AvgFunctionLinesOfCode: avgFuntionLinesOfCode,
            NumberOfTypes: numberOfTypes,
            NumberOfImports: numberOfImports,
            NumberOfExports: numberOfExports,
            NumberOfDataSections: numberOfDataSections,
            NumberOfTableEntries: numberOfTableEntries,
            InternalFunctions: internalFunctions,
            Types,
            Globals,
            TotalInstructionCount
        }

        return FeatureDetails;
    }

    cleanToken(token) {
        return token.replace(/[\(\)\;]/g, '');
    }

    getDifferentTypes(otherProgramDetails){
        const typesObjToArray = (typesObject) => {
            let returnArr = [];
            for(const key in typesObject){
                const type = typesObject[key];
                returnArr.push(type);
            }
            return returnArr
        }
        const typesArray1 = typesObjToArray(this.Types);
        const typesArray2 = typesObjToArray(otherProgramDetails.Types);

        const typeMapping = new Map();
        for(let i = 0; i < typesArray1.length; i++){
            const leftType= typesArray1[i];
            for(let j =0; j < typesArray2.length; j++){
                const rightType = typesArray2[j];
                if(leftType.equals(rightType)){
                    typeMapping.set(i,j);
                    break;
                }
            }
        }

        const typesFrom1 = [...typeMapping.keys()];
        const typesFrom2 = [...typeMapping.values()];
        const typesWithNoMappingFrom1 = typesArray1.filter((_, index) => !typesFrom1.includes(index)).map(type => type.Name);
        const typesWithNoMappingFrom2 = typesArray2.filter((_, index) => !typesFrom2.includes(index)).map(type => type.Name);

        return {
                    Removed: typesWithNoMappingFrom1,
                    Added: typesWithNoMappingFrom2
                }
    }

    getDifferentGlobals(otherProgramDetails){
        const globalsFrom1 = this.Globals;
        const globalsFrom2 = otherProgramDetails.Globals;
        const excludedIndicesFrom2 = [];
        
        const globalsMapping = new Map();
        for(const leftGlobal of globalsFrom1){
            for(let j= 0; j < globalsFrom2.length; j++){

                if(excludedIndicesFrom2.includes(j)){
                    continue;
                }

                const rightGlobal = globalsFrom2[j];
                if(leftGlobal.equals(rightGlobal)){
                    excludedIndicesFrom2.push(j)
                    globalsMapping.set(leftGlobal.Index,rightGlobal.Index);
                    break;
                }
            }
        }

        const globalsWithMappingFrom1 = [...globalsMapping.keys()];
        const globalsWithMappingFrom2 = [...globalsMapping.values()];
        const globalsWithNoMappingFrom1 = globalsFrom1.filter((_, index) => !globalsWithMappingFrom1.includes(index));
        const globalsWithNoMappingFrom2 = globalsFrom2.filter((_, index) => !globalsWithMappingFrom2.includes(index));

        const changedGlobalNames = []
        for(const globalUnmappedLeft of globalsWithNoMappingFrom1){
            for(const globalUnmappedRight of globalsWithNoMappingFrom2){
                if(globalUnmappedLeft.Name === globalUnmappedRight.Name 
                    && globalUnmappedLeft.Value === globalUnmappedRight.Value){
                    changedGlobalNames.push(globalUnmappedLeft.Name);
                    break;
                }
            }
        }

        const removedGlobals = globalsWithNoMappingFrom1.filter(global => !changedGlobalNames.includes(global.Name)).map(global => global.Name)
        const addedGlobals = globalsWithNoMappingFrom2.filter(global => !changedGlobalNames.includes(global.Name)).map(global => global.Name)

        return {    
            Removed: removedGlobals,
            Added: addedGlobals,
            Changed: changedGlobalNames

        }
    }

    async main() {
        let programDetailResults = null;
        try {
            await this.readWatForDetails();
            programDetailResults = this.getFeatures();
        } catch (error) {
            throw error;
        }
        return programDetailResults;
    }

    

};



module.exports = ProgramDetailsModel;