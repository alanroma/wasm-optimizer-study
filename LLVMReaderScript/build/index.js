"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var argv = require('yargs')
    .option('file', {
    alias: '-f',
    type: 'string',
    description: 'LLVM IR file to read in and proccess'
}).argv;
var fs_1 = __importDefault(require("fs"));
var util_1 = require("util");
var readFile = util_1.promisify(fs_1.default.readFile);
var BlockDetails = /** @class */ (function () {
    function BlockDetails(label) {
        this.Label = label;
    }
    return BlockDetails;
}());
var GlobalValues = /** @class */ (function () {
    function GlobalValues(name, globalType) {
        this.Name = name;
        this.GlobalType = globalType;
    }
    return GlobalValues;
}());
var FunctionDetails = /** @class */ (function () {
    function FunctionDetails(name, lines) {
        this.Blocks = [];
        this.NumberOfBlocks = 0;
        this.LoadLines = [];
        this.NumberOfLoads = 0;
        this.StoreLines = [];
        this.NumberOfStores = 0;
        this.CallLines = [];
        this.NumberOfCalls = 0;
        this.Name = name;
        this.Lines = lines;
    }
    FunctionDetails.prototype.getDetails = function () {
        return {
            Name: this.Name,
            TotalLines: this.Lines.length,
            Blocks: this.NumberOfBlocks,
            Loads: this.NumberOfLoads,
            Stores: this.NumberOfStores,
            Calls: this.NumberOfCalls
        };
    };
    FunctionDetails.prototype.extractBlocks = function () {
        var e_1, _a;
        try {
            for (var _b = __values(this.Lines.slice(1)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var line = _c.value;
                this.findBranchLabelInLine(line);
                this.findLoadsInLine(line);
                this.findStoresInLine(line);
                this.findCallsInLine(line);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    FunctionDetails.prototype.findBranchLabelInLine = function (line) {
        if (line.includes(':') && !line.includes('::')) {
            var colonIndex = line.indexOf(':');
            var labelName = line.slice(0, colonIndex);
            var blockDetails = new BlockDetails(labelName);
            this.NumberOfBlocks += 1;
            this.Blocks.push(blockDetails);
        }
    };
    FunctionDetails.prototype.findLoadsInLine = function (line) {
        var e_2, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_1 = __values(tokens), tokens_1_1 = tokens_1.next(); !tokens_1_1.done; tokens_1_1 = tokens_1.next()) {
                var token = tokens_1_1.value;
                if (token === 'load') {
                    this.LoadLines.push(line);
                    this.NumberOfLoads += 1;
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (tokens_1_1 && !tokens_1_1.done && (_a = tokens_1.return)) _a.call(tokens_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
    };
    FunctionDetails.prototype.findStoresInLine = function (line) {
        var e_3, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_2 = __values(tokens), tokens_2_1 = tokens_2.next(); !tokens_2_1.done; tokens_2_1 = tokens_2.next()) {
                var token = tokens_2_1.value;
                if (token === 'store') {
                    this.StoreLines.push(line);
                    this.NumberOfStores += 1;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (tokens_2_1 && !tokens_2_1.done && (_a = tokens_2.return)) _a.call(tokens_2);
            }
            finally { if (e_3) throw e_3.error; }
        }
    };
    FunctionDetails.prototype.findCallsInLine = function (line) {
        var e_4, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_3 = __values(tokens), tokens_3_1 = tokens_3.next(); !tokens_3_1.done; tokens_3_1 = tokens_3.next()) {
                var token = tokens_3_1.value;
                if (token === 'call') {
                    this.CallLines.push(line);
                    this.NumberOfCalls += 1;
                }
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (tokens_3_1 && !tokens_3_1.done && (_a = tokens_3.return)) _a.call(tokens_3);
            }
            finally { if (e_4) throw e_4.error; }
        }
    };
    return FunctionDetails;
}());
var DataTypes;
(function (DataTypes) {
    DataTypes["i1"] = "i1";
    DataTypes["i8"] = "i32";
    DataTypes["i16"] = "i16";
    DataTypes["i32"] = "i32";
    DataTypes["i64"] = "i64";
    DataTypes["f16"] = "f16";
    DataTypes["f32"] = "f32";
    DataTypes["f64"] = "f64";
    DataTypes["f128"] = "f128";
    DataTypes["v64"] = "v64";
    DataTypes["v128"] = "v128";
    DataTypes["p"] = "p";
})(DataTypes || (DataTypes = {}));
var ParserResults = /** @class */ (function () {
    function ParserResults() {
        this.Functions = [];
        this.Globals = [];
        this.Exports = [];
        this.Imports = [];
    }
    return ParserResults;
}());
var LLVMParser = /** @class */ (function () {
    function LLVMParser() {
        this.Details = new ParserResults();
    }
    LLVMParser.prototype.readInFile = function (filepath) {
        return __awaiter(this, void 0, void 0, function () {
            var fileString;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, readFile(filepath, { encoding: 'utf8' })];
                    case 1:
                        fileString = _a.sent();
                        return [2 /*return*/, fileString];
                }
            });
        });
    };
    LLVMParser.prototype.getDetails = function () {
        var functionDetails = this.Details.Functions.map(function (x) { return x.getDetails(); });
        var numberOfFunctions = functionDetails.length;
        var countDetails = {
            Functions: numberOfFunctions,
            Globals: this.Details.Globals.length,
            Exports: this.Details.Exports.length,
            Imports: this.Details.Imports.length,
            Blocks: functionDetails.reduce(function (acc, curr) { return acc + curr.Blocks; }, 0),
            Calls: functionDetails.reduce(function (acc, curr) { return acc + curr.Calls; }, 0),
            Loads: functionDetails.reduce(function (acc, curr) { return acc + curr.Loads; }, 0),
            Stores: functionDetails.reduce(function (acc, curr) { return acc + curr.Stores; }, 0)
        };
        return countDetails;
    };
    LLVMParser.prototype.findExportLines = function (line) {
        var e_5, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_4 = __values(tokens), tokens_4_1 = tokens_4.next(); !tokens_4_1.done; tokens_4_1 = tokens_4.next()) {
                var token = tokens_4_1.value;
                if (token === 'comdat') {
                    var equalIndex = line.indexOf('=');
                    var exportName = line.slice(0, equalIndex).trim();
                    this.Details.Exports.push(exportName);
                }
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (tokens_4_1 && !tokens_4_1.done && (_a = tokens_4.return)) _a.call(tokens_4);
            }
            finally { if (e_5) throw e_5.error; }
        }
    };
    LLVMParser.prototype.findImportLines = function (line) {
        var e_6, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_5 = __values(tokens), tokens_5_1 = tokens_5.next(); !tokens_5_1.done; tokens_5_1 = tokens_5.next()) {
                var token = tokens_5_1.value;
                if (token === 'declare') {
                    var atSymbolIndex = line.indexOf('@');
                    var functionName = line.slice(atSymbolIndex, line.indexOf('(', atSymbolIndex));
                    this.Details.Imports.push(functionName);
                }
            }
        }
        catch (e_6_1) { e_6 = { error: e_6_1 }; }
        finally {
            try {
                if (tokens_5_1 && !tokens_5_1.done && (_a = tokens_5.return)) _a.call(tokens_5);
            }
            finally { if (e_6) throw e_6.error; }
        }
    };
    LLVMParser.prototype.findFuncAttributesInLine = function (line) {
        if (line.includes('; Function Attrs')) {
            var attributes = line.trim().replace('; Function Attrs', '')
                .split(' ');
            return attributes;
        }
        else {
            return null;
        }
    };
    LLVMParser.prototype.findFuncInLine = function (line) {
        var atSymbolIndex = line.indexOf('@');
        var functionName = line.slice(atSymbolIndex, line.indexOf('(', atSymbolIndex));
        return { functionName: functionName };
    };
    LLVMParser.prototype.findGlobalsInLine = function (line) {
        var e_7, _a;
        var tokens = line.split(' ');
        try {
            for (var tokens_6 = __values(tokens), tokens_6_1 = tokens_6.next(); !tokens_6_1.done; tokens_6_1 = tokens_6.next()) {
                var token = tokens_6_1.value;
                if (token === 'global' || token === "constant") {
                    var equalIndex = line.indexOf('=');
                    var globalName = line.slice(0, equalIndex).trim();
                    var globalDetails = new GlobalValues(globalName, token);
                    this.Details.Globals.push(globalDetails);
                }
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (tokens_6_1 && !tokens_6_1.done && (_a = tokens_6.return)) _a.call(tokens_6);
            }
            finally { if (e_7) throw e_7.error; }
        }
    };
    LLVMParser.prototype.readLLVM = function (filepath) {
        return __awaiter(this, void 0, void 0, function () {
            var fileString, fileLines, lineIndex, line, nextLine, functionLinesBuffer, funcAtrributes, functionLineDetails, functionDetails;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.readInFile(filepath)];
                    case 1:
                        fileString = _a.sent();
                        fileLines = fileString.split('\n');
                        lineIndex = 0;
                        line = fileLines[lineIndex];
                        nextLine = function () {
                            lineIndex += 1;
                            line = fileLines[lineIndex];
                        };
                        while (lineIndex < fileLines.length) {
                            functionLinesBuffer = [];
                            funcAtrributes = this.findFuncAttributesInLine(line);
                            if (funcAtrributes != null) {
                                functionLinesBuffer.push(line);
                                nextLine();
                                functionLineDetails = this.findFuncInLine(line);
                                while (!line.includes('}')) {
                                    functionLinesBuffer.push(line);
                                    nextLine();
                                }
                                functionLinesBuffer.push(line);
                                functionDetails = new FunctionDetails(functionLineDetails.functionName, functionLinesBuffer);
                                functionDetails.extractBlocks();
                                this.Details.Functions.push(functionDetails);
                                functionLinesBuffer = [];
                                nextLine();
                                continue;
                            }
                            this.findGlobalsInLine(line);
                            this.findExportLines(line);
                            this.findImportLines(line);
                            nextLine();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LLVMParser.prototype.main = function () {
        return __awaiter(this, void 0, void 0, function () {
            var file;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        file = argv.file;
                        return [4 /*yield*/, this.readLLVM(file)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return LLVMParser;
}());
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var parser;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                parser = new LLVMParser();
                return [4 /*yield*/, parser.main()];
            case 1:
                _a.sent();
                console.log(parser.getDetails());
                return [2 /*return*/];
        }
    });
}); })();
//# sourceMappingURL=index.js.map