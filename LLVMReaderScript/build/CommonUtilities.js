"use strict";
var crypto = require('crypto');
var fs = require('fs');
var util = require('util');
function makeFileHash(filename, algorithm) {
    if (algorithm === void 0) { algorithm = 'sha256'; }
    //obtained from https://gist.github.com/GuillermoPena/9233069
    return new Promise(function (resolve, reject) {
        // Algorithm depends on availability of OpenSSL on platform
        // Another algorithms: 'sha1', 'md5', 'sha256', 'sha512' ...
        var shasum = crypto.createHash(algorithm);
        try {
            var s = fs.ReadStream(filename);
            s.on('data', function (data) {
                shasum.update(data);
            });
            // making digest
            s.on('end', function () {
                var hash = shasum.digest('hex');
                return resolve(hash);
            });
        }
        catch (error) {
            return reject('calc fail');
        }
    });
}
function pr(obj) {
    console.log(util.inspect(obj, false, null, true /* enable colors */));
}
module.exports.makeFileHash = makeFileHash;
module.exports.pr = pr;
//# sourceMappingURL=CommonUtilities.js.map