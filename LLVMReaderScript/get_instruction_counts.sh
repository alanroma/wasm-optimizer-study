#! /bin/bash

echo "$1"
echo "$2"

removedDiffResults=$(diff $1 $2 --suppress-common-lines -iEZbwB | diff-ignore-moved-lines | grep ^\<)
addedDiffResults=$(diff $1 $2 --suppress-common-lines -iEZbwB | diff-ignore-moved-lines | grep ^\>)
numOfTypes=$( echo "$addedDiffResults" | grep "(type (;" | wc -l)
numOfFunctions=$( echo "$addedDiffResults" | grep "(func (;" | wc -l)
numOfGlobals=$( echo "$addedDiffResults" | grep "(global (;" | wc -l)

firstFileLOC=$(cat $1 | wc -l)
secondFileLOC=$(cat $2 | wc -l)
locDifference=$(( $firstFileLOC - $secondFileLOC ))

echo "Number of types: $numOfTypes"
echo "Number of functions: $numOfFunctions"
echo "Lines of Code changed: $locDifference"
# diff $1 $2 --suppress-common-lines -iEZbwB | diff-ignore-moved-lines > $1-$2-diff.txt 