import {pr} from './CommonUtilities'
const { argv } = require('yargs')
.option('file', {
    alias: '-f',
    type: 'string',
    description: 'LLVM IR file to read in and proccess'
});
import fs from 'fs';
import { promisify } from 'util';
const readFile = promisify(fs.readFile);

declare interface FunctionAttributes{
    [x: number]: string
}

class BlockDetails {
    Label: string;

    constructor(label: string){
        this.Label = label;
    }
}

class GlobalValues {
    Name: string;
    GlobalType: string;
    constructor(name: string, globalType: string){
        this.Name = name;
        this.GlobalType = globalType;
    }
}

class FunctionDetails {
    Name: string;
    Lines: string[];
    Blocks: BlockDetails[] = [];
    NumberOfBlocks = 0;
    LoadLines: string[] = [];
    NumberOfLoads: number = 0;
    StoreLines: string[] = [];
    NumberOfStores = 0;
    CallLines: string[] = [];
    NumberOfCalls = 0;

    constructor(name: string, lines: string[]){
        this.Name = name;
        this.Lines = lines;
    }

    getDetails(){
        return {
            Name: this.Name, 
            TotalLines: this.Lines.length,
            Blocks: this.NumberOfBlocks,
            Loads: this.NumberOfLoads,
            Stores: this.NumberOfStores,
            Calls: this.NumberOfCalls
        }
    }

    extractBlocks() {
        for(const line of this.Lines.slice(1)){
            this.findBranchLabelInLine(line);
            this.findLoadsInLine(line);
            this.findStoresInLine(line);
            this.findCallsInLine(line);
        }
    }

    findBranchLabelInLine(line: string){
        if(line.includes(':') && !line.includes('::')){
            const colonIndex = line.indexOf(':');
            const labelName = line.slice(0, colonIndex);
            const blockDetails = new BlockDetails(labelName);
            this.NumberOfBlocks += 1;
            this.Blocks.push(blockDetails);
        }
    }

    findLoadsInLine(line: string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'load' ){
                this.LoadLines.push(line);
                this.NumberOfLoads += 1;
            }
        }
    }

    findStoresInLine(line: string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'store' ){
                this.StoreLines.push(line);
                this.NumberOfStores += 1;
            }
        }
    }

    findCallsInLine(line: string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'call' ){
                this.CallLines.push(line);
                this.NumberOfCalls += 1;
            }
        }
    }


}

enum DataTypes{
    i1 = 'i1',
    i8 = 'i32',
    i16 = 'i16',
    i32 = 'i32',
    i64 = 'i64',
    f16 = 'f16',
    f32 = 'f32',
    f64 = 'f64',
    f128 = 'f128',
    v64 = 'v64',
    v128 = 'v128',
    p = 'p'
    
} 

class ParserResults {
    Functions: FunctionDetails[] = [];
    Globals: GlobalValues[] = [];
    Exports: string[] = [];
    Imports: string[] = [];
}

class LLVMParser {
    Details: ParserResults = new ParserResults();
    async readInFile(filepath: string){
        const fileString = await readFile(filepath, {encoding: 'utf8'});
        return fileString
    }

    getDetails(){
        const functionDetails = this.Details.Functions.map(x => x.getDetails());
        const numberOfFunctions = functionDetails.length;
        const countDetails = {
            Functions: numberOfFunctions,
            Globals: this.Details.Globals.length,
            Exports: this.Details.Exports.length,
            Imports: this.Details.Imports.length,
            Blocks: functionDetails.reduce((acc, curr) => acc + curr.Blocks, 0),
            Calls: functionDetails.reduce((acc, curr) => acc + curr.Calls, 0),
            Loads: functionDetails.reduce((acc, curr) => acc + curr.Loads, 0),
            Stores: functionDetails.reduce((acc, curr) => acc + curr.Stores, 0)
        };

        return countDetails
    }

    findExportLines(line: string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'comdat' ){
                const equalIndex = line.indexOf('=');
                const exportName = line.slice(0, equalIndex).trim();
                this.Details.Exports.push(exportName);
            }
        }
    }
    findImportLines(line: string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'declare' ){
                const atSymbolIndex = line.indexOf('@');
                const functionName = line.slice(atSymbolIndex, line.indexOf('(', atSymbolIndex))
                this.Details.Imports.push(functionName);
            }
        }
    }
    findFuncAttributesInLine(line: string): FunctionAttributes | null {
        if(line.includes('; Function Attrs')){
            const attributes = line.trim().replace('; Function Attrs', '')
                .split(' ');
            return attributes as FunctionAttributes
        } else {
            return null
        }
    }
    findFuncInLine(line: string){
        const atSymbolIndex = line.indexOf('@');
        const functionName = line.slice(atSymbolIndex, line.indexOf('(', atSymbolIndex))
        return {functionName}
    }
    findGlobalsInLine(line:string){
        const tokens = line.split(' ');
        for(const token of tokens){
            if(token === 'global' || token === "constant"){
                const equalIndex = line.indexOf('=');
                const globalName = line.slice(0, equalIndex).trim();
                const globalDetails = new GlobalValues(globalName, token);
                this.Details.Globals.push(globalDetails);
            }
        }
    }
    async readLLVM(filepath: string){
        const fileString = await this.readInFile(filepath);
        const fileLines = fileString.split('\n');
        let lineIndex = 0;
        let line = fileLines[lineIndex];
        const nextLine = () => {
            lineIndex += 1;
            line = fileLines[lineIndex];
        }
        while(lineIndex < fileLines.length){
            let functionLinesBuffer = [];
            const funcAtrributes = this.findFuncAttributesInLine(line);
            if(funcAtrributes != null){
                functionLinesBuffer.push(line)
                nextLine();
                const functionLineDetails = this.findFuncInLine(line);
                while(!line.includes('}')){
                    functionLinesBuffer.push(line);
                    nextLine();
                }
                functionLinesBuffer.push(line)
                const functionDetails = new FunctionDetails(functionLineDetails.functionName, functionLinesBuffer);
                functionDetails.extractBlocks();
                this.Details.Functions.push(functionDetails);
                functionLinesBuffer = [];
                nextLine();
                continue;
            }
            this.findGlobalsInLine(line);
            this.findExportLines(line);
            this.findImportLines(line);
            nextLine();
        }
    }
    
    async main() {
        const { file } = argv;
        await this.readLLVM(file);
    }
}

(async () => {
    const parser = new LLVMParser();
    await parser.main();
    console.log(parser.getDetails());
})()